#!/bin/bash
# Run unit tests in Docker container.
# To run entire unit test suite:
# ./bin/docker-test.sh
# To run target suite:
# ./bin/docker-test.sh sitepad
# Available suites: core|logpad|sitepad|widgets

# If the syndication/test image doesn't exist, build it.
if [[ "$(docker images -q syndication/test 2> /dev/null)" == "" ]]; then
    docker build -t syndication/test --file ./docker/test/Dockerfile .
fi

docker run --rm -v $PWD/app:/usr/src/app -e CONF=$1 --security-opt="seccomp=unconfined" syndication/test