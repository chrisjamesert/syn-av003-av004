
var opts = require('optimist').argv;

module.exports = function (grunt) {

    return {
        options: {
            // Works here, does not work in shared.conf.js
            // this is pretty ugly
            client: {
                captureConsole: !(opts.quiet || /cons/.test(opts.suppress))
            }
        },
        core : {
            configFile  : 'app/test/conf/core.conf.js'
        },
        logpad : {
            configFile  : 'app/test/conf/logpad.conf.js'
        },
        sitepad : {
            configFile  : 'app/test/conf/sitepad.conf.js'
        },
        widgets : {
            configFile  : 'app/test/conf/widgets.conf.js'
        },
        web : {
            configFile  : 'app/test/conf/web.conf.js'
        },
        'logpad-jenkins' : {
            configFile  : 'app/test/conf/logpad-jenkins.conf.js'
        },
        'trial-common_study-test': {
            configFile: 'trial/test/conf/trial.common_study.conf.js',
            failOnEmptyTestSuite: false
        },
        'trial-handheld-test' : {
            configFile  : 'trial/test/conf/trial.handheld.conf.js',
            failOnEmptyTestSuite: false
        },
        'trial-tablet-test' : {
            configFile  : 'trial/test/conf/trial.tablet.conf.js',
            failOnEmptyTestSuite: false
        },
        'PDE_Core-common-test': {
            configFile  : 'PDE_Core/test/conf/PDE_Core.common.conf.js',
            failOnEmptyTestSuite: false
        },
        'PDE_Core-handheld-test': {
            configFile  : 'PDE_Core/test/conf/PDE_Core.handheld.conf.js',
            failOnEmptyTestSuite: false
        },
        'PDE_Core-tablet-test': {
            configFile  : 'PDE_Core/test/conf/PDE_Core.tablet.conf.js',
            failOnEmptyTestSuite: false
        },
        integrationTest_handheld: {
            configFile : 'PDE_Core/test/conf/integration.hh.conf.js',
            failOnEmptyTestSuite: false
        },
        integrationTest_tablet: {
            configFile : 'PDE_Core/test/conf/integration.tablet.conf.js',
            failOnEmptyTestSuite: false
        },
        validateSD_handheld: {
            configFile : 'trial/test/conf/validateSD.handheld.conf.js',
            failOnEmptyTestSuite: false
        },
        validateSD_tablet: {
            configFile : 'trial/test/conf/validateSD.tablet.conf.js',
            failOnEmptyTestSuite: false
        }
    };
};
