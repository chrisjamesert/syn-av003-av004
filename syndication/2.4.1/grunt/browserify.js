var pathmodify = require('pathmodify'),
    caseVerify = require('dep-case-verify'),
    path = require('path'),
    _ = require('underscore');

var watchOptions = {
    watch: true,
    browserifyOptions: {
        debug: true
    }
};

var settings = {
    options: {
        browserifyOptions: {
            debug : true
        },
        plugin    : [

            // Ensures case-sensitiviy with module imports.
            caseVerify,

            // Configures aliases for common import paths.
            [pathmodify(), {
                mods : [
                    pathmodify.mod.dir('core', path.join(__dirname, '../app/core')),
                    pathmodify.mod.dir('sitepad', path.join(__dirname, '../app/sitepad')),
                    pathmodify.mod.dir('logpad', path.join(__dirname, '../app/logpad')),
                    pathmodify.mod.dir('trainer', path.join(__dirname, '../app/trainer')),
                    pathmodify.mod.dir('web', path.join(__dirname, '../app/web')),
                    pathmodify.mod.dir('trial', path.join(__dirname, '../trial')),
                    pathmodify.mod.dir('PDE_Core', path.join(__dirname, '../PDE_Core'))
                ]
            }]
        ]
    },

    logpad : {
        options : {
            transform : ['babelify']
        },
        files : [{
            src     : ['<%= logpad %>/index.js'],
            dest    : '<%= target %>/logpad/www/runtime.bundle.js'
        }]
    },

    sitepad : {
        options : {
            transform : ['babelify']
        },
        files : [{
            src     : ['<%= sitepad %>/index.js'],
            dest    : '<%= target %>/sitepad/www/runtime.bundle.js'
        }]
    },

    web: {
        options: {
            transform: ['babelify']
        },
        files: [{
            src: ['<%= web %>/index.js'],
            dest: '<%= target %>/web/runtime.bundle.js'
        }]
    },

    'unlock-code' : {
        options : {
            transform : ['babelify']
        },
        files : [{
            src     : ['tools/unlock-code/src/index.js'],
            dest    : '<%= target %>/unlock-code/src/index.js'
        }, {
            src     : ['tools/unlock-code/src/background.js'],
            dest    : '<%= target %>/unlock-code/src/background.js'
        }, {
            src     : ['tools/unlock-code/src/content-script.js'],
            dest    : '<%= target %>/unlock-code/src/content-script.js'
        }]
    }

};

settings.sitepad_watch = _.extend({}, settings.sitepad, {
    options: _.extend({}, settings.sitepad.options, watchOptions)
});

settings.logpad_watch = _.extend({}, settings.logpad, {
    options: _.extend({}, settings.logpad.options, watchOptions)
});

settings.web_watch = _.extend({}, settings.web, {
    options: _.extend({}, settings.web.options, watchOptions)
});

module.exports = settings;
