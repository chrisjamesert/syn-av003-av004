/**
 * Created by uadhikari on 1/3/2018.
 */
module.exports = {
    handheld: {
        options: {
            cwd: './',
            modality : 'handheld'
        },
        files: [{
            dest: '<%= target %>/hash/',
            src: ['trial/handheld/Lib_*']
        }]
    },
    tablet: {
        options: {
            cwd: './',
            modality : 'tablet'
        },
        files: [{
            dest: '<%= target %>/hash/',
            src: ['trial/tablet/Lib_*']
        }]
    }
}
