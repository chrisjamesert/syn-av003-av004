module.exports = {
    web : {
        files: [{
            src : [
                '<%= web %>/sandbox/start.txt',
                '<%= core %>/wrapperjs/Wrapper.js',
                '<%= core %>/wrapperjs/Alarms.js',
                '<%= core %>/wrapperjs/eSense.js',
                '<%= target %>/web/runtime.bundle.js',
                '<%= target %>/web/resources/nls.js',
                '<%= web %>/sandbox/end.txt'
            ],
            dest : '<%= target %>/web/runtime.bundle.js'
        }]
    }
};
