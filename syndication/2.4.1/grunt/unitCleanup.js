/**
 * grunt tasks to cleanup xml output in junit and coverage directories
 */
module.exports = {
    junit: {
        options: {
            cwd: './'
        },
        files: [{
            src: [
                'junit/*.xml'
            ]
        }]
    },
    coverage: {
        options: {
            cwd: './'
        },
        files: [{
            src: [
                'coverage/*.xml'
            ]
        }]
    }
};
