module.exports = {
    logpadCSS: {
        options: {
            start: '<!-- START:Stylesheets -->',
            end: '<!-- END:Stylesheets -->',
            template: '<link rel="stylesheet" type="text/css" href="./%s" />',
            cwd: './'
        },
        files: [{
            target: '<%= target %>/logpad/www/index.html',
            dest: '<%= target %>/logpad/www/index.html',
            src: ['trial/css/*.css']
        }]
    },
    sitepadCSS: {
        options: {
            start: '<!-- START:Stylesheets -->',
            end: '<!-- END:Stylesheets -->',
            template: '<link rel="stylesheet" type="text/css" href="./%s" />',
            cwd: './'
        },
        files: [{
            target: '<%= target %>/sitepad/www/index.html',
            dest: '<%= target %>/sitepad/www/index.html',
            src: ['trial/css/*.css']
        }]
    },
    web_css_prod: {
        options: {
            // useCDN property is just for dev testing purposes
            useCDN: '<%= webConfig.useCDN %>',
            cdnUrl: '<%= webConfig.cdnUrl %>',
            start: '<!-- ALL STYLES:BEGIN -->',
            end: '<!-- ALL STYLES:END -->',
            template: '<link rel="stylesheet" type="text/css" href="%s" />',
            cwd: './'
        },
        files: [{
            cwd: '<%= target %>/web/',
            target: 'index.html',
            dest: 'index.html',
            src: ['stylesheets.min.css']
        }]
    },
    web_scripts_prod: {
        options: {
            // useCDN property is just for dev testing purposes
            useCDN: '<%= webConfig.useCDN %>',
            cdnUrl: '<%= webConfig.cdnUrl %>',
            start: '<!-- ALL SCRIPTS:BEGIN -->',
            end: '<!-- ALL SCRIPTS:END -->',
            template: '<script src="%s"></script>',
            cwd: './'
        },
        files: [{
            cwd: '<%= target %>/web/',
            target: 'index.html',
            dest: 'index.html',
            src: ['scripts.min.js']
        }]
    }
};
