var path = require('path');

module.exports = {
    taco_android_logpad: {
        command: '' +
            'taco platform add android && ' +
            'taco build android && ' +
            'taco run android --livereload',
        cwd: './target/logpad'
    },

    taco_android_sitepad: {
        command: '' +
            'taco platform add android && ' +
            'taco build android && ' +
            'taco run android --livereload',
        cwd: './target/sitepad'
    },

    start_server: {
        command: 'node ./server/bin/www &',
        stdout: false,
        stderr: true
    },

    start_server_with_logs: {
        command: 'node ./server/bin/www &',
        stdout: true,
        stderr: true
    },

    install_android_logpad: {
        command: 'adb install -r android-debug.apk',
        cwd: './target/logpad/platforms/android/build/outputs/apk'
    },

    install_android_sitepad: {
        command: 'adb install -r android-debug.apk',
        cwd: './target/sitepad/platforms/android/build/outputs/apk'
    },

    add_android_sitepad : {
        command : 'cd ./target/sitepad && cordova platform add android',
        stdout  : true,
        stderr  : true
    },

    add_ios_sitepad : {
        command : 'cd ./target/sitepad && cordova platform add ios',
        stdout : true,
        stderr : true
    },

    add_windows_sitepad : {
        command : 'cd ./target/sitepad && cordova platform add windows',
        stdout  : true,
        stderr  : true
    },

    build_android_sitepad : {
        command : 'cd ./target/sitepad && cordova build android',
        stdout  : true,
        stderr  : true
    },

    build_ios_sitepad : {
        cwd: './target/sitepad',
        command : 'cordova build ios --device --debug',
        stdout  : true,
        stderr  : true,
        maxBuffer: 1024 * 1024
    },

    // Requires ios-deploy.  npm install -g ios-deploy
    install_ios_sitepad: {
        cwd: './target/sitepad/platforms/ios/build/device',
        command: 'ios-deploy --debug --bundle "JobName ERT eCOA Tablet.app"',
        stdout  : true,
        stderr  : true
    },

    // To get a full list of available simulators, execute: cordova emulate ios --list
    emulate_ios_sitepad: {
        cwd: './target/sitepad',
        command: 'cordova emulate ios --target="iPhone-6, 9.3"',
        stdout  : true,
        stderr  : true,
        maxBuffer: 1024 * 1024
    },

    build_android_sitepad_release : {
        command : 'cd ./target/sitepad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release -- --keystore=../../syndication.keystore --storePassword=IlovePht --alias=PHT --password=IlovePht',
        //command : 'cd ./target/sitepad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release',
        stdout  : true,
        stderr  : true
    },

    build_windows_sitepad : {
        command : 'cd ./target/sitepad && cordova build windows -- --win --archs="x64"',
        stdout  : true,
        stderr  : true
    },

    build_windows_sitepad_release_x64 : {
        cwd: './target/sitepad',
        //command : 'cd ./target/sitepad && cordova build windows -- --win --archs="x64" --release',
        command: 'cordova build windows -- --win --archs="x64" --release',
        stdout  : true,
        stderr  : true
    },

    build_windows_sitepad_release_x86 : {
        cwd: './target/sitepad',
        //command : 'cd ./target/sitepad && cordova build windows -- --win --archs="x64" --release',
        command: 'cordova build windows -- --win --archs="x86" --release',
        stdout  : true,
        stderr  : true
    },

    build_windows_sitepad_release_arm : {
        cwd: './target/sitepad',
        //command : 'cd ./target/sitepad && cordova build windows -- --win --archs="x64" --release',
        command: 'cordova build windows -- --win --archs="arm" --release',
        stdout  : true,
        stderr  : true
    },

    run_windows_sitepad : {
        command : 'cd ./target/sitepad && cordova run windows -- --win --archs="x64"',
        stdout  : true,
        stderr  : true
    },

    add_android_logpad : {
        command : 'cd ./target/logpad && cordova platform add android',
        stdout  : true,
        stderr  : true
    },

    build_android_logpad : {
        command : 'cd ./target/logpad && cordova build android',
        stdout  : true,
        stderr  : true
    },

    build_android_logpad_release : {
        command : 'cd ./target/logpad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release -- --keystore=../../syndication.keystore --storePassword=IlovePht --alias=PHT --password=IlovePht',
        //command : 'cd ./target/logpad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release',
        stdout  : true,
        stderr  : true
    },

    add_ios_logpad : {
        command : 'cd ./target/logpad && cordova platform add ios',
        stdout  : true,
        stderr  : true
    },

    build_ios_logpad: {
        cwd: './target/logpad',
        command : 'cordova build ios --device --debug',
        stdout  : true,
        stderr  : true,
        maxBuffer: 1024 * 1024
    },

    build_ios_logpad_release: {
        cwd: './target/logpad',
        command : 'cordova build ios --device --release',
        stdout  : true,
        stderr  : true,
        maxBuffer: 1024 * 1024
    },

    // Requires ios-deploy.  npm install -g ios-deploy
    install_ios_logpad: {
        cwd: './target/logpad/platforms/ios/build/device',
        command: 'ios-deploy --debug --bundle "JobName ERT eCOA Handheld.app"',
        stdout  : true,
        stderr  : true
    },

    // To get a full list of available simulators, execute: cordova emulate ios --list
    emulate_ios_logpad: {
        cwd: './target/logpad',
        command: 'cordova emulate ios --target="iPhone-6, 9.3"',
        stdout  : true,
        stderr  : true,
        maxBuffer: 1024 * 1024
    },

    lint_sitepad : {
        command : path.relative('', './node_modules/.bin/eslint') + ' -c .eslintrc-relaxed app/sitepad --ext .js --ext .json --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    lint_core : {
        command : path.relative('', './node_modules/.bin/eslint') + ' -c .eslintrc-relaxed app/core --ext .js --ext .json --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    lint_logpad : {
        command : path.relative('', './node_modules/.bin/eslint') + ' -c .eslintrc-relaxed app/logpad --ext .js --ext .json --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    lint_web : {
        command : path.relative('', './node_modules/.bin/eslint') + ' -c .eslintrc-relaxed app/web --ext .js --ext .json --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    lint_trial : {
        command : path.relative('', './node_modules/.bin/eslint') + ' -c .eslintrc-relaxed trial --ext .js --ext .json --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    // remove this when server side host code is removed from product
    lint_host : {
        command : 'cd ./host && npm run lint',
        stdout  : true,
        stderr  : true
    },

    // remove this when server side host code is removed from product
    ut_host : {
        command : 'cd ./host && npm run test',
        stdout  : true,
        stderr  : true
    },

    custom_config_gen : {
        command : 'node trial/custom-config-gen.js',
        stdout  : true,
        stderr  : true
    },

    custom_config_clean : {
        command : 'node trial/custom-config-clean.js',
        stdout  : true,
        stderr  : true
    },

    extract_lockout_config: {
        command: 'node app/web/extract-lockout-config.js',
        stdout  : true,
        stderr  : true
    }

};
