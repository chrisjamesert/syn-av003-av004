/**
 * Created by uadhikari on 1/4/2018.
 */
module.exports = {
    cleanup: {
        options: {
            cwd: './'
        },
        files: [{
            src: [
                'PDE_Core/common/**/*.json',
                'trial/handheld/**/*.json',
                'trial/tablet/**/*.json'
            ]
        }]
    }
};
