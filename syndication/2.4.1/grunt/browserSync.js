module.exports = {
    dev: {
        bsFiles: {src: './server/public/**'},
        options: {
            proxy: 'localhost:3000',
            port: 8080,
            browser: ''
        }
    }
};
