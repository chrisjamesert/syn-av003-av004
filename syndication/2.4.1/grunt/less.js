module.exports = {

    logpad : {
        options : {
            paths : ['<%= logpad %>less/']
        },
        files : {
            '<%= target %>/logpad/www/css/base.css' : [
                'app/core/less/**/*.less',
                'app/core/screenshot/*.less',
                '<%= logpad %>/less/style.less',
                '<%= logpad %>/less/modules/*.less',
                '<%= logpad %>/less/widgets/*.less',
                'PDE_Core/handheld/**/*.less',
                'trial/common_study/**/*.less',
                'trial/handheld/**/*.less'
            ]
        }
    },

    sitepad : {
        options : {
            paths : ['<%= sitepad %>less/']
        },
        files : {
            '<%= target %>/sitepad/www/css/base.css' : [
                'app/core/less/**/*.less',
                'app/core/screenshot/*.less',
                '<%= sitepad %>/less/base.less',
                '<%= sitepad %>/less/widgets/*.less',
                'PDE_Core/tablet/**/*.less',
                'trial/common_study/**/*.less',
                'trial/tablet/**/*.less'
            ]
        }
    },

    web: {
        options: {
            compress: true,
            paths: ['<%= web %>less/', '<%= sitepad %>less/']
        },
        files: {
            '<%= target %>/web/css/base.css': [
                '<%= core %>/less/**/*.less',
                '<%= core %>/screenshot/*.less',
                '<%= sitepad %>/less/base.less',
                '<%= sitepad %>/less/widgets/*.less',
                '<%= web %>/less/base.less',
                'PDE_Core/tablet/**/*.less',
                'trial/common_study/**/*.less',
                'trial/tablet/**/*.less'
            ]
        }
    }
};
