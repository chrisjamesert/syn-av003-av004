module.exports = {
    web: {
        options: {

        },
        files: [{
            expand: true,
            cwd: '<%= target %>/web',
            src: ['stylesheets.css'],
            dest: '<%= target %>/web',
            ext: '.min.css'
        }]
    }
};
