/**
 * grunt tasks to cleanup xml output in junit and coverage directories
 */
module.exports = {
    'tablet-test-suite': {
        options: {
            cwd: './',
            title: 'Tablet Test Suite'
        },
        files: [{
            src: [
                'junit/junit_reports/tablet-test-suite_report.html'
            ]
        }]
    },
    'handheld-test-suite': {
        options: {
            cwd: './',
            title: 'Handheld Test Suite'
        },
        files: [{
            src: [
                'junit/junit_reports/handheld-test-suite_report.html'
            ]
        }]
    },
    'PDE_Core-test-suite': {
        options: {
            cwd: './',
            title: 'PDE_Core Test Suite'
        },
        files: [{
            src: [
                'junit/junit_reports/PDE_Core-test-suite_report.html'
            ]
        }]
    }
};
