module.exports = {
    logpad: {
        options: {
            wrapper: 'LF.strings.addLanguageResources({content});',
            raw: true
        },
        files: {
            'target/logpad/www/resources/nls.js': [
                'app/core/nls/**/*.json',
                'app/logpad/nls/**/*.json',
                'PDE_Core/common/**/*.json',
                'trial/common_study/**/nls/**/*.json',
                'trial/handheld/**/nls/**/*.json'
            ]
        }
    },
    sitepad: {
        options: {
            wrapper: 'LF.strings.addLanguageResources({content});',
            raw: true
        },
        files: {
            'target/sitepad/www/resources/nls.js': [
                'app/core/nls/**/*.json',
                'app/sitepad/nls/**/*.json',
                'PDE_Core/common/**/*.json',
                'trial/common_study/**/nls/**/*.json',
                'trial/tablet/**/nls/**/*.json'
            ]
        }
    },
    web: {
        options: {
            wrapper: 'LF.strings.addLanguageResources({content});',
            raw: true
        },
        files: {
            'target/web/resources/nls.js': [
                'app/core/nls/**/*.json',
                'app/sitepad/nls/**/*.json',
                'app/web/nls/**/*.json',
                'PDE_Core/common/**/*.json',
                'trial/common_study/**/nls/**/*.json',
                'trial/tablet/**/nls/**/*.json'
            ]
        }
    }
};
