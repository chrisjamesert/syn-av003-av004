module.exports = {
    // Read content of all <link> and <script> tags in target/web/index.html
    // Bundle all stylesheets into single .css file
    // Bundle all js scripts into single .js file
    // Replace tags in index.html, with single <link> and <script> tags
    web: {
        options: {
            cwd: '<%= target %>/web',
            ignore: [
                /nls.js/gi,
                /wrapperjs\/eSense.js/gi,
                /wrapperjs\/Wrapper.js/gi,
                /wrapperjs\/Alarms.js/gi
            ],
            modify: {
                css: function (content) {
                    return content.replace(/\.\.\/fonts/g, 'media/fonts')
                        .replace(/\.\.\/(lib\/(bootstrap|fontawesome)|(media|images))/g, 'media');
                },
                js: function (content) {
                    return content.replace(/trial\/images/g, 'media/images');
                }
            }
        },
        files: [{
            type: 'css',            
            src: ['<%= target %>/web/index.html'],
            dest: '<%= target %>/web/stylesheets.css'
        }, {
            type: 'js',            
            src: ['<%= target %>/web/index.html'],
            dest: '<%= target %>/web/scripts.js'
        }]
    }
};
