'use strict';

var manifest = require('./manifest'),
    sharedConfig = require('./shared.conf');

module.exports = function (config) {

    sharedConfig(config);

    config.set({

        // list of files / patterns to load in the browser
        // Order matters!
        files: manifest.mergeFilesFor('thirdparty', 'core', 'helpers', '~coreSpecs'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude', 'widgets'),

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'core/templates/*.ejs': ['html2js'],
            'logpad/templates/*.ejs': ['html2js'],
            'sitepad/templates/*.ejs': ['html2js'],
            'core/less/**/*.less': ['less'],
            'test/specs/core/**/*.js': ['browserify'],
            'test/helpers/*.js': ['browserify'],
            'core/**/*.js': ['coverage']
        },

        lessPreprocessor: {
            options: {
                paths: ['app/core/less/']
            }
        },

        junitReporter: {
            outputDir: '../junit',
            outputFile: 'TEST-results-core.xml',
            suite: 'Core',
            useBrowserName: false
        },

        coverageReporter: {
            type: 'cobertura',
            dir: '../coverage',
            file: 'core-coverage.xml'
        }
    });
};
