'use strict';

// jscs:disable disallowKeywords
var manifest = require('./manifest'),
    sharedConfig = require('./shared.conf'),
    opts = require('optimist').argv;

module.exports = function (config) {

    sharedConfig(config);

    config.set({

        colors: opts.color, // default false. ANSI escapes make Jenkins console output unreadable

        // list of files / patterns to load in the browser
        // Order matters!
        // Loading specs for both LOGPAD and CORE
        files : manifest.mergeFilesFor('thirdparty', 'core', 'helpers', 'coreSpecs', 'logpad'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude'),

        reporters: ['spec', 'junit', 'coverage'],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            // copied from logpad.conf.js
            'logpad/templates/*.ejs'    : ['html2js'],
            'test/specs/logpad/**/*.js' : ['browserify'],
            'test/helpers/*.js'         : ['browserify'],
            'logpad/**/*.js'            : ['coverage'],

            // copied from core.conf.js
            'core/templates/*.ejs'      : ['html2js'],
            'core/less/**/*.less'       : ['less'],
            'test/specs/core/**/*.js'   : ['browserify'],
            //'test/helpers/*.js'         : ['browserify'],
            'core/**/*.js'              : ['coverage']
        },

        lessPreprocessor: {
            options: {
                paths: ['app/core/less/']
            }
        },

        junitReporter: {
            outputDir: '../junit',
            outputFile: 'TEST-results-logpad.xml',
            suite: 'LogPad+Core',
            useBrowserName: false
        },

        coverageReporter : {
            type: 'cobertura',
            dir: '../coverage',
            file: 'logpad-coverage.xml'
        },

        specReporter: {
            suppressSkipped: true, // these just pollute the console output
            suppressPassed:  false,
            suppressFailed:  false,

            prefixes: {
                success: '[PASS] ',
                failure: '[FAIL] ',
                skipped: '[SKIP] '
            }
        }
    });
};
