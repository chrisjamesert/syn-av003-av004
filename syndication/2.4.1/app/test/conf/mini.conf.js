'use strict';

var manifest = require('./manifest'),
    sharedConfig = require('./shared.conf');

module.exports = function (config) {

    sharedConfig(config);

    config.set({

        // list of files / patterns to load in the browser
        // Order matters!
        files : manifest.mergeFilesFor('thirdparty', 'core', 'helpers', '~mini'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude'),

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'test/specs/mini/**/*.js'  : ['browserify'],
            'test/helpers/*.js'        : ['browserify'],
            'core/**/*.js'             : ['coverage']
        },

        coverageReporter : {
            dir       : '../coverage',
            reporters : [
                { type: 'text-summary' },
                { type: 'cobertura', file: 'mini-coverage.xml' }
            ]
        }

    });
};
