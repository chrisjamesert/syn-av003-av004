xdescribe('Helpers', function () {

    let questions,
        schedules = LF.StudyDesign.schedules;

    beforeEach(function () {
        localStorage.setItem('krpt', 'krpt.39ed120a0981231');

        // MOCK Subject data.
        LF.Data.Subjects = new LF.Collection.Subjects([
            {
                id                  : 1,
                subject_id          : '12345',
                subject_password    : 'apple',
                krpt                : '39ed120a0981231'
            }
        ]);

        questions = new LF.Collection.Questions([
            {
                id          : 'DAILY_DIARY_Q_1',
                IG          : 'Daily_Diary',
                IT          : 'DAILY_DIARY_Q_1',
                help        : 'HELP_TEXT',
                text        : 'QUESTION_1',
                className   : 'DAILY_DIARY_Q_1',
                widget      : {
                    id          : 'DAILY_DIARY_W_1',
                    type        : 'CustomRadioButton',
                    templates   : { },
                    answers     : [
                        {
                            text    : 'NO_PROBLEMS_WALKING',
                            value   : '0'
                        }, {
                            text    : 'SOME_PROBLEMS_WALKING',
                            value   : '1'
                        }, {
                            text    : 'UNABLE_WALKING',
                            type    : 'number',
                            value   : '2'
                        }
                    ]
                }
            }, {
                id          : 'DAILY_DIARY_Q_2',
                IG          : 'Daily_Diary',
                IT          : 'DAILY_DIARY_Q_2',
                help        : 'HELP_TEXT',
                text        : 'QUESTION_2',
                className   : 'DAILY_DIARY_Q_2',
                widget      : {
                    id          : 'DAILY_DIARY_W_2',
                    type        : 'RadioButton',
                    className   : 'DAILY_DIARY_W_2',
                    templates   : { },
                    answers     : [
                        {
                            text    : 'NO_PROBLEMS_SELF_CARE',
                            value   : '0'
                        }, {
                            text    : 'SOME_PROBLEMS_SELF_CARE',
                            value   : '1'
                        }, {
                            text    : 'UNABLE_SELF_CARE',
                            value   : '2'
                        }
                    ]
                }
            }, {
                id          : 'DAILY_DIARY_Q_3',
                IG          : 'Daily_Diary',
                IT          : 'DAILY_DIARY_Q_3',
                help        : 'HELP_TEXT',
                text        : 'QUESTION_3',
                className   : 'DAILY_DIARY_Q_3',
                widget      : {
                    id          : 'DAILY_DIARY_W_3',
                    type        : 'RadioButton',
                    className   : 'DAILY_DIARY_W_3',
                    templates   : {},
                    answers     : [
                        {
                            text    : 'NO_PROBLEMS_USUAL_ACTIVITIES',
                            value   : '0'
                        }, {
                            text    : 'SOME_PROBLEMS_USUAL_ACTIVITIES',
                            value   : '1'
                        }, {
                            text    : 'UNABLE_USUAL_ACTIVITIES',
                            value   : '2'
                        }
                    ]
                }
            }, {
                id          : 'DAILY_DIARY_Q_4',
                IG          : 'Daily_Diary',
                IT          : 'DAILY_DIARY_Q_4',
                help        : 'HELP_TEXT',
                text        : 'QUESTION_4',
                className   : 'DAILY_DIARY_Q_4',
                widget      : {
                    id          : 'DAILY_DIARY_W_4',
                    type        : 'RadioButton',
                    className   : 'DAILY_DIARY_W_4',
                    templates   : {},
                    answers     : [
                        {
                            text    : 'NO_PAIN',
                            value   : '0'
                        }, {
                            text    : 'SOME_PAIN',
                            value   : '1'
                        }, {
                            text    : 'EXTREME_PAIN',
                            value   : '2'
                        }
                    ]
                }
            }, {
                id          : 'DAILY_DIARY_Q_5',
                IG          : 'Daily_Diary',
                IT          : 'DAILY_DIARY_Q_5',
                help        : 'HELP_TEXT',
                text        : 'QUESTION_5',
                className   : 'DAILY_DIARY_Q_5',
                widget      : {
                    id          : 'DAILY_DIARY_W_5',
                    type        : 'RadioButton',
                    className   : 'DAILY_DIARY_W_5',
                    templates   : { },
                    answers     : [
                        {
                            text    : 'NO_ANXIETY',
                            value   : '0'
                        }, {
                            text    : 'SOME_ANXIETY',
                            value   : '1'
                        }, {
                            text    : 'EXTREME_ANXIETY',
                            value : '2'
                        }
                    ]
                }
            }, {
                id          : 'DAILY_DIARY_Q_6',
                IG          : 'Daily_Diary',
                IT          : 'DAILY_DIARY_Q_6',
                text        : 'QUESTION_6',
                className   : 'DAILY_DIARY_Q_6',
                widget      : {
                    id          : 'DAILY_DIARY_W_6',
                    type        : 'NumericRatingScale',
                    className   : 'DAILY_DIARY_W_6',
                    templates   : { },
                    markers     : {
                        left    : 'BEST_HEALTHSTATE',
                        right   : 'WORST_HEALTHSTATE'
                    },
                    answers : [
                        {
                            text    : 'NRS_0',
                            value   : '0'
                        }, {
                            text    : 'NRS_1',
                            value   : '1'
                        }, {
                            text    : 'NRS_2',
                            value   : '2'
                        }, {
                            text    : 'NRS_3',
                            value   : '3'
                        }, {
                            text    : 'NRS_4',
                            value   : '4'
                        }, {
                            text    : 'NRS_5',
                            value   : '5'
                        }, {
                            text    : 'NRS_6',
                            value   : '6'
                        }, {
                            text    : 'NRS_7',
                            value   : '7'
                        }, {
                            text    : 'NRS_8',
                            value   : '8'
                        }, {
                            text    : 'NRS_9',
                            value   : '9'
                        }, {
                            text    : 'NRS_10',
                            value   : '10'
                        }
                    ]
                }
            }
        ]);
    });

    afterEach(function () {
        localStorage.clear();
        LF.Data = {};
        LF.Gateways.Site = {};
        LF.Gateways.Subject = {};
        LF.logs.reset();
        LF.StudyDesign.schedules = schedules;
    });

    it('should install database', function () {
        LF.SpecHelpers.installDatabase();
    });

    it('should check local storage.', function () {
        spyOn(LF.StudyRules, 'activate');
        LF.Helpers.checkLocalStorage();

        expect(LF.StudyRules.activate).not.toHaveBeenCalled();
    });

    it('should add cache event listeners.', function () {
        let response;

        spyOn(window.applicationCache, 'addEventListener');

        LF.Helpers.addAppCacheEventListeners($.noop);

        expect(window.applicationCache.addEventListener).toHaveBeenCalled();
    });

    it('should check the install.', function () {
        let response;

        LF.Data.Subjects.at(0).save({}, {
            onSuccess :function () {
                LF.Helpers.checkInstall(function (result) {
                    response = result;
                });
            },
            onError : function () {return false;}
        });

        waitsFor(function () {
            return response;
        }, 'install to be checked', 2000);

        runs(function () {
            expect(response).toBeTruthy();
        });
    });

    it('should get the answer records from the views in memory.', function () {
        let views = [],
            questionnaires = LF.StudyDesign.questionnaires,
            viewQuestionnaire,
            data = {
                Dashboard : new LF.Model.Dashboard({
                    id                  : 1,
                    subject_id          : '12345',
                    instance_ordinal    : 1,
                    questionnaire_id    : 'DAILY_DIARY',
                    SU                  : 'DAILY_DIARY'
                }),
                Answers : new LF.Collection.Answers()
            };
        LF.Data.Questionnaire = {};
        LF.StudyDesign.questionnaires = new LF.Collection.Questionnaires([
            {
                id: 'Daily_Diary',
                SU: 'Daily_Diary',
                displayName: 'DISPLAY_NAME',
                className: 'DAILY_DIARY',
                previousScreen: true,
                affidavit: false,
                screens: [
                    'DAILY_DIARY_S_1',
                    'DAILY_DIARY_S_2',
                    'DAILY_DIARY_S_3',
                    'DAILY_DIARY_S_4',
                    'DAILY_DIARY_S_5',
                    'DAILY_DIARY_S_6'
                ]
            }
        ]);
        viewQuestionnaire = new LF.View.QuestionnaireView({
            id : 'Daily_Diary'
        });
        viewQuestionnaire.data.dashboard = data.Dashboard;
        viewQuestionnaire.data.answers = data.Answers;
        viewQuestionnaire.data.igList = { Daily_Diary: 0 };
        viewQuestionnaire.data.currentIGR = { Daily_Diary: 0 };

        // Create new question views.
        questions.each(function (question, index) {
            views.push(new LF.View.QuestionView({
                id          : question.get('id'),
                screen      : `DAILY_DIARY_S_${index}`,
                model       : question,
                parent      : viewQuestionnaire,
                mandatory   : false
            }));
        });

        _(views).each(function (view, index) {
            view.widget = new LF.Widget.Base({
                model   : new LF.Model.Widget({
                    id          : 'DAILY_DIARY_W_1',
                    type        : 'Base',
                    templates   : { },
                    answers     : [
                        {
                            text    : 'NO_PROBLEMS_WALKING',
                            value   : '0'
                        }, {
                            text    : 'SOME_PROBLEMS_WALKING',
                            value   : '1'
                        }, {
                            text    : 'UNABLE_WALKING',
                            type    : 'number',
                            value   : '2'
                        }
                    ]
                }),
                mandatory   : true,
                parent      : view
            });

            view.widget.addAnswer();
            view.widget.respond(view.widget.answers.at(0), index.toString());
        });

        LF.Data.Questionnaire = {
            QuestionViews : views
        };

        LF.StudyDesign.questionnaires = questionnaires;

        expect(LF.Helpers.getResponses('DAILY_DIARY_Q_1')[0].response).toEqual('0');
        expect(LF.Helpers.getResponses('DAILY_DIARY_Q_6')[0].response).toEqual('5');
        expect(LF.Helpers.getResponses('DAILY_DIARY_Q_')).toBeFalsy();

    });

    it('should get the widget object from the views in memory.', function () {
        let views = [],
            questionnaires = LF.StudyDesign.questionnaires,
            viewQuestionnaire,
            data = {
                Dashboard : new LF.Model.Dashboard({
                    id                  : 1,
                    subject_id          : '12345',
                    instance_ordinal    : 1,
                    questionnaire_id    : 'DAILY_DIARY',
                    SU                  : 'DAILY_DIARY'
                }),
                Answers : new LF.Collection.Answers()
            };

        LF.Data.Questionnaire = {};
        LF.StudyDesign.questionnaires = new LF.Collection.Questionnaires([
            {
                id: 'Daily_Diary',
                SU: 'Daily_Diary',
                displayName: 'DISPLAY_NAME',
                className: 'DAILY_DIARY',
                previousScreen: true,
                affidavit: false,
                screens: [
                    'DAILY_DIARY_S_1',
                    'DAILY_DIARY_S_2',
                    'DAILY_DIARY_S_3',
                    'DAILY_DIARY_S_4',
                    'DAILY_DIARY_S_5',
                    'DAILY_DIARY_S_6'
                ]
            }
        ]);
        viewQuestionnaire = new LF.View.QuestionnaireView({
            id: 'Daily_Diary'
        });
        viewQuestionnaire.data.dashboard = data.Dashboard;
        viewQuestionnaire.data.answers = data.Answers;
        viewQuestionnaire.data.igList = { Daily_Diary: 0 };
        viewQuestionnaire.data.currentIGR = { Daily_Diary: 0 };

        // Create new question views.
        questions.each(function (question, index) {
            views.push(new LF.View.QuestionView({
                id          : question.get('id'),
                screen      : `DAILY_DIARY_S_${index}`,
                model       : question,
                parent      : viewQuestionnaire,
                mandatory   : false
            }));
        });

        _(views).each(function (view, index) {
            view.widget = new LF.Widget.Base({
                model   : new LF.Model.Widget({
                    id          : 'DAILY_DIARY_W_1',
                    type        : 'Base',
                    templates   : { },
                    answers     : [
                        {
                            text    : 'NO_PROBLEMS_WALKING',
                            value   : '0'
                        }, {
                            text    : 'SOME_PROBLEMS_WALKING',
                            value   : '1'
                        }, {
                            text    : 'UNABLE_WALKING',
                            type    : 'number',
                            value   : '2'
                        }
                    ]
                }),
                mandatory   : true,
                parent      : view
            });

            view.widget.addAnswer();
            view.widget.respond(view.widget.answers.at(0), index.toString());
        });

        LF.Data.Questionnaire = {
            QuestionViews : views
        };

        LF.StudyDesign.questionnaires = questionnaires;

        expect(LF.Helpers.getWidget('DAILY_DIARY_Q_1')).toBeTruthy();
        expect(LF.Helpers.getWidget('DAILY_DIARY_Q_6')).toBeTruthy();
        expect(LF.Helpers.getWidget('DAILY_DIARY_Q_')).toBe(null);

    });

    it('should create empty gateways schedule collections', function () {

        LF.StudyDesign.schedules = new LF.Collection.Schedules();

        LF.Helpers.loadGatewaySchedules();

        expect(LF.Gateways.Site.schedules.length).toEqual(0);
        expect(LF.Gateways.Subject.schedules.length).toEqual(0);

    });

    it('should create subject gateway schedule collection with \'1\' item', function () {

        LF.StudyDesign.schedules = new LF.Collection.Schedules([{
            id: 'Daily_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                gateway: 'subject',
                id: 'P_Daily_Diary'
            }
        }]);

        runs(function () {
            LF.Helpers.loadGatewaySchedules();
        });

        waitsFor(function () {
            return LF.Gateways.Subject.schedules.length === 1;
        }, 'Gateway Subject to update', 2000);

        runs(function () {
            expect(LF.Gateways.Subject.schedules.length).toEqual(1);
            expect(LF.Gateways.Site.schedules.length).toEqual(0);
        });

    });

    it('should create site gateway schedule collection with \'1\' item', function () {

        LF.StudyDesign.schedules = new LF.Collection.Schedules([{
            id: 'Daily_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                gateway: 'site',
                id: 'P_Daily_Diary'
            }
        }]);

        runs(function () {
            LF.Helpers.loadGatewaySchedules();
        });

        waitsFor(function () {
            return LF.Gateways.Site.schedules.length === 1;
        }, 'Gateway Subject to update', 2000);

        runs(function () {
            expect(LF.Gateways.Subject.schedules.length).toEqual(0);
            expect(LF.Gateways.Site.schedules.length).toEqual(1);
        });

    });

    it('should create site and subject gateway schedule collections with \'1\' item', function () {

        LF.StudyDesign.schedules = new LF.Collection.Schedules([{
            id: 'Daily_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                gateway: 'site',
                id: 'P_Daily_Diary'
            }
        }, {
            id: 'Med_01',
            target: {
                objectType: 'questionnaire',
                gateway: 'subject',
                id: 'P_Daily_Diary'
            }
        }]);

        runs(function () {
            LF.Helpers.loadGatewaySchedules();
        });

        waitsFor(function () {
            return LF.Gateways.Subject.schedules.length === 1 && LF.Gateways.Site.schedules.length === 1;
        }, 'Gateway Subject to update', 2000);

        runs(function () {
            expect(LF.Gateways.Subject.schedules.length).toEqual(1);
            expect(LF.Gateways.Site.schedules.length).toEqual(1);
        });

    });

    it('should throw an error. Incorrect gateway type defined', function () {
        let logger = LF.Log.Logger('Helpers.loadGatewaySchedules');

        LF.StudyDesign.schedules = new LF.Collection.Schedules([{
            id: 'Daily_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                gateway: 'sit',
                id: 'P_Daily_Diary'
            }
        }]);

        runs(function () {
            LF.Helpers.loadGatewaySchedules();
        });

        waitsFor(function () {
            return logger.loggingEvents.length > 0;
        }, 2000);

        runs(function () {
            expect(logger.loggingEvents[0].message).toEqual('Incorrect gateway type: gateway type is not subject or site');
        });

    });

    it('should uninstall database.', function () {
        LF.SpecHelpers.uninstallDatabase();
    });
});
