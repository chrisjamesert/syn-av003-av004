import * as helpers from 'test/helpers/SpecHelpers';
import * as lStorage from 'core/lStorage';
import WebService from 'core/classes/WebService';

describe('WebService', () => {
    let serviceBase = LF.StudyDesign.environments.at(0).get('url');
    let webService,
        res;

    beforeEach(() => {
        lStorage.setItem('serviceBase', serviceBase);
        LF.appName = 'LogPad App';
        webService = new WebService();
    });

    afterEach(() => {
        webService = undefined;
        localStorage.clear();
    });

    describe('Using getKrDom method', () => {
        helpers.setupAjax(`${serviceBase}/api/v1/SWAPI/GetDataClin`, [{
            sitecode: '450',
            krdom: 'DOM.91916.573'
        }]);

        Async.beforeEach(() => {
            return webService.getkrDom()
            .then((r) => {
                res = r.res;
            });
        });

        it('should receive admin password using getAdminPassword', () => {
            expect(res.sitecode).toBe('450');
            expect(res.krdom).toBe('DOM.91916.573');
        });
    });

    describe('Using getAdminPassword', () => {
        helpers.setupAjax(`${serviceBase}/api/v1/SWAPI/GetDataClin`, [{
            role: 'Admin',
            hash: '0xee9dab80da2e4165abb19324df1b29a1',
            key: 'apple'
        }]);

        Async.beforeEach(() => {
            lStorage.setItem('site', JSON.stringify({
                sitecode: 1,
                krdom: 'DOM.91356.559'
            }));

            return webService.getAdminPassword()
            .then((r) => {
                res = r.res;
            });
        });

        it('should receive admin password using getAdminPassword', () => {
            expect(res.role).toBe('Admin');
            expect(res.hash).toBe('0xee9dab80da2e4165abb19324df1b29a1');
            expect(res.key).toBe('apple');
        });
    });

    TRACE_MATRIX('US7392')
    .describe('sendDiary', () => {
        Async.it('should set the correct jsonh value for the ajaxConfig in sendDiary', () => {
            let diary = {
                    diary_id: '1',
                    SU: 'Meds',
                    Answers: [{}, {}]
                },
                ajaxConfig = {
                    type: 'POST',
                    uri: webService.buildUri('/v1/diaries/'),
                    auth: ''
                };

            spyOn(webService, 'transmit').and.resolve();

            spyOn(lStorage, 'getItem').and.callFake((key) => {
                switch (key) {
                    case 'PHT_Authorization':
                        return '123:abcdef456:789abc-def';
                    case 'serviceBase':
                        return serviceBase;
                    default:
                        return null;
                }
            });

            return webService.sendDiary(diary, '', true)
            .then(() => {
                ajaxConfig.jsonh = true;
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, diary);
            })
            .then(() => webService.sendDiary(diary, '', false))
            .then(() => {
                ajaxConfig.jsonh = false;
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, diary);
            });
        });
    });
});

// The test is disabled so let's disable the eslint rules as well
/* eslint-disable */   
xdescribe('WebService - OLD', function () {

    beforeEach(function () {
        LF.webService = new LF.Class.WebService();
    });

    afterEach(function () {
        LF.webService = undefined;
        localStorage.clear();
        LF.logs.reset();
    });

    it('should get subject\'s active using getSubjectActive', function () {

        let setupCode = '12345678',
            active,
            subjectSyncID,
            deviceReplaced,
            response;

        fakeAjax({
            registrations: [{
                url: `../../api/v1/subjects/${setupCode}`,
                type: 'GET',
                success: {
                    data: {
                        isActive: true
                    },
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '0';
                                case 'X-Sync-ID':
                                    return null;
                                default:
                                    return '';
                            }
                        },
                        status : 200
                    }
                }
            }]
        });

        LF.webService.getSubjectActive(setupCode, function (res, { syncID, isSubjectActive }) {
            active = res.isActive;
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(active).toBeTruthy();
            expect(subjectSyncID).toBeNull();
            expect(deviceReplaced).toBeTruthy();
        });

    });

    it('should get error on getting subject\'s active using getSubjectActive', function () {

        let setupCode = '12345678',
            errCode,
            httpErrCode,
            deviceReplaced,
            response;

        fakeAjax({
            registrations: [{
                url: `../../api/v1/subjects/${setupCode}`,
                type: 'GET',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return '6';
                                default:
                                    return '';
                            }
                        },
                        status: 500
                    }
                }
            }]
        });

        LF.webService.getSubjectActive(setupCode, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('6');
            expect(httpErrCode).toEqual(500);
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should throw an error because setupCode is null or undefined for getSubjectActive', function () {

        expect(function () {
            LF.webService.getSubjectActive();
        }).toThrow('Missing Argument: setupCode is null or undefined.');

    });

    it('should do subject sync using doSubjectSync', function () {

        let setupCode = '12345678',
            auth = '123:456:78',
            phase,
            logLevel,
            subjectSyncID,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_syncID', '12');

        fakeAjax({
            registrations: [{
                url: `../../api/v1/subjects/${setupCode}`,
                type: 'GET',
                success: {
                    data: {
                        Phase: 10,
                        LogLevel: 'ERROR'
                    },
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '0';
                                case 'X-Sync-ID':
                                    return '12';
                                default:
                                    return '';
                            }
                        },
                        status: 200
                    }
                }
            }]
        });

        LF.webService.doSubjectSync(setupCode, auth, function (res, { syncID, isSubjectActive }) {
            phase = res.Phase;
            logLevel = res.LogLevel;
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(phase).toEqual(10);
            expect(logLevel).toEqual('ERROR');
            expect(subjectSyncID).toEqual('12');
            expect(deviceReplaced).toBeTruthy();
        });

    });

    it('should get error on subject sync using doSubjectSync', function () {

        let setupCode = '12345678',
            auth = '123:456:78',
            errCode,
            httpErrCode,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_syncID', '12');

        fakeAjax({
            registrations: [{
                url: `../../api/v1/subjects/${setupCode}`,
                type: 'GET',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '0';
                                case 'X-Sync-ID':
                                    return '3';
                                default:
                                    return '';
                            }
                        },
                        status: 500
                    }
                }
            }]
        });

        LF.webService.doSubjectSync(setupCode, auth, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('3');
            expect(httpErrCode).toEqual(500);
            expect(deviceReplaced).toBeTruthy();
        });
    });

    it('should throw an error because setupCode is null or undefined for doSubjectSync', function () {

        expect(function () {
            LF.webService.doSubjectSync();
        }).toThrow('Missing Argument: setupCode is null or undefined.');

    });

    it('should get subject\'s data using query using querySubjectData', function () {

        let setupCode = '12345678',
            auth = '123:456:78',
            queryList = ['Phase', 'LogLevel', 'SiteCode'],
            phase,
            logLevel,
            siteCode,
            subjectSyncID,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_syncID', '12');

        fakeAjax({
            registrations: [{
                url: `../../api/v1/subjects/${setupCode}\\?fields=Phase,LogLevel,SiteCode`,
                type: 'GET',
                success: {
                    data: {
                        Phase: 10,
                        LogLevel: 'FATAL',
                        SiteCode: '555'
                    },
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '0';
                                case 'X-Sync-ID':
                                    return '13';
                                default:
                                    return '';
                            }
                        },
                        status: 200
                    }
                }
            }]
        });

        LF.webService.querySubjectData(setupCode, queryList, auth, function (res, { syncID, isSubjectActive }) {
            phase = res.Phase;
            logLevel = res.LogLevel;
            siteCode = res.SiteCode;
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(phase).toEqual(10);
            expect(logLevel).toEqual('FATAL');
            expect(siteCode).toEqual('555');
            expect(subjectSyncID).toEqual('13');
            expect(deviceReplaced).toBeTruthy();
        });

    });

    it('should get error on getting subject\'s data using query using querySubjectData', function () {

        let setupCode = '12345678',
            auth = '123:456:78',
            queryList = [],
            errCode,
            httpErrCode,
            deviceReplaced,
            response;

        fakeAjax({
            registrations: [{
                url: `../../api/v1/subjects/${setupCode}`,
                type: 'GET',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return '3';
                                default:
                                    return '';
                            }
                        },
                        status: 404
                    }
                }
            }]
        });

        LF.webService.querySubjectData(setupCode, queryList, auth, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('3');
            expect(httpErrCode).toEqual(404);
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should throw an error because setupCode is null or undefined for querySubjectData', function () {

        expect(function () {
            LF.webService.querySubjectData();
        }).toThrow('Missing Argument: setupCode is null or undefined.');

    });

    it('should throw an error because queryList is null or undefined for querySubjectData', function () {

        expect(function () {
            LF.webService.querySubjectData('12345678');
        }).toThrow('Missing Argument: queryList is null or undefined.');

    });

    it('should update the subject\'s data and should return nothing using updateSubjectData', function () {

        let deviceID = '4f4-17bd-38134c',
        auth = '123:456:78',
        subjectData = {
            Question: '1',
            Answer: 'aaaa'
        },
        result,
        subjectSyncID,
        deviceReplaced,
        response;

        fakeAjax({
            registrations: [{
                url: `../../api/v1/devices/${deviceID}`,
                type: 'PUT',
                success: {
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return null;
                                default:
                                    return '';
                            }
                        },
                        status: 200
                    }
                }
            }]
        });

        LF.webService.updateSubjectData(deviceID, subjectData, auth, function (res, { syncID, isSubjectActive }) {
            result = res;
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(result).toBeUndefined();
            expect(subjectSyncID).toBeNull();
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should get error on updating subject\'s data using updateSubjectData', function () {

        let deviceID = '4f4-17bd-38134c',
        auth = '123:456:78',
        subjectData = {
            Question: '1',
            Answer: 'aaaa'
        },
        errCode,
        httpErrCode,
        deviceReplaced,
        response;

        fakeAjax({
            registrations: [{
                url: `../../api/v1/devices/${deviceID}`,
                type: 'PUT',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '0';
                                case 'X-Sync-ID':
                                    return '3';
                                default:
                                    return '';
                            }
                        },
                        status: 500
                    }
                }
            }]
        });

        LF.webService.updateSubjectData(deviceID, subjectData, auth, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('3');
            expect(httpErrCode).toEqual(500);
            expect(deviceReplaced).toBeTruthy();
        });

    });

    it('should throw an error because deviceID is null or undefined for updateSubjectData', function () {
        expect(function () {
            LF.webService.updateSubjectData();
        }).toThrow('Missing Argument: deviceID is null or undefined.');
    });

    it('should get the device ID using getDeviceID', function () {

        let subjectData = {
                UserID: '12345678',
                Password: 'qqqq'
            },
            device,
            pwd,
            subjectSyncID,
            deviceReplaced,
            response;

        fakeAjax({
            registrations: [{
                url: '../../api/v1/devices',
                type: 'POST',
                success: {
                    data: {
                        deviceID: '123-abc',
                        Password: 'abc123def'
                    },
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return null;
                                default:
                                    return '';
                            }
                        },
                        status : 200
                    }
                }
            }]
        });

        LF.webService.getDeviceID(subjectData, function (res, { syncID, isSubjectActive }) {
            device = res.deviceID;
            pwd = res.Password;
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(device).toEqual('123-abc');
            expect(pwd).toEqual('abc123def');
            expect(subjectSyncID).toBeNull();
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should get the device ID using getDeviceID with unlock code', function () {
        let subjectData = {
                UserID: '12345678',
                Password: 'qqqq',
                code: 123456
            },
            device,
            pwd,
            subjectSyncID,
            deviceReplaced,
            response;

        fakeAjax({
            registrations: [{
                url: '../../api/v1/devices',
                type: 'POST',
                success: {
                    data: {
                        deviceID: '123-abc',
                        Password: 'abc123def'
                    },
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return null;
                                default:
                                    return '';
                            }
                        },
                        status: 200
                    }
                }
            }]
        });

        LF.webService.getDeviceID(subjectData, function (res, { syncID, isSubjectActive }) {
            device = res.deviceID;
            pwd = res.Password;
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(device).toEqual('123-abc');
            expect(pwd).toEqual('abc123def');
            expect(subjectSyncID).toBeNull();
            expect(deviceReplaced).toBeFalsy();
            expect(subjectData.code).toBeUndefined();
        });

    });

    it('should get error on getting device ID using getDeviceID', function () {

        let subjectData = {
                UserID: '12345678',
                Password: 'qqqq'
            },
            errCode,
            httpErrCode,
            deviceReplaced,
            response;

        fakeAjax({
            registrations: [{
                url: '../../api/v1/devices',
                type: 'POST',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return '3';
                                default:
                                    return '';
                            }
                        },
                        status: 500
                    }
                }
            }]
        });

        LF.webService.getDeviceID(subjectData, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('3');
            expect(httpErrCode).toEqual(500);
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should send diary successfully and return nothing using sendDiary', function () {

        let diary = {
                diary_id: '1',
                SU: 'Meds',
                Answers: [{}, {}]
            },
            subjectSyncID,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_Authorization', '123:abcdef456:789abc-def');

        fakeAjax({
            registrations: [{
                url: '../../api/v1/diaries/',
                type: 'POST',
                success: {
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return null;
                                default:
                                    return '';
                            }
                        },
                        status: 200
                    }
                }
            }]
        });

        LF.webService.sendDiary(diary, '', true, function (res, { syncID, isSubjectActive }) {
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(subjectSyncID).toBeNull();
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should get error on sending diary using sendDiary', function () {

        let diary = {
                diary_id  : '1',
                SU       : 'Meds',
                Answers  : [{}, {}]
            },
            errCode,
            httpErrCode,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_Authorization', '123:abcdef456:789abc-def');

        fakeAjax({
            registrations: [{
                url: '../../api/v1/diaries/',
                type: 'POST',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return '3';
                                default:
                                    return '';
                            }
                        },
                        status: 500
                    }
                }
            }]
        });

        LF.webService.sendDiary(diary, '', true, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('3');
            expect(httpErrCode).toEqual(500);
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should send logs successfully and return nothing using sendLogs', function () {

        let logs = {
                id: '1',
                params: [{}, {}]
            },
            subjectSyncID,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_Authorization', '123:abcdef456:789abc-def');

        fakeAjax({
            registrations: [{
                url: '../../api/v1/logs/',
                type: 'POST',
                success: {
                    status: 'success',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return null;
                                default:
                                    return '';
                            }
                        },
                        status: 200
                    }
                }
            }]
        });

        LF.webService.sendLogs(logs, function (res, { syncID, isSubjectActive }) {
            subjectSyncID = syncID;
            deviceReplaced = !isSubjectActive;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(subjectSyncID).toBeNull();
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should get error on sending logs using sendLogs', function () {

        let logs = {
                id: '1',
                params: [{}, {}]
            },
            errCode,
            httpErrCode,
            deviceReplaced,
            response;

        localStorage.setItem('PHT_Authorization', '123:abcdef456:789abc-def');

        fakeAjax({
            registrations: [{
                url: '../../api/v1/logs/',
                type: 'POST',
                error: {
                    status: 'error',
                    xhr: {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return '3';
                                default:
                                    return '';
                            }
                        },
                        status: 500
                    }
                }
            }]
        });

        LF.webService.sendLogs(logs, $.noop, function (errorCode, httpCode, isSubjectActive) {
            errCode = errorCode;
            httpErrCode = httpCode;
            deviceReplaced = !isSubjectActive;
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(errCode).toEqual('3');
            expect(httpErrCode).toEqual(500);
            expect(deviceReplaced).toBeFalsy();
        });

    });

    it('should throw an error because type is null or undefined for transmit', function () {
        let ajaxConfig = {};

        expect(function () {
            LF.webService.transmit(ajaxConfig);
        }).toThrow('Missing Argument: type is null or undefined.');

    });

    it('should throw an error because uri is null or undefined for transmit', function () {
        let ajaxConfig = {
            type : 'GET'
        };

        expect(function () {
            LF.webService.transmit(ajaxConfig);
        }).toThrow('Missing Argument: uri is null or undefined.');

    });

    it('should throw an error because setupCode is null or undefined for getSubjectQuestion', function () {
        expect(function () {
            LF.webService.getSubjectQuestion();
        }).toThrow('Missing Argument: setupCode is null or undefined.');
    });

    it('should get subject\'s security question using getSubjectQuestion', function () {
        let setupCode = '12345678',
            response,
            question;

        fakeAjax({
            registrations: [{
                url: `../../api/v1/devices\\?UserID=${setupCode}`,
                type: 'GET',
                success: {
                    data: [{
                        Q: 3
                    }],
                    status: 'success',
                    xhr: {
                        getResponseHeader: $.noop,
                        status: 200
                    }
                }
            }]
        });

        LF.webService.getSubjectQuestion(setupCode, function (res, { syncID, isSubjectActive }) {
            question = res[0].Q;
            response = true;
        }, $.noop);

        waitsFor(function () {
            return response;
        }, 'transmission to complete.', 1000);

        runs(function () {
            expect(question).toEqual(3);
        });
    });

    xit('should sync the last diaries with an empty result set.', (done) => {
        fakeAjax({
            registrations: [{
                url: '../../api/v1/SWAPI/GetDataClin',
                type: 'POST',
                success: {
                    data: '[]',
                    status: 'success',
                    xhr: {
                        getResponseHeader: $.noop,
                        status: 200
                    }
                }
            }]
        });

        LF.studyWebService.syncLastDiary({}, (res) => {
            expect(result).toEqual([]);
            done();
        });
    });

    xit('should sync the last diaries with last diary result set', () => {
        LF.StudyDesign.questionnaires = new Questionnaires([{
            id: 'P_Daily_Diary',
            SU: 'Daily',
            displayName: 'DISPLAY_NAME',
            screens: []
        }, {
            id: 'Medication',
            SU: 'Meds',
            displayName: 'DISPLAY_NAME',
            screens: []
        }]);

        fakeAjax({
            registrations: [{
                url: '../../api/v1/SWAPI/GetDataClin',
                type: 'POST',
                success: {
                    data: '[{"SU": "Daily", "lastCompletedDate": "2014-03-17T19:43:43Z", "lastStartedDate": "2014-03-17T19:43:29Z"}, ' +
                        '{"SU": "Meds", "lastCompletedDate": "2014-02-11T16:46:02Z", "lastStartedDate": "2014-02-11T16:45:57Z"}]',
                    status: 'success',
                    xhr: {
                        getResponseHeader: $.noop,
                        status: 200
                    }
                }
            }]
        });

        LF.studyWebService.syncLastDiary({}, (result) => {
            expect(result.length).toEqual(2);
            expect(result[0].questionnaire_id).toEqual('Daily_Diary');
            expect(result[1].questionnaire_id).toEqual('Medication');
        });
    });
});
/* eslint-enable */
