// The test is disabled so let's disable the eslint rules as well
/* eslint-disable */

xdescribe('DatabaseUpgradeCheck', function () {

    let model;

    beforeEach(function () {

        model = new LF.Model.DatabaseVersion({
            versionName : 'DB_Core',
            version     : 0
        });

        LF.dataAccess = new LF.DataAccess({
            name        : 'Unit_Test_Data',
            version     : '1.0',
            description : 'LogPad LF unit test data.',
            size        : 5 * 1024 * 1024
        });

    });

    afterEach(function () {

        LF.dataAccess = undefined;

    });

    it('should get database version 0 when DatabaseVersion table is missing', function () {

        let response,
            logger = Log4js.getLogger('Helpers.getDatabaseVersion'),
            versionName = 'DB_Core';

        spyOn(logger, 'log');

        LF.Helpers.getDatabaseVersion(versionName, function (res) {
            response = res;
        });

        waitsFor(function () {
            return response === 0;
        }, 'getDatabaseVersion callback to be invoked.', 1000);

        runs(function () {
            expect(response).toEqual(0);
            expect(logger.log).toHaveBeenCalledWith(Log4js.Level.INFO, 'DatabaseVersion table does not exist. Database version is 0');
        });

    });

    it('should not get database version with no versionName used in getDatabaseVersion function', function () {

        let response,
            logger = Log4js.getLogger('Helpers.getDatabaseVersion'),
            versionName;

        spyOn(logger, 'log');

        LF.Helpers.getDatabaseVersion(versionName, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'getDatabaseVersion callback to be invoked.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
            expect(logger.log).toHaveBeenCalledWith(Log4js.Level.ERROR, 'Missing version type: version type is null or undefined.');
        });

    });

    it('should create the DatabaseVersion storage object from the schema', function () {

        let response;

        model.createStorage({
            onSuccess : function () {
                response = true;
            }
        });

        waitsFor(function () {
            return response;
        }, 'database storage object to be created.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
        });

    });

    it('should save the DatabaseVersion model to the database.', function () {

        let response;

        model.save({ }, {
            onSuccess : function (res) {
                response = res;
            }
        });

        waitsFor(function () {
            return response;
        }, 'save the DatabaseVersion model to the storage object', 1000);

        runs(function () {
            expect(response).toEqual(1);
        });

    });

    it('should return 0 if wrong versionName is used in getDatabaseVersion function', function () {

        let response,
            versionName = 'DB_Apple';

        LF.Helpers.getDatabaseVersion(versionName, function (res) {
            response = res;
        });

        waitsFor(function () {
            return response === 0;
        }, 'getDatabaseVersion callback to be invoked.', 1000);

        runs(function () {
            expect(response).toEqual(0);
        });

    });

    it ('should upgrade database with sql upgrades', function () {

        let response,
            params = {
                config : [
                {
                    version : 1,
                    sql : [
                        'CREATE TABLE IF NOT EXISTS DatabaseVersions (id INTEGER PRIMARY KEY AUTOINCREMENT, versionName VARCHAR, version INTEGER);',
                        'INSERT INTO DatabaseVersions (versionName, version) VALUES (\'DB_Core\', 0);',
                        'INSERT INTO DatabaseVersions (versionName, version) VALUES (\'DB_Study\', 0);'
                    ]
                },{
                    version : 2,
                    sql : [
                        'CREATE TABLE IF NOT EXISTS SiteUsers (id INTEGER PRIMARY KEY AUTOINCREMENT, site_user_name VARCHAR, last_used VARCHAR);',
                        'CREATE TABLE IF NOT EXISTS Sites (id INTEGER PRIMARY KEY AUTOINCREMENT, site_code VARCHAR, study_name VARCHAR, hash VARCHAR);',
                        'CREATE TABLE IF NOT EXISTS Schedules (id INTEGER PRIMARY KEY AUTOINCREMENT, scheduleId VARCHAR, target VARCHAR, scheduleFunction VARCHAR, ' +
                            'scheduleParams VARCHAR, alarmFunction VARCHAR, alarmParams VARCHAR, phase VARCHAR);'
                    ]
                }],
                versionName : 'DB_Core'
            };

        LF.Actions.databaseUpgradeCheck(params, function () {
            LF.dataAccess.transaction('fetchAll', {
                storage : 'DatabaseVersions'
            }, function (res) {
                model.set(res[0]);
                response = true;
            });
        });

        waitsFor(function () {
            return response;
        }, 'the DatabaseVersion data to be fetched from the database.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
            expect(model.get('id')).toEqual(1);
            expect(model.get('version')).toEqual(2);
            expect(model.get('versionName')).toEqual('DB_Core');
        });

    });

    it ('should upgrade database with javascript upgrades', function () {

        let response,
            params = {
                config : [
                    {
                        version: 3,
                        upgradeFunction : function (callback) {
                            callback(true);
                        }
                    },{
                        version: 9,
                        upgradeFunction : function (callback) {
                            callback(true);
                        }
                    }],
                versionName : 'DB_Core'
            };

        LF.Actions.databaseUpgradeCheck(params, function () {
            LF.dataAccess.transaction('fetchAll', {
                storage : 'DatabaseVersions'
            }, function (res) {
                model.set(res[0]);
                response = true;
            });
        });

        waitsFor(function () {
            return response;
        }, 'the DatabaseVersion data to be fetched from the database.', 1000);

        runs(function () {
            expect(model.get('id')).toEqual(1);
            expect(model.get('version')).toEqual(9);
            expect(model.get('versionName')).toEqual('DB_Core');
            expect(response).toBeTruthy();
        });

    });

    it ('should try to run same database upgrade after it was completed successfully', function () {

        let response,
            params = {
                config : [
                    {
                        version: 3,
                        upgradeFunction : function (callback) {
                            callback(true);
                        }
                    },{
                        version: 9,
                        upgradeFunction : function (callback) {
                            callback(true);
                        }
                    }],
                versionName : 'DB_Core'
            };

        LF.Actions.databaseUpgradeCheck(params, function () {
            LF.dataAccess.transaction('fetchAll', {
                storage : 'DatabaseVersions'
            }, function (res) {
                model.set(res[0]);
                response = true;
            });
        });

        waitsFor(function () {
            return response;
        }, 'the DatabaseVersion data to be fetched from the database.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
            expect(model.get('id')).toEqual(1);
            expect(model.get('version')).toEqual(9);
            expect(model.get('versionName')).toEqual('DB_Core');
        });

    });

    it ('should fail database upgrade', function () {

        let response,
            params = {
                config : [
                    {
                        version: 10,
                        upgradeFunction : function (callback) {
                            callback(false);
                        }
                    }
                ],
                versionName : 'DB_Core'
            };

        spyOn(LF.Actions, 'notify').andCallFake(function () {
            LF.dataAccess.transaction('fetchAll', {
                storage : 'DatabaseVersions'
            }, function (res) {
                model.set(res[0]);
                response = true;
            });
        });
        LF.Actions.databaseUpgradeCheck(params, $.noop);

        waitsFor(function () {
            return response;
        }, 'the notification to have been called.', 1000);

        runs(function () {
            expect(model.get('id')).toEqual(1);
            expect(model.get('version')).toEqual(9);
            expect(model.get('versionName')).toEqual('DB_Core');
            expect(response).toBeTruthy();
            expect(LF.Actions.notify).toHaveBeenCalled();
        });

    });

    it ('should run invalid database upgrade', function () {

        let response,
            logger = Log4js.getLogger('Actions.databaseUpgradeCheck'),
            params = {
                config : [
                    {
                        version: 11
                    }
                ],
                versionName : 'DB_Core'
            };

        spyOn(logger, 'log');

        LF.Actions.databaseUpgradeCheck(params, function () {
            LF.dataAccess.transaction('fetchAll', {
                storage : 'DatabaseVersions'
            }, function (res) {
                model.set(res[0]);
                response = true;
            });
        });

        waitsFor(function () {
            return response;
        }, 'the DatabaseVersion data to be fetched from the database', 1000);

        runs(function () {
            expect(model.get('id')).toEqual(1);
            expect(model.get('version')).toEqual(9);
            expect(model.get('versionName')).toEqual('DB_Core');
            expect(response).toBeTruthy();
            expect(logger.log).toHaveBeenCalledWith(Log4js.Level.ERROR, 'Database Upgrade did not process for version 11 of DB_Core: No sql or function configured');
        });

    });

    it('should remove the DatabaseVersion storage', function () {

        LF.SpecHelpers.uninstallDatabase(['DatabaseVersion']);

    });

});
