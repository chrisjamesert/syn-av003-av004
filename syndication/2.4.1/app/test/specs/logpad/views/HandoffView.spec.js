import HandoffView from 'logpad/views/HandoffView';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import * as lStorage from 'core/lStorage';
import EULA from 'core/classes/EULA';

const emptyFlashParam = {
    patientEULA: '',
    siteEULA: ''
};

describe('HandoffView', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();

        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/handoff.ejs');

        CurrentContext.init();
        CurrentContext().setContextLanguage('en-US');

        view = new HandoffView();
        spyOn(view, 'navigate').and.stub();

        return view.resolve()
        .then(() => view.render());
    });

    afterEach(() => {
        localStorage.clear();
        removeTemplate();
        view = null;
    });

    it('should render handoff view', () => {
        expect(view.$el.attr('id')).toEqual('handoff-page');
    });

    describe('method:next for activation', () => {
        it('should redirect to EULA page', () => {
            localStorage.setItem('activationFlag', true);
            view.next();

            expect(view.navigate).toHaveBeenCalledWith('end_user_license_agreements', true, emptyFlashParam);
        });

        it('should redirect to privacy policy page if EULA has been accepted', () => {
            localStorage.setItem('activationFlag', true);
            EULA.accept();
            view.next();

            expect(view.navigate).toHaveBeenCalledWith('privacy_policy_activation', true);
        });
    });

    describe('method:next for re-activation', () => {
        it('should redirect to EULA page', () => {
            localStorage.setItem('activationFlag', false);
            view.next();

            expect(view.navigate).toHaveBeenCalledWith('reactivation', true);
        });
    });

    
});
