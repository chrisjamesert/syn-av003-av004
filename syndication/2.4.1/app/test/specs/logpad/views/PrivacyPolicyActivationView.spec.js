import PrivacyPolicyActivationView from 'logpad/views/PrivacyPolicyActivationView';
import ELF from 'core/ELF';

import * as specHelpers from 'test/helpers/SpecHelpers';

describe('PrivacyPolicyActivationView', () => {
    let view,
        removeTemplate;

    beforeEach(done => {
        LF.StudyDesign = { };

        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/privacy-policy-activation.ejs');
        view = new PrivacyPolicyActivationView();

        spyOn(view, 'navigate').and.stub();

        view.resolve()
            .then(() => view.render())
            .catch(e => fail(e))
            .done(done);
    });

    afterEach(() => {
        removeTemplate();
        view = null;
    });

    it('should render  \'privacy-policy-activation-view\'', () => {
        expect(view.$el.attr('id')).toEqual('privacy-policy-activation-view');
    });

   describe('method:back', () => {
        it('should trigger a rule.', () => {
            spyOn(ELF, 'trigger').and.stub();

            view.back();

            expect(ELF.trigger).toHaveBeenCalledWith('PRIVACYPOLICYACTIVATION:Backout', {}, jasmine.any(Object));
        });
    });

    describe('method:activation', () => {
        it('should redirect to \'activation-page\'', () => {
            view.activation();
            expect(view.navigate).toHaveBeenCalledWith('activation');
        });
    });

});
