import ChangeSecretQuestionView from 'logpad/views/ChangeSecretQuestionView';
import SecretQuestionBaseView from 'core/views/SecretQuestionBaseView';
import Logger from 'core/Logger';
import User from 'core/models/User';
import CurrentSubject from 'core/classes/CurrentSubject';
import Subject from 'core/models/Subject';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('ChangeSecretQuestionView', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();

        LF.security = jasmine.createSpyObj('security', [
            'checkLogin',
            'getUser',
            'pauseSessionTimeOut',
            'restartSessionTimeOut',
            'listenForActivity',
            'on',
            'off'
        ]);

        LF.security.checkLogin.and.callFake(() => Q(true));
        LF.security.getUser.and.callFake(() => {
            let user = new User({
                id: 1,
                username: 'System Administrator',
                secretAnswer: hex_sha512('apple'),
                password: hex_sha512('appleorange'),
                salt: 'orange',
                secretQuestion: '0',
                role: 'admin'
            });

            LF.security.activeUser = user;

            return Q(user);
        });

        let subject = new Subject({
            phase: '10',
            subject_active: 0
        });

        spyOn(CurrentSubject, 'getSubject').and.callFake(() => {
            return Q(subject);
        });

        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/change-secret-question.ejs');
        view = new ChangeSecretQuestionView({ subject });

        spyOn(view, 'navigate').and.stub();

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id of \'change-password-page\'.', () => {
        expect(view.id).toBe('change-secret-question-page');
        expect(view.$el.attr('id')).toBe('change-secret-question-page');
    });

    it('should have a template of \'#change-secret-question-template\'.', () => {
        expect(view.template).toBe('#change-secret-question-template');
    });

    describe('method:back', () => {
        it('should navigate back to the toolbox view.', () => {
            view.back();

            expect(view.navigate).toHaveBeenCalledWith('toolbox');
        });
    });

    describe('method:resolve', () => {
        Async.it('should resolve.', () => {
            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.user).toEqual(jasmine.any(User));
            });
        });
    });

    describe('method:render', () => {
        Async.it('should render the view.', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.$el.html()).toBeDefined();
            });
        });

        Async.it('should fail to render the view.', () => {
            spyOn(view, 'buildHTML').and.reject('Error');
            spyOn(Logger.prototype, 'error').and.stub();

            let request = view.render();

            expect(request).toBePromise();

            return request.catch(e => {
                expect(e).toBe('Error');
            });
        });
    });

    describe('method:onSecretQuestionSaved', () => {
        Async.it('should navigate back to the toolbox.', () => {
            spyOn(ELF, 'trigger').and.resolve();
            spyOn(Logger.prototype, 'operational').and.resolve();

            let request = view.onSecretQuestionSaved();

            expect(request).toBePromise();

            return request.then(() => {
                expect(LF.security.restartSessionTimeOut).toHaveBeenCalled();
                expect(view.navigate).toHaveBeenCalledWith('toolbox');
                expect(Logger.prototype.operational)
                    .toHaveBeenCalledWith('Security question/answer reset by user System Administrator with role admin');
            });
        });
    });

    describe('method:submit', () => {
        it('should show an input error.', () => {
            spyOn(view, 'showInputError').and.stub();

            view.$currentPassword.val('12345');

            view.submit({ preventDefault: $.noop });

            expect(view.showInputError).toHaveBeenCalledWith(view.$currentPassword, 'PASSWORD_INVALID');
        });

        it('should submit.', () => {
            spyOn(SecretQuestionBaseView.prototype, 'submit').and.stub();

            view.$currentPassword.val('apple');

            view.submit({ preventDefault: $.noop });

            expect(SecretQuestionBaseView.prototype.submit).toHaveBeenCalled();
        });
    });
});
