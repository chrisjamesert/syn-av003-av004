import SupportView from 'logpad/views/SupportView';
import Subject from 'core/models/Subject';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('SupportView', () => {

    let removeTemplate,
        view,
        location;

   Async.beforeEach(() => {
        let subject = new Subject({ subject_id: '100-001' });
        resetStudyDesign();

        // Fetch the template used for the customerSupportView and render it to the DOM.
        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/support.ejs');
        view = new SupportView({ subject: subject });
        spyOn(view, 'navigate').and.callFake((url) => location = url);
        spyOn($.fn, 'select2').and.stub();

        return view.render();
    });

   Async.afterEach(() => specHelpers.uninstallDatabase());

    Async.it('should render the view with an id of support-page', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.id).toBe('support-page');
            });
    });

    it('should render correct number of support list elements', () => {
        view.$('#support_dropdown').val(0);
        specHelpers.waitsFor(() => {view.showSupportNumber(0);}).then(() => {
            expect($('#support-list li').length).toEqual(LF.StudyDesign.supportOptions[0].number.length);
        });
    });
    it('should navigate back to the settings view', () => {
        view.back();

        expect(location).toEqual('toolbox');
    });

});
