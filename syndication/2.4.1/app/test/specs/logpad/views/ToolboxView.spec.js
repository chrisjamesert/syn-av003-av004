import ToolboxView from 'logpad/views/ToolboxView';
import Schedules from 'core/collections/Schedules';
import Subjects from 'core/collections/Subjects';
import Subject from 'core/models/Subject';
import ELF from 'core/ELF';
import * as Helpers from 'core/Helpers';
import CurrentSubject from 'core/classes/CurrentSubject';

import toolboxViewData from './ToolboxViewData';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class ToolboxViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();
        LF.StudyDesign.schedules = new Schedules(toolboxViewData.schedules);
        localStorage.setItem('krpt', '*');

        this.view = new ToolboxView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/toolbox.ejs');

        // spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
        //     this.add({ id: 1, subject_active: 1 });

        //     return Q();
        // });

        // Stub this for now... we can call through later.
        spyOn(this.view, 'showSettingsItems').and.stub();

        return specHelpers.initializeContent()
            .then(() => this.view.resolve())
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(ToolboxViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should fail to resolve.', () => {
                spyOn(CurrentSubject, 'getSubject').and.reject('DatabaseError');

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method resolve to be rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));

            });

            Async.it('should resolve.', () => {
                spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
                    this.add({ id: 1, subject_active: 1, krpt: '*' });

                    return Q();
                });

                delete this.view.subject;

                expect(this.view.subject).toBe(undefined);

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.subject).toEqual(jasmine.any(Subject));
                });
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            it('should navigate to the dashboard view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('dashboard');
            });
        });
    }

    testAbout () {
        describe('method:about', () => {
            it('should navigate to the About view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.about();

                expect(this.view.navigate).toHaveBeenCalledWith('about');
            });
        });
    }

    testChangePassword () {
        describe('method:changePassword', () => {
            it('should navigate to the Change Password view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.changePassword();

                expect(this.view.navigate).toHaveBeenCalledWith('change_password');
            });
        });
    }

    testChangeSecretQuestion () {
        describe('method:changeSecretQuestion', () => {
            it('should navigate to the Change Secret Question view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.changeSecretQuestion();

                expect(this.view.navigate).toHaveBeenCalledWith('change_secret_question');
            });
        });
    }

    testPrivacyPolicy () {
        describe('method:privacyPolicy', () => {
            it('should navigate to the Privacy Policy view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.privacyPolicy();

                expect(this.view.navigate).toHaveBeenCalledWith('privacy_policy');
            });
        });
    }

    testSupport () {
        describe('method:support', () => {
            it('should navigate to the Support view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.support();

                expect(this.view.navigate).toHaveBeenCalledWith('support');
            });
        });
    }

    testSetAlarms () {
        describe('method:setAlarms', () => {
            it('should navigate to the Set Alarms view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.setAlarms();

                expect(this.view.navigate).toHaveBeenCalledWith('set_alarms');
            });
        });
    }
    testTakeMessageBoxScreenshots () {
        describe('method:takeMessageBoxScreenshots', () => {
            it('should navigate to the Message Box Screenshot view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.takeMessageBoxScreenshots();

                expect(this.view.navigate).toHaveBeenCalledWith('take_messagebox_screenshots');
            });
        });
    }

    testSetTimeZone () {
        describe('method:setTimeZone', () => {
            it('should navigate to the Set Timezone view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.setTimeZone();

                expect(this.view.navigate).toHaveBeenCalledWith('set_time_zone');
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should not render the view.', () => {
                spyOn(this.view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to have been rejected.'))
                    .catch(e => {
                        expect(e).toBe('DOMError');
                    });
            });
        });
    }

    testShowSetAlarm () {
        describe('method:showSetAlarm', () => {
            beforeEach(() => {
                LF.Wrapper = jasmine.createSpyObj('Wrapper', ['exec', 'isAndroid']);
                spyOn(Helpers, 'getScheduleModels');
            });
        //DE15087 removed old tests,leaving test section here for possible later use.
        });
    }

    testShowMessageBoxScreenShots () {
        describe('method:showMessageBoxScreenShots', () => {
            beforeEach(() => {
                LF.Wrapper = jasmine.createSpyObj('Wrapper', ['exec', 'isAndroid']);
                LF.Wrapper.exec.and.callFake(options => options.execWhenWrapped());
            });

            it('should do nothing.', done => {
                LF.StudyDesign.enableMessageBoxScreenshots = false;
                spyOn($.fn, 'removeClass');

                this.view.showMessageBoxScreenShots({ }, () => {
                    expect($.fn.removeClass).not.toHaveBeenCalled();

                    done();
                });
            });

            it('should show the message box item.', done => {
                LF.StudyDesign.enableMessageBoxScreenshots = true;
                spyOn($.fn, 'removeClass');

                this.view.showMessageBoxScreenShots({ }, () => {
                    expect($.fn.removeClass).toHaveBeenCalledWith('hidden');

                    done();
                });
            });
        });
    }

    testShowChangePassword () {
        describe('method:showChangePassword', () => {
            it('should show the change password item.', () => {
                spyOn($.fn, 'removeClass');

                this.view.showChangePassword();

                expect($.fn.removeClass).toHaveBeenCalledWith('hidden');
            });
        });
    }

    testShowChangeSecurityQuestion () {
        describe('method:showChangeSecurityQuestion', () => {
            beforeEach(() => spyOn($.fn, 'removeClass'));

            it('show do nothing.', () => {
                LF.StudyDesign.askSecurityQuestion = false;

                this.view.showChangeSecurityQuestion();

                expect($.fn.removeClass).not.toHaveBeenCalled();
            });

            it('show the change security question item.', () => {
                LF.StudyDesign.askSecurityQuestion = true;

                this.view.showChangeSecurityQuestion();

                expect($.fn.removeClass).toHaveBeenCalledWith('hidden');
            });
        });
    }

    testShowSetTimeZone () {
        describe('method:showSetTimeZone', () => {
            it('should show the change password item.', () => {
                spyOn($.fn, 'removeClass');

                this.view.showSetTimeZone();

                expect($.fn.removeClass).toHaveBeenCalledWith('hidden');
            });
        });
    }

    testHideToolboxItem () {
        describe('method:hideToolboxItem', () => {
            it('should throw an error.', () => {
                expect(() => {
                    this.view.hideToolboxItem();
                }).toThrow(new Error('Missing Argument: params is null or undefined.'));
            });

            it('should hide the element.', () => {
                this.view.hideToolboxItem('#set-time-zone');

                let hidden = this.view.$('#set-time-zone').hasClass('hidden');

                expect(hidden).toBe(true);
            });
        });
    }

    testShowSettingsItems () {
        describe('method:showSettingsItems', () => {
            beforeEach(() => this.view.showSettingsItems.and.callThrough());
            it('should throw an error.', () => {
                LF.StudyDesign.toolboxConfig = [{
                    id: 'change-password',
                    toolboxRole: ['subject', 'caregiver', 'site']
                }];

                expect(() => {
                    this.view.showSettingsItems();
                }).toThrow(new Error('Invalid/Missing Argument: itemFunction for change-password is null, undefined or invalid'));
            });

            it('should show the item.', () => {
                LF.StudyDesign.toolboxConfig = [{
                    id: 'change-password',
                    toolboxRole: ['subject', 'caregiver', 'site'],
                    itemFunction: 'showChangePassword'
                }];
                spyOn(this.view, 'showChangePassword').and.stub();

                this.view.showSettingsItems();

                expect(this.view.showChangePassword).toHaveBeenCalled();
            });
        });
    }

    testAttemptLogout () {
        describe('method:attemptLogout', () => {
            it('should throw an error.', () => {
                expect(() => {
                    this.view.attemptLogout();
                }).toThrow(jasmine.any(Error));
            });

            it('should attempt to logout and do nothing.', () => {
                spyOn(ELF, 'trigger').and.resolve({ preventDefault: true });
                spyOn(LF.security, 'logout').and.stub();

                this.view.attemptLogout({ preventDefault: $.noop });
                expect(LF.security.logout).not.toHaveBeenCalled();
            });
        });
    }
}

describe('ToolboxView', () => {
    let suite = new ToolboxViewSuite();

    suite.executeAll({
        id: 'toolbox-page',
        template: '#toolbox-template',
        button: '#logout',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
