import AboutView from 'logpad/views/AboutView';
import Subjects from 'core/collections/Subjects';
import Subject from 'core/models/Subject';
import Transmissions from 'core/collections/Transmissions';
import Transmission from 'core/models/Transmission';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class AboutViewSuite extends PageViewSuite {
    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        resetStudyDesign();

        // jscs:disable requireArrowFunctions
        // Long hand functions used in these cases to retain the context of this to the spied upon objects.
        spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
            this.add([{ id: 1 }, { id: 2 }]);

            return Q();
        });

        spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
            this.add({
                device_id: hex_sha512('123456'),
                initials: 'bc',
                log_level: 'ERROR',
                id: 1,
                phase: 10,
                phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
                phaseTriggered: 'false',
                secret_answer: hex_sha512('apple'),
                secret_question: '0',
                site_code: '267',
                subject_id: '12345',
                subject_password: hex_sha512('apple' + 'krpt.39ed120a0981231'),
                service_password: hex_sha512('apple' + 'krpt.39ed120a0981231'),
                subject_number: '54321',
                subject_active: 1,
                krpt: 'krpt.39ed120a0981231',
                enrollmentDate: '2013-10-05T08:15:30-05:00',
                activationDate: '2013-10-05T08:15:30-05:00'
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        // Fetch the template used for the AboutView and render it to the DOM.
        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/about.ejs');
        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        this.view = new AboutView();

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    /**
     * Invoked after each unit test.
     * @returns {Q.Promise<void>}
     */
    afterEach () {
        this.removeTemplate();
        this.removeSpinnerTemplate();

        return super.afterEach();
    }

    /**
     * Test the view's resolve method.
     * @example suite.testResolve();
     */
    testResolve () {
        describe('method:resolve', () => {
            Async.it('should resolve.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.tap(() => {
                    expect(this.view.subject).toBeDefined();

                    expect(this.view.transmissionCount).toBeDefined();
                    expect(this.view.transmissionCount).toBe(2);
                });
            });

            Async.it('should fail to resolve.', () => {
                Subjects.prototype.fetch.and.callFake(() => Q.reject('Database error.'));

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.catch(e => {
                    expect(e).toBe('Database error.');
                });
            });
        });
    }

    /**
     * Test the view's render method.
     * @example suite.testRender();
     */
    testRender () {
        describe('method:render', () => {
            Async.it('should render.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should fail to render.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.catch(e => {
                    expect(e).toBe('DOMError');
                });
            });
        });
    }

    /**
     * Test the view's back method.
     * @example suite.testBack();
     */
    testBack () {
        describe('method:back', () => {
            it('should navigate back to the toolbox view', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('toolbox');
            });
        });
    }
}

describe('AboutView', () => {
    let suite = new AboutViewSuite();

    suite.executeAll({
        id: 'about-page',
        template: '#about-template',
        button: '#back',
        dynamicStrings: {
            studyVersionValue   : '2.0.0',
            activationCodeValue : '12345',
            studyURLValue       : 'http://localhost:3000',
            subjectIDValue      : '102',
            logLevelValue       : 'ERROR',
            siteCodeValue       : '001',
            protocolValue       : 10,
            clientNameValue     : 'LogPad App',
            studyReportCounts   : 2,
            coreProductVerValue : '2.0.1'
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testShowInputError',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });

    suite.testResolve();
    suite.testRender();
    suite.testBack();
});
