import DashboardView from 'logpad/views/DashboardView';
import Schedules from 'core/collections/Schedules';
import Questionnaires from 'core/collections/Questionnaires';
import Transmissions from 'core/collections/Transmissions';
import Subjects from 'core/collections/Subjects';
import Subject from 'core/models/Subject';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import CurrentSubject from 'core/classes/CurrentSubject';

import testData from './DashboardViewData';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class DashboardViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        LF.appName = 'LogPad App';
        LF.StudyDesign.schedules = new Schedules(testData);
        LF.StudyDesign.questionnaires = new Questionnaires(testData);

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/dashboard.ejs');
        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        lStorage.setItem('User_Login', 1);

        spyOn(Users.prototype, 'fetch').and.callFake(function () {
            this.add(testData.user);

            return Q();
        });

        spyOn(Users.prototype, 'updateSubjectUser').and.callFake(function () {
            return Q();
        });

        spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
            this.add(testData.transmissions);

            return Q();
        });

        spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
            this.add(testData.subject);

            return Q();
        });

        this.subject = new Subject(testData.subject);
        spyOn(CurrentSubject, 'getSubject').and.callFake(() => Q(this.subject));

        return specHelpers.initializeContent().then(() => {
            this.view = new DashboardView();

            return this.view.resolve()
            .then(() => this.view.render());
        })
        .then(() => {
            return super.beforeEach();
        });
    }

    afterEach () {
        this.removeTemplate();
        this.removeSpinnerTemplate();
        this.view.clearTemplateCache();

        return super.afterEach();
    }

    testResolve () {
        describe('method:resolve', () => {
            beforeEach(() => delete this.view.subject);

            Async.it('should fail to resolve.', () => {
                CurrentSubject.getSubject.and.reject('DatabaseError');

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method resolve to be rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should resolve.', () => {
                spyOn(ELF, 'trigger').and.stub();

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('DASHBOARD:Open/subject', {},  this.view);
                });
            });
        });
    }

    testRender () {
        describe('method:render', () =>  {
            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'getPendingReportCount').and.reject('DatabaseError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to be rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should render the view.', () => {
                spyOn(this.view, 'getPendingReportCount').and.resolve(2);
                spyOn(this.view, 'renderContent').and.stub();

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.renderContent).toHaveBeenCalled();
                });
            });
        });
    }

    testDisableAll () {
        describe('method:disableAll', () => {
            it('should disable all the buttons', () => {
                //this.view.buildSelectors();
                this.view.disableAll();

                expect(this.view.isDisabled(this.view.$sync)).toBe(true);
                expect(this.view.isDisabled(this.view.$toolbox)).toBe(true);
            });
        });
    }

   testEnableAll () {
        describe('method:enableAll', () => {
            beforeEach(() => this.view.disableAll());
            it('should enable all buttons.', () => {
                this.view.enableAll();

                expect(this.view.isDisabled(this.view.$sync)).toBe(false);
                expect(this.view.isDisabled(this.view.$toolbox)).toBe(false);
            });
        });
    }

    testTransmit () {
        describe('method:transmit', () => {
            Async.it('should do nothing.', () => {
                spyOn(this.view, 'isDisabled').and.returnValue(true);
                spyOn(this.view, 'disableAll').and.stub();

                return this.view.resolve()
                .then(() => {
                    this.view.transmit();

                    expect(this.view.disableAll).not.toHaveBeenCalled();
                });
            });

            Async.it('should trigger a transmit event.', () => {
                spyOn(this.view, 'isDisabled').and.returnValue(false);
                spyOn(ELF, 'trigger').and.resolve();
                spyOn(this.view.spinner, 'show').and.resolve();
                spyOn(this.view, 'enableAll');

                this.view.transmit();

                return Q().then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('DASHBOARD:Transmit/subject', { subject: this.subject }, this.view);
                });
            });
        });
    }

    testLoadSchedules () {
        describe('method:loadSchedules', () => {
            Async.it('should load the schedules.', () => {

                return this.view.resolve()
                .then(() => {
                    let request = this.view.loadSchedules();

                    expect(request).toBePromise();

                    return request.then(availableSchedules => {
                        expect(availableSchedules).toEqual(jasmine.any(Schedules));
                    });
                });
            });
        });
    }

    testRenderContent () {
        describe('method:renderContent', () => {
            // unimplemented
        });
    }

    testToolbox () {
        describe('method:toolbox', () => {
            it('should do nothing.', () => {
                spyOn(this.view, 'isDisabled').and.returnValue(true);
                spyOn(this.view, 'disableAll').and.stub();

                this.view.toolbox();

                expect(this.view.disableAll).not.toHaveBeenCalled();
            });

            it('should navigate to the toolbox view.', () => {
                spyOn(this.view, 'isDisabled').and.returnValue(false);
                spyOn(this.view, 'navigate').and.stub();

                this.view.toolbox({ preventDefault: $.noop });

                expect(this.view.navigate).toHaveBeenCalledWith('toolbox');
            });
        });
    }

    testRemoveFromDashboard () {
        describe('method:removeFromDashboard', () => {
            // unimplemented
        });
    }

    testResetModels () {
        describe('method:resetModels', () => {
            // unimplemented
        });
    }

    testFilterSchedules () {
        describe('method:filterSchedules', () => {
            it('should be defined.', () => {
                expect(this.view.filterSchedules).toBeDefined();
            });
        });
    }

    testRefresh () {
        describe('method:refresh', () => {
            it('should refresh the list views', () => {
                this.view.listViews.questionnairesList = { refresh: $.noop };

                spyOn(this.view.listViews.questionnairesList, 'refresh');
                this.view.refresh();

                expect(this.view.listViews.questionnairesList.refresh).toHaveBeenCalled();
            });
        });
    }

    testClear () {
        describe('method:clear', () => {
            it('should clear the dashboard items.', () => {
                let view = { close: $.noop };

                spyOn(view, 'close').and.stub();

                this.view.listViews.EQ5D = view;
                this.view.clear();

                expect(view.close).toHaveBeenCalled();
            });
        });
    }
}

describe('DashboardView', () => {
    let suite = new DashboardViewSuite(),
        dynamicStrings = { count: 10 };

    suite.executeAll({
        id: 'dashboard-page',
        template: '#dashboard-template',
        button: '#application-toolbox',
        dynamicStrings: { count: 10 },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // We are overriding this method.
            'testTransmit',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testShowInputError',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });

    // DashboardView tests.
    suite.testProperty('template', '#dashboard-template');
    suite.testResolve();
    suite.testRender();
    suite.testDisableAll();
    suite.testEnableAll();
    suite.testTransmit();
    suite.testLoadSchedules();
    suite.testRenderContent();
    suite.testToolbox();
    suite.testRemoveFromDashboard();
    suite.testResetModels();
    suite.testFilterSchedules();
    suite.testRefresh();
    suite.testClear();

});
