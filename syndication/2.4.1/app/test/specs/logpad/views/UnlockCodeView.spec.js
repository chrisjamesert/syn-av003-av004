import UnlockCodeView from 'logpad/views/UnlockCodeView';
import * as helpers from 'core/Helpers';
import User from 'core/models/User';
import Users from 'core/collections/Users';
import * as lStorage from 'core/lStorage';
import DailyUnlockCode from 'core/classes/DailyUnlockCode';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import { Banner } from 'core/Notify';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('UnlockCodeView', () => {
    let preventDefault = $.noop,
        view,
        location,
        removeTemplate;

    Async.beforeEach(() => {
        CurrentContext().set('unlockCodeConfig', {
            seedCode: 1111,
            codeLength: 6
        });

        LF.security = jasmine.createSpyObj('security', ['resetFailureCount']);

        // We want to maintain the context of the User collection, so we use a anon. function declaration.
        // jscs:disable
        spyOn(Users.prototype, 'fetch').and.callFake(function () {
            let user = new User({
                id:1,
                username: 'admin'
            });

            this.add(user);
            return Q();
        });
        // jscs:enable

        lStorage.setItem('User_Login', '1');

        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/unlock-code.ejs');

        view = new UnlockCodeView();
        spyOn(view, 'navigate').and.callFake(url => location = url);

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => {
        localStorage.clear();

        removeTemplate();
    });

    it('should have a default id of \'unlock-code-page\'', () => {
        expect(view.$el.attr('id')).toEqual('unlock-code-page');
    });

    describe('method:back', () => {
        Async.it('should navigate to the forgot password view.', () => {
            LF.StudyDesign.askSecurityQuestion = true;

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(true));

            let request = view.back();

            expect(request).toBePromise();

            return request.then((value) => {
                expect(value).toBeUndefined();
                expect(view.navigate).toHaveBeenCalledWith('forgot-password');
            });
        });

        Async.it('should navigate to the forgot password view (activation).', () => {
            LF.StudyDesign.askSecurityQuestion = true;

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(false));

            let request = view.back();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('forgot_password_activation');
            });
        });

        Async.it('should navigate to the reactivation view.', () => {
            LF.StudyDesign.askSecurityQuestion = false;

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(false));

            let request = view.back();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('reactivation');
            });
        });

        Async.it('should navigate to the login view.', () => {
            LF.StudyDesign.askSecurityQuestion = false;

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(true));

            let request = view.back();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('login');
            });
        });
    });

    describe('method:onInput', () => {
        it('should disable the submit button.', () => {
            view.$input.val('');

            spyOn(view, 'disableButton');
            view.onInput({ preventDefault });

            expect(view.disableButton).toHaveBeenCalledWith(view.$submit);
        });

        it('should enable the submit button.', () => {
            view.$input.val('12345');

            spyOn(view, 'enableButton');
            view.onInput({ preventDefault });

            expect(view.enableButton).toHaveBeenCalledWith(view.$submit);
        });
    });

    describe('method:resolve', () => {
        /*
        Async.it('should resolve the view.', () => {
            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.user).toBeDefined();
                expect(view.user.get('id')).toBe(1);
            });
        });
*/

        Async.it('should fail to resolve the view.', () => {
            Users.prototype.fetch.and.callFake(() =>  Q.reject('Error'));

            let request = view.resolve();

            expect(request).toBePromise();

            return request.catch(e => {
                expect(e).toBe('Error');
            }).then(() => {
                expect(request).toHaveBeenRejected();
            });
        });
    });

    describe('method:render', () => {
 /*
        Async.it('should render the view.', () => {
            LF.StudyDesign.askSecurityQuestion = false;

            let request = view.render();

            expect(request).toBePromise();
            expect(view.templateStrings.disabledSQText).toBe('PASSWORD_ATTEMPT_EXCEEDED');

            return request.then(() => {
                expect(view.$el.html()).toBeDefined();
            });
        });

        Async.it('should render the view (ask security question).', () => {
            LF.StudyDesign.askSecurityQuestion = true;

            let request = view.render();

            expect(request).toBePromise();
            expect(view.templateStrings.disabledSQText).toBe('ANSWER_ATTEMPT_EXCEEDED');

            return request.then(() => {
                expect(view.$el.html()).toBeDefined();
            });
        });
*/
        Async.it('should not render the view.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('Error'));

            let request = view.render();

            expect(request).toBePromise();

            return request
                .catch(e => {
                    expect(e).toBe('Error');
                })
                .then(() => {
                    expect(request).toHaveBeenRejected();
                });
        });

    });
/*
    describe('method:validate', () => {
        Async.it('should validate the unlock code (installed).', () => {
            let unlockCode = new DailyUnlockCode();

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(true));
            spyOn(Logger.prototype, 'operational').and.callFake(() => Q());

            view.$input.val(unlockCode.getCode(new Date()));

            let request = view.validate({ preventDefault });

            expect(request).toBePromise();

            return request
                .then(() => {
                    expect(Logger.prototype.operational).toHaveBeenCalledWith('Correct unlock code entered');
                    expect(LF.security.resetFailureCount).toHaveBeenCalledWith(view.user);
                    expect(view.navigate).toHaveBeenCalledWith('reset_password');
                });
        });

        Async.it('should validate the unlock code (not installed).', () => {
            let unlockCode = new DailyUnlockCode();

            LF.StudyDesign.askSecurityQuestion = true;

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(false));
            spyOn(Logger.prototype, 'operational').and.callFake(() => Q());

            view.$input.val(unlockCode.getCode(new Date()));

            let request = view.validate({ preventDefault });

            expect(request).toBePromise();

            return request.then(() => {
                expect(localStorage.getItem('Reset_Password_Secret_Question')).toBe('true');
                expect(Logger.prototype.operational).toHaveBeenCalledWith('Correct unlock code entered');
                expect(LF.security.resetFailureCount).toHaveBeenCalledWith(view.user);
                expect(view.navigate).toHaveBeenCalledWith('reset_password_activation');
            });
        });


        Async.it('should fail to validate the unlock code.', () => {
            LF.StudyDesign.askSecurityQuestion = true;

            spyOn(helpers, 'checkInstall').and.callFake(() => Q(true));
            spyOn(Logger.prototype, 'error').and.callFake(() => Q());
            spyOn(view, 'i18n').and.callFake(() => Q('Incorrect unlock code entered'));
            spyOn(Banner, 'show').and.callFake($.noop);

            view.$input.val('wrongUnlockCode');

            let request = view.validate({ preventDefault });

            expect(request).toBePromise();

            return request.then(() => {
                expect(Logger.prototype.error.calls.count()).toBe(2);
                expect(Logger.prototype.error.calls.argsFor(0)).toEqual(['INCORRECT_UNLOCK_CODE']);
                expect(Logger.prototype.error.calls.argsFor(1)).toEqual(['Incorrect unlock code entered']);
                expect(Banner.show).toHaveBeenCalledWith({
                    text: 'Incorrect unlock code entered',
                    type: 'error'
                });
            });
        });
    });
    */
});
