import SetAlarmsView from 'logpad/views/SetAlarmsView';
import * as helpers from 'core/Helpers';
import Schedules from 'core/collections/Schedules';
import SubjectAlarms from 'core/collections/SubjectAlarms';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import PageViewSuite from 'test/specs/core/views/PageView.specBase';

const sched = new Schedules([{
    id: 'LogPadSchedule',
    target: {
        objectType: 'questionnaire',
        id: 'LogPad'
    },
    scheduleFunction: 'checkRepeatByDateAvailability',
    phase: [
        'SCREENING'
    ],
    scheduleParams: {
        startAvailability: '01:00',
        endAvailability: '19:00'
    },
    alarmFunction: 'addAlwaysAlarm',
    alarmParams: {
        id: 1,
        time: '9:00',
        repeat: 'daily',
        subjectConfig: {
            minAlarmTime: '9:00',
            maxAlarmTime: '15:00',
            alarmRangeInterval: 30,
            alarmOffSubject: true
        }
    }
}, {
    id: 'LogPad1Schedule_01',
    target: {
        objectType: 'questionnaire',
        id: 'LogPad1'
    },
    scheduleFunction: 'checkRepeatByDateAvailability',
    phase: [
        'SCREENING'
    ],
    scheduleParams: {
        startAvailability: '00:01',
        endAvailability: '23:58'
    }
}]);

class SetAlarmsViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        this.view = new SetAlarmsView();

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/alarms.ejs');

        spyOn(helpers, 'getScheduleModels').and.returnValue(sched.models);
        spyOn(SubjectAlarms.prototype, 'fetch').and.resolve();
        spyOn(SetAlarmsView.prototype, 'render').and.resolve('<div>Alarm</div>');

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SetAlarmsViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should fail to resolve.', () => {
                SubjectAlarms.prototype.fetch.and.reject('DBError');

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Method resolve should have been rejected.'))
                .catch(e => expect(e).toBe('DBError'));
            });

            Async.it('should resolve.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.schedulesWithSubjectAlarms).toBeDefined();
                    expect(this.view.schedulesWithSubjectAlarms.length).toBe(1);
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render.', () => {
                this.view.render.and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            it('should navigate to the ToolboxView.', () => {
                spyOn(this.view,  'navigate').and.stub();

                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('toolbox');
            });
        });
    }

    testUpdateAlarm () {
        describe('method:updateAlarm', () => {
            it('should set the subject alarms.', () => {
                let params = { id: 1, time: '13:00' };

                spyOn(this.view.subjectAlarms, 'setSubjectAlarm').and.stub();

                this.view.updateAlarm(null, params);

                expect(this.view.subjectAlarms.setSubjectAlarm).toHaveBeenCalledWith(params);
            });
        });
    }
}

describe('SetAlarmView', () => {
    let suite = new SetAlarmsViewSuite();

    suite.executeAll({
        id: 'set-alarms-page',
        template: '#alarms-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
