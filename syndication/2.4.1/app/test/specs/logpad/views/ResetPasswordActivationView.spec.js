// The test is disabled so let's disable the eslint rules as well
/* eslint-disable */

xdescribe('ResetPasswordActivationView', function () {

    let view,
        location,
        security = null,
        askSecurityQuestion = LF.StudyDesign.askSecurityQuestion,
        template = '<div id="reset-password-template">' +
                '<form data-ajax="false" autocomplete="off">' +
                    '<div class="input-textbox"><input type="text" name="txtNewPassword_{{ key }}" id="txtNewPassword" /></div>' +
                    '<div class="input-textbox"><input type="text" name="txtConfirmPassword_{{ key }}" id="txtConfirmPassword" /></div>' +
                    '<div class="input-textbox"><button type="submit" id="submit" /></div>' +
                '</form>' +
            '</div>';

    beforeEach(function () {
        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(true);
        });

        LF.router = new LF.Router.ActivationRouter();

        spyOn(LF.router, 'navigate').andCallFake(function (url) {
            location = url;
        });

        security = new LF.Class.ActivationSecurity();

        LF.webService = new LF.Class.WebService();

        $('body').append(template);
    });

    afterEach(function () {
        $('#reset-password-template').remove();
        LF.Data = { };
        LF.router = undefined;
        LF.security = undefined;
        location = undefined;
        LF.webService = undefined;
        LF.StudyDesign.askSecurityQuestion = askSecurityQuestion;
        localStorage.clear();
    });

    it('should render \'reset-password-activation-page\'', function () {
        view = new LF.View.ResetPasswordActivationView();

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.$el.attr('id')).toEqual('reset-password-activation-page');
        });
    });

    it('should submit the form and reactivate if security question is turned off', function () {
        let response;

        LF.Data.code = '12345678';
        LF.StudyDesign.askSecurityQuestion = false;
        view.$('#txtNewPassword').val(1234);
        view.$('#txtConfirmPassword').val(1234);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices',
            type        : 'POST',
            success     : {
                data    : {
                    D : '123-abc',
                    W : 'abc123def'
                },
                status  : 'success',
                xhr     : {
                    getResponseHeader: $.noop,
                    status : 200
                }
            }
        }]});

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${LF.Data.code}`,
            type        : 'GET',
            success     : {
                data    : {
                    P : 10,
                    L : 'ERROR',
                    K : '1234'
                },
                status  : 'success',
                xhr     : {
                    getResponseHeader: (id) => {
                        switch (id) {
                            case 'X-Device-Status':
                                return '0';
                            case 'X-Sync-ID':
                                return '12';
                            default:
                                return '';
                        }
                    },
                    status : 200
                }
            }
        }]});

        view.submit({
            preventDefault: $.noop
        });

        setTimeout(function () {
            response = true;
        }, 50);

        waitsFor(function () {
            return response;
        }, 'Transmission to complete', 50);

        runs(function () {
            expect(LF.Data.service_password).toEqual('abc123def');
            expect(LF.Data.deviceID).toEqual('123-abc');
            expect(localStorage.getItem('krpt')).toEqual('1234');
            expect(localStorage.getItem('PHT_syncID')).toEqual('12');
            expect(localStorage.getItem('PHT_TEMP')).not.toBeNull();
        });

    });

    it('should submit the form and navigate to the reset security question page if security question is turned on', function () {
        LF.Data.code = '1323456';
        LF.StudyDesign.askSecurityQuestion = true;
        view.$('#txtNewPassword').val(1234);
        view.$('#txtConfirmPassword').val(1234);
        view.submitted = false;

        view.submit({
            preventDefault: $.noop
        });

        expect(location).toEqual('reset_secret_question');
    });

});
