import Questionnaire from 'core/models/Questionnaire';
import Templates from 'core/collections/Templates';
import UpcomingQuestionnaire from 'logpad/views/UpcomingQuestionnaire';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('UpcomingQuestionnaire', () => {
    let questionnaire,
        location,
        rule,
        view;

    Async.beforeEach(() => {
        resetStudyDesign();

        location = 'dashboard';

        questionnaire = new Questionnaire({
            id: 'LogPad',
            SU: 'LogPad',
            displayName: 'DISPLAY_NAME',
            className: 'medication',
            previousScreen: false,
            screens: ['LogPad_S_1', 'LogPad_S_2']
        });

        spyOn(LF, 'getStrings').and.callFake((strings, callback, options) => {
            if (strings === 'DISPLAY_NAME' && options.namespace === 'LogPad') {
                return Q('LogPad App');
            }

            return Q('');
        });

        // Add the template required to render the view.
        LF.templates = new Templates([{
            name: 'Questionnaire',
            namespace: 'DEFAULT',
            template: '<a class="navigate-right"><strong>{{ title }}</strong></a>'
        }]);

        // Set up a mock router to capture navigation.
        LF.router = jasmine.createSpy();
        LF.router.navigate = jasmine.createSpy().and.callFake(url => location = url);

        specHelpers.createSubject();
        return specHelpers.saveSubject();
    });

    afterEach(() => {
        location = undefined;
        LF.router = undefined;
    });

    Async.it('should render the view', () => {
        let view = new UpcomingQuestionnaire({
            model: questionnaire,
            scheduleId: 'Daily_LogPad'
        });

        return view.render()
        .then(() => {
            expect(view.$('strong').html()).toEqual('LogPad App');
        });
    });

    describe('method:open', () => {
        xit('should trigger the rule on open.', function () {

            let response;

            rule = new LF.Model.Rule(
                {
                    id: 'openQuestionnaire',
                    trigger: 'DASHBOARD:OpenQuestionnaire/LogPad',
                    expression: true,
                    actionData: [
                        {
                            trueAction: function () {
                                response = 'LogPad';
                            }
                        }
                    ]
                }
            );

            LF.StudyRules.add(rule);

            view.open({
                preventDefault: $.noop
            });

            waitsFor(function () {
                return response;
            }, 'the rule to trigger', 1000);

            runs(function () {
                LF.StudyRules.remove(rule);
                expect(response).toEqual('LogPad');
            });

        });

        xit('should open the questionnaire.', function () {

            view.open({
                preventDefault: $.noop
            });

            waitsFor(function () {
                return location !== 'dashboard';
            }, 'the questionnaire to open', 1000);

            runs(function () {
                expect(location).toEqual('questionnaire/LogPad/Daily_LogPad');
            });

        });

    });
});
