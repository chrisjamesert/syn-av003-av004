import LogPadEULAView from 'logpad/views/LogPadEULAView';
import EULAView from 'core/views/EULAView';
import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import EULAViewTests from 'test/specs/core/views/EULAView.specBase';

class LogPadEULAViewSuite extends EULAViewTests {
    beforeAll () {
        return specHelpers.initializeContent()
            .then(() => super.beforeAll());
    }

    beforeEach () {
        this.view = new LogPadEULAView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/end-user-license.ejs');

        return super.beforeEach();
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(LogPadEULAViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testNextChild () {
        describe('method:next (child)', () => {
            beforeEach(() => {
                spyOn(EULAView.prototype, 'next').and.stub();
                spyOn(this.view, 'navigate').and.stub();
            });

            it('should navigate to the privacy policy view view.', () => {
                this.view.patientEULA = 'accept';
                this.view.next();

                expect(EULAView.prototype.next).toHaveBeenCalled();
                expect(this.view.navigate).toHaveBeenCalledWith('privacy_policy_activation');
            });

            it('should navigate to the privacy policy view view.', () => {
                this.view.patientEULA = 'decline';
                this.view.next();

                expect(EULAView.prototype.next).toHaveBeenCalled();
                expect(this.view.navigate).toHaveBeenCalledWith('handoff', true, {
                    patientEULA: 'decline',
                    siteEULA: 'accept'
                });
            });
        });
    }

    testBackHandler () {
        describe('method:backHandler', () => {
            it('should invoke back.', () => {
                spyOn(EULAView.prototype, 'back').and.stub();
                spyOn(this.view, 'back').and.resolve();

                this.view.backHandler();

                expect(EULAView.prototype.back).toHaveBeenCalled();
                expect(this.view.back).toHaveBeenCalled();
            });
        });
    }

    testBackChild () {
        describe('method:back (child)', () => {
            Async.it('should trigger a backout event.', () => {
                spyOn(ELF, 'trigger').and.resolve();
                this.view.patientEULA = 'decline';

                let request = this.view.back();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('SiteEULA:Backout', {}, this.view);
                });
            });

            Async.it('should direct the EULA to the site.', () => {
                spyOn(MessageRepo, 'display').and.resolve();
                spyOn(this.view, 'render').and.resolve();

                let request = this.view.back();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(MessageRepo.display).toHaveBeenCalled();
                    expect(this.view.render).toHaveBeenCalled();
                });
            });
        });
    }
}

describe('LogPadEULAView', () => {
    let suite = new LogPadEULAViewSuite();

    suite.executeAll({
        id: 'end-user-license-view',
        template: '#end-user-license-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter',

            // Disabled to test manually.
            'testBack'
        ]
    });
});