xdescribe('AlarmTimeView', function () {

    let scheduleModel,
        questionnaires = LF.StudyDesign.questionnaires,
        view;

    beforeEach(function () {

        scheduleModel = new LF.Model.Schedule({
            id: 'LogPadSchedule',
            target: {
                objectType: 'questionnaire',
                id: 'LogPad'
            },
            scheduleFunction: 'checkRepeatByDateAvailability',
            phase: [
                'SCREENING'
            ],
            scheduleParams: {
                startAvailability: '01:00',
                endAvailability: '19:00'
            },
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id:   1,
                time:   '9:00',
                repeat:   'daily',
                subjectConfig: {
                    minAlarmTime: '9:00',
                    maxAlarmTime: '15:00',
                    alarmRangeInterval: 30,
                    alarmOffSubject: true
                }
            }
        });

        LF.strings.add([
            {
                language: 'en',
                locale: 'US',
                namespace: 'LogPad',
                dir: 'ltr',
                resources: {
                    DISPLAY_NAME: 'LogPad App'
                }
            }
        ]);

        LF.StudyDesign.questionnaires = new LF.Collection.Questionnaires([
            {
                id              : 'LogPad',
                SU              : 'LogPad',
                displayName     : 'DISPLAY_NAME',
                className       : 'medication',
                previousScreen  : false,
                screens         : []
            }
        ]);

    });

    afterEach(function () {

        LF.StudyDesign.questionnaires = questionnaires;

        LF.strings.remove(LF.strings.where({
            namespace: 'LogPad'
        })[0]);

    });

    it('should render the view with an id of alarm_id', function () {

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.$el.attr('id')).toEqual(scheduleModel.get('alarmParams').id.toString());
        });

    });

    it('should configure datebox from schedule model', function () {

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.config.defaultValue).toEqual('9:00:00');

            expect(view.config.useClearButton).toEqual(true);

            expect(view.config.overrideClearButton).toEqual('Turn Off');

            expect(view.config.minuteStep).toEqual(30);

        });

    });

    it('should configure correct datebox property ', function () {

        scheduleModel.get('alarmParams').subjectConfig.alarmOffSubject = false;

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.config.useClearButton).toBeUndefined();
        });
    });

    it('should configure view properties from schedule model', function () {

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.alarmTime).toEqual('09:00');

            expect(view.alarmOffLbl).toEqual('Off');

        });
    });

    it('should display correct diary name and alarm time', function () {

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.$('a span:first').text()).toEqual('LogPad App');

            expect(view.$('.direction-right').text()).toEqual('09:00');

            expect(view.$('input[data-role="datebox"]').val()).toEqual('09:00');

        });
    });

    it('should display OFF label if alarm is turned off', function () {

        scheduleModel.get('alarmParams').time = '';

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.$('.direction-right').text()).toEqual('Off');
        });
    });

    it('should update alarm time property and label on respond', function () {

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {

            view.$('input[data-role="datebox"]').val('14:30');

            view.respond({}, {
                date : new Date(2013, 2, 2, 14, 30)
            });

            expect(view.alarmTime).toEqual('14:30');

            expect(view.$('.direction-right').text()).toEqual('14:30');
        });
    });

    it('should update time property and label on respond when alarm is turned off', function () {

        view = new LF.View.AlarmTimeView({
            model : scheduleModel,
            id    : scheduleModel.get('alarmParams').id
        });

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            view.$('input[data-role="datebox"]').val('');

            view.respond({}, {});

            expect(view.alarmTime).toEqual('');

            expect(view.$('.direction-right').text()).toEqual('Off');
        });
    });

});
