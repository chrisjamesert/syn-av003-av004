// The test is disabled so let's disable the eslint rules as well
/* eslint-disable */

xdescribe('ResetSecretQuestionActivationView', function () {

    let view,
        location,
        security = null,
        template = '<div id="reset-secret-question-template">' +
                        '<form data-ajax="false" autocomplete="off" autocorrect="off">' +
                            '<div class="input-textbox">' +
                                '<select name="secret_question_{{ key }}" id="secret_question"></select>' +
                            '</div>' +
                            '<div class="input-textbox"><input type="text" name="secret_answer_{{ key }}" id="secret_answer" value="" /></div>' +
                            '<div class="input-textbox"><button type="submit" id="submit" /></div>' +
                        '</form>' +
                    '</div>';

    beforeEach(function () {
        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(true);
        });

        LF.router = new LF.Router.ActivationRouter();

        spyOn(LF.router, 'navigate').andCallFake(function (url) {
            location = url;
        });

        security = new LF.Class.ActivationSecurity();

        LF.webService = new LF.Class.WebService();

        $('body').append(template);
    });

    afterEach(function () {
        $('#reset-secret-question-template').remove();
        LF.Data = { };
        LF.router = undefined;
        LF.security = undefined;
        location = undefined;
        LF.webService = undefined;
        localStorage.clear();
    });

    it('should render \'reset-secret-question-activation-page\'', function () {
        view = new LF.View.ResetSecretQuestionActivationView();

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.$el.attr('id')).toEqual('reset-secret-question-activation-page');
        });
    });

    it('shouldn\'t enable OK button with no value in secret_answer.', function () {
        view.$('#secret_answer').val('');
        view.onInput();

        expect(view.$('#submit')).toHaveAttr('disabled');
    });

    it('shouldn\'t enable OK button with an invalid value in secret_answer.', function () {
        view.$('#secret_answer').val('1');
        view.onInput();

        expect(view.$('#submit')).toHaveAttr('disabled');
        expect(view.$('#secret_answer').parent().hasClass('invalid-icon')).toBeTruthy();
    });

    it('should enable OK button with a valid value in secret_answer.', function () {
        view.$('#secret_answer').val('1988');
        view.onInput();

        expect(view.$('#submit')).not.toHaveAttr('disabled');
        expect(view.$('#secret_answer').parent().hasClass('valid-icon')).toBeTruthy();
    });

    it('should clear answer input field', function () {
        let secretAnswer = '1234';
        $('#secret_answer').val(secretAnswer);
        view.clearSecretAnswer();

        expect($('#secret_answer').val()).toEqual('');
        expect(view.$('#secret_answer').parent().hasClass('valid-icon')).toBeFalsy();
    });

    it('should reactivate', function () {
        let response;

        LF.Data.code = '12345678';
        LF.Data.password = 'abc123def';
        view.$('#secret_question').val(0);
        view.$('#secret_answer').val(1988);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices',
            type        : 'POST',
            success     : {
                data    : {
                    D : '123-abc',
                    W : 'abc123def',
                    Q : 0,
                    A : '1988'
                },
                status  : 'success',
                xhr     : {
                    getResponseHeader: $.noop,
                    status : 200
                }
            }
        }]});

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${LF.Data.code}`,
            type        : 'GET',
            success     : {
                data    : {
                    P : 10,
                    L : 'ERROR',
                    K : '1234'
                },
                status  : 'success',
                xhr     : {
                    getResponseHeader: (id) => {
                        switch (id) {
                            case 'X-Device-Status':
                                return '0';
                            case 'X-Sync-ID':
                                return '12';
                            default:
                                return '';
                        }
                    },
                    status : 200
                }
            }
        }]});

        view.reactivate({
            preventDefault: $.noop
        });

        setTimeout(function () {
            response = true;
        }, 50);

        waitsFor(function () {
            return response;
        }, 'Transmission to complete', 50);

        runs(function () {
            expect(LF.Data.service_password).toEqual('abc123def');
            expect(LF.Data.deviceID).toEqual('123-abc');
            expect(localStorage.getItem('krpt')).toEqual('1234');
            expect(localStorage.getItem('PHT_syncID')).toEqual('12');
            expect(localStorage.getItem('PHT_TEMP')).not.toBeNull();
        });
    });
});
