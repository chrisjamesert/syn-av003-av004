xdescribe('Questionnaires', function () {

    let collection;

    beforeEach(function () {

        collection = new LF.Collection.Questionnaires([
            {
                id              : 'EQ5D',
                displayName     : 'DISPLAY_NAME',
                className       : 'EQ5D',
                previousScreen  : true,
                screens         : ['EQ5D_S_1', 'EQ5D_S_2', 'EQ5D_S_3']
            }, {
                id              : 'EQ5D_2',
                displayName     : 'DISPLAY_NAME',
                className       : 'EQ5D',
                previousScreen  : true,
                screens         : ['EQ5D_S_1', 'EQ5D_S_2', 'EQ5D_S_3']
            }
        ]);

    });

    afterEach(function () {

        LF.logs.reset();
        localStorage.clear();

    });

    it('should be defined', function () {

        expect(collection.length).toEqual(2);

    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should NOT have a table', function () {

        expect(collection.table).not.toBeDefined();

    });

    it('should return 1 model', function () {

        let models = collection.match({
                id  : 'EQ5D'
            });

        expect(models.length).toEqual(1);

    });

    it('should return 2 models', function () {

        let models = collection.match({
                className   : 'EQ5D'
            });

        expect(models.length).toEqual(2);

    });

    it('should return 0 models', function () {

        let models = collection.match({
                id  : 'SF36'
            });

        expect(models.length).toBeFalsy();

    });

    it('should return a missing arguments error (match)', function () {

        expect(function () {
            let models = collection.match();
        }).toThrow({
            code    : 0,
            message : 'Invalid number of arguments.'
        });

    });

});
