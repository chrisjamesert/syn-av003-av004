import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Session from 'core/classes/Session';
import StudyDesign from 'core/classes/StudyDesign';
import Templates from 'core/collections/Templates';
import User from 'core/models/User';
import Spinner from 'core/Spinner';
import ELF from 'core/ELF';

import * as specHelpers from 'test/helpers/SpecHelpers';

import testData from './Looping.testData';
import {templates} from './Looping.testData';
import * as lStorage from 'core/lStorage';
import loopingTestFunctions from './LoopingTestFunctions';

export default class BaseLoopingTests {

    constructor () {
        this.testQuestionView = null;

        this.template = `<div id="questionnaire-template">
            <div id="questionnaire"></div>
            </div>`;

        this.Questionnaire = BaseQuestionnaireView;

        this.options = {
            id: 'Meds',
            schedule_id: 'looping_schedule',
            SU: 'Meds',
            displayName: 'DISPLAY_NAME',
            previousScreen: true
        };

        let that = this;

        /**
         * gives the last question added to the questionViews collection
         * @returns {*}
         */
        this.getLastAddedQuestionView = () => {
            if (this.testQuestionView.questionViews.length === 0) {
                return null;
            }

            return this.testQuestionView.questionViews[this.testQuestionView.questionViews.length - 1];
        };

        /**
         * Adds answers for the given number of loops to iterate
         * @param numberOfLoops - the number of loops to iterate and create answers for.  If this is 3, for example,
         * the function will call navigate the entire loop adding answers for each widget that's displayed.
         * @returns {*} - a resolved promise chain
         */
        this.addAllAnswersForACertainNumberOfLoops = (numberOfLoops) => {
            let resolved_promise_chain = Q();

            for (let i = 1; i <= numberOfLoops; i++) {
                resolved_promise_chain = resolved_promise_chain.then(() => {
                    return LF.Branching.branchFunctions.addNewMedsLoop(that.testQuestionView);
                })
                .then(() => {
                    return this.getLastAddedQuestionView().widget.addAnswer({response: i});
                })
                .then(() => {
                    return that.testQuestionView.navigationHandler('next');
                })
                .then(() => {
                    return this.getLastAddedQuestionView().widget.addAnswer({response: `12:0${i}`});
                })
                .then(() => {
                    return that.testQuestionView.navigationHandler('next');
                });
            }

            return resolved_promise_chain;
        };
    }

    /**
     * Any tests you want executed should be added here
     */
    execTests () {
        describe('Base Looping tests', () => {
            Async.beforeEach(() => this.beforeEach());
            Async.afterEach(() => this.afterEach());
            Async.beforeAll(() => this.beforeAll());
            Async.it('should have good test data', () => this.testDataIsStillGood());
            Async.it('should skip screens and follow branch when nothing entered',
                () => this.advancesToCorrectScreenFromBranch1());
            Async.it('should go to add screen when addNewMeds branch is triggered',
                () => this.advancesToCorrectScreenFromBranch2());
            Async.it('should go to conditional branch when certain answer is selected',
                () => this.advancesToCorrectScreenBasedOnAnswerSelection());
            Async.it('should get the latest IGR from the master igList', () => this.getIGRFromMasterIGList());
            Async.it('should should increment the IGR after view.incrementIGR is called for a specific IG',
                () => this.incrementsIGRForIG());
            Async.it('should return all answers for specific IG after one loop',
                () => this.queryAnswersForSpecificIGOneLoop());
            Async.it('should return all answers for specific IG after two loops',
                () => this.queryAnswersForSpecificIGMultipleLoops());
            Async.it('should remove all screen ID\'s from the view\'s Screen Stack for a given IG',
                () => this.clearLoopStackShouldRemoveAllScreensForIG());
            Async.it('should increment IGR when called', () => this.incrementIGRIncreasesItForAGivenIG());
            Async.it('should decrement IGR when called', () => this.decrementIGRDecreasesItForAGivenIG());
            Async.it('should return answers for given question ID and IGR',
                () => this.returnsAnswersForQuestionIDAndIGR());
            Async.it('should return all answers for a given IGR', () => this.returnsAnswersForSpecificIGR());
            Async.it('should return all answers by IT and IGR', () => this.returnAnswersByITandIGR());
            Async.it('should return all answers by IG IT and IGR', () => this.returnAnswersByIGITandIGR());

            // TODO: TA37872 Fix me!
            Async.xit('should return QuestionViews by IGR', () => this.returnQuestionViewsByIGR());
            Async.xit('should return QuestionViews by IG and IGR', () => this.returnQuestionViewsByIGAndIGR());

            Async.it('should remove the answer that matches the IG, IT and IGR',
                () => this.removesAnswerWithMatchingIGITAndIGR());
            Async.it('should copy answers from one IGR to another and make sure they are the same',
                () => this.copiesAnswersFromOneIGRtoAnotherIGRForAGivenIG());
            Async.it('should remove LoopingEvent', () => this.removeLoopEventRemovesLoopForIG());
            Async.it('should make an IGR available', () => this.makeIGRAvailableInsertsEmptyIGR());
        });
    }

    /**
     * anything you want executed before each test runs
     * @returns {Q.Promise<null>} resolved when completed
     */
    beforeEach () {
        LF.StudyDesign = new StudyDesign(testData);

        LF.templates = new Templates(templates);

        LF.security = new Session();
        LF.security.activeUser = new User({id: 1});
        LF.spinner = Spinner;

        // spoof the router navigate();
        LF.router = {
            flash: () => LF.router,
            navigate: (url) => {
                this.location = url;
            }
        };

        this.testQuestionView = new this.Questionnaire(this.options);

        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        return this.testQuestionView.resolve()
            .then(() => this.testQuestionView.render());
    }

    /**
     * anything you want executed after each test runs
     * @returns {Q.Promise<null>} resolved when completed
     */
    afterEach () {
        this.removeSpinnerTemplate();
        return Q();
    }

    /**
     * any setup stuff you want can be done here
     * @returns {Q.Promise<null>} resolved when completed
     */
    beforeAll () {

        // SM: HACK: some other test file apparently starts history, but if run standalone
        // need to start it ourselves.
        // MEM: I don't know what this does, but it looked important in BaseQuestionnaireView.specBase
        try {
            Backbone.history.start({ pushState: false });
        } catch (e) {
            // Guess it was already started.
        }

        this.location = 'questionnaire';
        LF.SpecHelpers.installDatabase();
        $('body').append(this.template);
        $.noty = $.noty ? $.noty : { closeAll: $.noop };
        return Q();
    }

    /**
     * Just testing that if someone has changed the test data, they have updated the tests.  This will help isolate
     * that and save time if the test data was changed.  If you have changed Looping.testData.js you must update this
     * test as the test relies on that data.
     * @returns {*}
     */
    testDataIsStillGood () {
        let curIG;

        // expecting 6 screens
        expect(this.testQuestionView.data.screens[0].id).toEqual('LOOP1_DidYouTakeMeds_Screen');
        expect(this.testQuestionView.data.screens[1].id).toEqual('LOOP1_Review_Screen');
        expect(this.testQuestionView.data.screens[2].id).toEqual('LOOP1_MedSelection_Screen');
        expect(this.testQuestionView.data.screens[3].id).toEqual('LOOP1_TimePicker_Screen');
        expect(this.testQuestionView.data.screens[4].id).toEqual('LOOP1_SelectedMotrin_TimePicker_Screen');
        expect(this.testQuestionView.data.screens[5].id).toEqual('LOOP1_FinalReview_Screen');
        expect(this.testQuestionView.data.screens[6].id).toEqual('AFFIDAVIT');

        // IGR for DidYouTakeMeds_Loop_IG should be 0
        curIG = this.testQuestionView.getCurrentIGR('DidYouTakeMeds_Loop_IG');
        expect(curIG).toEqual(0);

        // IGR for MedicationEntry_Loop_IG should be 1 <-- this is the main loop
        curIG = this.testQuestionView.getCurrentIGR('MedicationEntry_Loop_IG');
        expect(curIG).toEqual(1);

        return Q();
    }

    /**
     * Ensures that we go directly to the final review screen from screen 2.  See Looping.testData.js for test data
     * @returns {Q.Promise}
     */
    advancesToCorrectScreenFromBranch1 () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                // got to the second screen...according to our test data, clicking next on this screen
                // should move us directly to screen 5
                expect(this.testQuestionView.currentScreenId).toEqual('LOOP1_Review_Screen');
                return this.testQuestionView.navigationHandler('next');
            })
            .tap(() => {
                // make sure we are now on screen 5 as the test data has a branch setup to go from LOOP1_Review_Screen
                // to LOOP1_FinalReview_Screen
                expect(this.testQuestionView.currentScreenId).toEqual('LOOP1_FinalReview_Screen');
            });
    }

    /**
     * Another test of branching to make sure we go to the addMeds screen
     * @returns {*}
     */
    advancesToCorrectScreenFromBranch2 () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return LF.Branching.branchFunctions.addNewMedsLoop(this.testQuestionView);
            })
            .tap(() => {
                // the addNewMedsLoop should have taken us to the 3rd screen
                expect(this.testQuestionView.currentScreenId).toEqual('LOOP1_MedSelection_Screen');
            });
    }

    /**
     * Tests the conditional branching for when a specific answer is selected
     * @returns {Q.Promise}
     */
    advancesToCorrectScreenBasedOnAnswerSelection () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return LF.Branching.branchFunctions.addNewMedsLoop(this.testQuestionView);
            })
            .then(() => {
                this.getLastAddedQuestionView().widget.addAnswer({response: 4});
                return this.testQuestionView.navigationHandler('next');
            })
            .tap(() => {
                // in the test data from Looping.testData.js there is a conditional branch to go to a different screen
                // if MOTRIN (answer 4) is selected.  Make sure it goes to that screen.
                expect(this.testQuestionView.currentScreenId).toEqual('LOOP1_SelectedMotrin_TimePicker_Screen');
            });
    }

    /**
     * Tests the incrementIGR fucntion to make sure the IGR increases when an IG is passed in
     * when moving through the loop
     * @returns {Q.Promise}
     */
    incrementsIGRForIG () {
        let ig;

        // advance to screen 2 which is the beginning of 'MedicationEntry_Loop_IG'
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                // increment the IGR for this loop
                this.testQuestionView.incrementIGR(this.testQuestionView.getQuestions()[0].ig);
            })
            .tap(() => {
                // the IGR started out at 1.  After incrementing, it should now be 2
                expect(this.testQuestionView.getCurrentIGR(this.testQuestionView.getQuestions()[0].ig)).toEqual(2);
            });
    }

    /**
     * Checks the answers for the IG after completing one loop
     * @returns {Promise.<T>}
     */
    queryAnswersForSpecificIGOneLoop () {
        let widget;

        return this.addAllAnswersForACertainNumberOfLoops(1)
        .tap(() => {
            // get the answers for this IG.  Right now we should have two answers as we completed one loop
            let answersByIG = this.testQuestionView.queryAnswersByIG(this.testQuestionView.getQuestions()[0].ig);
            expect(answersByIG.length).toEqual(2);
            expect(answersByIG[0].get('response')).toEqual(1);
            expect(answersByIG[1].get('response')).toEqual('12:01');
        });
    }

    /**
     * Checks the answers for the IG after completing multiple loops
     * @returns {Q.Promise}
     */
    queryAnswersForSpecificIGMultipleLoops () {
        let widget,
            answersByIG;

        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(4);
            })
            .tap(() => {
                // get the answers for this IG (Loop1B)...there should now be 8 of them.
                // 4 loops at 2 answers per loop for this IG
                answersByIG = this.testQuestionView.queryAnswersByIG('MedicationEntry_Loop_IG');
                expect(answersByIG.length).toEqual(8);
            });
    }

    /**
     * Check that it clears the screen stack for the IG
     * @returns {Q.Promise}
     */
    clearLoopStackShouldRemoveAllScreensForIG () {
        let widget,
            answersByIG;

        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(1);
            })
            .tap(() => {
                this.testQuestionView.clearLoopScreenFromStack(this.testQuestionView.getQuestions()[0].ig);

                // should be no screens for Loop1B...we are back at the review screen but haven't pushed it to the stack yet.
                expect(this.testQuestionView.screenStack.length).toEqual(1);

                // should only be the original screen from Loop1A still on the stack
                expect(this.testQuestionView.screenStack[0] === 'LOOP1_DidYouTakeMeds_Screen');

                // attempt to clear the screens from the stack for IG - Loop1A
                this.testQuestionView.clearLoopScreenFromStack('DidYouTakeMeds_Loop_IG');

                // should be no screens for on the stack for IG - Loop1A now either
                expect(this.testQuestionView.screenStack.length).toEqual(0);
            });
    }

    /**
     * Gets the IGR from the master igList for the matching IG and returns a number. It looks for the IGR in
     * @returns {Q.Promise}
     */
    getIGRFromMasterIGList () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(2);
            })
            .tap(() => {
                expect(this.testQuestionView.getIGR('MedicationEntry_Loop_IG')).toEqual(3);
            });
    }

    /**
     * Test explicitly calling incrementIGR increases the IGR for the IG
     * @returns {*}
     */
    incrementIGRIncreasesItForAGivenIG () {
        let currentIGR = this.testQuestionView.getCurrentIGR('MedicationEntry_Loop_IG');
        expect(currentIGR).toEqual(1);

        this.testQuestionView.incrementIGR('MedicationEntry_Loop_IG');
        currentIGR = this.testQuestionView.getCurrentIGR('MedicationEntry_Loop_IG');
        expect(currentIGR).toEqual(2);

        return Q();
    }

    /**
     * Test explicitly calling decrementIGR decreases the IGR for the IG
     * @returns {*}
     */
    decrementIGRDecreasesItForAGivenIG () {
        let currentIGR = this.testQuestionView.getCurrentIGR('MedicationEntry_Loop_IG');
        this.testQuestionView.incrementIGR('MedicationEntry_Loop_IG');
        this.testQuestionView.decrementIGR('MedicationEntry_Loop_IG');
        currentIGR = this.testQuestionView.getCurrentIGR('MedicationEntry_Loop_IG');
        expect(currentIGR).toEqual(1);

        return Q();
    }

    /**
     * Tests that we can correctly query the answers based on ID and IGR
     * @returns {Q.Promise}
     */
    returnsAnswersForQuestionIDAndIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(2);
            })
            .tap(() => {
                // grab the answers for the first IGR and make sure they're pulled in correctly
                let answers = this.testQuestionView.queryAnswersByQuestionIDAndIGR('LOOP1_MedSelection_Question', 1);
                expect(answers[0].get('response')).toEqual(1);
                answers = this.testQuestionView.queryAnswersByQuestionIDAndIGR('LOOP1_TimePicker_Question', 1);
                expect(answers[0].get('response')).toEqual('12:01');
                return LF.Branching.branchFunctions.addNewMedsLoop(this.testQuestionView);

                // The return above makes the following unreachable:
                // // grab the answers for the second IGR and make sure they're pulled in correctly
                // answers = this.testQuestionView.queryAnswersByQuestionIDAndIGR('LOOP1_MedSelection_Question', 2);
                // expect(answers[0].get('response')).toEqual(2);
                // answers = this.testQuestionView.queryAnswersByQuestionIDAndIGR('LOOP1_TimePicker_Question', 2);
                // expect(answers[0].get('response')).toEqual('12:02');
            });
    }

    /**
     * Tests that we can query all answers for a specific IGR
     * @returns {Q.Promise}
     */
    returnsAnswersForSpecificIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(2);
            })
            .tap(() => {
                // check that all answers for each IGR are returned
                let answersForIGR = this.testQuestionView.queryAnswersByIGR(1);
                expect(answersForIGR.length).toEqual(2);
                expect(answersForIGR[0].get('response')).toEqual(1);
                expect(answersForIGR[1].get('response')).toEqual('12:01');

                answersForIGR = this.testQuestionView.queryAnswersByIGR(2);
                expect(answersForIGR.length).toEqual(2);
                expect(answersForIGR[0].get('response')).toEqual(2);
                expect(answersForIGR[1].get('response')).toEqual('12:02');
            });
    }

    /**
     * checks that only answers for a specific IGR are returned when calling queryAnswersByIGAndIGR
     * @returns {Q.Promise}
     */
    returnsAnswersByIGAndIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(2);
            })
            .tap(() => {
                let answersForIGRandIG = this.testQuestionView.queryAnswersByIGAndIGR('MedicationEntry_Loop_IG', 1);
                expect(answersForIGRandIG.length).toEqual(2);
                expect(answersForIGRandIG[0].get('response')).toEqual(1);
                expect(answersForIGRandIG[1].get('response')).toEqual('12:01');

                answersForIGRandIG = this.testQuestionView.queryAnswersByIGAndIGR('MedicationEntry_Loop_IG', 2);
                expect(answersForIGRandIG.length).toEqual(2);
                expect(answersForIGRandIG[0].get('response')).toEqual(2);
                expect(answersForIGRandIG[1].get('response')).toEqual('12:02');
            });
    }

    /**
     * checks that only answers for a specific IGR and IT are returned when calling queryAnswersByITAndIGR
     * @returns {Q.Promise}
     */
    returnAnswersByITandIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(3);
            })
            .tap(() => {
                let answersForIGRandIG = this.testQuestionView.queryAnswersByITAndIGR('MedLoop_IT', 1);
                expect(answersForIGRandIG.length).toEqual(1);
                expect(answersForIGRandIG[0].get('response')).toEqual(1);

                answersForIGRandIG = this.testQuestionView.queryAnswersByITAndIGR('MedLoop_IT', 2);
                expect(answersForIGRandIG.length).toEqual(1);
                expect(answersForIGRandIG[0].get('response')).toEqual(2);

                answersForIGRandIG = this.testQuestionView.queryAnswersByITAndIGR('MedLoop_IT', 3);
                expect(answersForIGRandIG.length).toEqual(1);
                expect(answersForIGRandIG[0].get('response')).toEqual(3);
            });
    }

    /**
     * This test ensures that the correct answers are returned when queryAnswersByIGITAndIGR is called
     * @returns {Q.Promise}
     */
    returnAnswersByIGITandIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(3);
            })
            .tap(() => {
                let answersForIGRandIG = this.testQuestionView.queryAnswersByIGITAndIGR('MedicationEntry_Loop_IG', 'MedLoop_IT', 1);
                expect(answersForIGRandIG.length).toEqual(1);
                expect(answersForIGRandIG[0].get('response')).toEqual(1);

                answersForIGRandIG = this.testQuestionView.queryAnswersByIGITAndIGR('MedicationEntry_Loop_IG', 'MedLoop_IT', 2);
                expect(answersForIGRandIG.length).toEqual(1);
                expect(answersForIGRandIG[0].get('response')).toEqual(2);

                answersForIGRandIG = this.testQuestionView.queryAnswersByIGITAndIGR('MedicationEntry_Loop_IG', 'MedLoop_IT', 3);
                expect(answersForIGRandIG.length).toEqual(1);
                expect(answersForIGRandIG[0].get('response')).toEqual(3);
            });
    }

    /**
     * tests that the correct question views are returned by queryQuestionsByIGR
     * @returns {Q.Promise}
     */
    returnQuestionViewsByIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(3);
            })
            .tap(() => {
                let questionViewsByIGandIGR = this.testQuestionView.queryQuestionsByIGR(1);

                // there should be 3 question views for Loop1B IG.
                // 0 = Review Screen
                // 1 = Screen 3 (Choose Meds Screen)
                // 2 = Screen 4 (Choose Time Screen)
                expect(questionViewsByIGandIGR.length).toEqual(3);

                expect(questionViewsByIGandIGR[1].widget.answers.models[0].get('response')).toEqual(1);
                expect(questionViewsByIGandIGR[2].widget.answers.models[0].get('response')).toEqual('12:01');

                questionViewsByIGandIGR = this.testQuestionView.queryQuestionsByIGR(2);
                expect(questionViewsByIGandIGR[1].widget.answers.models[0].get('response')).toEqual(2);
                expect(questionViewsByIGandIGR[2].widget.answers.models[0].get('response')).toEqual('12:02');

                questionViewsByIGandIGR = this.testQuestionView.queryQuestionsByIGR(3);
                expect(questionViewsByIGandIGR[1].widget.answers.models[0].get('response')).toEqual(3);
                expect(questionViewsByIGandIGR[2].widget.answers.models[0].get('response')).toEqual('12:03');
            });
    }

    /**
     * tests that the correct questions views are returned by queryQuestionViewsByIGAndIGR()
     * @returns {Q.Promise}
     */
    returnQuestionViewsByIGAndIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(3);
            })
            .tap(() => {
                let questionViewsByIGandIGR = this.testQuestionView.queryQuestionViewsByIGAndIGR('MedicationEntry_Loop_IG', 1);

                // there should be 3 question views for Loop1B IG.
                // 0 = Review Screen
                // 1 = Screen 3 (Choose Meds Screen)
                // 2 = Screen 4 (Choose Time Screen)
                expect(questionViewsByIGandIGR.length).toEqual(3);

                expect(questionViewsByIGandIGR[1].widget.answers.models[0].get('response')).toEqual(1);
                expect(questionViewsByIGandIGR[2].widget.answers.models[0].get('response')).toEqual('12:01');

                questionViewsByIGandIGR = this.testQuestionView.queryQuestionViewsByIGAndIGR('MedicationEntry_Loop_IG', 2);
                expect(questionViewsByIGandIGR[1].widget.answers.models[0].get('response')).toEqual(2);
                expect(questionViewsByIGandIGR[2].widget.answers.models[0].get('response')).toEqual('12:02');

                questionViewsByIGandIGR = this.testQuestionView.queryQuestionViewsByIGAndIGR('MedicationEntry_Loop_IG', 3);
                expect(questionViewsByIGandIGR[1].widget.answers.models[0].get('response')).toEqual(3);
                expect(questionViewsByIGandIGR[2].widget.answers.models[0].get('response')).toEqual('12:03');
            });
    }

    /**
     * Removes the answer that matches the IG, IT and IGR
     * @returns {Q.Promise}
     */
    removesAnswerWithMatchingIGITAndIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(1);
            })
            .tap(() => {
                this.testQuestionView.removeIT('MedicationEntry_Loop_IG', 'MedLoop_IT', 1);

                //the answer should no longer exist
                let answers = this.testQuestionView.queryAnswersByIGITAndIGR('MedicationEntry_Loop_IG', 'MedLoop_IT', 1);
                expect(answers.length).toEqual(0);
            });
    }

    /**
     * Copies the answers from one IGR to another and makes sure they are the same
     * @returns {Q.Promise}
     */
    copiesAnswersFromOneIGRtoAnotherIGRForAGivenIG () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(2);
            })
            .tap(() => {
                // copy the answers from IGR = 1 to IGR = 2
                this.testQuestionView.copyIGR('MedicationEntry_Loop_IG', 1, 3);

                // query the answers for IGR = 2 and make sure they are now the same as IGR = 1
                let answersByIGR = this.testQuestionView.queryAnswersByIGR(3);

                expect(answersByIGR[0].get('response')).toEqual(1);
                expect(answersByIGR[1].get('response')).toEqual('12:01');
            });
    }

    /**
     * Make sure removeLoopEvent removes an entire loop for a given IG
     * @returns {Q.Promise}
     */
    removeLoopEventRemovesLoopForIG () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(3);
            })
            .tap(() => {
                expect(this.testQuestionView.queryAnswersByIGR(3).length).toEqual(2);
                this.testQuestionView.removeLoopEvent('MedicationEntry_Loop_IG', 3);
                expect(this.testQuestionView.queryAnswersByIGR(3).length).toEqual(0);
            });
    }

    /**
     * Creates an empty IGR using makeIGRAvailable
     * @returns {Q.Promise}
     */
    makeIGRAvailableInsertsEmptyIGR () {
        return this.testQuestionView.navigationHandler('next')
            .then(() => {
                return this.addAllAnswersForACertainNumberOfLoops(2);
            })
            .tap(() => {
                expect(this.testQuestionView.questionViews[
                        this.testQuestionView.questionViews.length - 1
                    ].igr).toEqual(3);
                expect(this.testQuestionView.queryAnswersByIGAndIGR('MedicationEntry_Loop_IG', 2).length).toEqual(2);

                this.testQuestionView.makeIGRAvailable('MedicationEntry_Loop_IG', 2);

                expect(this.testQuestionView.queryAnswersByIGAndIGR('MedicationEntry_Loop_IG', 2).length).toEqual(0);
                expect(this.testQuestionView.questionViews[
                        this.testQuestionView.questionViews.length - 1
                    ].igr).toEqual(4);
            });
    }
}
