export default {

    questionnaires: [
        {
            id: 'Meds',
            SU: 'Loop1',
            displayName: 'DISPLAY_NAME',
            previousScreen: true,
            affidavit: 'DEFAULT',
            accessRoles: ['subject'],
            noRoleFound: 'STUDY_NO_ROLE_FOUND',
            screens: [
                'LOOP1_DidYouTakeMeds_Screen',
                'LOOP1_Review_Screen',
                'LOOP1_MedSelection_Screen',
                'LOOP1_TimePicker_Screen',
                'LOOP1_SelectedMotrin_TimePicker_Screen',
                'LOOP1_FinalReview_Screen'
            ],
            branches: [{
                branchFrom: 'LOOP1_Review_Screen',
                branchTo: 'LOOP1_FinalReview_Screen',
                clearBranchedResponses: false,
                branchFunction: 'always'
            }, {
                branchFrom: 'LOOP1_TimePicker_Screen',
                branchTo: 'LOOP1_Review_Screen',
                clearBranchedResponses: false,
                branchFunction: 'toReviewScreenLoop1'
            }, {
                branchFrom: 'LOOP1_MedSelection_Screen',
                branchTo: 'LOOP1_SelectedMotrin_TimePicker_Screen',
                clearBranchedResponses: false,
                branchFunction: 'selectedMotrin'
            }, {
                branchFrom: 'AFFIDAVIT',
                branchTo: 'LOOP1_Review_Screen',
                clearBranchedResponses: false,
                branchFunction: 'always'
            }]
        }
    ],
    screens: [
        {
            id: 'LOOP1_DidYouTakeMeds_Screen',
            className: 'LOOP1_DidYouTakeMeds_Screen',
            questions: [{
                id: 'LOOP1_DidYouTakeMeds_Question',
                mandatory: false
            }]
        }, {
            id: 'LOOP1_Review_Screen',
            className: 'LOOP1_Review_Screen',
            questions: [{
                id: 'LOOP1_Review_Question',
                mandatory: false
            }]
        }, {
            id: 'LOOP1_MedSelection_Screen',
            className: 'LOOP1_MedSelection_Screen',
            questions: [{
                id: 'LOOP1_MedSelection_Question',
                mandatory: false
            }]
        }, {
            id: 'LOOP1_TimePicker_Screen',
            className: 'LOOP1_TimePicker_Screen',
            questions: [{
                id: 'LOOP1_TimePicker_Question',
                mandatory: false
            }]
        }, {
            id: 'LOOP1_SelectedMotrin_TimePicker_Screen',
            className: 'LOOP1_SelectedMotrinTimePicker_Screen',
            questions: [{
                id: 'LOOP1_SelectedMotrin_TimePicker_Question',
                mandatory: false
            }]
        }, {
            id: 'LOOP1_FinalReview_Screen',
            className: 'LOOP1_FinalReview_Screen',
            accessRoles: ['subject'],
            questions: [{
                id: 'LOOP1_FinalReview_Question',
                mandatory: false
            }]
        }
    ],
    questions: [
        {
            id: 'LOOP1_DidYouTakeMeds_Question',
            IG: 'DidYouTakeMeds_Loop_IG',
            IT: 'MedsToday',
            text: 'QUESTION_1',
            className: 'LOOP1_DidYouTakeMeds_Question',
            widget: {
                id: 'LOOP1_DidYouTakeMeds_Widget',
                type: 'RadioButton',
                templates: {},
                answers: [{
                    text: 'YES',
                    value: '1'
                }, {
                    text: 'NO',
                    value: '0'
                }]
            }
        }, {
            id: 'LOOP1_Review_Question',
            IG: 'MedicationEntry_Loop_IG',
            repeating: true,
            text: 'QUESTION_2',
            className: 'LOOP1_Review_Question',
            widget: {
                id: 'LOOP1_ReviewScreen_Widget',
                type: 'ReviewScreen',
                screenFunction: 'medsLoop1',
                templates: {
                    itemsTemplate: 'LOOP1:ScreenItems'
                },
                buttons: [{
                    id: 1,
                    availability: 'limitReached',
                    text: 'ADD_MEDS',
                    actionFunction: 'addNewMedsLoop1'
                }, {
                    id: 2,
                    text: 'EDIT_MEDS',
                    actionFunction: 'editMedLoop1'
                }, {
                    id: 3,
                    text: 'DELETE_MEDS',
                    actionFunction: 'deleteMedLoop1'
                }, {
                    id: 4,
                    text: 'INSERT_MEDS',
                    actionFunction: 'insertMedLoop1'
                }]
            }
        }, {
            id: 'LOOP1_MedSelection_Question',
            IG: 'MedicationEntry_Loop_IG',
            IT: 'MedLoop_IT',
            text: 'QUESTION_3',
            className: 'LOOP1_MedSelection_Question',
            widget: {
                id: 'LOOP1_MedSelection_Widget',
                type: 'RadioButton',
                answers: [{
                    text: 'ADVIL',
                    value: '0'
                }, {
                    text: 'ALEVE',
                    value: '1'
                }, {
                    text: 'ASPIRIN',
                    value: '2'
                }, {
                    text: 'EXCEDRIN',
                    value: '3'
                }, {
                    text: 'MOTRIN',
                    value: '4'
                }, {
                    text: 'OXYCONTIN',
                    value: '5'
                }, {
                    text: 'TYLENOL',
                    value: '6'
                }, {
                    text: 'VICODIN',
                    value: '7'
                }]
            }
        }, {
            id: 'LOOP1_TimePicker_Question',
            IG: 'MedicationEntry_Loop_IG',
            IT: 'MedTime',
            text: 'QUESTION_4',
            className: 'LOOP1_TimePicker_Question',
            widget: {
                id: 'LOOP1_TimePicker_Widget',
                type: 'TimePicker',
                configuration: {}
            }
        }, {
            id: 'LOOP1_SelectedMotrin_TimePicker_Question',
            IG: 'MedicationEntry_Loop_IG',
            IT: 'MedTime',
            text: 'What time did you take your MOTRIN',
            className: 'LOOP1_SelectedMotrin_TimePicker_Question',
            widget: {
                id: 'LOOP1_TimePicker_Widget',
                type: 'TimePicker',
                configuration: {}
            }
        }, {
            id: 'LOOP1_FinalReview_Question',
            IG: 'MedicationEntry_Loop_IG',
            text: 'QUESTION_5',
            className: 'LOOP1_FinalReview_Question',
            widget: {
                id: 'LOOP1_FinalReview_Widget',
                type: 'ReviewScreen',
                screenFunction: 'medsLoop1',
                templates: {
                    itemsTemplate: 'LOOP1:ScreenItems'
                }
            }
        }
    ],

    affidavits: [
        { // Do NOT remove this configuration. Please refer to Affidavit_Study_Design_1_4 for more information.
            id: 'DEFAULT',
            text: [
                'AFFIDAVIT_HELP',
                'AFFIDAVIT'
            ],
            krSig: 'AFFIDAVIT',
            widget: {
                id: 'AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'CheckBox',
                answers: [
                    {
                        text: 'OK',
                        value: 0
                    }
                ]
            }
        }
    ]
};

export let templates = [
    {
        name: 'Screen',
        namespace: 'DEFAULT',
        template: '<section class="screen {{ className }}"></div>'
    },
    {
        name: 'Question',
        namespace: 'DEFAULT',
        template: '<p class="{{ label }}">{{ text }}</p>'
    },
    // Label wrapper for radio buttons.
    {
        name: 'RadioButtonWrapper',
        namespace: 'DEFAULT',
        template: '<label class="btn btn-default btn-block"><span data-container></span></label>'
    },
    // Default radio button input.
    {
        name: 'RadioButton',
        namespace: 'DEFAULT',
        template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
    },

    // Default template for custom RadioButton input that does not include jquery mobile css style.
    {
        name: 'CustomRadioButton',
        namespace: 'DEFAULT',
        template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}" data-role="none"/>'
    },

    // Default template for RadioButton label.
    {
        name: 'CustomRadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ link }}" class="radio-text {{ className }}"> {{ text }} </label>'
    },

    // Default template for RadioButton label.
    {
        name: 'RadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<span class="radio-text {{ className }}"> {{ text }} </span>'
    },
    {
        name: 'VerticalButtonGroup',
        namespace: 'DEFAULT',
        template: '<div class="btn-group-vertical" data-toggle="buttons" data-container></ul>'
    },
    // Label wrapper for radio buttons.
    {
        name: 'CheckBoxWrapper',
        namespace: 'DEFAULT',
        template: '<label class="btn btn-default btn-block"><span data-container></span></label>'
    },
    // Default radio button input.
    {
        name: 'CheckBox',
        namespace: 'DEFAULT',
        template: '<input type="checkbox" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
    },
    // Default template for RadioButton label.
    {
        name: 'CheckboxLabel',
        namespace: 'DEFAULT',
        template: '<span class="checkbox-text {{ className }}"> {{ text }} </span>'
    },
    {
        name        : 'ReviewScreen',
        namespace   : 'DEFAULT',
        template    : '<div id="{{ id }}" class="reviewScreen"><ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview"></ul>' +
        '<hr />' +
        '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul></div>'
    },
    {
        name        : 'ReviewScreenItems',
        namespace   : 'DEFAULT',
        template    : '<li><div>{{ text0 }}</div></li>'
    },
    {
        name: 'ReviewScreenButtons',
        namespace: 'DEFAULT',
        template: '<li><a data-role="button">{{ text }}</a></li>'
    },
    {
        name: 'TimePicker',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}_time_input" class="form-control time-input"  data-role="datebox" />'
    },
    {
        name: 'TimePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}_time_input">{{ timeLabel }}</label>'
    }
];
