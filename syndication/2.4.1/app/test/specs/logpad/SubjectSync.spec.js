// The test is disabled so let's disable the eslint rules too
/* eslint-disable */   
xdescribe('SubjectSync', function () {

    let getResponseHeader = (id) => {
        switch (id) {
            case 'X-Device-Status':
                return '1';
            case 'X-Sync-ID':
                return '13';
            default:
                return '';
        }
    };

    beforeEach(function () {
        LF.webService = new LF.Class.WebService();
        LF.SpecHelpers.createSubject();
        localStorage.setItem('krpt', 'krpt.312489213jk123241232kjasdkj');

        spyOn(LF.webService, 'getkrDom').and.callFake(() => Q(null));
        spyOn(LF.Collection.Users.prototype, 'syncAdminPassword').and.callFake(() => Q());
    });

    afterEach(function () {
        LF.webService = undefined;
        localStorage.clear();
        LF.Data = {};
    });

    it('should install the database.', function () {

        LF.SpecHelpers.installDatabase();

    });

    it('should create the Subjects storage object and save the record.', function () {

        LF.SpecHelpers.saveSubject();

    });

    it('should save a transmission to the transmissions table.', function () {

        let response,
            model = new LF.Model.Transmission();

        model.save({
            method  : 'FakeTransmission',
            created : new Date().getTime()
        }, {
            onSuccess : function (res) {
                response = res;
            }
        });

        waitsFor(function () {
            return response;
        }, 'the transmission record to save to the database.', 1000);

        runs(function () {
            expect(response).toEqual(1);
        });

    });

    it('should remove the fake transmission from the queue.', function () {

        let response,
            collection = new LF.Collection.Transmissions();

        collection.pullQueue(function () {

            collection.destroy(1, function () {

                response = collection.size();

            });

        });

        waitsFor(function () {
            return typeof response === 'number';
        }, 'the fake transmission to be removed.', 1000);

        runs(function () {
            expect(response).toEqual(0);
        });

    });

    it('should create and execute the transmission record with no resulting DCF.', function () {

        let response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id');

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${setupCode}\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber`,
            type        : 'GET',
            success     : {
                data    : {},
                status  : 'success',
                xhr     : {
                    getResponseHeader,
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync(null, function () {

            response = true;

        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 2000);

        runs(function () {

            expect(response).toBeTruthy();

        });

    });

    it('should create and execute the transmission record with a resulting DCF change to site_code.', function () {

        let response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id'),
            collection = new LF.Collection.Subjects();

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${setupCode}\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber`,
            type        : 'GET',
            success     : {
                data    : { C: '007' },
                status  : 'success',
                xhr     : {
                    getResponseHeader,
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync(null, function () {

            collection.fetch({
                onSuccess : function () {
                    response = collection.at(0);
                }
            });

        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 1000);

        runs(function () {

            expect(response.get('site_code')).toBeTruthy('007');
            expect(response.get('subject_number')).toEqual('54321');
            expect(response.get('phase')).toEqual(10);
            expect(response.get('initials')).toEqual('bc');

        });

    });

    it('should create and execute the transmission record with a resulting DCF change to subject_number.', function () {

        let response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id'),
            collection = new LF.Collection.Subjects();

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${setupCode}\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber`,
            type        : 'GET',
            success     : {
                data    : { N: '43110' },
                status  : 'success',
                xhr     : {
                    getResponseHeader,
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync(null, function () {

            collection.fetch({
                onSuccess : function () {
                    response = collection.at(0);
                }
            });

        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 1000);

        runs(function () {

            expect(response.get('site_code')).toBeTruthy('007');
            expect(response.get('subject_number')).toEqual('43110');
            expect(response.get('phase')).toEqual(10);
            expect(response.get('initials')).toEqual('bc');

        });

    });

    it('should create and execute the transmission record with a resulting DCF change to initials.', function () {

        let response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id'),
            collection = new LF.Collection.Subjects();

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${setupCode}\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber`,
            type        : 'GET',
            success     : {
                data    : { I: 'REB' },
                status  : 'success',
                xhr     : {
                    getResponseHeader,
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync(null, function () {

            collection.fetch({
                onSuccess : function () {
                    response = collection.at(0);
                }
            });

        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 2000);

        runs(function () {

            expect(response.get('site_code')).toBeTruthy('007');
            expect(response.get('subject_number')).toEqual('54321');
            expect(response.get('phase')).toEqual(10);
            expect(response.get('initials')).toEqual('REB');

        });

    });

    it('should create and execute the transmission record with a resulting DCF change to phase.', function () {

        let response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id'),
            collection = new LF.Collection.Subjects();

        fakeAjax({registrations: [{
            url         : `../../api/v1/subjects/${setupCode}\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber`,
            type        : 'GET',
            success     : {
                data    : { P: 20 },
                status  : 'success',
                xhr     : {
                    getResponseHeader,
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync(null, function () {

            collection.fetch({
                onSuccess : function () {
                    response = collection.at(0);
                }
            });

        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 1000);

        runs(function () {

            expect(response.get('site_code')).toBeTruthy('007');
            expect(response.get('subject_number')).toEqual('54321');
            expect(response.get('phase')).toEqual(20);
            expect(response.get('initials')).toEqual('bc');

        });

    });

    it('should uninstall the database.', function () {

        LF.SpecHelpers.uninstallDatabase();

    });

});
