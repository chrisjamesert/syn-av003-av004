xdescribe('Indicator', function () {

    let indicatorView,
        loginView,
        dashboardView,
        currentDate = new Date(),
        currentTime = `${currentDate.getHours()}:${currentDate.getMinutes()}`,
        availableHrsEnd = currentDate.getHours() + 2,
        availableTimeEnd = `${availableHrsEnd}:${currentDate.getMinutes()}`,
        pendingMinEnd = currentDate.getMinutes() + 30,
        pendingTimeEnd = `${currentDate.getHours()}:${pendingMinEnd}`,
        urgentMinEnd = currentDate.getMinutes() + 3,
        urgentTimeEnd = `${currentDate.getHours()}:${urgentMinEnd}`,
        unavailableHrsBegin = currentDate.getHours - 3,
        unavailableTimeBegin = `${unavailableHrsBegin}:${currentDate.getMinutes()}`,
        unavailableTimeEnd = `${unavailableHrsBegin}:${pendingMinEnd}`,
        lastDiaries = new LF.Collection.LastDiaries(),
        LFindicators = LF.StudyDesign.indicators,
        LFrules = LF.StudyRules,
        LFschedules = LF.StudyDesign.schedules,
        triggerRun = false,
        loginTemplate = '<div id="login-template">' +
            '<a id="pending-transmission" href="#"></a>' +
            '<div id="indicators"></div>' +
            '<input type="password" name="password_{{ key }}" id="password" />' +
            '<button disabled type="submit" id="login" />' +
            '</div>',
        dashboardTemplate = '<div id="dashboard-template">' +
            '<a id="pending-transmission" href="#"></a>' +
            '<div id="dashboard-content"><div id="indicators"></div></div>' +
            '</div>';

    beforeEach(function () {

        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(true);
        });

        spyOn(LF.DynamicText, 'imageFolder').andCallFake(function (callback) {
            callback('../../trial/images/');
        });

        lastDiaries.reset();
        LF.SpecHelpers.createSubject();
        LF.router = new LF.Router.ApplicationRouter();

        if (dashboardView) {
            spyOn(dashboardView, 'refresh');
        }

        LF.router.view = { id: 'dashboard-page'};

        $('body').append(loginTemplate);

        $('body').append(dashboardTemplate);

        LF.StudyRules.add({
            id          : 'IndicatorTriggerTest',
            trigger     : 'DASHBOARD:TriggerIndicator/Test_Urgent',
            expression  : true,
            actionData  : [
                {
                    trueAction      : function (filter, callback) {
                        triggerRun = true;
                        callback();
                    }
                }
            ]
        });

        LF.StudyDesign.indicators = new LF.Collection.Indicators([
            {
                id: 'Take_Medication',
                className: 'Take_Pill',
                image: 'PILL_IMAGE',
                label: 'TAKE_PILL'
            }, {
                id: 'Test_Indicator',
                className: 'Test_Indicator',
                label: 'TEST_INDICATOR'
            }, {
                id: 'Test_Urgent',
                className: 'Test_Indicator',
                image: 'PILL_IMAGE'
            }, {
                id: 'Test_Unavailable',
                className: 'Test_Indicator',
                image: 'PILL_IMAGE',
                label: 'TEST_INDICATOR'
            }
        ]);

        LF.StudyDesign.schedules = new LF.Collection.Schedules([
            {
                id: 'Take_Pill',
                target: {
                    objectType: 'indicator',
                    id: 'Take_Medication'
                },
                scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
                scheduleParams: {
                    startAvailability: currentTime,
                    endAvailability: availableTimeEnd
                }
            }, {
                id: 'Indicator_Test',
                target: {
                    objectType: 'indicator',
                    id: 'Test_Indicator'
                },
                scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
                scheduleParams: {
                    startAvailability: currentTime,
                    endAvailability: pendingTimeEnd,
                    customClassname: 'impending'
                }
            }, {
                id: 'Urgent_Indicator_Test',
                target: {
                    objectType: 'indicator',
                    id: 'Test_Urgent'
                },
                scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
                scheduleParams: {
                    startAvailability: currentTime,
                    endAvailability: urgentTimeEnd,
                    customClassname: 'urgent'
                }
            }, {
                id: 'Unavailable_Test',
                target: {
                    objectType: 'indicator',
                    id: 'Test_Unavailable'
                },
                scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
                scheduleParams: {
                    startAvailability: unavailableTimeBegin,
                    endAvailability: unavailableTimeEnd
                }
            }
        ]);

    });

    afterEach(function () {
        LF.schedule.stopTimer();
        LF.Data = {};
        LF.router = undefined;
        LF.StudyDesign.indicators = LFindicators;
        LF.StudyRules = LFrules;
        LF.StudyDesign.schedules = LFschedules;
        localStorage.clear();
        lastDiaries.reset();
        LF.logs.reset();

        $('#login-template').remove();
        $('#dashboard-template').remove();

    });

    it('should install the database.', function () {

        LF.SpecHelpers.installDatabase();

    });

    it('should load Gateway Schedules', function () {
        runs(LF.Helpers.loadGatewaySchedules);

        waitsFor(function () {
            return LF.Gateways.Subject.schedules.length > 0;
        }, 2000);
    });

    it('should create and save Subject the record.', function () {

        LF.SpecHelpers.createSubject();
        LF.SpecHelpers.saveSubject();

    });

    it('should render the Login View with three indicators.', function () {

        loginView =  new LF.View.LoginView();
        LF.router.view = loginView;

        waitsFor(function () {
            return loginView.el.childElementCount > 0 && loginView.el.childNodes[1].children.length > 0;
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(loginView.$el.attr('id')).toEqual('login-page');
            expect(loginView.indicators.length).toEqual(4);
            expect(loginView.el.childNodes[1].children[0].children[0].id).toEqual('Take_Medication');
            expect(loginView.el.childNodes[1].children[0].children[0].className).toEqual('indicator Take_Pill');
            expect(loginView.el.childNodes[1].children[0].children[1].id).toEqual('Test_Indicator');
            expect(loginView.el.childNodes[1].children[0].children[1].className).toEqual('indicator impending');
            expect(loginView.el.childNodes[1].children[0].children[2].id).toEqual('Test_Urgent');
            expect(loginView.el.childNodes[1].children[0].children[2].className).toEqual('indicator urgent top');
            expect(loginView.el.childNodes[1].children[0].children[3]).toBeUndefined();

        });

    });

    xit('should render the Dashboard View with three indicators.', function () {

        dashboardView =  new LF.View.DashboardView();
        LF.router.view = dashboardView;

        waitsFor(function () {
            return dashboardView.el.childElementCount > 0 && dashboardView.el.lastChild.childNodes[0].children.length > 0;
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(dashboardView.listViews.indicatorsList.subviews.length).toEqual(3);
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[0].id).toEqual('Take_Medication');
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[0].className).toEqual('indicator Take_Pill');
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[1].id).toEqual('Test_Indicator');
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[1].className).toEqual('indicator impending');
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[2].id).toEqual('Test_Urgent');
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[2].className).toEqual('indicator urgent top');
            expect(dashboardView.el.lastChild.childNodes[0].children[0].children[3]).toBeUndefined();

        });

    });

    it('should not trigger Test_Indicator.', function () {

        indicatorView =  new LF.View.IndicatorView({
            model   : LF.StudyDesign.indicators.at(1),
            parent  : dashboardView,
            id      : LF.StudyDesign.indicators.at(1).get('id')
        });

        indicatorView.trigger();

        expect(triggerRun).toBeFalsy();

    });

    it('should trigger Test_Urgent.', function () {

        indicatorView =  new LF.View.IndicatorView({
            model   : LF.StudyDesign.indicators.at(2),
            parent  : dashboardView,
            id      : LF.StudyDesign.indicators.at(2).get('id')
        });

        indicatorView.trigger();

        expect(triggerRun).toBeTruthy();

    });

    it('should get availability true for available indicator Take Medication', function () {

        let response,
            phaseInfo,
            indicatorModel,
            schedules = LF.StudyDesign.schedules;

        LF.router.view = dashboardView;

        lastDiaries.fetch({
            onSuccess : function () {
                LF.schedule.getPhaseInformation(function (phaseInformation) {
                    phaseInfo = phaseInformation;
                    response = true;
                });
            }
        });
        waitsFor(function () {
            return response;
        }, 'the last diaries and current phase information to be fetched.', 1000);

        runs(function () {
            let response,
                availability;

            LF.schedule.evaluateSchedule(schedules.models[0], phaseInfo, lastDiaries, function (availabilityCallback) {
                availability = availabilityCallback;
                indicatorModel = dashboardView.indicators.find(function (indicator) {
                    return indicator.get('id') === schedules.models[0].get('target').id;
                });
                response = true;
            });

            waitsFor(function () {
                return response;
            }, 'schedule evaluation to be executed.', 1000);

            runs(function () {
                expect(availability).toEqual(true);
                expect(indicatorModel.get('className')).toEqual('Take_Pill');
            });
        });
    });

    it('should get availability true for available indicator Test Indicator', function () {

        let response,
            phaseInfo,
            indicatorModel,
            schedules = LF.StudyDesign.schedules;

        LF.router.view = dashboardView;

        lastDiaries.fetch({
            onSuccess : function () {
                LF.schedule.getPhaseInformation(function (phaseInformation) {
                    phaseInfo = phaseInformation;
                    response = true;
                });
            }
        });
        waitsFor(function () {
            return response;
        }, 'the last diaries and current phase information to be fetched.', 1000);

        runs(function () {
            let response,
                availability;

            LF.schedule.evaluateSchedule(schedules.models[1], phaseInfo, lastDiaries, function (availabilityCallback) {
                availability = availabilityCallback;
                indicatorModel = dashboardView.indicators.find(function (indicator) {
                    return indicator.get('id') === schedules.models[1].get('target').id;
                });
                response = true;
            });

            waitsFor(function () {
                return response;
            }, 'schedule evaluation to be executed.', 1000);

            runs(function () {
                expect(availability).toEqual(true);
                expect(indicatorModel.get('className')).toEqual('impending');
            });
        });
    });

    it('should get availability true for available indicator Test Urgent', function () {

        let response,
            phaseInfo,
            indicatorModel,
            schedules = LF.StudyDesign.schedules;

        LF.router.view = dashboardView;

        lastDiaries.fetch({
            onSuccess : function () {
                LF.schedule.getPhaseInformation(function (phaseInformation) {
                    phaseInfo = phaseInformation;
                    response = true;
                });
            }
        });
        waitsFor(function () {
            return response;
        }, 'the last diaries and current phase information to be fetched.', 1000);

        runs(function () {
            let response,
                availability;

            LF.schedule.evaluateSchedule(schedules.models[2], phaseInfo, lastDiaries, function (availabilityCallback) {
                availability = availabilityCallback;
                indicatorModel = dashboardView.indicators.find(function (indicator) {
                    return indicator.get('id') === schedules.models[2].get('target').id;
                });
                response = true;
            });

            waitsFor(function () {
                return response;
            }, 'schedule evaluation to be executed.', 1000);

            runs(function () {
                expect(availability).toEqual(true);
                expect(indicatorModel.get('className')).toEqual('urgent');
            });
        });
    });

    it('should get availability false for indicator Test Indicator with nonexistent scheduling function', function () {

        let response,
            phaseInfo,
            schedules = LF.StudyDesign.schedules;

        schedules.models[1].set('scheduleFunction', 'wrongScheduleFunction');
        lastDiaries.fetch({
            onSuccess : function () {
                LF.schedule.getPhaseInformation(function (phaseInformation) {
                    phaseInfo = phaseInformation;
                    response = true;
                });
            }
        });

        waitsFor(function () {
            return response;
        }, 'the last diaries and current phase information to be fetched.', 1000);

        runs(function () {
            let response,
                availability;

            LF.schedule.evaluateSchedule(schedules.models[1], phaseInfo, lastDiaries, function (availabilityCallback) {
                availability = availabilityCallback;
                response = true;
            });

            waitsFor(function () {
                return response;
            }, 'schedule evaluation to be executed.', 1000);

            runs(function () {
                expect(availability).toEqual(false);
            });
        });
    });

    it('should get availability false for unavailable diary Test Unavailable', function () {

        let response,
            phaseInfo,
            schedules = LF.StudyDesign.schedules;

        LF.router.view = dashboardView;

        lastDiaries.fetch({
            onSuccess : function () {
                LF.schedule.getPhaseInformation(function (phaseInformation) {
                    phaseInfo = phaseInformation;
                    response = true;
                });
            }
        });

        waitsFor(function () {
            return response;
        }, 'the last diaries and current phase information to be fetched.', 1000);

        runs(function () {
            let response,
                availability;

            LF.schedule.evaluateSchedule(schedules.models[3], phaseInfo, lastDiaries, function (availabilityCallback) {
                availability = availabilityCallback;
                response = true;
            });

            waitsFor(function () {
                return response;
            }, 'schedule evaluation to be executed.', 1000);

            runs(function () {
                expect(availability).toEqual(false);
            });
        });
    });

    it('should uninstall the database.', function () {

        LF.SpecHelpers.uninstallDatabase();

    });

    it('should clean up schedules', function () {
        LF.Gateways.Subject = {};
        LF.Gateways.Site = {};
    });

});
