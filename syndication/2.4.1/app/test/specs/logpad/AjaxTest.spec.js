// The test is disabled so let's disable the eslint rules as well
/* eslint-disable */    
xdescribe('AJAX Test', function () {
    it('just works', function () {
        let message = 'Hello ';


        fakeAjax({
            registrations: [
                {
                    url         : '../../Controller.aspx',
                    type        : 'post',
                    data        : {
                        user    : 'dog'
                    },
                    successData : 'World!'
                }
            ]
        });

        $.post('../../Controller.aspx', {
            user    : 'dog'
        }, function (result) {
            message += result;
        });

        expect(message).toEqual('Hello World!');

    });
});
