xdescribe('TerminationCheck', function () {

    let subject,
        triggered,
        rules = LF.StudyRules;

    beforeEach(function () {

        LF.SpecHelpers.createSubject();

        LF.StudyRules = new LF.Collection.Rules([
                {
                    id          : 'Termination',
                    trigger     : 'Termination',
                    expression  : true,
                    actionData  : [
                        {
                            trueAction      : function (params) {
                                triggered = params;
                            },
                            trueArguments   : true
                        }
                    ]
                }
            ]);

    });

    afterEach(function () {

        LF.StudyRules = rules;
        LF.Data = {};
        localStorage.clear();

    });

    it('should install the database.', function () {

        LF.SpecHelpers.installDatabase();

    });

    it('should create the Subjects storage object and save the record.', function () {

        LF.SpecHelpers.createSubject();
        LF.SpecHelpers.saveSubject();

    });

    it('should populate the transmission queue.', function () {

        let transmission = new LF.Model.Transmission(),
            response;

        transmission.save({
            method  : 'MyFirstTransmission',
            params  : 'Hello World!',
            created : new Date().getTime()
        }, {
            onSuccess : function (res) {
                response = res;
            }
        });

        waitsFor(function () {
            return response;
        }, 'the transmission to save.', 1000);

        runs(function () {
            expect(response).toEqual(1);
        });

    });

    it('should not invoke the termination event (populated transmission queue).', function () {

        let response;

        LF.Actions.terminationCheck(null, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the termination check to complete.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
        });

    });

    it('should clear the transmission queue.', function () {

        let transmissions = new LF.Collection.Transmissions(),
            response;

        transmissions.clear({
            onSuccess : function () {
                response = true;
            }
        });

        waitsFor(function () {
            return response;
        }, 'the transmissions queue to clear.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
        });

    });

    it('should not invoke the termination event (not in termination phase).', function () {

        let response;

        LF.Actions.terminationCheck(null, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the termination check to complete.', 1000);

        runs(function () {
            expect(response).toBeTruthy();
        });

    });

    it('should set the subject\'s phase to termination.', function () {

        let response,
            subject = LF.Data.Subjects.at(0);

        subject.save({
            phase : LF.StudyDesign.terminationPhase
        }, {
            onSuccess : function () {
                response = true;
            }
        });

        waitsFor(function () {
            return response;
        }, 'the subject\'s phase to update.', 1000);

    });

    it('should invoke the termination event.', function () {

        LF.Actions.terminationCheck();

        waitsFor(function () {
            return triggered;
        }, 'the termination rule to be triggered.', 1000);

        runs(function () {
            expect(triggered).toBeTruthy();
        });

    });

    it('should uninstall the database.', function () {

        LF.SpecHelpers.uninstallDatabase();

    });

});
