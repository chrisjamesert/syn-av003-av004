import ELF from 'core/ELF';
import trainerRules from 'trainer/sitepad/resources/rules';

TRACE_MATRIX('US6733').
describe('Trainer rules', () => {
    beforeEach(() => {
        ELF.rules.clear();
    });

    afterEach(() => {
        ELF.rules.clear();
    });

    it('Should have the rule "GatewayTransmit"', () => {
        let found = _.find(trainerRules, (rule) => rule.id === 'GatewayTransmit');
        expect(found).not.toBeUndefined();
    });

    it('Should have GatewayTransmit triggered with correct ELF triggers', () => {
        let rule = _.find(trainerRules, (rule) => rule.id === 'GatewayTransmit'),
            triggers = [
                'HOME:Transmit',
                'VISITGATEWAY:Transmit',
                'FORMGATEWAY:Transmit',
                'LOGIN:Transmit',
                'USERMANAGEMENT:Transmit'
            ];

        triggers.forEach(triggerName => {
            expect(_.find(rule.trigger, (item) => item === triggerName)).not.toBeUndefined();
        });
    });

    it('Should have the rule "QuestionnaireTransmit"', () => {
        let found = _.find(trainerRules, (rule) => rule.id === 'QuestionnaireTransmit');
        expect(found).not.toBeUndefined();
    });

    it('Should have QuestionnaireTransmit triggered with correct ELF triggers', () => {
        let rule = _.find(trainerRules, (rule) => rule.id === 'QuestionnaireTransmit');
        expect(rule.trigger).toEqual('QUESTIONNAIRE:Transmit');
    });

    it('Should have the rule "ExitSitepadTrainer"', () => {
        let found = _.find(trainerRules, (rule) => rule.id === 'ExitSitepadTrainer');
        expect(found).not.toBeUndefined();
    });

    it('Should have ExitSitepadTrainer triggered with correct ELF triggers', () => {
        let rule = _.find(trainerRules, (rule) => rule.id === 'ExitSitepadTrainer');
        expect(rule.trigger).toEqual('SITEPADTRAINER:Exit');
    });

    it('Should run the correct actions for ExitSitepadTrainer', () => {
        let rule = _.find(trainerRules, (rule) => rule.id === 'ExitSitepadTrainer'),
            actions = [
                'uninstall',
                'disableTrainer'
            ];

        actions.forEach(actionName => {
            expect(_.find(rule.resolve, (item) => item.action === actionName)).not.toBeUndefined();
        });
    });
});
