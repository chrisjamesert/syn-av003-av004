import ELF from 'core/ELF';
import * as lStorage from 'core/lStorage';
import * as notify from 'core/Notify';
import * as utilities from 'core/utilities';
import * as trainerSiteSelectionView from 'trainer/sitepad/views/TrainerSiteSelectionView';
import * as trainerUnlockCodeView from 'trainer/sitepad/views/TrainerUnlockCodeView';
import * as trainerSettingsView from 'trainer/sitepad/views/TrainerSettingsView';
import * as trainerWebservice from 'trainer/sitepad/classes/TrainerWebservice';
import * as trainerUtilities from 'trainer/sitepad/classes/TrainerUtilities';
import * as trainerSetTimeZoneActivationView from 'trainer/sitepad/views/TrainerSetTimeZoneActivationView';
import * as trainerSetTimeZoneView from 'trainer/sitepad/views/TrainerSetTimeZoneView';
import * as isStudyOnline from 'trainer/sitepad/expressions/isStudyOnline';
import Trainer from 'trainer/sitepad/classes/Trainer';
import TrainerUI from 'trainer/sitepad/classes/TrainerUI';
import TimeTravel from 'core/TimeTravel';

TRACE_MATRIX('US6732')
.TRACE_MATRIX('US6733')
.TRACE_MATRIX('US6734')
.TRACE_MATRIX('US6736')
.TRACE_MATRIX('US6735')
.describe('Trainer class', () => {
    describe('Property: enabled', () => {
        beforeEach(() => {
            localStorage.clear();
            ELF.rules.clear();
        });

        afterEach(() => {
            localStorage.clear();
            ELF.rules.clear();
        });

        it('Should have true when trainer mode is active.', () => {
            localStorage.setItem('trainer', true);

            expect(Trainer.enabled).toEqual(true);
        });

        it('Should have false when trainer mode is inactive.', () => {
            expect(Trainer.enabled).toEqual(false);

            localStorage.setItem('trainer', false);
            expect(Trainer.enabled).toEqual(false);

            localStorage.setItem('trainer', 1);
            expect(Trainer.enabled).toEqual(false);

            localStorage.setItem('trainer', 'Non-True value');
            expect(Trainer.enabled).toEqual(false);
        });
    });

    describe('Method: initialize', () => {
        beforeEach(() => {
            localStorage.clear();
            ELF.rules.clear();
        });

        afterEach(() => {
            localStorage.clear();
            ELF.rules.clear();
        });

        it('Should register disableTrainer as an ELF action.', () => {
            spyOn(ELF, 'action').and.callThrough();

            Trainer.initialize();
            expect(ELF.action).toHaveBeenCalledWith('disableTrainer', Trainer.disableTrainer);
        });

        it('Should register Trainer Mode Listener.', () => {
            spyOn(Trainer, 'registerTrainerModeListener').and.stub();

            Trainer.initialize();
            expect(Trainer.registerTrainerModeListener).toHaveBeenCalled();
        });

        it('Should enable trainer if the trainer flag is set.', () => {
            spyOn(Trainer, 'enableTrainer').and.stub();

            localStorage.setItem('trainer', true);
            Trainer.initialize();
            expect(Trainer.enableTrainer).toHaveBeenCalled();
        });

        it('Should not enable trainer if the trainer flag is not set.', () => {
            spyOn(Trainer, 'enableTrainer').and.stub();

            Trainer.initialize();
            expect(Trainer.enableTrainer).not.toHaveBeenCalled();
        });
    });

    describe('Method: registerTrainerModeListener', () => {
        beforeEach(() => {
            ELF.rules.clear();
        });

        afterEach(() => {
            ELF.rules.clear();
            localStorage.clear();
        });

        it('Should add SitepadTrainerModeListener to the ELF rules.', () => {
            spyOn(ELF.rules, 'add').and.callThrough();

            Trainer.registerTrainerModeListener();
            expect(ELF.rules.find('SitepadTrainerModeListener')).not.toBeUndefined();
        });

        Async.it('SitepadTrainerModeListener should call enableTrainer on MODESELECT:Submit.', () => {
            spyOn(Trainer, 'enableTrainer').and.stub();

            Trainer.registerTrainerModeListener();
            return ELF.trigger('MODESELECT:Submit', { product: 'sitepad', value: 'trainer' }, this)
            .then(() => {
                expect(Trainer.enableTrainer).toHaveBeenCalled();
            });
        });

        Async.it('SitepadTrainerModeListener should not call enableTrainer on MODESELECT:Submit.', () => {
            spyOn(Trainer, 'enableTrainer').and.stub();

            Trainer.registerTrainerModeListener();
            return ELF.trigger('MODESELECT:Submit', { product: 'logpad', value: 'trainer' }, this)
            .then(() => {
                expect(Trainer.enableTrainer).not.toHaveBeenCalled();
            })
            .then(() => ELF.trigger('MODESELECT:Submit', { product: 'sitepad', value: 'staging' }, this))
            .then(() => {
                expect(Trainer.enableTrainer).not.toHaveBeenCalled();
            });
        });

        it('Should add SitepadDisableTrainerMode to the ELF rules.', () => {
            spyOn(ELF.rules, 'add').and.callThrough();

            Trainer.registerTrainerModeListener();
            expect(ELF.rules.find('SitepadDisableTrainerMode')).not.toBeUndefined();
        });

        Async.it('SitepadDisableTrainerMode should call disableTrainer on SITESELECTION:Back.', () => {
            spyOn(Trainer, 'disableTrainer').and.stub();
            localStorage.setItem('trainer', true);

            Trainer.registerTrainerModeListener();
            return ELF.trigger('SITESELECTION:Back', { product: 'sitepad' }, this)
            .then(() => {
                expect(Trainer.disableTrainer).toHaveBeenCalled();
            });
        });

        Async.it('SitepadDisableTrainerMode should not call disableTrainer on SITESELECTION:Back', () => {
            spyOn(Trainer, 'disableTrainer').and.stub();
            localStorage.setItem('trainer', true);

            Trainer.registerTrainerModeListener();
            return ELF.trigger('SITESELECTION:Back', { product: 'logpad' }, this)
            .then(() => {
                expect(Trainer.disableTrainer).not.toHaveBeenCalled();
            })
            .then(() => {
                localStorage.setItem('trainer', false);
            })
            .then(() => ELF.trigger('SITESELECTION:Back', { product: 'sitepad' }, this))
            .then(() => {
                expect(Trainer.disableTrainer).not.toHaveBeenCalled();
            });
        });
    });

    describe('Method: enableTrainer', () => {
        beforeAll(() => {
            spyOn(localStorage, 'setItem').and.callThrough();

            spyOn(trainerWebservice, 'extendCoreWebservice').and.stub();
            spyOn(trainerUtilities, 'extendCoreUtilities').and.stub();
            spyOn(trainerSiteSelectionView, 'extendSiteSelectionView').and.stub();
            spyOn(trainerUnlockCodeView, 'extendUnlockCodeView').and.stub();
            spyOn(trainerSettingsView, 'extendSettingsView').and.stub();
            spyOn(trainerSetTimeZoneActivationView, 'extendSetTimeZoneActivationView').and.stub();
            spyOn(trainerSetTimeZoneView, 'extendSetTimeZoneView').and.stub();
            spyOn(isStudyOnline, 'overrideIsStudyOnline').and.stub();

            spyOn(TrainerUI, 'registerCSS').and.stub();
            spyOn(TrainerUI, 'overrideCoreStrings').and.stub();

            spyOn(Trainer, 'registerMessages').and.stub();
            spyOn(Trainer, 'unRegisterCoreRules').and.stub();
            spyOn(Trainer, 'overrideCoreActions').and.stub();
            spyOn(Trainer, 'registerTrainerRules').and.stub();

            spyOn(TimeTravel, 'displayTimeTravel').and.stub();
        });

        beforeEach(() => {
            localStorage.clear();
        });

        afterEach(() => {
            localStorage.clear();
        });

        it('Should set trainer flag.', () => {
            Trainer.enableTrainer();
            expect(localStorage.setItem).toHaveBeenCalledWith('trainer', true);
            expect(localStorage.getItem('trainer')).toEqual('true');
        });

        it('Should extend CoreWebservice.', () => {
            Trainer.enableTrainer();
            expect(trainerWebservice.extendCoreWebservice).toHaveBeenCalled();
        });

        it('Should extend CoreUtilities.', () => {
            Trainer.enableTrainer();
            expect(trainerUtilities.extendCoreUtilities).toHaveBeenCalled();
        });

        it('Should extend SiteSelectionView.', () => {
            Trainer.enableTrainer();
            expect(trainerSiteSelectionView.extendSiteSelectionView).toHaveBeenCalled();
        });

        it('Should extend UnlockCodeView.', () => {
            Trainer.enableTrainer();
            expect(trainerUnlockCodeView.extendUnlockCodeView).toHaveBeenCalled();
        });

        it('Should extend SettingsView.', () => {
            Trainer.enableTrainer();
            expect(trainerSettingsView.extendSettingsView).toHaveBeenCalled();
        });

        it('Should extend SetTimeZoneActivationView.', () => {
            Trainer.enableTrainer();
            expect(trainerSetTimeZoneActivationView.extendSetTimeZoneActivationView).toHaveBeenCalled();
        });

        it('Should extend SetTimeZoneView.', () => {
            Trainer.enableTrainer();
            expect(trainerSetTimeZoneView.extendSetTimeZoneView).toHaveBeenCalled();
        });

        it('Should override isStudyOnline.', () => {
            Trainer.enableTrainer();
            expect(isStudyOnline.overrideIsStudyOnline).toHaveBeenCalled();
        });

        it('Should register trainer specific CSS classes.', () => {
            Trainer.enableTrainer();
            expect(TrainerUI.registerCSS).toHaveBeenCalled();
        });

        it('Should modify strings for trainer UI.', () => {
            Trainer.enableTrainer();
            expect(TrainerUI.overrideCoreStrings).toHaveBeenCalled();
        });

        it('Should register trainer specific messages.', () => {
            Trainer.enableTrainer();
            expect(Trainer.registerMessages).toHaveBeenCalled();
        });

        it('Should un-register CoreRules.', () => {
            Trainer.enableTrainer();
            expect(Trainer.unRegisterCoreRules).toHaveBeenCalled();
        });

        it('Should override CoreActions.', () => {
            Trainer.enableTrainer();
            expect(Trainer.overrideCoreActions).toHaveBeenCalled();
        });

        it('Should register trainer specific rules.', () => {
            Trainer.enableTrainer();
            expect(Trainer.registerTrainerRules).toHaveBeenCalled();
        });

        it('Should enable time travel.', () => {
            Trainer.enableTrainer();
            expect(TimeTravel.displayTimeTravel).toHaveBeenCalled();
        });
    });

    describe('Method: registerMessages', () => {
        beforeEach(() => {
            notify.MessageRepo.clear();
        });

        afterEach(() => {
            notify.MessageRepo.clear();
        });

        it('Should add all trainer specific messages to MessageRepo.', () => {
            spyOn(notify.MessageRepo, 'add').and.callThrough();

            Trainer.registerMessages();

            expect(notify.MessageRepo.add).toHaveBeenCalled();
            expect(notify.MessageRepo.messages.TRAINER_EXIT_NOTIFICATION).not.toBeUndefined();
        });
    });

    describe('Method: unRegisterCoreRules', () => {
        beforeEach(() => {
            let getDummyRule = () => ({
                id: 'dummyRule',
                trigger: 'DUMMY:TRIGGER',
                resolve: [{ action: $.noop }]
            });

            ELF.rules.clear();

            ELF.rules.add(_.extend(getDummyRule(), { id: 'UserChangeCredentials' }));
            ELF.rules.add(_.extend(getDummyRule(), { id: 'DuplicateSubjectHandling' }));
            ELF.rules.add(_.extend(getDummyRule(), { id: 'GatewayTransmit' }));
            ELF.rules.add(_.extend(getDummyRule(), { id: 'QuestionnaireTransmit' }));
            ELF.rules.add(_.extend(getDummyRule(), { id: 'EditPatientDiarySync' }));
        });

        afterEach(() => {
            ELF.rules.clear();
        });

        it('Should remove specific core rules from ELF engine.', () => {
            spyOn(ELF.rules, 'remove').and.callThrough();

            expect(ELF.rules.find('DuplicateSubjectHandling')).not.toBeUndefined();
            expect(ELF.rules.find('GatewayTransmit')).not.toBeUndefined();
            expect(ELF.rules.find('QuestionnaireTransmit')).not.toBeUndefined();
            expect(ELF.rules.find('EditPatientDiarySync')).not.toBeUndefined();

            Trainer.unRegisterCoreRules();

            expect(ELF.rules.remove).toHaveBeenCalledWith('DuplicateSubjectHandling');
            expect(ELF.rules.remove).toHaveBeenCalledWith('GatewayTransmit');
            expect(ELF.rules.remove).toHaveBeenCalledWith('QuestionnaireTransmit');
            expect(ELF.rules.remove).toHaveBeenCalledWith('EditPatientDiarySync');

            expect(ELF.rules.find('DuplicateSubjectHandling')).toBeUndefined();
            expect(ELF.rules.find('GatewayTransmit')).toBeUndefined();
            expect(ELF.rules.find('QuestionnaireTransmit')).toBeUndefined();
            expect(ELF.rules.find('EditPatientDiarySync')).toBeUndefined();
        });
    });

    describe('Method: registerTrainerRules', () => {
        beforeEach(() => {
            ELF.rules.clear();
        });

        afterEach(() => {
            ELF.rules.clear();
        });

        it('Should add all trainer specific rules to ELF engine.', () => {
            Trainer.registerTrainerRules();

            expect(ELF.rules.all().length).toEqual(4);
            expect(ELF.rules.find('GatewayTransmit')).not.toBeUndefined();
            expect(ELF.rules.find('QuestionnaireTransmit')).not.toBeUndefined();
            expect(ELF.rules.find('ExitSitepadTrainer')).not.toBeUndefined();
            expect(ELF.rules.find('TrainerEditUserCompleted')).not.toBeUndefined();
        });
    });

    describe('Method: overrideCoreActions', () => {
        it('Should override transmitAll action to be noopAction.', () => {
            spyOn(ELF, 'action').and.callThrough();

            Trainer.overrideCoreActions();
            expect(ELF.action).toHaveBeenCalledWith('transmitAll', jasmine.any(Function));
        });

        it('Should override transmitAll action to be noopAction.', () => {
            spyOn(ELF, 'action').and.callThrough();

            Trainer.overrideCoreActions();
            expect(ELF.action).toHaveBeenCalledWith('refreshAllData', jasmine.any(Function));
        });
    });

    describe('Method: disableTrainer', () => {
        beforeEach(() => {
            spyOn(utilities, 'restartApplication').and.stub();
        });

        it('Should return a Q promise.', () => {
            let result = Trainer.disableTrainer();

            expect(result).toBePromise();
        });

        it('Should remove trainer flag.', () => {
            spyOn(localStorage, 'removeItem').and.callThrough();

            Trainer.disableTrainer();
            expect(localStorage.removeItem).toHaveBeenCalledWith('trainer');
        });

        it('Should restart application.', () => {
            Trainer.disableTrainer();
            expect(utilities.restartApplication).toHaveBeenCalled();
        });
    });
});
