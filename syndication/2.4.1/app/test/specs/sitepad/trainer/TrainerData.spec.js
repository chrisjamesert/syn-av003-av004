import TrainerData from 'trainer/sitepad/classes/TrainerData';

TRACE_MATRIX('US6733')
.describe('TrainerUnlockCodeView', () => {
    let testReturnNonEmptyData = (webserviceFunction) => {
        let trainerData = TrainerData.getData(webserviceFunction);

        expect(typeof trainerData).toEqual('object');
        expect(trainerData.res).not.toEqual(undefined);
    };

    let testReturnEmptyData = (webserviceFunction) => {
        let trainerData = TrainerData.getData(webserviceFunction);

        expect(typeof trainerData).toEqual('object');
        expect(trainerData.res).toBeArray();
        expect(trainerData.res.length).toEqual(0);
    };

    it('Should return non empty data.', () => {
        let webserviceNames = [
            'getSites',
            'registerDevice',
            'addUser',
            'updateUserCredentials',
            'sendLogs',
            'sendSubjectAssignment',
            'setSubjectData',
            'sendDiary',
            'updateSubjectData',
            'sendEditPatient',
            'getSetupCode',
            'getDeviceID',
            'updateUser',
            'syncUserVisitsAndReports'
        ];

        webserviceNames.forEach((name) => {
            testReturnNonEmptyData(name);
        });
    });

    it('Should return empty data.', () => {
        let webserviceNames = [
            'getAllSubjects',
            'syncUsers'
        ];

        webserviceNames.forEach((name) => {
            testReturnEmptyData(name);
        });
    });

    it('Should return an Object with lastRefreshTime for "syncUserVisitsAndReports".', () => {
        let trainerData = TrainerData.getData('syncUserVisitsAndReports');

        expect(trainerData.res.lastRefreshTime).toBeTruthy();
    });

    it('Should return undefined for non existing webservice function.', () => {
        let trainerData = TrainerData.getData('NON-EXISTING-WEBSERVICE-NAME');

        expect(trainerData).toEqual(undefined);
    });
});
