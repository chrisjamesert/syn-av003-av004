import WebService from 'core/classes/WebService';
import CurrentContext from 'core/CurrentContext';
import Transmissions from 'core/collections/Transmissions';
import Subjects from 'core/collections/Subjects';
import transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import DailyUnlockCode from 'core/classes/DailyUnlockCode';
import * as lStorage from 'core/lStorage';
import CurrentSubject from 'core/classes/CurrentSubject';
import Spinner from 'core/Spinner';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import { MessageRepo } from 'core/Notify';

TRACE_MATRIX('US7958')
.describe('Transmit:transmitResetCredentials', () => {
    let subject,
        transmissions = new Transmissions(),
        setupCode = '28105953';

    beforeAll(() => {
        resetStudyDesign();
        CurrentContext.init();
        CurrentContext().setContextByRole('admin');
        LF.appName = 'SitePad App';
        LF.security = LF.security || {};
        LF.security.activeUser = new User({ id: 1 });
        LF.spinner = Spinner;
    });

    beforeEach(() => {
        spyOn(Transmissions.prototype, 'destroy').and.callFake((id, callback = $.noop) => {
            callback();
            return Q();
        });
        spyOn(Transmissions.prototype, 'remove').and.callThrough();

        transmissions.add({
            id: 1,
            method: 'transmitResetCredentials',
            params: JSON.stringify({
                krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813',
                password: 'New EncryptedPasswordHere',
                secretQuestion: '1',
                secretAnswer: 'New EncryptedSecretAnswerHere'
            }),
            created: new Date().getTime()
        });

        subject = new Subject({
            id: 1,
            krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813',
            site_code: '267',
            subject_id: '12345',
            subject_number: '54321',
            subject_active: 1,
            secret_question: '0',
            secret_answer: 'Old EncryptedSecretAnswerHere',
            subject_password: 'Old EncryptedPasswordHere',
            service_password: 'Old EncryptedServicePasswordHere'
        });

        spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
            this.add(subject);

            return Q();
        });

        spyOn(lStorage, 'getItem').and.callFake((key) => {
            switch (key) {
                case 'deviceId':
                    return 'DeviceId123456';
                case 'clientId':
                    return 'ClientId123456';
                default:
                    return null;
            }
        });

        spyOn(WebService.prototype, 'getSetupCode').and.resolve(setupCode);
        spyOn(Logger.prototype, 'error').and.stub();
        spyOn(Spinner, 'hide').and.resolve();
        spyOn(Spinner, 'show').and.resolve();
        spyOn(MessageHelpers, 'notifyDialogCreator').and.returnValue($.noop);
    });

    afterEach(() => {
        transmissions.reset();
    });

    describe('Transmission', () => {
        Async.it('should get SetupCode if subject does not have SetupCode.', () => {
            spyOn(WebService.prototype, 'resetSubjectCredentials').and.callThrough();

            return transmitResetCredentials.call(transmissions, transmissions.at(0))
            .finally(() => {
                expect(WebService.prototype.getSetupCode).toHaveBeenCalledWith('SA.ff634d2f7a87eebf47eda9ae99eca813');
            })

            // We expect to reject, handle so the test doesn't automatically fail.
            .catch($.noop);
        });

        Async.it('should call the webservice method with the correct payload and parameters.', () => {
            let expectedUnlockCode = '123456',
                expectedPayload = {
                    setupCode,
                    clientPassword: 'New EncryptedPasswordHere',
                    regDeviceId: 'DeviceId123456',
                    clientId: 'ClientId123456',
                    challengeQuestions: [{
                        question: '1',
                        answer: 'New EncryptedSecretAnswerHere'
                    }],
                    source: 'SitePad'
                },
                expects = [expectedPayload, 'SA.ff634d2f7a87eebf47eda9ae99eca813', expectedUnlockCode];

            spyOn(WebService.prototype, 'resetSubjectCredentials').and.callThrough();
            spyOn(DailyUnlockCode.prototype, 'getCode').and.callFake(() => expectedUnlockCode);

            return transmitResetCredentials.call(transmissions, transmissions.at(0))
            .finally(() => {
                expect(WebService.prototype.resetSubjectCredentials).toHaveBeenCalledWith(...expects);
            })

            // We expect to reject, handle so the test doesn't automatically fail.
            .catch($.noop);
        });

        describe('Unexpected error', () => {
            let transmission;

            Async.beforeEach(() => {
                transmission = transmissions.at(0);

                spyOn(WebService.prototype, 'resetSubjectCredentials').and.reject('Unexpected error');

                return transmitResetCredentials.call(transmissions, transmissions.at(0))

                // We expect to reject, handle so the test doesn't automatically fail.
                .catch($.noop);
            });

            it('should log an error.', () => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('Error in transmitResetCredentials', 'Unexpected error');
            });

            it('should skip transmission.', () => {
                expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                expect(transmissions.length).toBe(0);
            });
        });
    });

    describe('Transmission Success', () => {
        let transmission,
            transmissionId;

        Async.beforeEach(() => {
            transmission = transmissions.at(0);
            transmissionId = transmission.get('id');

            spyOn(CurrentSubject, 'clearSubject').and.stub();
            spyOn(WebService.prototype, 'resetSubjectCredentials').and.resolve({
                res: {
                    password: 'New EncryptedServicePasswordHere'
                },
                syncID: null,
                isSubjectActive: 1,
                isDuplicate: false
            });

            return transmitResetCredentials.call(transmissions, transmissions.at(0))

            // We expect to reject, handle so the test doesn't automatically fail.
            .catch($.noop);
        });

        it('should update the subject credentials', () => {
            expect(subject.get('subject_password')).toBe('New EncryptedPasswordHere');
            expect(subject.get('secret_question')).toBe('1');
            expect(subject.get('secret_answer')).toBe('New EncryptedSecretAnswerHere');
            expect(subject.get('subject_active')).toBe(1);
            expect(subject.get('service_password')).toBe('New EncryptedServicePasswordHere');
        });

        it('should invalidate CurrentSubject cache', () => {
            expect(CurrentSubject.clearSubject).toHaveBeenCalled();
        });

        it('should delete transmission.', () => {
            expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId);
        });
    });

    describe('Transmission Error', () => {
        describe('Subject is not active', () => {
            let transmission,
                transmissionId;

            Async.beforeEach(() => {
                transmission = transmissions.at(0);
                transmissionId = transmission.get('id');

                MessageRepo.add({
                    type: 'Dialog',
                    key: 'PASSWORD_CHANGE_FAILED',
                    message: () => Q()
                });

                spyOn(MessageRepo, 'display').and.callThrough();

                spyOn(WebService.prototype, 'resetSubjectCredentials')
                .and.reject({
                    errorCode: null,
                    httpCode: null,
                    isSubjectActive: 0
                });

                return transmitResetCredentials.call(transmissions, transmissions.at(0))

                // We expect to reject, handle so the test doesn't automatically fail.
                .catch($.noop);
            });

            afterEach(() => {
                MessageRepo.remove({
                    type: 'Dialog',
                    key: 'PASSWORD_CHANGE_FAILED'
                });
            });

            it('Should show notification', () => {
                expect(_.contains(MessageRepo.display.calls.mostRecent().args, 'PASSWORD_CHANGE_FAILED'))
                .toBeTruthy();
            });

            it('Should delete the transmission item after notification', () => {
                expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId);
            });
        });

        describe('Error different than Page Not Found 404', () => {
            let transmission,
                transmissionId;

            beforeEach(() => {
                transmission = transmissions.at(0);
                transmissionId = transmission.get('id');

                MessageRepo.add({
                    type: 'Dialog',
                    key: 'TRANSMISSION_ERROR_HTTP_AND_ERROR',
                    message: () => Q()
                });

                spyOn(MessageRepo, 'display').and.callThrough();
            });

            afterEach(() => {
                MessageRepo.remove({
                    type: 'Dialog',
                    key: 'TRANSMISSION_ERROR_HTTP_AND_ERROR'
                });
            });

            Async.it('should show notification', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials')
                .and.reject({
                    errorCode: null,
                    httpCode: null,
                    isSubjectActive: 1
                });

                return transmitResetCredentials.call(transmissions, transmissions.at(0))
                .finally(() => {
                    expect(_.contains(MessageRepo.display.calls.mostRecent().args, 'TRANSMISSION_ERROR_HTTP_AND_ERROR'))
                    .toBeTruthy();
                })

                // We expect to reject, handle so the test doesn't automatically fail.
                .catch($.noop);
            });

            Async.it('should delete transmission item if Subject is deleted on backend', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials')
                .and.reject({
                    errorCode: '6',
                    httpCode: 403,
                    isSubjectActive: 1
                });

                return transmitResetCredentials.call(transmissions, transmissions.at(0))
                .finally(() => {
                    expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId);
                })

                // We expect to reject, handle so the test doesn't automatically fail.
                .catch($.noop);
            });

            Async.it('should delete transmission item if Subject is not activated on backend', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials')
                .and.reject({
                    errorCode: '0',
                    httpCode: 403,
                    isSubjectActive: 1
                });

                return transmitResetCredentials.call(transmissions, transmissions.at(0))
                .finally(() => {
                    expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId);
                })

                // We expect to reject, handle so the test doesn't automatically fail.
                .catch($.noop);
            });

            Async.it('should skip transmission item if Subject exists on backend ', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials')
                .and.reject({
                    errorCode: '1',
                    httpCode: 403,
                    isSubjectActive: 1
                });

                return transmitResetCredentials.call(transmissions, transmissions.at(0))
                .finally(() => {
                    expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                    expect(transmissions.length).toBe(0);
                })

                // We expect to reject, handle so the test doesn't automatically fail.
                .catch($.noop);
            });
        });

        Async.it('Should skip transmission item on Page Not Found 404 error', () => {
            let transmission = transmissions.at(0);

            spyOn(WebService.prototype, 'resetSubjectCredentials')
            .and.reject({
                errorCode: null,
                httpCode: 404,
                isSubjectActive: 1
            });

            return transmitResetCredentials.call(transmissions, transmissions.at(0))
            .finally(() => {
                expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                expect(transmissions.length).toBe(0);
            })

            // We expect to reject, handle so the test doesn't automatically fail.
            .catch($.noop);
        });
    });
});
