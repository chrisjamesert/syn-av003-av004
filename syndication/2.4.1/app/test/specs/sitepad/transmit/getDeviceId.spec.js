import COOL from 'core/COOL';
import * as Utilities from 'core/utilities';
import WebService from 'core/classes/WebService';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';

import getDeviceId from 'sitepad/transmit/getDeviceId';

describe('Transmit:getDeviceId', () => {
    let subject;

    beforeEach(() => {
        subject = new Subject({
            setupCode: '12345678',
            subject_password: 'password',
            service_password: 'password'
        });

        spyOn(WebService.prototype, 'getDeviceID').and.stub();
        spyOn(Utilities, 'isSitePad').and.returnValue(true);
    });

    Async.it('should not attempt to get the device ID (no service password).', () => {
        subject.set('service_password', null);

        let request = getDeviceId(subject);

        expect(request).toBePromise();

        return request.then(() => {
            expect(WebService.prototype.getDeviceID).not.toHaveBeenCalled();
        });
    });

    Async.it('should not attempt to get the device ID (device ID exists).', () => {
        subject.set('device_id', 12345);

        let request = getDeviceId(subject);

        expect(request).toBePromise();

        return request.then(() => {
            expect(WebService.prototype.getDeviceID).not.toHaveBeenCalled();
        });
    });

    Async.it('should fail to get the device ID.', () => {
        WebService.prototype.getDeviceID.and.reject('HTTP_ERROR');
        spyOn(Logger.prototype, 'error').and.stub();

        let request = getDeviceId(subject);

        expect(request).toBePromise();

        return request.catch((err) => {
            expect(Logger.prototype.error).toHaveBeenCalledWith('Error getting device id', 'HTTP_ERROR');
            expect(err).toEqual('HTTP_ERROR');
        });
    });

    Async.it('should get the device ID.', () => {
        WebService.prototype.getDeviceID.and.resolve({ res: { D: 12345 } });
        spyOn(Subject.prototype, 'save').and.resolve();

        let request = getDeviceId(subject);

        expect(request).toBePromise();

        return request.then(() => {
            expect(Subject.prototype.save).toHaveBeenCalledWith({ device_id: 12345 });
        });
    });
});
