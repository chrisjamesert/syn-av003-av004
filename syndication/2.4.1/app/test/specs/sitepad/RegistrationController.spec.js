import RegistrationController from 'sitepad/controllers/RegistrationController';
import * as lStorage from 'core/lStorage';
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import ModeSelectionView from 'sitepad/views/ModeSelectionView';
import LanguageSelectionView from 'sitepad/views/LanguageSelectionView';
import SiteSelectionView from 'sitepad/views/SiteSelectionView';
import UnlockCodeView from 'sitepad/views/UnlockCodeView';
import Site from 'core/models/Site';

// TODO - Disabled for now.  Causes failures with other unit tests. Maybe due to state changes.
TRACE_MATRIX('US5986').
xdescribe('RegistrationController', function () {

    let controller;

    beforeEach(function () {

        helpers.installDatabase();

        controller = new RegistrationController();

    });

    Async.afterAll(() => helpers.uninstallDatabase());

    it('should have a state property.', function () {

        expect(controller.state).toBeDefined();

    });

    describe('method:modeSelection', function () {

        it('should initiate the ModeSelectionView.', function () {

            controller.modeSelection();

            expect(controller.view).toBeDefined();
            expect(controller.view).toEqual(jasmine.any(ModeSelectionView));

        });

    });

    describe('method:setupUser', function () {

        it('should initiate the BaseQuestionnaireView.', function () {

            controller.setupUser();

            expect(controller.view).toBeDefined();
            expect(controller.view).toEqual(jasmine.any(BaseQuestionnaireView));

        });

    });

    describe('method:languageSelection', function () {

        it('should initiate the LanguageSelectionView.', function () {

            controller.languageSelection();

            expect(controller.view).toBeDefined();
            expect(controller.view).toEqual(jasmine.any(LanguageSelectionView));

        });

    });

    describe('method:siteSelection', function () {

        it('should resolve the SiteSelectionView.', function () {

            controller.siteSelection();

            expect(controller.view).toBeDefined();
            expect(controller.view).toEqual(jasmine.any(SiteSelectionView));

        });

    });

    describe('method:UnlockCodeView', function () {

        beforeEach(function (done) {

            let model = new Site({ siteCode: '0008' }),
                request = model.save();

            request
                .then(() => controller.unlockCode())
                .then(done);

        });

        it('should resolve the UnlockCodeView.', function () {

            expect(controller.view).toBeDefined();
            expect(controller.view).toEqual(jasmine.any(UnlockCodeView));

        });

    });

});
