import Subjects from 'core/collections/Subjects';
import WebService from 'core/classes/WebService';
import COOL from 'core/COOL';
import ELF from 'core/ELF';
import * as action from 'sitepad/actions/getAllSubjects';

describe('Synchronizing All Subjects Data:', () => {
    let studyDesign,
        subjectsToBeReturned = [{
            krpt: 'SA.ceb48a213da18b30c187de5acb0134f9c',
            initials: 'AAA',
            language: 'en-US',
            phase: 30,
            deleted: false,
            spStartDate: '2017-07-04T11:22:07.000-04:00',
            enrollmentDate: '2017-07-04T11:21:26.000-04:00',
            setupCode: '27834418',
            subject_active: 1,
            site_code: '0172',
            subject_number: '0100',
            subject_id: '0172-0100',
            service_password: '93b6aec44ebc2e374385be0ee728571b',
            subject_password: '71a53fc513d5a44700c718b06aca89f19bf7e573c1685525f9c3fd88eeb9049801fca8be0e2d0293cc99e452e6670c909eb8be401947596f2ed85f373a406271',
            secret_answer: '91649befdbfead98474f42d9853c54621ac5c6ca25125cd9963f5f9b45012fa45ed8df8819bb99b246fdc7ca93c115279f6b2c97bdd67acc83ade70a2ce53580',
            phaseStartDateTZOffset: '2017-07-04T18:26:53.000+03:00',
            secret_question: '0'
        }, {
            krpt: 'SA.abe79694e6a793488674a5b38d68260fe',
            initials: 'AAB',
            language: 'en-US',
            phase: 10,
            deleted: false,
            spStartDate: '2017-07-04T12:57:00.000-04:00',
            enrollmentDate: '2017-07-04T12:56:36.000-04:00',
            setupCode: '99843030',
            subject_active: 1,
            site_code: '0172',
            subject_number: '0101',
            subject_id: '0172-0101',
            service_password: '152c934293d8a4b8050844a75026ba9f',
            subject_password: '939f3b199a7689ff1b52e7a7f935c50d6c7f020ebafe3f7113d593778d0234101c79b00a97350e622eee37200c6604681776347df4cfe0d8630b99dc72ac4182',
            secret_answer: '91649befdbfead98474f42d9853c54621ac5c6ca25125cd9963f5f9b45012fa45ed8df8819bb99b246fdc7ca93c115279f6b2c97bdd67acc83ade70a2ce53580',
            phaseStartDateTZOffset: '2017-07-04T12:56:36.000-04:00',
            secret_question: '0'
        }];

    beforeEach(() => {
        spyOn(ELF, 'trigger').and.callThrough();
    });

    Async.it('should fetch all subjects from server for this site', () => {
        spyOn(COOL.getClass('WebService', WebService).prototype, 'getAllSubjects').and.resolve({
            res: subjectsToBeReturned
        });

        // Let's prevent running default built in rules
        ELF.rules.add({
            id: 'PreventSyncSubjectsData',
            trigger: ['HISTORICALDATASYNC:Received/Subjects'],
            salience: 1000,
            resolve: [{
                action: () => ({ stopRules: true })
            }]
        });

        return action.getAllSubjects()
        .then(() => {
            expect(ELF.trigger).toHaveBeenCalledWith('HISTORICALDATASYNC:Received/Subjects', { res: subjectsToBeReturned, collection: jasmine.any(Subjects) });
        })
        .finally(() => {
            ELF.rules.remove('PreventSyncSubjectsData');
        });
    });
});
