import ApplicationController from 'sitepad/controllers/ApplicationController';
import Logger from 'core/Logger';
import Data from 'core/Data';
import Subject from 'core/models/Subject';
import Visit from 'core/models/Visit';
import Questionnaires from 'core/collections/Questionnaires';
import Session from 'core/classes/Session';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import COOL from 'core/COOL';

import FormGatewayView from 'sitepad/views/FormGatewayView';
import LoginView from 'sitepad/views/LoginView';
import SitePadQuestionnaireView from 'sitepad/views/SitePadQuestionnaireView';
import SkipVisitQuestionnaireView from 'sitepad/views/SkipVisitQuestionnaireView';
import HomeView from 'sitepad/views/HomeView';
import VisitGatewayView from 'sitepad/views/VisitGatewayView';
import DeactivatePatientView from 'sitepad/views/DeactivatePatientView';
import SiteUsersView from 'sitepad/views/SiteUsersView';
import SetPasswordView from 'sitepad/views/SetPasswordView';
import SecretQuestionView from 'sitepad/views/SecretQuestionView';
import AccountConfiguredView from 'sitepad/views/AccountConfiguredView';
import NewPatientQuestionnaireView from 'sitepad/views/NewPatientQuestionnaireView';

describe('ApplicationController', () => {
    let controller;

    beforeEach(() => {
        controller = new ApplicationController();

        LF.security = new Session();

        spyOn(LF.security, 'checkLogin').and.callFake(() => Q(true));
        spyOn(COOL.getClass('Utilities'), 'isOnline').and.callFake(() => Q(true));
        spyOn(controller, 'authenticateThenGo').and.callFake(() => Q());
        spyOn(controller, 'go').and.callFake(() => Q());
        spyOn(controller, 'navigate').and.stub();
        spyOn(Logger.prototype, 'error').and.stub();
    });

    xdescribe('method:addSiteUser', () => {
        // @todo Proof of concept view
    });

    xdescribe('method:editSiteUser', () => {
        // @todo Proof of concept view
    });

    describe('method:dashboard', () => {
        xit('should navigate to the home view.', () => {
            controller.dashboard();

            expect(controller.navigate).toHaveBeenCalledWith('home');
        });

        it('should navigate to the visits view.', () => {
            let subject = new Subject({ id: 1 });

            controller.dashboard({ subject });

            expect(controller.navigate).toHaveBeenCalledWith('visits/1');
        });

        Async.it('should fail to invoke the FormGatewayView.', () => {
            let subject = new Subject({ id: 1 });
            let visit = new Visit({ id: 1 });

            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.dashboard({ subject, visit });

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the FormGatewayView.', () => {
            let subject = new Subject({ id: 1 });
            let visit = new Visit({ id: 1 });

            controller.dashboard({ subject, visit });

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('FormGatewayView', FormGatewayView, { subject, visit });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    // TODO: US6599
    xdescribe('method:login', () => {
        Async.it('should fail to invoke the LoginView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.login();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the LoginView.', () => {
            controller.login();

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('LoginView', LoginView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    TRACE_MATRIX('DE22319')
    .describe('method:questionnaire', () => {
        let subject = new Subject({ id: 1 });
        let visit = new Visit({ id: 1 });
        let questionnaireConfig = LF.StudyDesign.questionnaires;

        beforeAll(() => {
            LF.StudyDesign.questionnaires = new Questionnaires([{
                id: 'DAILY',
                SU: 'Daily',
                displayName: 'DISPLAY_NAME',
                className: 'DAILY_DIARY',
                affidavit: 'CustomAffidavit',
                previousScreen: true,
                triggerPhase: 'TREATMENT',
                allowTranscriptionMode: true,
                product: ['sitepad'],
                screens: [],
                branches: []
            }]);
        });

        afterAll(() => {
            LF.StudyDesign.questionnaires = questionnaireConfig;
        });

        Async.it('should fail to invoke the SitePadQuestionnaireView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            return controller.questionnaire('DAILY', { subject, visit })
            .then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the SitePadQuestionnaireView.', () => {
            return controller.questionnaire('DAILY', { subject, visit })
            .then(() => {
                expect(controller.go).toHaveBeenCalledWith('SitePadQuestionnaireView', SitePadQuestionnaireView, {
                    id: 'DAILY',
                    subject,
                    visit
                });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });

        Async.it('should invoke the configured view instead of SitePadQuestionnaireView.', () => {
            let customSitePadQuestionnaireView = class customView extends SitePadQuestionnaireView {};

            COOL.add('customSitePadQuestionnaireView', customSitePadQuestionnaireView);
            LF.StudyDesign.questionnaires.find({ id: 'DAILY' }).set('syndicationClass', 'customSitePadQuestionnaireView');

            return controller.questionnaire('DAILY', { subject, visit })
            .then(() => {
                expect(controller.go).toHaveBeenCalledWith('customSitePadQuestionnaireView', SitePadQuestionnaireView, {
                    id: 'DAILY',
                    subject,
                    visit
                });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:home', () => {
        Async.it('should fail to invoke the HomeView.', () => {
            controller.authenticateThenGo.and.callFake(() => Q.reject('DatabaseError'));
            controller.home();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the HomeView.', () => {
            controller.home();

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('HomeView', HomeView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    // TODO: US6599
    xdescribe('method:visitGateway', () => {
        Async.it('should fail to invoke the VisitGatewayView.', () => {
            controller.authenticateThenGo.and.callFake(() => Q.reject('DatabaseError'));
            controller.visitGateway('1');

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the VisitGatewayView.', () => {
            controller.visitGateway('1');

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('VisitGatewayView', VisitGatewayView, { subjectId: 1 });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    xdescribe('method:editPatient', () => {
        // @todo...
    });

    describe('method:deactivatePatient', () => {
        Async.it('should fail to invoke the DeactivatePatientView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.deactivatePatient();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the DeactivatePatientView.', () => {
            let subject = new Subject({ id: 1 });
            controller.deactivatePatient(subject);

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('DeactivatePatientView', DeactivatePatientView, { subject });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:siteUsers', () => {
        Async.it('should fail to invoke the SiteUsersView.', () => {
            controller.authenticateThenGo.and.callFake(() => Q.reject('DatabaseError'));
            controller.siteUsers();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the SiteUsersView.', () => {
            controller.siteUsers();

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('SiteUsersView', SiteUsersView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:addNewPatient', () => {
        Async.it('should fail to invoke the BaseQuestionnaireView.', () => {
            controller.authenticateThenGo.and.reject('DatabaseError');
            controller.addNewPatient();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the BaseQuestionnaireView.', () => {
            controller.addNewPatient();

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('NewPatientQuestionnaireView', NewPatientQuestionnaireView, {
                    id: 'New_Patient',
                    showCancel: true
                });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:skipVisits', () => {
        let subject = new Subject({ id: 1 });
        let visit = new Visit({ id: 1 });

        Async.it('should fail to invoke the SkipVisitQuestionnaireView.', () => {
            controller.authenticateThenGo.and.callFake(() => Q.reject('DatabaseError'));
            controller.skipVisits({ subject });

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the SkipVisitQuestionnaireView.', () => {
            controller.skipVisits({ subject, visit });

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('SkipVisitQuestionnaireView', SkipVisitQuestionnaireView, {
                    id: 'Skip_Visits',
                    subject,
                    visit,
                    showCancel: true,
                    cancelPopupStyle: 'skipVisit'
                });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:questionnaireCompletion', () => {
        Async.it('should fail to invoke the QuestionnaireCompletionView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.questionnaireCompletion();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the QuestionnaireCompletionView.', () => {
            controller.questionnaireCompletion();

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('QuestionnaireCompletionView', QuestionnaireCompletionView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:setPassword', () => {
        Async.it('should fail to invoke the SetPasswordView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.setPassword();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the SetPasswordView.', () => {
            controller.setPassword();

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('SetPasswordView', SetPasswordView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:setSecretQuestion', () => {
        Async.it('should fail to invoke the SecretQuestionView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.setSecretQuestion();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the SecretQuestionView.', () => {
            controller.setSecretQuestion();

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('SecretQuestionView', SecretQuestionView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:accountConfigured', () => {
        Async.it('should fail to invoke the AccountConfiguredView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.accountConfigured();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the AccountConfiguredView.', () => {
            controller.accountConfigured();

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('AccountConfiguredView', AccountConfiguredView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:timeConfirmation', () => {
        Async.it('should fail to invoke the Time Confirmation BaseQuestionnaireView.', () => {
            controller.go.and.callFake(() => Q.reject('DatabaseError'));
            controller.timeConfirmation();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the Time Confirmation BaseQuestionnaireView.', () => {
            controller.timeConfirmation();

            return Q().then(() => {
                expect(controller.go).toHaveBeenCalledWith('BaseQuestionnaireView', BaseQuestionnaireView, {
                    id: 'Time_Confirmation'
                });
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });
});
