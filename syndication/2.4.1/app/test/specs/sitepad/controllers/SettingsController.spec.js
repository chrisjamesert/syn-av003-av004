import SettingsController from 'sitepad/controllers/SettingsController';
import Logger from 'core/Logger';

import AboutView from 'sitepad/views/AboutView';
import SettingsView from 'sitepad/views/SettingsView';

describe('SettingsController', () => {
    let controller;

    beforeEach(() => {
        controller = new SettingsController();

        spyOn(controller, 'authenticateThenGo').and.callFake(() => Q());
        spyOn(Logger.prototype, 'error').and.stub();
    });

    describe('method:index', () => {
        Async.it('should fail to invoke the SettingsView.', () => {
            controller.authenticateThenGo.and.callFake(() => Q.reject('DatabaseError'));
            controller.index();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the SettingsView.', () => {
            controller.index();

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('SettingsView', SettingsView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });

    describe('method:about', () => {
        Async.it('should fail to invoke the AboutView.', () => {
            controller.authenticateThenGo.and.callFake(() => Q.reject('DatabaseError'));
            controller.about();

            return Q().then().then(() => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('DatabaseError');
            });
        });

        Async.it('should invoke the AboutView.', () => {
            controller.about();

            return Q().then(() => {
                expect(controller.authenticateThenGo).toHaveBeenCalledWith('AboutView', AboutView);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    });
});
