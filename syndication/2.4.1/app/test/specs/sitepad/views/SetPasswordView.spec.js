/**
 * Created by dvukman on 2/16/2016.
 */
import SetPasswordView from 'sitepad/views/SetPasswordView';
import CurrentContext from 'core/CurrentContext';
import Role from 'core/models/Role';
import * as utilities from 'core/utilities';

import 'test/helpers/StudyDesign';

describe('SetPasswordView', () => {
    let view,
        user,
        template =
            '<div id="set-password-template">' +
            '<div id="content">' +
            '<div id="wrapper">' +
            '<input type="password" name="txtNewPassword" id="txtNewPassword" class="form-control" value=""/>' +
            '<input type="password" name="txtConfirmPassword" id="txtConfirmPassword" class="form-control" value=""/>' +
            '</div>' +
            '</div>' +
            '</div>';

    Async.beforeEach(() => {
        $('body').append(template);
        CurrentContext.init();
        user = CurrentContext().get('users').add({
            id: 1,
            language: 'fr-CA',
            username: 'Justin Trudeau',
            role: 'subject'
        });

        LF.StudyDesign.roles.reset([{
            id: 'site',
            passwordInputType: 'number'
        }, {
            id: 'subject',
            passwordInputType: 'password'
        }]);

        return user.save()
        .then(() => CurrentContext().setContextByUser(user))
        .then(() => {
            view = new SetPasswordView();
            spyOn(view, 'reload').and.callFake($.noop);
        })
        .then(() => view.resolve());
    });

    afterEach(() => {
        LF.StudyDesign.roles.reset();
        $('#set-password-template').remove();
    });

    Async.it('should have an id.', () => {
        return view.render()
        .then(() => expect(view.id).toBe('set-password'));
    });

    Async.it('should call makeWindowsNumericInputCover if numeric type input is set for password.', () => {
        let spy = spyOn(utilities, 'makeWindowsNumericInputCover').and.stub();
        CurrentContext().get('user').set({ role: 'site' });
        LF.Wrapper.platform = 'windows';

        return view.render()
        .then(() => {
            expect(spy).toHaveBeenCalled();
            expect(spy.calls.count()).toBe(2);
        });
    });

    describe('method:validate', () => {
        Async.it('should verify that next button gets enabled when passwords match', () => {
            let spy = spyOn(view, 'inputSuccess');

            return view.render()
            .then(() => {
                view.$('#txtNewPassword').val('1234');
                view.$('#txtConfirmPassword').val('1234');
                view.validatePasswords({ preventDefault: $.noop });
            })
            .then(() => expect(spy).toHaveBeenCalled());
        });

        Async.it('should verify that next button gets disabled when passwords match', () => {
            let spy = spyOn(view, 'inputError');

            return view.render()
            .then(() => {
                view.$('#txtNewPassword').val('2211');
                view.$('#txtConfirmPassword').val('3344');
                view.validatePasswords({ preventDefault: $.noop });
            })
            .then(() => expect(spy).toHaveBeenCalled());
        });
    });
});
