import SiteUsersView from 'sitepad/views/SiteUsersView';

import Sites from 'core/collections/Sites';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import Role from 'core/models/Role';
import Site from 'core/models/Site';
import ELF from 'core/ELF';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class SiteUsersViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        this.removeConfirmTemplate = specHelpers.renderTemplateToDOM('core/templates/confirm.ejs');
        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/site-users.ejs');

        this.user = new User({
            id: 1,
            username: 'admin'
        });

        this.role = new Role({
            id: 'site',
            displayName: 'SITE_USER'
        });

        // jscs:disable requireArrowFunctions
        // Long hand functions used in this case to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });

        spyOn(Users.prototype, 'fetch').and.callFake(function () {
            this.add(this.user);

            return Q();
        });
        // jscs:enable requireArrowFunctions

        LF.security = {
            activeUser: this.user,
            getUserRole: () => this.role
        };

        this.view = new SiteUsersView();

        return this.view.resolve()
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeConfirmTemplate();
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context = {}) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SiteUsersViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should fail to resolve.', () => {
                Sites.prototype.fetch.and.callFake(() => Q.reject('DatabaseError'));

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Method resolve should have been rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should resolve.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.site).toEqual(jasmine.any(Site));
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Method render should have been rejected.'))
                    .catch(e => expect(e).toBe('DOMError'));
            });

            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });
        });
    }

    testSelect () {
        TRACE_MATRIX('US4668').describe('method:select', () => {
            beforeEach(() => {
                spyOn(this.view, 'enableButton').and.stub();
                spyOn(this.view, 'disableButton').and.stub();
            });

            it('should enable the deactivate button.', () => {
                let selected = new User({ id: 2, active: 1 });

                this.user.set('active', 1);

                this.view.select(selected);

                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$deactivate);
                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$edit);

                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$activate);
            });

            it('should disable the deactivate button.', () => {
                this.user.set('active', 1);

                this.view.select(this.user);

                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$deactivate);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$edit);

                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$activate);
            });

            it('should disable the deactivate button (selected user disabled).', () => {
                this.user.set('active', 0);

                this.view.select(this.user);

                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$deactivate);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$edit);

                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$activate);
            });
        });
    }

    testDeselect () {
        describe('method:deselect', () => {
            it('should deselect the user.', () => {
                spyOn(this.view, 'disableButton').and.stub();
                this.view.selected = this.user;

                this.view.deselect();

                expect(this.view.selected).toBe(undefined);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$edit);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$deactivate);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$activate);
            });
        });
    }

    testTransmit () {
        describe('method:transmit', () => {
            Async.it('should trigger a rule and reload.', () => {
                spyOn(ELF, 'trigger').and.resolve({ });
                spyOn(this.view, 'reload').and.stub();

                let request = this.view.transmit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('USERMANAGEMENT:Transmit', { }, this.view);
                    expect(this.view.reload).toHaveBeenCalled();
                });
            });
        });
    }

    testDeactivate () {
        describe('method:deactivate', () => {
            // Not yet supported.
            it('should be defined.', () => {
                expect(this.view.deactivate).toBeDefined();
            });
        });
    }

    testGoBack () {
        describe('method:goBack', () => {
            it('should navigate back to the home view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.goBack();

                expect(this.view.navigate).toHaveBeenCalledWith('home');
            });
        });
    }

    testEditUser () {
        TRACE_MATRIX('US4668').describe('method:editUser', () => {
            Async.it('should navigate to the edit user view.', () => {
                spyOn(ELF, 'trigger').and.resolve({ });
                spyOn(this.view, 'navigate').and.stub();

                this.view.selected = this.user;

                let request = this.view.editUser();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('edit-user/1');
                });
            });
        });
    }

    testAddUser () {
        describe('method:addNewUser', () => {
            Async.it('should navigate to the add new user view.', () => {
                spyOn(ELF, 'trigger').and.resolve({ });
                spyOn(this.view, 'navigate').and.stub();

                let request = this.view.addNewUser();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('add-site-user');
                });
            });
        });
    }
}

describe('SiteUsersView', () => {
    let suite = new SiteUsersViewSuite();

    suite.executeAll({
        id: 'site-users-view',
        template: '#site-users-tpl',
        button: '#back',
        dynamicStrings: {
            username: 'admin',
            siteCode: '0222',
            appVersion: '2.0.0',
            studyVersionVal: '1.0.0',
            protocolVal: 'TestProtocol',
            sponsorVal: 'ERT'
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            'testTransmitRetry',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testShowInputError',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
