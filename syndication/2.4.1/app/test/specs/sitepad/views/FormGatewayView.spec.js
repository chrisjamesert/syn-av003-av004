import FormGatewayView from 'sitepad/views/FormGatewayView';

import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import CurrentContext from 'core/CurrentContext';
import Session from 'core/classes/Session';

import Site from 'core/models/Site';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Questionnaire from 'core/models/Questionnaire';
import FormRow from 'sitepad/models/FormRow';

import Sites from 'core/collections/Sites';
import UserVisits from 'sitepad/collections/UserVisits';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import { getCurrentProtocol } from 'core/Helpers';

describe('FormGatewayView', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();

        LF.appName = 'SitePad App';
        LF.security = new Session();
        LF.security.activeUser = new User({
            id: 1,
            role: 'admin'
        });

        CurrentContext.init();
        CurrentContext().setContextByRole('admin');

        // jscs:disable requireArrowFunctions
        // Long hand functions used in this case to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        spyOn(Subject.prototype, 'fetch').and.callFake(function () {
            this.set({
                id: 1,
                subject_id: '100-001',
                phase: 10,
                initials: 'SA',
                lastVisit: new Date()
            });

            return Q();
        });

        let visit = LF.StudyDesign.visits.at(1);
        let subject = new Subject({
            id: 1,
            subject_id: '100-001',
            phase: 10,
            initials: 'SA',
            lastVisit: new Date()
        });

        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/form-gateway.ejs');
        view = new FormGatewayView({ subject, visit });

        spyOn(view, 'renderTable').and.stub();
        spyOn(window.localStorage, 'setItem').and.stub();
        spyOn(window.localStorage, 'getItem').and.stub();
        spyOn(window.localStorage, 'removeItem').and.stub();
        spyOn(view, 'reload').and.stub();
        spyOn(view, 'navigate').and.stub();
        spyOn(view, 'i18n').and.callFake(value => Q(value));
        spyOn(LF.router, 'navigate').and.stub();

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('begin-visit');
    });

    describe('method:select', () => {
        it('should enable the start and skip buttons.', () => {
            spyOn(view, 'enableButton').and.stub();

            let row = new FormRow({ status: 'available' });
            view.select(row);

            expect(view.selected).toEqual(jasmine.any(FormRow));
            expect(view.enableButton).toHaveBeenCalledWith('#start-form, #skip-form');
        });

        it('should disabled the start and skip buttons.', () => {
            spyOn(view, 'deselect').and.stub();

            let row = new FormRow({ status: 'unavailable' });
            view.select(row);

            expect(view.selected).toEqual(jasmine.any(FormRow));
            expect(view.deselect).toHaveBeenCalled();
        });
    });

    describe('method:deselect', () => {
        it('should disable the start and skip buttons.', () => {
            spyOn(view, 'disableButton');

            view.selected = LF.StudyDesign.questionnaires.at(1);

            view.deselect();

            expect(view.selected).toBeFalsy();
            expect(view.disableButton).toHaveBeenCalledWith('#start-form, #skip-form');
        });
    });

    describe('method:skip', () => {
        it('should be defined.', () => {
            expect(view.skip).toBeDefined();
        });
    });

    describe('method:resolve', () => {
        Async.it('should resolve.', () => {
            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.site).toEqual(jasmine.any(Site));
            });
        });

        Async.it('should be rejected.', () => {
            Sites.prototype.fetch.and.callFake(() => Q.reject('DatabaseError'));

            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => fail('Method resolve should have been rejected.'))
                .catch(e => expect(e).toBe('DatabaseError'));
        });
    });

    describe('method:render', () => {
        Async.it('should render.', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.renderTable).toHaveBeenCalled();
            });
        });

        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));
            view.renderTable.calls.reset();

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
                .catch(() => expect(view.renderTable).not.toHaveBeenCalled());
        });

        TRACE_MATRIX('DE21111')
        .describe('DE21111 Unit Test', () => {
            Async.it('should render for user having subject as role with username hidden and patient-info unhidden.', () => {
                spyOn(view, 'hideElement').and.stub();
                spyOn(view, 'showElement').and.stub();

                LF.security.activeUser.set('role', 'subject');

                let request = view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(view.hideElement).toHaveBeenCalledWith('#site-user-image,#site-user-info,#site-user-visits,.ellipsis-overflow');
                    expect(view.showElement).toHaveBeenCalledWith('#patient-image,#patient-info,#patient-visits');
                });
            });
        });

        TRACE_MATRIX('DE21172')
        .describe('DE21172 Unit Test', () => {
            it('should render patient.png for users having role as subject', () => {
                spyOn(view, 'getImageIconUrl').and.stub();

                LF.security.activeUser.set('role', 'subject');
                let request = view.render();

                expect(request).toBePromise();

                expect(view.getImageIconUrl).toHaveBeenCalledWith('subject');
            });
        });
    });

    describe('method:checkVisitStatus', () => {
        Async.it('should navigate to the visit gateway if visit status is complete.', () => {
            spyOn(MessageRepo, 'display').and.resolve();
            spyOn(UserVisits.prototype, 'fetch').and.callFake(function () {
                this.add({
                    visitId: view.visit.id,
                    subject_id: view.subject.get('id'),
                    subjectKrpt: view.subject.get('krpt'),
                    state: LF.VisitStates.COMPLETED,
                    dateModified: new Date()
                });
                return Q();
            });

            let request = view.checkVisitStatus();

            expect(request).toBePromise();

            return request.then((res) => {
                expect(view.navigate).toHaveBeenCalledWith('visits/1', true, { visit: view.visit });
            });
        });

        Async.it('should navigate to the visit gateway (rejected).', () => {
            spyOn(UserVisits.prototype, 'fetch').and.callFake(function () {
                this.add({
                    visitId: view.visit.id,
                    subject_id: view.subject.get('id'),
                    subjectKrpt: view.subject.get('krpt'),
                    state: LF.VisitStates.COMPLETED,
                    dateModified: new Date()
                });
                return Q();
            });

            view.i18n.and.callFake(() => Q.reject());

            let request = view.checkVisitStatus();

            expect(request).toBePromise();

            return request.then((res) => {
                expect(view.navigate).toHaveBeenCalledWith('visits/1', true, { visit: view.visit });
            });
        });
    });

    TRACE_MATRIX('DE16263')
    .describe('method:start', () => {
        Async.it('should start the questionnaire.', () => {
            spyOn(ELF, 'trigger').and.callFake(() => Q({}));
            spyOn(view, 'openQuestionnaire').and.stub();

            view.selected = LF.StudyDesign.questionnaires.at(0);

            let request = view.start();

            expect(request).toBePromise();

            return request.then(() => {
                expect(ELF.trigger).toHaveBeenCalledWith('DASHBOARD:OpenQuestionnaire/Daily_Diary', {
                    questionnaire: view.selected
                }, view);
                expect(view.openQuestionnaire).toHaveBeenCalledWith('Daily_Diary');
            });
        });

        Async.it('should not start the questionnaire (preventDefault).', () => {
            spyOn(ELF, 'trigger').and.callFake(() => Q({ preventDefault: true }));
            spyOn(view, 'openQuestionnaire').and.stub();

            view.selected = LF.StudyDesign.questionnaires.at(0);

            let request = view.start();

            expect(request).toBePromise();

            return request.then(() => {
                expect(ELF.trigger).toHaveBeenCalledWith('DASHBOARD:OpenQuestionnaire/Daily_Diary', {
                    questionnaire: view.selected
                }, view);
                expect(view.openQuestionnaire).not.toHaveBeenCalledWith({ questionnaire_id: 'Daily_Diary' });
            });
        });

        Async.it('should not start the questionnaire (reject).', () => {
            spyOn(ELF, 'trigger').and.callFake(() => Q.reject('Error'));
            spyOn(view, 'openQuestionnaire').and.stub();

            view.selected = LF.StudyDesign.questionnaires.at(0);

            let request = view.start();

            expect(request).toBePromise();

            return request.then(() => fail('Method start should have been rejected.'))
                .catch(() => {
                    expect(view.openQuestionnaire).not.toHaveBeenCalled();
                });
        });
    });

    describe('method:openQuestionnaire', () => {
        Async.it('should open the questionnaire (w/params).', () => {
            let request = view.openQuestionnaire('Daily_Diary');

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('questionnaire/Daily_Diary', true, {
                    subject: view.subject,
                    visit: view.visit
                });
            });
        });

        Async.it('should open the questionnaire (w/o params).', () => {
            view.selected = LF.StudyDesign.questionnaires.at(0);
            let request = view.openQuestionnaire();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('questionnaire/Daily_Diary', true, {
                    subject: view.subject,
                    visit: view.visit
                });
            });
        });
    });

    describe('method:back', () => {
        it('should navigate back to the visit gateway.', () => {
            view.back();

            expect(view.navigate).toHaveBeenCalledWith('visits/1', true, { visit: view.visit });
        });

        // TODO: US6599
        Async.xit('should navigate to the login view.', () => {
            // spyOn(ConfirmView.prototype, 'show').and.callFake(() => Q());
            LF.security.activeUser.set('role', 'subject');

            view.back();

            // .then()x2 to add an appropriate number of events to the queue...
            return Q().then().then().then(() => {
                expect(view.navigate).toHaveBeenCalledWith('login');
            });
        });
    });
});
