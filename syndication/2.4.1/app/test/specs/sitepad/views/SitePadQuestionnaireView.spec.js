import SitePadQuestionnaireView from 'sitepad/views/SitePadQuestionnaireView';
import Transmission from 'core/models/Transmission';
import Session from 'core/classes/Session';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import * as lStorage from 'core/lStorage';
import Dashboard from 'core/models/Dashboard';
import { createEndVisitReport } from 'sitepad/visits/visitUtils';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Visit from 'core/models/Visit';

describe('SitePadQuestionnaireView', () => {
    let view;

    beforeEach(function () {

        view = new SitePadQuestionnaireView({
            id: 'EQ5D',
            SU: 'EQ5D',
            displayName: 'DISPLAY_NAME',
            className: 'EQ5D',
            screens: ['EQ5D_S_1', 'EQ5D_S_2', 'EQ5D_S_3']
        });

        LF.security = new Session();
        LF.security.activeUser = new User({
            id: 1,
            role: 'admin',
            username: 'aaaa'
        });

        view.subject = new Subject({
            id: 2,
            subject_id: '500',
            krpt: '123456',
            custom10: '2'
        });

        view.data.started = new Date();
        view.data.dashboard = new Dashboard({
            battery_level: '100'
        });

        lStorage.setItem('deviceId', '123456');

    });

    afterEach(function () {
        view = undefined;
    });

    TRACE_MATRIX('US6009').
    describe('method:createStartVisitReport', () => {
        Async.it('should resolve to promise', () => {
            let spy = spyOn(Transmission.prototype, 'save');
            let request = view.createStartVisitReport('10', false, null);

            expect(request).toBePromise();

            return request.then(() => {
                expect(spy).toHaveBeenCalled();
            });
        });
    });

    TRACE_MATRIX('US6009').
    describe('method:createEndVisitReport', () => {
        Async.it('should resolve to promise', () => {
            let spy = spyOn(Transmission.prototype, 'save'),
                visit = new Visit({ id: 1, studyEventId: '10'}),
                request = createEndVisitReport({
                    visitStateCode: 1,
                    visit,
                    skippedReason: null,
                    visitStartDate: LF.Utilities.timeStamp(new Date()),
                    subject: view.subject,
                    instanceOrdinal: BaseQuestionnaireView.getInstanceOrdinal(),
                    ink: null
                });

            expect(request).toBePromise();

            return request.then(() => {
                expect(spy).toHaveBeenCalled();
            });
        });
    });
});
