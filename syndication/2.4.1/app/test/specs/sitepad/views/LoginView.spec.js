import LoginView from 'sitepad/views/LoginView';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import Sites from 'core/collections/Sites';
import * as lStorage from 'core/lStorage';
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import COOL from 'core/COOL';
import Data from 'core/Data';

// TODO: US6599
TRACE_MATRIX('US5917').
xdescribe('LoginView', () => {
    const templateId = 'login-tpl';
    let view,
        template = `<div id="${templateId}">
            <select id="siteUser" name="user" data-placeholder="Select User"></select>
            <input id="password" name="password" type="password" placeholder="{{ password }}..."/>
            <button id="login" type="submit" disabled> Login </button>
            </div>`;

    Async.beforeEach(() => {

        $('body').append(template);

        helpers.installDatabase();

        helpers.createUsers();
        helpers.createSite();
        const myUsers = new Users();

        return myUsers.clear()
            .then(() => {
                return Q.allSettled([helpers.saveUsers(), helpers.saveSite()]);
            })
            .then(() => {
                return myUsers.fetch();
            })
            .then(() => {
                expect(myUsers.length).toBe(Data.Users.length);
            })
            .then(() => {

                view = new LoginView();

                spyOn(view, 'reload').and.callFake($.noop);

                return view.resolve();
            })
            .then(() => {
                return view.render();
            });
    });

    afterEach(() => $(`#${templateId}`).remove());

    it('should have an id.', () => {
        expect(view.id).toBe('login-view');
    });

    describe('method:renderSiteUsers', () => {

        Async.it('should add each site user to the UI.', () => {

            spyOn(view, 'addUser');
            return view.renderSiteUsers().then(() => {
                expect(view.addUser.calls.count()).toBe(Data.Users.length);
                expect(view.addUser).toHaveBeenCalledWith(jasmine.any(User));
            });
        });
    });

    describe('method:siteChanged', () => {

        Async.it('should clear password field when site is changed', () => {

            const spy = spyOn(view, 'disableButton');
            return view.renderSiteUsers()
                .then(() => {
                    view.siteUserChanged();
                    expect(view.$password.val()).toEqual('');
                    expect(spy).toHaveBeenCalled();
                });
        });
    });

    describe('method:validate', () => {

        Async.it('should verify that login button gets enabled when password is entered', () => {

            const spy = spyOn(view, 'enableButton');

            view.$siteUser.val(Data.Users.at(0).get('id')).trigger('change');
            view.$password.val('aaa');
            view.validate();
            expect(spy).toHaveBeenCalled();
            return Q();
        });

        Async.it('should verify that login button gets disabled when password is deleted', () => {
            const spyEnableButton = spyOn(view, 'enableButton'),
                spyDisableButton = spyOn(view, 'disableButton');
            view.$siteUser.val(Data.Users.at(0).get('id')).trigger('change');
            view.$password.val('aaa');
            view.validate();
            expect(spyEnableButton).toHaveBeenCalled();
            view.$password.val('');
            view.validate();
            expect(spyDisableButton).toHaveBeenCalled();
            return Q();
        });
    });

    describe('method:login', () => {

        Async.it('should verify that login fails when password is wrong', () => {
            const spyDisableButton = spyOn(view, 'disableButton');

            LF.security = {
                /**
                 * fake invalid login, by returning a Promise<null>
                 * @returns {Q.Promise<null>}
                 */
                login: () => {
                    return Q();
                }
            };

            view.$siteUser.val(Data.Users.at(0).get('id')).trigger('change');
            view.$password.val('wrong password');
            return view.login()
                .then(() => {
                    expect(spyDisableButton).toHaveBeenCalled();
                });
        });

    });
});

