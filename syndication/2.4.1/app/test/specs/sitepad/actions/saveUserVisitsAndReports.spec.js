import { saveUserVisitsAndReports } from 'sitepad/actions/saveUserVisitsAndReports';
import UserReports from 'sitepad/collections/UserReports';
import UserVisits from 'sitepad/collections/UserVisits';
import * as SpecHelpers from 'test/helpers/SpecHelpers';
import Logger from 'core/Logger';
import * as DateTimeUtil from 'core/DateTimeUtil';
import StudyDesign, { resetStudyDesign } from 'test/helpers/StudyDesign';

TRACE_MATRIX('US6812').

describe('saveUserVisitsAndReports', () => {
    const res = {
        subjects: [{
            krpt: 'krpt.39ed120a0981231',
            visit: [{
                eventId: '10',
                report: [{
                    krsu: 'MEDS',
                    sigOrig: 'SU.437882.1',
                    completedTS: '2016-06-21T09:17:32',
                    phaseId: '10',
                    deleted: '0'
                }]
            }, {
                eventId: '20',
                report: [{
                    krsu: 'DAILY_DIARY',
                    sigOrig: 'SU.437882.2',
                    completedTS: '2016-06-21T09:17:32',
                    phaseId: '10',
                    deleted: '0'
                }]
            }]
        }]
    };

    SpecHelpers.clearCollections(UserReports);
    const collection = new UserReports(),
        save = (res) => {
            return saveUserVisitsAndReports({ res, collection });
        };

    beforeAll(() => {
        SpecHelpers.createSubject();
        SpecHelpers.saveSubject();
        resetStudyDesign();

        spyOn(Logger.prototype, 'info').and.resolve();
    });

    afterAll(() => {
        resetStudyDesign();
    });

    Async.it('Should save newly received reports to UserReport collection', () => {
        return save(res)
        .then(() => {
            expect(collection.length).toBe(2);
        })
        .catch(fail);
    });

    Async.it('Should skip received reports if they exist', () => {
        return save(res)
        .then(() => {
            expect(Logger.prototype.info).toHaveBeenCalledWith('Updating the existing user report record: Meds for patient: 12345 in visit: visit1 with sigorig: SU.437882.1');
            expect(Logger.prototype.info).toHaveBeenCalledWith('Updating the existing user report record: Daily_Diary for patient: 12345 in visit: visit2 with sigorig: SU.437882.2');
        })
        .catch(fail);
    });

    Async.it('Should discard reports for a visit that does not exist in a visits configuration', () => {
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '99',
                    report: [{
                        krsu: 'MEDS',
                        sigOrig: 'SU.437882.1',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        deleted: '0'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => {
            expect(Logger.prototype.info).toHaveBeenCalledWith('Unknown visit for record with SU: MEDS and sigorig: SU.437882.1');
        })
        .catch(fail);
    });

    Async.it('Should discard reports for a form that does not exist in study configuration or in visit', () => {
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '20',
                    report: [{
                        krsu: 'NODIARY',
                        sigOrig: 'SU.437882.1',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        deleted: '0'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => {
            expect(Logger.prototype.info).toHaveBeenCalledWith('Unknown questionnaire or not a part of visit for report record with SU: NODIARY for visit: 20 with sigorig: SU.437882.1');
        })
        .catch(fail);
    });

    Async.it('Should delete matching reports from UserReport collection', () => {
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '10',
                    report: [{
                        krsu: 'MEDS',
                        sigOrig: 'SU.437882.1',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        deleted: '1'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => {
            expect(Logger.prototype.info).toHaveBeenCalledWith('Deleting user report record: Meds for patient: 12345 in visit: visit1 with sigorig: SU.437882.1');
            expect(collection.length).toBe(1);
        })
        .catch(fail);
    });

    Async.it('Should create a new UserVisit record when received VisitStart report', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '10',
                    report: [{
                        krsu: 'VisitStart',
                        sigOrig: 'SU.437882.3',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        startDate: '2016-06-21T09:17:32',
                        deleted: '0'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then(() => {
            expect(userVisits.length).toBe(1);
            expect(Logger.prototype.info).toHaveBeenCalledWith('Visit Start form received. Creating new user visit record for patient: 12345 and visit: visit1 with sigorig: SU.437882.3');
        })
        .catch(fail);
    });

    Async.it('Should update UserVisit record when VisitStart report is changed', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '20',
                    report: [{
                        krsu: 'VisitStart',
                        sigOrig: 'SU.437882.3',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        startDate: '2017-01-01T01:11:11',
                        deleted: '0'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then((visits) => {
            expect(userVisits.length).toBe(1);
            expect(visits[0].get('studyEventId')).toEqual('20');
            expect(visits[0].get('dateStarted')).toEqual('2017-01-01T01:11:11');
            expect(Logger.prototype.info).toHaveBeenCalledWith('Visit Start form received. Updating the existing user visit record for patient: 12345 and visit: visit2 with sigorig: SU.437882.3');
        })
        .catch(fail);
    });

    Async.it('Should delete UserVisit record if VisitStart was deleted', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '20',
                    report: [{
                        krsu: 'VisitStart',
                        sigOrig: 'SU.437882.3',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        startDate: '2017-01-01T01:11:11',
                        deleted: '2'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then((visits) => {
            expect(userVisits.length).toBe(0);
            expect(Logger.prototype.info).toHaveBeenCalledWith('Visit Start form deleted. Deleting user visit record for patient: 12345 and visit: visit2 with sigorig: SU.437882.3');
        })
        .catch(fail);
    });

    Async.it('Should create a new UserVisit record when received VisitEnd report', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '10',
                    report: [{
                        krsu: 'VisitEnd',
                        sigOrig: 'SU.437882.4',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '10',
                        endDate: '2016-06-21T09:17:32',
                        deleted: '0',
                        status: '3'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then((visits) => {
            expect(userVisits.length).toBe(1);
            expect(visits[0].get('state')).toEqual(LF.VisitStates.COMPLETED);
            expect(Logger.prototype.info).toHaveBeenCalledWith('Visit End form received. Creating new user visit record for patient: 12345 and visit: visit1 with sigorig: SU.437882.4');
        })
        .catch(fail);
    });

    Async.it('Should update UserVisit record when VisitEnd report is changed', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '20',
                    report: [{
                        krsu: 'VisitEnd',
                        sigOrig: 'SU.437882.4',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '20',
                        endDate: '2011-01-01T01:11:11',
                        deleted: '0',
                        status: '1'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then((visits) => {
            expect(userVisits.length).toBe(1);
            expect(visits[0].get('state')).toEqual(LF.VisitStates.SKIPPED);
            expect(visits[0].get('studyEventId')).toEqual('20');
            expect(visits[0].get('dateEnded')).toEqual('2011-01-01T01:11:11');
            expect(Logger.prototype.info).toHaveBeenCalledWith('Visit End form received. Updating the existing user visit record for patient: 12345 and visit: visit2 with sigorig: SU.437882.4');
        })
        .catch(fail);
    });

    Async.it('Should update UserVisit record to AVAILABLE when VisitEnd report has no VisitStatus', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '20',
                    report: [{
                        krsu: 'VisitEnd',
                        sigOrig: 'SU.437882.4',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '20',
                        endDate: '2011-01-01T01:11:11',
                        deleted: '0'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then((visits) => {
            expect(userVisits.length).toBe(1);
            expect(visits[0].get('state')).toEqual(LF.VisitStates.COMPLETED);
            expect(visits[0].get('studyEventId')).toEqual('20');
            expect(visits[0].get('dateEnded')).toEqual('2011-01-01T01:11:11');
            expect(Logger.prototype.info).toHaveBeenCalledWith('Visit End form received. Updating the existing user visit record for patient: 12345 and visit: visit2 with sigorig: SU.437882.4');
        })
        .catch(fail);
    });

    Async.it('Should update UserVisit record to IN PROGRESS if VisitEnd was deleted', () => {
        let userVisits = new UserVisits();
        const res = {
            subjects: [{
                krpt: 'krpt.39ed120a0981231',
                visit: [{
                    eventId: '20',
                    report: [{
                        krsu: 'VisitEnd',
                        sigOrig: 'SU.437882.4',
                        completedTS: '2016-06-21T09:17:32',
                        phaseId: '20',
                        endDate: '2011-01-01T01:11:11',
                        deleted: '2'
                    }]
                }]
            }]
        };

        return save(res)
        .then(() => userVisits.fetch())
        .then((visits) => {
            expect(userVisits.length).toBe(1);
            expect(visits[0].get('state')).toEqual(LF.VisitStates.IN_PROGRESS);
        })
        .catch(fail);
    });
});
