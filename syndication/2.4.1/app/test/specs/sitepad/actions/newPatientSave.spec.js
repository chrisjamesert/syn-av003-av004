import ELF from 'core/ELF';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Transmissions from 'core/collections/Transmissions';
import Subjects from 'core/collections/Subjects';
import Data from 'core/Data';
import Site from 'core/models/Site';
import User from 'core/models/User';
import CurrentContext from 'core/CurrentContext';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import setupSitepadMessages from 'sitepad/resources/Messages';
import { MessageRepo } from 'core/Notify';
import * as helpers from 'test/helpers/SpecHelpers';
import * as action from 'sitepad/actions/newPatientSave';
import * as rules from 'sitepad/resources/Rules';
import * as DateTimeUtil from 'core/DateTimeUtil';
import * as lStorage from 'core/lStorage';


TRACE_MATRIX('US6527')
.describe('newPatientSave', () => {
    let view,
        serial = '9876543210ABCDEF',
        tzSwId = 21,
        customSubjectPhaseSet = false,
        siteData = { siteCode: '0001', site_id: 'DOM.287206.246' },
        userData = { username: 'batman' },
        answers = [{
            user_id: 1,
            question_id: 'ADD_PATIENT_ID',
            questionnaire_id: 'New_Patient',
            response: '0001-0172',
            SW_Alias: 'Add_Patient.0.ADD_PATIENT_ID'
        }, {
            user_id: 1,
            question_id: 'ADD_PATIENT_INITIALS',
            questionnaire_id: 'New_Patient',
            response: 'AAA',
            SW_Alias: 'Add_Patient.0.ADD_PATIENT_INITIALS'
        }, {
            user_id: 1,
            question_id: 'ADD_PATIENT_LANGUAGE',
            questionnaire_id: 'New_Patient',
            response: '{"language":"en-US"}',
            SW_Alias: 'Add_Patient.0.ADD_PATIENT_LANGUAGE'
        }, {
            user_id: 1,
            question_id: 'AFFIDAVIT',
            questionnaire_id: 'New_Patient',
            response: '1',
            SW_Alias: 'AFFIDAVIT'
        }, {
            question_id: 'SYSVAR',
            questionnaire_id: 'New_Patient',
            response: '1988',
            SW_Alias: 'DOB'
        }, {
            user_id: 1,
            question_id: 'ADD_PATIENT_PASSWORD',
            questionnaire_id: 'New_Patient',
            response: '{"password":"aaaa"}',
            SW_Alias: 'Add_Patient.0.ADD_PATIENT_PASSWORD'
        }, {
            user_id: 1,
            question_id: 'ADD_PATIENT_SALT',
            questionnaire_id: 'New_Patient',
            response: '{"salt":"temp"}',
            SW_Alias: 'Add_Patient.0.ADD_PATIENT_SALT'
        }, {
            user_id: 1,
            question_id: 'ADD_PATIENT_ROLE',
            questionnaire_id: 'New_Patient',
            response: '{"role":"subject"}',
            SW_Alias: 'Add_Patient.0.ADD_PATIENT_ROLE'
        }];

    Async.beforeAll(() => {
        resetStudyDesign();
        localStorage.clear();
        setupSitepadMessages();
        CurrentContext.init();

        Data.Questionnaire = view;
        Data.site = new Site(siteData);
        LF.security = LF.security || {};
        LF.security.activeUser = new User(userData);
        LF.appName = 'SitePad App';

        view = new BaseQuestionnaireView({
            id: 'New_Patient',
            showCancel: true
        });
        view.resolve();
        view.data.answers.add(answers);

        lStorage.setItem('IMEI', serial);
        lStorage.setItem('tzSwId', tzSwId);
        helpers.installDatabase();

        ELF.rules.add({
            id: 'customPhaseDeterminationTest',
            trigger: 'QUESTIONNAIRE:New_Patient/DETERMINE_PHASE',
            resolve: [{
                action: () => {
                    customSubjectPhaseSet = true;
                }
            }]
        });

        return action.newPatientSave.call(view, {
            questionnaire: view.id
        });
    });

    Async.afterAll(() => {
        MessageRepo.clear();
        return helpers.uninstallDatabase()
        .then(localStorage.clear)
        .then(resetStudyDesign);
    });

    describe('custom subject phase', () => {
        it('should have set subject phase via custom rule', () => {
            expect(customSubjectPhaseSet).toBeTruthy();
        });
    });

    describe('create transmission', () => {
        let transmissions = new Transmissions(),
            subjects = new Subjects();

        Async.it('should save Transmission record in the database.', () => {
            return transmissions.fetch()
            .then(() => transmissions.count())
            .then((res) => {
                expect(res).toBe(1);
            });
        });

        Async.it('should save a new Subject.', () => {
            return subjects.fetch()
            .then(() => subjects.count())
            .then((res) => {
                expect(res).toBe(1);
            });
        });

        it('should save Transmission with the correct parameters.', () => {
            let transmission = transmissions.at(0),
                subject = subjects.at(0),
                params = JSON.parse(transmission.get('params'));

            expect(transmission.get('method')).toBe('transmitSubjectAssignment');
            expect(transmission.get('token')).toBe('');
            expect(params.krpt).toBe(subject.get('krpt'));
            expect(params.responsibleParty).toBe(userData.username);
            expect(params.dateStarted).toBe(view.data.started.ISOStamp());
            expect(params.reportDate).toBe(DateTimeUtil.convertToDate(view.data.started));
            expect(params.subjectAssignmentPhase).toBe(LF.StudyDesign.subjectAssignmentPhase);
            expect(params.phaseStartDate).toBe(DateTimeUtil.timeStamp(view.data.started));
            expect(params.sigID).toBe(`SA.${view.data.started.getTime().toString(16)}${serial}`);
            expect(params.initials).toBe('AAA');
            expect(params.patientId).toBe('0001-0172');
            expect(params.enrollDate).toBe(DateTimeUtil.timeStamp(view.data.started));
            expect(params.TZValue).toBe('21');
            expect(params.language).toBe('en_US');
            expect(params.affidavit).toBe('AFFIDAVIT');
            expect(params.SPStartDate).toBeDefined();
            expect(params.customSysVars).toBeDefined();
        });

        it('should save Subject with SPStartDate field', () => {
            let subject = subjects.at(0);
            expect(subject.get('spStartDate')).toBeDefined();
        });
    });
});
