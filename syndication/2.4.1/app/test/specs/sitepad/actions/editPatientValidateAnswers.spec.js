import ELF from 'core/ELF';
import EditPatientQuestionnaireView from 'sitepad/views/EditPatientQuestionnaireView';
import * as action from 'sitepad/actions/editPatientValidateAnswers';
import Answers from 'core/collections/Answers';
import Questionnaire from 'core/models/Questionnaire';
import Subject from 'core/models/Subject';
import User from 'core/models/User';
import { MessageRepo } from 'core/Notify';
import setupSitepadMessages from 'sitepad/resources/Messages';
import Spinner from 'core/Spinner';

describe('editPatientValidateAnswers', () => {
    let view,
        subjectData = { id: 1, subject_id: '0001-0172', krpt: 'SA.1234567890', initials: 'AAA', user: 1 },
        userData = { id: 1, username: 'batman', language: 'en-US' },
        sameAnswers = new Answers([{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            response: subjectData.subject_id,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            response: subjectData.initials,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'Assignment.0.Language',
            question_id: 'EDIT_PATIENT_LANGUAGE',
            questionnaire_id: 'Edit_Patient',
            response: '{"language":"en-US"}',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.Initials.Comment',
            question_id: 'EDIT_PATIENT_REASON',
            questionnaire_id: 'Edit_Patient',
            response: 'NoReason',
            subject_id: subjectData.subject_id
        }]),
        changedAnswers = new Answers([{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            response: '0001-0200',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            response: 'BBB',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'Assignment.0.Language',
            question_id: 'EDIT_PATIENT_LANGUAGE',
            questionnaire_id: 'Edit_Patient',
            response: '{"language":"en-US"}',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.Initials.Comment',
            question_id: 'EDIT_PATIENT_REASON',
            questionnaire_id: 'Edit_Patient',
            response: 'ReasonIsRelative',
            subject_id: subjectData.subject_id
        }]);

    beforeAll(() => {
        setupSitepadMessages();
    });

    beforeEach(() => {
        view = {
            id: 'Edit_Patient',
            data: {},
            subject: new Subject(subjectData),
            user: new User(userData),
            navigate: $.noop,
            defaultRouteOnExit: 'home',
            defaultFlashParamsOnExit: {}
        };

        spyOn(Spinner, 'hide').and.resolve();
        spyOn(view, 'navigate');
    });

    Async.it('should not show the "no change dialog" if some answers are changed.', () => {
        view.data.answers = changedAnswers;

        spyOn(MessageRepo, 'display').and.resolve();

        return action.editPatientValidateAnswers.call(view)
        .then(() => {
            expect(MessageRepo.display).not.toHaveBeenCalled();
        });
    });

    Async.it('should show the "no change dialog" if none of the answers are changed.', () => {
        view.data.answers = sameAnswers;

        spyOn(MessageRepo, 'display').and.resolve();

        return action.editPatientValidateAnswers.call(view)
        .then(() => {
            expect(MessageRepo.display).toHaveBeenCalledWith(MessageRepo.Dialog[`${view.id}_NO_CHANGES`]);
        });
    });

    Async.it('should exit diary if user clicks OK on the "no change dialog".', () => {
        view.data.answers = sameAnswers;

        spyOn(MessageRepo, 'display').and.resolve();

        return action.editPatientValidateAnswers.call(view)
        .then((flags) => {
            expect(flags.preventDefault).toEqual(true);
            expect(flags.stopRules).toEqual(true);
            expect(view.navigate).toHaveBeenCalledWith(view.defaultRouteOnExit, true, view.defaultFlashParamsOnExit);
        });
    });

    Async.it('should not exit diary if user clicks CANCEL on the "no change dialog".', () => {
        view.data.answers = sameAnswers;

        spyOn(MessageRepo, 'display').and.reject();

        return action.editPatientValidateAnswers.call(view)
        .then((flags) => {
            expect(flags.preventDefault).toEqual(true);
            expect(flags.stopRules).toEqual(true);
            expect(view.navigate).not.toHaveBeenCalledWith(view.defaultRouteOnExit, true, view.defaultFlashParamsOnExit);
        });
    });
});
