import WebService from 'sitepad/classes/WebService';
import CoreWebService from 'core/classes/WebService';
import * as lStorage from 'core/lStorage';

class WebServiceSuite {
    constructor () {
        beforeEach(() => this.beforeEach());
    }

    beforeEach () {
        spyOn(lStorage, 'getItem').and.stub();
        this.service = new WebService();
    }

    testBeforeSend () {
        describe('method:beforeSend', () => {
            let xhr;

            beforeEach(() => {
                spyOn(CoreWebService.prototype, 'beforeSend');
                xhr = jasmine.createSpyObj('xhr', ['setRequestHeader']);
            });

            it('should do nothing.', () => {
                this.service.beforeSend({});

                expect(xhr.setRequestHeader).not.toHaveBeenCalled();
            });

            it('should set the X-Sync-API-Token header.', () => {
                lStorage.getItem.and.returnValue('12345');

                this.service.beforeSend({ xhr });

                expect(xhr.setRequestHeader).toHaveBeenCalledWith('X-Sync-API-Token', '12345');
            });
        });
    }
}

describe('WebService', () => {
    let suite = new WebServiceSuite();

    suite.testBeforeSend();
});