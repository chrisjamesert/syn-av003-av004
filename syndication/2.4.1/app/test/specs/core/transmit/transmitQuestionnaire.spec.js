import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import Transmissions from 'core/collections/Transmissions';
import Subjects from 'core/collections/Subjects';
import Subject from 'core/models/Subject';
import WebService from 'core/classes/WebService';
import transmitQuestionnaire from 'core/transmit/transmitQuestionnaire';
import COOL from 'core/COOL';

TRACE_MATRIX('US7392')
.describe('transmitQuestionnaire', () => {
    let studyDesign,
        service = COOL.getClass('WebService', WebService),
        answer = {
            id: 1,
            subject_id: '12345',
            response: '7',
            SW_Alias: 'EQ5D_Q_1',
            instance_ordinal: 1,
            questionnaire_id: 'EQ5D',
            question_id: 'EQ5D_Q_1'
        },
        expectedPayload = {
            I: jasmine.any(Number),
            U: undefined,
            S: undefined,
            C: jasmine.any(String),
            R: undefined,
            O: jasmine.any(Number),
            P: undefined,
            T: undefined,
            B: 'false',
            D: undefined,
            E: jasmine.any(String),
            V: undefined,
            L: undefined,
            J: undefined,
            A: [{
                G: '7',
                F: 'EQ5D_Q_1',
                Q: 'EQ5D_Q_1'
            }]
        },
        answersWithJsonh = [3, 'G', 'F', 'Q', '7', 'EQ5D_Q_1', 'EQ5D_Q_1'];

    beforeEach(() => {
        let now = new Date();

        studyDesign = LF.StudyDesign;
        LF.StudyDesign = LF.StudyDesign || {};
        LF.StudyDesign.jsonh = true;

        LF.appName = 'LogPad App';

        spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
            this.add([{
                id: 1,
                method: 'transmitQuestionnaire',
                params: JSON.stringify({ dashboardId: 1, sigId: 'LA.14f7031e9cc352136068949490' }),
                created: now.getTime()
            }]);

            return Q();
        });

        spyOn(Dashboards.prototype, 'destroy').and.resolve();
        spyOn(Dashboards.prototype, 'fetch').and.callFake(function () {
            this.add([{
                id: 1,
                subject_id: '12345',
                instance_ordinal: 1,
                questionnaire_id: 'EQ5D',
                completed: now.ISOStamp(),
                diary_id: now.getTime(),
                completed_tz_offset: now.getOffset()
            }]);

            return Q();
        });

        spyOn(Subjects, 'getSubjectBy').and.resolve(new Subject({ id: 1, subject_id: '123', krpt: 'krpt' }));
        spyOn(Subjects.prototype, 'save').and.resolve();

        // spyOn(service.prototype, 'sendDiary').and.resolve({
        //     res: null,
        //     syncID: null,
        //     isSubjectActive: 1,
        //     isDuplicate: false
        // });

        spyOn(service.prototype, 'sendDiary').and.callFake((a, b, c) => {
            return Q({
                res: null,
                syncID: null,
                isSubjectActive: 1,
                isDuplicate: false
            });
        });
    });

    afterEach(() => {
        LF.StudyDesign = studyDesign;
    });

    Async.it('should not send T parameter if there is no type set for any answer.', () => {
        spyOn(Answers.prototype, 'fetch').and.callFake(function () {
            this.add([answer]);

            return Q();
        });

        return Transmissions.fetchCollection()
        .then(transmissions => transmitQuestionnaire.call(transmissions, transmissions.at(0)))
        .then(() => {
            let payload = _.extend(expectedPayload, { A: answersWithJsonh });

            expect(service.prototype.sendDiary).toHaveBeenCalledWith(payload, undefined, jasmine.any(Boolean));
        });
    });

    Async.it('should not use JSONH compression send T parameter if there is exists type value set for any answer.', () => {
        spyOn(Answers.prototype, 'fetch').and.callFake(function () {
            this.add([_.extend(answer, { type: 'typeValue' })]);

            return Q();
        });

        return Transmissions.fetchCollection()
        .then(transmissions => transmitQuestionnaire.call(transmissions, transmissions.at(0)))
        .then(() => {
            let payload = _.extend(expectedPayload, { A: [jasmine.any(Object)] });

            expect(service.prototype.sendDiary).toHaveBeenCalledWith(payload, undefined, false);
        });
    });

    Async.it('should send T parameter if there is exists type value set for any answer.', () => {
        spyOn(Answers.prototype, 'fetch').and.callFake(function () {
            this.add([_.extend(answer, { type: 'typeValue' })]);

            return Q();
        });

        return Transmissions.fetchCollection()
        .then(transmissions => transmitQuestionnaire.call(transmissions, transmissions.at(0)))
        .then(() => {
            let payloadAnswer = _.extend(expectedPayload.A[0], { T: 'typeValue' }),
                payload = _.extend(expectedPayload, { A: [payloadAnswer] });

            expect(service.prototype.sendDiary).toHaveBeenCalledWith(payload, undefined, jasmine.any(Boolean));
        });
    });
});
