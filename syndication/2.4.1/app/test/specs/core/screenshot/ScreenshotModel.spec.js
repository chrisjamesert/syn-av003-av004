import ScreenshotModel, { ScreenshotSelectionModel } from 'core/screenshot/ScreenshotConfigModel';
import ScreenshotSetup from 'core/screenshot/screenshotSetupHelper'
import * as StudyDesignHelpers from 'test/helpers/StudyDesign';

TRACE_MATRIX('US6500').
describe('Screenshot Configuration Models', () => {
    const SITEPAD_PRODUCT = 'sitepad';
    const LOGPAD_PRODUCT = 'logpad';

    afterEach(StudyDesignHelpers.resetStudyDesign);

    describe('ScreenshotSelectionModel', () => {
        it('should create the correct defaults', () => {
            const model = new ScreenshotSelectionModel();

            expect(model.toJSON()).toEqual({
                languages: [],
                modes: [],
                diaries: [],
                core_screens: [],
                messageboxes: []
            });
        });
    });

    describe('ScreenshotConfigModel', () => {
        describe('diaryList', () => {
            const diaryList = [{
                id: 'Test1',
                product: [SITEPAD_PRODUCT]
            }, {
                id: 'Test2',
                product: [LOGPAD_PRODUCT]
            }, {
                id: 'Test3'
            }, {
                id: 'P_DatePicker_Screenshot_Diary',
                product: [LOGPAD_PRODUCT]

            }, {
                id: 'P_DatePicker_Screenshot_Diary_SitePad',
                product: [SITEPAD_PRODUCT]

            }];

            const filterDiaryList = product => _.filter(diaryList, diary => {
                return diary.product ? _.contains(diary.product, product) : true;
            });

            const testDiaryList = product => () => {
                const setupHelper = new ScreenshotSetup({ product })
                LF.StudyDesign.assign(diaryList, 'questionnaires');
                const model = new ScreenshotModel({ diaryList: setupHelper.getDiaryList() });

                const diaryIdList = _.invoke(model.diaryList, 'get', 'id');
                const testIdList = _.map(filterDiaryList(product), diary => diary.id);

                // expect all items in testIdList to exist in diaryIdList.
                expect(_.difference(testIdList, diaryIdList).length).toBe(0);

                //expect testIdList to be the same as diaryIdList for a particular product.
                expect(testIdList).toEqual(diaryIdList);
            };

            it('should create the correct diary list with SitePad product', testDiaryList(SITEPAD_PRODUCT));
            it('should create the correct diary list with LogPad product', testDiaryList(LOGPAD_PRODUCT));
        });

        describe('changing mode', () => {
            let model;

            beforeEach(() => {
                model = new ScreenshotModel();
            });

            it('nextMode should increment mode', () => {
                model.nextMode();
                expect(model.get('modeIndex')).toBe(1);
                model.nextMode();
                expect(model.get('modeIndex')).toBe(2);
            });

            it('prevMode should decrement mode', () => {
                model.set('modeIndex', 5);
                model.prevMode();
                expect(model.get('modeIndex')).toBe(4);
                model.prevMode();
                expect(model.get('modeIndex')).toBe(3);
            });

        });

    });

});
