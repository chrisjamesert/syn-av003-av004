import ScreenshotController from 'core/screenshot/ScreenshotController';
import ScreenshotTool from 'core/screenshot/ScreenshotTool';
import ScreenshotSetupHelper from 'core/screenshot/screenshotSetupHelper';
import ScreenshotUtils from 'core/screenshot/screenshotUtils';
import RouterBase from 'core/classes/RouterBase';
import BaseController from 'core/controllers/BaseController';
import SettingsController from 'logpad/controllers/SettingsController';

import { MessageRepo } from 'core/Notify';
import NotifyView from 'core/views/NotifyView';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import * as DetectFeatures from 'core/screenshot/detectFeatures';
import Templates from 'core/collections/Templates';
import { templates } from 'test/specs/core/views/BaseQuestionnaireViewTestData';

const testDesign = {
    questionnaires: [{
        id: 'test1',
        screens: [
            'test_screen_1',
            'test_screen_2',
            'test_screen_3'
        ]
    }, {
        id: 'test2',
        product: ['logpad'],
        screens: [
            'test_screen_1',
            'test_screen_2',
            'test_screen_3'
        ]
    }, {
        id: 'test3',
        product: ['sitepad'],
        screens: [
            'test_screen_1',
            'test_screen_2',
            'test_screen_3'
        ]
    }],
    screens: [{
        id: 'test_screen_1',
        questions: [{ id: 'test_q' }]
    }, {
        id: 'test_screen_2',
        questions: [{ id: 'test_q' }]
    }, {
        id: 'test_screen_3',
        questions: [{ id: 'test_q' }]
    }],
    questions: [{
        id: 'test_q'
    }]
};

const fakeDiaryTestDesign = {
    questionnaires: [{
        id: 'P_DatePicker_Screenshot_Diary',
        SU: 'Date',
        displayName: 'DISPLAY_NAME',
        className: 'DATEPICKER_WIDGET_DIARY',
        affidavit: undefined,
        product: ['logpad'],
        screens: [
            'DATEPICKER_DIARY_S_1'
        ]
    }],
    screens: [{
        id: 'DATEPICKER_DIARY_S_1',
        className: 'DATEPICKER_DIARY_S_1',
        questions: [
            { id: 'DATEPICKER_DIARY_Q_1', mandatory: true }
        ]
    }],
    questions: [{
        id: 'DATEPICKER_DIARY_Q_1',
        IG: 'Date',
        IT: 'DATE_1',
        text: ['QUESTION_1'],
        className: 'DATEPICKER_DIARY_Q_1',
        widget: {
            id: 'DATEPICKER_DIARY_W_1',
            type: 'DatePicker',
            className: 'DATEPICKER_DIARY_W_1',
            configuration: {
                maxDays: 180,
                useAnimation: false,
                defaultValue: '2014-01-01'
            }
        }
    }]
};

const fakeDetector = ({
    takeScreenshot = $.noop,
    render = $.noop,
    cleanup = $.noop
}) => {
    return Q()
    .then(() => render())
    .then(() => takeScreenshot())
    .then(() => cleanup());
};

TRACE_MATRIX('US6500')
.describe('ScreenshotTool', () => {
    let screenshotTool,
        controller;

    const init = (product = 'logpad') => {
        const screenshotSetup = new ScreenshotSetupHelper({ product });

        controller = new ScreenshotController({
            diaryList: screenshotSetup.getDiaryList(product)
        });

        spyOn(controller, 'navigate').and.callFake((location) => {
            controller.clear();
        });
        screenshotTool = new ScreenshotTool({ delay: 0, controller });
    };

    beforeAll(() => {
        LF.appName = 'LogPad App';
    });

    afterAll(() => {
        LF.appName = null;
    });

    Async.beforeEach(() => {
        ScreenshotSetupHelper.fakeData();
        LF.security = jasmine.createSpyObj('security', ['set']);
        resetStudyDesign();
        LF.templates = new Templates(templates);
        init();

        spyOn(screenshotTool, 'delayIt').and.resolve();

        return specHelpers.initializeContent();
    });

    TRACE_MATRIX('US7375')
    .describe('addScreenShotStrings', () => {
        it('should call addScreenShotStrings when product is LogPad', () => {
            spyOn(ScreenshotUtils, 'addScreenShotStrings').and.callFake($.noop);
            spyOn(LF.Wrapper, 'exec').and.callFake(({ execWhenWrapped }) => {
                execWhenWrapped();
            });
            controller.screenshot();
            expect(ScreenshotUtils.addScreenShotStrings).toHaveBeenCalled();

            // @TODO: test through logpad/../screenshotSetup instead.
            // expect(controller.strings.header).toEqual('APPLICATION_HEADER');
        });

        it('should call addScreenShotStrings when product is SitePad', () => {
            spyOn(ScreenshotUtils, 'addScreenShotStrings').and.callFake($.noop);
            spyOn(LF.Wrapper, 'exec').and.callFake(({ execWhenWrapped }) => {
                execWhenWrapped();
            });
            init('sitepad');
            controller.screenshot();
            expect(ScreenshotUtils.addScreenShotStrings).toHaveBeenCalled();

            // @TODO: test through sitepad/../screenshotSetup instead.
            // expect(controller.strings.header).toEqual('APP_NAME');
        });
    });

    describe('showNotification', () => {
        Async.it('should show a notification', () => {
            let opts;
            spyOn(NotifyView.prototype, 'show').and.callFake((options) => {
                opts = options;
                return Q();
            });

            const expectedOpts = {
                header: '',
                body: 'Screenshots capture is complete.',
                ok: 'OK',
                type: 'success'
            };

            return screenshotTool.showNotification()
                .then(() => {
                    expect(opts).toEqual(expectedOpts);
                });
        });
    });

    describe('takeDiaryScreenshots', () => {
        let loopDiaryScreensCalled = [];

        const setup = () => {
            resetStudyDesign();
            LF.StudyDesign.questionnaires.reset(testDesign.questionnaires);
            LF.StudyDesign.screens.reset(testDesign.screens);
            spyOn(screenshotTool, 'prepDiary').and.callFake($.noop);
            spyOn(screenshotTool, 'showNotification').and.callFake($.noop);

            spyOn(screenshotTool, 'loopDiaryScreens')
                .and.callFake((diary, language) => loopDiaryScreensCalled.push({ diary, language }));
        };

        const testProduct = product => () => {
            const langList = ['en-US', 'some-language'];
            init(product);
            setup();

            controller.model.get('selection').set('diaries', controller.model.diaryList);
            controller.model.get('selection').set('languages', langList);

            return screenshotTool.takeDiaryScreenshots()
                .then(() => {
                    // A diary is expected for each language.
                    const expectedDiaryIds = langList.reduce((list, lang) => {
                        const getProduct = (q) => {
                            return q.product ? _.contains(q.product, product) : true;
                        };

                        const diaryIds = _
                            .filter(testDesign.questionnaires, getProduct)
                            .map(q => q.id);

                        return list.concat(diaryIds);
                    }, []);

                    const testIdList = _.invoke(_.map(loopDiaryScreensCalled, 'diary'), 'get', 'id');

                    // expect all items in expectedDiaryIds to exist in testIdList.
                    expect(_.difference(expectedDiaryIds, testIdList).length).toBe(0);

                    expect(_.uniq(_.map(loopDiaryScreensCalled, 'language')))
                        .toEqual(langList);
                })
                .catch(err => fail(err));
        };

        beforeEach(() => {
            setup();
            loopDiaryScreensCalled = [];
        });

        Async.it('should take screenshots for all LogPad Diaries once per langauge', testProduct('logpad'));
        Async.it('should take screenshots for all SitePad Diaries once per language', testProduct('sitepad'));
    });

    describe('prepDiary', () => {
        Async.it('should prepare the QuestionnaireView', () => {
            LF.StudyDesign.questionnaires.reset(testDesign.questionnaires);
            LF.StudyDesign.screens.reset(testDesign.screens);
            LF.StudyDesign.questions.reset(testDesign.questions);

            return screenshotTool.prepDiary(testDesign.questionnaires[0])
                .then(() => {
                    // prepScreens
                    expect(controller.view.data.screens.length)

                    // +1 for affidavit
                        .toBe(testDesign.questionnaires[0].screens.length + 1);

                    // prepQuestions
                    expect(controller.view.data.questions.length)

                    // +1 for affidavit
                        .toBe(testDesign.questions.length + 1);

                    // configureAffidavit
                    expect(_.last(controller.view.data.screens).get('id'))
                        .toBe('AFFIDAVIT');

                    // prepAnswerData
                    expect(controller.view.answers)
                        .toBeDefined();
                });
        });
    });

    describe('loopDiaryScreens', () => {
        Async.it('should render each screen for the given diary', () => {
            const renderedScreens = [];
            const screenshotsTaken = [];
            const screenId = testDesign.questionnaires[0].id;

            LF.StudyDesign.questionnaires.reset(testDesign.questionnaires);
            LF.StudyDesign.screens.reset(testDesign.screens);
            LF.StudyDesign.questions.reset(testDesign.questions);
            spyOn(ScreenshotUtils, 'takeScreenshot').and.callFake(({ filename }) => screenshotsTaken.push(filename));
            spyOn(DetectFeatures, 'runDetectors').and.callFake(({ takeScreenshot, render }) => render().then(() => takeScreenshot()));

            return screenshotTool.prepDiary(screenId)
                .then(() => spyOn(controller.view, 'render').and.callFake(screenId => renderedScreens.push(screenId)))
                .then(() => screenshotTool.loopDiaryScreens(screenId, 'en-US', 0))
                .then(() => {
                    expect(renderedScreens)
                        .toEqual(_.map(testDesign.screens, 'id').concat('AFFIDAVIT'));
                    expect(screenshotsTaken.length)
                        .toBe(testDesign.screens.length + 1);
                });
        });

        Async.it('should render each screen for the fake diary', () => {
            const renderedScreens = [];
            const screenshotsTaken = [];
            let selectTagOpenCount = 0;
            const screenId = fakeDiaryTestDesign.questionnaires[0].id;
            LF.StudyDesign.questionnaires.reset(fakeDiaryTestDesign.questionnaires);
            LF.StudyDesign.screens.reset(fakeDiaryTestDesign.screens);
            LF.StudyDesign.questions.reset(fakeDiaryTestDesign.questions);
            spyOn(ScreenshotUtils, 'takeScreenshot').and.callFake(({ filename }) => screenshotsTaken.push(filename));

            $('body').append(`<div id="questionnaire-template">
            <div id="questionnaire"></div>
            </div>`
            );
            return screenshotTool.prepDiary(screenId)
                .then(() => spyOn(controller.view, 'render').and.callThrough())
                .then(() => screenshotTool.loopDiaryScreens(screenId, 'en-US', 0))
                .then(() => {
                    expect(screenshotsTaken.length)
                        .toBe(12);
                });
        }, 15000);
    });

    TRACE_MATRIX('US6501')
    .describe('takeMessageScreenshots', () => {
        const message1 = {
            type: 'Banner',
            key: 'TEST_1',
            message: jasmine.createSpy('message1').and.callFake(({ afterShow = $.noop }) => afterShow())
        };
        const message2 = {
            type: 'Banner',
            key: 'TEST_2',
            message: jasmine.createSpy('message2').and.callFake(({ afterShow = $.noop }) => afterShow())
        };
        const message3 = {
            type: 'Dialog',
            key: 'TEST_3',
            message: jasmine.createSpy('message3').and.callFake(({ afterShow = $.noop }) => afterShow())
        };

        beforeEach(() => {
            spyOn(screenshotTool, 'showNotification').and.callFake($.noop);
            MessageRepo.clear();
            MessageRepo.add(message1, message2, message3);
        });

        afterEach(() => MessageRepo.clear());

        Async.it('should loop through each message in the MessageRepo system and take a screenshot', () => {
            const screenshotsTaken = [];

            spyOn(ScreenshotUtils, 'takeScreenshot').and.callFake(({ filename }) => screenshotsTaken.push(filename));
            spyOn(DetectFeatures, 'runDetectors').and.callFake(fakeDetector);
            const langList = ['en-US', 'fr-CA'];

            controller.model.get('selection').set('languages', langList);

            return screenshotTool.takeMessageScreenshots(0)
                .then(() => {
                    // each message rendered once for each language
                    expect(message1.message.calls.count()).toBe(langList.length);
                    expect(message2.message.calls.count()).toBe(langList.length);
                    expect(message3.message.calls.count()).toBe(langList.length);

                    expect(screenshotsTaken.length).toBe(langList.length * 3);

                    const expectedMessageIds = langList.reduce((list) => {
                        return list.concat([
                            `${message1.type}_${message1.key}`,
                            `${message2.type}_${message2.key}`,
                            `${message3.type}_${message3.key}`
                        ]);
                    }, []);

                    expect(screenshotsTaken).toEqual(expectedMessageIds);
                });
        });
    });

    TRACE_MATRIX('US6500')
    .describe('takeCoreScreenshots', () => {
        let oldRouter,
            testController;

        const trueFilter = item => item === true;

        const testList = [
            'screenshotTest#test1',
            'screenshotTest#test2',
            'screenshotTest#test3'
        ];

        const TestRouter = class extends RouterBase {
            constructor (options = {}) {
                const routes = {
                    test1: 'screenshotTest#test1',
                    test2: 'screenshotTest#test2',
                    test3: 'screenshotTest#test3',
                    support: 'settings#support'
                };
                super(options);
            }
        };

        const TestController = class extends BaseController {
            constructor (options) {
                super(options);
                this.forced = [];
            }

            test (options) {
                let force = options && options.force;

                this.forced.push(!!force);
            }

            test1 (options) {
                this.options1 = options;
                return this.test(options);
            }

            test2 (options) {
                this.options2 = options;
                return this.test(options);
            }

            test3 (options) {
                this.options3 = options;
                return this.test(options);
            }
        };

        beforeEach(() => {
            // Preserve old router, just in case there are other tests relying on it.
            oldRouter = LF.router;
            testController = new TestController();

            LF.router = new TestRouter({
                controllers: {
                    screenshotTest: testController,
                    settings: new SettingsController()
                }
            });

            spyOn(ScreenshotUtils, 'takeScreenshot').and.resolve();
            spyOn(DetectFeatures, 'runDetectors').and.callFake(fakeDetector);
        });

        afterEach(() => {
            LF.router = oldRouter;
        });

        Async.it('should loop through each selected page and take a screenshot', () => {
            controller.model.get('selection').set('languages', ['en-US']);
            controller.model.get('selection').set('pages', testList);

            return screenshotTool.takeCoreScreenshots()
            .then(() => {
                expect(ScreenshotUtils.takeScreenshot.calls.count()).toBe(testList.length);
                expect(testController.forced.filter(trueFilter).length).toBe(testList.length);
            });
        });

        Async.it('should run the controller method with options set in screenSpecificConfigurations', () => {
            const setupCode = '0000';

            controller.model.get('selection').set('languages', ['en-US']);
            controller.model.get('selection').set('pages', testList);

            screenshotTool.screenConfigurations = {
                pages: {
                    test2: {
                        stringValues: {
                            setupCode
                        }
                    }
                }
            };

            return screenshotTool.takeCoreScreenshots()
            .then(() => {
                expect(ScreenshotUtils.takeScreenshot.calls.count()).toBe(testList.length);
                expect(testController.options1.stringValues).toBeUndefined();
                expect(testController.options2.stringValues.setupCode).toBe(setupCode);
                expect(testController.options3.stringValues).toBeUndefined();
            });
        });

        // TRACE_MATRIX('US6880')
        describe('Customer Support', () => {
            let removeTemplate;

            beforeEach(() => {
                removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/support.ejs');

                LF.StudyDesign.supportOptions = [{
                    text: 'SUPPORT_NAME_ARGENTINA',
                    number: 'SUPPORT_NUMBER_ARGENTINA',
                    display: true
                }, {
                    text: 'SUPPORT_NAME_ARMENIA',
                    number: 'SUPPORT_NUMBER_ARMENIA',
                    display: true
                }, {
                    text: 'SUPPORT_NAME_AUSTRALIA',
                    number: 'SUPPORT_NUMBER_AUSTRALIA',
                    display: true
                }];
            });

            afterEach(() => {
                removeTemplate();
            });

            Async.it('should loop through each country on the support screen', () => {
                const langList = ['en-US', 'fr-CA'];

                // 1 extra screen for when there's no selection ('Select your Country')
                const expectedScreenshotCount = langList.length * (LF.StudyDesign.supportOptions.length + 1);

                spyOn($.fn, 'select2').and.stub();

                controller.model.get('selection').set('languages', langList);
                controller.model.get('selection').set('pages', ['settings#support']);

                return screenshotTool.takeCoreScreenshots(0)
                .then(() => {
                    expect(ScreenshotUtils.takeScreenshot.calls.count()).toBe(expectedScreenshotCount);
                });
            });
        });
    });

    describe('takeModeScreenshot', () => {
        Async.it('should call the correct function for the mode passed', () => {
            spyOn(screenshotTool, 'takeDiaryScreenshots').and.callFake(() => Q());
            spyOn(screenshotTool, 'takeMessageScreenshots').and.callFake(() => Q());
            spyOn(screenshotTool, 'takeCoreScreenshots').and.callFake(() => Q());
            spyOn(screenshotTool, 'showNotification').and.resolve(true);

            return screenshotTool.takeModeScreenshot(['Diaries', 'Messagebox', 'Pages'])
                .then(() => {
                    expect(screenshotTool.takeDiaryScreenshots.calls.count()).toBe(1);
                    expect(screenshotTool.takeMessageScreenshots.calls.count()).toBe(1);
                    expect(screenshotTool.takeCoreScreenshots.calls.count()).toBe(1);
                });
        });
    });
});
