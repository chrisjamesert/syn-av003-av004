import * as DetectFeatures from 'core/screenshot/detectFeatures';
import DynamicTextDetector from 'core/screenshot/detectFeatures/detectDynamicText';
import ELF from 'core/ELF';
import Language from 'core/models/Language';

// Just importing sets up LF.StudyDesign if it isn't set up already.
// eslint-disable-next-line no-unused-vars
import * as studyDesignHelpers from 'test/helpers/StudyDesign';

// eslint-disable-next-line new-cap
TRACE_MATRIX('US6813')
.describe('DetectFeatures', () => {
    let renderSpy,
        screenshotSpy;

    beforeAll(() => {
        renderSpy = jasmine.createSpy('render');
        screenshotSpy = jasmine.createSpy('takeScreenshot');
    });

    beforeEach(() => {
        renderSpy.calls.reset();
        screenshotSpy.calls.reset();
    });

    describe('runDetectors', () => {
        class TestDetectorBase {
            constructor (render, takeScreenshot) {
                super(render, takeScreenshot);

                this.isDetected = false;
                this.render = render;
                this.takeScreenshot = takeScreenshot;
            }

            detected () {
                this.isDetected = true;
            }

            run () {
                // Do nothing
            }
        }

        describe('should run all of the detectors', () => {
            const setupDetector = (options = {}) => {
                _.defaults(options, { functionsToSpy: ['run', 'detected'] });

                const detector = new (options.detectorClass || TestDetectorBase)(renderSpy, screenshotSpy);
                _.each(options.functionsToSpy, func => spyOn(detector, func).and.callThrough());

                return detector;
            };

            const setupDetectors = (options = {}) => {
                _.defaults(options, {
                    numDetectors: 2,
                    functionsToSpy: ['run', 'detected']
                });

                return _.times(options.numDetectors, () => setupDetector(options));
            };

            Async.it('and the detectors will not detect anything, so only one screenshot is taken.', () => {
                const detectors = setupDetectors();

                return DetectFeatures.runDetectors({
                    render: renderSpy,
                    takeScreenshot: screenshotSpy,
                    detectors
                })
                .then(() => {
                    expect(renderSpy).toHaveBeenCalled();

                    detectors.forEach((detector) => {
                        expect(detector.run).toHaveBeenCalled();
                        expect(detector.detected).not.toHaveBeenCalled();
                    });

                    expect(screenshotSpy.calls.count()).toBe(1);
                });
            });

            Async.it('and one detector will detect, screenshots number will be determined by the detector', () => {
                class TestDetector extends TestDetectorBase {
                    setupDetectionListener () {
                        // setupDetectionListener should set up a listener that will be called on screen render if this feature is detected.
                        renderSpy.and.callFake(() => {
                            return this.detected();
                        });
                    }

                    run () {
                        if (this.isDetected) {
                            return this.takeScreenshot();
                        }

                        return Q();
                    }
                }

                const detectors = [
                    setupDetector(),
                    setupDetector({ detectorClass: TestDetector })
                ];

                return DetectFeatures.runDetectors({
                    render: renderSpy,
                    takeScreenshot: screenshotSpy,
                    detectors
                })
                .then(() => {
                    // One detector that just calls takeScreenshot once. Will render multiple times.
                    expect(renderSpy.calls.count() > 0).toBe(true);

                    // first detector has no setupDetectionListener function.
                    expect(detectors[0].run).toHaveBeenCalled();
                    expect(detectors[0].detected).not.toHaveBeenCalled();

                    // second detector has a working setupDetectionListener function.
                    expect(detectors[1].run).toHaveBeenCalled();
                    expect(detectors[1].detected).toHaveBeenCalled();
                    expect(detectors[1].isDetected).toBe(true);

                    // takeScreenshot is still only called once
                    expect(screenshotSpy.calls.count()).toBe(1);
                });
            });
        });
    });

    describe('DynamicTextDetector', () => {
        let dynamicTextDetector;

        beforeEach(() => {
            dynamicTextDetector = new DynamicTextDetector(renderSpy, screenshotSpy);
        });

        afterEach(() => {
            if (ELF.rules.find(dynamicTextDetector.ELFId)) {
                ELF.rules.remove(dynamicTextDetector.ELFId);
            }

            dynamicTextDetector = null;
        });

        it(':setupDetectionListener - should set up an ELF rule.', () => {
            dynamicTextDetector.setupDetectionListener();

            expect(ELF.rules.find(dynamicTextDetector.ELFId)).toBeDefined();
            ELF.rules.remove(dynamicTextDetector.ELFId);
        });

        Async.it(':parseDynamicTextValues - should parse dynamic text options and retrieve values', () => {
            const strings = new Language({
                namespace: 'ScreenshotTests',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    FIVE: 'five',
                    SIX: 'six'
                }
            });

            LF.strings.add(strings);

            const testDynamicTextValues = {
                values: ['one', 'two'],
                getValues: () => ['three', 'four'],
                keys: {
                    test1: 'FIVE',
                    test2: 'SIX',
                    options: {
                        namespace: strings.get('namespace')
                    }
                }
            };

            return dynamicTextDetector.parseDynamicTextValues({ screenshots: testDynamicTextValues })
            .then((valuesList) => {
                const expectedValues = ['one', 'two', 'three', 'four', 'five', 'six'];
                expect(_.difference(expectedValues, valuesList).length).toBe(0);
            })
            .then(() => {
                LF.strings.remove(strings);
            });
        });

        Async.it(':increment - should increment the correct item in the list', () => {
            const listItem = (id, values) => ({ id, values, index: 0, completed: false });
            const listItems = new Backbone.Collection([
                listItem('dynamicTextOne', ['one']),
                listItem('dynamicTextTwo', ['one', 'two']),
                listItem('dynamicTextThree', ['one', 'two', 'three'])
            ]);

            dynamicTextDetector.list = listItems;

            const expectDynamicTextItems = ({ id, completed, index }) => {
                const item = dynamicTextDetector.list.get(id);

                expect(item.get('completed')).toBe(completed);
                if (index) {
                    expect(item.get('index')).toBe(index);
                }

                return item;
            };

            // each list item should start with completed:false and index:0
            listItems.pluck('id').forEach(id => expectDynamicTextItems({
                id,
                index: 0,
                completed: false
            }));

            return Q()
            .then(() => dynamicTextDetector.increment('dynamicTextOne'))
            .then((text) => {
                const firstDynamicText = expectDynamicTextItems({
                    id: 'dynamicTextOne',
                    completed: true
                });

                expect(text).toBe(firstDynamicText.get('values')[0]);

                // the other two should remain unchanged
                ['dynamicTextTwo', 'dynamicTextThree'].forEach(id => expectDynamicTextItems({
                    id,
                    index: 0,
                    completed: false
                }));
            })
            .then(() => dynamicTextDetector.increment('dynamicTextTwo'))
            .then((text) => {
                const secondDynamicText = expectDynamicTextItems({
                    id: 'dynamicTextTwo',
                    completed: false,
                    index: 1
                });

                expect(text).toBe(secondDynamicText.get('values')[0]);

                // the other two should remain unchanged
                expectDynamicTextItems({
                    id: 'dynamicTextOne',
                    completed: true
                });
                expectDynamicTextItems({
                    id: 'dynamicTextTwo',
                    index: 0,
                    completed: false
                });
            })
            .then(() => dynamicTextDetector.increment('dynamicTextTwo'))
            .then((text) => {
                const secondDynamicText = expectDynamicTextItems({
                    id: 'dynamicTextTwo',
                    completed: true
                });

                expect(text).toBe(secondDynamicText.get('values')[1]);
            });
        });

        Async.it(':detected - should create a new list and new item if it doesn\'t exist and increment', () => {
            spyOn(dynamicTextDetector, 'increment').and.resolve('test');
            spyOn(dynamicTextDetector, 'parseDynamicTextValues').and.resolve();

            expect(dynamicTextDetector.list).not.toBeDefined();

            return dynamicTextDetector.detected('dynamicTextOne')
            .then(({ text }) => {
                expect(text).toBe('test');

                expect(dynamicTextDetector.list).toBeDefined();
                expect(dynamicTextDetector.list.get('dynamicTextOne')).toBeDefined();
                expect(dynamicTextDetector.isDetected).toBe(true);
            });
        });

        Async.it(`:run - should be called after the first render and after setupDetectionListener is called,
            then recursively run until all screenshots are taken.
            Total will be the amount of values of the biggest screenshot values list.`,
        () => {
            let textReturned = [];
            const makeItem = (key, values) => {
                return { key, dynamicText: { screenshots: { values } } };
            };
            const testList = [
                makeItem('dynamicTextOne', ['one', 'two', 'three']),
                makeItem('dynamicTextTwo', ['one', 'two'])
            ];

            const render = () => {
                const textPair = [];

                return ELF.trigger('DYNAMICTEXT:Trigger', testList[0])
                .then(({ text }) => textPair.push(text))

                .then(() => ELF.trigger('DYNAMICTEXT:Trigger', testList[1]))
                .then(({ text }) => textPair.push(text))

                .then(() => textReturned.push(textPair));
            };

            renderSpy.and.callFake(render);
            screenshotSpy.and.callFake(() => Q());

            dynamicTextDetector.setupDetectionListener();
            return Q()
            .then(() => renderSpy())
            .then(() => dynamicTextDetector.run())
            .then(() => {
                expect(textReturned).toEqual([
                    ['one', 'one'],
                    ['two', 'two'],
                    ['three', 'two']
                ]);
                expect(screenshotSpy.calls.count()).toBe(3);
            });
        });
    });
});
