import DropDownListDetector from 'core/screenshot/detectFeatures/detectDropdownList';

const testWidget = {
    model: {
        items: [
            {
                value: 1,
                text: 'Value_1'
            },
            {
                value: 2,
                text: 'Value_2'
            },
            {
                value: 3,
                text: 'Value_3'
            }
        ],
        visibleRows: 2,
        get (attr) {
            return this[attr];
        },
        set (attr, value) {
            this[attr] = value;
        }
    },
    $select2: {
        select2: $.noop
    }
};

// eslint-disable-next-line new-cap
TRACE_MATRIX('US6878')
.describe('DetectDropdownList', () => {
    let detector,
        renderSpy,
        takeScreenshotSpy;

    beforeEach(() => {
        renderSpy = jasmine.createSpy('renderFunc').and.callFake(function () {
            this.isLast = true;
            return Q();
        });
        takeScreenshotSpy = jasmine.createSpy('takeScreenshot').and.callFake(() => Q());

        detector = new DropDownListDetector(renderSpy, takeScreenshotSpy);
    });

    afterEach(() => {
        renderSpy.calls.reset();
        takeScreenshotSpy.calls.reset();
    });

    describe('detected', () => {
        Async.it('should fill the dropdownlist with items in order', () => {
            return Q()
            .then(() => detector.detected({ widget: testWidget }))
            .then(() => {
                expect(testWidget.model.items).toEqual([
                    {
                        value: 1,
                        text: 'Value_1'
                    },
                    {
                        value: 2,
                        text: 'Value_2'
                    },
                    {
                        value: 3,
                        text: 'Value_3'
                    }
                ]);
            })
            .then(() => {
                return detector.detected({ widget: testWidget });
            })
            .then(() => {
                expect(testWidget.model.items).toEqual([
                    {
                        value: 1,
                        text: 'Value_1'
                    },
                    {
                        value: 2,
                        text: 'Value_2'
                    }
                ]);
            })
            .then(() => {
                return detector.detected({ widget: testWidget });
            })
            .then(() => {
                expect(testWidget.model.items).toEqual([
                    {
                        value: 2,
                        text: 'Value_2'
                    },
                    {
                        value: 3,
                        text: 'Value_3'
                    }
                ]);
            });
        });
    });

    describe('run', () => {
        Async.it('should do nothing if it is not detected', () => {
            return detector.run()
            .then(() => {
                expect(renderSpy).not.toHaveBeenCalled();
                expect(takeScreenshotSpy).not.toHaveBeenCalled();
            });
        });

        Async.it('should stop rendering and take the last screen shot', () => {
            detector.isLast = true;
            detector.isDetected = true;
            detector.widget = testWidget;

            return Q()
            .then(() => detector.loopDropdownOptions(true))
            .then(() => {
                expect(renderSpy).not.toHaveBeenCalled();
                expect(takeScreenshotSpy).toHaveBeenCalled();
            });
        }, 10000);

        Async.it('should keep rendering and taking screenshots', () => {
            return Q()
            .then(() => detector.detected({ widget: testWidget }))
            .then(() => {
                spyOn(detector.widget.$select2, 'select2').and.callFake($.noop);
                return detector.loopDropdownOptions(true);
            })
            .then(() => {
                expect(renderSpy).toHaveBeenCalled();
                expect(takeScreenshotSpy).toHaveBeenCalled();
                expect(detector.widget.$select2.select2).toHaveBeenCalledWith('open');
            });
        }, 10000);
    });
});
