import * as DetectFeatures from 'core/screenshot/detectFeatures';
import ParamFunctionDetector from 'core/screenshot/detectFeatures/detectParamFunction';
import ELF from 'core/ELF';
import Language from 'core/models/Language';

TRACE_MATRIX('US7577')
.describe('ParamFunctionDetector', () => {
    let paramFunctionDetector,
        renderSpy,
        screenshotSpy;

    beforeAll(() => {
        renderSpy = jasmine.createSpy('render');
        screenshotSpy = jasmine.createSpy('takeScreenshot');
    });

    beforeEach(() => {
        renderSpy.calls.reset();
        screenshotSpy.calls.reset();
        paramFunctionDetector = new ParamFunctionDetector(renderSpy, screenshotSpy);
    });

    afterEach(() => {
        if (ELF.rules.find(paramFunctionDetector.ELFId)) {
            ELF.rules.remove(paramFunctionDetector.ELFId);
        }

        paramFunctionDetector = null;
    });

    it(':setupDetectionListener - should set up an ELF rule.', () => {
        paramFunctionDetector.setupDetectionListener();

        expect(ELF.rules.find(paramFunctionDetector.ELFId)).toBeDefined();
        ELF.rules.remove(paramFunctionDetector.ELFId);
    });

    it(':parseParamFunctionValues - should parse dynamic text options and retrieve values', () => {

        const testParamFunctionValues = {
            values: ['one', 'two'],
            getValues: () => ['three', 'four']
        };
        let results = paramFunctionDetector.parseParamFunctionValues({ screenshots: testParamFunctionValues });

        const expectedValues = ['one', 'two', 'three', 'four'];
        expect(_.difference(expectedValues, results).length).toBe(0);
    });

    it(':increment - should increment the correct item in the list', () => {
        const listItem = (id, values) => ({ id, values, index: 0, completed: false });
        const listItems = new Backbone.Collection([
            listItem('paramFunctionOne', ['one']),
            listItem('paramFunctionTwo', ['one', 'two']),
            listItem('paramFunctionThree', ['one', 'two', 'three'])
        ]);

        paramFunctionDetector.list = listItems;

        const expectParamFunctionItems = ({ id, completed, index }) => {
            const item = paramFunctionDetector.list.get(id);

            expect(item.get('completed')).toBe(completed);
            if (index) {
                expect(item.get('index')).toBe(index);
            }

            return item;
        };

        // each list item should start with completed:false and index:0
        listItems.pluck('id').forEach(id => expectParamFunctionItems({
            id,
            index: 0,
            completed: false
        }));
        
        const valueOne = paramFunctionDetector.increment('paramFunctionOne');
        const paramFuncOne = expectParamFunctionItems({
            id: 'paramFunctionOne',
            completed: true
        });
        expect(valueOne).toBe(paramFuncOne.get('values')[0]);
        ['paramFunctionTwo', 'paramFunctionThree'].forEach(id => expectParamFunctionItems({
            id,
            index: 0,
            completed: false
        }));

        const valueTwoLoopOne = paramFunctionDetector.increment('paramFunctionTwo');
        const paramFuncTwo = expectParamFunctionItems({
            id: 'paramFunctionTwo',
            completed: false,
            index: 0
        });
        expect(valueTwoLoopOne).toBe(paramFuncTwo.get('values')[0]);
        // the other two should remain unchanged
        expectParamFunctionItems({
            id: 'paramFunctionOne',
            completed: true
        });
        expectParamFunctionItems({
            id: 'paramFunctionThree',
            index: 0,
            completed: false
        });

        const valueTwoLoopTwo = paramFunctionDetector.increment('paramFunctionTwo');
        const paramFuncTwoLoopTwo = expectParamFunctionItems({
            id: 'paramFunctionTwo',
            completed: true
        });
        expect(valueTwoLoopTwo).toBe(paramFuncTwoLoopTwo.get('values')[1]);
    });

    it(':detected - should create a new list and new item if it doesn\'t exist and increment', () => {
        spyOn(paramFunctionDetector, 'increment').and.returnValue('test');
        spyOn(paramFunctionDetector, 'parseParamFunctionValues').and.callThrough();

        expect(paramFunctionDetector.list).not.toBeDefined();

        let text = paramFunctionDetector.detected('paramFunctionOne');

        expect(text.value).toBe('test');
        expect(paramFunctionDetector.list).toBeDefined();
        expect(paramFunctionDetector.list.get('paramFunctionOne')).toBeDefined();
        expect(paramFunctionDetector.isDetected).toBe(true);
    });

    Async.it(`:run - should be called after the first render and after setupDetectionListener is called,
        then recursively run until all screenshots are taken.
        Total will be the amount of values of the biggest screenshot values list.`, () => {
        let textReturned = [];
        const makeItem = (key, values) => {
            return { key, paramFunction: { screenshots: { values } } };
        };
        const testList = [
            makeItem('paramFunctionOne', ['one', 'two', 'three']),
            makeItem('paramFunctionTwo', ['one', 'two'])
        ];

        const render = () => {
            const textPair = [];

            return ELF.trigger('PARAMFUNCTION:Trigger', testList[0])
            .then(({ value }) => textPair.push(value))
            .then(() => ELF.trigger('PARAMFUNCTION:Trigger', testList[1]))
            .then(({ value }) => textPair.push(value))
            .then(() => textReturned.push(textPair));
        };

        renderSpy.and.callFake(render);
        screenshotSpy.and.callFake(() => Q());

        paramFunctionDetector.setupDetectionListener();
        return Q()
        .then(() => renderSpy())
        .then(() => paramFunctionDetector.run())
        .then(() => {
            expect(textReturned).toEqual([
                ['one', 'one'],
                ['two', 'two'],
                ['three', 'two']
            ]);
            expect(screenshotSpy.calls.count()).toBe(3);
        });
    });
});
