import ScreenshotView from 'core/screenshot/pages/ScreenshotView';
import ScreenshotController from 'core/screenshot/ScreenshotController';

import NotifyView from 'core/views/NotifyView';
import Language from 'core/models/Language';

TRACE_MATRIX('US6500').
describe('ScreenshotView', () => {

    let location,
        product = 'logpad',
        template = `<div id="screenshot-template">

            <div class="screenshot-checkbox-container">

                <div class="select-link"><span class="check-all">Select All</span> | <span class="uncheck-all">Deselect All</span></div>

                <div id="screenshot-checkbox-group" class="list">

                </div>

            </div>

            </div>`;

    const controller = new ScreenshotController({ product });

    const { model, strings } = controller;

    const view = new ScreenshotView({ model, strings });

    const createListItems = (items) => {

        let listItems = items || ['<li class="list-item" data-value="Dummy_Diary1"><input type="checkbox"><span id="dummy">Dummy Diary 1</span></li>',
                    '<li class="list-item" data-value="Dummy_Diary2"><input type="checkbox"><span id="dummy2">Dummy Diary 2</span></li>'
                ],
            unorderedList = $('<div>');

        listItems.forEach(function (listItem, idx) {
            unorderedList.append($(listItem));
        });

        return unorderedList;
    };

    const addListToHtml = (item, thisView) => {
        return thisView.$checkboxGroup.html(item);
    };

    const clickListItem = (item, id) => {
        return item.click();
    };

    $('body').append(template);


    Async.beforeEach(() => {
        // PhantomJS defect causes it to always return false for navigator.online: https://github.com/ariya/phantomjs/issues/10647
        // Once navigator has been replaced, it will no longer update as expected otherwise
        // Remove this workaround once PhantomJS defect is resolved
        let html = '';

        if (!navigator.onLine) {
            let fakeNavigator = _.clone(navigator);
            fakeNavigator.onLine = true;

            // eslint-disable-next-line no-global-assign, no-native-reassign
            navigator = fakeNavigator;
        }

        spyOn(view, 'next').and.callFake((url) => location = url);
        spyOn(NotifyView.prototype, 'show').and.resolve();

        // We need to wait until the view is rendered...
        return view.render()
            .then(() => {

                view.$checkboxContainer = view.$('.screenshot-checkbox-container');
                view.$checkboxGroup = view.$('#screenshot-checkbox-group');
                view.$screenshotBtnContainer = view.$('.screenshot-btn-container');

            });

    });

    it('should verify that selection has been made before navigating when NEXT is clicked', () => {
        let languages,
            spanElem,
            listItems = createListItems();

        addListToHtml(listItems, view);

        spanElem = view.$el.find('li.list-item').first().children('span');

        clickListItem(spanElem);

        view.nextHandler();

        languages = model.get('selection').get('languages');

        expect(languages.length).toBeGreaterThan(0);

    });

    it('should correctly select all items when \'Select All\' is clicked', () => {
        let languages,
            listItems = createListItems();

        addListToHtml(listItems, view);

        view.selectAll();

        view.nextHandler();

        languages = model.get('selection').get('languages');

        expect(languages.length).toBe(2);
        expect(location).toBe('screenshot_mode');

    });

    it('should not navigate if no selection was made', () => {
        let listItems = createListItems();

        view.nextHandler();

        expect(location).toBe('screenshot');

    });

    it('should display error modal if no selection was made', () => {
        let listItems = createListItems();

        view.selectAll();

        view.deselectAll();

        view.nextHandler();

        expect(NotifyView.prototype.show).toHaveBeenCalled();
    });

    afterEach(() => {

        view.deselectAll();
        location = 'screenshot';

    });
});
