import FileHandler from 'core/classes/FileHandler';
import * as utils from 'core/utilities';

const downloadOptions = {
    retry: [1000, 2000, 3000]
};
    
describe('FileHandler', () => {
    let fileHandler = null,
        rootDir = 'myDir',
        fs;
    
    beforeEach(() => {
        fs = jasmine.createSpyObj('fs', ['write', 'remove', 'file', 'toURL', 'create', 'download']);
        spyOn(utils, 'createFilesystem').and.callFake(() => {
            return fs;
        });
        fileHandler = new FileHandler(rootDir);
    });

    afterEach(() => {
        fileHandler = null;
    });

    describe('#constructor', () => {
        it('should set the rootDir based on the constructor', () => {
            expect(fileHandler._rootDir).toEqual(rootDir);
        });

        it('should set the filesystem to null in the constructor', () => {
            expect(fileHandler._fs).toBeNull();
        });
    });

    describe('#functions', () => {
        describe('#write', () => {
            Async.it('should call fs.write()', () => {
                fs.write.and.callFake(() => Q());
                
                return fileHandler.write('path', 'test val').then(() => {
                    expect(fileHandler.fs.write).toHaveBeenCalled();
                });
            });
        });

        describe('#getSize', () => {
            Async.it('should call fs.file()', () => {
                let fileEntry = jasmine.createSpyObj('fileEntry', ['getMetadata'])
                fileEntry.getMetadata.and.callFake(cb => cb({ size: 10 }));
                fs.file.and.callFake(() => Q(fileEntry));
                
                return fileHandler.getSize('foo').then((res) => {
                    expect(fileHandler.fs.file).toHaveBeenCalled();
                    expect(res).toBe(10);
                });
            });
        });

        describe('#getIndexURL', () => {
            Async.it('should call fs.toURL', () => {
                fs.toURL.and.callFake(() => Q());
                
                return fileHandler.getIndexURL('path').then(() => {
                    expect(fileHandler.fs.toURL).toHaveBeenCalled();
                });
            });
        });
        
        describe('#download', () => {
            Async.it('should create file and call fs.download', () => {
                fs.download.and.callFake(() => Q());
                fs.create.and.callFake(() => Q());
                spyOn(fileHandler, 'generatePath').and.returnValue('generated path');

                return fileHandler.download('url', 'path').tap(() => {
                    expect(fileHandler.fs.create).toHaveBeenCalledWith('generated path');
                    expect(fileHandler.fs.download).toHaveBeenCalledWith('url', 'generated path', downloadOptions);
                });
            });
        });
    });
});