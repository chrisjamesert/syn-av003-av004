import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import NumberSpinner from 'core/widgets/NumberSpinner';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';

// import Templates from 'core/collections/Templates';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';
import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';
import BaseSpinnerTests from './BaseSpinner.spec';

import 'core/resources/Templates';

export default class NumberSpinnerTests extends BaseSpinnerTests {
    constructor (model = NumberSpinnerTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'SPINNER_DIARY_W_1',
            type: 'NumberSpinner',
            className: 'SPINNER_DIARY_W_1',
            label: 'PICK_A_NUMBER',
            spinnerInputOptions: [
                {
                    min: 0,
                    max: 500
                }
            ]
        });
    }

    static get decimalModel () {
        return new Widget({
            id: 'SPINNER_DIARY_W_2',
            type: 'NumberSpinner',
            className: 'SPINNER_DIARY_W_2',
            label: 'PICK_A_NUMBER',
            min: 0.11,
            max: 100,
            templates: {
                modal: 'DecimalSpinnerModal'
            },
            okButtonText: 'MODAL_OK_TEXT',
            defaultVal: '3.00',
            spinnerInputOptions: [
                {
                    min: 0,
                    max: 101
                },
                {
                    min: 0,
                    max: 0.99,
                    step: 0.05,
                    precision: 2
                }
            ]
        });
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US6106')
        .describe('NumberSpinner', () => {
            let templates,
                dummyParent,
                security,
                numberSpinnerWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();

                templates = LF.templates;
            });
            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                $('.modal').remove();
                $('.modal-backdrop').remove();
                numberSpinnerWidget = null;
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });

                it('#defaultModalTemplate', () => {
                    expect(numberSpinnerWidget.defaultModalTemplate).toEqual('DEFAULT:NumberSpinnerModal');
                });

                it('#defaultSpinnerTemplate', () => {
                    expect(numberSpinnerWidget.defaultSpinnerTemplate).toEqual('DEFAULT:NumberSpinnerControl');
                });

                it('#defaultSpinnerItemTemplate', () => {
                    expect(numberSpinnerWidget.defaultSpinnerItemTemplate).toEqual('DEFAULT:NumberItemTemplate');
                });
            });

            TRACE_MATRIX('DE22219')
            .describe('Event binding methods', () => {
                Async.beforeEach(() => {
                    numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    return numberSpinnerWidget.render();
                });

                describe('#addCustomEvents', () => {
                    it('listens to the value change event for each spinner to set spinner limits', () => {
                        spyOn(numberSpinnerWidget, 'listenTo').and.callThrough();

                        numberSpinnerWidget.addCustomEvents();

                        expect(numberSpinnerWidget.spinners.length > 0).toBe(true);
                        for (let i = 0; i < numberSpinnerWidget.spinners.length; ++i) {
                            expect(numberSpinnerWidget.listenTo).toHaveBeenCalledWith(numberSpinnerWidget.spinners[i].model, 'change:value', numberSpinnerWidget.setSpinnerLimits);
                        }
                    });
                });

                describe('#removeCustomEvents', () => {
                    it('removes listeners for the value change event for each spinner', () => {
                        spyOn(numberSpinnerWidget, 'stopListening').and.callThrough();

                        numberSpinnerWidget.removeCustomEvents();

                        expect(numberSpinnerWidget.spinners.length > 0).toBe(true);
                        for (let i = 0; i < numberSpinnerWidget.spinners.length; ++i) {
                            expect(numberSpinnerWidget.stopListening).toHaveBeenCalledWith(numberSpinnerWidget.spinners[i].model);
                        }
                    });
                });
            });

            TRACE_MATRIX('DE22219')
            .describe('Value functions', () => {
                Async.it('#getSpinnerValuesArray() with value', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    numberSpinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .tap(() => {
                            expect(numberSpinnerWidget.getSpinnerValuesArray()[0]).toEqual('4');
                        });
                });

                Async.it('#getModalValuesString() calls getNumericValue', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    numberSpinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(numberSpinnerWidget, 'getNumericValue').and.callThrough();

                    return numberSpinnerWidget.render()
                        .then(() => {
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        })
                        .then(() => {
                            return numberSpinnerWidget.getModalValuesString();
                        }).tap((value) => {
                            expect(numberSpinnerWidget.getNumericValue).toHaveBeenCalled();
                            expect(value).toBe('4');
                        });
                });

                Async.it('#getNumericValue() with value', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    numberSpinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .then(() => {
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        })
                        .then(() => {
                            _.each(numberSpinnerWidget.spinners, (spinner) => {
                                spinner.stop();
                                spinner.pushValue();
                            });
                            return numberSpinnerWidget.getNumericValue();
                        }).tap((value) => {
                            expect(value).toBe(4);
                        });
                });

                Async.it('#getNumericValue() overrides value with customValues array', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    numberSpinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .then(() => {
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        })
                        .then(() => {
                            _.each(numberSpinnerWidget.spinners, (spinner) => {
                                spinner.stop();
                                spinner.pushValue();
                            });
                            return numberSpinnerWidget.getNumericValue([{ index: 0, value: 7 }]);
                        }).tap((value) => {
                            expect(value).toBe(7);
                        });
                });

                Async.it('#injectSpinnerInputs() places spinners inside modal template', () => {
                    numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .then(() => {
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        })
                        .tap(() => {
                            let $containers = numberSpinnerWidget.$modal.find('.number-spinner-container');
                            expect($containers.length).toBe(1);

                            $containers.each((index, element) => {
                                expect(numberSpinnerWidget.spinners[index] instanceof NumberSpinnerInput).toBe(true);
                                expect($(element).children().length).toBe(1);
                            });
                        });
                });
            });

            TRACE_MATRIX('DE22219')
            .describe('min/max validation', () => {
                Async.beforeEach(() => {
                    numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: NumberSpinnerTests.decimalModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .then(() => {
                            // disable automatic change listening, since we are testing the change handlers themselves.
                            numberSpinnerWidget.stopListening(numberSpinnerWidget.spinners[0].model);
                            numberSpinnerWidget.stopListening(numberSpinnerWidget.spinners[1].model);
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        });
                });

                describe('#testNumericValue', () => {
                    it('returns true for whole number containing part of range (max)', () => {
                        expect(numberSpinnerWidget.testNumericValue(0, 100)).toBe(true);
                    });
                    it('returns false for whole number containing no part of range (max)', () => {
                        expect(numberSpinnerWidget.testNumericValue(0, 101)).toBe(false);
                    });
                    it('returns true for whole number containing part of range (min)', () => {
                        expect(numberSpinnerWidget.testNumericValue(0, 0)).toBe(true);
                    });
                    it('returns true if whole number contains entirety of range', () => {
                        numberSpinnerWidget.model.set('min', 75.25);
                        numberSpinnerWidget.model.set('max', 75.75);
                        expect(numberSpinnerWidget.testNumericValue(0, 75)).toBe(true);
                    });
                    it('returns true for decimal above min of range', () => {
                        numberSpinnerWidget.spinners[0].setValue(0);
                        expect(numberSpinnerWidget.testNumericValue(1, 0.15)).toBe(true);
                    });
                    it('returns false for decimal below min of range', () => {
                        numberSpinnerWidget.spinners[0].setValue(0);
                        expect(numberSpinnerWidget.testNumericValue(1, 0.10)).toBe(false);
                    });
                    it('returns true for decimal below max of range', () => {
                        numberSpinnerWidget.spinners[0].setValue(100);
                        expect(numberSpinnerWidget.testNumericValue(1, 0)).toBe(true);
                    });
                    it('returns false for decimal above max of range', () => {
                        numberSpinnerWidget.spinners[0].setValue(100);
                        expect(numberSpinnerWidget.testNumericValue(1, 0.5)).toBe(false);
                    });
                });

                TRACE_MATRIX('DE22348')
                .describe('#testAllValuesForIndex', () => {
                    it('returns true if min and max both pass testNumericValue', () => {
                        spyOn(numberSpinnerWidget, 'testNumericValue').and.callFake((index, value) => {
                            expect(index).toBe(0);
                            switch (value) {
                                case 0:
                                    return true;
                                case 101:
                                    return true;
                                default:
                                    fail(`unexpected value ${value}`);
                                    return false;
                            }
                        });

                        expect(numberSpinnerWidget.testAllValuesForIndex(0)).toBe(true);
                    });
                    it('returns false if min returns false for testNumericValue', () => {
                        spyOn(numberSpinnerWidget, 'testNumericValue').and.callFake((index, value) => {
                            expect(index).toBe(0);
                            switch (value) {
                                case 0:
                                    return false;
                                case 101:
                                    return true;
                                default:
                                    fail(`unexpected value ${value}`);
                                    return false;
                            }
                        });

                        expect(numberSpinnerWidget.testAllValuesForIndex(0)).toBe(false);
                    });
                    it('returns false if max returns false for testNumericValue', () => {
                        spyOn(numberSpinnerWidget, 'testNumericValue').and.callFake((index, value) => {
                            expect(index).toBe(0);
                            switch (value) {
                                case 0:
                                    return true;
                                case 101:
                                    return false;
                                default:
                                    fail(`unexpected value ${value}`);
                                    return false;
                            }
                        });

                        expect(numberSpinnerWidget.testAllValuesForIndex(0)).toBe(false);
                    });
                });

                describe('#adjustSpinner', () => {
                    Async.it('hides the correct number from the whole number portion', () => {
                        return numberSpinnerWidget.adjustSpinner(0)
                        .then(() => {
                            // 0-101, minus the 101st value (out of range), is 101 items.  Verify that is our count.
                            let visibleItemsCount = 0;
                            numberSpinnerWidget.spinners[0].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(101);
                        });
                    });

                    Async.it('hides the correct number from the decimal number portion (min)', () => {
                        numberSpinnerWidget.spinners[0].setValue(0);
                        return numberSpinnerWidget.adjustSpinner(1)
                        .then(() => {
                            // 0.00-0.95 (20 items), minus 0.00, 0.05, and 0.10 (out of range), is 17 items.  Verify that is our count.
                            let visibleItemsCount = 0;
                            numberSpinnerWidget.spinners[1].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(17);
                        });
                    });

                    Async.it('hides the correct number from the decimal number portion (max)', () => {
                        numberSpinnerWidget.spinners[0].setValue(100);
                        return numberSpinnerWidget.adjustSpinner(1)
                        .then(() => {
                            // 0.00 is the only item allowed.  Verify our count is 1.
                            let visibleItemsCount = 0;
                            numberSpinnerWidget.spinners[1].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(1);
                        });
                    });

                    Async.it('hides the nothing from the decimal number portion (mid-range)', () => {
                        numberSpinnerWidget.spinners[0].setValue(50);
                        return numberSpinnerWidget.adjustSpinner(1)
                        .then(() => {
                            // Verify our count is 20.
                            let visibleItemsCount = 0;
                            numberSpinnerWidget.spinners[1].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(20);
                        });
                    });

                    Async.it('Does not allow decimal value to be blank if whole number is selected', () => {
                        // Clear stuff out so we can redefine our widget
                        $(`#${dummyParent.getQuestionnaire().id}`).remove();
                        $('.modal').remove();
                        $('.modal-backdrop').remove();

                        let customModel = NumberSpinnerTests.decimalModel;

                        customModel.unset('defaultVal');

                        numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                            model: customModel,
                            mandatory: false,
                            parent: dummyParent
                        });

                        return numberSpinnerWidget.render()
                            .then(() => {
                                // disable automatic change listening, since we are testing the change handlers themselves.
                                numberSpinnerWidget.stopListening(numberSpinnerWidget.spinners[0].model);
                                numberSpinnerWidget.stopListening(numberSpinnerWidget.spinners[1].model);
                                return numberSpinnerWidget.openDialog();
                            })
                            .then(() => {
                                // duplicating some work that is done by render, but show() and setting values
                                //  are thread safe so this is a good way to listen to the promise
                                return numberSpinnerWidget.refreshSpinners();
                            }).then(() => {
                                expect(numberSpinnerWidget.spinners[1].value).toBe('');
                            }).then(() => {
                                return numberSpinnerWidget.spinners[0].setValue(50);
                            }).then(() => {
                                return numberSpinnerWidget.adjustSpinner(1);
                            }).then(() => {
                                expect(numberSpinnerWidget.spinners[1].value).toBe(0);
                            });
                    });
                });

                TRACE_MATRIX('DE22260')
                .TRACE_MATRIX('DE22302')
                .describe('#setSpinnerLimits', () => {
                    beforeEach(() => {
                        spyOn(numberSpinnerWidget, 'adjustSpinner').and.callThrough();
                    });

                    Async.it('calls adjustSpinner for each of our spinner indexes', () => {
                        return numberSpinnerWidget.setSpinnerLimits()
                        .then(() => {
                            expect(numberSpinnerWidget.adjustSpinner.calls.count()).toBe(2);
                            expect(numberSpinnerWidget.adjustSpinner).toHaveBeenCalledWith(0);
                            expect(numberSpinnerWidget.adjustSpinner).toHaveBeenCalledWith(1);
                        });
                    });

                    Async.it('skips if spinnerLimitsPromise is already a running promise', () => {
                        numberSpinnerWidget.spinnerLimitsPromise = Q();
                        return numberSpinnerWidget.setSpinnerLimits()
                        .then(() => {
                            expect(numberSpinnerWidget.adjustSpinner.calls.count()).toBe(0);
                        });
                    });

                    Async.it('skips if spinners length is 0', () => {
                        numberSpinnerWidget.spinners = [];
                        return numberSpinnerWidget.setSpinnerLimits()
                        .then(() => {
                            expect(numberSpinnerWidget.adjustSpinner.calls.count()).toBe(0);
                        });
                    });

                    Async.it('skips if a spinner is not yet rendered', () => {
                        numberSpinnerWidget.spinners[1].isRendered = false;
                        return numberSpinnerWidget.setSpinnerLimits()
                        .then(() => {
                            expect(numberSpinnerWidget.adjustSpinner.calls.count()).toBe(0);
                        });
                    });
                });
            });
        });
    }
}

let tester = new NumberSpinnerTests();
tester.execTests();
