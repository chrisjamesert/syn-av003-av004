let { Model, View } = Backbone;
import Templates from 'core/resources/Templates';
import { mix } from 'core/utilities/languageExtensions';
import ModalLauncherMixin from 'core/widgets/ModalLauncherMixin';
const defaultOkButtonText = 'MODAL_OK_TEXT',
    testModalTemplate = 'DEFAULT:MultipleChoiceModal',
    spinnerModalTemplate = 'DEFAULT:NumberSpinnerModal';

export default class ModalLauncherMixinTests {
    get model () {
        return new Model({
            id: 'testID',
            inputOptions: {
                itemTemplate: 'DEFAULT:ModalMultipleChoiceItem',
                type: 'MultipleChoiceInput',
                items: [{
                    text: 'IMAGE_OPTION_0',
                    value: '0'
                }, {
                    text: 'IMAGE_OPTION_1',
                    value: '1'
                }, {
                    text: 'IMAGE_OPTION_2',
                    value: '2'
                }, {
                    text: 'IMAGE_OPTION_3',
                    value: '3'
                }]
            },
            templates: {
                modal: testModalTemplate
            }
        });
    }

    get spinnerLauncherModel () {
        return new Model({
            id: 'testID',
            inputOptions: {
                type: 'NumberSpinnerInput',
                itemTemplate: 'DEFAULT:NumberItemTemplate',
                template: 'DEFAULT:NumberSpinnerControl',
                min: 0,
                max: 3
            },
            templates: {
                modal: spinnerModalTemplate
            }
        });
    }

    execTests () {
        // eslint-disable-next-line new-cap
        TRACE_MATRIX('US7326')
            .describe('ModalLauncherMixin', () => {
                // Implicitly tests languageExtensions/mix as well.
                let MixinClass = mix(View).with(ModalLauncherMixin),
                    mixin,
                    temp;
                beforeEach(() => {
                    mixin = new MixinClass({
                        model: this.model
                    });
                    temp = null;
                });

                afterEach(() => {
                    mixin.destroy();
                    mixin.$el.remove();
                    mixin = null;
                });

                describe('getters and abstract methods', () => {
                    describe('#defaultModalTemplate', () => {
                        it('throws error if not defined by subclass', () => {
                            expect(() => {
                                temp = mixin.defaultModalTemplate;
                            }).toThrow(new Error('Unimplemented defaultModalTemplate getter in a ModalLauncher implementation.'));
                        });
                    });

                    describe('#defaultOkButtonText', () => {
                        it('returns expected default', () => {
                            expect(mixin.defaultOkButtonText).toBe(defaultOkButtonText);
                        });
                    });

                    describe('#$modal', () => {
                        it('getters/setters interface with private _$modal', () => {
                            let $modal = $('<div></div>');
                            mixin.$modal = $modal;
                            expect(mixin.$modal.get(0)).toEqual($modal.get(0));
                            expect(mixin.$modal).toBe(mixin._$modal);
                        });
                    });

                    describe('#getInputValuesArray()', () => {
                        it('throws error when undefined in subclass', () => {
                            expect(() => {
                                temp = mixin.getInputValuesArray();
                            }).toThrow(new Error('Must define getInputValuesArray() in ModalLauncherMixin implementation'));
                        });
                    });

                    describe('#getModalValuesString()', () => {
                        it('throws error when undefined in subclass', () => {
                            expect(() => {
                                temp = mixin.getModalValuesString();
                            }).toThrow(new Error('Unimplemented getModalValuesString() method in a ModalLauncher implementation'));
                        });
                    });
                });

                describe('methods to test pre-render', () => {
                    describe('#delegateEvents()', () => {
                        it('calls addCustomEvents()', () => {
                            spyOn(mixin, 'addCustomEvents');
                            mixin.delegateEvents();
                            expect(mixin.addCustomEvents).toHaveBeenCalled();
                        });
                    });

                    describe('#undelegateEvents()', () => {
                        it('calls removeCustomEvents()', () => {
                            spyOn(mixin, 'removeCustomEvents');
                            mixin.undelegateEvents();
                            expect(mixin.removeCustomEvents).toHaveBeenCalled();
                        });
                    });

                    describe('#addCustomEvents()', () => {
                        it('adds click and hidden events to modal', () => {
                            // mock up $modal
                            mixin.$modal = $('<div></div>');
                            spyOn(mixin.$modal, 'on');
                            mixin.addCustomEvents();
                            expect(mixin.$modal.on).toHaveBeenCalledWith('click', '[data-dismiss]', mixin.dialogClosing);
                            expect(mixin.$modal.on).toHaveBeenCalledWith('hidden.bs.modal', mixin.dialogHidden);
                        });
                    });

                    describe('#removeCustomEvents()', () => {
                        it('removes click and hidden events from modal', () => {
                            // mock up $modal
                            mixin.$modal = $('<div></div>');
                            spyOn(mixin.$modal, 'off');
                            mixin.removeCustomEvents();
                            expect(mixin.$modal.off).toHaveBeenCalledWith('click', '[data-dismiss]', mixin.dialogClosing);
                            expect(mixin.$modal.off).toHaveBeenCalledWith('hidden.bs.modal', mixin.dialogHidden);
                        });
                    });
                });
                describe('renderModal', () => {
                    let testStrings = {
                            test1: 'test 1',
                            test2: 'test 2',
                            labelOne: 'test label'
                        },
                        expectedStrings = {
                            test1: 'test 1',
                            test2: 'test 2',
                            labelOne: 'test label',
                            modalTitle: '&nbsp;',
                            okButtonText: '&nbsp;',
                            labelTwo: '&nbsp;',
                            shouldBeBlank1: '&nbsp;',
                            shouldBeBlank2: '&nbsp;'
                        },
                        testLabels = { shouldBeBlank1: '', shouldBeBlank2: '', shouldBePopulated: 'POPULATED' };

                    beforeEach(() => {
                        mixin.model.set('labels', testLabels);
                    });

                    Async.it('calls #renderTemplate with proper strings', () => {
                        spyOn(mixin, 'renderTemplate').and.returnValue('<div></div>');
                        return mixin.renderModal(testStrings).tap(() => {
                            expect(mixin.renderTemplate).toHaveBeenCalledWith(testModalTemplate, expectedStrings);
                        });
                    });

                    Async.it('calls #injectModalInputs with proper strings', () => {
                        spyOn(mixin, 'injectModalInputs').and.resolve();
                        return mixin.renderModal(testStrings).tap(() => {
                            expect(mixin.injectModalInputs).toHaveBeenCalledWith(expectedStrings);
                        });
                    });

                    Async.it('returns with the contents of this.$modal', () => {
                        return mixin.renderModal(testStrings).tap((value) => {
                            expect(value).toBe(mixin.$modal);
                        });
                    });
                });

                describe('#getModalContainer', () => {
                    it('returns the modalContainer jquery collection', () => {
                        // just return the selector so we can use it to test
                        spyOn(window, '$').and.callFake((paramIn) => {
                            if (paramIn === '#modalContainer') {
                                return $('<div id="modalTest"></div>');
                            } else if (paramIn === 'body') {
                                return $('<div id="bodyTest"></div>');
                            }
                            return jQuery(paramIn);
                        });
                        expect(mixin.getModalContainer().attr('id')).toBe('modalTest');
                    });

                    it('returns the body jquery collection if no modalContainer id', () => {
                        // just return the selector so we can use it to test
                        spyOn(window, '$').and.callFake((paramIn) => {
                            if (paramIn === '#modalContainer') {
                                return $();
                            } else if (paramIn === 'body') {
                                return $('<div id="bodyTest"></div>');
                            }
                            return jQuery(paramIn);
                        });
                        expect(mixin.getModalContainer().attr('id')).toBe('bodyTest');
                    });
                });

                describe('post-render functions', () => {
                    Async.beforeEach(() => {
                        return mixin.renderModal({});
                    });

                    TRACE_MATRIX('DE22841')
                    .describe('#injectModalInputs', () => {
                        it('populates inputs array', () => {
                            expect(mixin.inputs.length).toBe(mixin.$modal.find('.modal-input-container').length);
                            expect(mixin.inputs.length).toBe(1);
                        });

                        Async.it('sets includeBlankValue to false', () => {
                            // remove and re-render as a spinner launcher
                            mixin.destroy();
                            mixin.$el.remove();
                            mixin = null;

                            mixin = new MixinClass({
                                model: this.spinnerLauncherModel
                            });

                            return mixin.renderModal({})
                            .then(() => {
                                expect(mixin.inputs[0].model.get('includeBlankValue')).toBe(false);
                            });
                        });
                    });

                    describe('#refreshInputs()', () => {
                        it('calls show, setValue, and pushValue on input', () => {
                            let testVal = 2;
                            spyOn(mixin, 'getInputValuesArray').and.returnValue([testVal]);
                            spyOn(mixin.inputs[0], 'show').and.callThrough();
                            spyOn(mixin.inputs[0], 'setValue').and.callThrough();
                            spyOn(mixin.inputs[0], 'pushValue').and.callThrough();
                            mixin.refreshInputs().then(() => {
                                expect(mixin.inputs[0].show).toHaveBeenCalled();
                                expect(mixin.inputs[0].setValue).toHaveBeenCalledWith(testVal);
                                expect(mixin.inputs[0].pushValue).toHaveBeenCalled();
                            });
                        });
                    });

                    describe('#dialogOpened', () => {
                        it('returns undefined if called from a jQuery event (finishes promise).', () => {
                            let e = new $.Event();
                            let ret = mixin.openDialog(e);
                            expect(ret).toBe(undefined);
                        });

                        Async.it('calls refreshSpinners', () => {
                            spyOn(mixin, 'refreshInputs');
                            return mixin.dialogOpened().then(() => {
                                expect(mixin.refreshInputs.calls.count()).toBe(1);
                            });
                        });
                    });

                    describe('#dialogHidden', () => {
                        it('calls onHidden in input', () => {
                            let isCalled = false,
                                _oldOnHidden = mixin.inputs[0].onHidden,
                                context;

                            // input may not have an onHidden method.  Put one here.
                            mixin.inputs[0].onHidden = function () {
                                isCalled = true;
                                context = this;
                            };

                            mixin.dialogHidden();
                            expect(isCalled).toBe(true);
                            expect(context).toBe(mixin.inputs[0]);

                            // return input to its previous state (no or old onHidden)
                            mixin.inputs[0].onHidden = _oldOnHidden;
                        });
                    });

                    describe('#getModalTranslationsObject()', () => {
                        it('creates a toTranslate object with okButtonText, modalTitle, inputOption items, and labels', () => {
                            mixin.model.set('labels', { testBlank: '', testPopulated: 'populated' });
                            mixin.model.set('okButtonText', 'ok');
                            mixin.model.set('modalTitle', 'title');

                            expect(mixin.getModalTranslationsObject()).toEqual({
                                testPopulated: 'populated',
                                okButtonText: 'ok',
                                modalTitle: 'title',
                                IMAGE_OPTION_0: 'IMAGE_OPTION_0',
                                IMAGE_OPTION_1: 'IMAGE_OPTION_1',
                                IMAGE_OPTION_2: 'IMAGE_OPTION_2',
                                IMAGE_OPTION_3: 'IMAGE_OPTION_3'
                            });
                        });
                    });

                    describe('#openDialog', () => {
                        beforeEach(() => {
                            // mock abstract function.
                            mixin.getInputValuesArray = () => [1];
                        });
                        it('returns undefined if called from a jQuery event (finishes promise).', () => {
                            let e = new $.Event();
                            let ret = mixin.openDialog(e);
                            expect(ret).toBe(undefined);
                        });

                        Async.it('opens the dialog by calling "show".  Adds and cleans up event handlers for dialog, calls dialogOpened handler.', () => {
                            spyOn(mixin.$modal, 'on').and.callThrough();
                            spyOn(mixin.$modal, 'modal').and.callThrough();
                            spyOn(mixin.$modal, 'off').and.callThrough();
                            spyOn(mixin, 'dialogOpened').and.callThrough();
                            return mixin.openDialog().tap(() => {
                                expect(mixin.$modal.on.calls.count()).toBe(1);
                                expect(mixin.$modal.off.calls.count()).toBe(1);
                                expect(mixin.$modal.modal.calls.count()).toBe(1);
                                expect(mixin.dialogOpened.calls.count()).toBe(1);
                                expect(mixin.$modal.modal).toHaveBeenCalledWith('show');
                            });
                        });
                    });

                    describe('#dialogClosing', () => {
                        it('returns undefined if called from a jQuery event (finishes promise).', () => {
                            let e = new $.Event();
                            let ret = mixin.dialogClosing(e);
                            expect(ret).toBe(undefined);
                        });
                    });

                    describe('#removeInputs()', () => {
                        it('destroys and de-allocates inputs', () => {
                            let isCalled = false;
                            mixin.inputs[0].destroy = function () {
                                isCalled = true;
                            };
                            mixin.removeInputs();
                            expect(isCalled).toBe(true);
                            expect(mixin.inputs.length).toBe(0);
                        });
                    });

                    describe('#removeModal()', () => {
                        it('removes modal dialog', () => {
                            spyOn(mixin.$modal, 'modal');
                            spyOn(mixin.$modal, 'remove');

                            // cache modal selector, since we are de-referencing it.
                            let $oldModal = mixin.$modal;

                            mixin.removeModal();

                            expect($oldModal.modal).toHaveBeenCalledWith('hide');
                            expect($oldModal.remove).toHaveBeenCalled();
                            expect(mixin.$modal).toBeNull();
                        });
                    });

                    describe('#destroy()', () => {
                        it('undelegates events and removes modal', () => {
                            spyOn(mixin, 'undelegateEvents');
                            spyOn(mixin, 'removeInputs');
                            spyOn(mixin, 'removeModal');

                            mixin.destroy();

                            expect(mixin.undelegateEvents).toHaveBeenCalled();
                            expect(mixin.removeInputs).toHaveBeenCalled();
                            expect(mixin.removeModal).toHaveBeenCalled();
                        });
                    });

                    describe('#renderTemplate()', () => {
                        it('calls LF.templates.display', () => {
                            spyOn(LF.templates, 'display');

                            mixin.renderTemplate('testName', { option: 'val' });

                            expect(LF.templates.display).toHaveBeenCalledWith('testName', { option: 'val' });
                        });
                    });
                });
            });
    }
}

new ModalLauncherMixinTests().execTests();
