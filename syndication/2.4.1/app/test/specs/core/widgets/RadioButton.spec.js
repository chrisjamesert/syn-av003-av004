import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Answers from 'core/collections/Answers';
import User from 'core/models/User';
import Dashboard from 'core/models/Dashboard';
import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Widget from 'core/models/Widget';
import Templates from 'core/collections/Templates';
import Logger from 'core/Logger';

import QuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionView from 'core/views/QuestionView';

import 'core/resources/Templates';

import WidgetBaseTests from './WidgetBase.specBase';

class RadioButtonCoreTests extends WidgetBaseTests {

    constructor (renderedSelector, options) {
        super(options);
        this.renderedSelector = renderedSelector;
    }
    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${widget.question.id}`).find(this.renderedSelector).length).toEqual(1);
            });
    }
}

describe('RadioButtons', () => {
    let security,
        templates;

    beforeAll(() => {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();

        templates = LF.templates;
        LF.templates = new Templates([{
                name        : 'VerticalButtonGroup',
                namespace   : 'DEFAULT',
                template    : '<div class="vertical-buttons" data-toggle="buttons" data-container></div>'
            },
            // Label wrapper for radio buttons.
            {
                name        : 'RadioButtonWrapper',
                namespace   : 'DEFAULT',
                template: '<label class="btn btn-default btn-block btn-{{ id }}"><span data-container></span></label>'
            },
            // Default radio button input.
            {
                name        : 'RadioButton',
                namespace   : 'DEFAULT',
                template    : '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
            },
           // Default template for custom RadioButton input that does not include jquery mobile css style.
            {
                name        : 'CustomRadioButton',
                namespace   : 'DEFAULT',
                template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}" data-role="none"/>'
            },
            // Default template for RadioButton label.
            {
                name        : 'CustomRadioButtonLabel',
                namespace   : 'DEFAULT',
                template: '<label for="{{ link }}" class="radio-text {{ className }}"> <div>{{ text }}</div> </label>'
            },

            // Default template for RadioButton label.
            {
                name        : 'RadioButtonLabel',
                namespace   : 'DEFAULT',
                template    : '<label for="{{ link }}" class="radio-text {{ className }}"> {{ text }} </label>'
            },
            // Default Vertical Fieldset template that does not include jquery css mobile style. Used as a container for CustomRadioButton and CustomCheckBox widget.
            {
                name        : 'CustomVerticalFieldset',
                namespace   : 'DEFAULT',
                template    : '<div data-container data-role="none" ></div>'
            }
        ]);

    });

    afterAll(() => {
        LF.security = security;
        LF.templates = templates;
    });

    let dummyParent;

    describe('RadioButton Widgets', () => {
        describe('RadioButton', () => {
            const buildModel =
                () => {
                    return new Widget({
                            id: 'DAILY_DIARY_W_0',
                            type: 'RadioButton',
                            className: 'DAILY_DIARY_W_0',
                            templates: {},
                            answers: [
                                {
                                    text: 'NO_PROBLEMS_SELF_CARE',
                                    value: '0',
                                    IT: 'W_0_radio1'
                                }, {
                                    text: 'SOME_PROBLEMS_SELF_CARE',
                                    value: '1',
                                    IT: 'W_0_radio2'
                                }, {
                                    text: 'UNABLE_SELF_CARE',
                                    value: '2',
                                    IT: 'W_0_radio3'
                                }, {
                                    text: 'NONE',
                                    value: '3',
                                    IT: 'W_0_radio4'
                                }, {
                                    text: 'NONE',
                                    value: '4',
                                    IT: 'W_0_radio5'
                                }
                            ]
                        });
                },
                baseTests = new RadioButtonCoreTests('.vertical-buttons', buildModel());

            baseTests.execTests();
            describe('control specific tests', () => {
                beforeEach(() => {
                    dummyParent = WidgetBaseTests.getMinDummyQuestion();
                });
                afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                });

                it('should select the label and save answer when responded', (done) => {
                    const model = buildModel(),
                        widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(
                            () => {
                                // When triggering click through code, you must trigger twice to register it
                                let selector = $('#DAILY_DIARY_W_0_radio_1');
                                selector.trigger('click');
                                selector.trigger('click');

                                expect(widget.answer.get('response')).toEqual('1');
                                expect($('.btn-DAILY_DIARY_W_0_radio_1').hasClass('active')).toBeTruthy();
                            }
                        )
                        .catch(e => fail(e))
                        .done(done);
                });

                it('should render with the default selected answer', (done) => {
                    const model = buildModel();
                    model.set('answer', '0');
                    const widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(
                            () => {
                                expect(widget.answer.get('response')).toEqual('0');
                                expect($('.btn-DAILY_DIARY_W_0_radio_0').hasClass('active'))
                                    .toBeTruthy('expected btn-DAILY_DIARY_W_0_radio_0 to be active');
                            }
                        )
                        .catch(e => fail(e))
                        .done(done);
                });

                it('should deselect old answer when new one is selected', (done) => {
                    const model = buildModel(),
                        widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(
                            () => {
                                // When triggering click through code, you must trigger twice to register it
                                let selector = $('#DAILY_DIARY_W_0_radio_1');
                                selector.trigger('click');
                                selector.trigger('click');

                                expect(widget.answer.get('response')).toEqual('1');
                                expect($('.btn-DAILY_DIARY_W_0_radio_0').hasClass('active')).toBeFalsy();
                                expect($('.btn-DAILY_DIARY_W_0_radio_1').hasClass('active')).toBeTruthy();
                            }
                        )
                        .catch(e => fail(e))
                        .done(done);
                });
            });
        });

        describe('CustomRadioButton', () => {
            const buildModel = () => {
                return new Widget({
                    id: 'DAILY_DIARY_W_1',
                    type: 'CustomRadioButton',
                    className: 'DAILY_DIARY_W_1',
                    templates: {},
                    answers: [
                        {
                            text: 'NO_PROBLEMS_SELF_CARE',
                            value: '0',
                            IT: 'W_1_radio1'
                        }, {
                            text: 'SOME_PROBLEMS_SELF_CARE',
                            value: '1',
                            IT: 'W_1_radio2'
                        }, {
                            text: 'UNABLE_SELF_CARE',
                            value: '2',
                            IT: 'W_1_radio3'
                        }, {
                            text: 'NONE',
                            value: '3',
                            IT: 'W_1_radio4'
                        }, {
                            text: 'NONE',
                            value: '4',
                            IT: 'W_1_radio5'
                        }
                    ]
                });
            };

            const baseTests = new RadioButtonCoreTests('div[data-container]', buildModel());

            baseTests.execTests();

            describe('control specific tests', () => {
                beforeEach(() => {
                    dummyParent = WidgetBaseTests.getMinDummyQuestion();
                });
                afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                });

                it('should create the custom radiobutton using render method', (done) => {
                    const model = buildModel(),
                        widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(
                            () => {
                                expect($('#DAILY_DIARY_W_1')).toBeTruthy();
                            }
                        )
                        .catch(e => fail(e))
                        .done(done);
                });

                it('event trigger calls respond()', (done) => {
                    const model = buildModel(),
                        widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(() => {
                            spyOn(widget, 'respond');
                            widget.delegateEvents();
                            // When triggering click through code, you must trigger twice to register it
                            let selector = $('#DAILY_DIARY_W_1_radio_0');
                            selector.trigger('click');
                            selector.trigger('click');
                            expect(widget.respond).toHaveBeenCalled();
                        })
                        .catch((e) => fail(e))
                        .done(done);
                });
                it('should select the label and save answer when responded', (done) => {
                    const model = buildModel(),
                        widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(() => {
                            return widget.respond({target: '#DAILY_DIARY_W_1_radio_0'});
                        })
                        .then(() => {
                            expect(widget.answer.get('response')).toEqual('0');
                        })
                        .catch(e => fail(e))
                        .done(done);
                });

                it('should deselect old answer when new one is selected', (done) => {
                    const model = buildModel(),
                        widget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    widget.render()
                        .then(() => {
                            return widget.respond({target: '#DAILY_DIARY_W_1_radio_0'});
                        })
                        .then(() => {
                            expect(widget.answer.get('response')).toEqual('0');
                        })
                        .then(() => {
                            return widget.respond({target: '#DAILY_DIARY_W_1_radio_1'});
                        })
                        .then(() => {
                            expect(widget.answer.get('response')).toEqual('1');
                        })
                        .catch(e => fail(e))
                        .done(done);
                });
            });
        });
    });
});
