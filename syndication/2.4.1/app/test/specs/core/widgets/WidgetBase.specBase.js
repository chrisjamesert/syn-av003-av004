import Widget from 'core/models/Widget';
import AnswerBase from 'core/models/AnswerBase';
import Answers from 'core/collections/Answers';
import WidgetBase from 'core/widgets/WidgetBase';
import * as helpers from 'test/helpers/SpecHelpers';
import { Banner, MessageRepo } from 'core/Notify';
import setupCoreMessages from 'core/resources/Messages';
import NotifyView from 'core/views/NotifyView';
import User from 'core/models/User';

const model = {
    get: (key) => {
        const values = {
            id: 'dummyIG'
        };

        return values[key];
    }
};

const validationErrors = [
    {
        property: 'error1',
        errorType: 'popup',
        errString: 'ERROR_1_BODY',
        header: 'ERROR_1_HEADER',
        ok: 'ERROR_1_OK'
    },
    {
        property: 'error2',
        errorType: 'alert',
        errString: 'ERROR_2_BODY',
        header: 'ERROR_2_HEADER',
        ok: 'ERROR_2_OK'
    },
    {
        property: 'error3',
        errorType: 'banner',
        errString: 'ERROR_3_BODY',
        header: 'ERROR_3_HEADER',
        ok: 'ERROR_3_OK'
    },
    {
        property: 'error4',
        errorType: 'yell',
        errString: 'ERROR_4_BODY',
        header: 'ERROR_4_HEADER',
        ok: 'ERROR_4_OK'
    }
];

export default class WidgetBaseTests {
    /**
     * @param {Widget} model the widget model.
     */
    constructor (model) {
        this.widgetModel = model;
    }

    /**
     * defined here to be used by all widgets so that if any of the real structures change
     * the spoofed environment only needs to be changed in one place.
     * @param {object} studyDesign - currently ignored as everything is spoofed, but the
     *          intent is if you wanted to use real objects, you could pass it here and
     *          this would create one for you in a consistent way for all widget tests
     * @returns {{id: string, getQuestionnaire: getQuestionnaire, model: {get: model.get}}}
     */
    static getMinDummyQuestion (studyDesign = undefined) {
        const idBase = 'dummy1',
            questionTemplate = `<div id="${idBase}-questionnaire" class="questionnaire"><section><div id="${idBase}-question" class="question"></div></section></div>`;

        $('body').append(questionTemplate);

        return {
            id: `${idBase}-question`,
            $el: $(`#${idBase}-question`)[0],
            getQuestionnaire: () => ({ id: `${idBase}-questionnaire`,
                $el: $(`#${idBase}-questionnaire`),
                data: { answers: new Answers() },
                model: {
                    get: (key) => {
                        const values = {
                            id: `${idBase}-question`
                        };

                        return values[key];
                    }
                },
                getCurrentIGR: () => 'currentIGR',
                Answer: AnswerBase
            }),
            model: {
                get: (key) => {
                    const values = {
                        id: `${idBase}-question`,
                        IG: 'dummyIG'
                    };

                    return values[key];
                }
            },
            get parent () {
                return this.getQuestionnaire();
            }
        };
    }
    getMinDummyQuestion (studyDesign = undefined) {
        return WidgetBaseTests.getMinDummyQuestion();
    }

    static getDummySecurity () {
        return {
            activeUser: new User({ id: 1 })
        };
    }
    getDummySecurity (studyDesign = undefined) {
        return WidgetBaseTests.getDummySecurity();
    }

    execTests () {
        describe(`WidgetBase tests for ${this.widgetModel.get('type')}.`, () => {
            Async.beforeAll(() => this.beforeAll());

            Async.afterAll(() => this.afterAll());

            beforeEach(() => {
                this.dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${this.dummyParent.getQuestionnaire().id}`).remove();
            });
            Async.it('Check model is correct.', () => this.checkModel());
            Async.xit('does not set answer model if answerFunc is undefined.', () => this.noAnswerFunc());
            Async.it('Check to ensure is of type WidgetBase', () => this.checkPolymorphism());
            Async.xit('test answerFunc', () => this.AnswerFunctTests());
            Async.it('returns content of this.parent.', () => this.parentTest());
            Async.it('render() returns a promise.', () => this.renderReturnsPromise());
            Async.it('render() generates an HTML elements for control prior to completion',
                    () => this.htmlRenderedCheck());
            Async.it('respond() returns a promise.', () => this.respondReturnsPromise());
            Async.it('test mandatory immediately after render()', () => this.mandatoryTests());

            describe('#displayError()', () => {
                Async.it('returns false if validate passes', () => this.displayErrorWhenValid());
                Async.it('throws default error if no validationErrors', () => this.displayErrorInvalidDefault());
                Async.it('throws first error (popup)', () => this.displayErrorInvalidFirst());
                Async.it('throws second error (alert)', () => this.displayErrorInvalidSecond());
                Async.it('throws third error (banner)', () => this.displayErrorInvalidThird());
                Async.it('defaults to banner for unknown error type', () => this.displayErrorInvalidFourth());
                Async.it('throws only first error in array, if multiple false properties',
                        () => this.displayErrorMultiple());
            });

            TRACE_MATRIX('US7392')
            .describe('Answer Type', () => {
                it('should set type in answer model.', () => this.saveAnswerTypeWithValue());
                it('should not set type into answer model if type is null.', () => this.saveAnswerTypeWithoutValue());
                it('addAnswer should call saveAnswerType', () => this.addAnswerCallsSaveAnswerType());
            });

            TRACE_MATRIX('US7101')
            .describe('Widget Parameter Functions (#setupModel)', () => {
                Async.it('should process a lambda function in the model.', () => this.processLamdbaFunc());
                Async.it('should process a function in the LF.Widget.ParamFunctions namespace.', () => this.processLFFunc());
                Async.it('should not do anything without a function.', () => this.processNoFunc());
                Async.it('should accept a function that returns a value.', () => this.processNoPromiseFunc());
                Async.it('should accept a function that returns a promise which returns a value.', () => this.processPromiseFunc());
                Async.it('should cache and re-run function keys, even recursively.', () => this.processRecursiveMultiTest());
            });
        });
    }

    /**
     * property for getting the model passed into the constructor.   Returns a semi-shallow copy of the model
     * by a shallow copy on model but also a shallow copy on model.attributes as well.
     * @returns {Object}
     */
    get model () {
        // at a minimum we are changing attributes with model.set, so make a shallow copy there
        const ret = _.extend({}, this.widgetModel);
        ret.attributes = _.extend({}, this.widgetModel.attributes);
        return ret;
    }

    /**
     * the beforeAll to be called prior to all tests.
     * @returns {Q.Promise<void>}
     */
    beforeAll () {
        setupCoreMessages();
        this._security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();
        return Q();
    }

    /**
     * the afterAll to be called prior to all tests.
     * @returns {Q.Promise<void>}
     */
    afterAll () {
        MessageRepo.clear();
        LF.security = this._security;
        return Q();
    }

    /**
     * verifies the model is an instanceof Widget, and contains a schema, id, and type
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    checkModel () {
        expect(this.widgetModel instanceof Widget)
            .toBeTruthy('Model passed to WidgetBaseTest constructor not type Widget');
        expect(this.widgetModel.get('id')).toBeDefined();
        expect(this.widgetModel.get('type')).toBeDefined();
        expect(this.widgetModel.schema).toBeDefined();
        expect(typeof this.widgetModel.schema).toEqual('object');
        return Q();
    }

    /**
     * verifies that the widget extend WidgetBase
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    checkPolymorphism () {
        const myModel = this.model;

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });
        expect(widget instanceof WidgetBase).toBeTruthy('Expected to be of type WidgetBase');
        return Q();
    }

    /**
     * Verifies that if model.answerFunc is not defined that calling answerBase doesn't modify the answers
     * in the model.
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    noAnswerFunc () {
        const myModel = this.model;
        myModel.set('answerFunc', undefined);

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });
        spyOn(myModel, 'set');

        return widget.setupAnswers()
            .then(() => {
                expect(myModel.set.calls.count()).toBe(0);
            });
    }

    /**
     * verifies that setupAnswer calls the answerFunc passed into via the model
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
      */
    AnswerFunctTests () {
        const myModel = this.model;
        const answers = [{ text: 'answer 1' }, { text: 'answer2' }, { text: 'answer3' }];
        myModel.set('answerFunc', 'testAnswerFunc');

        LF.Widget.ParamFunctions.testAnswerFunc = () => answers;

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        spyOn(widget.model, 'set');
        spyOn(LF.Widget.ParamFunctions, 'testAnswerFunc').and.callThrough();

        return widget.setupAnswers()
            .then(() => {
                expect(LF.Widget.ParamFunctions.testAnswerFunc.calls.first().object).toBe(widget);
                expect(widget.model.set).toHaveBeenCalledWith('answers', answers);
            });
    }

    /* Parameter function tests */
    processLamdbaFunc () {
        const myModel = this.model;
        let f = () => {
            return new Q.Promise((resolve) => {
                resolve('3');
            });
        };

        myModel.set('testAttr', f);

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.setupModel()
            .then(() => {
                let a = myModel.get('testAttr');
                expect(a).toBe('3');
            });
    }

    processLFFunc () {
        const myModel = this.model;
        myModel.set('testAttr', 'paramFunc');

        LF.Widget.ParamFunctions.paramFunc = () => {
            return new Q.Promise((resolve) => {
                resolve('3');
            });
        };

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.setupModel()
            .then(() => {
                let a = myModel.get('testAttr');
                expect(a).toBe('3');
            })
            .finally(() => {
                LF.Widget.ParamFunctions.paramFunc = undefined;
            });
    }

    processNoFunc () {
        const myModel = this.model;
        myModel.set('testAttr', '3');

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.setupModel()
            .then(() => {
                let a = myModel.get('testAttr');
                expect(a).toBe('3');
            });
    }

    processNoPromiseFunc () {
        const myModel = this.model;
        const answer = '3';

        let f = () => {
            return answer;
        };
        myModel.set('testAttr', f);

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.setupModel()
            .then(() => {
                let a = myModel.get('testAttr');
                expect(a).toBe(answer);
            });
    }

    processPromiseFunc () {
        const myModel = this.model;
        const answer = '3';

        let f = () => {
            return new Q.Promise((resolve) => {
                resolve(answer);
            });
        };
        myModel.set('testAttr', f);

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.setupModel()
            .then(() => {
                let a = myModel.get('testAttr');
                expect(a).toBe(answer);
            });
    }

    // Check the object recursion on multiple levels with the same key, running twice on the same model.
    processRecursiveMultiTest () {
        const myModel = this.model,
            constVal = 'const value';

        let answerIndex = 0;

        let f = () => {
            answerIndex++;
            return answerIndex;
        };

        myModel.set('testObj', {
            path: {
                containing: {
                    func: {
                        testKey: f
                    }
                },
                without: {
                    func: {
                        testKey: constVal
                    }
                }
            }
        });

        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.setupModel().then(() => {
            expect(myModel.get('testObj').path.containing.func.testKey).toBe(1, 'run 1, answer with function');
            expect(myModel.get('testObj').path.without.func.testKey).toBe(constVal, 'run 1, value without function');
        }).then(() => {
            return widget.setupModel();
        }).then(() => {
            expect(myModel.get('testObj').path.containing.func.testKey).toBe(2, 'run 2, answer with function');
            expect(myModel.get('testObj').path.without.func.testKey).toBe(constVal, 'run 2, value without function');
        });
    }

    /* End parameter function tests */

    /**
     * verifies that after construction that the widget's parents question property works and that it
     * returns the parent that was passed in
     * @return {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    parentTest () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        expect(widget.question.id).toBe(this.dummyParent.id);
        return Q();
    }

    /**
     * verifies that respond() returns a promise and that the promise completes.
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    respondReturnsPromise () {
        const delegateEventSplitter = /^(\S+)\s*(.*)$/;
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                let mockEvent = $.Event('click', { target: widget.$el, currentTarget: widget.$el });
                if (widget.events) {
                    for (let key in widget.events) {
                        if (widget.events[key] === 'respond') {
                            const match = key.match(delegateEventSplitter),
                                eventName = match[1],
                                selector = match[2];

                            if (selector !== '') {
                                mockEvent = { target: widget.$el.find(selector)[0] };
                                break;
                            }
                        }
                    }
                }

                const respondResult = widget.respond(mockEvent);
                expect(respondResult).toBePromise();
                return respondResult;
            });
    }

    /**
     * verifies that render() returns a promise and that the promise completes.
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    renderReturnsPromise () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        let renderPromise = widget.render();

        expect(renderPromise).toBePromise();

        // for some
        return renderPromise;
    }

    /**
     * checks to see that after a render, an HTML element exists.  Bse check verifies object
     *  with the ID of the Model.id
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find(`#${myModel.id}`).length).toEqual(1);
            });
    }

    /**
     * verifies if mandatory and user hasn't put anything in, that it fails.
     */
    mandatoryTests () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: true,
            parent: this.dummyParent
        });

        return widget.render()
            .then(() => {
                expect(widget.validate()).toBeFalsy('if widget not set and mandatory = true, then would expect validate() to be false.');
            })
            .then(() => {
                widget = new LF.Widget[myModel.get('type')]({
                    model: myModel,
                    mandatory: false,
                    parent: this.dummyParent
                });

                return widget.render();
            })
            .then(() => {
                expect(widget.validate()).toBeTruthy('if widget not set and mandatory = false, then would expect validate() to be true.');
            });
    }

    /**
     * displayError() doesn't display and error if valid (no error)
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorWhenValid () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        // spoof validate to return true;
        widget.validate = () => true;
        helpers.fakeGetStrings();
        spyOn(Banner, 'show');

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(false);
            });
    }

    /**
     * displayError() displays the generic REQUIRED_ANSWER if no specific error set
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorInvalidDefault () {
        const myModel = this.model;
        myModel.set('validationErrors', undefined);
        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        // spoof validate to return true;
        widget.validate = () => false;
        helpers.fakeGetStrings();

        spyOn(Banner, 'show');

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(true);
            })
            .tap(() => {
                expect(Banner.show).toHaveBeenCalledWith(jasmine.objectContaining({
                    text: ' REQUIRED_ANSWER',
                    type: 'error'
                }));
            });
    }

    /**
     * displayError() shows the first error if set
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorInvalidFirst () {
        const myModel = this.model;
        myModel.set('validationErrors', validationErrors);
        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        // spoof validate to return true;
        widget.validate = () => false;
        helpers.fakeGetStrings();

        // spies for error UI
        spyOn(Banner, 'show');
        spyOn(window, 'alert').and.stub();
        spyOn(NotifyView.prototype, 'show');

        widget.error1 = false;
        widget.error2 = true;
        widget.error3 = true;
        widget.error4 = true;

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(true);
            })
            .tap(() => {
                expect(NotifyView.prototype.show).toHaveBeenCalledWith({
                    header: 'ERROR_1_HEADER',
                    body: 'ERROR_1_BODY',
                    ok: 'ERROR_1_OK'
                });
            });
    }

    /**
     * displayError() shows the second (but not the first) error if set
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorInvalidSecond () {
        const myModel = this.model;
        myModel.set('validationErrors', validationErrors);
        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        // spoof validate to return true;
        widget.validate = () => false;
        helpers.fakeGetStrings();

        // spies for error UI
        spyOn(Banner, 'show');
        spyOn(window, 'alert').and.stub();
        spyOn(NotifyView.prototype, 'show');

        widget.error1 = true;
        widget.error2 = false;
        widget.error3 = true;
        widget.error4 = true;

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(true);
            });
    }

    /**
     * displayError() shows the third error if set
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorInvalidThird () {
        const myModel = this.model;
        myModel.set('validationErrors', validationErrors);
        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        // spoof validate to return true;
        widget.validate = () => false;
        helpers.fakeGetStrings();

        // spies for error UI
        spyOn(Banner, 'show');
        spyOn(window, 'alert').and.stub();
        spyOn(NotifyView.prototype, 'show');

        widget.error1 = true;
        widget.error2 = true;
        widget.error3 = false;
        widget.error4 = true;

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(true);
            })
            .tap(() => {
                expect(Banner.show).toHaveBeenCalledWith({ text: 'ERROR_3_BODY', type: 'error' });
            });
    }

    /**
     * displayError() shows the fourth error if set
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorInvalidFourth () {
        const myModel = this.model;
        myModel.set('validationErrors', validationErrors);
        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        // spoof validate to return true;
        widget.validate = () => false;
        helpers.fakeGetStrings();

        // spies for error UI
        spyOn(Banner, 'show');
        spyOn(window, 'alert').and.stub();
        spyOn(NotifyView.prototype, 'show');

        widget.error1 = true;
        widget.error2 = true;
        widget.error3 = true;
        widget.error4 = false;

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(true);
            })
            .tap(() => {
                expect(Banner.show).toHaveBeenCalledWith({ text: 'ERROR_4_BODY', type: 'error' });
            });
    }

    /**
     * displayError() shows the first error if multiple are set
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    displayErrorMultiple () {
        const myModel = this.model;
        myModel.set('validationErrors', validationErrors);
        const widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        // spoof validate to return true;
        widget.validate = () => false;
        helpers.fakeGetStrings();

        // spies for error UI
        spyOn(Banner, 'show');
        spyOn(window, 'alert').and.stub();
        spyOn(NotifyView.prototype, 'show');

        widget.error1 = true;
        widget.error2 = true;
        widget.error3 = false;
        widget.error4 = false;

        return widget.displayError()
            .then((displayed) => {
                expect(displayed).toBe(true);
            })
            .tap(() => {
                expect(Banner.show).toHaveBeenCalledWith({ text: 'ERROR_3_BODY', type: 'error' });
                expect(Banner.show.calls.count()).toBe(1);
            });
    }

    saveAnswerTypeWithValue () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        let answer = widget.addAnswer();

        spyOn(AnswerBase.prototype, 'set').and.callThrough();

        widget.getAnswerType = () => 'typeValue';

        widget.saveAnswerType(answer);
        expect(AnswerBase.prototype.set).toHaveBeenCalledWith({ type: 'typeValue' });
    }

    saveAnswerTypeWithoutValue () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        let answer = widget.addAnswer();

        spyOn(AnswerBase.prototype, 'set').and.callThrough();

        widget.saveAnswerType(answer);
        expect(AnswerBase.prototype.set).not.toHaveBeenCalled();
    }

    addAnswerCallsSaveAnswerType () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        spyOn(WidgetBase.prototype, 'saveAnswerType').and.callThrough();

        let answer = widget.addAnswer();

        expect(WidgetBase.prototype.saveAnswerType).toHaveBeenCalledWith(answer);
    }
}
