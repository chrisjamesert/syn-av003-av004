import WidgetBaseTests from './WidgetBase.specBase';
import TextBoxTests from './TextBox.specBase';
import TextBox from 'core/widgets/TextBox';
import Widget from 'core/models/Widget';
import Session from 'core/classes/Session';
import User from 'core/models/User';

describe('TextBoxTests', () => {
    let baseTests = new TextBoxTests(),
        dummyParent = WidgetBaseTests.getMinDummyQuestion(),
        model = new Widget({
            id: 'DAILY_DIARY_W_0',
            type: 'TextBox',
            className: 'DAILY_DIARY_W_0',
            templates: {},
            answers: []
        });

    beforeAll(() => {
        LF.security = new Session();
        LF.security.activeUser = new User({
            id: 1,
            role: 'admin'
        });
    });

    baseTests.execTests();

    describe('respond method', () => {
        Async.it('should save the response as an answer', () => {
            let textValue = 'some-value',
                textBox = new TextBox({
                    model,
                    mandatory: false,
                    parent: dummyParent
                });

            return textBox.render()
            .then(() => {
                // We can't spy on property with current version of jasmine. So let's set the value in DOM
                //$($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(textValue);
                textBox.value = textValue;

                expect(textBox.answers.size()).toEqual(0);
                return textBox.respond();
            })
            .then(() => {
                expect(textBox.answers.size()).toEqual(1);
                expect(textBox.answer.get('response')).toEqual(textValue);
            });
        });
    });
});
