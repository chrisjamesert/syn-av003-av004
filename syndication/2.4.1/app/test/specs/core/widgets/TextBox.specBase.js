import TextBox from 'core/widgets/TextBox';
import TextBoxWidgetBase from 'core/widgets/TextBoxWidgetBase';
import WidgetBase from 'core/widgets/WidgetBase';

import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Answers from 'core/collections/Answers';
import Dashboard from 'core/models/Dashboard';
import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Widget from 'core/models/Widget';
import Templates from 'core/collections/Templates';

import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';

import 'core/resources/Templates';
import WidgetBaseTests from './WidgetBase.specBase';


export default class TextBoxTests extends WidgetBaseTests {
    constructor (model = TextBoxTests.model) {
        super(model);
    }
    static get model () {
        return new Widget({
            id: 'DAILY_DIARY_W_0',
            type: 'TextBox',
            className: 'DAILY_DIARY_W_0',
            templates: {},
            answers: []
        });
    }

    get model () {
        return this.constructor.model;
    }

    /**
     * a valid first character.  used for tests that set the value of the control (respond, deletion tests, etc)
     */
    get validEntry () {
        return 'ABC';
    }

    /**
     * a valid first character.  used for tests that set the value of the control (respond, deletion tests, etc)
     */
    get invalidEntry () {
        return 'Abc';
    }

    /**
     * executes all the tests for this file
     */
    execTests () {
        super.execTests();
        describe('TextBox Widget', () => {
            let security,
                widgetBaseQuestion,
                dummyParent;

            beforeAll(() => {
                security = LF.security;
                LF.security = WidgetBaseTests.getDummySecurity();
            });

            afterAll(() => {
                LF.security = security;
            });

            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            const minModel = new Widget({
                type: 'TextBox',
                id: 'dummyTextBox'
            });

            let baseTests = new WidgetBaseTests(minModel);

            baseTests.execTests();

            describe('Event Handling', () => {
                Async.it('calls keyPressed handler', () => {
                    const model = this.model,
                        widget = new LF.Widget[model.get('type')]({
                            model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    spyOn(widget, 'keyPressed').and.callThrough();
                    return widget.render()
                        .then(() => {
                            let e;

                            // When triggering click through code, you must trigger twice to register it
                            e = jQuery.Event('keypress');
                            e.which = 50; // # Some key code value
                            e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                            $(`#${dummyParent.id}`).find(`#${model.get('id')}`).trigger(e);
                            expect(widget.keyPressed).toHaveBeenCalledWith(e);
                        });
                });

                Async.it('#input calls sanitize handler', () => {
                    const model = this.model,
                        widget = new LF.Widget[model.get('type')]({
                            model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    spyOn(widget, 'sanitize').and.callThrough();
                    return widget.render()
                        .then(() => {
                            let e;
                            widget.delegateEvents();
                            e = jQuery.Event('input');
                            e.which = 50; // # Some key code value
                            e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                            $(`#${dummyParent.id}`).find(`#${model.get('id')}`).trigger(e);
                            expect(widget.sanitize).toHaveBeenCalledWith(e);
                        });
                });
            });
            describe('TextBox methods', () => {
                describe('#sanitize()', () => {
                    Async.it('if val is blank, respond saves answer', () => {
                        const model = this.model,
                            widget = new LF.Widget[model.get('type')]({
                                model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        spyOn(widget, 'respond').and.callThrough();
                        return widget.render()
                            .then(() => {
                                let e;
                                const testVal = '';

                                $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(testVal);

                                e = jQuery.Event('input');
                                e.which = 50; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.sanitize(e);
                                expect(widget.respond.calls.count()).toEqual(1);
                                expect(widget.answer).toBe(undefined);
                            });
                    });

                    Async.it('calls respond and super if val is not blank', () => {
                        const model = this.model,
                            widget = new LF.Widget[model.get('type')]({
                                model,
                                mandatory: false,
                                parent: dummyParent
                            });

                        return widget.render()
                            .then(() => {
                                let e;
                                const testVal = this.validEntry;

                                spyOn(widget, 'respond').and.callThrough();

                                // Set a value
                                $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(testVal);

                                e = jQuery.Event('input');
                                e.which = 50; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.sanitize(e);
                                expect(widget.respond).toHaveBeenCalledWith(e);
                            });
                    });
                });

                describe('#keyPressed()', () => {
                    Async.it('calls checkEnteredKey() with a string representation of our key', () => {
                        const model = this.model,
                            widget = new LF.Widget[model.get('type')]({
                                model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        spyOn(widget, 'checkEnteredKey').and.returnValue(false);

                        return widget.render()
                            .then(() => {
                                let e;

                                // Set a value
                                $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(this.validEntry);

                                e = jQuery.Event('keypress');
                                e.which = 90; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.keyPressed(e);

                                expect(widget.checkEnteredKey).toHaveBeenCalledWith('Z');
                            });
                    });

                    Async.it('cancels the press if checkEnteredKey returns false', () => {
                        const model = this.model,
                            widget = new LF.Widget[model.get('type')]({
                                model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        spyOn(widget, 'keyPressed').and.callThrough();
                        spyOn(widget, 'checkEnteredKey').and.returnValue(false);

                        return widget.render()
                            .then(() => {
                                let e;

                                // Set a value
                                $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(this.validEntry);

                                e = jQuery.Event('keypress');
                                e.which = 90; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.keyPressed(e);

                                expect(e.returnValue).toBe(false);
                            });
                    });

                    Async.it("doesn't cancel the press if checkEnteredKey returns true", () => {
                        const model = this.model,
                            widget = new LF.Widget[model.get('type')]({
                                model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        spyOn(widget, 'keyPressed').and.callThrough();
                        spyOn(widget, 'checkEnteredKey').and.returnValue(true);

                        return widget.render()
                            .then(() => {
                                let e;

                                // Set a value
                                $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(this.validEntry);

                                e = jQuery.Event('keypress');
                                e.which = 90; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.keyPressed(e);

                                expect(e.returnValue).toBe(true);
                            });
                    });

                    Async.it('shortcuts on keyCodes 8, 9, and 229', () => {
                        const model = this.model,
                            widget = new LF.Widget[model.get('type')]({
                                model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        spyOn(widget, 'keyPressed').and.callThrough();
                        spyOn(widget, 'checkEnteredKey').and.returnValue(false);

                        return widget.render()
                            .then(() => {
                                let e;

                                // Set a value
                                $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]).val(this.validEntry);

                                e = jQuery.Event('keypress');
                                e.which = 8; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.keyPressed(e);

                                e = jQuery.Event('keypress');
                                e.which = 9; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.keyPressed(e);

                                e = jQuery.Event('keypress');
                                e.which = 229; // # Some key code value
                                e.target = $(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0];
                                widget.keyPressed(e);

                                expect(widget.checkEnteredKey.calls.count()).toBe(0);
                            });
                    });
                });
                Async.it('Enter valid entry, then remove all data with mandatory - should return false', () => {
                    const model = this.model,
                        widget = new LF.Widget[model.get('type')]({
                            model,
                            mandatory: true,
                            parent: dummyParent
                        });
                    spyOn(widget, 'checkEnteredKey').and.returnValue(false);

                    let ctrl;
                    const e = jQuery.Event('input');
                    return widget.render()
                        .then(() => {
                            ctrl = $($(`#${dummyParent.id}`).find(`#${model.get('id')}`)[0]);

                            // Set a value
                            ctrl.val(this.validEntry);
                            e.target = ctrl;
                            return widget.respond(e);
                        })
                        .then(() => {
                            // clear the value
                            ctrl.val('');
                            return widget.respond(e);
                        })
                        .then(() => {
                            expect(widget.answers.length).toBe(0);
                            expect(widget.answer).toBeUndefined();
                            expect(widget.validate()).toBeFalsy('expected validate would be false');
                        });
                });
            });
        });
    }
}
