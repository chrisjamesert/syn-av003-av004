import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Answers from 'core/collections/Answers';
import User from 'core/models/User';
import Dashboard from 'core/models/Dashboard';
import Subject from 'core/models/Subject';
import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Widget from 'core/models/Widget';
import WidgetBase from 'core/widgets/WidgetBase';
import BarcodeScanner from 'core/widgets/BarcodeScanner';
import Logger from 'core/Logger';

import QuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import 'core/resources/Templates';
import WidgetBaseTests from './WidgetBase.specBase';


class BarcodeCoreTests extends WidgetBaseTests {
    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('.scannerData').length).toEqual(1);
            });
    }
}
//jscs:disable requireArrowFunctions
describe('BarcodeScanner', function () {
    const model = new Widget({
        type: 'BarcodeScanner',
        id: 'testWidgetId'
    });
    const baseTests = new BarcodeCoreTests(model);

    baseTests.execTests();

    describe('Tests with a mock questionnaire', function () {
        let modelQuestion,
            modelDashboard,
            questionnaires = LF.StudyDesign.questionnaires,
            viewQuestionnaire,
            view,
            modelWidget,
            barcodeWidget,
            template;

        beforeEach(function () {
            LF.security = {
                activeUser: new User({
                    username: 'mark.matthews',
                    password: '1111',
                    salt: 'mark.matthews',
                    secretQuestion: 'mark.matthews',
                    secretAnswer: 'mark.matthews',
                    language: 'en-US',
                    role: 'siteUser',
                    failedLoginAttempts: 5,
                    lastLogin: '1234'
                })
            };

            LF.Data.Questionnaire = {};
            LF.StudyDesign.questionnaires = new Questionnaires([
                {
                    id: 'BARCODE_DIARY',
                    SU: 'BARCODE_DIARY',
                    displayName: 'DISPLAY_NAME',
                    className: 'BARCODE_DIARY',
                    previousScreen: true,
                    screens: ['BARCODE_DIARY_S_1', 'BARCODE_DIARY_S_2', 'BARCODE_DIARY_S_3']
                }
            ]);

            modelQuestion = new Question({
                id: 'LogPad_Q_1',
                IG: 'LogPad',
                IT: 'LogPad_Q_1',
                text: 'QUESTION_1',
                className: 'LogPad_Q_1',
                widget: {
                    type: 'BarcodeScanner'
                }
            });

            modelDashboard = new Dashboard({
                instance_ordinal: 1
            });

            viewQuestionnaire = new QuestionnaireView({
                id: 'BARCODE_DIARY'
            });

            view = new QuestionView({
                id: modelQuestion.get('id'),
                screen: 'BARCODE_DIARY',
                model: modelQuestion,
                parent: viewQuestionnaire,
                mandatory: false
            });

            modelWidget = new Widget({
                id: 'BARCODE_DIARY_W_1',
                type: 'BarcodeScanner',
                className: 'BARCODE_DIARY_W_1',
                regexToMatch: /^ABC/i
            });

            barcodeWidget = new LF.Widget.BarcodeScanner({
                model: modelWidget,
                answers: new LF.Collection.Answers(),
                parent: view,
                validation: {
                    validationFunc: 'testFunction'
                }
            });

            template = '<div id="questionnaire-template">' +
                '<div id="questionnaire"></div>' +
                '</div>';

            viewQuestionnaire.data.dashboard = modelDashboard;
            viewQuestionnaire.data.answers = new LF.Collection.Answers();
            viewQuestionnaire.data.igList = { LogPad: 0 };
            viewQuestionnaire.data.currentIGR = { LogPad: 0 };

            LF.SpecHelpers.createSubject();

            $('body').append(template);
        });

        afterEach(function () {
            LF.router = undefined;
            LF.security = undefined;
            LF.schedule = undefined;
            LF.Data = {};
            LF.dataAccess = undefined;
            LF.StudyDesign.questionnaires = questionnaires;

            localStorage.clear();

            $('#questionnaire-template').remove();
        });

        it('should render correctly', function (done) {
            $('#questionnaire').append(barcodeWidget.parent.el);
            barcodeWidget.render().then(() => {
                expect($('.BARCODE_DIARY_W_1')).toBeTruthy();
                expect($('.scanBtn').hasClass('hidden')).toBeTruthy();
            }).catch((e) => {
                fail(`unexpected exception: ${e}`);
            }).done(done);
        });

        it('should not complete the question', function (done) {
            $('#questionnaire').append(barcodeWidget.parent.el);
            barcodeWidget.render().then(() => {
                barcodeWidget.manualInputChange();
                expect(barcodeWidget.completed).toEqual(false);
            }).catch((e) => {
                fail(`unexpected exception: ${e}`);
            }).done(done);
        });

        it('should set correct answer', function (done) {
            $('#questionnaire').append(barcodeWidget.parent.el);
            barcodeWidget.render().then(() => {
                barcodeWidget.$('.scannerData').val('ok');
                barcodeWidget.manualInputChange();

                expect(barcodeWidget.answer.get('response')).toEqual('ok');
            }).catch((e) => {
                fail(`unexpected exception: ${e}`);
            }).done(done);
        });

        it('should trigger the next button', function () {
            let e = document.createEvent('KeyboardEvent'),
                nextHandler = spyOn(viewQuestionnaire, 'nextHandler');

            barcodeWidget.goToNextQuestion(e);

            expect(nextHandler).toHaveBeenCalled();
        });

        it('should populate the scanned value field with the answer if the answer is available on render',
            function (done) {
                $('#questionnaire').append(barcodeWidget.parent.el);
                barcodeWidget.render().then(() => {
                    barcodeWidget.$('.scannerData').val('ok');
                    barcodeWidget.manualInputChange();
                    barcodeWidget.$el.empty();

                    expect(barcodeWidget.answer.get('response')).toEqual('ok');
                }).then(() => barcodeWidget.render()
                ).then(() => {
                    let scannedVal = barcodeWidget.$('.scannerData').val();
                    expect(scannedVal).toEqual('ok');
                }).catch((e) => {
                    fail(`unexpected exception: ${e}`);
                }).done(done);
            });

        it('should not show the manual text box when hideInput is true', function (done) {

            // reset the widget to tell it to hide the text box
            let overrideModelWidget = new Widget({
                    id: 'BARCODE_DIARY_W_1',
                    type: 'BarcodeScanner',
                    className: 'BARCODE_DIARY_W_1',
                    hideManualInput: true
                }),
                barcodeWidget = new LF.Widget.BarcodeScanner({
                    model: overrideModelWidget,
                    answers: new LF.Collection.Answers(),
                    parent: view,
                    validation: {
                        validationFunc: 'testFunction'
                    }
                });

            $('#questionnaire').append(barcodeWidget.parent.el);
            barcodeWidget.render().then(() => {
                let hidden_value = barcodeWidget.$('.scannerData').is(':hidden');
                expect(hidden_value).toEqual(true);
            }).catch((e) => {
                fail(`unexpected exception: ${e}`);
            }).done(done);
        });

        it('should show the manual text box by default', function (done) {

            $('#questionnaire').append(barcodeWidget.parent.el);
            barcodeWidget.render().then(() => {
                let hidden_value = barcodeWidget.$('.scannerData').is(':hidden');
                expect(hidden_value).toEqual(false);
            }).catch((e) => {
                fail(`unexpected exception: ${e}`);
            }).done(done);
        });

        it('should read the value from the plugin, populate the input field and set the correct answer',
            function (done) {
                let response = false;

                // Fake the wrapper as if it was running on a device
                LF.Wrapper.savePlatform('test-platform');

                // Create fake plugin interface
                window.cordova = {};
                window.cordova.plugins = {};
                window.cordova.plugins.barcodeScanner = {};
                window.cordova.plugins.barcodeScanner.scan = function (callback) {
                    callback({text: 'ok'});
                    response = true;
                };

                $('#questionnaire').append(barcodeWidget.parent.el);
                barcodeWidget.render()
                    .then(() => barcodeWidget.scan()).then(
                    () => {
                        expect(barcodeWidget.$('.scannerData').val()).toEqual('ok');
                        expect(barcodeWidget.answer.get('response')).toEqual('ok');
                    }).catch(
                    (e) => {
                        fail(e);
                    }).finally(
                    () => {
                        // Undo fake platform and fake plugin interface
                        LF.Wrapper.savePlatform(undefined);
                        delete window.cordova;
                    }).done(done);
            });

        it('returns false when barcode value does not match regex', function (done) {
            // Create fake plugin interface
            window.cordova = {};
            window.cordova.plugins = {};
            window.cordova.plugins.barcodeScanner = {};
            window.cordova.plugins.barcodeScanner.scan = function (callback) {
                callback({text: 'missing_regex_value'});
            };

            barcodeWidget.render()
                .then(() => barcodeWidget.scan()).then(
                () => {
                    let scannedValue = barcodeWidget.$('.scannerData').val();
                    expect(scannedValue).toEqual('missing_regex_value');
                    barcodeWidget.doesScannedMatchRegex(scannedValue);
                    expect(barcodeWidget.matchesRegex).toEqual(false);
                }).catch(
                (e) => {
                    fail(e);
                }).finally(
                () => {
                    // Undo fake platform and fake plugin interface
                    LF.Wrapper.savePlatform(undefined);
                    delete window.cordova;
                }).done(done);

        });

        it('returns true when barcode value matches the regex', function (done) {
            // Create fake plugin interface
            window.cordova = {};
            window.cordova.plugins = {};
            window.cordova.plugins.barcodeScanner = {};
            window.cordova.plugins.barcodeScanner.scan = function (callback) {
                callback({text: 'ABCdefgh'});
            };

            barcodeWidget.render()
                .then(() => barcodeWidget.scan()).then(
                () => {
                    let scannedValue = barcodeWidget.$('.scannerData').val();
                    expect(scannedValue).toEqual('ABCdefgh');
                    barcodeWidget.doesScannedMatchRegex(scannedValue);
                    expect(barcodeWidget.matchesRegex).toEqual(true);
                }).catch(
                (e) => {
                    fail(e);
                }).finally(
                () => {
                    // Undo fake platform and fake plugin interface
                    LF.Wrapper.savePlatform(undefined);
                    delete window.cordova;
                }).done(done);

        });

    });


    describe('Functional unit testing', function () {
        describe('validate function', function () {
            let scanner;
            beforeEach(() => {
                spyOn(WidgetBase.prototype, 'constructor');
                scanner = new BarcodeScanner();
            });
            it('returns false when super is false', function () {
                spyOn(WidgetBase.prototype, 'validate').and.returnValue(false);
                scanner.matchesRegex = true;
                let testVal = scanner.validate();
                expect(testVal).toBe(false);
            });

            it('returns true when super is true', function () {
                spyOn(WidgetBase.prototype, 'validate').and.returnValue(true);
                scanner.matchesRegex = true;
                let testVal = scanner.validate();
                expect(testVal).toBe(true);
            });

            it('calls super validate', function () {
                spyOn(WidgetBase.prototype, 'validate');
                scanner.validate();
                expect(WidgetBase.prototype.validate).toHaveBeenCalled();
            });
        });
    });
});
