import 'test/helpers/StudyDesign';
import Widget from 'core/models/Widget';
import DateTimePicker from 'core/widgets/DateTimePicker';
import WidgetBaseTests from './WidgetBase.specBase';
import Templates from 'core/collections/Templates';

describe('DateTimePicker', () => {
    let templates,
        dummyParent,
        security;
    beforeAll(() => {
        templates = LF.templates;
        LF.templates = new Templates([{
            name: 'DatePicker',
            namespace: 'DEFAULT',
            template: `<input id="{{ id }}" class="date-input" min="{{ min }}" max="{{ max }}"
                                           data-role="datebox" data-options=\'{{ configuration }}\' />`
        }, {
            name: 'DatePickerLabel',
            namespace: 'DEFAULT',
            template: '<label for="{{ id }}_date_input">{{ dateLabel }}</label>'
        }, {
            name: 'TimePicker',
            namespace: 'DEFAULT',
            template: '<input id="{{ id }}_time_input" class="form-control time-input"  data-role="timebox" />'
        }, {
            name: 'TimePickerLabel',
            namespace: 'DEFAULT',
            template: '<label for="{{ id }}_time_input">{{ timeLabel }}</label>'
        }]);
    });
    beforeAll(() => {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();
    });

    afterAll(() => {
        LF.security = security;
    });
    afterAll(() => {
        LF.templates = templates;
    });
    beforeEach(() => {
        dummyParent = WidgetBaseTests.getMinDummyQuestion();
    });
    afterEach(() => {
        $(`#${dummyParent.getQuestionnaire().id}`).remove();
    });

    const modelWidget = () => {
        return new Widget({
            id: 'DATE_DIARY_W_1',
            type: 'DateTimePicker',
            className: 'DATE_DIARY_W_1',
            showLabels: true,
            templates: {},
            configuration: {
                max: '2016-01-14T15:21:00'
            },
            configFunc: 'dateTimePickerUnitTest'
        });
    };
    const baseTests = new WidgetBaseTests(modelWidget());

    baseTests.execTests();

    it('should have undefined localizedDate', () => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        expect(widget.localizedDate).toBeUndefined();
    });

    // noinspection JSUnresolvedFunction
    it('should have undefined localizedTime', () => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        expect(widget.localizedTime).toBeUndefined();
    });

    // noinspection JSUnresolvedFunction
    it('should return the correct value', () => {
        let date = new Date('2015', '04', '11', '12', '23', '11');
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        expect(widget.buildStudyWorksString(date)).toEqual('11 May 2015 12:23:00');
    });

    it('should set the answer model to the widget', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedDate = 'Thursday, January 06, 2000';
        widget.localizedTime = '12:11';
        let date = new Date();
        date.setFullYear(2000, 0, 6);
        widget.dateObj = date;
        widget.render()
            .then(() => {
                widget.respond({}, {});
            })
            .tap(() => {
                expect(widget.answer.get('response')).toEqual('06 Jan 2000 12:11:00');
            })
            .catch(e => fail(e))
            .done(done);
    });

    it('should set the correct value to datebox', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedDate = 'Friday, April 03, 2015';
        widget.render()
                .then(() => {
                    expect($('input.date-input').val()).toEqual('Friday, April 03, 2015');
                })
                .catch(e => fail(e))
                .done(done);
    });

    it('should set the correct value to timebox', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedTime = '11:21';
        widget.render()
            .tap(() => {
                expect($('input.time-input').val()).toEqual('11:21');
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    });

    it('should set the correct value to datetimeVal', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedDate = 'Friday, April 3, 2015';
        widget.localizedTime = '11:21';
        let date = new Date();
        date.setFullYear(2015, 3, 3);
        widget.dateObj = date;
        widget.render()
            .then(() => {
                widget.respond({}, {});
            })
            .tap(() => {
                expect(widget.datetimeVal).toEqual(new Date(2015, 3, 3, 11, 21, 0, 0));
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    });

    it('should update datetimeVal with date and time on date input', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedDate = 'Friday, January 03, 2016';
        widget.localizedTime = '10:21';
        let date = new Date('2015', '04', '11', '12', '23', '00');
        widget.render()
            .then(() => {
                widget.$el.find('input.date-input').val(date.toDateString());
                widget.$el.find('input.date-input').datebox('setTheDate', date);
                widget.respond({}, {});
            })
            .tap(() => {
                expect(widget.datetimeVal.toDateString()).toEqual(date.toDateString());
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    }, 400);

    it('should update datetimeVal with time on time input', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedDate = 'Friday, November 13, 2015';
        widget.localizedTime = '10:21';
        let date = new Date('2015', '04', '11', '13', '20', '00');
        widget.dateObj = date;
        widget.render()
            .then(() => {
                widget.$el.find('input.time-input').val(`${date.getHours()}:${date.getMinutes()}`);
                widget.respond({}, {});
            })
            .tap(() => {
                expect(widget.datetimeVal.getHours()).toEqual(date.getHours());
                expect(widget.datetimeVal.getMinutes()).toEqual(date.getMinutes());
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    }, 400);

    it('should render labels if showLabels true', (done) => {
        const model = modelWidget();

        model.set('showLabels', true);
        const widget = new LF.Widget[model.get('type')]({
            model,
            mandatory: false,
            parent: dummyParent
        });
        widget.render()
            .tap(() => {
                expect(widget.$el.find('label').length).toEqual(2);
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    }, 400);

    it('shouldn\'t render labels if showLabels omitted', (done) => {
        const model = modelWidget();

        model.set('showLabels', undefined);
        const widget = new LF.Widget[model.get('type')]({
            model,
            mandatory: false,
            parent: dummyParent
        });
        widget.render()
            .tap(() => {
                expect($('#questionnaire label').length).toEqual(0);
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    }, 400);

    it('should clear saved input field data on skip', (done) => {
        const model = modelWidget(),
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
        widget.localizedDate = 'Friday, January 03, 2016';
        widget.localizedTime = '10:21';
        widget.render()
            .then(() => {
                widget.skip('LogPad_Q_1');
            })
            .tap(() => {
                expect(widget.localizedDate).toEqual('');
                expect(widget.localizedTime).toEqual('');
            })
            .catch(e => fail(e)) // w/o this test may not fail
            .done(done); // when promise done, test is done
    }, 400);

    TRACE_MATRIX('DE22851')
    .describe('#respond', () => {
        let model,
            widget,
            $dateInput,
            $timeInput;

        Async.beforeEach(() => {
            model = modelWidget();
            widget = new LF.Widget[model.get('type')]({
                model,
                mandatory: false,
                parent: dummyParent
            });
            return widget.render()
            .then(() => {
                $dateInput = widget.$('input.date-input');
                $timeInput = widget.$('input.time-input');

                $dateInput.val('29 Jan 2017');
                widget.dateObj = new Date('01 Jan 2017');
                $timeInput.val('13:30');

                spyOn($.fn, 'datebox').and.callFake((param) => {
                    if (param === 'getTheDate') {
                        return new Date('30 Jan 2017');
                    }
                });
            });
        });
        Async.it('uses cached date object (dateObj) as base date if target is not calendar', () => {
            return widget.respond({ target: $timeInput[0] })
            .then(() => {
                expect(widget.datetimeVal.toString()).toBe(new Date('01 Jan 2017 13:30').toString());
            });
        });

        Async.it('uses value from datebox if target is calendar', () => {
            return widget.respond({ target: $dateInput[0] })
            .then(() => {
                expect(widget.datetimeVal.toString()).toBe(new Date('30 Jan 2017 13:30').toString());
            });
        });
    });
});
