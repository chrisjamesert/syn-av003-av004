import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import NumberSpinner from 'core/widgets/NumberSpinner';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';
import ModalWidgetBaseTests from './ModalWidgetBase.spec.js';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';
import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';
import BaseSpinner from 'core/widgets/BaseSpinner';
import TextBox from 'core/widgets/TextBox';

const DEFAULT_CLASS_NAME = 'Spinner',
    DEFAULT_LABELS = {labelOne: '', labelTwo: ''};

/*global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class BaseSpinnerTests extends ModalWidgetBaseTests {
    get testValue1 () {
        return 'test1';
    }

    get displayValue1 () {
        return this.testValue1;
    }

    get testValue2 () {
        return 'test2';
    }

    get displayValue2 () {
        return this.testValue2;
    }

    constructor (model) {
        if (!(model instanceof Widget)) {
            throw new Error('Invalid use of BaseSpinnerTests.  Must be tested with an implementation of the abstract class.');
        }
        super(model);
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US6106').
        describe('BaseSpinner', () => {
            let templates,
                dummyParent,
                security,
                spinnerWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();

                // Remove the backdrop in case it was open while we exited.
                $('.modal-backdrop').remove();
                spinnerWidget = null;
            });

            describe('#getters', () => {
                let testTemplate,
                    baseSpinner;
                describe('#defaultSpinnerTemplate', () => {
                    it('Throws an error if unimplemented by instance', () => {
                        baseSpinner = new BaseSpinner({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(() => {
                            testTemplate = baseSpinner.defaultSpinnerTemplate;
                        }).toThrow(new Error('Unimplemented defaultSpinnerTemplate getter in a BaseSpinner implementation.'));
                    });
                });
                describe('#defaultSpinnerItemTemplate', () => {
                    it('Throws an error if unimplemented by instance', () => {
                        baseSpinner = new BaseSpinner({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(() => {
                            testTemplate = baseSpinner.defaultSpinnerItemTemplate;
                        }).toThrow(new Error('Unimplemented defaultSpinnerItemTemplate getter in a BaseSpinner implementation.'));
                    });
                });

                describe('#defaultClassName', () => {
                    it('returns correct default value', () => {
                        baseSpinner = new BaseSpinner({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(baseSpinner.defaultClassName).toBe(DEFAULT_CLASS_NAME);
                    });
                });

                describe('#defaultLabels', () => {
                    it('returns correct default value', () => {
                        baseSpinner = new BaseSpinner({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(baseSpinner.defaultLabels).toEqual(DEFAULT_LABELS);
                    });
                });

                describe('#spinners', () => {
                    it('interfaces with this.input', () => {
                        baseSpinner = new BaseSpinner({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        baseSpinner.spinners = ['test 1', 'test 2'];
                        expect(baseSpinner.inputs).toEqual(baseSpinner.spinners);                        
                    });
                });

                describe('#getSpinnerValuesArray', () => {
                    it('throws an exception if unimplemented in class', () => {
                        let baseSpinner = new BaseSpinner({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            }),
                            testVal;

                        expect(() => {
                            testVal = baseSpinner.getSpinnerValuesArray();
                        }).toThrow(new Error('Unimplemented getSpinnerValuesArray method in BaseSpinner implementation.'));
                    });
                });

                describe ('#getInputValuesArray', () => {
                    it('calls getSpinnerValuesArray', () => {
                        let baseSpinner = new BaseSpinner({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            }),
                            testVal;

                        spyOn(baseSpinner, 'getSpinnerValuesArray');
                        baseSpinner.getInputValuesArray();
                        expect(baseSpinner.getSpinnerValuesArray).toHaveBeenCalled();
                    });
                });

                describe('#injectSpinnerInputs', () => {
                    it('throws an exception if unimplemented in class', () => {
                        let baseSpinner = new BaseSpinner({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            }),
                            testVal;

                        expect(() => {
                            testVal = baseSpinner.injectSpinnerInputs();
                        }).toThrow(new Error('Unimplemented injectSpinnerInputs() method for a BaseSpinner Implementation.'));
                    });
                });

                describe('#injectSpinnerInputs', () => {
                    it('throws an exception if unimplemented in class', () => {
                        let baseSpinner = new BaseSpinner({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            }),
                            testVal;

                        expect(() => {
                            testVal = baseSpinner.injectSpinnerInputs();
                        }).toThrow(new Error('Unimplemented injectSpinnerInputs() method for a BaseSpinner Implementation.'));
                    });
                });

                describe('#injectModalInputs', () => {
                    it('calls injectSpinnerInputs', () => {
                        let baseSpinner = new BaseSpinner({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        spyOn(baseSpinner, 'injectSpinnerInputs');
                        baseSpinner.injectModalInputs();
                        expect(baseSpinner.injectSpinnerInputs).toHaveBeenCalled();                        
                    });
                });
            });

            describe('#constructor', () => {
                it('creates object of type BaseSpinner, with everything initialized', () => {
                    spinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(spinnerWidget instanceof BaseSpinner).toBe(true);
                    expect(spinnerWidget.spinners.length).toBe(0);
                });
            });

            describe('post-rendered methods', () => {
                beforeEach((done) => {

                    spinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    spinnerWidget.render()
                        .then(() => {
                            done();
                        })
                        .catch((e) => {
                            fail(e);
                        })
                        .done();
                });

                describe('#refreshSpinners', () => {
                    Async.it('calls getSpinnerValuesArray and pushes values to spinners', () => {
                        let vals = spinnerWidget.getSpinnerValuesArray();
                        expect(vals.length > 0).toBe(true);
                        expect(spinnerWidget.spinners.length).toBe(vals.length);

                        for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                            spyOn(spinnerWidget.spinners[i], 'show').and.callThrough();
                            spyOn(spinnerWidget.spinners[i], 'pushValue').and.callThrough();
                        }

                        return spinnerWidget.refreshSpinners()
                            .then(() => {
                                for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                                    expect(spinnerWidget.spinners[i].show.calls.count()).toBe(1);
                                    expect(spinnerWidget.spinners[i].pushValue.calls.count()).toBe(1);
                                    expect(spinnerWidget.spinners[i].value).toBe(vals[i]);
                                }
                            });
                    });
                });

                describe('#getSpinnerTemplate', () => {
                    it('returns spinnerTemplates from model if they exist, default if index is out of range', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: {
                                spinnerTemplates: [
                                    'test1',
                                    'test2'
                                ]
                            }
                        });
                        spinnerWidget.model = customModel;
                        expect(spinnerWidget.getSpinnerTemplate(0)).toBe('test1');
                        expect(spinnerWidget.getSpinnerTemplate(1)).toBe('test2');
                        expect(spinnerWidget.getSpinnerTemplate(2)).toBe(spinnerWidget.defaultSpinnerTemplate);
                    });

                    it('returns default spinner template if no templates defined', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: null
                        });
                        spinnerWidget.model = customModel;
                        expect(spinnerWidget.getSpinnerTemplate(0)).toBe(spinnerWidget.defaultSpinnerTemplate);
                    });
                });

                describe('#getSpinnerItemTemplate', () => {
                    it('returns spinnerItemTemplates if they exist in model, default if index is out of range', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: {
                                spinnerItemTemplates: [
                                    'test1',
                                    'test2'
                                ]
                            }
                        });
                        spinnerWidget.model = customModel;
                        expect(spinnerWidget.getSpinnerItemTemplate(0)).toBe('test1');
                        expect(spinnerWidget.getSpinnerItemTemplate(1)).toBe('test2');
                        expect(spinnerWidget.getSpinnerItemTemplate(2)).toBe(spinnerWidget.defaultSpinnerItemTemplate);
                    });

                    it('returns default spinner template if no templates defined', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: null
                        });
                        spinnerWidget.model = customModel;
                        expect(spinnerWidget.getSpinnerItemTemplate(0)).toBe(spinnerWidget.defaultSpinnerItemTemplate);
                    });
                });
            });
        });
    }
}
