import TextBox from 'core/widgets/TextBox';
import TextBoxWidgetBase from 'core/widgets/TextBoxWidgetBase';
import WidgetBase from 'core/widgets/WidgetBase';
import Widget from 'core/models/Widget';
import WidgetBaseTests from './WidgetBase.specBase';

describe('TextBoxWidgetBase', function () {

    // Simple textBox, used to check our sanitize functions, etc.
    let textBox;

    afterEach(() => {
        textBox = null;
    });

    const buildModel = () => {
        return new Widget({
            id: 'DAILY_DIARY_W_0',
            validateRegex: /[A-Z]{5}/
        });
    };
    describe('TextBoxWidgetBase methods', () => {
        describe('#sanitize()', () => {
            it('removes unaccepted characters from our entry string', (done) => {
                let model = buildModel(),
                widgetBase = new TextBoxWidgetBase({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });
                textBox = $('<input type="text" value="Here is a value for our text box."/>');

                textBox.ready(() => {
                    spyOn(widgetBase, 'checkEnteredKey').and.callFake((char) => {
                        // True if it isn't a space or a lowercase o.
                        return !(char === ' ' || char === 'o');
                    });

                    spyOn(textBox, 'val').and.callThrough();

                    widgetBase.sanitize(textBox);

                    expect(textBox.val.calls.count()).toBe(2); // 1 call to get the value during sanitize, one to set.
                    expect(textBox.val).toHaveBeenCalledWith('Hereisavaluefrurtextbx.');
                    expect(textBox.val()).toBe('Hereisavaluefrurtextbx.');
                    done();
                });
            });

            it('does not alter the uiElement if no change', () => {
                let widgetBase;

                spyOn(WidgetBase.prototype, 'constructor');
                spyOn(TextBoxWidgetBase.prototype, 'sanitize').and.callThrough();
                spyOn(TextBoxWidgetBase.prototype, 'checkEnteredKey').and.returnValue(true);

                widgetBase = new TextBoxWidgetBase({});
                textBox = $('<input type="text" value="Here is a value for our text box."/>');
                spyOn(textBox, 'val').and.callThrough();

                widgetBase.sanitize(textBox);

                expect(textBox.val.calls.count()).toBe(1); // 1 call to get the value, none to set
            });
        });

        describe('#checkEnteredKey()', () => {
            let fakeModel = {
                    allowedRegex: null,
                    disallowedRegex: null,

                    get: function (str) {
                        return this[str];
                    },
                    set: function (str, val) {
                        this[str] = val;
                    }
                },
                widgetBase;

            beforeEach((done) => {
                fakeModel.set('allowedRegex', /0-9A-Fa-f/);
                fakeModel.set('disallowedRegex', /5-9/);
                spyOn(WidgetBase.prototype, 'constructor');
                widgetBase = new TextBoxWidgetBase({});
                widgetBase.model = fakeModel;
                done();
            });

            it('is true: key in our allowed range and key not in disallowed range', () => {
                expect(widgetBase.checkEnteredKey('b')).toBe(true);
            });

            it('is true: allowed range doesn\'t exist, and key not in disallowed range', () => {
                widgetBase.model.set('allowedRegex', null);
                expect(widgetBase.checkEnteredKey('b')).toBe(true);
            });

            it('is true: allowed range doesn\'t exist, and disallowed range doesn\'t exist', () => {
                widgetBase.model.set('allowedRegex', null);
                widgetBase.model.set('disallowedRegex', null);
                expect(widgetBase.checkEnteredKey('b')).toBe(true);
            });

            it('is true: in allowed range, and disallowed range doesn\'t exist', () => {
                widgetBase.model.set('disallowedRegex', null);
                expect(widgetBase.checkEnteredKey('b')).toBe(true);
            });

            it('is false: key in our allowed range and also in disallowed range', () => {
                expect(widgetBase.checkEnteredKey('8')).toBe(true);
            });

            it('is false: allowed range doesn\'t exist and key is in disallowed range', () => {
                widgetBase.model.set('allowedRegex', null);
                expect(widgetBase.checkEnteredKey('8')).toBe(true);
            });

            it('is false: key not in allowed range and disallowed range doesn\'t exist', () => {
                widgetBase.model.set('disallowedRegex', null);
                expect(widgetBase.checkEnteredKey('Z')).toBe(true);
            });
        });
        describe('#respond()', () => {

            it('Sets completed to true if value is validated', () => {
                spyOn(WidgetBase.prototype, 'respondHelper').and.callThrough();
                const value = 'ABCDEF',
                    fakeAnswer = {
                        get: function (str) {
                            return this[str];
                        },
                        set: function (str, val) {
                            this[str] = val;
                        }
                    },
                    model = buildModel(),
                    widget = new TextBoxWidgetBase({
                        model: model,
                        mandatory: false,
                        parent: WidgetBaseTests.getMinDummyQuestion()
                    });

                widget.respondHelper(fakeAnswer, value, undefined);
                expect(widget.completed).toBeTruthy('Expected widget.completed to be true');
                expect(WidgetBase.prototype.respondHelper).toHaveBeenCalledWith(fakeAnswer, value, true);
            });
            it('Sets completed to false if value is not validated', () => {
                spyOn(WidgetBase.prototype, 'respondHelper');
                const fakeAnswer = {
                        get: function (str) {
                            return this[str];
                        },
                        set: function (str, val) {
                            this[str] = val;
                        }
                    },
                    model = buildModel(),
                    widget = new TextBoxWidgetBase({
                        model: model,
                        mandatory: false,
                        parent: WidgetBaseTests.getMinDummyQuestion()
                    }),
                    value = 'ABCD'; // only 4 chars, not 5: won't match

                spyOn(widget, 'removeAnswer');

                widget.respondHelper(fakeAnswer, value, undefined);
                expect(widget.completed).toBeFalsy('Expected widget.completed to be false');
                expect(widget.removeAnswer.calls.count()).toBe(0);
                expect(WidgetBase.prototype.respondHelper).toHaveBeenCalledWith(fakeAnswer, value, false);
            });

            it('Keeps completed as false if false is passed in', () => {
                spyOn(WidgetBase.prototype, 'respondHelper').and.callThrough();
                const value = 'ABCDEF',
                    fakeAnswer = {
                        get: function (str) {
                            return this[str];
                        },
                        set: function (str, val) {
                            this[str] = val;
                        }
                    },
                    model = buildModel(),
                    widget = new TextBoxWidgetBase({
                        model: model,
                        mandatory: false,
                        parent: WidgetBaseTests.getMinDummyQuestion()
                    });
                spyOn(widget, 'removeAnswer');

                widget.respondHelper(fakeAnswer, value, false);
                expect(widget.completed).toBeFalsy('Expected widget.completed to be false');
                expect(widget.removeAnswer.calls.count()).toBe(0);
                expect(WidgetBase.prototype.respondHelper).toHaveBeenCalledWith(fakeAnswer, value, false);
            });

            it('Removes answer and short circuits (doesn\'t call core respond) if empty string', () => {
                spyOn(WidgetBase.prototype, 'respondHelper').and.callThrough();
                const value = '',
                    fakeAnswer = {
                        get: function (str) {
                            return this[str];
                        },
                        set: function (str, val) {
                            this[str] = val;
                        }
                    },
                    model = buildModel(),
                    widget = new TextBoxWidgetBase({
                        model: model,
                        mandatory: false,
                        parent: WidgetBaseTests.getMinDummyQuestion()
                    });
                spyOn(widget, 'removeAnswer');

                widget.respondHelper(fakeAnswer, value, false);
                expect(widget.completed).toBeFalsy('Expected widget.completed to be false');
                expect(widget.removeAnswer.calls.count()).toBe(1);
                expect(widget.removeAnswer).toHaveBeenCalledWith(fakeAnswer);
                expect(WidgetBase.prototype.respondHelper.calls.count()).toBe(0);
            });
        });
    });
});
