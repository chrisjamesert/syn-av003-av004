// jscs:disable requireShorthandArrowFunctions
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Widget from 'core/models/Widget';
import DatePicker from 'core/widgets/TimePicker';
import WidgetBaseTests from './WidgetBase.specBase';
import Templates from 'core/collections/Templates';

class TimePickerCoreTests extends WidgetBaseTests {
    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find(`#${myModel.id}_time_input`).length).toEqual(1);
            });
    }
}
describe('TimePicker', () => {

    const template = `<div id="questionnaire-template">
            <div id="questionnaire"></div>
            </div>`;
    let security,
        templates;

    beforeAll(() => {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();

        templates = LF.templates;
        LF.templates = new Templates([{
            name: 'TimePicker',
            namespace: 'DEFAULT',
            template: '<input id="{{ id }}_time_input" class="form-control time-input"  data-role="timebox" />'
        }, {
                name: 'TimePickerLabel',
                namespace: 'DEFAULT',
                template: '<label for="{{ id }}_time_input">{{ timeLabel }}</label>'
            }
        ]);

        $('body').append(template);
    });

    afterAll(() => {
        $('body').remove(template);
        LF.security = security;
        LF.templates = templates;
    });

    const model = new Widget({
        type: 'TimePicker',
        id: 'testWidgetId'
    });
    const baseTests = new TimePickerCoreTests(model);

    baseTests.execTests();

    describe('Class Specific', () => {

        it('localizedTime is undefined', () => {
            const model = new Widget({
                    type: 'TimePicker'
                }),
                timepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            expect(timepickerWidget.localizedTime).toBeUndefined();

        });

        Async.it('should set the correct value to datebox', () => {
            const model = new Widget({
                    type: 'TimePicker'
                }),
                timepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            timepickerWidget.localizedTime = '10:10';

            let request = timepickerWidget.render();
            expect(request).toBePromise();

            return request.then(() => {
                expect(timepickerWidget.$('input[data-role="timebox"]').val()).toEqual('10:10');
            });
        });

        Async.it('should return the correct value for time of a day', () => {
            const model = new Widget({
                    type: 'TimePicker'
                }),
                timepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            let date = new Date();
            date.setHours(10);
            date.setMinutes(45);

            let request = timepickerWidget.render();
            expect(request).toBePromise();

            return request.then(() => {
                timepickerWidget.inputTime.datebox('setTheDate', date);
                expect(timepickerWidget.buildStudyWorksString()).toEqual('10:45:00');
            });
        });

        Async.it('should set the correct value', () => {
            const model = new Widget({
                    type: 'TimePicker'
                }),
                timepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            let date = new Date();
            date.setHours(14);
            date.setMinutes(25);

            return timepickerWidget.render()
            .then(() => {
                timepickerWidget.inputTime.datebox('setTheDate', date);
                timepickerWidget.respond({}, {});
            })
            .tap(() => {
                expect(timepickerWidget.answer.get('response')).toEqual('14:25:00');
            });
        });

        Async.it('should render labels if showLabels true', () => {
            const model = new Widget({
                    type: 'TimePicker',
                    showLabels: true
                }),
                timepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            return timepickerWidget.render()
            .tap(() => {
                expect(timepickerWidget.$('label').length).toEqual(1);
            });
        });

        Async.it('shouldn\'t render labels if showLabels omitted', () => {
            const model = new Widget({
                    type: 'TimePicker'
                }),
                timepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            let showLabels = timepickerWidget.model.get('showLabels');

            showLabels = undefined;

            return timepickerWidget.render()
            .tap(() => {
                expect(timepickerWidget.$('label').length).toEqual(0);
            });
        });
    });
});
