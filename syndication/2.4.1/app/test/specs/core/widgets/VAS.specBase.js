import Widget from 'core/models/Widget';
import VerticalVas from '../../../../core/widgets/VAS/VerticalVAS';
import StudyDesign from 'core/classes/StudyDesign';
import WidgetBaseTests from './WidgetBase.specBase';
import testData from './VVAS.testData';

import {resetStudyDesign} from 'test/helpers/StudyDesign';
import HorizontalVAS from '../../../../core/widgets/VAS/HorizontalVAS';

export default class VASTests extends WidgetBaseTests {

    htmlRenderedCheck () {
        let myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${widget.question.id}`).find('canvas').length > 2)
                    .toEqual(true, 'Must have more than 2 canvases');
            });
    }

    constructor (model) {

        Object.defineProperty(this, 'simpleModel', {
            get: () => {
                return new Widget({
                    id: 'VVAS_DIARY_W_1',
                    type: 'VAS',
                    anchors: {
                        'min': {
                            'text': 'mintext'
                        },
                        'max': {
                            'text': 'maxtext'
                        }
                    }
                });
            }
        });

        if (model === undefined) {
            model = this.simpleModel;
        }

        super(model);
    }


    /**
     * Any tests you want executed should be added here
     */
    execTests () {
        describe('Base Widget tests', () => {
            super.execTests();
        });

        describe('VAS tests', () => {
            let templates,
                dummyParent,
                security,
                VASWidget = null;

            describe('VAS Tests', () => {
                Async.beforeAll(() => {
                    resetStudyDesign();

                    security = LF.security;
                    LF.security = this.getDummySecurity();
                    templates = LF.templates;
                    return Q();
                });

                Async.afterAll(() => {
                    LF.security = security;
                    LF.templates = templates;
                    return Q();
                });

                Async.beforeEach(() => {
                    dummyParent = this.getMinDummyQuestion();
                    return Q();
                });

                Async.afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                    VASWidget = null;
                    return Q();
                });

                Async.it('creates a Horiztonal VAS when displayAs is empty', () => {
                    let _model = this.simpleModel;

                    VASWidget = new LF.Widget[_model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VASWidget.render()
                        .tap(() => {
                            expect(VASWidget.constructor.name).toEqual('HorizontalVAS');
                        });
                });

                Async.it('creates a Horizontal VAS when displayAs = Horizontal', () => {
                    let _model = this.simpleModel;
                    _model.set('displayAs', 'Horizontal');

                    VASWidget = new LF.Widget[_model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VASWidget.render()
                        .tap(() => {
                            // should have a class called 'vas-label' which is specific to the HVAS
                            expect(VASWidget.constructor.name).toEqual('HorizontalVAS');
                        });
                });

                Async.it('initializes all default values correctly', () => {
                    VASWidget = new LF.Widget[this.simpleModel.get('type')]({
                        model: this.simpleModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VASWidget.render()
                        .tap(() => {
                            expect(VASWidget.anchors.min.value).toEqual(0);
                            expect(VASWidget.anchors.max.value).toEqual(100);

                            expect(VASWidget.pointer.isVisible).toEqual(false);
                            expect(VASWidget.pointer.displayInitially).toEqual(false);

                            expect(VASWidget.selectedValue.isVisible).toEqual(false);
                            expect(VASWidget.selectedValue.location).toEqual('static');

                            expect(VASWidget.selectedValue.selectionBox.isVisible).toEqual(false);
                            expect(VASWidget.selectedValue.selectionBox.borderWidth).toEqual(1);
                            expect(VASWidget.selectedValue.selectionBox.borderColor).toEqual('#000');
                            expect(VASWidget.selectedValue.selectionBox.fillColor).toEqual('#000');
                        });
                });
            });
        });
    }
    
}
