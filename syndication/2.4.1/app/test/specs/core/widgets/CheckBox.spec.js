import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Answers from 'core/collections/Answers';
import User from 'core/models/User';
import Dashboard from 'core/models/Dashboard';
import Subject from 'core/models/Subject';
import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Widget from 'core/models/Widget';
import Templates from 'core/collections/Templates';
import Logger from 'core/Logger';

import QuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionView from 'core/views/QuestionView';

import 'core/resources/Templates';
import WidgetBaseTests from './WidgetBase.specBase';

class CheckBoxCoreTests extends WidgetBaseTests {
    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                const expectedCount = myModel.get('answers').length;
                expect($(`#${widget.question.id}`).find('input').length)
                    .toEqual(expectedCount);
            });
    }
}

// function() is required rather than fat arrow functions for "this" context to be correct in the testing framework.
//jscs:disable requireArrowFunctions
/*global describe, it, beforeEach, assert, afterEach, expect, xdescribe, beforeAll, afterAll */
//jscs:disable requireArrowFunctions
describe('CheckBox Widgets', function () {
    let security,
        templates;

    beforeAll(() => {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();

        templates = LF.templates;
        LF.templates = new Templates([{
            name: 'VerticalButtonGroup',
            namespace: 'DEFAULT',
            template: '<div class="vertical-buttons" data-toggle="buttons" data-container></div>'
        },
            // Label wrapper for radio buttons.
            {
                name: 'CheckBoxWrapper',
                namespace: 'DEFAULT',
                template: '<label class="btn btn-default btn-block btn-{{ id }}"><span data-container></span></label>'
            },

            // Default template for Checkbox input that allows jquery mobile css style
            {
                name: 'CheckBox',
                namespace: 'DEFAULT',
                template: '<input type="checkbox" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
            },

            // Default template for Checkbox input that does not include jquery mobile css style
            {
                name: 'CustomCheckBox',
                namespace: 'DEFAULT',
                template:
                    '<input type="checkbox" id="{{ id }}" name="{{ name }}" value="{{ value }}" data-role="none"/>'
            },

            // Default template for Checkbox label.
            {
                name: 'CheckboxLabel',
                namespace: 'DEFAULT',
                template: '<label for="{{ link }}" class="checkbox-text {{ className }}"> {{ text }} </label>'
            },
            // Default Vertical Fieldset template that does not include jquery css mobile style.
            // Used as a container for CustomRadioButton and CustomCheckBox widget.
            {
                name: 'CustomVerticalFieldset',
                namespace: 'DEFAULT',
                template: '<fieldset data-container data-role="none" ></fieldset>'
            }
        ]);

    });

    afterAll(() => {
        LF.security = security;
        LF.templates = templates;
    });

    let dummyParent;

    describe('CheckBox', function () {
        const buildModel =
                () => {
                    return new Widget({
                        id: 'DAILY_DIARY_W_2',
                        type: 'CheckBox',
                        className: 'DAILY_DIARY_W_2',
                        templates: {},
                        answers: [
                            {
                                text: 'NO_PROBLEMS_SELF_CARE',
                                value: '0',
                                IT: 'W_2_checkbox1',
                                exclude: ['2', '0', '']
                            }, {
                                text: 'SOME_PROBLEMS_SELF_CARE',
                                value: '1',
                                IT: 'W_2_checkbox2',
                                exclude: ['1', '10', 'null', '16']
                            }, {
                                text: 'UNABLE_SELF_CARE',
                                value: '2',
                                IT: 'W_2_checkbox3',
                                exclude: ['1']
                            }, {
                                text: 'NONE',
                                value: '3',
                                IT: 'W_2_checkbox4',
                                exclude: ['All']
                            }, {
                                text: 'NONE',
                                value: '4',
                                IT: 'W_2_checkbox5'
                            }, {
                                text: 'NONE',
                                value: '5',
                                IT: 'W_2_checkbox6'
                            }
                        ]
                    });
                },
            baseTests = new CheckBoxCoreTests(buildModel());

        baseTests.execTests();
        describe('control specific tests', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            it('The matrix should have the length of 6', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix.length).toEqual(6);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('matrix[0] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(helpers.fillEmptyArrayValues(widget.matrix[0])).toEqual([undefined, undefined, true, undefined, undefined, undefined]);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('matrix[1] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[1]).toEqual([undefined, undefined, undefined, undefined, undefined, undefined]);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('matrix[2] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(helpers.fillEmptyArrayValues(widget.matrix[2])).toEqual([undefined, true, undefined, undefined, undefined, undefined]);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('matrix[3] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(
                            helpers.fillEmptyArrayValues(widget.matrix[3]))
                            .toEqual([true, true, true, undefined, true, true]);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('matrix[4] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[4])
                            .toEqual([undefined, undefined, undefined, undefined, undefined, undefined]);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('matrix[5] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[5])
                            .toEqual([undefined, undefined, undefined, undefined, undefined, undefined]);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'0\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('0')).toEqual(0);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'1\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('1')).toEqual(1);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'2\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('2')).toEqual(2);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'3\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('3')).toEqual(3);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'4\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('4')).toEqual(4);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'5\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('5')).toEqual(5);
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'6\') should not to be defined', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('6')).not.toBeDefined();
                    })
                    .catch((e) => fail(e))
                    .done(done);
            });

            it('should have the correct answers collection for checkbox.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        let answers = widget.answers;

                        widget.skipped = true;
                        widget.$('#DAILY_DIARY_W_2_checkbox_0').attr('checked', 'checked');
                        widget.respond({target: '#DAILY_DIARY_W_2_checkbox_0'});

                        expect(answers.at(0).get('response')).toEqual('1');
                        expect(answers.at(1).get('response')).toEqual('0');
                        expect(answers.at(2).get('response')).toEqual('0');
                        expect(answers.at(3).get('response')).toEqual('0');
                        expect(answers.at(4).get('response')).toEqual('0');
                        expect(answers.at(5).get('response')).toEqual('0');
                    })
                    .catch((e = '') => {
                        fail(`Unexpected .catch() while setting values: ${e}`);
                    })
                    .done(done);
            });

            it('should have the correct answers collection for checkbox.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        let answers = widget.answers;

                        widget.$('#DAILY_DIARY_W_2_checkbox_1').attr('checked', 'checked');
                        widget.respond({target: '#DAILY_DIARY_W_2_checkbox_1'});

                        expect(answers.at(0).get('response')).toEqual('0');
                        expect(answers.at(1).get('response')).toEqual('1');
                        expect(answers.at(2).get('response')).toEqual('0');
                        expect(answers.at(3).get('response')).toEqual('0');
                        expect(answers.at(4).get('response')).toEqual('0');
                        expect(answers.at(5).get('response')).toEqual('0');
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should have the correct answers collection for checkbox.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        let answers = widget.answers;

                        widget.$('#DAILY_DIARY_W_2_checkbox_3').attr('checked', 'checked');
                        widget.respond({target: '#DAILY_DIARY_W_2_checkbox_3'});

                        expect(answers.at(0).get('response')).toEqual('0');
                        expect(answers.at(1).get('response')).toEqual('0');
                        expect(answers.at(2).get('response')).toEqual('0');
                        expect(answers.at(3).get('response')).toEqual('1');
                        expect(answers.at(4).get('response')).toEqual('0');
                        expect(answers.at(5).get('response')).toEqual('0');
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should have the correct answers collection for checkbox.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        widget.skipped = true;
                        widget.$('#DAILY_DIARY_W_2_checkbox_0').attr('checked', 'checked');
                        widget.respond({target: '#DAILY_DIARY_W_2_checkbox_0'});

                        expect(widget.isAffidavitAnswered()).toBe(true);
                    })
                    .catch(e => fail(e))
                    .done(done);

            });

            it('should have the correct answers collection for checkbox.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        widget.skipped = true;
                        expect(widget.isAffidavitAnswered()).toBe(false);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });
        });
    });

    describe('CustomCheckBox', () => {
        const buildModel =
            () => {
                return new Widget({
                    id: 'DAILY_DIARY_W_2',
                    type: 'CustomCheckBox',
                    className: 'DAILY_DIARY_W_2',
                    templates: {},
                    answers: [
                        {
                            text: 'NO_PROBLEMS_SELF_CARE',
                            value: '30',
                            IT: 'W_2_checkbox1',
                            exclude: ['0', '18', '30', 'All']
                        }, {
                            text: 'SOME_PROBLEMS_SELF_CARE',
                            value: '18',
                            IT: 'W_2_checkbox2',
                            exclude: ['All', '43']
                        }, {
                            text: 'UNABLE_SELF_CARE',
                            value: '43',
                            IT: 'W_2_checkbox3',
                            exclude: ['3', 'All', '43']
                        }, {
                            text: 'NONE',
                            value: '3',
                            IT: 'W_2_checkbox4',
                            exclude: ['All']
                        }, {
                            text: 'NONE',
                            value: '25',
                            IT: 'W_2_checkbox5',
                            exclude: ['All']
                        }
                    ]
                });
            };

        const baseTests = new CheckBoxCoreTests(buildModel());

        baseTests.execTests();
        describe('control specific tests', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            it('The matrix should have the length of 5', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix.length).toEqual(5);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('matrix[0] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[0]).toEqual([undefined, true, true, true, true]);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('matrix[1] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[1]).toEqual([true,undefined, true, true, true]);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('matrix[2] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[2]).toEqual([true, true, undefined, true, true]);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('matrix[3] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[3]).toEqual([true, true, true, undefined, true]);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('matrix[4] has the correct values', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.matrix[4]).toEqual([true, true, true, true,undefined]);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'30\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('30')).toEqual(0);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'18\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('18')).toEqual(1);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'43\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('43')).toEqual(2);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'3\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('3')).toEqual(3);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'25\') should return the correct value', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('25')).toEqual(4);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('getAnswerOptionIndex(\'55\') should not to be defined', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        expect(widget.getAnswerOptionIndex('55')).not.toBeDefined();
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should have the correct answers collection for checkbox.', (done) => {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        let answers = widget.answers;

                        widget.$('#DAILY_DIARY_W_2_checkbox_0').attr('checked', 'checked');

                        widget.respond({target: '#DAILY_DIARY_W_2_checkbox_0'});

                        expect(answers.at(0).get('response')).toEqual('1');
                        expect(answers.at(1).get('response')).toEqual('0');
                        expect(answers.at(2).get('response')).toEqual('0');
                        expect(answers.at(3).get('response')).toEqual('0');
                        expect(answers.at(4).get('response')).toEqual('0');
                    })
                    .catch(e => fail(e))
                    .done(done);

            });

            it('should have the correct answers collection for checkbox.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        let answers = widget.answers;
                        widget.$('#DAILY_DIARY_W_2_checkbox_3').attr('checked', 'checked');

                        widget.respond({target: '#DAILY_DIARY_W_2_checkbox_3'});

                        expect(answers.at(0).get('response')).toEqual('0');
                        expect(answers.at(1).get('response')).toEqual('0');
                        expect(answers.at(2).get('response')).toEqual('0');
                        expect(answers.at(3).get('response')).toEqual('1');
                        expect(answers.at(4).get('response')).toEqual('0');
                    })
                    .catch(e => fail(e))
                    .done(done);

            });

            it('should render the checkboxes correctly for already existing answers.', function (done) {
                const model = buildModel(),
                    widget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    });
                widget.render()
                    .then(() => {
                        widget.$('#DAILY_DIARY_W_2_checkbox_0').attr('checked', 'checked');
                        return widget.respond({target: '#DAILY_DIARY_W_2_checkbox_0'});
                    })
                    .then(() => {
                        return widget.render();
                    }).then(() => {
                        expect(widget.$('#DAILY_DIARY_W_2_checkbox_0').attr('checked'))
                            .toEqual('checked');
                    })
                    .catch(e => fail(e))
                    .done(done);
            });
        });
    });
});
