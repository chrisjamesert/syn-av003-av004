import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import IntervalSpinnerBase from 'core/widgets/IntervalSpinnerBase';
import ValueSpinnerInput from 'core/widgets/input/ValueSpinnerInput';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';
import BaseSpinnerTests from './BaseSpinner.spec.js';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';

import { FlowOptions } from 'core/widgets/IntervalSpinnerBase';

const DEFAULT_SPINNER_TEMPLATE = 'DEFAULT:NumberSpinnerControl',
    DEFAULT_SPINNER_ITEM_TEMPLATE = 'DEFAULT:NumberItemTemplate',
    DEFAULT_CLASS_NAME = 'DateSpinner',
    DEFAULT_STORAGE_LOCALE = 'en-US',

    DEFAULT_NUMBER_OF_SPINNER_ITEMS = 2.5,

    DEFAULT_DATE_FORMAT = 'LL',
    DEFAULT_TIME_FORMAT = 'LT',
    DEFAULT_DATE_TIME_FORMAT = 'LLL',

    DEFAULT_DATE_YEAR_RANGE = 30;

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class IntervalSpinnerBaseTests extends BaseSpinnerTests {
    constructor (model) {
        if (!(model instanceof Widget)) {
            throw new Error('Invalid use of IntervalSpinnerBase. Must be tested with an implementation of the abstract class.');
        }
        super(model);
    }

    execTests () {
        super.execTests();

        describe('IntervalSpinnerBase', () => {
            let templates,
                dummyParent,
                security,
                spinnerWidget = null,
                preferred;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                preferred = LF.Preferred;
                LF.Preferred = { language: 'en', locale: 'US' };
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
                LF.Preferred = preferred;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();

                // Remove the backdrop in case it was open while we exited.
                $('.modal-backdrop').remove();
                spinnerWidget = null;
            });

            describe('#getters', () => {
                let testValue,
                    baseSpinner;

                describe('#value', () => {
                    Async.it('can be set by the storage format and returns in the storage format, but writes displayFormat into text box', () => {
                        let customModel = _.extend({}, this.model),
                            testValue = new Date('12/19/2016'),
                            displayFormat = 'LL',
                            displayLocale = 'hi-IN',
                            storageFormat = 'YYYY-MM-DD',
                            storageLocale = 'en-US';

                        customModel.attributes = _.extend({}, this.model.attributes, {
                            displayFormat,
                            storageFormat,
                            storageLocale
                        });
                        spinnerWidget = new LF.Widget[customModel.get('type')]({
                            model: customModel,
                            mandatory: false,
                            parent: dummyParent
                        });

                        spinnerWidget.moment.locale(displayLocale);

                        return spinnerWidget.render().then(() => {
                            spinnerWidget.value = moment(testValue).locale(storageLocale).format(storageFormat);
                            expect(spinnerWidget.value).toBe(moment(testValue).locale(storageLocale).format(storageFormat));
                            expect(spinnerWidget.$('input').val()).toBe(moment(testValue).locale(displayLocale).format(displayFormat));
                        });
                    });
                });

                describe('IntervalSpinnerBase (base class) getters and abstract functions', () => {
                    let temp = null;
                    beforeEach(() => {
                        let customModel = _.extend({}, this.model),
                            testValue = new Date('12/19/2016'),
                            displayFormat = 'LL',
                            displayLocale = 'hi-IN',
                            storageFormat = 'DD MMM YYYY',
                            storageLocale = 'en-US';

                        customModel.attributes = _.extend({}, this.model.attributes, {
                            displayFormat,
                            storageFormat,
                            storageLocale
                        });

                        spinnerWidget = new IntervalSpinnerBase({
                            model: customModel,
                            mandatory: false,
                            parent: dummyParent
                        });
                        temp = null;
                    });

                    describe('#defaultModalTemplate', () => {
                        it('throws an error when called from the base class', () => {
                            expect(() => {
                                temp = spinnerWidget.defaultModalTemplate;
                            }).toThrow(new Error('Invalid Interval Spinner Implementation.  A Modal Template is required'));
                        });
                    });
                    describe('#defaultDisplayFormat', () => {
                        it('throws an error when called from the base class', () => {
                            expect(() => {
                                temp = spinnerWidget.defaultDisplayFormat;
                            }).toThrow(new Error('Invalid Interval Spinner Implementation.  A Display Format is required'));
                        });
                    });
                    describe('#defaultStorageFormat', () => {
                        it('throws an error when called from the base class', () => {
                            expect(() => {
                                temp = spinnerWidget.defaultStorageFormat;
                            }).toThrow(new Error('Invalid Interval Spinner Implementation.  A Storage Format is required'));
                        });
                    });
                    describe('#defaultDateformat', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultDateFormat).toBe(DEFAULT_DATE_FORMAT);
                        });
                    });
                    describe('#defaultDateTimeFormat', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultDateTimeFormat).toBe(DEFAULT_DATE_TIME_FORMAT);
                        });
                    });
                    describe('#defaultStorageLocale', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultStorageLocale).toBe(DEFAULT_STORAGE_LOCALE);
                        });
                    });
                    describe('#defaultSpinnerTemplate', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultSpinnerTemplate).toBe(DEFAULT_SPINNER_TEMPLATE);
                        });
                    });
                    describe('#defaultClassName', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultClassName).toBe(DEFAULT_CLASS_NAME);
                        });
                    });
                    describe('#defaultSpinnerItemTemplate', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultSpinnerItemTemplate).toBe(DEFAULT_SPINNER_ITEM_TEMPLATE);
                        });
                    });
                    describe('#defaultYear', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultYear)
                                .toBe(spinnerWidget.model.get('initialDate').getFullYear());
                        });
                    });
                    describe('#defaultMonth', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultMonth)
                                .toBe(spinnerWidget.model.get('initialDate').getMonth());
                        });
                    });
                    describe('#defaultDay', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultDay)
                                .toBe(spinnerWidget.model.get('initialDate').getDate());
                        });
                    });
                    describe('#defaultHour', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultHour)
                                .toBe(spinnerWidget.model.get('initialDate').getHours());
                        });
                    });
                    describe('#defaultMinute', () => {
                        it('defaults to correct value', () => {
                            expect(spinnerWidget.defaultMinute)
                                .toBe(spinnerWidget.model.get('initialDate').getMinutes());
                        });
                    });
                    describe('#defaultInitialDate', () => {
                        it('defaults to correct value', () => {
                            let compTime = (new Date()).getTime(),
                                widgetTime = spinnerWidget.defaultInitialDate.getTime();

                            // should never be a whole second off from current date
                            expect(widgetTime).toBeGreaterThan(compTime - 1000);
                            expect(widgetTime).toBeLessThan(compTime + 1000);
                        });
                    });
                    describe('#defaultMinDate', () => {
                        it('defaults to correct value', () => {
                            let compTime = (new Date()).setFullYear((new Date()).getFullYear() - DEFAULT_DATE_YEAR_RANGE),
                                widgetTime = spinnerWidget.defaultMinDate.getTime();

                            // should never be a whole second off from current date
                            expect(widgetTime).toBeGreaterThan(compTime - 1000);
                            expect(widgetTime).toBeLessThan(compTime + 1000);
                        });
                    });
                    describe('#defaultMaxDate', () => {
                        it('defaults to correct value', () => {
                            let compTime = (new Date()).setFullYear((new Date()).getFullYear() + DEFAULT_DATE_YEAR_RANGE),
                                widgetTime = spinnerWidget.defaultMaxDate.getTime();

                            // should never be a whole second off from current date
                            expect(widgetTime).toBeGreaterThan(compTime - 1000);
                            expect(widgetTime).toBeLessThan(compTime + 1000);
                        });
                    });
                    describe('#year', () => {
                        it('defaults to defaultYear', () => {
                            expect(spinnerWidget.year).toBe(spinnerWidget.defaultYear);
                        });
                        it('overrides with the value of the spinner at the correct index', () => {
                            // mock up the spinners array and yearIndex index.
                            spinnerWidget.spinners = [{ value: 1950 }];
                            spinnerWidget.yearIndex = 0;

                            expect(spinnerWidget.year).toBe(1950);
                        });
                    });
                    describe('#month', () => {
                        it('defaults to defaultMonth', () => {
                            expect(spinnerWidget.month).toBe(spinnerWidget.defaultMonth);
                        });
                        it('overrides with the value of the spinner at the correct index', () => {
                            // mock up the spinners array and yearIndex index.
                            spinnerWidget.spinners = [{ value: 10 }];
                            spinnerWidget.monthIndex = 0;

                            // 9, because months are 0-indexed in the actual date object.
                            expect(spinnerWidget.month).toBe(9);
                        });
                    });
                    describe('#day', () => {
                        it('defaults to defaultDay', () => {
                            expect(spinnerWidget.day).toBe(spinnerWidget.defaultDay);
                        });
                        it('overrides with the value of the spinner at the correct index', () => {
                            // mock up the spinners array and yearIndex index.
                            spinnerWidget.spinners = [{ value: 4 }];
                            spinnerWidget.dayIndex = 0;

                            expect(spinnerWidget.day).toBe(4);
                        });
                    });
                    describe('#hour', () => {
                        it('defaults to defaultHour', () => {
                            expect(spinnerWidget.hour).toBe(spinnerWidget.defaultHour);
                        });
                        it('overrides with the value of the spinner at the correct index', () => {
                            // mock up the spinners array and yearIndex index.
                            spinnerWidget.spinners = [{ value: 8 }];
                            spinnerWidget.hourIndex = 0;

                            expect(spinnerWidget.hour).toBe(8);
                        });
                    });
                    describe('#minute', () => {
                        it('defaults to defaultMinute', () => {
                            expect(spinnerWidget.minute).toBe(spinnerWidget.defaultMinute);
                        });
                        it('overrides with the value of the spinner at the correct index', () => {
                            // mock up the spinners array and yearIndex index.
                            spinnerWidget.spinners = [{ value: 30 }];
                            spinnerWidget.minuteIndex = 0;

                            expect(spinnerWidget.minute).toBe(30);
                        });
                    });

                    describe('#validateUI', () => {
                        it('throws an error when called from the base class', () => {
                            expect(() => {
                                temp = spinnerWidget.validateUI();
                            }).toThrow(new Error('IntervalSpinnerBase.validateUI() must be overridden by subclass'));
                        });
                    });
                });
            });

            describe('#constructor', () => {
                it('creates object of type IntervalSpinnerBase, with everything initialized', () => {
                    spinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(spinnerWidget.spinners).toEqual([]);
                    expect(spinnerWidget.yearIndex).toBe(null);
                    expect(spinnerWidget.monthIndex).toBe(null);
                    expect(spinnerWidget.dayIndex).toBe(null);
                    expect(spinnerWidget.hourIndex).toBe(null);
                    expect(spinnerWidget.minuteIndex).toBe(null);
                    expect(spinnerWidget.meridianIndicatorIndex).toBe(null);

                    expect(spinnerWidget.suppressUpdateEvents).toBe(false);

                    expect(spinnerWidget.model.get('initialDate')).toEqual(spinnerWidget.defaultInitialDate);
                    expect(spinnerWidget.model.get('minDate')).toEqual(spinnerWidget.defaultMinDate);
                    expect(spinnerWidget.model.get('maxDate')).toEqual(spinnerWidget.defaultMaxDate);

                    expect(spinnerWidget.model.get('displayFormat')).toBe(spinnerWidget.defaultDisplayFormat);
                    expect(spinnerWidget.model.get('storageFormat')).toBe(spinnerWidget.defaultStorageFormat);
                    expect(spinnerWidget.model.get('storageLocale')).toBe(spinnerWidget.defaultStorageLocale);

                    expect(spinnerWidget.model.get('dateFormat')).toBe(spinnerWidget.defaultDateFormat);
                    expect(spinnerWidget.model.get('timeFormat')).toBe(spinnerWidget.defaultTimeFormat);
                    expect(spinnerWidget.model.get('dateTimeFormat')).toBe(spinnerWidget.defaultDateTimeFormat);

                    expect(spinnerWidget.model.get('flow')).toBe(FlowOptions.CALCULATE);

                    expect(spinnerWidget.moment instanceof moment).toBe(true);
                    expect(spinnerWidget.moment.locale()).toBe(moment().locale('en-US').locale());
                });

                it('accepts model parameters.', () => {
                    let customModel = _.extend({}, this.model),
                        options = {
                            initialDate: new Date('10/04/16'),
                            minDate: new Date('10/04/06'),
                            maxDate: new Date('10/04/25'),
                            displayFormat: 'DD/MMM/YYYY',
                            storageFormat: 'YYYY~DD~MM',
                            storageLocale: 'fr-CA',
                            dateFormat: 'MMMM DD YYYY',
                            timeFormat: 'HH:mm a',
                            dateTimeFormat: 'MMMM DD YYYY HH:MM a',
                            flow: 'ltr'
                        };
                    customModel.attributes = _.extend({}, this.model.attributes, options);
                    spinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(spinnerWidget.model.get('initialDate')).toEqual(options.initialDate);
                    expect(spinnerWidget.model.get('minDate')).toEqual(options.minDate);
                    expect(spinnerWidget.model.get('maxDate')).toEqual(options.maxDate);

                    expect(spinnerWidget.model.get('displayFormat')).toBe(options.displayFormat);
                    expect(spinnerWidget.model.get('storageFormat')).toBe(options.storageFormat);
                    expect(spinnerWidget.model.get('storageLocale')).toBe(options.storageLocale);

                    expect(spinnerWidget.model.get('dateFormat')).toBe(options.dateFormat);
                    expect(spinnerWidget.model.get('timeFormat')).toBe(options.timeFormat);
                    expect(spinnerWidget.model.get('dateTimeFormat')).toBe(options.dateTimeFormat);

                    expect(spinnerWidget.model.get('flow')).toBe(FlowOptions[options.flow.toUpperCase()]);
                });
            });

            // eslint-disable-next-line new-cap
            TRACE_MATRIX('DE21576')
            .describe('dateOrNull', () => {
                it('can be used with initialDate, minDate and maxDate as string parameters', () => {
                    let customModel = _.extend({}, this.model),
                        options = {
                            initialDate: '10/04/16',
                            minDate: '10/04/06',
                            maxDate: '10/04/25',
                            displayFormat: 'DD/MMM/YYYY',
                            storageFormat: 'YYYY~DD~MM',
                            storageLocale: 'en-US',
                            dateFormat: 'MMMM DD YYYY',
                            timeFormat: 'HH:mm a',
                            dateTimeFormat: 'MMMM DD YYYY HH:MM a'
                        };
                    customModel.attributes = _.extend({}, this.model.attributes, options);
                    spinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(spinnerWidget.model.get('initialDate')).toEqual(new Date(options.initialDate));
                    expect(spinnerWidget.model.get('minDate')).toEqual(new Date(options.minDate));
                    expect(spinnerWidget.model.get('maxDate')).toEqual(new Date(options.maxDate));
                });

                it('can be used with initialDate, minDate and maxDate as date parameters', () => {
                    let customModel = _.extend({}, this.model),
                        options = {
                            initialDate: new Date('10/04/16'),
                            minDate: new Date('10/04/06'),
                            maxDate: new Date('10/04/25'),
                            displayFormat: 'DD/MMM/YYYY',
                            storageFormat: 'YYYY~DD~MM',
                            storageLocale: 'en-US',
                            dateFormat: 'MMMM DD YYYY',
                            timeFormat: 'HH:mm a',
                            dateTimeFormat: 'MMMM DD YYYY HH:MM a'
                        };
                    customModel.attributes = _.extend({}, this.model.attributes, options);
                    spinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(spinnerWidget.model.get('initialDate')).toEqual(new Date('10/04/16'));
                    expect(spinnerWidget.model.get('minDate')).toEqual(new Date('10/04/06'));
                    expect(spinnerWidget.model.get('maxDate')).toEqual(new Date('10/04/25'));
                });

                it('can be used with initialDate, minDate and maxDate as function parameters', () => {
                    let customModel = _.extend({}, this.model),
                        options = {
                            initialDate: () => {
                                return new Date(new Date().setFullYear(new Date().getFullYear() + 10));
                            },
                            minDate: () => {
                                return new Date(new Date().setFullYear(new Date().getFullYear() - 35));
                            },
                            maxDate: () => {
                                return new Date(new Date().setFullYear(new Date().getFullYear() + 35));
                            },
                            displayFormat: 'DD/MMM/YYYY',
                            storageFormat: 'YYYY~DD~MM',
                            storageLocale: 'en-US',
                            dateFormat: 'MMMM DD YYYY',
                            timeFormat: 'HH:mm a',
                            dateTimeFormat: 'MMMM DD YYYY HH:MM a'
                        };
                    customModel.attributes = _.extend({}, this.model.attributes, options);
                    spinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(spinnerWidget.model.get('initialDate').getFullYear()).toEqual(new Date().getFullYear() + 10);
                    expect(spinnerWidget.model.get('minDate').getFullYear()).toEqual(new Date().getFullYear() - 35);
                    expect(spinnerWidget.model.get('maxDate').getFullYear()).toEqual(new Date().getFullYear() + 35);
                });
            });

            describe('post-rendered tests', () => {
                Async.beforeEach(() => {
                    spinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return spinnerWidget.render();
                });

                describe('#getModalValuesString', () => {
                    Async.it('calls pushValue() on each spinner, sets the limits, and returns the date', () => {
                        for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                            spyOn(spinnerWidget.spinners[i], 'stop').and.callThrough();
                            spyOn(spinnerWidget.spinners[i], 'pushValue').and.callThrough();
                        }

                        spyOn(spinnerWidget, 'setSpinnerLimits').and.callThrough();


                        return spinnerWidget.openDialog()
                            .then(() => {
                                return spinnerWidget.getModalValuesString();
                            })
                            .tap((val) => {
                                for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                                    expect(spinnerWidget.spinners[i].stop).toHaveBeenCalled();
                                    expect(spinnerWidget.spinners[i].pushValue).toHaveBeenCalled();
                                }
                                expect(spinnerWidget.setSpinnerLimits).toHaveBeenCalled();
                                let dt = spinnerWidget.getDate(),
                                    storageLocale = spinnerWidget.model.get('storageLocale'),
                                    storageFormat = spinnerWidget.model.get('storageFormat');
                                expect(val).toBe(moment(dt).locale(storageLocale).format(storageFormat));
                            });
                    });
                });

                describe('#getSpinnerValuesArray', () => {
                    it('returns array with indexes populated with correct values', () => {
                        let valArray = spinnerWidget.getSpinnerValuesArray();

                        // verify we actually have values, and the loop will be entered.
                        expect(valArray.length > 0).toBe(true);

                        for (let i = 0; i < valArray.length; ++i) {
                            let compVal,
                                defaultVal = spinnerWidget.model.get('initialDate') || '',
                                val;

                            val = spinnerWidget.$(`#${spinnerWidget.model.get('id')}`).val();
                            if (val === '' || val === undefined) {
                                val = moment(defaultVal).locale(spinnerWidget.moment.locale()).format(spinnerWidget.model.get('displayFormat'));
                            }
                            val = moment(val, spinnerWidget.model.get('displayFormat'), spinnerWidget.moment.locale()).toDate();
                            switch (i) {
                                case spinnerWidget.yearIndex:
                                    compVal = val.getFullYear();
                                    break;
                                case spinnerWidget.monthIndex:
                                    compVal = val.getMonth() + 1;  // + 1 because months are zero indexed
                                    break;
                                case spinnerWidget.dayIndex:
                                    compVal = val.getDate();
                                    break;
                                case spinnerWidget.hourIndex:
                                    compVal = val.getHours();
                                    if (typeof spinnerWidget.meridianIndicatorIndex === 'number') {
                                        compVal = compVal % 12;
                                    }
                                    break;
                                case spinnerWidget.meridianIndicatorIndex:
                                    if (val.getHours() >= 12) {
                                        compVal = 1;
                                    } else {
                                        valArray[this.meridianIndicatorIndex] = 0;
                                    }
                                    break;
                                case spinnerWidget.minuteIndex:
                                    compVal = val.getMinutes();
                                    break;
                                default:
                                    compVal = null;
                            }

                            if (compVal !== null) {
                                expect(compVal).toBe(valArray[i]);
                            }
                        }
                    });
                });

                // eslint-disable-next-line new-cap
                TRACE_MATRIX('DE19538')
                .describe('#adjustDatePart', () => {
                    Async.it('hides dates and sets the scroll value accordingly', () => {
                        let dateParts = [
                            {
                                name: 'month',
                                startRange: 1,
                                endRange: 12,
                                validStart: 3,
                                validEnd: 10,
                                scrollAdjustment: 2
                            },
                            {
                                name: 'day',
                                startRange: 1,
                                endRange: 31,
                                validStart: 3,
                                validEnd: 20,
                                scrollAdjustment: 2
                            },
                            {
                                name: 'meridianIndicator',
                                startRange: 0,
                                endRange: 1,
                                validStart: 1,
                                validEnd: 1,
                                scrollAdjustment: 1
                            },
                            {
                                name: 'hour',
                                startRange: 0,
                                endRange: 23,
                                validStart: 3,
                                validEnd: 20,
                                scrollAdjustment: 3
                            },
                            {
                                name: 'minute',
                                startRange: 0,
                                endRange: 59,
                                validStart: 3,
                                validEnd: 45,
                                scrollAdjustment: 3
                            }
                        ];

                        // take the guesswork out of testing.
                        spyOn(spinnerWidget, 'testDate').and.callFake((datePart, comp) => {
                            for (let i = 0; i < dateParts.length; ++i) {
                                if (dateParts[i].name === datePart) {
                                    return dateParts[i].validStart <= comp &&
                                        dateParts[i].validEnd >= comp;
                                }
                            }
                            return false;
                        });

                        spinnerWidget.value = '04 Oct 2010 13:00';
                        let ret = spinnerWidget.openDialog();
                        for (let spinnerNdx = 0; spinnerNdx < spinnerWidget.spinners.length; ++spinnerNdx) {
                            spyOn(spinnerWidget.spinners[spinnerNdx], 'hideItem').and.callThrough();
                            spyOn(spinnerWidget.spinners[spinnerNdx], 'unHideItem').and.callThrough();
                            spyOn(spinnerWidget.spinners[spinnerNdx], 'adjustScroll').and.callThrough();
                            spyOn(spinnerWidget.spinners[spinnerNdx], 'pushValue').and.callThrough();
                        }

                        for (let i = 0; i < dateParts.length; ++i) {
                            ret = ret.then(() => {
                                let spinnerNdx = spinnerWidget[`${dateParts[i].name}Index`];

                                if (spinnerNdx === null) {
                                    return Q.resolve();
                                }

                                spyOn(spinnerWidget.spinners[spinnerNdx].scroller, 'scrollTo').and.callThrough();

                                return spinnerWidget.adjustDatePart(dateParts[i].name).tap(() => {
                                    let hiddenCallCount = dateParts[i].validStart - dateParts[i].startRange +
                                                            dateParts[i].endRange - dateParts[i].validEnd,
                                        unHiddenCallCount = dateParts[i].validEnd - dateParts[i].validStart + 1;
                                    for (let j = dateParts[i].startRange; j < dateParts[i].validStart; ++j) {
                                        expect(spinnerWidget.spinners[spinnerNdx].hideItem).toHaveBeenCalledWith(j, false);
                                    }
                                    for (let j = dateParts[i].validEnd + 1; j <= dateParts[i].endRange; ++j) {
                                        expect(spinnerWidget.spinners[spinnerNdx].hideItem).toHaveBeenCalledWith(j, false);
                                    }
                                    for (let j = dateParts[i].validStart; j <= dateParts[i].validEnd; ++j) {
                                        expect(spinnerWidget.spinners[spinnerNdx].unHideItem).toHaveBeenCalledWith(j, false);
                                    }
                                    let actualScrollAdjustment = 0;
                                    let scrollAdjustments = spinnerWidget.spinners[spinnerNdx].adjustScroll.calls.all();
                                    for (let j = 0; j < scrollAdjustments.length; ++j) {
                                        actualScrollAdjustment += scrollAdjustments[j].args[0];
                                    }

                                    // Add 60 back in to compensate for blank item (removed by pushValue() spy no longer being inhibited)
                                    expect(actualScrollAdjustment).toBe(dateParts[i].scrollAdjustment * 60 + 60);
                                    expect(spinnerWidget.spinners[spinnerNdx].pushValue).toHaveBeenCalled();

                                    // DE19538: Verify scrollTo is called with 0 as the third param for the scroll delay
                                    expect(spinnerWidget.spinners[spinnerNdx].scroller.scrollTo.calls.count()).toBeGreaterThan(0);

                                    _.each(spinnerWidget.spinners[spinnerNdx].scroller.scrollTo.calls.all(), (call) => {
                                        expect(call.args[2]).toBe(0);
                                    });
                                });
                            });
                        }

                        return ret;
                    });
                });

                describe('#setMeridianDisplayValues()', () => {
                    it('sets meridian indicators to correspond with the current time.  Returns false if no meridianIndicatorIndex',
                        () => {
                            if (spinnerWidget.meridianIndicatorIndex !== 'number') {
                                expect(spinnerWidget.setMeridianDisplayValues()).toBe(false);
                            } else {
                                fail('Not yet tested case for setMeridianDisplayValues.  meridianIndictorIndex is set.  Implement this test for time and date time spinners.');
                            }
                        });
                });

                describe('setSpinnerLimits()', () => {
                    Async.it('returns promise without running anything if suppressUpdateEvents is true or if spinners are not rendered or are null', () => {
                        return spinnerWidget.openDialog().then(() => {
                            let ret = Q();
                            spyOn(spinnerWidget, 'adjustDatePart');
                            ret = ret.then(() => {
                                spinnerWidget.suppressUpdateEvents = true;
                                return spinnerWidget.setSpinnerLimits();
                            }).then(() => {
                                expect(spinnerWidget.adjustDatePart.calls.count()).toBe(0);
                            });

                            ret = ret.then(() => {
                                spinnerWidget.suppressUpdateEvents = false;
                                spinnerWidget.spinners[0].isRendered = false;
                                return spinnerWidget.setSpinnerLimits();
                            }).then(() => {
                                expect(spinnerWidget.adjustDatePart.calls.count()).toBe(0);
                            });


                            ret = ret.then(() => {
                                spinnerWidget.spinners[0].isRendered = true;
                                spinnerWidget.spinners[0].value = null;
                                return spinnerWidget.setSpinnerLimits();
                            }).then(() => {
                                expect(spinnerWidget.adjustDatePart.calls.count()).toBe(0);
                            });

                            return ret;
                        });
                    });

                    Async.it('calls adjustDatePart on each date part, keeping suppressUpdateEvents as true until finished', () => {
                        spyOn(spinnerWidget, 'adjustDatePart').and.callFake(() => {
                            expect(spinnerWidget.suppressUpdateEvents).toBe(true);
                            return Q.resolve();
                        });
                        return spinnerWidget.openDialog().then(() => {
                            return spinnerWidget.setSpinnerLimits();
                        }).tap(() => {
                            expect(spinnerWidget.adjustDatePart).toHaveBeenCalledWith('month');
                            expect(spinnerWidget.adjustDatePart).toHaveBeenCalledWith('day');
                            expect(spinnerWidget.adjustDatePart).toHaveBeenCalledWith('meridianIndicator');
                            expect(spinnerWidget.adjustDatePart).toHaveBeenCalledWith('hour');
                            expect(spinnerWidget.adjustDatePart).toHaveBeenCalledWith('minute');
                        });
                    });
                });

                describe('#addCustomEvents', () => {
                    it('listens to the value change event for each spinner to set spinner limits', () => {
                        spyOn(spinnerWidget, 'listenTo').and.callThrough();

                        spinnerWidget.addCustomEvents();

                        expect(spinnerWidget.spinners.length > 0).toBe(true);
                        for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                            expect(spinnerWidget.listenTo).toHaveBeenCalledWith(spinnerWidget.spinners[i].model, 'change:value', spinnerWidget.setSpinnerLimits);
                        }
                    });
                });

                describe('#removeCustomEvents', () => {
                    it('removes listeners for the value change event for each spinner', () => {
                        spyOn(spinnerWidget, 'stopListening').and.callThrough();

                        spinnerWidget.removeCustomEvents();

                        expect(spinnerWidget.spinners.length > 0).toBe(true);
                        for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                            expect(spinnerWidget.stopListening).toHaveBeenCalledWith(spinnerWidget.spinners[i].model);
                        }
                    });
                });

                describe('#testDate', () => {
                    Async.it('returns true or false on the proper side of minDate', () => {
                        spinnerWidget.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        spinnerWidget.model.set('maxDate', new Date('10/30/17 8:30 PM'));
                        spinnerWidget.value = '25 Oct 2016 22:00';

                        return spinnerWidget.openDialog().then(() => {
                            if (typeof spinnerWidget.yearIndex === 'number') {
                                expect(spinnerWidget.testDate('year', 2016)).toBe(false, 'year before min');
                                expect(spinnerWidget.testDate('year', 2017)).toBe(true, 'year after min');
                            }
                        }).then(() => {
                            if (typeof spinnerWidget.monthIndex === 'number') {
                                expect(spinnerWidget.testDate('month', 9)).toBe(false, 'month before min');
                                expect(spinnerWidget.testDate('month', 10)).toBe(true, 'month after min');
                            }
                        }).then(() => {
                            if (typeof spinnerWidget.dayIndex === 'number') {
                                expect(spinnerWidget.testDate('day', 24)).toBe(false, 'day before min');
                                expect(spinnerWidget.testDate('day', 25)).toBe(true, 'day after min');
                            }
                        }).then(() => {
                            if (spinnerWidget.hourIndex) {
                                expect(spinnerWidget.testDate('hour', 21)).toBe(false, 'hour before min');
                                expect(spinnerWidget.testDate('hour', 22)).toBe(true, 'hour after min');
                            }
                        }).then(() => {
                            if (spinnerWidget.minuteIndex) {
                                expect(spinnerWidget.testDate('minute', 29)).toBe(false, 'minute before min');
                                expect(spinnerWidget.testDate('minute', 30)).toBe(true, 'minute after min');
                            }
                        });
                    });
                    Async.it('returns true or false on the proper side of maxDate', () => {
                        spinnerWidget.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        spinnerWidget.model.set('maxDate', new Date('10/30/17 8:30 PM'));
                        spinnerWidget.value = '30 Oct 2017 22:00';

                        return spinnerWidget.openDialog().then(() => {
                            if (typeof spinnerWidget.yearIndex === 'number') {
                                expect(spinnerWidget.testDate('year', 2017)).toBe(true, 'year before max');
                                expect(spinnerWidget.testDate('year', 2018)).toBe(false, 'year after max');
                            }
                        }).then(() => {
                            if (typeof spinnerWidget.monthIndex === 'number') {
                                expect(spinnerWidget.testDate('month', 10)).toBe(true, 'month before max');
                                expect(spinnerWidget.testDate('month', 11)).toBe(false, 'month after max');
                            }
                        }).then(() => {
                            if (typeof spinnerWidget.dayIndex === 'number') {
                                expect(spinnerWidget.testDate('day', 30)).toBe(true, 'day before max');
                                expect(spinnerWidget.testDate('day', 31)).toBe(false, 'day after max');
                            }
                        }).then(() => {
                            if (spinnerWidget.hourIndex) {
                                expect(spinnerWidget.testDate('hour', 22)).toBe(true, 'hour before max');
                                expect(spinnerWidget.testDate('hour', 23)).toBe(false, 'hour after max');
                            }
                        }).then(() => {
                            if (spinnerWidget.minuteIndex) {
                                expect(spinnerWidget.testDate('minute', 30)).toBe(true, 'minute before max');
                                expect(spinnerWidget.testDate('minute', 31)).toBe(false, 'minute after max');
                            }
                        });
                    });
                });

                describe('#getDate()', () => {
                    Async.it('returns a date with the custom property altered', () => {
                        spinnerWidget.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        spinnerWidget.model.set('maxDate', new Date('10/30/17 8:30 PM'));
                        spinnerWidget.value = '15 May 2017 22:10';

                        return spinnerWidget.openDialog().then(() => {
                            if (typeof spinnerWidget.yearIndex === 'number') {
                                expect(spinnerWidget.getDate().getFullYear()).toBe(2017, 'year, default');
                                expect(spinnerWidget.getDate('year', 2018).getFullYear()).toBe(2018, 'year, custom');
                            }

                            if (typeof spinnerWidget.monthIndex === 'number') {
                                expect(spinnerWidget.getDate().getMonth() + 1).toBe(5, 'month, default');
                                expect(spinnerWidget.getDate('month', 9).getMonth() + 1).toBe(9, 'month, custom');
                            }

                            if (typeof spinnerWidget.dayIndex === 'number') {
                                expect(spinnerWidget.getDate().getDate()).toBe(15, 'day, default');
                                expect(spinnerWidget.getDate('day', 20).getDate()).toBe(20, 'day, custom');
                            }

                            if (typeof spinnerWidget.hourIndex === 'number') {
                                expect(spinnerWidget.getDate().getHours()).toBe(22, 'hour, default');
                                expect(spinnerWidget.getDate('hour', 18).getHours()).toBe(18, 'hour, custom');
                            }

                            if (typeof spinnerWidget.hourIndex === 'number') {
                                expect(spinnerWidget.getDate().getMinutes()).toBe(10, 'minute, default');
                                expect(spinnerWidget.getDate('minute', 40).getMinutes()).toBe(40, 'minute, custom');
                            }
                        });
                    });
                });

                describe('#getYearItemsArray', () => {
                    it('loops on years in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'YYYY';
                        spinnerWidget.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        spinnerWidget.model.set('maxDate', new Date('10/30/25 8:30 PM'));
                        spinnerWidget.moment.locale('hi-IN');

                        let values = spinnerWidget.getYearItemsArray(format),
                            curVal = new Date(spinnerWidget.model.get('minDate')).getFullYear();
                        expect(values.length).toBe(10);

                        for (let i = 0; i < values.length; ++i) {
                            let expectedVal = moment(`01 Jan ${curVal}`, 'DD MMM YYYY', 'en-US');
                            expectedVal.locale(locale);
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                        }
                    });
                });

                describe('#getMonthItemsArray', () => {
                    it('loops on months in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'MMM';
                        spinnerWidget.moment.locale('hi-IN');

                        let values = spinnerWidget.getMonthItemsArray(format),
                            curVal = 1,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(12);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'M');
                        }
                    });
                });

                describe('#getDayItemsArray', () => {
                    it('loops on days in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'MMM';
                        spinnerWidget.moment.locale('hi-IN');

                        let values = spinnerWidget.getDayItemsArray(format),
                            curVal = 1,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(31);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'd');
                        }
                    });
                });

                describe('#getHourItemsArray', () => {
                    it('loops on days in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'HH';
                        spinnerWidget.moment.locale('hi-IN');

                        let values = spinnerWidget.getHourItemsArray(format),
                            curVal = 0,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(24);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'h');
                        }
                    });
                });

                describe('#getMinuteItemsArray', () => {
                    it('loops on days in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'HH';
                        spinnerWidget.moment.locale('hi-IN');

                        let values = spinnerWidget.getMinuteItemsArray(format),
                            curVal = 0,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(60);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'm');
                        }
                    });
                });

                describe('#getMeridiemItemsArray', () => {
                    it('returns an array of length 2, with blank text values (placeholders)', () => {
                        let values = spinnerWidget.getMeridiemItemsArray();
                        expect(values.length).toBe(2);
                        expect(values[0].value).toBe(0);
                        expect(values[0].text).toBe('');
                        expect(values[1].value).toBe(1);
                        expect(values[1].text).toBe('');
                    });
                });

                describe('#parseDateParts', () => {
                    let parseTestCases = [];
                    parseTestCases.push({
                        locale: 'en-US',
                        format: 'DMY',
                        value: ['D', 'M', 'Y']
                    });
                    parseTestCases.push({
                        locale: 'en-US',
                        format: 'Do MMMM YYYYY',
                        value: ['Do', ' ', 'MMMM', ' ', 'YYYYY']
                    });
                    parseTestCases.push({
                        locale: 'zh-CN',
                        format: 'LT',
                        value: ['A', 'h', '点', 'mm', '分']
                    });
                    parseTestCases.push({
                        locale: 'hi',
                        format: 'LLL',
                        value: [
                            'D', ' ', 'MMMM', ' ', 'YYYY', ', ',
                            'A', ' ', 'h', ':', 'mm', ' बजे']
                    });
                    _.each(parseTestCases, (testCase) => {
                        let locale = testCase.locale || 'en-US';
                        it(`correctly parses "${testCase.format}" value in locale ${locale}`, () => {
                            spinnerWidget.moment.locale(locale);
                            expect(spinnerWidget.parseDateParts(testCase.format)).toEqual(testCase.value);
                        });
                    });
                });

                TRACE_MATRIX('DE22389')
                .describe('#datePartsWithDocumentFlow', () => {
                    it('returns original dateparts if LTR', () => {
                        let testDateParts = spinnerWidget.parseDateParts('LL');
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('ltr');
                        spinnerWidget.model.set('flow', FlowOptions.CALCULATE);

                        let dateParts = spinnerWidget.datePartsWithDocumentFlow(testDateParts);

                        expect(dateParts).toEqual(testDateParts);
                    });

                    it('returns original dateparts if flowOptions is not calculate', () => {
                        let testDateParts = spinnerWidget.parseDateParts('LL');
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('rtl');
                        spinnerWidget.model.set('flow', FlowOptions.LTR);

                        let dateParts = spinnerWidget.datePartsWithDocumentFlow(testDateParts);

                        expect(dateParts).toEqual(testDateParts);
                    });

                    it('returns reverse of all date parts in reverse order (expected display value of toStrictRTL in en-US)', () => {
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('rtl');
                        spinnerWidget.model.set('flow', FlowOptions.CALCULATE);

                        let testDateParts = spinnerWidget.parseDateParts('LL'),
                            dateParts = spinnerWidget.datePartsWithDocumentFlow(testDateParts),
                            expectedDateParts = [];
                        for (let i = testDateParts.length - 1; i >= 0; --i) {
                            let part = testDateParts[i],
                                testFormat = spinnerWidget.moment.format(part);

                            if (testFormat === part) {
                                // not a format for date parsing... so reverse the string
                                part = part.split('').reverse().join('');
                            }
                            expectedDateParts.push(part);
                        }

                        expect(dateParts).toEqual(expectedDateParts);
                    });

                    it('Correctly returns reversed dateparts for D/M/YYYY format (formerly replaced part of the year, due to DE22389', () => {
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('rtl');
                        spinnerWidget.model.set('flow', FlowOptions.CALCULATE);

                        let testDateParts = spinnerWidget.parseDateParts('D/M/YYYY'),
                            dateParts = spinnerWidget.datePartsWithDocumentFlow(testDateParts),
                            expectedDateParts = [];
                        for (let i = testDateParts.length - 1; i >= 0; --i) {
                            let part = testDateParts[i],
                                testFormat = spinnerWidget.moment.format(part);

                            if (testFormat === part) {
                                // not a format for date parsing... so reverse the string
                                part = part.split('').reverse().join('');
                            }
                            expectedDateParts.push(part);
                        }

                        expect(dateParts).toEqual(expectedDateParts);
                    });
                });

                describe('#buildDynamicTemplate', () => {
                    it('creates containers for parsed date parts and separators', () => {
                        spyOn(spinnerWidget, 'parseDateParts').and.returnValue([
                            'MMM', 'DD', 'YY', ' ', 'H', ':', 'mm', ' ~~ ', 'A'
                        ]);
                        let $dateContainer = spinnerWidget.$modal.find('.date-container, .time-container, .date-time-container').first();
                        $dateContainer.empty();
                        spinnerWidget.buildDynamicTemplate();
                        let $dateParts = $dateContainer.find('div');

                        let i = 0;
                        expect($($dateParts.get(i)).attr('class')).toBe('month-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('MMM');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('day-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('DD');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('year-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('YY');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('separator');
                        expect($($dateParts.get(i)).html()).toBe('&nbsp;');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('hour-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('H');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('separator');
                        expect($($dateParts.get(i)).html()).toBe(':');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('minute-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('mm');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('separator');
                        expect($($dateParts.get(i)).html()).toBe('&nbsp;~~&nbsp;');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('meridian-indicator-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('A');
                    });
                });

                describe('#setModalFlow', () => {
                    beforeEach(() => {
                        // Remove any styles that were already on the modal.
                        spinnerWidget.$modal[0].classList.remove('flow-calculate');
                        spinnerWidget.$modal[0].classList.remove('flow-natural');
                        spinnerWidget.$modal[0].classList.remove('flow-ltr');
                        spinnerWidget.$modal[0].classList.remove('flow-rtl');
                    });
                    it('sets flow-calculate class', () => {
                        let expectedClass = 'flow-calculate';
                        spinnerWidget.model.set('flow', FlowOptions.CALCULATE);
                        spinnerWidget.setModalFlow();
                        expect(spinnerWidget.$modal.hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                    it('sets flow-natural class', () => {
                        let expectedClass = 'flow-natural';
                        spinnerWidget.model.set('flow', FlowOptions.NATURAL);
                        spinnerWidget.setModalFlow();
                        expect(spinnerWidget.$modal.hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                    it('sets flow-ltr class', () => {
                        let expectedClass = 'flow-ltr';
                        spinnerWidget.model.set('flow', FlowOptions.LTR);
                        spinnerWidget.setModalFlow();
                        expect(spinnerWidget.$modal.hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                    it('sets flow-rtl class', () => {
                        let expectedClass = 'flow-rtl';
                        spinnerWidget.model.set('flow', FlowOptions.RTL);
                        spinnerWidget.setModalFlow();
                        expect(spinnerWidget.$modal.hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                });

                describe('#injectSpinnerInputs', () => {
                    it('creates a ValueSpinnerInput for each spinner and injects them into the modal dialog', () => {
                        spyOn(spinnerWidget, 'setModalFlow').and.callThrough();
                        spinnerWidget.injectSpinnerInputs();
                        let valueCnt = 0,
                            $datePart;

                        $datePart = spinnerWidget.$modal.find('.year-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = spinnerWidget.$modal.find('.month-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = spinnerWidget.$modal.find('.day-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = spinnerWidget.$modal.find('.hour-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = spinnerWidget.$modal.find('.minute-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = spinnerWidget.$modal.find('.meridian-indicator-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        expect(spinnerWidget.spinners.length).toBe(valueCnt);

                        for (let i = 0; i < spinnerWidget.spinners.length; ++i) {
                            expect(spinnerWidget.spinners[i] instanceof ValueSpinnerInput).toBe(true);
                        }

                        expect(spinnerWidget.setModalFlow).toHaveBeenCalled();
                    });
                });
            });
        });
    }
}
