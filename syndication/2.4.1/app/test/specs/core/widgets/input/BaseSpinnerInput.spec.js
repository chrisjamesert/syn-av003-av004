const {Model, View} = Backbone;
import BaseSpinnerInput from 'core/widgets/input/BaseSpinnerInput';
import Templates from 'core/resources/Templates';

const DEFAULT_DECELERATION = 0.0006,
    AUTO_SCROLL_MILLISECONDS = 250;

/*global describe, beforeEach, afterEach, beforeAll, afterAll, it, spyOn */
TRACE_MATRIX('US6106').
describe('BaseSpinnerInput', () => {
    let options,
        $parent,
        spinnerInput,
        oldTemplateSettings;

    beforeAll(() => {
        // Set the correct interpolation settings for templates.
        oldTemplateSettings = _.templateSettings;
        _.templateSettings = {
            evaluate: /\{\[([\s\S]+?)]}/g,
            interpolate: /\{\{(.+?)}}/g
        };
    });

    afterAll(() => {
        _.templateSettings = oldTemplateSettings;
    });

    beforeEach(() => {
        $parent = $('<div class="spinner-container">');

        document.documentElement.appendChild($parent[0]);

        options = {
            parent: $parent[0],
            itemTemplate: 'DEFAULT:NumberItemTemplate',
            template: LF.templates.display('DEFAULT:NumberSpinnerControl'),
            deceleration: 0.003,
            numItems: 5,
            model: new Model()
        };
    });

    afterEach(() => {
        if (spinnerInput) {
            spinnerInput.remove();
            spinnerInput = null;
        }
        if ($parent) {
            $parent.remove();
            $parent = null;
        }
    });

    describe('Tests before rendered', () => {
        describe('#mouseDown getter', () => {
            beforeEach(() => {
                spinnerInput = new BaseSpinnerInput(options);
            });

            it('Skips last item in array, returns false if last item is mousedown', () => {
                spinnerInput.spinnerEventQueue = ['something', 'something', 'mousedown'];
                expect(spinnerInput.mouseDown).toBe(false);
            });

            it('Returns true if mousedown found more recently than mouseup', () => {
                spinnerInput.spinnerEventQueue = ['mouseup', 'mousedown', 'something'];
                expect(spinnerInput.mouseDown).toBe(true);
            });

            it('Returns false if mouseup found more recently than mousedown', () => {
                spinnerInput.spinnerEventQueue = ['mousedown', 'mouseup', 'something'];
                expect(spinnerInput.mouseDown).toBe(false);
            });

            it('Returns false by default', () => {
                spinnerInput.spinnerEventQueue = ['something', 'something', 'something'];
                expect(spinnerInput.mouseDown).toBe(false);
            });
        });

        describe('#inScroll getter', () => {
            beforeEach(() => {
                spinnerInput = new BaseSpinnerInput(options);
            });

            it('Skips last item in array, returns false if last item is scrollStart', () => {
                spinnerInput.spinnerEventQueue = ['something', 'something', 'scrollStart'];
                expect(spinnerInput.inScroll).toBe(false);
            });

            it('Returns true if scrollStart found more recently than scrollEnd-success', () => {
                spinnerInput.spinnerEventQueue = ['scrollEnd-success', 'scrollStart', 'something'];
                expect(spinnerInput.inScroll).toBe(true);
            });

            it('Returns false if scrollEnd-success found more recently than scrollStart', () => {
                spinnerInput.spinnerEventQueue = ['scrollStart', 'scrollEnd-success', 'something'];
                expect(spinnerInput.inScroll).toBe(false);
            });

            it('Returns false by default', () => {
                spinnerInput.spinnerEventQueue = ['something', 'something', 'something'];
                expect(spinnerInput.inScroll).toBe(false);
            });
        });
        
        describe('#userInitiatedScroll getter', () => {
            beforeEach(() => {
                spinnerInput = new BaseSpinnerInput(options);
            });

            it('Skips last item in array, returns false if last item is beforeScrollStart', () => {
                spinnerInput.spinnerEventQueue = ['something', 'something', 'beforeScrollStart'];
                expect(spinnerInput.userInitiatedScroll).toBe(false);
            });

            it('Returns true if beforeScrollStart found more recently than scrollEnd-success', () => {
                spinnerInput.spinnerEventQueue = ['scrollEnd-success', 'beforeScrollStart', 'something'];
                expect(spinnerInput.userInitiatedScroll).toBe(true);
            });

            it('Returns false if scrollEnd-success found more recently than beforeScrollStart', () => {
                spinnerInput.spinnerEventQueue = ['beforeScrollStart', 'scrollEnd-success', 'something'];
                expect(spinnerInput.userInitiatedScroll).toBe(false);
            });

            it('Returns false by default', () => {
                spinnerInput.spinnerEventQueue = ['something', 'something', 'something'];
                expect(spinnerInput.userInitiatedScroll).toBe(false);
            });
        });
        
        describe('#previousSpinnerEvent getter', () => {
            beforeEach(() => {
                spinnerInput = new BaseSpinnerInput(options);
            });

            it('Returns null if array is of size 1 or smaller', () => {
                spinnerInput.spinnerEventQueue = ['something'];
                expect(spinnerInput.previousSpinnerEvent).toBe(null);
                spinnerInput.spinnerEventQueue = [];
                expect(spinnerInput.previousSpinnerEvent).toBe(null);
            });

            it('Returns second to last event in the queue', () => {
                spinnerInput.spinnerEventQueue = ['something', 'another thing'];
                expect(spinnerInput.previousSpinnerEvent).toBe('something');
                spinnerInput.spinnerEventQueue = ['something', 'another thing', 'a third thing'];
                expect(spinnerInput.previousSpinnerEvent).toBe('another thing');
            });
        });

        describe('#constructor', () => {
            it('throws an error if no parent', () => {
                options.parent = null;
                expect(() => {
                    spinnerInput = new BaseSpinnerInput(options);
                }).toThrow(new Error('Invalid configuration for Spinner.  "parent" is undefined.'));
            });

            it('throws an error if no itemTemplate', () => {
                options.itemTemplate = null;
                expect(() => {
                    spinnerInput = new BaseSpinnerInput(options);
                }).toThrow(new Error('Invalid configuration for Spinner.  "itemTemplate" is undefined.'));
            });

            it('throws an error if no template', () => {
                options.template = null;
                expect(() => {
                    spinnerInput = new BaseSpinnerInput(options);
                }).toThrow(new Error('Invalid configuration for Spinner.  "template" is undefined.'));
            });

            it('sets up model and property defaults', () => {
                spinnerInput = new BaseSpinnerInput(options);
                expect(spinnerInput.model.get('itemTemplate')).toBe(options.itemTemplate);
                expect(spinnerInput.model.get('template')).toBe(options.template);
                expect(spinnerInput.model.get('deceleration')).toBe(options.deceleration);
                expect(spinnerInput.model.get('numItems')).toBe(options.numItems);

                expect(spinnerInput.scroller).toBe(null);
                expect(spinnerInput.isRendered).toBe(false);
                expect(spinnerInput.$itemContainer).toBe(null);
                expect(spinnerInput.userInitiatedScroll).toBe(false);
                expect(spinnerInput.inScroll).toBe(false);
                expect(spinnerInput.mouseDown).toBe(false);
                expect(spinnerInput._$items).toBe(null);
            });

            it('uses default for unsupplied deceleration and numItems', () => {
                options.deceleration = undefined;
                options.numItems = undefined;
                spinnerInput = new BaseSpinnerInput(options);
                expect(spinnerInput.model.get('deceleration')).toBe(DEFAULT_DECELERATION);
                expect(spinnerInput.model.get('numItems')).toBe(null);
            });
        });
        describe('#itemDisplayValueFunction', () => {
            it('returns passed in value', () => {
                spinnerInput = new BaseSpinnerInput(options);
                let testVal = spinnerInput.itemDisplayValueFunction('18');
                expect(testVal).toBe('18');
            });
        });
        describe('#itemValueFunction', () => {
            it('returns passed in value', () => {
                spinnerInput = new BaseSpinnerInput(options);
                let testVal = spinnerInput.itemValueFunction('19');
                expect(testVal).toBe('19');
            });
        });

        describe('#setValues', () => {
            it('throws an exception that it is not yet implemented', () => {
                spinnerInput = new BaseSpinnerInput(options);
                expect(() => {
                    spinnerInput.setValues();
                }).toThrow(new Error('setValues() is not defined in a BaseSpinnerInput extension.'));
            });
        });

        describe('#show', () => {
            Async.it('calls render if not already rendered', () => {
                spinnerInput = new BaseSpinnerInput(options);
                spyOn(spinnerInput, 'render').and.callFake(() => Q());
                return spinnerInput.show()
                    .tap(() => {
                        expect(spinnerInput.render.calls.count()).toBe(1);
                    });
            });
            Async.it('calls refresh on scroller if already rendered', () => {
                spinnerInput = new BaseSpinnerInput(options);
                spinnerInput.scroller = {
                    refresh: $.noop
                };
                spinnerInput.isRendered = true;
                spyOn(spinnerInput.scroller, 'refresh');
                return spinnerInput.show()
                    .tap(() => {
                        expect(spinnerInput.scroller.refresh.calls.count()).toBe(1);
                    });
            });
        });

    });
    describe('Tests after rendering', () => {
        beforeEach((done) => {
            spinnerInput = new BaseSpinnerInput(options);

            /*
             Mock up required instance methods
             */
            //jscs:disable requireArrowFunctions

            // Make a spinner for 0 to 100
            spinnerInput.setValues = _.bind(
                function () {
                    let fullItemString,
                        itemTemplate = this.model.get('itemTemplate'),
                        itemTemplateFactory,
                        template;

                    itemTemplateFactory = LF.templates.getTemplateFromKey(itemTemplate);
                    fullItemString = itemTemplateFactory({value: '', displayValue: '&nbsp;'});
                    // Add range of values if min/max/step are defined.
                    for (let i = 0; i <= 100; ++i) {
                        template = itemTemplateFactory(
                            {value: this.itemValueFunction(i), displayValue: this.itemDisplayValueFunction(i)}
                        );
                        fullItemString += template;
                    }
                    this.$itemContainer.append(fullItemString);
                }, spinnerInput);
            //jscs:enable requireArrowFunctions

            spinnerInput.render().then(() => {
                done();
            }).catch((e) => {
                fail(e);
            });
        });
        describe('#delegateEvents', () => {
            it('calls super', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(spinnerInput, 'removeCustomEvents');
                spyOn(spinnerInput, 'addCustomEvents');

                spinnerInput.delegateEvents();
                expect(View.prototype.delegateEvents.calls.count()).toBe(1);
            });
            it('calls removeEvents()', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(spinnerInput, 'removeCustomEvents');
                spyOn(spinnerInput, 'addCustomEvents');

                spinnerInput.delegateEvents();
                expect(spinnerInput.removeCustomEvents.calls.count()).toBe(1);
            });
            it('calls addCustomEvents()', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(spinnerInput, 'removeCustomEvents');
                spyOn(spinnerInput, 'addCustomEvents');

                spinnerInput.delegateEvents();
                expect(spinnerInput.addCustomEvents.calls.count()).toBe(1);
            });
        });

        describe('#undelegateEvents', () => {
            it('calls super', () => {
                spyOn(View.prototype, 'undelegateEvents');
                spyOn(spinnerInput, 'removeCustomEvents');

                spinnerInput.undelegateEvents();
                expect(View.prototype.undelegateEvents.calls.count()).toBe(1);
            });
            it('calls removeEvents()', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(spinnerInput, 'removeCustomEvents');

                spinnerInput.undelegateEvents();
                expect(spinnerInput.removeCustomEvents.calls.count()).toBe(1);
            });
        });

        describe('#addCustomEvents', () => {
            it('adds appropriate UI events to scroller and item container', () => {
                spyOn(spinnerInput.scroller, 'on');
                spyOn(spinnerInput.$itemContainer, 'on');
                spyOn(spinnerInput.$win, 'on');

                spinnerInput.addCustomEvents();

                expect(spinnerInput.scroller.on).toHaveBeenCalledWith('scrollEnd', spinnerInput.handleScrollEnd);
                expect(spinnerInput.scroller.on)
                    .toHaveBeenCalledWith('beforeScrollStart', spinnerInput.handleBeforeScrollStart);
                expect(spinnerInput.scroller.on).toHaveBeenCalledWith('scrollStart', spinnerInput.handleScrollStart);
                expect(spinnerInput.scroller.on).toHaveBeenCalledWith('scrollCancel', spinnerInput.handleScrollCancel);

                expect(spinnerInput.$itemContainer.on).toHaveBeenCalledWith('tap', spinnerInput.handleTap);

                expect(spinnerInput.$itemContainer.on).toHaveBeenCalledWith('mousedown', spinnerInput.handleMouseDown);
                expect(spinnerInput.$itemContainer.on).toHaveBeenCalledWith('touchstart', spinnerInput.handleMouseDown);
                expect(spinnerInput.$itemContainer.on).toHaveBeenCalledWith('MSPointerDown', spinnerInput.handleMouseDown);
                expect(spinnerInput.$itemContainer.on).toHaveBeenCalledWith('pointerdown', spinnerInput.handleMouseDown);

                expect(spinnerInput.$win.on).toHaveBeenCalledWith('mouseup', spinnerInput.handleMouseUp);
                expect(spinnerInput.$win.on).toHaveBeenCalledWith('touchend', spinnerInput.handleMouseUp);
                expect(spinnerInput.$win.on).toHaveBeenCalledWith('MSPointerUp', spinnerInput.handleMouseUp);
                expect(spinnerInput.$win.on).toHaveBeenCalledWith('pointerup', spinnerInput.handleMouseUp);
            });
        });

        describe('#removeCustomEvents', () => {
            it('removes UI events from scroller and item container', () => {
                spyOn(spinnerInput.scroller, 'off');
                spyOn(spinnerInput.$itemContainer, 'off');
                spyOn(spinnerInput.$win, 'off');

                spinnerInput.removeCustomEvents();

                expect(spinnerInput.scroller.off).toHaveBeenCalledWith('scrollEnd', spinnerInput.handleScrollEnd);
                expect(spinnerInput.scroller.off)
                    .toHaveBeenCalledWith('beforeScrollStart', spinnerInput.handleBeforeScrollStart);
                expect(spinnerInput.scroller.off).toHaveBeenCalledWith('scrollStart', spinnerInput.handleScrollStart);
                expect(spinnerInput.scroller.off).toHaveBeenCalledWith('scrollCancel', spinnerInput.handleScrollCancel);

                expect(spinnerInput.$itemContainer.off).toHaveBeenCalledWith('tap', spinnerInput.handleTap);

                expect(spinnerInput.$itemContainer.off).toHaveBeenCalledWith('mousedown', spinnerInput.handleMouseDown);
                expect(spinnerInput.$itemContainer.off).toHaveBeenCalledWith('touchstart', spinnerInput.handleMouseDown);
                expect(spinnerInput.$itemContainer.off).toHaveBeenCalledWith('MSPointerDown', spinnerInput.handleMouseDown);
                expect(spinnerInput.$itemContainer.off).toHaveBeenCalledWith('pointerdown', spinnerInput.handleMouseDown);

                expect(spinnerInput.$win.off).toHaveBeenCalledWith('mouseup', spinnerInput.handleMouseUp);
                expect(spinnerInput.$win.off).toHaveBeenCalledWith('touchend', spinnerInput.handleMouseUp);
                expect(spinnerInput.$win.off).toHaveBeenCalledWith('MSPointerUp', spinnerInput.handleMouseUp);
                expect(spinnerInput.$win.off).toHaveBeenCalledWith('pointerup', spinnerInput.handleMouseUp);
            });
        });

        describe('#setHeights', () => {
            it('sets the itemHeight property to first item\'s height', () => {
                let calculatedHeight = spinnerInput.$items.first().outerHeight();
                spinnerInput.setHeights();
                expect(spinnerInput.itemHeight).toBe(calculatedHeight);
            });

            Async.it('sets the field height to numItems * itemHeight.  Alter the model to verify it works.', () => {
                return new Q.Promise((resolve) => {
                    let calculatedHeight = spinnerInput.$items.first().outerHeight(),
                        $el = spinnerInput.$el,
                        newNumItems = 7;
                    spinnerInput.model.set('numItems', newNumItems);
                    spinnerInput.setHeights();
                    $el.ready(() => {
                        expect($el.height()).toBe(calculatedHeight * newNumItems);
                        expect(spinnerInput.fieldHeight).toBe(calculatedHeight * newNumItems);
                        resolve();
                    });
                });
            });

            Async.it('leaves height alone if no numItems in model. Sets fieldHeight to existing height', () => {
                return new Q.Promise((resolve) => {
                    let oldElementHeight,
                        $el = spinnerInput.$el;
                    oldElementHeight = $el.height();
                    spinnerInput.model.set('numItems', null);
                    spinnerInput.setHeights();
                    $el.ready(() => {
                        expect($el.height()).toBe(oldElementHeight);
                        expect(spinnerInput.fieldHeight).toBe(oldElementHeight);
                        resolve();
                    });
                });
            });
        });

        describe('#setEmptyAreas', () => {
            it('removes old buffers', () => {
                expect(spinnerInput.$itemContainer.find('.buffer-item').length).toBe(2);

                // spy out append and prepend, so it doesn't do these this time
                spyOn(spinnerInput.$itemContainer, 'prepend');
                spyOn(spinnerInput.$itemContainer, 'append');

                spinnerInput.setEmptyAreas();
                expect(spinnerInput.$itemContainer.find('.buffer-item').length).toBe(0);
            });

            it('prepends and appends buffers with appropriate height', () => {
                let heightsCorrect = true,
                    bufferHeight = (spinnerInput.fieldHeight / 2.0) - (spinnerInput.itemHeight / 2.0);

                // remove buffer items to guarantee that this function actually adds them
                spinnerInput.$itemContainer.find('.buffer-item').remove();
                expect(spinnerInput.$itemContainer.find('.buffer-item').length).toBe(0);
                spinnerInput.setEmptyAreas();
                expect(spinnerInput.$itemContainer.find('.buffer-item').length).toBe(2);

                //jscs:disable requireArrowFunctions
                spinnerInput.$itemContainer.find('.buffer-item').each(function () {
                    heightsCorrect = heightsCorrect && ($(this).height() === bufferHeight);
                });
                //jscs:enable requireArrowFunctions
                expect(heightsCorrect).toBe(true);
            });
        });

        describe('#itemValueComparison', () => {
            it('returns -1 if target value is blank and current value is nonblank (special case)', () => {
                spinnerInput.itemValueFunction = () => 'test';
                expect(spinnerInput.itemValueComparison(1, '')).toBe(-1);
            });

            it('returns 1 if target value is nonblank and current value is blank', () => {
                spinnerInput.itemValueFunction = () => '';
                expect(spinnerInput.itemValueComparison('test', 1)).toBe(1);
            });

            it('returns 1 for strings or numbers if target is greater than current', () => {
                spinnerInput.itemValueFunction = () => 'test';
                expect(spinnerInput.itemValueComparison(1, 'zzz')).toBe(1);
                spinnerInput.itemValueFunction = () => 15;
                expect(spinnerInput.itemValueComparison(1, 105)).toBe(1);
            });

            it('returns -1 for strings or numbers if target is less than current', () => {
                spinnerInput.itemValueFunction = () => 'test';
                expect(spinnerInput.itemValueComparison(1, 'aaaa')).toBe(-1);
                spinnerInput.itemValueFunction = () => 15;
                expect(spinnerInput.itemValueComparison(1, 5)).toBe(-1);
            });

            it('returns 0 for strings or numbers if target is equal to current', () => {
                spinnerInput.itemValueFunction = () => 'test';
                expect(spinnerInput.itemValueComparison(1, 'test')).toBe(0);
                spinnerInput.itemValueFunction = () => 15;
                expect(spinnerInput.itemValueComparison(1, 15)).toBe(0);
            });
        });

        describe('#getItemByValue', () => {
            it('can return first item (by binary search)', () => {
                // spy on filter to verify return happens during binary search
                spyOn(spinnerInput.$items, 'filter');
                // Simple itemValueComparison (always negative 1 unless at our first item)
                spyOn(spinnerInput, 'itemValueComparison').and.callFake((currentIndex) => {
                    if (currentIndex > 0) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                // testVal is param for function... spy overrides behavior and also verifies correct param was passed
                let retItem = spinnerInput.getItemByValue('testval');
                expect($(retItem).data('value')).toBe($(spinnerInput.$items[0]).data('value'));
                expect(spinnerInput.$items.filter.calls.count()).toBe(0);

                expect(spinnerInput.itemValueComparison.calls.count()).toBe(6);
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(50, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(24, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(11, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(5, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(2, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(0, 'testval');
            });

            it('can return last item (by binary search)', () => {
                // spy on filter to verify return happens during binary search
                spyOn(spinnerInput.$items, 'filter');
                // Simple itemValueComparison (always 1 unless at our last item)
                spyOn(spinnerInput, 'itemValueComparison').and.callFake((currentIndex) => {
                    if (currentIndex < spinnerInput.$items.length - 1) {
                        return 1;
                    } else {
                        return 0;
                    }
                });

                // testVal is param for function... spy overrides behavior and also verifies correct param was passed
                let retItem = spinnerInput.getItemByValue('testval');
                expect($(retItem).data('value'))
                    .toBe($(spinnerInput.$items[spinnerInput.$items.length - 1]).data('value'));
                expect(spinnerInput.$items.filter.calls.count()).toBe(0);

                expect(spinnerInput.itemValueComparison.calls.count()).toBe(7);
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(50, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(76, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(89, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(95, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(98, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(100, 'testval');
                expect(spinnerInput.itemValueComparison).toHaveBeenCalledWith(101, 'testval');
            });

            it('can return arbitrary item (by binary search)', () => {
                let targetIndex = 41;
                // spy on filter to verify return happens during binary search
                spyOn(spinnerInput.$items, 'filter');

                spyOn(spinnerInput, 'itemValueComparison').and.callFake((currentIndex) => {
                    if (currentIndex < targetIndex) {
                        return 1;
                    } else if (currentIndex > targetIndex) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                let retItem = spinnerInput.getItemByValue('testval');
                expect($(retItem).data('value'))
                    .toBe($(spinnerInput.$items[targetIndex]).data('value'));
                expect(spinnerInput.$items.filter.calls.count()).toBe(0);
            });

            it('can return item by jquery selector, if binary search fails', () => {
                spyOn(spinnerInput.$items, 'filter').and.callThrough();

                // itemValueComparison will return 1 until binary search fails.
                spyOn(spinnerInput, 'itemValueComparison').and.callFake((currentIndex) => 1);

                let retItem = spinnerInput.getItemByValue(88);
                expect($(retItem).data('value')).toBe(88);
                expect(spinnerInput.$items.filter.calls.count()).toBe(1);
            });

            it('returns null if value could not be found by search or selector', () => {
                spyOn(spinnerInput.$items, 'filter').and.callThrough();

                // itemValueComparison will return 1 until binary search fails.
                spyOn(spinnerInput, 'itemValueComparison').and.callFake((currentIndex) => 1);

                let retItem = spinnerInput.getItemByValue('uh oh');
                expect(retItem).toBe(null);
                expect(spinnerInput.$items.filter.calls.count()).toBe(1);
            });
        });

        describe('#hideItem', () => {
            it('adds hidden class to item', () => {
                let value = 41;
                spinnerInput.hideItem(value);
                let $item = $(spinnerInput.getItemByValue(value));
                expect($item.data('value')).toBe(value);
                // expect($item.hasClass('hidden-item')).toBe(true);
            });

            Async.it('does not scroll if current scroll position is above the item', () => {
                let value = 41;
                let curScrollPos;

                spinnerInput.model.set('autoScrollMilliseconds', 0);

                return Q()
                    .then(() => {
                        return spinnerInput.setValue(value - 1);
                    })
                    .then(() => {
                        curScrollPos = spinnerInput.scroller.y;
                    })
                    .then(() => {
                        spinnerInput.hideItem(value);
                    })
                    .tap(() => {
                        expect(spinnerInput.scroller.y).toBe(curScrollPos);
                    });
            });

            Async.xit('scrolls if current scroll position is below the item', () => {
                let value = 41,
                    curScrollPos;

                spinnerInput.model.set('autoScrollMilliseconds', 0);
                return Q()
                    .then(() => {
                        return spinnerInput.setValue(value + 1);
                    })
                    .then(() => {
                        curScrollPos = spinnerInput.scroller.y;
                    })
                    .then(() => {
                        spinnerInput.hideItem(value);
                    })
                    .tap(() => {
                        let $item = $(spinnerInput.getItemByValue(value));
                        expect(spinnerInput.scroller.y).toBe(curScrollPos + $item.outerHeight());
                    });
            });

            Async.it('ignores scroll if rescrollIfNeeded set to false (default is true)', () => {
                let value = 41,
                    curScrollPos;

                spinnerInput.model.set('autoScrollMilliseconds', 0);
                return Q()
                    .then(() => {
                        return spinnerInput.setValue(value + 1);
                    })
                    .then(() => {
                        curScrollPos = spinnerInput.scroller.y;
                    })
                    .then(() => {
                        spinnerInput.hideItem(value, false);
                    })
                    .tap(() => {
                        expect(spinnerInput.scroller.y).toBe(curScrollPos);
                    });
            });
        });

        describe('#unHideItem', () => {
            Async.it('removes hidden-item class from item', () => {
                let value = 41,
                    $item;
                $item = $(spinnerInput.getItemByValue(value));
                $item.addClass('hidden-item');
                return spinnerInput.unHideItem(value)
                    .tap(() => {
                        expect($item.hasClass('hidden-item')).toBe(false);
                    });
            });

            Async.it('does not scroll if current scroll position is above item.', () => {
                let value = 41,
                    $item,
                    curScrollPos;
                $item = $(spinnerInput.getItemByValue(value));
                $item.addClass('hidden-item');
                spinnerInput.model.set('autoScrollMilliseconds', 0);

                return Q()
                    .then(() => {
                        return spinnerInput.setValue(value - 1);
                    })
                    .then(() => {
                        curScrollPos = spinnerInput.scroller.y;
                    })
                    .then(() => {
                        return spinnerInput.unHideItem(value);
                    })
                    .tap(() => {
                        expect(spinnerInput.scroller.y).toBe(curScrollPos);
                    });
            });

            Async.it('scrolls if current scroll position is below item.', () => {
                let value = 41,
                    $item,
                    curScrollPos;
                $item = $(spinnerInput.getItemByValue(value));
                $item.addClass('hidden-item');
                spinnerInput.model.set('autoScrollMilliseconds', 0);
                return Q()
                    .then(() => {
                        return spinnerInput.setValue(value + 1);
                    })
                    .then(() => {
                        curScrollPos = spinnerInput.scroller.y;
                    })
                    .then(() => {
                        return spinnerInput.unHideItem(value);
                    })
                    .tap(() => {
                        expect(spinnerInput.scroller.y).toBe(curScrollPos - $item.outerHeight());
                    });
            });

            Async.it('ignores scroll if rescrollIfNeeded is set to false (default is true)', () => {
                let value = 41,
                    $item,
                    curScrollPos;
                $item = $(spinnerInput.getItemByValue(value));
                $item.addClass('hidden-item');

                spinnerInput.model.set('autoScrollMilliseconds', 0);

                return Q()
                    .then(() => {
                        return spinnerInput.setValue(value + 1);
                    })
                    .then(() => {
                        curScrollPos = spinnerInput.scroller.y;
                    })
                    .then(() => {
                        return spinnerInput.unHideItem(value, false);
                    })
                    .tap(() => {
                        expect(spinnerInput.scroller.y).toBe(curScrollPos);
                    });
            });
        });

        describe('#stop', () => {
            it('tells scroller to stop', () => {
                spyOn(spinnerInput.scroller, 'scrollBy');
                spinnerInput.stop();
                expect(spinnerInput.scroller.scrollBy).toHaveBeenCalledWith(0, 0, 0);
            });
        });

        describe('#updateSelectedItemUI', () => {
            it('does nothing if selectedItem is not set', () => {
                spinnerInput.selectedItem = null;
                spinnerInput.value = 50;
                spyOn(spinnerInput, 'hideItem');
                spinnerInput.updateSelectedItemUI();

                // Would have been called if not for selectedItem being null (short circuit)
                expect(spinnerInput.hideItem.calls.count()).toBe(0);
            });

            it('adds class to selectedItem', () => {
                let item = spinnerInput.getItemByValue(41);
                spinnerInput.selectedItem = item;
                spinnerInput.value = 41;
                spinnerInput.updateSelectedItemUI();
                expect(item.classList.contains('selected')).toBe(true);
            });

            it('hides blank item if value is non-blank and non-null', () => {
                spinnerInput.selectedItem = spinnerInput.getItemByValue(41);
                spinnerInput.value = 41;

                spyOn(spinnerInput, 'hideItem');
                spinnerInput.updateSelectedItemUI();

                expect(spinnerInput.hideItem.calls.count()).toBe(1);
                expect(spinnerInput.hideItem).toHaveBeenCalledWith('');
            });

            it('does not hide blank item if value is blank or null', () => {
                spinnerInput.selectedItem = spinnerInput.getItemByValue('');
                spyOn(spinnerInput, 'hideItem');

                spinnerInput.value = '';
                spinnerInput.updateSelectedItemUI();

                spinnerInput.value = null;
                spinnerInput.updateSelectedItemUI();

                expect(spinnerInput.hideItem.calls.count()).toBe(0);
            });
            
            it('clears the event queue', () => {
                spinnerInput.selectedItem = spinnerInput.getItemByValue(41);
                spinnerInput.value = 41;

                spinnerInput.spinnerEventQueue = ['something', 'something', 'something'];

                spinnerInput.updateSelectedItemUI();

                expect(spinnerInput.spinnerEventQueue.length).toBe(0);
            });
        });

        describe('#setValue', () => {
            it('unhides the item containing the selected value', () => {
                // make then a blank function... so nothing else happens
                spyOn(spinnerInput, 'unHideItem').and.callFake(() => {
                    return { then: $.noop };
                });
                spinnerInput.setValue(15);
                expect(spinnerInput.unHideItem).toHaveBeenCalledWith(15, false);
            });

            Async.it('sets value property', () => {
                let value = 15;
                let selectedItem = spinnerInput.getItemByValue(value);

                spyOn(spinnerInput, 'unHideItem').and.callFake(() => Q.resolve(selectedItem));
                spyOn(spinnerInput, 'pullValue');

                return spinnerInput.setValue(value).then(() => {
                    expect(spinnerInput.value).toBe(value);
                });
            });

            Async.it('sets selected item, and clears selected class from previous selected item', () => {
                let value = 15,
                    oldValue = 19,
                    selectedItem = spinnerInput.getItemByValue(value),
                    oldItem = spinnerInput.getItemByValue(oldValue);
                spinnerInput.selectedItem = oldItem;
                oldItem.classList.add('selected');

                spyOn(spinnerInput, 'unHideItem').and.callFake(() => Q.resolve(selectedItem));
                spyOn(spinnerInput, 'pullValue');

                return spinnerInput.setValue(value).then(() => {
                    expect(spinnerInput.selectedItem).toBe(selectedItem);
                    expect(oldItem.classList.contains('selected')).toBe(false);
                });
            });

            Async.it('calls pullValue', () => {
                let value = 15,
                    selectedItem = spinnerInput.getItemByValue(value);

                spyOn(spinnerInput, 'unHideItem').and.callFake(() => Q.resolve(selectedItem));
                spyOn(spinnerInput, 'pullValue');

                return spinnerInput.setValue(value).then(() => {
                    expect(spinnerInput.pullValue.calls.count()).toBe(1);
                });
            });
        });
        describe('#pullValue', () => {
            it('clears selectedItem if value is null', () => {
                let oldItem = spinnerInput.getItemByValue(15);
                oldItem.classList.add('selected');
                spinnerInput.selectedItem = oldItem;
                spinnerInput.value = null;

                spinnerInput.pullValue();
                expect(spinnerInput.selectedItem).toBe(null);
                expect(oldItem.classList.contains('selected')).toBe(false);
            });

            it('leaves selectedItem if it already matches our value, does not call getItemByValue', () => {
                let value = 15,
                    item = spinnerInput.getItemByValue(value);

                spyOn(spinnerInput.scroller, 'scrollTo');
                spyOn(spinnerInput, 'updateSelectedItemUI');
                spyOn(spinnerInput, 'getItemByValue').and.callThrough();

                spinnerInput.value = value;
                spinnerInput.selectedItem = item;
                spinnerInput.pullValue();
                expect(spinnerInput.selectedItem).toBe(item);
                expect(spinnerInput.getItemByValue.calls.count()).toBe(0);
            });

            it('does nothing to UI if selectedItem ends up being null', () => {
                let value = 15;

                spyOn(spinnerInput.scroller, 'scrollTo');
                spyOn(spinnerInput, 'updateSelectedItemUI');
                spyOn(spinnerInput, 'getItemByValue').and.callFake(() => null);

                spinnerInput.value = value;
                spinnerInput.selectedItem = null;
                spinnerInput.pullValue();
                expect(spinnerInput.selectedItem).toBe(null);
                expect(spinnerInput.scroller.scrollTo.calls.count()).toBe(0);
                expect(spinnerInput.updateSelectedItemUI.calls.count()).toBe(0);
            });

            it('updates selectedItem if it does not already match, clears old selected item', () => {
                let value = 15,
                    oldValue = 20,
                    item = spinnerInput.getItemByValue(value),
                    oldItem = spinnerInput.getItemByValue(oldValue);

                spyOn(spinnerInput.scroller, 'scrollTo');
                spyOn(spinnerInput, 'updateSelectedItemUI');

                spinnerInput.value = value;
                spinnerInput.selectedItem = oldItem;
                oldItem.classList.add('selected');
                spinnerInput.pullValue();
                expect(spinnerInput.selectedItem).toBe(item);
                expect(oldItem.classList.contains('selected')).toBe(false);
            });

            it('scrolls to point where item is in the middle (if not already there)', () => {
                let value = 15,
                    item = spinnerInput.getItemByValue(value),
                    $item = $(item);

                spyOn(spinnerInput.scroller, 'scrollTo');
                spyOn(spinnerInput, 'updateSelectedItemUI');

                spinnerInput.value = value;

                // calculate target scroll point
                let top = $item.position().top,
                    height = $item.height(),
                    targetScrollTop;

                targetScrollTop = -(top - ((spinnerInput.fieldHeight / 2.0) - (height / 2.0)));
                spinnerInput.pullValue();
                expect(spinnerInput.scroller.scrollTo)
                    .toHaveBeenCalledWith(0, targetScrollTop, AUTO_SCROLL_MILLISECONDS);
            });

            Async.it('does not scroll if already in correct spot.  calls updateSelectedItemUI', () => {
                let value = 15,
                    item = spinnerInput.getItemByValue(value),
                    $item = $(item);

                spinnerInput.value = value;

                // calculate target scroll point
                let top = $item.position().top,
                    height = $item.height(),
                    targetScrollTop;

                targetScrollTop = -(top - ((spinnerInput.fieldHeight / 2.0) - (height / 2.0)));
                return Q()
                    .then(() => {
                        spinnerInput.scroller.scrollTo(0, targetScrollTop);
                    })
                    .then(() => {
                        spyOn(spinnerInput.scroller, 'scrollTo');
                        spyOn(spinnerInput, 'updateSelectedItemUI');
                        spinnerInput.pullValue();
                        expect(spinnerInput.scroller.scrollTo.calls.count()).toBe(0);
                        expect(spinnerInput.updateSelectedItemUI.calls.count()).toBe(1);
                    });
            });
        });

        describe('#itemPositionComparison', () => {
            it('returns null if item is set to display:none (edge case)', () => {
                let targetNdx = 76,
                    targetItem = spinnerInput.$items[targetNdx];
                $(targetItem).css('display', 'none');
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(null);
            });

            it('if scroll position is further than end of list, last item returns 0 (edge case)', () => {
                let targetNdx = 101;
                spinnerInput.scroller = {
                    y: spinnerInput.scroller.maxScrollY - 20
                };
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(0);
            });

            it('if scroll position is above beginning of list, first item returns 0 (edge case)', () => {
                let targetNdx = 0;
                spinnerInput.scroller = {
                    y: 20 // above the list by 20 pixels
                };
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(0);
            });

            it('if scroll position is above item (by 1 pixel), return -1', () => {
                let targetNdx = 41,
                    $item = $(spinnerInput.$items[targetNdx]);
                let top = $item.position().top,
                    height = $item.height(),
                    targetScrollTop;

                targetScrollTop = -(top - ((spinnerInput.fieldHeight / 2.0) - (height / 2.0))) + (height / 2.0) + 1;
                spinnerInput.scroller = {
                    y: targetScrollTop
                };
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(-1);
            });

            it('if scroll position is just inside item on top (by 1 pixel), return 0', () => {
                let targetNdx = 41,
                    $item = $(spinnerInput.$items[targetNdx]);
                let top = $item.position().top,
                    height = $item.height(),
                    targetScrollTop;

                targetScrollTop = -(top - ((spinnerInput.fieldHeight / 2.0) - (height / 2.0))) + (height / 2.0);
                spinnerInput.scroller = {
                    y: targetScrollTop
                };
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(0);
            });

            it('if scroll position is just inside item on bottom (by 1 pixel), return 0', () => {
                let targetNdx = 41,
                    $item = $(spinnerInput.$items[targetNdx]);
                let top = $item.position().top,
                    height = $item.height(),
                    targetScrollTop;

                targetScrollTop = -(top - ((spinnerInput.fieldHeight / 2.0) - (height / 2.0))) - (height / 2.0) + 1;
                spinnerInput.scroller = {
                    y: targetScrollTop
                };
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(0);
            });

            it('if scroll position is just outside item on bottom (by 1 pixel), return 1', () => {
                let targetNdx = 41,
                    $item = $(spinnerInput.$items[targetNdx]);
                let top = $item.position().top,
                    height = $item.height(),
                    targetScrollTop;

                targetScrollTop = -(top - ((spinnerInput.fieldHeight / 2.0) - (height / 2.0))) - (height / 2.0);
                spinnerInput.scroller = {
                    y: targetScrollTop
                };
                expect(spinnerInput.itemPositionComparison(targetNdx)).toBe(1);
            });
        });
        describe('#pushValue', () => {
            it('uses existing selectedItem to populate value when appropriate and calls pullValue', () => {
                let value = 49;
                spinnerInput.selectedItem = spinnerInput.getItemByValue(value);
                spyOn(spinnerInput, 'pullValue');
                spinnerInput.pushValue();
                expect(spinnerInput.value).toBe(value);
                expect(spinnerInput.pullValue.calls.count()).toBe(1);
            });

            it('can find first item with binary search', () => {
                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((index) => {
                    if (index > 0) {
                        return -1;
                    }
                    return 0;
                });
                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[0]);
                expect(spinnerInput.itemPositionComparison.calls.count()).toBe(6);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(50);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(24);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(11);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(5);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(2);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(0);
            });

            it('can find last item with binary search', () => {
                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((index) => {
                    if (index < spinnerInput.$items.length - 1) {
                        return 1;
                    }
                    return 0;
                });
                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[spinnerInput.$items.length - 1]);
                expect(spinnerInput.itemPositionComparison.calls.count()).toBe(7);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(50);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(76);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(89);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(95);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(98);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(100);
                expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(101);
            });

            it('can find an arbitrary index with binary search', () => {
                let targetIndex = 82;

                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((currentIndex) => {
                    if (currentIndex < targetIndex) {
                        return 1;
                    } else if (currentIndex > targetIndex) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[targetIndex]);
            });

            it('adjusts binary search index forward to avoid null comparisons', () => {
                let targetIndex = 45;

                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((currentIndex) => {
                    if (currentIndex >= 50 && currentIndex < 57) {
                        return null;
                    } else if (currentIndex < targetIndex) {
                        return 1;
                    } else if (currentIndex > targetIndex) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[targetIndex]);
                for (let i = 50; i <= 57; ++i) {
                    expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(i);
                }
            });

            it('adjusts binary search index backwards if all forward options return null values', () => {
                let targetIndex = 37;

                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((currentIndex) => {
                    if (currentIndex > 48) {
                        return null;
                    } else if (currentIndex < targetIndex) {
                        return 1;
                    } else if (currentIndex > targetIndex) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[targetIndex]);
                for (let i = 50; i <= 101; ++i) {
                    expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(i);
                }
                for (let i = 50; i >= 48; --i) {
                    expect(spinnerInput.itemPositionComparison).toHaveBeenCalledWith(i);
                }
            });

            it('adjusts selected item to be next that is not hidden', () => {
                let targetIndex = 76,
                    actualSelectedIndex = 98;

                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((currentIndex) => {
                    if (currentIndex < targetIndex) {
                        return 1;
                    } else if (currentIndex > targetIndex) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                for (let i = targetIndex; i < actualSelectedIndex; ++i) {
                    spinnerInput.$items[i].classList.add('hidden-item');
                }

                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[actualSelectedIndex]);
            });

            it('adjusts selected item to be previous that is not hidden if all till the end are hidden', () => {
                let targetIndex = 76,
                    actualSelectedIndex = 43;

                spyOn(spinnerInput, 'itemPositionComparison').and.callFake((currentIndex) => {
                    if (currentIndex < targetIndex) {
                        return 1;
                    } else if (currentIndex > targetIndex) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                for (let i = targetIndex; i < spinnerInput.$items.length; ++i) {
                    spinnerInput.$items[i].classList.add('hidden-item');
                }

                for (let i = targetIndex; i > actualSelectedIndex; --i) {
                    spinnerInput.$items[i].classList.add('hidden-item');
                }

                spinnerInput.pushValue();
                expect(spinnerInput.selectedItem).toBe(spinnerInput.$items[actualSelectedIndex]);
            });
        });

        describe('#handleScrollEnd', () => {
            it('does nothing if mouseDown is true', () => {
                spinnerInput.spinnerEventQueue = ['scrollStart', 'mousedown', 'scrollEnd'];

                spyOn(spinnerInput, 'updateSelectedItemUI');
                spyOn(spinnerInput, 'pushValue');

                spinnerInput.handleScrollEnd();
                expect(spinnerInput.updateSelectedItemUI.calls.count()).toBe(0);
                expect(spinnerInput.pushValue.calls.count()).toBe(0);
            });

            it('calls updateSelectItemUI and exits if scroll is not initiated by user', () => {
                spinnerInput.spinnerEventQueue = ['scrollEnd-success', 'scrollEnd'];

                spyOn(spinnerInput, 'updateSelectedItemUI');
                spyOn(spinnerInput, 'pushValue');

                spinnerInput.handleScrollEnd();
                expect(spinnerInput.updateSelectedItemUI.calls.count()).toBe(1);
                expect(spinnerInput.pushValue.calls.count()).toBe(0);
            });

            it('calls pushValue if scroll is initiated by user', () => {
                spinnerInput.spinnerEventQueue = ['beforeScrollStart', 'scrollEnd'];
                
                spyOn(spinnerInput, 'updateSelectedItemUI');
                spyOn(spinnerInput, 'pushValue');

                spinnerInput.handleScrollEnd();
                expect(spinnerInput.updateSelectedItemUI.calls.count()).toBe(0);
                expect(spinnerInput.pushValue.calls.count()).toBe(1);
            });
        });

        describe('#handleBeforeScrollStart', () => {
            it('appends itself to spinnerEventQueue', () => {
                spinnerInput.spinnerEventQueue = ['scrollEnd-success'];
                spinnerInput.handleBeforeScrollStart();
                
                expect(spinnerInput.spinnerEventQueue[spinnerInput.spinnerEventQueue.length - 1]).toBe('beforeScrollStart');
            });
        });

        describe('#handleScrollStart', () => {
            it('exits if user did not initiate the scroll, does not alter value or inScroll', () => {
                spyOn(spinnerInput, 'pullValue');
                spinnerInput.spinnerEventQueue = ['scrollEnd-success'];
                spinnerInput.value = 'stuff';
                spinnerInput.handleScrollStart();
                expect(spinnerInput.value).toBe('stuff');
                expect(spinnerInput.inScroll).toBe(false);
                expect(spinnerInput.pullValue.calls.count()).toBe(0);
            });

            it('sets value to null, adds self to event queue, and calls pullValue if user initiated the scroll', () => {
                spyOn(spinnerInput, 'pullValue');
                spinnerInput.spinnerEventQueue = ['beforeScrollStart'];
                spinnerInput.value = 'stuff';
                spinnerInput.handleScrollStart();
                expect(spinnerInput.value).toBe(null);
                expect(spinnerInput.spinnerEventQueue[spinnerInput.spinnerEventQueue.length - 1]).toBe('scrollStart');
                expect(spinnerInput.pullValue.calls.count()).toBe(1);
            });
        });

        describe('#handleMouseDown', () => {
            it('adds mousedown to spinnerEventQueue', () => {
                spinnerInput.handleMouseDown();
                expect(spinnerInput.spinnerEventQueue[spinnerInput.spinnerEventQueue.length - 1]).toBe('mousedown');
            });
        });
        
        describe('#handleMouseUp', () => {
            it('adds mouseup to spinnerEventQueue', () => {
                spinnerInput.handleMouseUp();
                expect(spinnerInput.spinnerEventQueue[spinnerInput.spinnerEventQueue.length - 1]).toBe('mouseup');
            });

            it('calls handleScrollEnd if scrollEnd (not scrollEnd-success) was the last event', () => {
                spinnerInput.spinnerEventQueue = ['scrollEnd'];
                spyOn(spinnerInput, 'handleScrollEnd');
                spinnerInput.handleMouseUp();
                expect(spinnerInput.handleScrollEnd).toHaveBeenCalled();
            });

            it('does not call handleScrollEnd if scrollEnd is not the last event', () => {
                spinnerInput.spinnerEventQueue = ['scrollStart'];
                spyOn(spinnerInput, 'handleScrollEnd');
                spinnerInput.handleMouseUp();
                expect(spinnerInput.handleScrollEnd.calls.count()).toBe(0);
            });
        });

        describe('#handleTap', () => {
            it('calls handleScrollEnd if tap was used to stop the scroller', () => {
                let targetNdx = 14,
                    e = {target: spinnerInput.$items[targetNdx]};
                spyOn(spinnerInput, 'pullValue');
                spyOn(spinnerInput, 'handleScrollEnd');

                spinnerInput.spinnerEventQueue = ['beforeScrollStart', 'scrollStart'];

                spinnerInput.handleTap(e);
                expect(spinnerInput.handleScrollEnd.calls.count()).toBe(1);
                expect(spinnerInput.pullValue.calls.count()).toBe(0);
            });

            it('finds the closest item, sets value, calls pullValue, resets userInitiatedScroll', () => {
                let targetNdx = 14,
                    e = {target: spinnerInput.$items[targetNdx]};
                spyOn(spinnerInput, 'pullValue');
                
                spinnerInput.spinnerEventQueue = [];

                spinnerInput.handleTap(e);
                expect(spinnerInput.value).toBe($(e.target).data('value'));
                expect(spinnerInput.userInitiatedScroll).toBe(false);
                expect(spinnerInput.pullValue.calls.count()).toBe(1);
            });
        });
        
        describe('#clearItemCache', () => {
            it('sets items array to null, items getter refreshes the list', () => {
                spinnerInput.clearItemCache();
                expect(spinnerInput._$items).toBe(null);
                expect(spinnerInput.$items.length).toBe(102);
                expect(spinnerInput._$items.length).toBe(102);
            });
        });
        
        describe('#destroy', () => {
            it('destroys references in object', () => {
                spyOn(spinnerInput, 'clearItemCache');
                spyOn(spinnerInput, 'undelegateEvents');
                spyOn(spinnerInput.scroller, 'destroy');
                let scroller = spinnerInput.scroller;

                spinnerInput.destroy();

                expect(spinnerInput.clearItemCache).toHaveBeenCalled();
                expect(spinnerInput.undelegateEvents).toHaveBeenCalled();
                expect(scroller.destroy).toHaveBeenCalled();
                expect(spinnerInput.$itemContainer).toBe(null);
                expect(spinnerInput.scroller).toBe(null);
            });
        });
    });
});
