import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import WidgetBaseTests from './WidgetBase.specBase.js';
import Widget from 'core/models/Widget';
import ImageWidgetModal from 'core/widgets/ImageWidgetModal';

const defaultModalTemplate = 'DEFAULT:MultipleChoiceModal';

/*global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class ImageWidgetModalTests extends WidgetBaseTests {
    constructor (model = ImageWidgetModalTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'ImageWidgetModal_W_1',
            type: 'ImageWidgetModal',
            imageName: 'skeleton.svg',
            modalTitle: 'SELECT_PAIN',
            options: [
                {
                    value: '0',
                    css: {
                        fill: '#FFFFFF'
                    }
                }, {
                    value: '1',
                    css: {
                        fill: '#0000FF'
                    }
                }, {
                    value: '2',
                    css: {
                        fill: '#FF0000'
                    }
                }, {
                    value: '3',
                    css: {
                        fill: '#00FF00'
                    }
                }
            ],
            inputOptions: {
                itemTemplate: 'DEFAULT:ModalMultipleChoiceItem',
                type: 'MultipleChoiceInput',
                items: [{
                    text: 'IMAGE_OPTION_0',
                    value: '0'
                }, {
                    text: 'IMAGE_OPTION_1',
                    value: '1'
                }, {
                    text: 'IMAGE_OPTION_2',
                    value: '2'
                }, {
                    text: 'IMAGE_OPTION_3',
                    value: '3'
                }]
            },

            imageText: {
                legend0: 'Q4_LEGEND_0',
                legend1: 'Q4_LEGEND_1'
            },
            nodeIDRegex: /[rl]\d+/                      
        });
    }

    get basicModel () {
        return new Widget({
            id: 'ImageWidgetModal_W_1',
            type: 'ImageWidgetModal',
            imageName: 'skeleton.svg',
            modalTitle: 'SELECT_PAIN',
            options: [
                {
                    value: '0',
                    css: {
                        fill: '#FFFFFF'
                    }
                }, {
                    value: '1',
                    css: {
                        fill: '#0000FF'
                    }
                }, {
                    value: '2',
                    css: {
                        fill: '#FF0000'
                    }
                }, {
                    value: '3',
                    css: {
                        fill: '#00FF00'
                    }
                }
            ],
            inputOptions: {
                itemTemplate: 'DEFAULT:ModalMultipleChoiceItem',
                type: 'MultipleChoiceInput',
                items: [{
                    text: 'IMAGE_OPTION_0',
                    value: '0'
                }, {
                    text: 'IMAGE_OPTION_1',
                    value: '1'
                }, {
                    text: 'IMAGE_OPTION_2',
                    value: '2'
                }, {
                    text: 'IMAGE_OPTION_3',
                    value: '3'
                }]
            },

            imageText: {
                legend0: 'Q4_LEGEND_0',
                legend1: 'Q4_LEGEND_1'
            },
            nodeIDRegex: /[rl]\d+/        
        });
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('object.imageSvg').length).toEqual(1);
            });
    }

    execTests () {
        // eslint-disable-next-line new-cap
        TRACE_MATRIX('US7326').
        describe('ImageWidgetModal', () => {
            super.execTests();

            describe('Instance Tests', () => {
                let templates,
                    dummyParent,
                    security,
                    widget = null;

                beforeAll(() => {
                    security = LF.security;
                    LF.security = this.getDummySecurity();
                    templates = LF.templates;
                });

                afterAll(() => {
                    LF.security = security;
                    LF.templates = templates;
                });

                beforeEach(() => {
                    dummyParent = this.getMinDummyQuestion();
                    widget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });

                afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                    widget = null;
                });

                describe('getters', () => {
                    beforeEach (() => {
                        widget = new LF.Widget[this.basicModel.get('type')]({
                            model: this.basicModel,
                            mandatory: false,
                            parent: this.dummyParent
                        });
                    });

                    describe ('#defaultModalTemplate', () => {
                        it ('returns expected value', () => {
                            expect(widget.defaultModalTemplate).toBe(defaultModalTemplate);
                        });
                    });
                });

                describe('#constructor', () => {
                    it('initalizes with default values', () => {
                        widget = new LF.Widget[this.basicModel.get('type')]({
                            model: this.basicModel,
                            mandatory: false,
                            parent: this.dummyParent
                        });

                        expect(widget.$selectedNode).toBeNull();
                    });
                });
                
                describe('#render', () => {
                    Async.it('renders the widget correctly', () => {
                        spyOn(widget, 'getModalTranslationsObject').and.callThrough();
                        spyOn(widget, 'renderModal').and.callThrough();
                        spyOn(widget, 'delegateEvents').and.callThrough();

                        return widget.render().tap(() => {
                            expect(widget.getModalTranslationsObject).toHaveBeenCalled();
                            expect(widget.renderModal).toHaveBeenCalled();
                            expect(widget.delegateEvents).toHaveBeenCalled();
                        });
                    });
                });

                describe('post-rendered methods', () => {
                    Async.beforeEach(() => {
                        return widget.render();
                    });

                    describe('#dialogClosing()', () => {
                        Async.it('selects option associated with value', () => {
                            spyOn(widget, 'getModalValuesString').and.resolve('testval');
                            spyOn(widget, 'getOptionIndex').and.returnValue(1);
                            widget.$selectedNode = $('<div></div>');
                            spyOn(widget.$selectedNode, 'data');
                            spyOn(widget.$selectedNode, 'css');
                            spyOn(widget, 'handleMultiSelect').and.resolve();
                            // cache selected node, since it becomes null
                            let $oldSelectedNode = widget.$selectedNode;
                            return widget.dialogClosing().tap(() => {
                                expect(widget.getModalValuesString).toHaveBeenCalled();
                                expect(widget.getOptionIndex).toHaveBeenCalledWith('testval');
                                expect($oldSelectedNode.data).toHaveBeenCalledWith('state', this.model.get('options')[1].value);
                                expect($oldSelectedNode.css).toHaveBeenCalledWith(this.model.get('options')[1].css);
                                expect(widget.$selectedNode).toBeNull();
                                expect(widget.handleMultiSelect).toHaveBeenCalled();
                            });
                        });
                    });
                    
                    describe('#getInputValuesArray', () => {
                        it('returns 0 if selected node has no state', () => {
                            widget.$selectedNode = $('<div></div>');
                            expect(widget.getInputValuesArray()).toEqual(['0']);
                        });
                        it('returns state of selected node', () => {
                            widget.$selectedNode = $('<div data-state="2"></div>');
                            expect(widget.getInputValuesArray()).toEqual([2]);
                        });
                    });

                    describe('#getModalValuesString', () => {
                        Async.it('calls push value', () => {
                            spyOn(widget.inputs[0], 'pushValue');
                            return widget.openDialog().then(() => {
                                return widget.getModalValuesString();
                            }).tap(() => {
                                expect(widget.inputs[0].pushValue).toHaveBeenCalled();
                            });
                        });

                        Async.it('returns item display value of input', () => {
                            spyOn(widget.inputs[0], 'itemDisplayValueFunction').and.returnValue(12);
                            return widget.openDialog().then(() => {
                                return widget.getModalValuesString();
                            }).tap((value) => {
                                expect(value).toBe(12);
                            });
                        });
                    });
                    
                    describe('#respond()', () => {
                        Async.it('sets selected node and calls openDialog', () => {
                            let $div = $('<div></div>');
                            spyOn(widget, 'openDialog').and.resolve();
                            let input = {target: $div};
                            return widget.respond(input).tap(() => {
                                expect(widget.$selectedNode.get(0)).toEqual($div.get(0));
                                expect(widget.openDialog).toHaveBeenCalled();
                            });
                        });
                    });
                });
            });
        });
    }
}

let tester = new ImageWidgetModalTests();
tester.execTests();