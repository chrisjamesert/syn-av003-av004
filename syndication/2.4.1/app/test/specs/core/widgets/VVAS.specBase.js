import VerticalVas from 'core/widgets/VAS/VerticalVAS';
import WidgetBaseTests from './WidgetBase.specBase';
import Widget from 'core/models/Widget';
import {resetStudyDesign} from '../../../helpers/StudyDesign';

export default class VVASTests extends WidgetBaseTests {

    htmlRenderedCheck () {
        let myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                let $canvas = $(`#${widget.question.id}`).find('canvas');
                expect($canvas.length)
                    .toEqual(1);
                // verify revealCanvas is working
                expect($canvas.css('visibility')).toBe('visible');
            });
    }

    constructor (model) {
        if (model === undefined) {
            model = this.simpleModel;
        }

        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    get simpleModel () {
        return new Widget({
            id: 'VVAS_DIARY_W_1',
            type: 'VAS',
            displayAs: 'Vertical',
            anchors: {
                'min': {
                    'text': 'NO_PAIN'
                },
                'max': {
                    'text': 'EXTREME_PAIN'
                }
            }
        });
    }

    /**
     * Any tests you want executed should be added here
     */
    execTests () {
        describe('Base Widget tests', () => {
            super.execTests();
        });

        describe('VVAS tests', () => {

            let templates,
                dummyParent,
                security,
                VVASWidget = null;

            describe('VVAS Tests', () => {
                Async.beforeAll(() => {
                    resetStudyDesign();

                    security = LF.security;
                    LF.security = this.getDummySecurity();
                    templates = LF.templates;
                    return Q();
                });

                Async.afterAll(() => {
                    LF.security = security;
                    LF.templates = templates;
                    return Q();
                });

                Async.beforeEach(() => {
                    dummyParent = this.getMinDummyQuestion();
                    return Q();
                });

                Async.afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                    VVASWidget = null;
                    return Q();
                });

                Async.it('creates object of type VerticalVas', () => {
                    let _model = this.simpleModel;

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .tap(() => {
                            expect(VVASWidget instanceof VerticalVas).toBe(true);
                        });
                });

                Async.it('creates a vas canvas with the id that matches the model id', () => {
                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: this.simpleModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .tap(() => {
                            expect($('#VVAS_DIARY_W_1').length).toBeTruthy();
                        });
                });

                Async.it('renderPointer is called on load when answer is undefined and pointer.displayInitially is true', () => {
                    let theModel = this.simpleModel;

                    theModel.set('pointer',
                        {
                            displayInitially: 'true'
                        }
                    );

                    spyOn(VerticalVas.prototype, 'renderThePointer');

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .tap(() => {
                            expect(VerticalVas.prototype.renderThePointer).toHaveBeenCalled();
                        });
                });

                Async.it('does NOT call renderPointer on initial load when answer is undefined and initialCursorDisplay is false', () => {
                    let theModel = this.simpleModel;
                    theModel.set('initialCursorDisplay', false);

                    spyOn(VerticalVas.prototype, 'renderThePointer');

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .tap(() => {
                            expect(VerticalVas.prototype.renderThePointer).not.toHaveBeenCalled();
                        });
                });

                Async.xit('sets the answer to zero when tapping at the top of the VAS line', () => {
                    let theModel = this.simpleModel,
                        eMouseDown = jQuery.Event('mousedown', {pageX: 0, pageY: 0});

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {
                            eMouseDown.pageX = VVASWidget.vasCanvas.width / 2;
                            eMouseDown.pageY = VVASWidget.vasLineStart + VVASWidget.vasCanvas.offsetTop;

                            VVASWidget.mouseyDownHandler(eMouseDown);

                            expect(VVASWidget.answer.get('response')).toBe('0');
                        });
                });

                Async.xit('sets the answer to 100 when tapping at the bottom of the VAS line', () => {
                    let theModel = this.simpleModel,
                        e = jQuery.Event('mousedown', {pageX: 0, pageY: 10000});

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {
                            e.pageX = VVASWidget.vasCanvas.width / 2;
                            e.pageY = VVASWidget.vasLineEnd + VVASWidget.vasCanvas.offsetTop;

                            VVASWidget.mouseyDownHandler(e);

                            expect(VVASWidget.answer.get('response')).toBe('100');
                        });
                });

                Async.xit('sets the answer to zero when tapping ABOVE the VAS line', () => {
                    let theModel = this.simpleModel,
                        e = jQuery.Event('mousedown', {pageX: 0, pageY: 0});

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {
                            VVASWidget.translated_topAnchorText = '';
                            VVASWidget.translated_bottomAnchorText = '';

                            e.pageX = VVASWidget.vasCanvas.width / 2;
                            e.pageY = VVASWidget.vasLineStart - 5000;

                            VVASWidget.mouseyDownHandler(e);

                            expect(VVASWidget.answer.get('response')).toBe('0');
                        });
                });

                Async.xit('sets the answer to 100 when tapping BELOW the VAS line', () => {
                    let theModel = this.simpleModel,
                        e = jQuery.Event('mousedown', {pageX: 0, pageY: 0});

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {
                            VVASWidget.translated_topAnchorText = '';
                            VVASWidget.translated_bottomAnchorText = '';

                            e.pageX = VVASWidget.vasCanvas.width / 2;
                            e.pageY = VVASWidget.vasLineEnd + 50000;

                            VVASWidget.mouseyDownHandler(e);

                            expect(VVASWidget.answer.get('response')).toBe('100');
                        });
                });

                Async.xit('sets the answer to 50 when tapping 1/2 way down the VAS line', () => {
                    let theModel = this.simpleModel,
                        e = jQuery.Event('mousedown', {pageX: 0, pageY: 0}),
                        vasLineStartValue = 0;

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {

                            VVASWidget.translated_topAnchorText = '';
                            VVASWidget.translated_bottomAnchorText = '';

                            vasLineStartValue = VVASWidget.vasLineStart;

                            e.pageX = VVASWidget.vasCanvas.width / 2;
                            e.pageY = ((VVASWidget.vasLineEnd - VVASWidget.vasLineStart) / 2) + vasLineStartValue +
                                VVASWidget.vasCanvas.offsetTop;

                            VVASWidget.mouseyDownHandler(e);

                            expect(VVASWidget.answer.get('response')).toBe('50');
                        });
                });

                Async.it('correctly sets this.mousedown during mouse events', () => {
                    let theModel = this.simpleModel,
                        eMouseDown = jQuery.Event('mousedown', {pageX: 0, pageY: 0}),
                        eMouseUp = jQuery.Event('mouseup', {pageX: 0, pageY: 0});

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {
                            eMouseDown.pageX = VVASWidget.centerX;
                            eMouseUp.pageX = VVASWidget.centerX;

                            VVASWidget.mouseyDownHandler(eMouseDown);
                            expect(VVASWidget.mouseDown).toBe(true);

                            VVASWidget.mouseUpHandler(eMouseUp);
                            expect(VVASWidget.mouseDown).toBe(false);
                        });
                });

                Async.it('calls canvas render functions after tap occurs', () => {
                    let theModel = this.simpleModel,
                        eMouseDown = jQuery.Event('mousedown', {pageX: 100, pageY: 100});

                    VVASWidget = new LF.Widget[this.model.get('type')]({
                        model: theModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return VVASWidget.render()
                        .then(() => {
                            VVASWidget.translated_topAnchorText = '';
                            VVASWidget.translated_bottomAnchorText = '';

                            eMouseDown.pageX = VVASWidget.vasCanvas.width / 2;
                            eMouseDown.pageY = ((VVASWidget.vasLineEnd - VVASWidget.vasLineStart) / 2) + VVASWidget.vasLineStart +
                                VVASWidget.vasCanvas.offsetTop;

                            spyOn(VVASWidget, 'renderScale');
                            spyOn(VVASWidget, 'renderMinAndMaxAnchors');
                            spyOn(VVASWidget, 'renderThePointer');
                            spyOn(VVASWidget, 'renderTheStrikeMark');
                            spyOn(VVASWidget, 'renderAnswerValue');

                            // Assume click is within touchable area.
                            spyOn(VVASWidget, 'isWithinTouchableArea').and.returnValue(true);

                            VVASWidget.mouseyDownHandler(eMouseDown);

                            expect(VVASWidget.renderScale).toHaveBeenCalled();
                            expect(VVASWidget.renderMinAndMaxAnchors).toHaveBeenCalled();
                            expect(VVASWidget.renderThePointer).toHaveBeenCalled();
                            expect(VVASWidget.renderTheStrikeMark).toHaveBeenCalled();
                            expect(VVASWidget.renderAnswerValue).toHaveBeenCalled();
                        });
                });
            });
        });
    }
}
