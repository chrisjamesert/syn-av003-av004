import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Answers from 'core/collections/Answers';
import User from 'core/models/User';
import Dashboard from 'core/models/Dashboard';
import Subject from 'core/models/Subject';
import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Widget from 'core/models/Widget';
import Templates from 'core/collections/Templates';
import Logger from 'core/Logger';

import TextBoxWidgetBase from 'core/widgets/TextBoxWidgetBase';

import QuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionView from 'core/views/QuestionView';

import 'core/resources/Templates';
import WidgetBaseTests from './WidgetBase.specBase';

class NumericTextBoxTests extends WidgetBaseTests {

    static get model () {
        return new Widget({
            id: 'NTB_1',
            type: 'NumericTextBox',
            className: 'NTB_1',
            min: 1,
            max: 100,
            step: 1
        });
    }

    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                // expect($(':input[type="number"]').length).toBe(1);
                expect($(`#${this.dummyParent.id} :input[type="number"]`).length).toBe(1);
            });
    }

}

TRACE_MATRIX('US6104').
describe('NumericTextBox Widget', () => {
    let security,
        templates;

    beforeAll(() => {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();

        templates = LF.templates;
    });
    afterAll(() => {
        LF.security = security;
        LF.templates = templates;
    });

    let dummyParent;

    describe('NumericTextBox', () => {
        const buildModel = () => {
            return new Widget({
                id: 'NTB_1',
                type: 'NumericTextBox',
                className: 'NTB_1',
                min: 1,
                max: 100,
                step: 1
            });
        },
            baseTests = new NumericTextBoxTests(buildModel());

        baseTests.execTests();

        describe('constructor', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            it('should call super', () => {
                spyOn(TextBoxWidgetBase.prototype, 'constructor')
                    .and.callThrough();
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                expect(TextBoxWidgetBase.prototype.constructor).toHaveBeenCalledWith(options);
                expect(TextBoxWidgetBase.prototype.constructor.calls.count()).toEqual(1);
            });

            it('should have a default step value of 1', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);

                expect(widget.step).toEqual(1);
            });

            it('should not accept a step value that is not a number', () => {
                const model = new Widget({
                    id: 'NTB_1',
                    type: 'NumericTextBox',
                    className: 'NTB_1',
                    min: 1,
                    max: 100,
                    step: 'foobar'
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); })
                        .toThrow();
            });

            it('should not accept a missing min value', () => {
                const model = new Widget({
                    id: 'NTB_1',
                    type: 'NumericTextBox',
                    className: 'NTB_1',
                    max: 100,
                    step: 1
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); })
                        .toThrow();
            });

            it('should not accept a min value that is not a number', () => {
                const model = new Widget({
                    id: 'NTB_1',
                    type: 'NumericTextBox',
                    className: 'NTB_1',
                    min: 'foobar',
                    max: 100,
                    step: 1
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); })
                        .toThrow();
            });

            it('should not accept a missing max value', () => {
                const model = new Widget({
                    id: 'NTB_1',
                    type: 'NumericTextBox',
                    className: 'NTB_1',
                    min: 100,
                    step: 1
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); })
                        .toThrow();
            });

            it('should not accept a max value that is not a number', () => {
                const model = new Widget({
                    id: 'NTB_1',
                    type: 'NumericTextBox',
                    className: 'NTB_1',
                    min: '1',
                    max: 'foobar',
                    step: 1
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); })
                        .toThrow();
            });
        });

        describe('respond()', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            Async.it('when constructed with no answer, answer doesn\'t exist', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let input;

                return widget.render()
                    .then(() => {
                        input = $($(`#${dummyParent.id}`).find('#NTB_1')[0]);
                        expect(widget.answer).toBeUndefined();
                    });
            });

            Async.it('respond() should set the answer model', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let input;

                return widget.render()
                    .then(() => {
                        input = $($(':input[type="number"]')[0]);
                        input.val('5');
                        const event = $.Event('keypress',
                            {
                                which: 5,
                                keycode: 5,
                                target: input,
                                currentTarget: input
                            }
                        );
                        input.trigger(event);
                        return widget.respond(event);
                    })
                    .then(() => {
                        expect(widget.answer).toBeDefined();
                        expect(widget.answer.get('response')).toEqual('5');
                    });
            });

            Async.it('clearing the textbox should invalidate the answer', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let input;

                return widget.render()
                    .then(() => {
                        input = $(widget.$(`#${dummyParent.id}`).find('#NTB_1')[0]);
                        input.val('8');

                        const event = $.Event('keypress',
                            {
                                which: 8,
                                keycode: 8,
                                target: input,
                                currentTarget: input
                            }
                        );
                        input.trigger(event);
                        return widget.respond(event);
                    })
                    .then(() => {
                        const e = $.Event('keypress',
                            {
                                target: input,
                                currentTarget: input
                            }
                        );
                        input.val('');
                        return widget.respond(e);
                    })
                    .then(() => {
                        expect(widget.answers.length).toBe(0);
                        expect(widget.answer).toBeUndefined();
                    });
            });

            Async.xit('valid number should validate', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let input;

                return widget.render()
                    .then(() => {
                        input = $(widget.$(`#${dummyParent.id}`).find('#NTB_1 input')[0]);
                        input.val('8');

                        expect(widget.isInputValid).toBe(true);
                    });
            });

        });
    });
});
