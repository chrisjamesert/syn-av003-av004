import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Templates from 'core/collections/Templates';
import Dashboard from 'core/models/Dashboard';
import User from 'core/models/User';
import Widget from 'core/models/Widget';
import QuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Answers from 'core/collections/Answers';
import SignatureBox from 'core/widgets/SignatureBox';
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import Data from 'core/Data';
import 'sitepad/resources/Templates';
import 'core/actions/notify';
import { MessageRepo } from 'core/Notify';
import setupCoreMessages from 'core/resources/Messages';
import WidgetBaseTests from './WidgetBase.specBase';

class SignatureBoxCoreTests extends WidgetBaseTests {

    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('.SignatureBox').length).toEqual(1);
            });
    }
}
// jscs:disable requireArrowFunctions, maximumLineLength
describe('SignatureBox', () => {
    let questionnaires = LF.StudyDesign.questionnaires,
        modelQuestion,
        modelDashboard,
        viewQuestionnaire,
        viewQuestion,
        modelWidget,
        signatureBoxWidget,
        template = `<div id="questionnaire-template">
            <div id="questionnaire" class="questionnaire"></div>
            </div>`,
        studyWorksSignature = '2..S@59.23@56.31@55.35@57.40@62.41@65.43@73.45@77.46@82.42@86.35@85.28@89.29@93.31S@74.61@72.64@71.70@72.74@75.78@78.80@82.84@87.87@89.90',
        nativeSignature = JSON.parse('[{"x":[59,56,55,57,62,65,73,77,82,86,85,89,93],"y":[23,31,35,40,41,43,45,46,42,35,28,29,31]},{"x":[74,72,71,72,75,78,82,87,89],"y":[61,64,70,74,78,80,84,87,90]}]'),
        studyWorksSignatureOversized = '3.500.250S@20.11@17.15@14.18@13.24@13.30@18.35@25.38@33.41@43.42@50.42@54.41@56.36@56.30@52.21@47.15@45.9@43.6@36.8@33.14@29.20@31.27@36.32@43.37@51.38@57.38@63.36@65.32@65.26@65.21@63.15@61.22@61.27@68.29@77.31@83.31@89.29@94.28@100.25@102.22@102.17@101.13@99.10@95.13@92.15@88.19@86.24@87.29@90.33@96.33@107.33@111.32@116.30@118.24@119.19@119.12@116.8@113.6@110.12@110.19@111.24@113.27@120.30@127.34@134.35@142.34@147.32@149.27@150.22@148.15@145.11@142.9@140.13@137.18@137.24@140.27@144.30@154.33@162.33@169.33@176.29@180.23@181.16@181.11@179.8@176.11@172.15@170.22@170.28@176.32@183.34@190.35@197.37@202.35@208.30@210.24@210.17@210.9@208.4@204.1@196.7@195.15@195.21@199.25@207.29@214.31@219.31@224.31@229.31@235.29@239.26@241.21@241.15@240.9@238.6@233.14@229.19@229.25@231.30@237.34@244.38@252.40@258.39@262.36@264.32@264.26@264.19@261.15@258.12@253.10@248.16@247.21@248.25@255.31@262.35@266.36@271.36@275.35@279.32@279.26@279.19@276.15@272.10@269.8@263.11@263.16@264.24@267.30@270.32@277.33@280.35@287.35@291.33@295.22@295.16@295.11@293.19@293.25@295.29@301.31@306.32@311.32@316.31@319.26@320.20@320.15@319.10@313.18@313.25@318.30@325.32@333.30@337.27@336.18@330.26@329.32@330.36@336.38@342.37@346.33@347.28@346.23@341.24@336.31@335.39@335.47@337.50@341.51@347.51@347.44@343.39@339.38@330.42@326.48@325.54@326.49@324.43@317.40@313.39@308.40@303.46@302.51@301.55@301.60@304.64@305.57@303.52@295.51@289.54@285.59@282.62@284.66@286.61@285.56@278.50@273.50@267.52@266.56@269.59@273.60@276.57@275.53@273.48@265.46@257.47@252.49@248.55@249.59@252.62@256.58@256.51@247.47@240.46@234.46@225.49@218.51@215.54@216.59@223.61@228.59@229.51@226.46@223.44@216.40@204.37@196.37@189.40@183.44@182.49@183.53@186.57@190.59@194.60@195.52@194.47@189.40@184.38@175.37@167.37@161.40@158.47@159.53@164.54@172.54@174.50@172.46@167.45@163.50@157.56@154.62@154.68@157.70@162.66@162.60@160.54@158.49@154.43@148.40@140.40@135.44@131.47@130.51@130.56@136.60@142.60@147.58@147.53@147.47@145.43@141.41@138.39@134.42@130.48@129.53@130.57@134.60@141.64@146.65@153.62@155.54@153.51@146.47@136.44@128.45@124.49@119.54@118.58@117.62@116.57@113.51@108.48@101.45@95.47@89.50@87.54@88.58@94.61@99.62@105.60@105.52@101.47@94.47@90.50@89.54@92.59@98.61@103.61@105.58@104.50@100.48@90.45@83.44@79.45@76.49@73.53@70.60@72.64@77.63@81.61@84.58@84.53@83.47@79.45@72.42@64.42@55.45@51.50@48.55@51.59@58.59@62.57@64.48@64.43@63.38@56.34@52.31@45.30@40.34@38.40@37.44@39.48@42.51@46.53@50.52@53.47@56.45@56.38@54.32@50.31@40.30@34.34@30.41@28.49@28.55@31.58@35.60@41.60@43.51@42.46@40.39@34.36@30.37@26.41@22.43@20.48@18.53S@25.77@21.75@16.76@14.81@12.90@14.96@21.97@24.95@26.92@28.88@27.82@26.75@23.80@23.86@23.91@27.96@32.98@37.99@45.98@50.95@55.93@57.87@58.82@56.77@53.74@49.80@49.87@58.90@64.90@71.90@78.86@82.84@84.80@85.75@81.68@75.74@74.79@72.84@75.88@83.91@92.92@100.93@104.92@106.89@107.85@106.80@104.77@99.80@96.87@95.93@103.97@112.97@121.95@123.90@123.82@123.76@120.71@116.69@110.71@108.76@108.82@111.88@115.93@122.95@127.95@132.94@137.91@140.86@141.81@141.73@138.67@135.63@131.70@129.79@128.88@130.93@135.96@145.97@156.98@163.97@169.95@170.89@169.83@165.78@162.76@158.75@152.82@152.89@152.94@154.98@157.100@165.101@170.97@172.94@171.86@168.82@162.83@162.93@165.99@170.102@176.105@182.106@187.106@193.105@195.101@197.93@197.87@197.81@195.76@189.74@186.79@184.88@184.95@187.101@196.102@208.103@216.103@222.99@224.94@225.85@223.75@222.71@218.75@218.80@217.86@220.89@224.91@229.94@234.94@239.93@243.89@245.82@246.75@245.69@239.65@235.64@233.67@231.73@231.82@233.90@240.93@249.95@256.95@261.95@265.94@266.86@266.80@265.73@261.69@254.74@254.81@254.88@257.93@261.96@269.98@274.98@279.97@280.93@280.87@277.83@275.79@269.81@268.88@269.97@275.102@282.107@288.108@293.102@295.94@294.88@292.82@288.80@285.82@284.89@287.93@290.96@295.99@302.100@308.100@311.98@314.94@314.88@314.82@310.78@307.80@308.85@315.85@320.85@326.85@330.83@332.78@333.71@333.64@333.58@335.64@338.66@346.66@349.63@351.58@349.51@346.49@342.48@339.51@337.56@336.64@338.68@341.70@339.77@338.83@337.88@342.91S@66.121@57.122@53.126@52.130@52.139@54.143@60.150@70.155@81.160@94.163@107.161@117.157@119.151@120.145@118.136@115.132@109.130@104.130@100.132@99.136@99.143@102.152@108.161@116.165@125.167@142.169@157.168@171.163@175.153@174.142@167.133@158.130@148.130@146.135@146.144@147.153@152.159@163.166@177.168@193.166@210.158@222.147@226.135@227.124@225.117@224.124@227.132@232.140@242.144@255.145@272.143@284.138@293.130@295.117@295.112@300.121@307.125@313.126@318.126',
        studyWorksSignatureOversized2 = '3.500.250S@20.11@17.15@14.18@13.24@13.30@18.35@25.38@33.41@43.42@50.42@54.41@56.36@56.30@52.21@47.15@45.9@43.6@36.8@33.14@29.20@31.27@36.32@43.37@51.38@57.38@63.36@65.32@65.26@65.21@63.15@61.22@61.27@68.29@77.31@83.31@89.29@94.28@100.25@102.22@102.17@101.13@99.10@95.13@92.15@88.19@86.24@87.29@90.33@96.33@107.33@111.32@116.30@118.24@119.19@119.12@116.8@113.6@110.12@110.19@111.24@113.27@120.30@127.34@134.35@142.34@147.32@149.27@150.22@148.15@145.11@142.9@140.13@137.18@137.24@140.27@144.30@154.33@162.33@169.33@176.29@180.23@181.16@181.11@179.8@176.11@172.15@170.22@170.28@176.32@183.34@190.35@197.37@202.35@208.30@210.24@210.17@210.9@208.4@204.1@196.7@195.15@195.21@199.25@207.29@214.31@219.31@224.31@229.31@235.29@239.26@241.21@241.15@240.9@238.6@233.14@229.19@229.25@231.30@237.34@244.38@252.40@258.39@262.36@264.32@264.26@264.19@261.15@258.12@253.10@248.16@247.21@248.25@255.31@262.35@266.36@271.36@275.35@279.32@279.26@279.19@276.15@272.10@269.8@263.11@263.16@264.24@267.30@270.32@277.33@280.35@287.35@291.33@295.22@295.16@295.11@293.19@293.25@295.29@301.31@306.32@311.32@316.31@319.26@320.20@320.15@319.10@313.18@313.25@318.30@325.32@333.30@337.27@336.18@330.26@329.32@330.36@336.38@342.37@346.33@347.28@346.23@341.24@336.31@335.39@335.47@337.50@341.51@347.51@347.44@343.39@339.38@330.42@326.48@325.54@326.49@324.43@317.40@313.39@308.40@303.46@302.51@301.55@301.60@304.64@305.57@303.52@295.51@289.54@285.59@282.62@284.66@286.61@285.56@278.50@273.50@267.52@266.56@269.59@273.60@276.57@275.53@273.48@265.46@257.47@252.49@248.55@249.59@252.62@256.58@256.51@247.47@240.46@234.46@225.49@218.51@215.54@216.59@223.61@228.59@229.51@226.46@223.44@216.40@204.37@196.37@189.40@183.44@182.49@183.53@186.57@190.59@194.60@195.52@194.47@189.40@184.38@175.37@167.37@161.40@158.47@159.53@164.54@172.54@174.50@172.46@167.45@163.50@157.56@154.62@154.68@157.70@162.66@162.60@160.54@158.49@154.43@148.40@140.40@135.44@131.47@130.51@130.56@136.60@142.60@147.58@147.53@147.47@145.43@141.41@138.39@134.42@130.48@129.53@130.57@134.60@141.64@146.65@153.62@155.54@153.51@146.47@136.44@128.45@124.49@119.54@118.58@117.62@116.57@113.51@108.48@101.45@95.47@89.50@87.54@88.58@94.61@99.62@105.60@105.52@101.47@94.47@90.50@89.54@92.59@98.61@103.61@105.58@104.50@100.48@90.45@83.44@79.45@76.49@73.53@70.60@72.64@77.63@81.61@84.58@84.53@83.47@79.45@72.42@64.42@55.45@51.50@48.55@51.59@58.59@62.57@64.48@64.43@63.38@56.34@52.31@45.30@40.34@38.40@37.44@39.48@42.51@46.53@50.52@53.47@56.45@56.38@54.32@50.31@40.30@34.34@30.41@28.49@28.55@31.58@35.60@41.60@43.51@42.46@40.39@34.36@30.37@26.41@22.43@20.48@18.53S@25.77@21.75@16.76@14.81@12.90@14.96@21.97@24.95@26.92@28.88@27.82@26.75@23.80@23.86@23.91@27.96@32.98@37.99@45.98@50.95@55.93@57.87@58.82@56.77@53.74@49.80@49.87@58.90@64.90@71.90@78.86@82.84@84.80@85.75@81.68@75.74@74.79@72.84@75.88@83.91@92.92@100.93@104.92@106.89@107.85@106.80@104.77@99.80@96.87@95.93@103.97@112.97@121.95@123.90@123.82@123.76@120.71@116.69@110.71@108.76@108.82@111.88@115.93@122.95@127.95@132.94@137.91@140.86@141.81@141.73@138.67@135.63@131.70@129.79@128.88@130.93@135.96@145.97@156.98@163.97@169.95@170.89@169.83@165.78@162.76@158.75@152.82@152.89@152.94@154.98@157.100@165.101@170.97@172.94@171.86@168.82@162.83@162.93@165.99@170.102@176.105@182.106@187.106@193.105@195.101@197.93@197.87@197.81@195.76@189.74@186.79@184.88@184.95@187.101@196.102@208.103@216.103@222.99@224.94@225.85@223.75@222.71@218.75@218.80@217.86@220.89@224.91@229.94@234.94@239.93@243.89@245.82@246.75@245.69@239.65@235.64@233.67@231.73@231.82@233.90@240.93@249.95@256.95@261.95@265.94@266.86@266.80@265.73@261.69@254.74@254.81@254.88@257.93@261.96@269.98@274.98@279.97@280.93@280.87@277.83@275.79@269.81@268.88@269.97@275.102@282.107@288.108@293.102@295.94@294.88@292.82@288.80@285.82@284.89@287.93@290.96@295.99@302.100@308.100@311.98@314.94@314.88@314.82@310.78@307.80@308.85@315.85@320.85@326.85@330.83@332.78@333.71@333.64@333.58@335.64@338.66@346.66@349.63@351.58@349.51@346.49@342.48@339.51@337.56@336.64@338.68@341.70@339.77@338.83@337.88@342.91@66.121@57.122@53.126@52.130@52.139S@54.143@60.150@70.155@81.160@94.163@107.161@117.157@119.151@120.145@118.136@115.132@109.130@104.130@100.132@99.136@99.143@102.152@108.161@116.165@125.167@142.169@157.168@171.163@175.153@174.142@167.133@158.130@148.130@146.135@146.144@147.153@152.159@163.166@177.168@193.166@210.158@222.147@226.135@227.124@225.117@224.124@227.132@232.140@242.144@255.145@272.143@284.138@293.130@295.117@295.112@300.121@307.125@313.126@318.126',
        studyWorksSignatureTrimmed = '2.500.250S@20.11@17.15@14.18@13.24@13.30@18.35@25.38@33.41@43.42@50.42@54.41@56.36@56.30@52.21@47.15@45.9@43.6@36.8@33.14@29.20@31.27@36.32@43.37@51.38@57.38@63.36@65.32@65.26@65.21@63.15@61.22@61.27@68.29@77.31@83.31@89.29@94.28@100.25@102.22@102.17@101.13@99.10@95.13@92.15@88.19@86.24@87.29@90.33@96.33@107.33@111.32@116.30@118.24@119.19@119.12@116.8@113.6@110.12@110.19@111.24@113.27@120.30@127.34@134.35@142.34@147.32@149.27@150.22@148.15@145.11@142.9@140.13@137.18@137.24@140.27@144.30@154.33@162.33@169.33@176.29@180.23@181.16@181.11@179.8@176.11@172.15@170.22@170.28@176.32@183.34@190.35@197.37@202.35@208.30@210.24@210.17@210.9@208.4@204.1@196.7@195.15@195.21@199.25@207.29@214.31@219.31@224.31@229.31@235.29@239.26@241.21@241.15@240.9@238.6@233.14@229.19@229.25@231.30@237.34@244.38@252.40@258.39@262.36@264.32@264.26@264.19@261.15@258.12@253.10@248.16@247.21@248.25@255.31@262.35@266.36@271.36@275.35@279.32@279.26@279.19@276.15@272.10@269.8@263.11@263.16@264.24@267.30@270.32@277.33@280.35@287.35@291.33@295.22@295.16@295.11@293.19@293.25@295.29@301.31@306.32@311.32@316.31@319.26@320.20@320.15@319.10@313.18@313.25@318.30@325.32@333.30@337.27@336.18@330.26@329.32@330.36@336.38@342.37@346.33@347.28@346.23@341.24@336.31@335.39@335.47@337.50@341.51@347.51@347.44@343.39@339.38@330.42@326.48@325.54@326.49@324.43@317.40@313.39@308.40@303.46@302.51@301.55@301.60@304.64@305.57@303.52@295.51@289.54@285.59@282.62@284.66@286.61@285.56@278.50@273.50@267.52@266.56@269.59@273.60@276.57@275.53@273.48@265.46@257.47@252.49@248.55@249.59@252.62@256.58@256.51@247.47@240.46@234.46@225.49@218.51@215.54@216.59@223.61@228.59@229.51@226.46@223.44@216.40@204.37@196.37@189.40@183.44@182.49@183.53@186.57@190.59@194.60@195.52@194.47@189.40@184.38@175.37@167.37@161.40@158.47@159.53@164.54@172.54@174.50@172.46@167.45@163.50@157.56@154.62@154.68@157.70@162.66@162.60@160.54@158.49@154.43@148.40@140.40@135.44@131.47@130.51@130.56@136.60@142.60@147.58@147.53@147.47@145.43@141.41@138.39@134.42@130.48@129.53@130.57@134.60@141.64@146.65@153.62@155.54@153.51@146.47@136.44@128.45@124.49@119.54@118.58@117.62@116.57@113.51@108.48@101.45@95.47@89.50@87.54@88.58@94.61@99.62@105.60@105.52@101.47@94.47@90.50@89.54@92.59@98.61@103.61@105.58@104.50@100.48@90.45@83.44@79.45@76.49@73.53@70.60@72.64@77.63@81.61@84.58@84.53@83.47@79.45@72.42@64.42@55.45@51.50@48.55@51.59@58.59@62.57@64.48@64.43@63.38@56.34@52.31@45.30@40.34@38.40@37.44@39.48@42.51@46.53@50.52@53.47@56.45@56.38@54.32@50.31@40.30@34.34@30.41@28.49@28.55@31.58@35.60@41.60@43.51@42.46@40.39@34.36@30.37@26.41@22.43@20.48@18.53S@25.77@21.75@16.76@14.81@12.90@14.96@21.97@24.95@26.92@28.88@27.82@26.75@23.80@23.86@23.91@27.96@32.98@37.99@45.98@50.95@55.93@57.87@58.82@56.77@53.74@49.80@49.87@58.90@64.90@71.90@78.86@82.84@84.80@85.75@81.68@75.74@74.79@72.84@75.88@83.91@92.92@100.93@104.92@106.89@107.85@106.80@104.77@99.80@96.87@95.93@103.97@112.97@121.95@123.90@123.82@123.76@120.71@116.69@110.71@108.76@108.82@111.88@115.93@122.95@127.95@132.94@137.91@140.86@141.81@141.73@138.67@135.63@131.70@129.79@128.88@130.93@135.96@145.97@156.98@163.97@169.95@170.89@169.83@165.78@162.76@158.75@152.82@152.89@152.94@154.98@157.100@165.101@170.97@172.94@171.86@168.82@162.83@162.93@165.99@170.102@176.105@182.106@187.106@193.105@195.101@197.93@197.87@197.81@195.76@189.74@186.79@184.88@184.95@187.101@196.102@208.103@216.103@222.99@224.94@225.85@223.75@222.71@218.75@218.80@217.86@220.89@224.91@229.94@234.94@239.93@243.89@245.82@246.75@245.69@239.65@235.64@233.67@231.73@231.82@233.90@240.93@249.95@256.95@261.95@265.94@266.86@266.80@265.73@261.69@254.74@254.81@254.88@257.93@261.96@269.98@274.98@279.97@280.93@280.87@277.83@275.79@269.81@268.88@269.97@275.102@282.107@288.108@293.102@295.94@294.88@292.82@288.80@285.82@284.89@287.93@290.96@295.99@302.100@308.100@311.98@314.94@314.88@314.82@310.78@307.80@308.85@315.85@320.85@326.85@330.83@332.78@333.71@333.64@333.58@335.64@338.66@346.66@349.63@351.58@349.51@346.49@342.48@339.51@337.56@336.64@338.68@341.70@339.77@338.83@337.88@342.91',
        nativeSignatureOversized = JSON.parse('[{"x":[20,17,14,13,13,18,25,33,43,50,54,56,56,52,47,45,43,36,33,29,31,36,43,51,57,63,65,65,65,63,61,61,68,77,83,89,94,100,102,102,101,99,95,92,88,86,87,90,96,107,111,116,118,119,119,116,113,110,110,111,113,120,127,134,142,147,149,150,148,145,142,140,137,137,140,144,154,162,169,176,180,181,181,179,176,172,170,170,176,183,190,197,202,208,210,210,210,208,204,196,195,195,199,207,214,219,224,229,235,239,241,241,240,238,233,229,229,231,237,244,252,258,262,264,264,264,261,258,253,248,247,248,255,262,266,271,275,279,279,279,276,272,269,263,263,264,267,270,277,280,287,291,295,295,295,293,293,295,301,306,311,316,319,320,320,319,313,313,318,325,333,337,336,330,329,330,336,342,346,347,346,341,336,335,335,337,341,347,347,343,339,330,326,325,326,324,317,313,308,303,302,301,301,304,305,303,295,289,285,282,284,286,285,278,273,267,266,269,273,276,275,273,265,257,252,248,249,252,256,256,247,240,234,225,218,215,216,223,228,229,226,223,216,204,196,189,183,182,183,186,190,194,195,194,189,184,175,167,161,158,159,164,172,174,172,167,163,157,154,154,157,162,162,160,158,154,148,140,135,131,130,130,136,142,147,147,147,145,141,138,134,130,129,130,134,141,146,153,155,153,146,136,128,124,119,118,117,116,113,108,101,95,89,87,88,94,99,105,105,101,94,90,89,92,98,103,105,104,100,90,83,79,76,73,70,72,77,81,84,84,83,79,72,64,55,51,48,51,58,62,64,64,63,56,52,45,40,38,37,39,42,46,50,53,56,56,54,50,40,34,30,28,28,31,35,41,43,42,40,34,30,26,22,20,18],"y":[11,15,18,24,30,35,38,41,42,42,41,36,30,21,15,9,6,8,14,20,27,32,37,38,38,36,32,26,21,15,22,27,29,31,31,29,28,25,22,17,13,10,13,15,19,24,29,33,33,33,32,30,24,19,12,8,6,12,19,24,27,30,34,35,34,32,27,22,15,11,9,13,18,24,27,30,33,33,33,29,23,16,11,8,11,15,22,28,32,34,35,37,35,30,24,17,9,4,1,7,15,21,25,29,31,31,31,31,29,26,21,15,9,6,14,19,25,30,34,38,40,39,36,32,26,19,15,12,10,16,21,25,31,35,36,36,35,32,26,19,15,10,8,11,16,24,30,32,33,35,35,33,22,16,11,19,25,29,31,32,32,31,26,20,15,10,18,25,30,32,30,27,18,26,32,36,38,37,33,28,23,24,31,39,47,50,51,51,44,39,38,42,48,54,49,43,40,39,40,46,51,55,60,64,57,52,51,54,59,62,66,61,56,50,50,52,56,59,60,57,53,48,46,47,49,55,59,62,58,51,47,46,46,49,51,54,59,61,59,51,46,44,40,37,37,40,44,49,53,57,59,60,52,47,40,38,37,37,40,47,53,54,54,50,46,45,50,56,62,68,70,66,60,54,49,43,40,40,44,47,51,56,60,60,58,53,47,43,41,39,42,48,53,57,60,64,65,62,54,51,47,44,45,49,54,58,62,57,51,48,45,47,50,54,58,61,62,60,52,47,47,50,54,59,61,61,58,50,48,45,44,45,49,53,60,64,63,61,58,53,47,45,42,42,45,50,55,59,59,57,48,43,38,34,31,30,34,40,44,48,51,53,52,47,45,38,32,31,30,34,41,49,55,58,60,60,51,46,39,36,37,41,43,48,53]},{"x":[25,21,16,14,12,14,21,24,26,28,27,26,23,23,23,27,32,37,45,50,55,57,58,56,53,49,49,58,64,71,78,82,84,85,81,75,74,72,75,83,92,100,104,106,107,106,104,99,96,95,103,112,121,123,123,123,120,116,110,108,108,111,115,122,127,132,137,140,141,141,138,135,131,129,128,130,135,145,156,163,169,170,169,165,162,158,152,152,152,154,157,165,170,172,171,168,162,162,165,170,176,182,187,193,195,197,197,197,195,189,186,184,184,187,196,208,216,222,224,225,223,222,218,218,217,220,224,229,234,239,243,245,246,245,239,235,233,231,231,233,240,249,256,261,265,266,266,265,261,254,254,254,257,261,269,274,279,280,280,277,275,269,268,269,275,282,288,293,295,294,292,288,285,284,287,290,295,302,308,311,314,314,314,310,307,308,315,320,326,330,332,333,333,333,335,338,346,349,351,349,346,342,339,337,336,338,341,339,338,337,342],"y":[77,75,76,81,90,96,97,95,92,88,82,75,80,86,91,96,98,99,98,95,93,87,82,77,74,80,87,90,90,90,86,84,80,75,68,74,79,84,88,91,92,93,92,89,85,80,77,80,87,93,97,97,95,90,82,76,71,69,71,76,82,88,93,95,95,94,91,86,81,73,67,63,70,79,88,93,96,97,98,97,95,89,83,78,76,75,82,89,94,98,100,101,97,94,86,82,83,93,99,102,105,106,106,105,101,93,87,81,76,74,79,88,95,101,102,103,103,99,94,85,75,71,75,80,86,89,91,94,94,93,89,82,75,69,65,64,67,73,82,90,93,95,95,95,94,86,80,73,69,74,81,88,93,96,98,98,97,93,87,83,79,81,88,97,102,107,108,102,94,88,82,80,82,89,93,96,99,100,100,98,94,88,82,78,80,85,85,85,85,83,78,71,64,58,64,66,66,63,58,51,49,48,51,56,64,68,70,77,83,88,91]},{"x":[66,57,53,52,52,54,60,70,81,94,107,117,119,120,118,115,109,104,100,99,99,102,108,116,125,142,157,171,175,174,167,158,148,146,146,147,152,163,177,193,210,222,226,227,225,224,227,232,242,255,272,284,293,295,295,300,307,313,318],"y":[121,122,126,130,139,143,150,155,160,163,161,157,151,145,136,132,130,130,132,136,143,152,161,165,167,169,168,163,153,142,133,130,130,135,144,153,159,166,168,166,158,147,135,124,117,124,132,140,144,145,143,138,130,117,112,121,125,126,126]}]'),
        nativeSignatureTrimmed = JSON.parse('[{"x":[20,17,14,13,13,18,25,33,43,50,54,56,56,52,47,45,43,36,33,29,31,36,43,51,57,63,65,65,65,63,61,61,68,77,83,89,94,100,102,102,101,99,95,92,88,86,87,90,96,107,111,116,118,119,119,116,113,110,110,111,113,120,127,134,142,147,149,150,148,145,142,140,137,137,140,144,154,162,169,176,180,181,181,179,176,172,170,170,176,183,190,197,202,208,210,210,210,208,204,196,195,195,199,207,214,219,224,229,235,239,241,241,240,238,233,229,229,231,237,244,252,258,262,264,264,264,261,258,253,248,247,248,255,262,266,271,275,279,279,279,276,272,269,263,263,264,267,270,277,280,287,291,295,295,295,293,293,295,301,306,311,316,319,320,320,319,313,313,318,325,333,337,336,330,329,330,336,342,346,347,346,341,336,335,335,337,341,347,347,343,339,330,326,325,326,324,317,313,308,303,302,301,301,304,305,303,295,289,285,282,284,286,285,278,273,267,266,269,273,276,275,273,265,257,252,248,249,252,256,256,247,240,234,225,218,215,216,223,228,229,226,223,216,204,196,189,183,182,183,186,190,194,195,194,189,184,175,167,161,158,159,164,172,174,172,167,163,157,154,154,157,162,162,160,158,154,148,140,135,131,130,130,136,142,147,147,147,145,141,138,134,130,129,130,134,141,146,153,155,153,146,136,128,124,119,118,117,116,113,108,101,95,89,87,88,94,99,105,105,101,94,90,89,92,98,103,105,104,100,90,83,79,76,73,70,72,77,81,84,84,83,79,72,64,55,51,48,51,58,62,64,64,63,56,52,45,40,38,37,39,42,46,50,53,56,56,54,50,40,34,30,28,28,31,35,41,43,42,40,34,30,26,22,20,18],"y":[11,15,18,24,30,35,38,41,42,42,41,36,30,21,15,9,6,8,14,20,27,32,37,38,38,36,32,26,21,15,22,27,29,31,31,29,28,25,22,17,13,10,13,15,19,24,29,33,33,33,32,30,24,19,12,8,6,12,19,24,27,30,34,35,34,32,27,22,15,11,9,13,18,24,27,30,33,33,33,29,23,16,11,8,11,15,22,28,32,34,35,37,35,30,24,17,9,4,1,7,15,21,25,29,31,31,31,31,29,26,21,15,9,6,14,19,25,30,34,38,40,39,36,32,26,19,15,12,10,16,21,25,31,35,36,36,35,32,26,19,15,10,8,11,16,24,30,32,33,35,35,33,22,16,11,19,25,29,31,32,32,31,26,20,15,10,18,25,30,32,30,27,18,26,32,36,38,37,33,28,23,24,31,39,47,50,51,51,44,39,38,42,48,54,49,43,40,39,40,46,51,55,60,64,57,52,51,54,59,62,66,61,56,50,50,52,56,59,60,57,53,48,46,47,49,55,59,62,58,51,47,46,46,49,51,54,59,61,59,51,46,44,40,37,37,40,44,49,53,57,59,60,52,47,40,38,37,37,40,47,53,54,54,50,46,45,50,56,62,68,70,66,60,54,49,43,40,40,44,47,51,56,60,60,58,53,47,43,41,39,42,48,53,57,60,64,65,62,54,51,47,44,45,49,54,58,62,57,51,48,45,47,50,54,58,61,62,60,52,47,47,50,54,59,61,61,58,50,48,45,44,45,49,53,60,64,63,61,58,53,47,45,42,42,45,50,55,59,59,57,48,43,38,34,31,30,34,40,44,48,51,53,52,47,45,38,32,31,30,34,41,49,55,58,60,60,51,46,39,36,37,41,43,48,53]},{"x":[25,21,16,14,12,14,21,24,26,28,27,26,23,23,23,27,32,37,45,50,55,57,58,56,53,49,49,58,64,71,78,82,84,85,81,75,74,72,75,83,92,100,104,106,107,106,104,99,96,95,103,112,121,123,123,123,120,116,110,108,108,111,115,122,127,132,137,140,141,141,138,135,131,129,128,130,135,145,156,163,169,170,169,165,162,158,152,152,152,154,157,165,170,172,171,168,162,162,165,170,176,182,187,193,195,197,197,197,195,189,186,184,184,187,196,208,216,222,224,225,223,222,218,218,217,220,224,229,234,239,243,245,246,245,239,235,233,231,231,233,240,249,256,261,265,266,266,265,261,254,254,254,257,261,269,274,279,280,280,277,275,269,268,269,275,282,288,293,295,294,292,288,285,284,287,290,295,302,308,311,314,314,314,310,307,308,315,320,326,330,332,333,333,333,335,338,346,349,351,349,346,342,339,337,336,338,341,339,338,337,342],"y":[77,75,76,81,90,96,97,95,92,88,82,75,80,86,91,96,98,99,98,95,93,87,82,77,74,80,87,90,90,90,86,84,80,75,68,74,79,84,88,91,92,93,92,89,85,80,77,80,87,93,97,97,95,90,82,76,71,69,71,76,82,88,93,95,95,94,91,86,81,73,67,63,70,79,88,93,96,97,98,97,95,89,83,78,76,75,82,89,94,98,100,101,97,94,86,82,83,93,99,102,105,106,106,105,101,93,87,81,76,74,79,88,95,101,102,103,103,99,94,85,75,71,75,80,86,89,91,94,94,93,89,82,75,69,65,64,67,73,82,90,93,95,95,95,94,86,80,73,69,74,81,88,93,96,98,98,97,93,87,83,79,81,88,97,102,107,108,102,94,88,82,80,82,89,93,96,99,100,100,98,94,88,82,78,80,85,85,85,85,83,78,71,64,58,64,66,66,63,58,51,49,48,51,56,64,68,70,77,83,88,91]}]'),
        siteUser;

    beforeAll((done) => {
        setupCoreMessages();
        LF.appName = 'SitePad App';
        helpers.uninstallDatabase().finally(done);
    });
    beforeEach((done) => {

        //execTests AfterAll clears messageRepo
        setupCoreMessages();

        LF.security = {
            activeUser: new User({
                username: 'tkelly',
                password: '1234',
                salt: 'tkelly',
                secretQuestion: 'tkelly',
                secretAnswer: 'tkelly',
                language: 'en-US',
                role: 'siteUser',
                failedLoginAttempts: 5,
                lastLogin: '1234'
            })
        };

        Data.Questionnaire = {};
        Data.Questionnaire.SiteUserName = 'apple';

        LF.StudyDesign.questionnaires = new Questionnaires([
            {
                id              : 'SIGNATURE_DIARY',
                SU              : 'SIGNATURE_DIARY',
                displayName     : 'DISPLAY_NAME',
                affidavit       : 'SignatureAffidavit',
                className       : 'SIGNATURE_DIARY',
                previousScreen  : true,
                screens         : ['SIGNATURE_DIARY_S_1']
            }
        ]);
        modelQuestion = new Question({
            id      : 'AFFIDAVIT',
            text    : 'SITE_REPORT_AFF',
            krSig   : 'CustomAffidavit',
            widget  : {
                id        : 'SIGNATURE_AFFIDAVIT_WIDGET',
                className : 'AFFIDAVIT',
                type      : 'SignatureBox'
            }
        });
        modelDashboard = new Dashboard({
            instance_ordinal: 1
        });
        viewQuestionnaire = new QuestionnaireView({
            id: 'SIGNATURE_DIARY'
        });
        viewQuestion = new QuestionView({
            id          : modelQuestion.get('id'),
            screen      : 'SIGNATURE_DIARY',
            model       : modelQuestion,
            parent      : viewQuestionnaire,
            mandatory   : false
        });
        modelWidget = new Widget({
            id          : 'SIGNATUREBOX_WIDGET',
            className   : 'AFFIDAVIT',
            type        : 'SignatureBox',
            templates   : {}
        });

        viewQuestionnaire.data.dashboard = modelDashboard;
        viewQuestionnaire.data.answers = new Answers();

        LF.router = { navigate: $.noop };

        spyOn(LF.router, 'navigate');

        helpers.createSubject();
        helpers.saveSubject().then(() => {
            Data.Questionnaire = viewQuestionnaire.data || {};

            signatureBoxWidget = new SignatureBox({
                model       : modelWidget,
                answers     : new Answers(),
                parent      : viewQuestion,
                mandatory   : true
            });

            $('body').append(template);
        })
        .catch(e => fail(e))
        .done(done);
    });

    afterEach(() => {
        MessageRepo.clear();
        Data = {};
        LF.router = undefined;
        LF.schedule = undefined;
        LF.security = undefined;
        LF.StudyDesign.questionnaires = questionnaires;
        $('#questionnaire-template').remove();
    });

    const model = new Widget({
        type: 'SignatureBox',
        id: 'testWidgetId'
    });
    const baseTests = new SignatureBoxCoreTests(model);

    baseTests.execTests();

    it('should render the widget correctly', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            expect(signatureBoxWidget.$('jSignature')).toBeDefined();
            expect(signatureBoxWidget.$('button#signature-clear')).toBeDefined();
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should show the label for site user signature', function () {
        expect($('#site-user-name')).toBeDefined();
    });

    it('should not show the label for site user signature if there is no site user selected', function () {
        delete Data.Questionnaire.SiteUserName;
        viewQuestion.render('questionnaire');

        expect(viewQuestion.$('#site-user-name').length).toEqual(0);
    });

    it('should clear the signature data', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render()
        .then(() => {
            signatureBoxWidget.$('.signature-box').jSignature('setData', nativeSignature, 'native');
            signatureBoxWidget.respond();
            signatureBoxWidget.$('button#signature-clear').trigger('click');
        })
        //.delay(1000)
        .then(() => {
            expect(signatureBoxWidget.ink).toEqual({});
            expect(signatureBoxWidget.nativeSignatureData).toEqual([]);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should load the previously signed signature after the widget is shown again (like when you click back in the question and click next again)', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.$('.signature-box').jSignature('setData', nativeSignature, 'native');
            signatureBoxWidget.respond();
            signatureBoxWidget.$el.html('');
        }).then(() => {
            signatureBoxWidget.render();
        }).then(() => {
            expect(signatureBoxWidget.answer).toBeTruthy();
        }).catch(e => {
            fail(e);
        }).done(done);
    });

    it('should not be validated if signature is not signed when widget is mandatory', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            expect(signatureBoxWidget.validate()).toBe(false);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should be validated if signature is signed when widget is mandatory', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render()
        .then(() => {
            signatureBoxWidget.$('.signature-box').jSignature('importData', nativeSignature, 'native');
            signatureBoxWidget.respond();
        })
        .then(() => {
            expect(signatureBoxWidget.validate()).toBe(true);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should not be validated if signature is signed but then cleared when widget is mandatory', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.$('.signature-box').jSignature('importData', nativeSignature, 'native');
            signatureBoxWidget.respond();
            signatureBoxWidget.$('button#signature-clear').trigger('click');
        })
        //.delay(300)
        .then(() => {
            expect(signatureBoxWidget.validate()).toBe(false);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should return false for the \'isAffidavitAnswered()\' function if signature is not signed', function () {
        expect(signatureBoxWidget.isAffidavitAnswered()).toBe(false);
    });

    it('should return true for the \'isAffidavitAnswered()\' function if signature is signed', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.setSignaturePluginData(nativeSignature);
            signatureBoxWidget.respond();

            expect(signatureBoxWidget.isAffidavitAnswered()).toBe(true);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should return false for the \'isAffidavitAnswered()\' function if signature is signed but then cleared', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.$('.signature-box').jSignature('setData', nativeSignature, 'native');
            signatureBoxWidget.respond();
            signatureBoxWidget.$('button#signature-clear').trigger('click');
        })
        //.delay(300)
        .then(() => {
            expect(signatureBoxWidget.isAffidavitAnswered()).toBe(false);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should display signature too long notification if data size exceed the limit', (done) => {
        spyOn(MessageRepo, 'display').and.resolve(true);

        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.$('.signature-box').jSignature('setData', nativeSignatureOversized, 'native');
            signatureBoxWidget.respond();

            expect(MessageRepo.display).toHaveBeenCalledWith(MessageRepo.Dialog.SIGNATURE_DATA_LIMIT_EXCEEDED);
        })
        .catch(e => fail(e))
        .done(done);
    });

    // Signature comparison issues
    it('should trim over sized signature correctly', function (done) {
        let ink = {
            xSize: 500,
            ySize: 250,
            author: 'apple'
        };

        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.$('canvas.jSignature').width(500).height(250);
            ink.signatureData = studyWorksSignatureOversized;
            signatureBoxWidget.ink = ink;
            signatureBoxWidget.trimSignature(460);

            expect(JSON.stringify(signatureBoxWidget.nativeSignatureData))
            .toEqual(JSON.stringify(nativeSignatureTrimmed));
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should clear ink data', function (done) {
        let ink = {
            xSize: 500,
            ySize: 250,
            signatureData: studyWorksSignature,
            author: 'apple'
        };

        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.ink = ink;
            signatureBoxWidget.clearInkData();

            expect(signatureBoxWidget.ink).toEqual({});
            expect(viewQuestionnaire.data.dashboard.get('ink')).toEqual(undefined);
        })
        .catch(e => fail(e))
        .done(done);
    });

    it('should set author correctly', function () {
        signatureBoxWidget.setAuthor('Red Apple');
        expect(signatureBoxWidget.author).toEqual('Red Apple');
    });

    it('should set signature plugin data correctly', function (done) {
        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.setSignaturePluginData(nativeSignature);

            expect(JSON.stringify(signatureBoxWidget.getSignaturePluginData()))
            .toEqual(JSON.stringify(nativeSignature));
        })
        .catch(e => fail(e))
        .done(done);
    });

    // TODO - Failing due to underscore upgrade.
    xit('should validate data size correctly', function (done) {
        let validationResult,
            ink = {
                xSize: 500,
                ySize: 250,
                signatureData: studyWorksSignature,
                author: 'apple'
            },
            inkOversized = {
                xSize: 500,
                ySize: 250,
                signatureData: studyWorksSignatureOversized,
                author: 'apple'
            };

        $('#questionnaire').append(signatureBoxWidget.parent.el);

        signatureBoxWidget.render().then(() => {
            signatureBoxWidget.ink = ink;
            validationResult = signatureBoxWidget.validateDataSize();
            expect(validationResult).toEqual(true);

            signatureBoxWidget.ink = inkOversized;
            validationResult = signatureBoxWidget.validateDataSize();
            expect(validationResult).toEqual(false);
        })
        .catch(e => fail(e))
        .done(done);
    });
});
// jscs:enable requireArrowFunctions, maximumLineLength
