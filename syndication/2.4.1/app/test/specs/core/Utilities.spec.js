import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Questions from 'core/collections/Questions';
import Questionnaire from 'core/models/Questionnaire';
import * as Utilities from 'core/utilities';
import Widget from 'core/models/Widget';
import Answer from 'core/models/Answer';
import Answers from 'core/collections/Answers';
import 'core/wrapperjs/Wrapper';
import COOL from 'core/COOL';

describe('Utilities', () => {
    let views = [],
        widget,
        addAnswers;

    afterEach(() => {
        views = [];
        widget = undefined;
    });

    it('should correctly verify the type of all items', () => {
        expect(Utilities.toType('Hello World!')).toEqual('string');
        expect(Utilities.toType(12345)).toEqual('number');
        expect(Utilities.toType({ hello: 'world' })).toEqual('object');

        // eslint-disable-next-line no-empty-function
        expect(Utilities.toType(() => {
        })).toEqual('function');
        expect(Utilities.toType([])).toEqual('array');
    });

    it('should adjust the page title.', () => {
        let title = document.title;

        Utilities.title('Hello World!');
        expect(document.title).toEqual('Hello World!');

        Utilities.title(title);
    });

    it('should adjust the language and direction of the text.', () => {
        Utilities.setLanguage('en-US', 'ltr');
        expect($('html').attr('lang')).toEqual('en-US');
        expect($('html').attr('dir')).toEqual('ltr');

        Utilities.setLanguage();
    });

    it('should confirm the connection status.', (done) => {
        let res = !!(navigator.onLine),
            response,
            connectionStatus;

        COOL.getClass('Utilities').isOnline((isOnline) => {
            connectionStatus = isOnline;
            expect(connectionStatus).toEqual(res);
            done();
        });
    });

    it('should encode the JSON.', () => {
        let json = {
            hello: "'world'"
        };

        expect(Utilities.encodeJSON(json)).toEqual('%7B%22hello%22:%22%27world%27%22%7D');
    });

    it('should decode the JSON', () => {
        expect(Utilities.decodeJSON('%7B%22hello%22:%22%27world%27%22%7D')).toEqual({
            hello: "'world'"
        });
    });

    it('should return the correct timezone offset.', () => {
        expect(new Date().getOffset()).toEqual(new Date().getTimezoneOffset() / 60 * -1);
    });

    // Explicitly setting timezone on date to avoid timezone offset issues

    it('should return the correct timestamp for \'May\' with negative offset.', () => {
        let date = new Date('Mon May 11 2015 00:00:00 GMT-0500');

        expect(date.ISOStamp()).toEqual('2015-05-11T05:00:00Z');
    });

    it('should return the correct timestamp for \'November\' with negative offset.', () => {
        let date = new Date('Wed Nov 26 2008 00:00:00 GMT-0500');

        expect(date.ISOStamp()).toEqual('2008-11-26T05:00:00Z');
    });

    // Expecting the ISOStamp to be the previous date because of positive time offset

    it('should return the correct timestamp for \'May\' with positive offset.', () => {
        let date = new Date('Mon May 11 2015 00:00:00 GMT+0530');

        expect(date.ISOStamp()).toEqual('2015-05-10T18:30:00Z');
    });

    it('should return the correct timestamp for \'November\' with positive offset.', () => {
        let date = new Date('Wed Nov 26 2008 00:00:00 GMT+0530');

        expect(date.ISOStamp()).toEqual('2008-11-25T18:30:00Z');
    });

    it('should return a formatted ISOStamp.', () => {
        let expression = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$'),
            date = new Date();

        expect(expression.test(date.ISOStamp())).toBeTruthy();
    });

    it('should return the correct timestamp.', () => {
        let date = new Date('Wed Nov 26 2008 20:00:00');

        expect(date.ISOLocalTZStamp()).toEqual('2008-11-26T20:00:00');
    });

    it('should return a formatted ISOStamp.', () => {
        let expression = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$'),
            date = new Date();

        expect(expression.test(date.ISOLocalTZStamp())).toBeTruthy();
    });

    // TODO: Move to DateTimeUtil
    xit('should return a formatted timeStamp.', () => {
        let expression = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[+-][0-9]{2}:[0-9]{2}$'),
            date = new Date();

        expect(expression.test(Utilities.timeStamp(date))).toBeTruthy();
    });

    // TODO: Move to DateTimeUtil
    xit('should return the correct date format for 2016-01-01', () => {
        let date = Utilities.convertToDate(new Date(2016, 0, 1));

        expect(date).toEqual('2016-01-01');
    });

    // TODO: Move to DateTimeUtil
    xit('should return the correct date format for 1998-12-12', () => {
        let date = Utilities.convertToDate(new Date(1998, 11, 12));

        expect(date).toEqual('1998-12-12');
    });

    // TODO: Move to DateTimeUtil
    xit('should return the correct date format for 1900-02-28', () => {
        let date = Utilities.convertToDate(new Date(1900, 1, 28));

        expect(date).toEqual('1900-02-28');
    });


    // TODO: Move to DateTimeUtil
    xit('should convert local times in other time zone to time in current time zone.', () => {
        let dateString,
            date,
            testDt = new Date('4/27/2016 12:01 pm');

        // positive tz offset
        dateString = '2016-04-27T12:01:00+09:00';
        date = Utilities.shiftToNewLocal(dateString);
        expect(date.getTime()).toEqual(testDt.getTime());

        // negative tz offset
        dateString = '2016-04-27T12:01:00-09:00';
        date = Utilities.shiftToNewLocal(dateString);
        expect(date.getTime()).toEqual(testDt.getTime());

        // no tz offset.  Verify date object is the same as just parsing the argument
        dateString = '2016-04-27T12:01:00';
        date = Utilities.shiftToNewLocal(dateString);
        expect(date.getTime()).toEqual((new Date(dateString)).getTime());

        // not a string.  verify date object is the same as just parsing the argument
        dateString = new Date('2016-04-27T12:01:00');
        date = Utilities.shiftToNewLocal(dateString);
        expect(date.getTime()).toEqual((new Date(dateString)).getTime());
    });

    // TODO: Move to DateTimeUtil
    xit('should return the correct UTC time', () => {
        let date = new Date('Fri Feb 22 2013 12:00:00');

        expect(Utilities.convertToUtc(date, true)).toEqual(new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds()));
    });

    // TODO: Move to DateTimeUtil
    xit('should return parsed timestamp', () => {
        let parseTime = new Date(),
            time = new Date();

        parseTime.setHours(12, 0, 0);
        time.setTime(Utilities.parseTime('12:00:00'));

        expect(time.getHours()).toEqual(parseTime.getUTCHours());
    });

    it('should return a GUID in the proper format', () => {
        let guid = Utilities.createGUID();

        expect(guid).toMatch(/^[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}$/i);
    });

    it('should return a unique GUID', () => {
        let guid1 = Utilities.createGUID();
        let guid2 = Utilities.createGUID();

        expect(guid1).not.toEqual(guid2);
    });

    it('should return false for an empty variable', () => {
        let value;

        expect(Utilities.safeCheck(value)).toEqual(false);
    });

    it('should return true for not empty variable', () => {
        let value = 'apple';

        expect(Utilities.safeCheck(value)).toEqual(true);
    });

    // TODO: Move to DateTimeUtil
    xit('should return date', () => {
        expect(Utilities.getNow()).toBeTruthy();
    });

    // Disabled until we can sort out what's going with the set-up code --bmj
    xit('should query and return question views for specific IGR', () => {
        expect(Utilities.queryQuestionsByIGR(0).length)
        .toEqual(3);
    });

    // Disabled until we can sort out what's going with the set-up code --bmj
    xit('should query and return empty question collection for invalid IGR', () => {
        addAnswers(3);

        expect(Utilities.queryQuestionsByIGR(3).length)
        .toEqual(0);
    });

    it('should pad numbers correctly (with default totalLength of 2)', () => {
        expect(Utilities.zeroPad(0)).toEqual('00');
        expect(Utilities.zeroPad(9)).toEqual('09');
        expect(Utilities.zeroPad(10)).toEqual('10');
        expect(Utilities.zeroPad(100)).toEqual('100');
        expect(Utilities.zeroPad('0')).toEqual('00');
        expect(Utilities.zeroPad('9')).toEqual('09');
        expect(Utilities.zeroPad('10')).toEqual('10');
        expect(Utilities.zeroPad('100')).toEqual('100');
    });

    it('should pad numbers correctly (with different totalLength parameters)', () => {
        expect(Utilities.zeroPad(0, 3)).toEqual('000');
        expect(Utilities.zeroPad(9, 4)).toEqual('0009');
        expect(Utilities.zeroPad(10, 3)).toEqual('010');
        expect(Utilities.zeroPad(100, 4)).toEqual('0100');
        expect(Utilities.zeroPad('0', 2)).toEqual('00');
        expect(Utilities.zeroPad('9', 2)).toEqual('09');
        expect(Utilities.zeroPad('10', 2)).toEqual('10');
        expect(Utilities.zeroPad('100', 3)).toEqual('100');
    });

    it('should return the value of nested object correctly', () => {
        delete window.NonExistingGlobal;

        window.a = {
            b: {
                c: {
                    u: undefined,
                    n: null,
                    f: false,
                    z: 0,
                    e: '',
                    nan: NaN,
                    a: [],
                    o: {}
                }
            }
        };

        expect(Utilities.getNested('a')).toBeTruthy();
        expect(Utilities.getNested('a.b')).toBeTruthy();
        expect(Utilities.getNested('a.b.c')).toBeTruthy();
        expect(Utilities.getNested('a.b.c.u')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.n')).toBeNull();
        expect(Utilities.getNested('a.b.c.f')).toEqual(false);
        expect(Utilities.getNested('a.b.c.z')).toEqual(0);
        expect(Utilities.getNested('a.b.c.e')).toEqual('');
        expect(Utilities.getNested('a.b.c.nan')).toBeNaN();
        expect(Utilities.getNested('a.b.c.a')).toEqual([]);
        expect(Utilities.getNested('a.b.c.o')).toEqual({});

        expect(Utilities.getNested('NonExistingGlobal')).toBeUndefined();
        expect(Utilities.getNested('a.x')).toBeUndefined();
        expect(Utilities.getNested('a.x.y')).toBeUndefined();
        expect(Utilities.getNested('a.b.x')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.f.x')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.z.x')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.e.x')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.nan.x')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.a.x')).toBeUndefined();
        expect(Utilities.getNested('a.b.c.o.x')).toBeUndefined();

        expect(Utilities.getNested('b.c.nan', window.a)).toBeNaN();
        expect(Utilities.getNested(window.a, 'b.c.nan')).toBeNaN();

        delete window.a;
    });

    describe('promiseChain', () => {
        Async.it('handles non-promise handler return values', () => {
            return Utilities.promiseChain(['a', 'b', 'c'], (prevRes, item) => {
                return prevRes + item;
            }, 'x')
            .tap((res) => {
                expect(res).toEqual('xabc');
            });
        });

        Async.it('handles promise handler return values', () => {
            return Utilities.promiseChain(['a', 'b', 'c'], (prevRes, item) => {
                return Q(prevRes + item);
            }, 'x')
            .tap((res) => {
                expect(res).toEqual('xabc');
            });
        });

        Async.it('seeds with undefined by default', () => {
            return Utilities.promiseChain(['a', 'b', 'c'], (prevRes, item) => {
                return Q(prevRes + item);
            })
            .tap((res) => {
                expect(res).toEqual('undefinedabc');
            });
        });
    });

    describe('mergeObjects', () => {
        it('retains values from the left object', () => {
            let test1 = {
                    a: 'value A',
                    b: 'value B'
                },
                test2 = {
                    c: 'value C',
                    d: 'value D'
                },
                testMerged = Utilities.mergeObjects(test1, test2);

            expect(testMerged.a).toBe('value A');
            expect(testMerged.b).toBe('value B');
        });
        it('overwrites objects on the left with objects on the right', () => {
            let test1 = {
                    a: 'value A',
                    b: 'value B',
                    c: 'old value C'
                },
                test2 = {
                    c: 'new value C',
                    d: 'value D'
                },
                testMerged = Utilities.mergeObjects(test1, test2);

            expect(testMerged.c).toBe('new value C');
        });
        it('appends an array on the left with an array on the right', () => {
            let test1 = {
                    a: 'value A',
                    b: 'value B',
                    c: 'old value C',
                    d: ['array val 1', 'array val 2', 'array val 3']
                },
                test2 = {
                    c: 'new value C',
                    d: ['array val 4', 'array val 5', 'array val 6']
                },
                testMerged = Utilities.mergeObjects(test1, test2);

            expect(testMerged.d.length).toBe(6);
            for (let i = 0; i < testMerged.d.length; ++i) {
                expect(testMerged.d[i]).toBe(`array val ${i + 1}`);
            }
        });
        it('recurses multiple objects', () => {
            let test1 = {
                    a: 'value A',
                    b: 'value B',
                    c: 'old value C',
                    d: ['array val 1', 'array val 2', 'array val 3'],
                    e: 'older value E}'
                },
                test2 = {
                    c: 'new value C',
                    d: ['array val 4', 'array val 5', 'array val 6'],
                    e: 'old value E'
                },
                test3 = {
                    e: 'new value E',
                    d: ['array val 7', 'array val 8', 'array val 9']
                },
                testMerged = Utilities.mergeObjects(test1, test2, test3);

            expect(testMerged.a).toBe('value A');
            expect(testMerged.e).toBe('new value E');
            expect(testMerged.c).toBe('new value C');
            expect(testMerged.d.length).toBe(9);
            for (let i = 0; i < testMerged.d.length; ++i) {
                expect(testMerged.d[i]).toBe(`array val ${i + 1}`);
            }
        });
    });
    describe('#deepExtend', () => {
        it('does not merge arrays', () => {
            let test1 = {
                    a: 'value A',
                    b: 'value B',
                    c: 'old value C',
                    d: ['array val 1', 'array val 2', 'array val 3'],
                    e: 'older value E}'
                },
                test2 = {
                    c: 'new value C',
                    d: ['array val 4', 'array val 5', 'array val 6'],
                    e: 'old value E'
                },
                test3 = {
                    e: 'new value E',
                    d: ['array val 7', 'array val 8', 'array val 9']
                },
                testMerged = Utilities.deepExtend(test1, test2, test3);

            expect(testMerged.a).toBe('value A');
            expect(testMerged.e).toBe('new value E');
            expect(testMerged.c).toBe('new value C');

            expect(testMerged.d.length).toBe(3);
            for (let i = 7; i < testMerged.d.length; ++i) {
                expect(testMerged.d[i]).toBe(`array val ${i + 1}`);
            }
        });
    });

    describe('method:mergeArrays', () => {
        it('pushes new values on an array', () => {
            let arr1 = [{
                    key1: 'value 1A',
                    key2: 'value 1B',
                    key3: 'value 1C'
                }, {
                    key1: 'value 2A',
                    key2: 'value 2B',
                    key3: 'value 2C'
                }],
                arr2 = [{
                    key1: 'value 3A',
                    key2: 'value 3B',
                    key3: 'value 3C'
                }, {
                    key1: 'value 4A',
                    key2: 'value 4B',
                    key3: 'value 4C'
                }],
                testMerged = Utilities.mergeArrays(arr1, arr2);

            expect(testMerged.length).toBe(4);
            expect(testMerged[2].key1).toBe('value 3A');
            expect(testMerged[3].key3).toBe('value 4C');
        });

        it('overwrites an old object if equivalency key is the same', () => {
            let arr1 = [{
                    key1: 'value 1A',
                    key2: 'value 1B',
                    key3: 'value 1C'
                }, {
                    key1: 'value 2A',
                    key2: 'value 2B',
                    key3: 'value 2C'
                }],
                arr2 = [{
                    key1: 'value 3A',
                    key2: 'value 3B',
                    key3: 'value 3C'
                }, {
                    key1: 'value 4A',
                    key2: 'value 1B',
                    key3: 'value 4C'
                }],
                testMerged = Utilities.mergeArrays(arr1, arr2, 'key2');

            expect(testMerged.length).toBe(3);
            expect(testMerged[0].key1).toBe('value 4A');
            expect(testMerged[0].key2).toBe('value 1B');
            expect(testMerged[0].key3).toBe('value 4C');

            expect(testMerged[2].key3).toBe('value 3C');
        });
    });

    describe('method:isValidPassword', () => {
        it('should validate password with default settings.', () => {
            expect(Utilities.isValidPassword('')).toBe(false);

            expect(Utilities.isValidPassword('abc')).toBe(false);
            expect(Utilities.isValidPassword('abcd')).toBe(true);

            expect(Utilities.isValidPassword('12345678901234567890123456789')).toBe(false);
            expect(Utilities.isValidPassword('12345678')).toBe(true);
        });

        it('should validate password with custom min settings.', () => {
            let passwordFormat = {
                min: 5
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min settings.', () => {
            let passwordFormat = {
                min: 0
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min settings.', () => {
            let passwordFormat = {
                min: -2
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(true);
        });

        it('should validate password with custom max settings.', () => {
            let passwordFormat = {
                max: 6
            };

            expect(Utilities.isValidPassword('1234567', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(true);
        });

        it('should validate password with custom max settings.', () => {
            let passwordFormat = {
                max: 0
            };

            expect(Utilities.isValidPassword('1234567', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
        });

        it('should validate password with custom max settings.', () => {
            let passwordFormat = {
                max: -2
            };

            expect(Utilities.isValidPassword('1234567', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
        });

        it('should validate password with custom min/max settings.', () => {
            let passwordFormat = {
                min: 5,
                max: 6
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('1234567', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(true);
        });

        it('should validate password with custom lower settings.', () => {
            let passwordFormat = {
                lower: 1
            };

            expect(Utilities.isValidPassword('ABCD', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('ABcD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABcd', passwordFormat)).toBe(true);

            passwordFormat = {
                lower: 5
            };

            expect(Utilities.isValidPassword('ABCDefg', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('Abcdef', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('Abcdefgh', passwordFormat)).toBe(true);
        });

        it('should validate password with custom lower settings.', () => {
            let passwordFormat = {
                lower: 0
            };

            expect(Utilities.isValidPassword('ABCD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABcD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABcd', passwordFormat)).toBe(true);
        });

        it('should validate password with custom lower settings.', () => {
            let passwordFormat = {
                lower: -1
            };

            expect(Utilities.isValidPassword('ABCD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABcD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABcd', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/lower settings.', () => {
            let passwordFormat = {
                min: 3,
                lower: 1
            };

            expect(Utilities.isValidPassword('1a', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456abcdefg', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1ab', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('abc', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('123', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12AB', passwordFormat)).toBe(false);
        });

        it('should validate password with custom max/lower settings.', () => {
            let passwordFormat = {
                max: 4,
                lower: 2
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12ab', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('12AB', passwordFormat)).toBe(false);
        });

        it('should validate password with custom min/max/lower settings.', () => {
            let passwordFormat = {
                min: 3,
                max: 6,
                lower: 2
            };

            expect(Utilities.isValidPassword('123', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1ab', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1234ab', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('1AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1234AB', passwordFormat)).toBe(false);
        });

        it('should validate password with custom upper settings.', () => {
            let passwordFormat = {
                upper: 1
            };

            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('abcD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABCD', passwordFormat)).toBe(true);

            passwordFormat = {
                upper: 5
            };

            expect(Utilities.isValidPassword('abcDEF', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('ABCDE', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABCDEFG', passwordFormat)).toBe(true);
        });

        it('should validate password with custom upper settings.', () => {
            let passwordFormat = {
                upper: 0
            };

            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('abcD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABCD', passwordFormat)).toBe(true);
        });

        it('should validate password with custom upper settings.', () => {
            let passwordFormat = {
                upper: -2
            };

            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('abcD', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABCD', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/upper settings.', () => {
            let passwordFormat = {
                min: 3,
                upper: 1
            };

            expect(Utilities.isValidPassword('1A', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456ABCDEFG', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1AB', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('ABC', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('123', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12ab', passwordFormat)).toBe(false);
        });

        it('should validate password with custom max/upper settings.', () => {
            let passwordFormat = {
                max: 4,
                upper: 2
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12AB', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('12ab', passwordFormat)).toBe(false);
        });

        it('should validate password with custom lower/upper settings.', () => {
            let passwordFormat = {
                lower: 2,
                upper: 2
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12ab', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('abAB', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('aAbB', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/max/lower/upper settings.', () => {
            let passwordFormat = {
                min: 3,
                max: 6,
                lower: 1,
                upper: 1
            };

            expect(Utilities.isValidPassword('123', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123abAB', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('12abAB', passwordFormat)).toBe(true);
        });

        it('should validate password with custom alpha settings.1', () => {
            let passwordFormat = {
                alpha: 1
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123a', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123A', passwordFormat)).toBe(true);
        });

        it('should validate password with custom alpha settings.3', () => {
            let passwordFormat = {
                alpha: 3
            };

            expect(Utilities.isValidPassword('a1b2', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('a1b2c3', passwordFormat)).toBe(true);
        });

        it('should validate password with custom alpha settings.0', () => {
            let passwordFormat = {
                alpha: 0
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123a', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123A', passwordFormat)).toBe(true);
        });

        it('should validate password with custom alpha settings.-1', () => {
            let passwordFormat = {
                alpha: -1
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123a', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123A', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/alpha settings.', () => {
            let passwordFormat = {
                min: 5,
                alpha: 1
            };

            expect(Utilities.isValidPassword('1A', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123456ABCDefg', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12Ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('ABCde', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123', passwordFormat)).toBe(false);
        });

        it('should validate password with custom max/alpha settings.', () => {
            let passwordFormat = {
                max: 4,
                alpha: 2
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12AB', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12ab', passwordFormat)).toBe(true);
        });

        it('should validate password with custom lower/alpha settings.', () => {
            let passwordFormat = {
                lower: 2,
                alpha: 3
            };

            expect(Utilities.isValidPassword('123ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12abC', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('1ABC', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('aAbB', passwordFormat)).toBe(true);
        });

        it('should validate password with custom upper/alpha settings.', () => {
            let passwordFormat = {
                upper: 2,
                alpha: 3
            };

            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12ABc', passwordFormat)).toBe(true);

            expect(Utilities.isValidPassword('1abc', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('aAbB', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/max/lower/upper/alpha settings.', () => {
            let passwordFormat = {
                min: 3,
                max: 6,
                lower: 2,
                upper: 2,
                alpha: 5
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1234aB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1ABab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123abAB', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('1abABe', passwordFormat)).toBe(true);
        });

        it('should validate password with custom numeric settings.', () => {
            let passwordFormat = {
                numeric: 1
            };

            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('a1bc', passwordFormat)).toBe(true);

            passwordFormat = {
                numeric: 3
            };

            expect(Utilities.isValidPassword('a1b2', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('a1b2c3', passwordFormat)).toBe(true);
        });

        it('should validate password with custom numeric settings.', () => {
            let passwordFormat = {
                numeric: 0
            };

            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('a1bc', passwordFormat)).toBe(true);
        });

        it('should validate password with custom numeric settings.', () => {
            let passwordFormat = {
                numeric: -5
            };

            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('a1bc', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/numeric settings.', () => {
            let passwordFormat = {
                min: 5,
                numeric: 3
            };

            expect(Utilities.isValidPassword('123A', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123ABCDefg', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12Abc', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('ABCde', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123', passwordFormat)).toBe(false);
        });

        it('should validate password with custom max/numeric settings.', () => {
            let passwordFormat = {
                max: 4,
                numeric: 2
            };

            expect(Utilities.isValidPassword('ABcdE', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12AB', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12ab', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
        });

        it('should validate password with custom lower/numeric settings.', () => {
            let passwordFormat = {
                lower: 2,
                numeric: 3
            };

            expect(Utilities.isValidPassword('12abc', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12abC', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('123ab', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12345abcde', passwordFormat)).toBe(true);
        });

        it('should validate password with custom upper/numeric settings.', () => {
            let passwordFormat = {
                upper: 2,
                numeric: 3
            };

            expect(Utilities.isValidPassword('12ABC', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12abC', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('123AB', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('12345ABCDE', passwordFormat)).toBe(true);
        });

        it('should validate password with custom alpha/numeric settings.', () => {
            let passwordFormat = {
                alpha: 1,
                numeric: 3
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12AB3', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('a123', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('AbCd1234', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/max/lower/upper/alpha/numeric settings.', () => {
            let passwordFormat = {
                min: 3,
                max: 6,
                lower: 1,
                upper: 1,
                alpha: 3,
                numeric: 2
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12aB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1ABab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123abAB', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('1abAB2', passwordFormat)).toBe(true);
        });

        it('should validate password with custom special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                special: 1,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('1234567', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123!', passwordFormat)).toBe(true);

            // expect(Utilities.isValidPassword('1_2_3')).toBe(true);

            passwordFormat = {
                max: 20,
                min: 4,
                special: 3,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('a_b_c', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('a_b_c_', passwordFormat)).toBe(true);
        });

        it('should validate password with custom special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 0,
                alpha: 0,
                numeric: 0,
                special: 0,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123!', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1_2_3', passwordFormat)).toBe(true);
        });

        it('should validate password with custom special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 0,
                alpha: 0,
                numeric: 0,
                special: -3,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('123!', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1_2_3', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/special settings.', () => {
            let passwordFormat = {
                min: 5,
                special: 3
            };

            expect(Utilities.isValidPassword('123!', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123Ab_$#', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('_!Abc', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('ABCde', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123_&', passwordFormat)).toBe(false);
        });

        it('should validate password with custom max/special settings.', () => {
            let passwordFormat = {
                max: 4,
                special: 2
            };

            expect(Utilities.isValidPassword('!@#$%', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('123#$', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12_!', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('!@#$', passwordFormat)).toBe(true);
        });

        it('should validate password with custom lower/special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                upper: 0,
                alpha: 0,
                numeric: 0,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true,
                lower: 2,
                special: 3
            };

            expect(Utilities.isValidPassword('!_abc', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('#$%AB', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('!@abC', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('_!_ab', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('!@#$%abcde', passwordFormat)).toBe(true);
        });

        it('should validate password with custom upper/special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 2,
                alpha: 0,
                numeric: 0,
                special: 3,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('!_ABC', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('!@#ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('!_abC', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('_!_AB', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('!@#$%ABCDE', passwordFormat)).toBe(true);
        });

        it('should validate password with custom alpha/special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 0,
                alpha: 1,
                numeric: 0,
                special: 3,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('!@#$', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('abcd', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('!_AB_', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('a!@#', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('AbCd_!_!', passwordFormat)).toBe(true);
        });

        it('should validate password with custom numeric/special settings.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 0,
                alpha: 0,
                numeric: 1,
                special: 3,
                custom: [],
                allowRepeating: true,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('!@#$', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1234', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('!@12_', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('1_!_', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('_!@#1234', passwordFormat)).toBe(true);
        });

        it('should validate password with custom min/max/lower/upper/alpha/numeric/special settings.', () => {
            let passwordFormat = {
                min: 3,
                max: 7,
                lower: 1,
                upper: 1,
                alpha: 3,
                numeric: 2,
                special: 1
            };

            expect(Utilities.isValidPassword('12345', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('aBcDeF', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('!@#$%^', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('12aB#$', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('1ABab_', passwordFormat)).toBe(false);

            expect(Utilities.isValidPassword('1ab_AB2', passwordFormat)).toBe(true);
        });

        it('should validate password with allowConsecutive turned off.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 0,
                alpha: 0,
                numeric: 0,
                special: 0,
                custom: [],
                allowRepeating: true,
                allowConsecutive: false
            };

            // Test min setting.
            expect(Utilities.isValidPassword('aabcd', passwordFormat)).toBe(false);
        });

        it('should validate password with allowRepeating turned off.', () => {
            let passwordFormat = {
                max: 20,
                min: 4,
                lower: 0,
                upper: 0,
                alpha: 0,
                numeric: 0,
                special: 0,
                custom: [],
                allowRepeating: false,
                allowConsecutive: true
            };

            expect(Utilities.isValidPassword('abcda', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('abcde', passwordFormat)).toBe(true);
        });

        it('should validate password with a custom function.', () => {
            let passwordFormat = {
                allowRepeating: true,
                allowConsecutive: true,
                custom: [
                    function (password) {
                        return password.length === 5;
                    }
                ]
            };

            expect(Utilities.isValidPassword('ab', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('abcde', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('abcde4', passwordFormat)).toBe(false);
        });

        it('should validate password with a custom RegExp.', () => {
            let passwordFormat = {
                max: 20,
                min: 1,
                allowRepeating: true,
                allowConsecutive: true,
                custom: [
                    new RegExp('p{2,}')
                ]
            };

            expect(Utilities.isValidPassword('aple', passwordFormat)).toBe(false);
            expect(Utilities.isValidPassword('apple', passwordFormat)).toBe(true);
            expect(Utilities.isValidPassword('aplep', passwordFormat)).toBe(false);
        });

        it('should validate password with multiple custom regular expressions', () => {
            let passwordFormat = {
                custom: [
                    new XRegExp('\\p{InHiragana}'),
                    new XRegExp('\u3053')
                ]
            };

            // Using JavaScript-escaped hexadecimal code.
            expect(Utilities.isValidPassword('\u3053\u3093\u306B\u3061\u306F\u4E16\u754C', passwordFormat)).toBe(true);
        });

        it('should validate the password based on the set language.', () => {
            // You would typically pull the lang attr from the LF preferred language and locale object.
            let lang = 'en-US';

            let passwordFormat = {
                max: 20,
                min: 1,
                allowRepeating: true,
                allowConsecutive: true,
                custom: [
                    (password) => {
                        if (lang === 'ja-JP') {
                            return new XRegExp('\\p{InHiragana}').test(password);
                        }
                        return true;
                    }
                ]
            };

            expect(Utilities.isValidPassword('Apple', passwordFormat)).toBe(true);

            lang = 'ja-JP';

            expect(Utilities.isValidPassword('\u3053\u3093\u306B\u3061\u306F\u4E16\u754C', passwordFormat)).toBe(true);
        });
    });

    describe('getRaterTrainingConfig', () => {
        it('returns correct node if logpad', () => {
            let oldAppName = LF.appName;
            LF.appName = 'LogPad App';

            let config = Utilities.getRaterTrainingConfig();
            expect(config).toEqual(LF.StudyDesign.raterTrainingConfig.logpad);
            LF.appName = oldAppName;
        });
        it('returns correct node if sitepad', () => {
            let oldAppName = LF.appName;
            LF.appName = 'SitePad App';

            let config = Utilities.getRaterTrainingConfig();
            expect(config).toEqual(LF.StudyDesign.raterTrainingConfig.sitepad);
            LF.appName = oldAppName;
        });
    });

    describe('localizationUtils', () => {
        describe('#toStrictRTL', () => {
            it('preserves, but mirrors, a static numeric string with dashes (so in a strict RTL BDO, will display left-to-right)', () => {
                let testString = '2016-10-15';
                expect(Utilities.toStrictRTL(testString)).toBe(testString.split('').reverse().join(''));
            });
            it('Creates a strict RTL representation of a string of mixed characters (as formatted in HTML)', () => {
                let testString = 'في فيلم "Blade Runner 2049"، كما سنشاهد';

                // Compare display with
                // http://www.bbc.com/arabic/art-and-culture-38449446
                let expectedOrder = [
                    'في فيلم '.split(''), '"Blade Runner 2049"'.split('').reverse(),
                    '، كما سنشاهد'.split('')
                ];
                expect(Utilities.toStrictRTL(testString).split('').join('\n')).toEqual(_.flatten(expectedOrder).join('\n'));
            });
        });
    });

    describe('makeWindowsNumericInputCover', () => {
        let inputId = 'passwordInput1',
            template =
                `<div id="password-template">
                    <div id="content">
                        <div id="wrapper">
                            <input id="${inputId}" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>`;

        beforeEach(() => {
            $('body').append(template);
        });

        afterEach(() => {
            $('#password-template').remove();
        });
        it('makes input numeric cover', () => {
            Utilities.makeWindowsNumericInputCover(inputId, $('#wrapper'));
            expect($(`#numberInput${inputId}`).length).toEqual(1);
            expect($(`#${inputId}`).prop('type')).toEqual('password');
            expect($(`#numberInput${inputId}`).prop('type')).toEqual('number');
        });
    });
});
