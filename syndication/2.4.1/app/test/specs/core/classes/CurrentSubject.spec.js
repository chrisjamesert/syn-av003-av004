import Subjects from 'core/collections/Subjects';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import CurrentSubject from 'core/classes/CurrentSubject';

TRACE_MATRIX('US66695').
describe('CurrentSubject', () => {
    let localStorageKrpt,
        krpt = 'SA.c18f9a50508341c2a816225316e69248',
        siteUser = new User({
            id: 1,
            userType: 'SiteUser',
            username: 'Thor',
            language: 'en-US',
            password: '36e368ca5df356e82efa7887d9176848',
            salt: '1e321604-d830-480e-9f34-298178f27c50',
            syncValue: 'DOM.287206.246',
            userId: 1,
            secretQuestion: '0',
            secretAnswer: 'hammer',
            active: 1,
            role: 'admin'
        }),
        subjectUser = new User({
            id: 2,
            userType: 'Subject',
            username: 'Batman',
            language: 'en-US',
            password: 'abc368ca5df356e82efa7887d9176848',
            salt: 'abc21604-d830-480e-9f34-298178f27c50',
            syncValue: 'DOM.287206.246',
            userId: 2,
            secretQuestion: '0',
            secretAnswer: 'alfred',
            active: 1,
            role: 'admin'
        }),
        subjectOfUser = new Subject({
            krpt: 'SA.f367ba475fa84f4bb8ffdccdc27c2e65',
            initials: 'BM',
            language: 'en-US',
            phase: 10,
            deleted: false,
            enrollmentDate: '2016-07-01T17:54:11.000-04:00',
            setupCode: '59622278',
            subject_active: 1,
            phaseStartDateTZOffset: '2016-07-01T17:54:11.000-04:00',
            site_code: '0009',
            subject_number: '0204',
            subject_id: '0009-0204',
            service_password: '83076693d063f0931a9ffa29be28c976',
            subject_password: '899b3baeae31c5ad13b243ad27b231d5',
            secret_answer: 'f7c0e071db137f5ae65382041c7cef4b',
            secret_question: '0',
            id: 1,
            user: 2
        }),
        subjectForKrpt = new Subject({
            krpt: krpt,
            initials: 'EDY',
            language: 'en-US',
            phase: 10,
            deleted: false,
            enrollmentDate: '2016-07-01T18:02:08.000-04:00',
            setupCode: '12192559',
            subject_active: 1,
            phaseStartDateTZOffset: '2016-07-01T18:02:08.000-04:00',
            site_code: '0009',
            subject_number: '0205',
            subject_id: '0009-0205',
            service_password: '1adb1d2f389202b7f5a2a820c3bb110f',
            subject_password: '338009e4f5928de4e2305ea2d2901350',
            secret_answer: 'f7c0e071db137f5ae65382041c7cef4b',
            secret_question: '0',
            id: 2
        }),
        subjects = new Subjects([subjectOfUser, subjectForKrpt]);

    beforeAll(() => {
        // fake the fetchCollection method of Subjects collection to prevent db operations
        spyOn(Subjects, 'fetchCollection').and.callFake(() => {
            return Q(subjects);
        });

        localStorageKrpt = localStorage.getItem('krpt');
    });

    afterAll(() => {
        localStorage.setItem('krpt', localStorageKrpt);
    });

    beforeEach(() => {
        LF.security = {};
        localStorage.removeItem('krpt');
    });

    Async.it('should return a promise for getSubject()', () => {
        localStorage.setItem('krpt', krpt);

        let result = CurrentSubject.getSubject();

        expect(result).toBePromise();

        return result
        .then(() => CurrentSubject.clearSubject());
    });

    Async.it('should return the subject with the krpt in localStorage', () => {
        localStorage.setItem('krpt', krpt);

        return CurrentSubject.getSubject()
        .then(subject => {
            expect(subject.get('krpt')).toBe(krpt);
        })
        .then(() => CurrentSubject.clearSubject());
    });

    Async.it('should return the subject of the active user when it is a subject user', () => {
        LF.security.activeUser = subjectUser;

        return CurrentSubject.getSubject()
        .then(subject => {
            expect(subject).toBe(subjectOfUser);
        })
        .then(() => CurrentSubject.clearSubject());
    });

    Async.it('should clear the cache if it has expired', () => {
        spyOn(CurrentSubject, 'clearSubject');

        LF.security.activeUser = subjectUser;
        return CurrentSubject.getSubject()
        .then(() => {
            LF.security.activeUser = siteUser;
        })
        .then(() => CurrentSubject.getSubject())
        .then(() => {
            expect(CurrentSubject.clearSubject).toHaveBeenCalled();
        })
        .then(() => CurrentSubject.clearSubject());
    });

    Async.it('should return null when active user is not a subject user', () => {
        LF.security.activeUser = siteUser;

        return CurrentSubject.getSubject()
        .then(subject => {
            expect(subject).toBe(null);
        })
        .then(() => CurrentSubject.clearSubject());
    });
});
