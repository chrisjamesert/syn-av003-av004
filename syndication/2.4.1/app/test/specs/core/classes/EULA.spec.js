import * as lStorage from 'core/lStorage';
import EULA from 'core/classes/EULA';

class EULASuite {
    constructor () {
        beforeEach(() => {
            spyOn(lStorage, 'getItem').and.returnValue(true);
            spyOn(lStorage, 'removeItem').and.stub();
            spyOn(lStorage, 'setItem').and.stub();
        });
    }

    testIsAccepted () {
        describe('method:isAccepted', () => {
            it('should return false (not accepted).', () => {
                lStorage.getItem.and.returnValue(null);

                expect(EULA.isAccepted()).toBe(false);
            });

            it('should return true (accepted).', () => {
                expect(EULA.isAccepted()).toBe(true);
            });
        });
    }

    testAccept () {
        describe('method:accept', () => {
            it('set the localStorage value to true.', () => {
                EULA.accept();

                expect(lStorage.setItem).toHaveBeenCalledWith('EULAAccepted', true);
            });
        });
    }

    testDecline () {
        describe('method:decline', () => {
            it('should remove the acceptance from localStorage', () => {
                EULA.decline();

                expect(lStorage.removeItem).toHaveBeenCalled();
            });
        });
    }
}

describe('EULA', () => {
    let suite = new EULASuite();

    suite.testIsAccepted();
    suite.testAccept();
    suite.testDecline();
})