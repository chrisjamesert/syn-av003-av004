import Questionnaire from 'core/models/Questionnaire';
import 'core/utilities';

/*
 *  NOTE: All data validation tests are disabled because that functionality is not currently
 *  available in the base model class.  -- bmj
 */
describe('Questionnaire', () => {
    let model;

    beforeEach(() => {
        model = new Questionnaire({
            id: 'EQ5D',
            SU: 'EQ5D',
            displayName: 'DISPLAY_NAME',
            className: 'EQ5D',
            screens: ['EQ5D_S_1', 'EQ5D_S_2', 'EQ5D_S_3']
        });
    });

    afterEach(() => {
        model = undefined;
    });

    it('should have an id of EQ5D', () => {
        expect(model.get('id')).toEqual('EQ5D');
    });

    it('should have an SU of EQ5D', () => {
        expect(model.get('SU')).toEqual('EQ5D');
    });

    it('should have a schema', () => {
        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');
    });

    it('should NOT have a table', () => {
        expect(model.table).not.toBeDefined();
    });

    it('should have a previousScreen attribute set to true', () => {
        expect(model.get('previousScreen')).toBe(true);
    });

    it('should have an allowTranscriptionMode attribute set to false', () => {
        expect(model.get('allowTranscriptionMode')).toBe(false);
    });

    it('should throw an error because the attribute (id) is too long', () => {
        model.set(
            { id: '123456789-123456789-123456789-123456789-123456789-123456789-' },
            { validate: true }
        );

        expect(model.validationError.message).toMatch(/Attribute: id Invalid Length/);
    });

    it('should throw an error because invalid attribute type (expects a string)', () => {
        model.set({ id: 1 }, { validate: true });

        expect(model.validationError.message).toMatch(/Attribute: id. Invalid DataType. Expected string. Received number./);
    });
});

