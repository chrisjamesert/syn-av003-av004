import LastDiary from 'core/models/LastDiary';
import * as DateTimeUtil from 'core/DateTimeUtil';
import 'test/helpers/StudyDesign';
import { eCoaDB } from 'core/dataAccess';

TRACE_MATRIX('US6572').
describe('LastDiary', () => {
    let model,
        id = 1,
        lastStartedDate = DateTimeUtil.timeStamp(new Date()),
        lastCompletedDate = (new Date()).ISOStamp(),
        updated = (new Date()).ISOStamp(),
        role = JSON.stringify(['Subject', 'Site']),
        studyQuestionnaire = LF.StudyDesign.questionnaires,
        krpt = 'NP.df058bd06f4e4373bc799fa84f187a6e',
        questionnaireId = 'DAILY_DIARY';

    beforeEach(() => {
        model = new LastDiary({
            id,
            krpt,
            questionnaire_id: questionnaireId,
            lastStartedDate,
            lastCompletedDate,
            role,
            updated
        });
    });

    it('should have a schema', () => {
        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');
    });

    it('should have a name property', () => {
        expect(model.name).toEqual('LastDiary');
    });

    it('should have a dateKeys property', () => {
        expect(model.dateKeys).toEqual({ 
            lastStartedDate: true,
            lastCompletedDate: true,
            updated: true
        });
    });

    it(`should have an id of ${id}`, () => {
        expect(model.get('id')).toEqual(id);
    });

    it('should not encrypt the id property', () => {
        model.encrypt();
        expect(model.get('id')).toEqual(id);
    });

    it(`should have a krpt of '${krpt}'`, () => {
        expect(model.get('krpt')).toEqual(krpt);
    });

    it('should throw an error if the krpt is set to null or undefined', () => {
        model.set({ krpt: null }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: krpt must not be null or undefined.');
    });

    it('should throw an error because invalid krpt data type (expects a string)', () => {
        model.set({ krpt: 9876 }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: krpt. Invalid DataType. Expected string. Received number.');
    });

    it('should not encrypt the krpt property', () => {
        model.encrypt();
        expect(model.get('krpt')).toEqual(krpt);
    });

    it(`should have a questionnaire_id value of '${questionnaireId}'`, () => {
        expect(model.get('questionnaire_id')).toEqual(questionnaireId);
    });

    it('should throw an error if the questionnaire_id is set to null or undefined', () => {
        model.set({ questionnaire_id: null }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: questionnaire_id must not be null or undefined.');
    });

    it('should throw an error because invalid questionnaire_id data type (expects a string)', () => {
        model.set({ questionnaire_id: 9876 }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: questionnaire_id. Invalid DataType. Expected string. Received number.');
    });

    it('should not encrypt the questionnaire_id property', () => {
        model.encrypt();
        expect(model.get('questionnaire_id')).toEqual(questionnaireId);
    });

    it(`should have a lastStartedDate value of '${lastStartedDate}'`, () => {
        expect(model.get('lastStartedDate')).toEqual(lastStartedDate);
    });

    it('should throw an error because invalid lastStartedDate data type (expects a string)', () => {
        model.set({ lastStartedDate: 9876 }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: lastStartedDate. Invalid DataType. Expected string. Received number.');
    });

    it('should throw an error because invalid lastStartedDate data type (expects a date)', () => {
        model.set({ lastStartedDate: 'Not A Date' }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: lastStartedDate. Invalid DataType. Expected date-parsable string. Received "Not A Date", which cannot be parsed as a date.');
    });

    it('should not encrypt the lastStartedDate property', () => {
        model.encrypt();
        expect(model.get('lastStartedDate')).toEqual(lastStartedDate);
    });

    it(`should have a lastCompletedDate value of '${lastCompletedDate}'`, () => {
        expect(model.get('lastCompletedDate')).toEqual(lastCompletedDate);
    });

    it('should throw an error because invalid lastCompletedDate data type (expects a string)', () => {
        model.set({ lastCompletedDate: 9876 }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: lastCompletedDate. Invalid DataType. Expected string. Received number.');
    });

    it('should throw an error because invalid lastCompletedDate data type (expects a date)', () => {
        model.set({ lastCompletedDate: 'Not A Date' }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: lastCompletedDate. Invalid DataType. Expected date-parsable string. Received "Not A Date", which cannot be parsed as a date.');
    });

    it('should not encrypt the lastCompletedDate property', () => {
        model.encrypt();
        expect(model.get('lastCompletedDate')).toEqual(lastCompletedDate);
    });

    it(`should have a role of '${role}'`, () => {
        expect(model.get('role')).toEqual(role);
    });

    it('should throw an error because invalid role data type (expects a string)', () => {
        model.set({ role: 9876 }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: role. Invalid DataType. Expected string. Received number.');
    });

    it('should not encrypt the role property', () => {
        model.encrypt();
        expect(model.get('role')).toEqual(role);
    });

    it(`should have a updated value of '${updated}'`, () => {
        expect(model.get('updated')).toEqual(updated);
    });

    it('should throw an error because invalid updated data type (expects a string)', () => {
        model.set({ updated: 9876 }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: updated. Invalid DataType. Expected string. Received number.');
    });

    it('should throw an error because invalid updated data type (expects a date)', () => {
        model.set({ updated: 'Not A Date' }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: updated. Invalid DataType. Expected date-parsable string. Received "Not A Date", which cannot be parsed as a date.');
    });

    it('should not encrypt the updated property', () => {
        model.encrypt();
        expect(model.get('updated')).toEqual(updated);
    });

    Async.it('should save the model to the database.', () => {
        return model.save()
            .then((res) => {
                expect(res).toBe(1);
            });
    });

    Async.it('should ensure the proper fields are encrypted at rest', () => {
        return eCoaDB.LastDiary.all()
            .then((res) => {
                let model = res.at(0);

                expect(model.id).toBe(id);
                expect(model.questionnaire_id).toBe(questionnaireId);
                expect(model.lastStartedDate).toBe(lastStartedDate);
                expect(model.lastCompletedDate).toBe(lastCompletedDate);
                expect(model.role).toBe(role);
                expect(model.updated).toBe(updated);
            });
    });
});
