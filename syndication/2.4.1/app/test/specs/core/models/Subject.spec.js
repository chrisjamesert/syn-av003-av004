// jscs:disable requireArrowFunctions, requireTemplateStrings
import Subject from 'core/models/Subject';
import * as utils from 'core/utilities';
import { timeStamp } from 'core/DateTimeUtil';
import { eCoaDB } from 'core/dataAccess';

import * as helpers from 'test/helpers/SpecHelpers';

describe('Subject', function () {

    let model;

    beforeAll((done) => helpers.uninstallDatabase().finally(done));

    beforeEach(function () {

        localStorage.setItem('krpt', 'krpt.39ed120a0981231');

        model = new Subject({
            id                      : 1,
            subject_id              : '9876',
            krpt                    : 'krpt.39ed120a0981231',
            device_id               : '2131243432234',
            subject_password        : hex_sha512('apple' + 'krpt.39ed120a0981231'),
            service_password        : hex_sha512('apple' + 'krpt.39ed120a0981231'),
            subject_number          : '002007',
            site_code               : '002',
            initials                : 'CWH',
            secret_question         : 'SECRET_QUESTION_0',
            secret_answer           : '3',
            phase                   : 10,
            phaseStartDateTZOffset  : timeStamp(new Date()),
            phaseTriggered          : 'false',
            enrollmentDate          : timeStamp(new Date()),
            activationDate          : timeStamp(new Date())

        });

    });

    afterEach(() => localStorage.clear());

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');

    });

    it('should have a name property', function () {

        expect(model.name).toEqual('Subject');

    });

    it('should have a storage property', function () {

        expect(model.storage).toEqual('Subject');

    });

    it('should have an id of 1', function () {

        expect(model.get('id')).toEqual(1);

    });

    it('should throw an error because invalid id data type (expects a number)', function () {

        model.set({ id: '1' }, {validate:true});

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the id property', function () {

        model.encrypt();

        expect(model.get('id')).toEqual(1);

    });

    it('should have a subject_id of \'9876\'', function () {

        expect(model.get('subject_id')).toEqual('9876');

    });

    it('should throw an error if the subject_id is set to null or undefined', function () {

        model.set({ subject_id: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: subject_id must not be null or undefined.');

    });

    it('should throw an error because invalid subject_id data type (expects a string)', function () {

        model.set({ subject_id: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: subject_id. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the subject_id property', function () {

        model.encrypt();

        expect(model.get('subject_id')).not.toEqual('9876');

    });

    it(`should have a subject_password of ${hex_sha512('apple' + 'krpt.39ed120a0981231')}`, function () {

        expect(model.get('subject_password')).toEqual(hex_sha512('apple' + 'krpt.39ed120a0981231'));

    });

    it('should throw an error because invalid subject_password data type (expects a string)', function () {

        model.set({ subject_password: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: subject_password. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the subject_password property', function () {

        model.encrypt();

        expect(model.get('subject_password')).toEqual(hex_sha512('apple' + 'krpt.39ed120a0981231'));

    });

    it(`should have a service_password of ${hex_sha512('apple' + 'krpt.39ed120a0981231')}`, function () {

        expect(model.get('service_password')).toEqual(hex_sha512('apple' + 'krpt.39ed120a0981231'));

    });

    it('should throw an error because invalid service_password data type (expects a string)', function () {

        model.set({ service_password: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: service_password. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the service_password property', function () {

        model.encrypt();

        expect(model.get('service_password')).toEqual(hex_sha512('apple' + 'krpt.39ed120a0981231'));

    });

    it('should have a initials of CWH', function () {

        expect(model.get('initials')).toEqual('CWH');

    });

    it('should throw an error because invalid initials data type (expects a string)', function () {

        model.set({ initials: 123 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: initials. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the initials property', function () {

        model.encrypt();

        expect(model.get('initials')).not.toEqual('CWH');

    });

    it('should have a subject_number of \'002007\'', function () {

        expect(model.get('subject_number')).toEqual('002007');

    });

    it('should throw an error because invalid subject_number data type (expects a string)', function () {

        model.set({ subject_number: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: subject_number. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the subject_id property', function () {

        model.encrypt();

        expect(model.get('subject_number')).not.toEqual('002007');

    });

    it('should have a site_code of \'002\'', function () {

        expect(model.get('site_code')).toEqual('002');

    });

    it('should throw an error because invalid site_code data type (expects a string)', function () {

        model.set({ site_code: 777 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: site_code. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the site_code property', function () {

        model.encrypt();

        expect(model.get('site_code')).not.toEqual('002');

    });

    it('should have a device_id of \'2131243432234\'', function () {

        expect(model.get('device_id')).toEqual('2131243432234');

    });

    it('should throw an error because invalid device_id data type (expects a string)', function () {

        model.set({ device_id: 123421323213 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: device_id. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the device_id property', function () {

        model.encrypt();

        expect(model.get('device_id')).not.toEqual('2131243432234');

    });

    it('should have a krpt of \'krpt.39ed120a0981231\'', function () {

        expect(model.get('krpt')).toEqual('krpt.39ed120a0981231');

    });

    it('should throw an error if the krpt is set to null or undefined', function () {

        model.set({ krpt: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: krpt must not be null or undefined.');

    });

    it('should throw an error because invalid krpt data type (expects a string)', function () {

        model.set({ krpt: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: krpt. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the krpt property', function () {

        model.encrypt();

        expect(model.get('krpt')).toEqual('krpt.39ed120a0981231');

    });

    it('should have a secret_question of \'SECRET_QUESTION_0\'', function () {

        expect(model.get('secret_question')).toEqual('SECRET_QUESTION_0');

    });

    it('should throw an error because invalid secret_question data type (expects a string)', function () {

        model.set({ secret_question: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: secret_question. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the secret_question property', function () {

        model.encrypt();

        expect(model.get('secret_question')).not.toEqual('SECRET_QUESTION_0');

    });

    it('should have a secret_answer of \'3\'', function () {

        expect(model.get('secret_answer')).toEqual('3');

    });

    it('should throw an error because invalid secret_answer data type (expects a string)', function () {

        model.set({ secret_answer: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: secret_answer. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the secret_answer property', function () {

        model.encrypt();

        expect(model.get('secret_answer')).not.toEqual('3');

    });

    it('should have a phase of \'10\'', function () {

        expect(model.get('phase')).toEqual(10);

    });

    it('should throw an error because invalid phase data type (expects a number)', function () {

        model.set({ phase: '10' }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: phase. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the phase property', function () {

        model.encrypt();

        expect(model.get('phase')).toEqual(10);

    });

    it('should save the model to the database.', function (done) {

        let response;

        model.save().then((res) => {

            expect(res).toEqual(1);
            done();

        });

    });

    it('should ensure the proper fields are encrypted at rest', function (done) {

        let response;

        eCoaDB.Subject.all().then((res) => {

            let model = res.at(0);

            expect(model.id).toEqual(1);
            expect(model.subject_id).not.toEqual('12345');
            expect(model.krpt).toEqual('krpt.39ed120a0981231');
            expect(model.device_id).not.toEqual('2131243432234');
            expect(model.initials).not.toEqual('CWH');
            expect(model.subject_password).toEqual(hex_sha512('apple' + 'krpt.39ed120a0981231'));
            expect(model.service_password).toEqual(hex_sha512('apple' + 'krpt.39ed120a0981231'));
            expect(model.secret_question).not.toEqual('SECRET_QUESTION_0');
            expect(model.secret_answer).not.toEqual(hex_sha512('3'));
            expect(model.subject_number).not.toEqual('002007');
            expect(model.site_code).not.toEqual('002');
            expect(model.phase).toEqual(10);

            done();

        });
    });

});
