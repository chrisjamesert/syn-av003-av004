import Answer from 'core/models/Answer';
import 'core/utilities';
import * as helpers from 'test/helpers/SpecHelpers';

describe('Answer', function () {

    let model;

    beforeEach(function () {

        model = new Answer({
            id                  : 1,
            subject_id          : '12345',
            response            : '7',
            SW_Alias            : 'EQ5D.0.EQ5D_Q_1',
            instance_ordinal    : 1,
            questionnaire_id    : 'EQ5D',
            question_id         : 'EQ5D_Q_1'
        });

    });

    it('should have an id of 1', function () {

        expect(model.get('id')).toEqual(1);

    });

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');

    });

    it('should have a storage property.', function () {

        expect(model.storage).toBeDefined();
        expect(typeof model.storage).toEqual('string');

    });

    it('should have response', function () {

        expect(model.get('response')).toBeDefined();

    });

    it('should have SW_Alias', function () {

        expect(model.get('SW_Alias')).toBeDefined();

    });

    it('should throw an error because invalid attribute type (expects a number)', function () {

        model.set({ id: 'bar' }, { validate: true });

        expect(model.get('id')).toBe(1);
        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected number. Received string.');

    });

    it('should throw an error because invalid attribute type (expects a string)', function () {

        model.set({ response: 1 }, { validate: true });

        expect(model.get('response')).toBe('7');
        expect(model.validationError.message).toBe('Attribute: response. Invalid DataType. Expected string. Received number.');

    });

});
