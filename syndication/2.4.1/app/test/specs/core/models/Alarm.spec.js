import 'test/helpers/SpecHelpers';
//import 'test/helpers/StudyDesign';
//import 'test/helpers/JasmineAsync';
//import 'logpad/Views/ActivationBaseView';
//import 'logpad/routers/ApplicationRouter';
//import * as lStorage from 'core/lStorage';
//import '../../../lib/noty/jquery.noty.PHTMOD';
import 'core/wrapperjs/Wrapper';
import 'core/collections/ActiveAlarms';

(function () {
    xdescribe('Alarm', function () {

        let testAlarmFunc = function (schedule, alarmParams, completedQuestionnaires, parentView, callback) { callback(true);};

        beforeEach(function () {
            LF.Schedule.alarmFunctions.testAlarmFunc = testAlarmFunc;

            spyOn(LF.Wrapper, 'exec').and.callFake(function (params) {
                params.execWhenWrapped();
            });
        });

        afterEach(function () {
            LF.Schedule.alarmFunctions.testAlarmFunc = undefined;
        });

        it('should install the database.', function () {
            LF.SpecHelpers.installDatabase();
        });

        it('should throw an error because time is null or undefined', function () {
            let schedule = new LF.Model.Schedule({
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   1
                    }
                }),
                alarm = new LF.Class.Alarm(schedule);

            expect(function () {
                alarm.evaluateAlarm(schedule);
            }).toThrowError('Missing alarm time: alarm time is null or undefined.');
        });

        it('should throw an error because of incorrect repeat type', function () {
            let schedule = new LF.Model.Schedule({
                    id              : 'test',
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   1,
                        time              : '10:30',
                        repeat            : 'everyday'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule);

            expect(function () {
                alarm.evaluateAlarm(schedule);
            }).toThrowError('Incorrect repeat type: repeat type "everyday" is not supported');
        });

        it('should evaluate alarm and accordingly add the alarm', function (done) {
            let schedule = new LF.Model.Schedule({
                    id                : 'Daily_DiarySchedule_01',
                    target            : {
                        objectType        : 'questionnaire',
                        id                : 'Daily_Diary'
                    },
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   1,
                        time              : '10:30',
                        repeat            : 'daily'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response = false;
            let alarms = new LF.Collection.ActiveAlarms();

            spyOn(LF.Collection.ActiveAlarms.prototype, 'addAlarm').and.callFake(function (params) {
                response = true;
            });

            spyOn(alarm, 'setAlarmDate');

            alarm.evaluateAlarm(schedule, new LF.Collection.LastDiaries());

            LF.SpecHelpers.Jasmine.waitFor('alarm to be evaluated.', function () {
                        return response;
                    }, 5000)
            .then(function () {
                    expect(LF.Collection.ActiveAlarms.prototype.addAlarm).toHaveBeenCalledWith(schedule.get('alarmParams'), 'Daily_DiarySchedule_01');
                    done();
                });

        });

        it('should evaluate alarm and accordingly cancel the alarm because the alarm function calls the callback with false value', function (done) {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_01',
                    target            : {
                        objectType        : 'questionnaire',
                        id                : 'Daily_Diary'
                    },
                    alarmFunction     : 'cancelAlwaysAlarm',
                    alarmParams       : {
                        id                :   1,
                        time              : '10:30',
                        repeat            : 'daily'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response;

            LF.Schedule.alarmFunctions.cancelAlwaysAlarm = function (schedule, alarmParams, completedQuestionnaires, parentView, callback) {
                callback(false);
            };

            spyOn(LF.Collection.ActiveAlarms.prototype, 'cancelAlarm').and.callFake(function (params) {
                response = true;
            });

            spyOn(alarm, 'setAlarmDate');

            alarm.evaluateAlarm(schedule, new LF.Collection.LastDiaries());

            LF.SpecHelpers.Jasmine.waitFor('alarm to be evaluated.', function () {
                return response;
            }, 1000)
                .then(function () {
                    expect(LF.Collection.ActiveAlarms.prototype.cancelAlarm).toHaveBeenCalledWith(schedule.get('alarmParams'));
                    done();
                });

        });

        it('should evaluate the alarm and accordingly cancel the alarm because the alarm is turned off', function (done) {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_01',
                    target            : {
                        objectType        : 'questionnaire',
                        id                : 'Daily_Diary'
                    },
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   1,
                        time              : '10:30',
                        repeat            : 'daily'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response;

            spyOn(LF.Collection.ActiveAlarms.prototype, 'cancelAlarm').and.callFake(function (params) {
                response = true;
            });

            spyOn(alarm, 'setAlarmDate');

            //Turn off the alarm
            alarm.turnOffAlarm = true;

            alarm.evaluateAlarm(schedule, new LF.Collection.LastDiaries());

            LF.SpecHelpers.Jasmine.waitFor('alarm to be evaluated.', function () {
                return response;
            }, 1000)
                .then(function () {
                    expect(LF.Collection.ActiveAlarms.prototype.cancelAlarm).toHaveBeenCalledWith(schedule.get('alarmParams'));
                    done();
                });
        });

        it('should evaluate subject config alarm and accordingly set the subject configured alarm time', function (done) {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_01',
                    target            : {
                        objectType        : 'questionnaire',
                        id                : 'Daily_Diary'
                    },
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   1,
                        time              : '10:30',
                        repeat            : 'daily',
                        subjectConfig : {}
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response,
                subjectAlarm = new LF.Model.SubjectAlarm();

            subjectAlarm.save({id: 1, schedule_id: 'Daily_DiarySchedule_01', time: '15:00'}, {
                onSuccess: function () {
                    alarm.evaluateSubjectConfigAlarm('Daily_DiarySchedule_01', function () {
                        response = true;
                    });
                }
            });

            LF.SpecHelpers.Jasmine.waitFor('subject selected alarm to be evaluated.', function () {
                return response;
            },  1000)
                .then(function () {
                    expect(alarm.alarmParams.time).toEqual('15:00');
                    expect(alarm.turnOffAlarm).toBeFalsy();
                    done();
                });

        });

        it('should evaluate subject config alarm and accordingly turn off the alarm', function (done) {
            let schedule = new LF.Model.Schedule({
                    id              : 'Daily_DiarySchedule_02',
                    target            : {
                        objectType        : 'questionnaire',
                        id                : 'Daily_Diary'
                    },
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   2,
                        time              : '10:30',
                        repeat            : 'daily',
                        subjectConfig : {}
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response,
                subjectAlarm = new LF.Model.SubjectAlarm();

            subjectAlarm.save({id: 2, schedule_id: 'Daily_DiarySchedule_02', time: ''}, {
                onSuccess: function () {
                    alarm.evaluateSubjectConfigAlarm('Daily_DiarySchedule_02', function () {
                        response = true;
                    });
                }
            });
            LF.SpecHelpers.Jasmine.waitFor('subject selected alarm to be evaluated.', () => {return response; }, 1000)
                .then(function () {
                    expect(alarm.alarmParams.time).toEqual('');
                    expect(alarm.turnOffAlarm).toBeTruthy();
                    done();
                });

        });

        it('should evaluate subject config alarm and accordingly turn off the alarm', function (done) {
            let schedule = new LF.Model.Schedule({
                    id              : 'Daily_DiarySchedule_02',
                    target            : {
                        objectType        : 'questionnaire',
                        id                : 'Daily_Diary'
                    },
                    alarmFunction     : 'testAlarmFunc',
                    alarmParams       : {
                        id                :   2,
                        time              : '10:30',
                        repeat            : 'daily',
                        subjectConfig : {}
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response,
                subjectAlarm = new LF.Model.SubjectAlarm();

            subjectAlarm.save({id: 2, schedule_id: 'Daily_DiarySchedule_02', time: ''}, {
                onSuccess: function () {
                    alarm.evaluateSubjectConfigAlarm('Daily_DiarySchedule_02', function () {
                        response = true;
                    });
                }
            });

            LF.SpecHelpers.Jasmine.waitFor('subject selected alarm to be evaluated.', function () {
                        return response;
                    }, 1000)
                .then(function () {
                    expect(alarm.alarmParams.time).toEqual('');
                    expect(alarm.turnOffAlarm).toBeTruthy();
                    done();
                });

        });

        it('should set the daily alarm date as tomorrow because diary already completed', function () {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_03',
                    alarmParams       : {
                        id                :   3,
                        time              : '10:30',
                        repeat            : 'daily'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                lastDiaryModel = new LF.Model.LastDiary({
                    lastStartedDate: new Date()
                }),
                today = new Date(),
                tomorrow = new Date();

            tomorrow.setDate(today.getDate() + 1);

            alarm.setAlarmDate(lastDiaryModel);

            expect(alarm.alarmParams.date.getDate()).toEqual(tomorrow.getDate());

        });

        it('should turn off one time alarm because diary already completed', function () {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_04',
                    alarmParams       : {
                        id                :   4,
                        time              : '10:30'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                lastDiaryModel = new LF.Model.LastDiary({
                    lastStartedDate: new Date()
                });

            alarm.setAlarmDate(lastDiaryModel);

            expect(alarm.turnOffAlarm).toBeTruthy();

        });

        it('should throw an error when setting the alarm date because of incorrect repeat type', function () {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_04',
                    alarmParams       : {
                        id                :   4,
                        time              : '10:30',
                        repeat            : 'everyday'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                lastDiaryModel = new LF.Model.LastDiary({
                    lastStartedDate: new Date()
                });

            expect(function () {
                alarm.setAlarmDate(lastDiaryModel);
            }).toThrowError(`Incorrect repeat type: ${alarm.alarmParams.repeat}`);

        });

        it('should evaluate the alarm and accordingly trigger', function (done) {
            LF.Schedule.alarmFunctions.testAlarmFunc = $.noop;

            spyOn(LF.Schedule.alarmFunctions, 'testAlarmFunc').and.callFake($.noop);

            LF.Wrapper.Utils.alarmTrigger('1', 'Foreground', '{"onAlarm": "testAlarmFunc", "onAlarmParams" : { "param1" : 123 }}');

            LF.SpecHelpers.Jasmine.wait(1000)
                .then(function () {
                    expect(LF.Schedule.alarmFunctions.testAlarmFunc).toHaveBeenCalled();
                    done();
                });
        });

        it('should return the same json parameters', function (done) {

            LF.Schedule.alarmFunctions.testAlarmFunc = $.noop;

            spyOn(LF.Schedule.alarmFunctions, 'testAlarmFunc').and.callFake($.noop);

            LF.Wrapper.Utils.alarmTrigger('1', 'Foreground', '{"onAlarm": "testAlarmFunc", "onAlarmParams" : { "param1" : 123 }}');

            LF.SpecHelpers.Jasmine.wait(250)
                .then(function () {
                    expect(LF.Schedule.alarmFunctions.testAlarmFunc.calls.mostRecent().args[2]).toEqual({ param1: 123 });
                    done();
                });
        });

        it('shouldn\'t return the same json parameters', function (done) {

            let parameters = { asdsadas: { sdsada: 3435 }};

            LF.Schedule.alarmFunctions.testAlarmFunc = $.noop;

            spyOn(LF.Schedule.alarmFunctions, 'testAlarmFunc').and.callFake($.noop);

            LF.Wrapper.Utils.alarmTrigger('1', 'Foreground', '{"onAlarm": "testAlarmFunc", "onAlarmParams" : { "sdsada" : 3435 }}');

            LF.SpecHelpers.Jasmine.wait(250)
                .then(function () {
                    expect(LF.Schedule.alarmFunctions.testAlarmFunc.calls.mostRecent().args[2]).not.toEqual({ param1: 123 });
                    done();
                });
        });

        it('should cancel the alarm', function (done) {
            let schedule = new LF.Model.Schedule({
                    id            : 'Daily_DiarySchedule_01',
                    alarmParams       : {
                        id                :   8,
                        time              : '10:30',
                        repeat            : 'daily'
                    }
                }),
                alarm = new LF.Class.Alarm(schedule),
                response;

            spyOn(LF.Collection.ActiveAlarms.prototype, 'cancelAlarm').and.callFake(function (params) {
                response = true;
            });

            alarm.cancelAlarm();

            LF.SpecHelpers.Jasmine.waitFor('alarm to be canceled.' ,
                function () { return response; }, 1000)
            .then(function () {
                expect(LF.Collection.ActiveAlarms.prototype.cancelAlarm.calls.mostRecent().args[2]).not.toEqual(schedule.get('alarmParams'));
                done();
            });
        });

    });
}).call(this);
