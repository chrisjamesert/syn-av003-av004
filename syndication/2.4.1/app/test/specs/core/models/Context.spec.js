import PatientSession from 'core/classes/PatientSession';
import Context from 'core/models/Context';
import Role from 'core/models/Role';
import Users from 'core/collections/Users';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as helpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US6567').
describe('Context:', () => {
    const origPreferred = LF.Preferred;

    let context;
    helpers.clearCollections(Users);

    beforeEach(() => {
        resetStudyDesign();

        context = new Context();

        LF.Preferred = {
            language: 'en',
            locale: 'US'
        };

        LF.security = new PatientSession();
    });

    afterAll(() => {
        LF.Preferred = origPreferred;
    });

    describe('Role getter', () => {
        it('should return false if no role has been set.', () => {
            expect(context.role).toBe(false);
        });

        it('should return the string id of the current context role', () => {
            const id = 'caregiver';

            context.set('role', new Role({ id }));
            expect(context.role).toBe(id);
        });
    });

    describe('fetchUsers', () => {
        Async.it('should fetch the context users instance', () => {
            const users = new Users([{
                username: 'Dick Grayson',
                role: 'site'
            }]);

            return users.save()
                .then(() => {
                    return context.fetchUsers();
                })
                .then(() => {
                    expect(context.get('users').toJSON()).toEqual(users.toJSON());
                });
        });
    });

    describe('checkUserAuth', () => {
        describe('Checks if the logged in user is still active and logged in then sets context accordingly ->', () => {
            const setupUser = active => {
                const user = context.get('users').add({
                    username: 'Jim Gordon',
                    role: 'site',
                    active
                });

                return user.save()
                    .then(() => user);
            };

            const loginSpy = isAuthenticated => spyOn(LF.security, 'checkLogin').and.callFake(() => Q(isAuthenticated));

            describe('falsy cases:', () => {
                const testFalseCase = ({ userExists = true, active = 1, authenticated = true }) => () => {
                    Async.it('should set to default language and set to default role', () => {
                        return Q().then(() => {
                            return setupUser(active);
                        })
                        .then(user => {
                            return userExists && localStorage.setItem('PHT_User_Login', user.get('id'));
                        })
                        .then(() => {
                            loginSpy(authenticated);
                            return context.checkUserAuth();
                        })
                        .then(() => {
                            expect(context.setContextByRole).toHaveBeenCalledWith();
                            let defaultLanguage = context.get('defaultLanguage').split('-');
                            expect(context.get('language')).toBe(defaultLanguage[0]);
                            expect(context.get('locale')).toBe(defaultLanguage[1]);
                        });
                    });
                };

                beforeEach(() => {
                    spyOn(context, 'setContextByUser').and.callFake(() => Q());
                    spyOn(context, 'setContextByRole').and.callFake(() => Q());
                });

                describe('If there is no user', testFalseCase({ userExists: false }));
                describe('If the user is not active', testFalseCase({ active: 0 }));
                describe('If the user is not authenticated', testFalseCase({ authenticated: false }));
            });

            describe('truthy cases:', () => {
                describe('If the user is active and Authenticated', () => {
                    let contextUser;

                    beforeEach(() => {
                        spyOn(context, 'setContextByUser').and.callFake(() => Q());
                        spyOn(context, 'setContextByRole').and.callFake(() => Q());
                    });

                    Async.it('should set context by that user', () => {
                        return setupUser(1)
                            .tap(user => {
                                loginSpy(true);
                                localStorage.setItem('PHT_User_Login', user.get('id'));
                                return context.checkUserAuth();
                            })
                            .tap(user => {
                                expect(context.setContextByUser).toHaveBeenCalledWith(user);
                            });
                    });
                });
            });

        });
    });

    describe('setContextByUser', () => {
        Async.it('Sets the context language and role to the User\'s language and role', () => {
            const lang = 'en';
            const locale = 'US';
            const user = context.get('users').add({
                id: 1,
                language: `${lang}-${locale}`,
                username: 'Oswald Cobblepot',
                role: 'site'
            });

            return user.save()
                .then(() => context.setContextByUser(user))
                .then(() => {
                    expect(context.get('user').toJSON()).toEqual(user.toJSON());
                    expect(context.get('language')).toBe(lang);
                    expect(context.get('locale')).toBe(locale);
                    expect(LF.Preferred.language).toBe(lang);
                    expect(LF.Preferred.locale).toBe(locale);
                });
        });
    });

    describe('setContextByRole', () => {
        Async.it('should do nothing if the role is not found', () => {
            LF.StudyDesign.roles.reset();

            return context.setContextByRole('Batman')
                .then(() => {
                    expect(context.role).toBe(false);
                });
        });

        Async.it('should set the role and load schedules', () => {
            LF.StudyDesign.roles.reset([{
                id: 'Nightwing'
            }]);

            spyOn(context, 'loadContextSchedules').and.callFake(() => Q());

            return context.setContextByRole('Nightwing')
                .then(() => {
                    expect(context.role).toBe('Nightwing');
                    expect(context.loadContextSchedules).toHaveBeenCalled();
                });
        });
    });
});
