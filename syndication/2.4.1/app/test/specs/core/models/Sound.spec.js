import Sound from 'core/classes/Sound';

describe('Sound', function () {

    let sound;

    beforeEach(function () {
        sound = new Sound();
    });

    it('should be defined.', function () {

        expect(sound).toBeDefined();

    });

    describe('method:play', function () {

        it('should have a play method.', function () {

            expect(sound.play).toBeDefined();

        });

    });

    describe('method:mute', function () {

        it('should have a mute method.', function () {

            expect(sound.mute).toBeDefined();

        });

        it('should mute.', function () {
            // unimplemented
        });

    });

    describe('method:unmute', function () {

        it('should have a unmute method.', function () {
            expect(sound.unmute).toBeDefined();
        });

    });

});
