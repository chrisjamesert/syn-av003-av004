import Screens from 'core/collections/Screens';
import * as helpers from 'test/helpers/SpecHelpers';

describe('Screens', function () {

    let collection;

    beforeEach(function () {

        helpers.installDatabase();

        collection = new Screens([
            {
                id          : 'SF36_S_1',
                questions   : [
                    {
                        id          : 'SF36_Q_1',
                        type        : 'RadioButton',
                        mandatory   : true
                    }
                ]
            }, {
                id          : 'SF36_S_2',
                questions   : [
                    {
                        id          : 'SF36_Q_2',
                        type        : 'RadioButton',
                        mandatory   : true
                    }
                ]
            }
        ]);

    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should NOT have a table', function () {

        expect(collection.table).not.toBeDefined();

    });

    it('should return 1 model with an id of SF36_S_1', function () {

        let model = collection.match({
                id : 'SF36_S_1'
            });

        expect(model.length).toEqual(1);
        expect(model[0].get('id')).toEqual('SF36_S_1');

    });

    it('should return 2 models with className\'s of \'screen\'.', function () {

        let models = collection.match({
                className : 'screen'
            });

        expect(models.length).toEqual(2);
        expect(models[0].get('className')).toEqual('screen');
        expect(models[1].get('className')).toEqual('screen');

    });

    it('should return 0 models.', function () {

        let models = collection.match({
                className : 'SF36_S_3'
            });

        expect(models.length).toBeFalsy();

    });

    // NOTE: Validation is currently disabled, so this test will fail -- bmj
    it('should throw a missing arguments error.', function () {

        expect(function () {
            collection.match();
        }).toThrowError('Invalid number of arguments.');

    });

});
