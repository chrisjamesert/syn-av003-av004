import Schedule from 'core/models/Schedule';
import SubjectAlarms from 'core/collections/SubjectAlarms';

import * as specHelpers from 'test/helpers/SpecHelpers';

describe('SubjectAlarms', () => {
    let collection = new SubjectAlarms(),
        scheduleModel;

    beforeEach(() => {
        scheduleModel = new Schedule({
            id: 'DailySchedule',
            target: {
                objectType: 'questionnaire',
                id: 'LogPad'
            },
            scheduleFunction: 'checkRepeatByDateAvailability',
            phase: [
                'SCREENING'
            ],
            scheduleParams: {
                startAvailability: '01:00',
                endAvailability: '19:00'
            },
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 1,
                time: '9:00',
                repeat: 'daily',
                subjectConfig: {
                    minAlarmTime: '9:00',
                    maxAlarmTime: '15:00',
                    alarmRangeInterval: 30,
                    alarmOffSubject: true
                }
            }
        });
    });

    afterEach(() => localStorage.clear());

    afterAll(done => specHelpers.uninstallDatabase().done(done));

    // This isn't really a test case...
    it('should create the Subjects storage object and save the record.', (done) => {
        specHelpers.createSubject();
        specHelpers.saveSubject()
            .catch(e => fail(e))
            .done(done);
    });

    it('should throw error if id is missing when setting Subject Alarm', () => {
        expect(() => {
            collection.setSubjectAlarm({
                schedule        : scheduleModel,
                time            : '13:00'
            },$.noop);
        }).toThrow(new Error('Missing Argument: alarm id is null or undefined.'));
    });

    xit('should throw error if schedule is missing when setting Subject Alarm', function () {
        expect(function () {
            collection.setSubjectAlarm({
                id      : 1,
                time    : '13:00'
            }, $.noop);
        }).toThrow({
            message : 'Missing Argument: schedule id is null or undefined.'
        });
    });

    xit('should throw error if time is missing when setting Subject Alarm', function () {
        expect(function () {
            collection.setSubjectAlarm({
                id          : 1,
                schedule : scheduleModel
            }, $.noop);
        }).toThrow({
            message : 'Missing Argument: alarm time is null or undefined.'
        });
    });

    xit('should set Subject Alarm when required parameters are passed in', function () {
        let response = false;

        collection.setSubjectAlarm({
            id          : 1,
            schedule    : scheduleModel,
            time        : '13:00'
        }, function () {
            collection.fetch({
                onSuccess : function () {
                    response = true;
                }
            });
        });

        waitsFor(function () {
            return response;
        }, 'the Subject Alarms collection to be fetched', 1000);

        runs(function () {
            expect(collection.length).toEqual(1);
        });
    });

    xit('should throw error if schedule id is missing when removing Subject Alarm', function () {
        expect(function () {
            collection.removeSubjectAlarm();
        }).toThrow({
            message : 'Missing Argument: schedule id is null or undefined.'
        });
    });

    xit('should remove Subject Alarm', function () {
        let response = false;

        collection.removeSubjectAlarm('DailySchedule', function () {
            collection.fetch({
                onSuccess : function () {
                    response = true;
                }
            });
        });

        waitsFor(function () {
            return response;
        }, 'the Subject Alarms collection to be fetched', 1000);

        runs(function () {
            expect(collection.length).toEqual(0);
        });
    });

    xit('should log ERROR within onError backbone fetch transaction function for bad sql statement in setSubjectAlarm collection function.', function () {
        let response = false;

        collection.storage = 'WrongStorage';

        collection.setSubjectAlarm({
            id          : 1,
            schedule    : scheduleModel,
            time        : '13:00'
        }, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the Subject Alarm to be set', 1000);

        runs(function () {
            expect(collection.logger.loggingEvents[4].message === '[object SQLError]').toBeTruthy();
        });
    });

    xit('should log ERROR within onError backbone fetch transaction function for bad sql statement in removeSubjectAlarm collection function.', function () {
        let response = false;

        collection.removeSubjectAlarm('DailySchedule', function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the Subject Alarm to be set', 1000);

        runs(function () {
            expect(collection.logger.loggingEvents[5].message === '[object SQLError]').toBeTruthy();
        });
    });

});
