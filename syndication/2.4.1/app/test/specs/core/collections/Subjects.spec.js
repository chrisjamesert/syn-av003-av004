import Subjects from 'core/collections/Subjects';

describe('Subjects', function () {

    let collection;

    beforeEach(function () {

        collection = new Subjects([{ id: 1 }]);

    });

    afterEach(() => localStorage.clear());

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should have a storage property', function () {

        expect(collection.storage).toBeDefined();

    });

});
