import * as helpers from 'test/helpers/SpecHelpers';
import * as studydesign from 'test/helpers/StudyDesign';
import Languages from 'core/collections/Languages';

describe('Languages', () => {
    let collection,
        prefLanguage = LF.Preferred.language,
        prefLocale = LF.Preferred.locale,
        defaultLanguage = LF.StudyDesign.defaultLanguage,
        defaultLocale = LF.StudyDesign.defaultLocale;

    beforeEach(() => {
        LF.StudyDesign.defaultLanguage = 'en';
        LF.StudyDesign.defaultLocale = 'US';

        collection = new Languages([
            {
                namespace: 'CORE',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY: 'Core Default',
                    STUDY: 'Core Default',
                    CORE: 'Core Default',
                    DIARY2: 'Core NoTrans',
                    STUDY2: 'Core NoTrans',
                    CORE2: 'Core NoTrans'
                },
                dates: {
                    CORE_DATE: 'Core Default Date'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Core Default Date Configs'
                }
            },
            {
                namespace: 'CORE',
                language: 'vi',
                locale: 'VI',
                direction: 'ltr',
                resources: {
                    DIARY: 'Core Pref',
                    STUDY: 'Core Pref',
                    CORE: 'Core Pref'
                },
                dates: {
                    CORE_DATE: 'Core Pref Date'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Core Pref Date Configs'
                }
            },
            {
                namespace: 'CORE',
                language: 'ar',
                locale: 'EG',
                direction: 'rtl',
                resources: {}
            },
            {
                namespace: 'STUDY',
                language: 'vi',
                locale: 'VI',
                direction: 'ltr',
                resources: {
                    DIARY: 'Study Pref',
                    STUDY: 'Study Pref'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Study Pref Date Configs'
                }
            },
            {
                namespace: 'STUDY',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY: 'Study Default',
                    STUDY: 'Study Default',
                    DIARY2: 'Study NoTrans',
                    STUDY2: 'Study NoTrans'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Study Default Date Configs'
                }
            },
            {
                namespace: 'DIARY',
                language: 'vi',
                locale: 'VI',
                direction: 'ltr',
                resources: {
                    DIARY: 'Diary Pref'
                }
            },
            {
                namespace: 'DIARY',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY: 'Diary Default',
                    DIARY2: 'Diary NoTrans'
                }
            },
            {
                namespace: 'DIARY',
                language: 'en',
                locale: 'UK',
                direction: 'ltr',
                resources: {
                    DIARY_TEST: 'Diary with same Language'
                }
            },
            {
                namespace: 'DIARY',
                language: 'fr',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY_TEST: 'Diary with same Locale'
                }
            }
        ]);
    });

    afterEach(() => {
        LF.Preferred.language = prefLanguage;
        LF.Preferred.locale = prefLocale;
        LF.StudyDesign.defaultLanguage = defaultLanguage;
        LF.StudyDesign.defaultLocale = defaultLocale;
        localStorage.clear();
    });

    it('should have a model', () => {
        expect(collection.model).toBeDefined();
    });

    describe('Method fetch()', () => {
        Async.it('(Core Default). Core namespace and default language and locale', () => {
            let arr = [];

            return collection
                .fetch('CORE', {})
                .then((result) => {
                    arr.push(result);

                    return collection.fetch('CORE', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr.push(result);

                    return collection.fetch('CORE', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr.push(result);

                    expect(arr[0]).toEqual('Core Default');
                    expect(arr[1]).toEqual('Core Default');
                    expect(arr[2]).toEqual('Core Default');
                });
        });

        Async.it('(Study Default). Study namespace and default language and locale', () => {
            let arr = [];

            return collection
                .fetch('STUDY', {})
                .then((result) => {
                    arr[0] = result;

                    return collection.fetch('STUDY', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[1] = result;

                    return collection.fetch('STUDY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[2] = result;

                    expect(arr[0]).toEqual('Study Default');
                    expect(arr[1]).toEqual('Study Default');
                    expect(arr[2]).toEqual('Study Default');
                });
        });

        Async.it('(Diary Default). Diary namespace and default language and locale', () => {
            return collection
                    .fetch('DIARY', {namespace: 'DIARY'})
                    .then((result) => {
                        expect(result).toEqual('Diary Default');
                    });
        });
        Async.it('(Core Pref). Core namespace and preferred language and locale', () => {
            let arr = [];

            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .fetch('CORE', {})
                .then((result) => {
                    arr[0] = result;

                    return collection.fetch('CORE', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[1] = result;

                    return collection.fetch('CORE', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[2] = result;

                    expect(arr[0]).toEqual('Core Pref');
                    expect(arr[1]).toEqual('Core Pref');
                    expect(arr[2]).toEqual('Core Pref');
                });
        });

        Async.it('(Core NoTrans). Missing Translation. Core namespace and default language and locale', () => {
            let arr = [];

            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .fetch('CORE2', {})
                .then((result) => {
                    arr[0] = result;

                    return collection.fetch('CORE2', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[1] = result;

                    return collection.fetch('CORE2', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[2] = result;

                    expect(arr[0]).toEqual('Core NoTrans');
                    expect(arr[1]).toEqual('Core NoTrans');
                    expect(arr[2]).toEqual('Core NoTrans');
                });
        });

        Async.it('(Study Pref). Study namespace and preferred language and locale', () => {
            let arr = [];

            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .fetch('STUDY', {})
                .then((result) => {
                    arr[0] = result;

                    return collection.fetch('STUDY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[1] = result;

                    expect(arr[0]).toEqual('Study Pref');
                    expect(arr[1]).toEqual('Study Pref');
                });
        });

        Async.it('(Study NoTrans). Missing Translation. Study namespace and default language and locale', () => {
            let arr = [], Q;

            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .fetch('STUDY2', {})
                .then((result) => {
                    arr[0] = result;

                    return collection.fetch('STUDY2', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[1] = result;

                    expect(arr[0]).toEqual('Study NoTrans');
                    expect(arr[1]).toEqual('Study NoTrans');
                });
        });

        Async.it('(Diary Pref). Diary namespace and preferred language and locale', () => {
            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .fetch('DIARY', {namespace: 'DIARY'})
                .then((result) => {
                    expect(result).toEqual('Diary Pref');
                });
        });

        Async.it('(Diary NoTrans). Missing Translation. Diary namespace and default language and locale', () => {
            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .fetch('DIARY2', {namespace: 'DIARY'})
                .then((result) => {
                    expect(result).toEqual('Diary NoTrans');
                });
        });

        Async.it('(Diary with same Locale). Diary Locale is same as default Locale', () => {
            let arr = [];

            LF.Preferred.language = 'fr';
            LF.Preferred.locale = 'US';

            return collection
                .fetch('DIARY_TEST', {namespace: 'DIARY'})
                .then((result) => {
                    arr[0] = result;
                    return collection.fetch('DIARY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[1] = result;

                    expect(arr[0]).toEqual('Diary with same Locale');
                    expect(arr[1]).toEqual('Diary Default');
                });
        });

        Async.it('(Diary with same Language). Diary Language is same as default Language', () => {
            let arr = [];

            LF.Preferred.language = 'en';
            LF.Preferred.locale = 'UK';

            return collection
                .fetch('DIARY_TEST', {namespace: 'DIARY'})
                .then((result) => {
                    arr[0] = result;
                    return collection.fetch('DIARY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[1] = result;

                    expect(arr[0]).toEqual('Diary with same Language');
                    expect(arr[1]).toEqual('Diary Default');
                });
        });

        Async.it('(Unknown locale). Reverting to Default language and locale', () => {
            let arr = [];

            LF.Preferred.language = 'es';
            LF.Preferred.locale = 'ES';

            return collection.fetch('CORE', {})
                .then((result) => {
                    arr[0] = result;
                    return collection.fetch('STUDY', {});
                })
                .then((result) => {
                    arr[1] = result;
                    return collection.fetch('CORE', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[2] = result;
                    return collection.fetch('STUDY', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[3] = result;
                    return collection.fetch('CORE', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[4] = result;
                    return collection.fetch('STUDY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[5] = result;
                    return collection.fetch('DIARY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[6] = result;

                    expect(arr[0]).toEqual('Core Default');
                    expect(arr[1]).toEqual('Study Default');
                    expect(arr[2]).toEqual('Core Default');
                    expect(arr[3]).toEqual('Study Default');
                    expect(arr[4]).toEqual('Core Default');
                    expect(arr[5]).toEqual('Study Default');
                    expect(arr[6]).toEqual('Diary Default');
                });
        });

        Async.it('(No preferred locale). Reverting to Default language and locale', () => {
            let arr = [];

            LF.Preferred.language = null;
            LF.Preferred.locale = null;

            return collection
                .fetch('CORE', {})
                .then((result) => {
                    arr[0] = result;

                    return collection.fetch('STUDY', {});
                })
                .then((result) => {
                    arr[1] = result;
                    return collection.fetch('CORE', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[2] = result;
                    return collection.fetch('STUDY', {namespace: 'STUDY'});
                })
                .then((result) => {
                    arr[3] = result;
                    return collection.fetch('CORE', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[4] = result;
                    return collection.fetch('STUDY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[5] = result;
                    return collection.fetch('DIARY', {namespace: 'DIARY'});
                })
                .then((result) => {
                    arr[6] = result;

                    expect(arr[0]).toEqual('Core Default');
                    expect(arr[1]).toEqual('Study Default');
                    expect(arr[2]).toEqual('Core Default');
                    expect(arr[3]).toEqual('Study Default');
                    expect(arr[4]).toEqual('Core Default');
                    expect(arr[5]).toEqual('Study Default');
                    expect(arr[6]).toEqual('Diary Default');
                });
        });

        describe('Error handling', () => {
            Async.it('should display the resource key if no string is found.', () => {
                return collection
                    .fetch('INVALID_PASSWORD', {namespace: 'DERP'})
                    .then((result) => {
                        expect(result).toEqual('{{ INVALID_PASSWORD }}');
                    });
            });

            it('should not find a Language model.', () => {
                LF.Preferred.language = 'es';
                LF.Preferred.locale = 'US';

                expect(collection.find()).toBeFalsy();
            });

            Async.it('should not find a Language model.', () => {
                LF.Preferred.language = 'en';
                LF.Preferred.locale = 'US';

                return collection
                    .fetch('TEST', {
                        namespace: 'STUDY',
                        language: '',
                        locale: ''
                    })
                    .then((result) => {
                        expect(result).toEqual('{{ TEST }}');
                    });
            });

            it('should throw an error. Missing argument key.', () => {
                expect(() => {
                    return collection.fetch();
                }).toThrow(new Error('Error: Missing argument.'));
            });

            it('should throw an error. Invalid argument type.', () => {
                expect(() => {
                    return collection.fetch(1);
                }).toThrow(new Error('Error: Invalid argument type.'));
            });
        });
    });

    describe('Method display()', () => {
        Async.it('should return a string when the argument is a string.', () => {
            return collection.display('TEST', (result) => {
                expect(typeof result).toEqual('string');
                expect(result).toEqual('{{ TEST }}');
            });
        });

        Async.it('should return an object the argument is an object.', () => {
            return collection
                .display({ key: 'TEST' },
                    (result) => {
                        expect(typeof result).toEqual('object');
                        expect(result).toEqual({ key: '{{ TEST }}'} );
                    }
                );
        });

        Async.it('should invoke callback even when argument is an empty object.', () => {
            let response;

            return collection.display({}, (result) => {
                response = 'callback invoked';
                expect(response).toEqual('callback invoked');
            });
        });

        Async.it('should resolve the string within the object when passed as an object.', () => {
            return collection.display(
                {
                    key1: {
                        key: 'TEST',
                        namespace: 'SOME_NAMESPACE'
                    }
                },
                    (result) => {
                        expect(result).toEqual({key1: '{{ TEST }}'});
                    }
                );
        });

        Async.it('should return an embedded array when object argument has array.', () => {
            return collection
                .display({
                    key1: [
                        'TEST1',
                        { key: 'TEST2', namespace: 'SOME_NAMESPACE' }
                    ]
                })
                .then((result) => {
                    expect(result).toEqual({
                        key1: [
                            '{{ TEST1 }}',
                            '{{ TEST2 }}'
                        ]
                    });
                });
        });

        Async.it('should return correct string with CORE and DIARY namespace combination.', () => {
            return collection.display(['CORE', 'DIARY'], undefined, {namespace: 'DIARY'})
                .then((result) => {
                    expect(result).toEqual(['Core Default', 'Diary Default']);
                });
        });

        Async.it('should return correct string with STUDY and DIARY namespace combination.', () => {
            return collection.display({str1: 'STUDY', str2: 'DIARY'}, undefined, {namespace: 'DIARY'})
                .then((result) => {
                    expect(result).toEqual({
                        str1: 'Study Default',
                        str2: 'Diary Default'
                    });
                });
        });

        Async.it('should return correct string with STUDY and CORE namespace combination.', () => {
            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            return collection
                .display({
                    str1: {key: 'CORE', namespace: 'CORE'},
                    str2: 'STUDY'
                }, undefined, {namespace: 'STUDY'})
                .then((result) => {
                    expect(result).toEqual({
                        str1: 'Core Pref',
                        str2: 'Study Pref'
                    });
                });
        });
    });

    describe('Method dates()', () => {
        it('(Core Default Date). Default language and locale', () => {
            let results = collection.dates({dates: {}});

            expect(results.CORE_DATE).toEqual('Core Default Date');
        });

        it('(Core Pref Date).', () => {
            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            let results = collection.dates({dates: {}});

            expect(results.CORE_DATE).toEqual('Core Pref Date');
        });

        it('(Study Default Date Configs). Default language and locale', () => {
            let results = collection.dates({dateConfigs: {}});

            expect(results.STUDY_DATE_CONFIGS).toEqual('Study Default Date Configs');
        });

        it('(Study Pref Date Configs).', () => {
            LF.Preferred.language = 'vi';
            LF.Preferred.locale = 'VI';

            let results = collection.dates({dateConfigs: {}});

            expect(results.STUDY_DATE_CONFIGS).toEqual('Study Pref Date Configs');
        });

        it('(Core Default Date). Reverting to default language and locale', () => {
            LF.Preferred.language = 'es';
            LF.Preferred.locale = 'ES';

            let results = collection.dates({dates: {}});

            expect(results.CORE_DATE).toEqual('Core Default Date');
        });

        it('(Core Default Date). Reverting to default locale and namespace', () => {
            let results = collection.dates({namespace: 'STUDY', dates: {}});

            expect(results.CORE_DATE).toEqual('Core Default Date');
        });

        it('(Core Default Date). No namespace. Reverting to default locale and namespace', () => {
            let results = collection.dates({namespace: null, dates: {}});

            expect(results.CORE_DATE).toEqual('Core Default Date');
        });
    });

    describe('Method getLanguageDirection()', () => {
        it('should get direction of "ltr" when LF.Preferred is English', () => {
            LF.Preferred = {
                language: 'en',
                locale: 'US'
            };

            expect(collection.getLanguageDirection()).toEqual('ltr');
        });

        it('should get direction of "rtl" when LF.Preferred is a RTL language such as Arabic.', () => {
            LF.Preferred = {
                language: 'ar',
                locale: 'EG'
            };

            expect(collection.getLanguageDirection()).toEqual('rtl');
        });
    });
});
