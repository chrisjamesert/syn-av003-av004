import WebService from 'core/classes/WebService';
import Transmissions from 'core/collections/Transmissions';

import COOL from 'core/COOL';
import Transmit from 'core/transmit';
import * as specHelpers from 'test/helpers/SpecHelpers';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

describe('Transmissions', () => {
    let now = Date.now(),
        oldTransmit,
        transmissions;

    Async.beforeEach(() => {
        transmissions = new Transmissions();
        return transmissions.clear()
        .then(() => {
            LF.webService = new WebService();

            oldTransmit = COOL.getService('Transmit');

            Transmit.testAction = function (transmissionItem) {
                return transmissionItem.destroy(transmissionItem.get('id'))
                .catch(e => fail(e));
            };

            Transmit.testError = function (transmissionItem) {
                transmissionItem.destroy(transmissionItem.get('id'));
                return Q.reject(new Error('Error.'));
            };

            Transmit.testCallback = function (transmissionItem, callback) {
                transmissionItem.destroy(transmissionItem.get('id'));

                callback();
            };

            COOL.service('Transmit', Transmit);

            transmissions = new Transmissions([{
                id: 1,
                method: 'testAction',
                params: '1',
                created: now
            }, {
                id: 2,
                method: 'testAction',
                params: '2',
                created: now
            }, {
                id: 3,
                method: 'transmitNothing',
                params: '3',
                created: now
            }]);
        });
    });

    afterEach(() => {
        localStorage.clear();
        LF.webService = undefined;

        COOL.service('Transmit', oldTransmit);
        oldTransmit = null;
    });

    describe('method:save', () => {
        Async.it('should save all the Transmission records in the database.', () => {
            let origCount;
            return transmissions.count()
            .then((res) => {
                origCount = res;
                return transmissions.save();
            })
            .then(() => {
                return transmissions.count();
            })
            .then((res) => {
                expect(res).toBe(3);
            });
        });
    });

    TRACE_MATRIX('DE22615')
    .describe('method:execute', () => {
        let col;

        beforeEach(() => col = new Transmissions());

        Async.it('should not execute the transmission. No transmissions pulled from the queue.', () => {
            return col.execute(0)
            .catch(({ err }) => {
                expect(err).toEqual(new Error('Transmission queue is empty.'));
            });
        });

        Async.it('shouldn\'t execute a transmission. Action doesn\'t exist.', () => {
            col.add({
                id: 3,
                method: 'transmitNothing',
                params: '3',
                created: now
            });

            return col.execute(0)
            .catch(({ err }) => {
                expect(err).toEqual(new Error('Nonexistent transmission type.'));
            });
        });

        Async.it('should not execute the transmission.  Non-existent transmission.', () => {
            col.add({
                id: 3,
                method: 'transmitNothing',
                params: '3',
                created: now
            });

            return col.pullQueue()
            .then(() => col.execute(3))
            .catch(({ err }) => {
                expect(err).toEqual(new Error('Nonexistent transmission record.'));
            });
        });

        Async.it('should invoke the provided callback function.', () => {
            col.add({
                id: 4,
                method: 'testCallback',
                params: '1',
                created: now
            });

            return Q.Promise((resolve) => {
                // No expected results other than resolve be called.
                col.execute(0, resolve);
            })
            .catch(() => fail('Failed to invoke callback function.'));
        });

        Async.it('should invoke the transmit methods callback function and resolve the returned promise.', () => {
            col.add({
                id: 4,
                method: 'testCallback',
                params: '1',
                created: now
            });

            let request = col.execute(0);

            expect(request).toBePromise();

            // Again we don't have any expected results. We only care that the promise is resolved.
            return request.catch(() => fail('Failed to invoke callback method.'));
        });

        Async.it('should execute a transmission and trigger TRANSMISSION:Failed.', () => {
            col.add({
                id: 1,
                method: 'testError',
                params: '1',
                created: now
            });

            spyOn(ELF, 'trigger').and.stub();

            return col.execute(0)
            .catch(({ err }) => {
                expect(ELF.trigger).toHaveBeenCalledWith('TRANSMISSION:Failed', jasmine.any(Object), col);
            });
        });
    });

    describe('method:destroy', () => {
        let col;

        beforeEach(() => col = new Transmissions());

        it('it shouldn\'t delete the transmission. No transmissions pulled from the queue.', () => {
            expect(() => {
                col.destroy(0, $.noop);
            }).toThrow(new Error('Cannot destroy a nonexistent record.'));
        });

        it('should throw an error.  No transmission specified.', () => {
            expect(() => {
                col.destroy();
            }).toThrow(new Error('Missing Argument: id is null or undefined.'));
        });

        Async.it('should remove one transmission from the queue.', () => {
            let origSize;

            return transmissions.save()
            .then(() => {
                return col.pullQueue();
            })
            .then(() => {
                expect(col.size()).toEqual(3);
            })
            .then(() => {
                return col.destroy(3);
            })
            .then(() => {
                expect(col.size()).toEqual(2);
            });
        });

        it('should throw an error upon attempted removal. Non-existent Transmission.', () => {
            expect(() => {
                col.destroy(3, $.noop);
            }).toThrow(new Error('Cannot destroy a nonexistent record.'));
        });
    });

    describe('method:pullQueue', () => {
        let col;

        beforeEach(() => col = new Transmissions());

        Async.it('should pull all the transmissions from the database(collection).', () => {
            let col = new Transmissions();

            return transmissions.save()
            .then(() => {
                return col.pullQueue();
            })
            .then(() => {
                expect(col.size()).toEqual(3);
            });
        });

        Async.it('should pull the queue and have no transmissions.', () => {
            return transmissions.save()
            .then(() => {
                return col.clear();
            })
            .then(() => col.pullQueue())
            .then(() => {
                expect(col.size()).toEqual(0);
            });
        });
    });

    TRACE_MATRIX('DE22615')
    .describe('method:executeAll', () => {
        Async.it('should execute every transaction and invoke the callback function.', () => {
            let col = new Transmissions([{
                method: 'testAction',
                params: '1',
                created: now
            }, {
                method: 'testAction',
                params: '2',
                created: now
            }]);

            return Q.Promise((resolve) => {
                col.save()
                .then(() => col.executeAll(() => {
                    expect(col.size()).toBe(0);

                    resolve();
                }))
                .catch(e => fail(e));
            });
        });

        Async.it('should execute every transaction.', () => {
            let col = new Transmissions([{
                method: 'testAction',
                params: '1',
                created: now
            }, {
                method: 'testAction',
                params: '2',
                created: now
            }]);

            return col.save()
            .then(() => col.executeAll())
            .then(() => {
                expect(col.size()).toBe(0);
            })
            .catch(e => fail(e));
        });

        Async.it('should execute every transaction and trigger TRANSMISSION:Failed (default onError behavior).', () => {
            let col = new Transmissions([{
                method: 'testAction',
                params: '1',
                created: now
            }, {
                method: 'testError',
                params: '2',
                created: now
            }]);

            spyOn(ELF, 'trigger').and.callThrough();

            return col.save()
            .then(() => col.executeAll())
            .then(() => {
                expect(ELF.trigger).toHaveBeenCalledWith('TRANSMISSION:Failed', jasmine.any(Object), col);
                expect(col.size()).toBe(0);
            });
        });

        Async.it('should execute every transaction and trigger TRANSMISSION:Failed (preventDefault flag: reject on error behavior).', () => {
            let col = new Transmissions([{
                method: 'testAction',
                params: '1',
                created: now
            }, {
                method: 'testError',
                params: '2',
                created: now
            }]);

            spyOn(Logger.prototype, 'error');
            spyOn(ELF, 'trigger').and.resolve({ preventDefault: true });

            return col.save()
            .then(() => col.executeAll())
            .catch((e) => {
                expect(Logger.prototype.error).toHaveBeenCalled();
                expect(ELF.trigger).toHaveBeenCalledWith('TRANSMISSION:Failed', jasmine.any(Object), col);
                expect(col.size()).toBe(0);
            });
        });

        Async.it('should execute every transaction and trigger EXECUTEALL:Failed event.', () => {
            let col = new Transmissions([{
                method: 'testAction',
                params: '1',
                created: now
            }, {
                method: 'testError',
                params: '2',
                created: now
            }]);

            spyOn(Logger.prototype, 'error');
            spyOn(ELF, 'trigger').and.resolve({ preventDefault: true });

            return col.save()
            .then(() => col.executeAll())
            .catch((e) => {
                expect(Logger.prototype.error).toHaveBeenCalled();
                expect(ELF.trigger).toHaveBeenCalledWith('EXECUTEALL:Failed', jasmine.any(Object), col);
            });
        });

        Async.it('should execute every transaction and trigger EXECUTEALL:Failed event (callback).', () => {
            let col = new Transmissions([{
                method: 'testAction',
                params: '1',
                created: now
            }, {
                method: 'testError',
                params: '2',
                created: now
            }]);

            spyOn(Logger.prototype, 'error');
            spyOn(ELF, 'trigger').and.resolve({ preventDefault: true });

            return Q.Promise((resolve) => {
                col.save()
                .then(() => col.executeAll(({ errorStack }) => {
                    expect(errorStack.length).toBe(1);

                    resolve();
                }))
                .done();
            });
        });
    });
});
