import EULAView from 'core/views/EULAView';
import EULA from 'core/classes/EULA';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import EULAViewTests from './EULAView.specBase';

class EULAViewSuite extends EULAViewTests {
    beforeEach () {
        this.view = new EULAView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/end-user-license.ejs');

        return super.beforeEach();
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }
}

describe('EULAView', () => {
    let suite = new EULAViewSuite();

    suite.executeAll({
        id: 'end-user-license-view',
        template: '#end-user-license-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});