import SecretQuestionBaseView from 'core/views/SecretQuestionBaseView';
import User from 'core/models/User';
import Subject from 'core/models/Subject';

import SecretQuestionBaseViewSpec from './SecretQuestionBaseView.specBase';

class SecretQuestionBaseViewSuite extends SecretQuestionBaseViewSpec {
    beforeEach () {
        let user = new User({ username: 'System Administrator' });
        let subject = new Subject({
            device_id: hex_sha512('123456'),
            initials: 'bc',
            log_level: 'ERROR',
            id: 1,
            phase: 10,
            phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
            phaseTriggered: 'false',
            secret_answer: hex_sha512('apple'),
            secret_question: '0',
            site_code: '267',
            subject_id: '12345',
            subject_password: hex_sha512('applekrpt.39ed120a0981231'),
            service_password: hex_sha512('applekrpt.39ed120a0981231'),
            subject_number: '54321',
            subject_active: 1,
            krpt: 'krpt.39ed120a0981231',
            enrollmentDate: '2013-10-05T08:15:30-05:00',
            activationDate: '2013-10-05T08:15:30-05:00'
        });
        let template = `
        <script id="secret-question-base-tpl" type="text/template">
            <form>
                <div class="form-group">
                    <input id="txtNewPassword" type="text />
                </div>
                <div class="form-group">
                    <input id="txtConfirmPassword" type="text" />
                </div>
                <button id="submit">Submit</button>
            </form>
        </script>`;

        $('body').append(template);

        LF.security = {
            getUser: () => {
                return Q(user);
            }
        };

        this.view = new SecretQuestionBaseView({ subject });
        this.view.template = '#secret-question-base-tpl';

        return super.beforeEach();
    }

    afterEach () {
        $('#secret-question-base-tpl').remove();

        return super.afterEach();
    }
}

describe('SecretQuestionBaseView', () => {
    let suite = new SecretQuestionBaseViewSuite();

    suite.executeAll({
        id: 'page',
        tagName: 'div',
        template: '#secret-question-base-tpl',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // TODO: This test became unstable.
            'testHideKeyboardOnEnter'
        ]
    });

    suite.testCreateTransmission();
});
