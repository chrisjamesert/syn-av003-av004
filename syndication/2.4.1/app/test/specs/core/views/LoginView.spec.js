import * as helpers from 'core/Helpers';
import Data from 'core/Data';
import LoginView from 'core/views/LoginView';
import Users from 'core/collections/Users';
import Session from 'core/classes/Session';
import CoreLoginViewSuite from './LoginView.specBase';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';

class CoreLoginTests extends CoreLoginViewSuite {
    beforeEach () {
        let template = `<div id="context-tpl">
            <input id="password" name="password" type="password" placeholder="{{ password }}..."/>
            <button id="login" type="submit" disabled> Login </button>
        </div>`;

        CurrentContext.init();        
        $('body').append(template);
        resetStudyDesign();
        this.view = new LoginView();
        LF.security = new Session();

        return specHelpers.initializeContent()
        .then(() => this.view.resolve())
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        $('#context-tpl').remove();
        return super.afterEach();
    }
}

TRACE_MATRIX('US7853').
describe('LoginView', () => {
    let suite = new CoreLoginTests();
    suite.testLoginSync();
});
