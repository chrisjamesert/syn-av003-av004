import EULAView from 'core/views/EULAView';
import EULA from 'core/classes/EULA';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import PageViewTests from './PageView.specBase';

export default class EULAViewTests extends PageViewTests {
    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(EULAViewTests.prototype);

        this.annotateAll(methods, context);
    }

    testNext () {
        describe('method:next', () => {
            it('should accept the EULA.', () => {
                spyOn(EULA, 'accept').and.stub();

                this.view.next();

                expect(EULA.accept).toHaveBeenCalled();
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            it('should decline the EULA.', () => {
                spyOn(EULA, 'decline').and.stub();

                this.view.back();

                expect(EULA.decline).toHaveBeenCalled();
            });
        });
    }
}