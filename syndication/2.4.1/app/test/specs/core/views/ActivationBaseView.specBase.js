import * as helpers from 'core/Helpers';
import 'test/helpers/StudyDesign';
import Data from 'core/Data';
import Role from 'core/models/Role';
import Roles from 'core/collections/Roles';
import setupCoreMessages from 'core/resources/Messages';
import { MessageRepo } from 'core/Notify';
import COOL from 'core/COOL';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

export default class ActivationBaseViewSuite extends PageViewSuite {
    beforeAll () {
        setupCoreMessages();
        return Q();
    }

    afterAll () {
        MessageRepo.clear();
        return Q();
    }

    beforeEach () {
        this.removeConfirmTemplate = specHelpers.renderTemplateToDOM('core/templates/confirm.ejs');

        resetStudyDesign();

        localStorage.setItem('krpt', 'NP.1447382bdaad4a85bb4b4f3e33c5105b');

        window.noty = $.noop;
        $.fn.noty = $.noop;

        // Add default password format
        LF.StudyDesign.defaultPasswordFormat = {
            max: 20,
            min: 4,
            lower: 0,
            upper: 0,
            alpha: 0,
            numeric: 0,
            special: 0,
            custom: [],
            allowRepeating: true,
            allowConsecutive: true
        };

        // Add roles with custom password format
        LF.StudyDesign.roles = new Roles();
        LF.StudyDesign.roles.add(new Role({
            id: 'subject',
            displayName: 'SUBJECT_ROLE',
            lastDiaryRoleCode: 0,
            defaultAffidavit: 'P_SignatureAffidavit',
            unlockCodeConfig: {
                seedCode: 1111,
                codeLength: 6
            },
            addPermissionsList: ['ALL'],
            passwordFormat: {
                min: 4,
                max: 8
            }
        }));

        LF.StudyDesign.roles.add(new Role({
            id: 'site',
            displayName: 'SITE_ROLE',
            lastDiaryRoleCode: 1,
            permissions: [],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'site',
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'SITE_GATEWAY_TITLE',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            passwordFormat: {
                min: 2,
                max: 8
            }
        }));

        LF.Wrapper = jasmine.createSpyObj('Wrapper', ['exec', 'isBrowser']);

        return super.beforeEach();
    }

    afterEach () {
        localStorage.clear();
        this.removeConfirmTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(ActivationBaseViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testIsValidCode () {
        describe('method:isValidCode', () => {
            it('should be false (empty code).', () => {
                expect(this.view.isValidCode('')).toBe(false);
            });

            it('should be false (should not contain characters).', () => {
                expect(this.view.isValidCode('920932OIKJLI')).toBe(false);
            });

            it('should be false (negative number).', () => {
                expect(this.view.isValidCode('-23423423')).toBe(false);
            });

            // Disabled due to Expert integration
            xit('should be false (less than 8).', () => {
                expect(this.view.isValidCode('1234567')).toBe(false);
            });

            // Disabled due to Expert integration.
            xit('should be false (greater than 8.', () => {
                expect(this.view.isValidCode('123456789')).toBe(false);
            });

            it('should be true.', () => {
                expect(this.view.isValidCode('12345678')).toBe(true);
            });
        });
    }

    testShowTransmissionError () {
        describe('method:showTransmissionError', () => {
            it('should show a transmission error.', (done) => {
                spyOn(this.view, 'i18n').and.resolve('An error has occurred.');
                spyOn(MessageRepo, 'display').and.resolve(true);

                this.view.showTransmissionError({
                    message: 'TRANSMISSION_ERROR'
                }, () => {
                    expect(MessageRepo.display).toHaveBeenCalled();
                    done();
                });
            });
        });
    }

    testGetSubjectData () {
        describe('method:getSubjectData', () => {
            beforeEach(() => {
                spyOn(this.view, 'removeMessage').and.stub();
            });

            Async.it('should navigate to the install view.', () => {
                Data.code = '12345678';
                Data.question = '0';
                Data.answer = '42';

                spyOn(this.view, 'navigate').and.stub();
                spyOn(this.view.webService, 'getkrDom').and.resolve();
                spyOn(this.view.webService, 'syncLastDiary').and.resolve({ res: [] });
                spyOn(this.view.webService, 'getSingleSubject').and.resolve({ res: { custom10: '3' } });

                spyOn(this.view.webService, 'doSubjectSync').and.resolve({
                    res: {
                        I: 'JS',
                        K: '12341231321',
                        N: '100-001',
                        C: '100',
                        P: '10',
                        T: '2012-07-25T17:39:43Z',
                        L: 'ERROR',
                        E: '2012-07-25T17:39:43Z',
                        A: '2012-07-25T17:39:43Z'
                    },
                    syncID: '31241312'
                });

                let request = this.view.getSubjectData('1231241231', null, $.noop);

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('install', true);
                });
            });

            Async.it('should fail to sync.', () => {
                spyOn(this.view, 'showTransmissionError').and.callFake($.noop);
                spyOn(this.view.webService, 'doSubjectSync').and.reject({
                    errorCode: 6701,
                    httpCode: 500
                });

                return this.view.getSubjectData('1231241231')
                .then(() => expect(this.view.showTransmissionError).toHaveBeenCalled());
            });
        });
    }

    testIsValidForm () {
        describe('method:isValidForm', () => {
            let selector = '#setupCode',
                input;

            beforeEach(() => {
                input = '<input id="setupCode" type="text" />';

                this.view.$el.append(input);
            });

            afterEach(() => {
                this.view.$(selector).remove();
            });

            it('should return true.', () => {
                this.view.$(selector).val('12345678');

                expect(this.view.isValidForm(selector)).toBe(true);
            });

            it('should return false.', () => {
                this.view.$(selector).val('-23423423');

                expect(this.view.isValidForm(selector)).toBe(false);
            });
        });
    }

    testPreInstallCheck () {
        describe('method:preInstallCheck', () => {
            Async.it('should check for the correct install', () => {
                spyOn(this.view, 'navigate').and.stub();
                spyOn(helpers, 'checkInstall').and.resolve(true);

                let request = this.view.preInstallCheck();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('#login');
                });
            });
        });
    }

    testAttemptTransmission () {
        describe('method:attemptTransmission', () => {
            let Utilities = COOL.getClass('Utilities');

            Async.it('should log an error (isOnline rejected).', () => {
                const errorText = 'oops';
                spyOn(Utilities, 'isOnline').and.reject(errorText);

                let request = this.view.attemptTransmission();

                expect(request).toBePromise();

                return request.then(() => fail('expected transmission to have failed'))
                .catch(res => expect(res).toBe(errorText));
            });

            Async.it('should resolve (online).', () => {
                spyOn(Utilities, 'isOnline').and.resolve(true);
                spyOn(this.view, 'displayMessage').and.stub();

                let request = this.view.attemptTransmission();

                expect(request).toBePromise();

                return request.then((isOnline) => {
                    expect(isOnline).toBe(true);
                    expect(this.view.displayMessage).toHaveBeenCalledWith('PLEASE_WAIT');
                });
            });

            Async.it('should resolve (offline).', () => {
                let spy = spyOn(MessageRepo, 'display').and.resolve(true);

                spyOn(Utilities, 'isOnline').and.resolve(false);

                let request = this.view.attemptTransmission();

                expect(request).toBePromise();

                return request.then((isOnline) => {
                    expect(isOnline).toBe(false);
                    expect(spy).toHaveBeenCalledWith(MessageRepo.Dialog.CONNECTION_REQUIRED_ERROR);
                });
            });
        });
    }

    testCodeEntry () {
        describe('method:codeEntry', () => {
            it('should navigate to code entry page', () => {
                spyOn(this.view, 'navigate').and.stub();
                this.view.codeEntry({ preventDefault: $.noop });

                expect(this.view.navigate).toHaveBeenCalledWith('code_entry', true);
            });
        });
    }

    testActivate () {
        describe('method:activate', () => {
            // SM: This seems like a pointless test. If you DELETE a method of the class
            //     the resulting behavior need not be defined; or am I misunderstanding
            //     something?
            xit('should throw an error.', () => {
                spyOn(this.view, 'attemptTransmission').and.callFake(() => Q(false));

                delete this.view.codeEntry;

                expect(() => {
                    this.view.activate();
                }).toThrow(jasmine.any(Error));
            });

            xit('should navigate to the enter code view.', (done) => {
                spyOn(this.view, 'codeEntry').and.callFake(() => done());
                spyOn(this.view, 'attemptTransmission').and.callFake(() => {
                    let deferred = Q.defer();

                    deferred.resolve(false);

                    return deferred.promise;
                });

                this.view.activate();

                setTimeout(() => {
                    expect(this.view.codeEntry).toHaveBeenCalled();
                    done();
                });
            });

            xit('should activate the logpad.', () => {
                // unimplemented
            });
        });
    }

    testDisplayInvalidIcon () {
        xdescribe('method:displayInvalidIcon', () => {
            it('should have the invalid-icon class', () => {
                this.view.displayInvalidIcon(this.view.$el);

                expect(this.view.$el.attr('class')).toEqual('invalid-icon');
                expect(this.view.$el.attr('class')).not.toEqual('valid-icon');
            });

            it('should have the valid-icon class', () => {
                this.view.displayValidIcon(this.view.$el);

                expect(this.view.$el.attr('class')).toEqual('valid-icon');
                expect(this.view.$el.attr('class')).not.toEqual('invalid-icon');
            });
        });
    }

    testValidateNewPassword () {
        describe('method:validateNewPassword', () => {
            let evt,
                input;

            beforeEach(() => {
                evt = { target: '#password' };
                input = '<input id="password" type="password" />';

                this.view.$el.append(input);
            });

            afterEach(() => {
                this.view.$el.find('#password').remove();
            });

            it('should be invalid.', () => {
                spyOn(this.view, 'inputError').and.callFake(() => true);

                this.view.$('#password').val('12345678901234567890123456789');
                this.view.validateNewPassword(evt);

                expect(this.view.inputError).toHaveBeenCalled();
            });

            it('should be valid.', () => {
                spyOn(this.view, 'inputSuccess').and.callFake(() => true);

                this.view.$('#password').val('12345678');
                this.view.validateNewPassword(evt);

                expect(this.view.inputSuccess).toHaveBeenCalled();
            });
        });
    }
}
