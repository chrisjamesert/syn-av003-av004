import ResetSecretQuestionView from 'core/views/ResetSecretQuestionView';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import * as lStorage from 'core/lStorage';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

// TODO: Extend with base class suite not yet in stream.
class ResetSecretQuestionViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());

        Async.afterEach(() => this.afterEach());
    }

    beforeEach () {
        let user = new User({
            username: 'System Administrator',
            role: 'admin'
        });

        LF.security = new Session();
        LF.security.activeUser = user;
        spyOn(LF.security, 'getUser').and.resolve(user);

        this.removeTemplate = specHelpers.renderTemplateToDOM('core/templates/reset-secret-question.ejs');
        this.view = new ResetSecretQuestionView();

        // TODO: call super.beforeEach();
        return this.view.render();
    }

    afterEach () {
        this.removeTemplate();
        localStorage.clear();

        // TODO: call super.afterEach();
        return Q();
    }

    // TODO: This is a base class test in PageView, but not yet promoted to this stream.
    testId (id) {
        it(`should have an id of ${id}.`, () => {
            expect(this.view.id).toBe(id);
            expect(this.view.$el.attr('id')).toBe(id);
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to be rejected'))
                    .catch(e => expect(e).toBe('DOMError'));
            });

            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    // Using form element to prove the view was rendered.
                    let rendered = !!this.view.$('form').length;

                    expect(rendered).toBe(true);
                });
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            beforeEach(() => spyOn(this.view, 'navigate').and.stub());

            it('should navigate back to the login view.', () => {
                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('login');
            });

            it('should navigate to the change temporary password view.', () => {
                lStorage.setItem('changePassword', true);

                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('change_temporary_password');
            });
        });
    }

    testOnSecretQuestionSave () {
        describe('method:onSecretQuestionSaved', () => {
            beforeEach(() => {
                spyOn(this.view, 'navigate').and.stub();
                spyOn(this.view, 'notify').and.resolve();
            });

            Async.it('should navigate to the dashboard view.', () => {
                lStorage.setItem('changePassword', true);

                let request = this.view.onSecretQuestionSaved();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.notify).not.toHaveBeenCalled();
                    expect(this.view.navigate).toHaveBeenCalledWith('dashboard');
                });
            });

            Async.it('should notify the user and then navigate to the dashboard view.', () => {
                let request = this.view.onSecretQuestionSaved();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.notify).toHaveBeenCalled();
                    expect(this.view.navigate).toHaveBeenCalledWith('dashboard');
                });
            });
        });
    }
}

describe('ResetSecretQuestionView', () => {
    let suite = new ResetSecretQuestionViewSuite();

    suite.testId('reset-secret-question-page');
    suite.testRender();
    suite.testBack();
    suite.testOnSecretQuestionSave();
});
