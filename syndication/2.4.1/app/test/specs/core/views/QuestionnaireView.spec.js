import BaseQuestionnaireViewTests from './BaseQuestionnaireView.specBase';

import QuestionnaireView from 'core/views/QuestionnaireView';

import Session from 'core/classes/Session';
import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';

import Questionnaires from 'core/collections/Questionnaires';
import Screens from 'core/collections/Screens';
import Questions from 'core/collections/Questions';
import Templates from 'core/collections/Templates';
import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import Subjects from 'core/collections/Subjects';

import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Transmissions from 'core/collections/Transmissions';
import Roles from 'core/collections/Roles';

import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Role from 'core/models/Role';
import Answer from 'core/models/Answer';
import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import * as lStorage from 'core/lStorage';

class QuestionnareBaseTests extends BaseQuestionnaireViewTests {
    constructor (options) {
        super(options);
        this.Questionnaire = QuestionnaireView;
        this.options.subject = new Subject(
            {
                device_id: hex_sha512('123456'),
                initials: 'bc',
                log_level: 'ERROR',
                id: 1,
                phase: 10,
                phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
                phaseTriggered: 'false',
                secret_answer: hex_sha512('apple'),
                secret_question: '0',
                site_code: '267',
                subject_id: '12345',
                subject_password: hex_sha512('applekrpt.39ed120a0981231'),
                service_password: hex_sha512('applekrpt.39ed120a0981231'),
                subject_number: '54321',
                subject_active: 1,
                krpt: 'krpt.39ed120a0981231',
                enrollmentDate: '2013-10-05T08:15:30-05:00',
                activationDate: '2013-10-05T08:15:30-05:00'
            }
        );
    }

    /**
     * called before each test
     * @returns {Q.Promise.<T>|*}
     */
    beforeEach () {
        return super.beforeEach()
            .then(() => {
                LF.StudyDesign.studyVersion = 'dummy Study Version';
                LF.StudyDesign.lastDiaryRole = {
                    IG: 'dummyIG',
                    IT: 'dummyRole'
                };
                LF.StudyDesign.roles = new Roles();
                LF.StudyDesign.roles.add(new Role({
                    id: 'dummyRole',
                    displayName: 'DUMMY_ROLE',
                    lastDiaryRoleCode: 0,
                    defaultAffidavit: 'DEFAULT',
                    unlockCodeConfig: {
                        seedCode: 1111,
                        codeLength: 6
                    },
                    addPermissionsList: ['ALL']
                }));

                CurrentContext().setContextByRole('dummyRole');

                window.localStorage.clear();

                const subjects = new Subjects();
                subjects.add(this.options.subject);
                return subjects.save();
            });
    }

    /**
     * check to verify that the constructor sets internal state correctly
     * @returns {Q.Promise<obj>} return the object created
     */
    constructorTests () {
        return super.constructorTests()
            .then((myView) => {
                expect(myView.subject).toBe(this.options.subject);
                expect(myView.Answer).toThrow();
            });

    }
    execTests () {

        describe('QuestionnaireView Tests', () => {
            super.execTests();

            describe('QuestionnaireView Class Specific Tests', () => {

                let view,
                    location,
                    questionnaires = LF.StudyDesign.questionnaires,
                    screens = LF.StudyDesign.screens,
                    questions = LF.StudyDesign.questions,
                    rules = LF.StudyRules,
                    template = '<div id="questionnaire-template"> <div id="questionnaire"></div></div>',
                    subject = new Subject({
                        device_id: hex_sha512('123456'),
                        initials: 'bc',
                        log_level: 'ERROR',
                        id: 1,
                        phase: 10,
                        phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
                        phaseTriggered: 'false',
                        secret_answer: hex_sha512('apple'),
                        secret_question: '0',
                        site_code: '267',
                        subject_id: '12345',
                        subject_password: hex_sha512('applekrpt.39ed120a0981231'),
                        service_password: hex_sha512('applekrpt.39ed120a0981231'),
                        subject_number: '54321',
                        subject_active: 1,
                        krpt: 'krpt.39ed120a0981231',
                        enrollmentDate: '2013-10-05T08:15:30-05:00',
                        activationDate: '2013-10-05T08:15:30-05:00'
                    });

                Async.it('should create a new dashboard record with an instance_ordinal of 2.', () => {
                    const myView = new this.Questionnaire(this.options);

                    return myView.prepDashboardData('LogPad')
                        .then(() => {
                            expect(myView.data.dashboard.get('user_id')).toEqual(LF.security.activeUser.get('id'));
                            expect(myView.data.dashboard.get('subject_id')).toEqual(myView.subject.get('subject_id'));
                            expect(myView.data.dashboard.get('device_id')).toEqual(myView.subject.get('device_id') ||
                                lStorage.getItem('IMEI') || subject.device_id || subject.get('device_id'));
                            expect(myView.data.dashboard.get('SU')).toEqual(myView.model.get('SU'));
                            expect(myView.data.dashboard.get('questionnaire_id')).toEqual('LogPad');
                            expect(myView.data.dashboard.get('instance_ordinal')).toEqual(myView.ordinal);
                            expect(myView.data.dashboard.get('phase')).toEqual(myView.subject.get('phase'));
                            expect(myView.data.dashboard.get('phaseStartDateTZOffset')).toEqual(myView.subject.get('phaseStartDateTZOffset'));
                            expect(myView.data.dashboard.get('study_version')).toEqual(LF.StudyDesign.studyVersion);
                            expect(myView.data.dashboard.get('sig_id')).toBeDefined();

                            const myAnswer = new myView.Answer();
                            expect(myAnswer instanceof Answer)
                                .toBeTruthy('Expected the answer builder to create instance of Answer');
                            expect(myAnswer.get('subject')).toBe(this.options.subject.get('subject_id'));
                            expect(myAnswer.get('instance_ordinal')).toBe(myView.ordinal);
                        });
                });

                Async.it('should save the dashboard record.', () => {
                    const dashboards = new Dashboards(),
                        myView = new this.Questionnaire(this.options);

                    let startDashboardLength;
                    return dashboards.fetch()
                        .then(() => {
                            startDashboardLength = dashboards.length;
                        })
                        .then(() => {
                            return myView.resolve();
                        })
                        .then(() => {
                            return myView.saveDashboard();
                        })
                        .then(() => {
                            return dashboards.fetch();
                        })
                        .tap(() => {
                            expect(dashboards.length).toBe(startDashboardLength + 1);
                            expect(dashboards.at(dashboards.length - 1).get('questionnaire_id')).toEqual(myView.id);
                        });
                });

                Async.xit('should save the transmission record.', () => {

                    const collection = new LF.Collection.Transmissions();


                    return view.createTransmission()
                        .then(() => {
                            expect(collection.at(0).get('method')).toEqual('transmitQuestionnaire');
                        });

                });

                Async.xit('should save the answers to the database', () => {

                    const collection = new LF.Collection.Answers();

                    return view.saveAnswers()
                        .then(() => {
                            return collection.fetch();
                        })
                        .then((response) => {
                            expect(response[0].response).toEqual('0');
                        });
                });

                Async.it('should not save the answer with null IT to the database', () => {
                    let response,
                        questionView,
                        answers =[{
                            user_id: 1,
                            question_id: 'TEST_Q_3',
                            questionnaire_id: 'TEST',
                            response: '3',
                            SW_Alias: 'TEST.0.TEST_Q_3',
                            localOnly: false
                        }, {
                            user_id: 1,
                            question_id: 'TEST_Q_6',
                            questionnaire_id: 'TEST',
                            response: '1',
                            SW_Alias: 'TEST.0.TEST_Q_6'
                        }, {
                            user_id: 1,
                            question_id: 'TEST_Q_7',
                            questionnaire_id: 'TEST',
                            response: '1',
                            SW_Alias: 'TEST.0.TEST_Q_7',
                            localOnly: true
                        }];
                    const myView = new this.Questionnaire(this.options);

                    return myView.resolve()
                    .then(() => {
                        myView.data.answers.add(answers);
                    })
                    .catch((e) => fail(e))
                    .then(() => {
                        const collection = new LF.Collection.Answers();
                        return myView.saveAnswers()
                        .then(() => {
                            return collection.fetch();
                        })
                        .then((response) => {
                            //LPARole, TEST_Q_3 and TEST_Q_6
                            expect(response.length).toEqual(3);
                        });
                    });
                });

                Async.xit('should save the questionnaire and add a transmission.', () => {

                    let collection = new LF.Collection.Transmissions(),
                        response;

                    view.save()
                        .then(() => {
                            return collection.fetch();
                        })
                        .then((response) => {
                            expect(response[1].params).toEqual('2');
                        });

                });

                Async.xit('should not save any answers', () => {

                    let response;

                    view.saveAnswers();

                    waitsFor(() => {
                        return response;
                    }, 'the callback to be invoked', 1000);

                    runs(() => {
                        expect(response).toBeTruthy();
                    });

                });

                // @todo phase change should be a rule and test should be in a rules test case?
                Async.xit('should change the phase.', () => {

                    view.changePhase({
                        phase: 20,
                        change_phase: true,
                        phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00'
                    });

                    expect(view.phase).toEqual(20);
                    expect(view.data.dashboard.get('phase')).toEqual(20);
                    expect(view.data.dashboard.get('phaseStartDateTZOffset')).toEqual('2013-10-05T08:15:30-05:00');

                });

                // @todo: should be in the dashboard test cases
                Async.xit('Report date should reflect date of availability when diary is started.', () => {

                    let model = new LF.Model.Dashboard(),
                        started = new Date(),
                        completed = new Date(),
                        response;

                    LF.SpecHelpers.createSubject();
                    LF.SpecHelpers.saveSubject();

                    completed.setDate(started.getDate() + 1);

                    model.save({
                        id: 1,
                        subject_id: '9876',
                        SU: 'LogPad',
                        questionnaire_id: 'LogPad',
                        instance_ordinal: 1,
                        started: started.ISOStamp(),
                        report_date: LF.Utilities.convertToDate(started),
                        completed: completed.ISOStamp(),
                        diary_id: completed.getTime(),
                        completed_tz_offset: completed.getOffset(),
                        phase: 20
                    }, {
                        onSuccess: () => {
                            view.prepDashboardData('LogPad', function (res) {
                                response = res;
                            });
                        }
                    });

                    waitsFor(() => {
                        return response;
                    }, 'the dashboard data to populate', 1000);

                    runs(() => {
                        expect(response.get('report_date')).toEqual(LF.Utilities.convertToDate(new Date()));
                    });

                });

                // @todo phase change rule should be in rules test
                Async.xit('should not branch from screen 1 to screen 6 if current phase is not treatment.', () => {

                    let response;

                    LF.SpecHelpers.createSubject();
                    LF.SpecHelpers.saveSubject();

                    view.displayScreen('LogPad_S_1');

                    view.next({}, () => {
                        response = true;
                    });

                    waitsFor(() => {
                        return response;
                    }, 'the next default screen to display.', 1000);

                    runs(() => {
                        expect(view.$('section').hasClass('LogPad_S_2')).toBeTruthy();
                        expect(view.subject.get('phase')).toNotEqual(30);
                    });

                });

                // @todo phase change rule should be in rules test
                Async.xit('should complete diary and trigger set new phase after next on affidavit screen.', () => {
                    //set only QUESTIONNAIRE:Completed trigger for this test
                    LF.StudyRules = new LF.Collection.Rules([
                        {
                            id: 'ChangePhase',
                            trigger: 'QUESTIONNAIRE:Completed',
                            expression: function (filter, callback) {
                                callback(filter.subject.get('phaseTriggered') === 'true' || filter.triggerPhase !== undefined);
                            },
                            actionData: [
                                {
                                    trueAction: 'changePhase',
                                    trueArguments: function (filter, callback) {
                                        let triggerPhase = filter.triggerPhase;
                                        callback({
                                            change: true,
                                            phase: LF.StudyDesign.studyPhase[triggerPhase],
                                            phaseStartDateTZOffset: LF.Utilities.timeStamp(new Date())
                                        });
                                    }
                                }
                            ]
                        }
                    ]);

                    view.next();

                    waitsFor(() => {
                        return location === 'dashboard';
                    }, 'the dashboard to be displayed.', 2000);

                    runs(() => {
                        expect(location).toEqual('dashboard');
                        expect(view.subject.get('phase')).toEqual(30);
                    });

                });
            });
        });
    }
}

const tests = new QuestionnareBaseTests();
tests.execTests();
