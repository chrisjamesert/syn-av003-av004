import ConfirmView from 'core/views/ConfirmView';
import ModalViewTests from './ModalView.specBase';
import * as specHelpers from 'test/helpers/SpecHelpers';

class ConfirmViewTests extends ModalViewTests {

    beforeEach () {
        this.view = new ConfirmView();

        this.removeTemplate = specHelpers.renderTemplateToDOM('core/templates/confirm.ejs');

        return Q();
    }

    afterEach () {
        this.removeTemplate();

        return Q();
    }

    testShow (strings) {
        describe('method:show', () => {
            Async.it('should show the modal.', () => {
                spyOn(this.view, 'render').and.stub();

                let request = this.view.show(strings);

                expect(request).toBePromise();

                // We don't need a resolve test, because it's tested here.
                this.view.resolve();

                return request.then(res => {
                    expect(res).toBe(true);
                    expect(this.view.render).toHaveBeenCalledWith(strings);
                });
            });
        });
    }

    testReject (strings) {
        describe('method:reject', () => {
            Async.it('should reject the confirmation.', () => {
                spyOn(this.view, 'hide').and.callThrough();

                let request = this.view.show(strings);

                expect(request).toBePromise();
                this.view.reject();

                return request
                    .then(() => fail('method show should have been rejected.'))
                    .catch(() => {
                        expect(this.view.hide).toHaveBeenCalled();
                    });
            });
        });
    }
}

describe('ConfirmView', () => {
    let strings = {
        header: 'Hello World',
        body: 'Hellow World',
        ok: 'OK',
        cancel: 'Cancel'
    };

    let suite = new ConfirmViewTests();

    suite.testId('modal');
    suite.testClass('modal');
    suite.testAttribute('role', 'dialog');
    suite.testAttribute('aria-hidden', 'true');
    suite.testAttribute('data-backdrop', 'static');
    suite.testGetTemplate();
    suite.testTeardown();
    suite.testShow(strings);
    suite.testAddGlyphicon();
    suite.testFindModalElement(strings);
    suite.testAddModalClass();
    suite.testApplyStyles({ type: 'error', icon: 'minus-circle'});
    suite.testApplyStyles({ type: 'success', icon: 'check-circle'});
    suite.testApplyStyles({ type: 'warning', icon: 'exclamation-triangle'});
    suite.testHide();
    suite.testReject(strings);
});
