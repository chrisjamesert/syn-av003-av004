import Transmission from 'core/models/Transmission';
import User from 'core/models/User';

import PageViewSuite from './PageView.specBase';

export default class SecretQuestionBaseViewSuite extends PageViewSuite {

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SecretQuestionBaseViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testCreateTransmission () {
        describe('method:createTransmission', () => {
            let params;

            beforeEach(() => params = JSON.stringify({
                secret_question: 1,
                secret_answer: 'Hello World'
            }));

            Async.it('should fail to create the transmission.', () => {
                spyOn(Transmission.prototype, 'save').and.reject('DatabaseError');

                let request = this.view.createTransmission(params);

                expect(request).toBePromise();

                return request.then(() => fail('Expected method createTransmission to be rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should create the transmission.', () => {
                let request = this.view.createTransmission(params);

                expect(request).toBePromise();

                return request.then(id => {
                    expect(id).toEqual(jasmine.any(Number));
                });
            });
        });
    }

}
