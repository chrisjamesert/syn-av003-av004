import * as helpers from 'core/Helpers';
import { createGUID } from 'core/utilities';
import PatientSession from 'core/classes/PatientSession';
import User from 'core/models/User';
import * as lStorage from 'core/lStorage';

import ForgotPasswordView from 'core/views/ForgotPasswordView';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import Logger from 'core/Logger';

describe('Forgot Password', () => {

    let preventDefault = $.noop,
        failures = 0,
        view,
        location,
        removeTemplate;

    beforeEach(done => {
        resetStudyDesign();
        LF.StudyDesign.maxSecurityQuestionAttempts = 5;
        failures = 0;

        $.noty = { closeAll: $.noop };
        LF.security = new PatientSession();

        spyOn(helpers, 'checkInstall').and.callFake(callback => callback(true));
        spyOn(LF.security, 'getSecurityQuestionFailure').and.callFake(() => failures);
        spyOn(LF.security, 'setSecurityQuestionFailure').and.callFake((attempts, user) => {
            failures = attempts += 1;
        });
        spyOn(LF.security, 'getUser').and.callFake(() => {
            let user = new User({
                id: 1,
                username: 'admin',
                secretAnswer: hex_sha512('apple'),
                secretQuestion: '0'
            });

            return Q(user);
        });

        lStorage.setItem('User_Login', 1);
        removeTemplate = specHelpers.renderTemplateToDOM('core/templates/forgot-password.ejs');
        view = new ForgotPasswordView();
        spyOn(view, 'navigate').and.callFake(url => location = url);

        localStorage.setItem('Forgot_Password', true);

        specHelpers.createSubject();
        specHelpers.saveSubject()
            .then(() => view.resolve())
            .then(() => view.render())
            .catch(e => fail(e))
            .done(done);
    });

    afterEach(() => {
        LF.security = undefined;
        LF.router = undefined;
        LF.schedule = undefined;
        location = undefined;

        removeTemplate();
        localStorage.clear();
    });

    afterAll(done => specHelpers.uninstallDatabase().done(done));

   it('should have an ID of forgot-password-page.', () => {
        expect(view.$el.attr('id')).toEqual('forgot-password-page');
    });

    describe('method:resolve', () => {
        it('should fail to resolve.', done => {
            localStorage.clear();
            view.resolve()
                .catch(e => {
                    expect(e.message).toEqual('Invalid access of ForgotPasswordView.');
                    expect(location).toBe('login');
                })
                .done(done);
        });

        it('should resolve.', done => {
            localStorage.setItem('Forgot_Password', true);
            view.resolve()
                .tap(() => {
                    expect(view.user).toBeDefined();
                    expect(view.user.get('id')).toBe(1);
                })
                .catch(e => fail(e))
                .done(done);
        });
    });

    describe('method:back', () => {
        it('should navigate to the login page', () => {
            view.back();
            expect(location).toBe('login');
        });
    });

    describe('method:unlockCode', () => {
        Async.it('should navigate to the unlock code view.', () => {
            let request = view.unlockCode();

            expect(request).toBe(request);

            return request.then(() => {
                expect(location).toBe('unlock_code');
            });
        });
    });

    describe('method:getSecretQuestion', () => {
        it('should get the security question.', () => {
            let question = view.getSecretQuestion(0);

            expect(question).toBeDefined();
            expect(question).toBe('SECURITY_QUESTION_0');
        });

        it('should fail to get the security question.', () => {
            let err = new Error('Security Question not found in the Study Configuration.');
            expect(() => {
                view.getSecretQuestion(1);
            }).toThrow(err);
        });
    });

    describe('method:validate', () => {
        beforeEach(() => {
            spyOn(Logger.prototype, 'operational').and.callFake(() => Q());
            spyOn(Logger.prototype, 'error').and.callFake(() => Q());
        });

        it('should not validate the answer.', () => {
            view.$secretAnswer.val('wrongAnswer');
            spyOn(view, 'showInputError');
            view.validate({ preventDefault });
            expect(Logger.prototype.error).toHaveBeenCalledWith('Incorrect secret answer entered, attempt #1/5 for user admin');
            expect(view.showInputError).toHaveBeenCalledWith(jasmine.any(Object), 'INCORRECT_ANSWER');
        });

        it('should validate the answer.', () => {
            view.$secretAnswer.val('apple');
            view.validate({ preventDefault });

            expect(location).toEqual('reset_password');
            expect(Logger.prototype.operational).toHaveBeenCalledWith('Correct secret answer entered');
        });

        it('should attempt to answer after maxSecurityQuestionAttempts and redirect to Unlock Code screen', () => {
            spyOn(view, 'unlockCode').and.stub();

            LF.security.setSecurityQuestionFailure(5);

            view.$secretAnswer.val('wrongAnswer');
            view.validate({ preventDefault });

            expect(view.unlockCode).toHaveBeenCalled();
        });

        it('shouldn\'t validate the correct answer when security question attempts is greater than maximum and redirect to Unlock Code screen', () => {
            spyOn(view, 'unlockCode').and.stub();
            LF.security.setSecurityQuestionFailure(5);

            view.$('#secret_answer').val('apple');
            view.validate({ preventDefault: $.noop });

            expect(view.unlockCode).toHaveBeenCalled();
        });
    });

    describe('method:onInput', () => {
        it('shouldn\'t enable OK button with no value in secret_answer.', () => {
            view.$secretAnswer.val('');

            view.onInput();

            expect(view.$submit.attr('disabled')).toBe('disabled');
        });

        it('should enable login button with value in password.', () => {
            view.$secretAnswer.val('abc');

            view.onInput();

            expect(view.$('#submit').attr('disabled')).not.toBe('disabled');
        });
    });

});
