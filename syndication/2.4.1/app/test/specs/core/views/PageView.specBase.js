import { Banner, MessageRepo } from 'core/Notify';
import RouterBase from 'core/classes/RouterBase';
import * as specHelpers from 'test/helpers/SpecHelpers';

export default class PageViewTests {
    constructor () {
        Async.beforeAll(() => this.beforeAll());
        Async.beforeEach(() => this.beforeEach());

        Async.afterEach(() => this.afterEach());
        Async.afterAll(() => this.afterAll());
    }

    /**
     * Invoked before all unit tests are executed.
     * @returns {Q.Promise<void>}
     */
    beforeAll () {
        return Q();
    }

    /**
     * Invoked after all unit tests are executed.
     * @returns {Q.Promise<void>}
     */
    afterAll () {
        return Q();
    }

    /**
     * Default values to pass into each test method.
     * @type Object
     */
    get defaults () {
        return {
            id: 'page',
            tagName: 'div',
            template: '#page-tpl',
            dynamicStrings: { count: 10 },
            translationKeys: 'TRANSMISSIONS',
            button: '#submit',
            input: 'input'
        };
    }

    /**
     * Annotate a method's arguments from the provided context.
     * @param {Function} fn The target function to annotate.
     * @param {Object} context The context to pull argument values from.
     * @returns {Array} An array of arguments to pass into the target method.
     */
    annotate (fn, context) {
        const FN_ARGS = /^function\s*[^\(]*\(\s*([^\)]*)\)/m;
        const FN_ARG = /^\s*(_?)(.+?)\1\s*$/;
        const FN_ARG_SPLIT = /,/;
        const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

        let fnText = fn.toString().replace(STRIP_COMMENTS, '');
        let argDecl = fnText.match(FN_ARGS);
        let inject = [];

        _.forEach(argDecl[1].split(FN_ARG_SPLIT), (arg) => {
            arg.replace(FN_ARG, (all, underscore, name) => {
                inject.push(name);
            });
        });

        // Start of a chain with the suite's default values.
        let args = _.chain(context)

            // Mixin the passed in context.
            .defaults(this.defaults)

            // Pick only the matching argument names.
            .pick(inject)
            .values()
            .value();

        return () => fn.apply(this, args);
    }

    /**
     * Annotate all test methods.
     * @param {String[]} methods The list of method names.
     * @param {Object} context The context to populate the values with.
     */
    annotateAll (methods, context) {
        _.forEach(methods, (name, index) => {
            let isTestCase = name.match('^test.{1,}$');
            let isNotExcluded = !~context.exclude.indexOf(name);

            if (isTestCase && isNotExcluded) {
                this.annotate(this[name], context)();
            }
        });
    }

    /**
     * Execute all unit tests.
     * @param {Object} [context={}] The context of which to populate test method arguments from.
     */
    executeAll (context = {}) {
        let methods = Object.getOwnPropertyNames(PageViewTests.prototype);

        // Populate an exlcusion property if non exists.
        context.exclude = context.exclude || [];

        this.annotateAll(methods, context);
    }

    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');
        let template = '<div id="application"></div>';

        $('body').append(template);

        return Q();
    }

    /**
     * Invoked after each unit test.
     * @returns {Q.Promise<void>}
     */
    afterEach () {
        this.removeSpinnerTemplate();

        $('#application').remove();
        this.view.clearTemplateCache();

        // This ensures all event listeners are removed from the view, and all its children.
        this.view.remove();

        return Q();
    }

    /**
     * Execute before each DOM related unit test.
     * @param {Object} dynamicStrings A map of strings required for the view to render.
     * @returns {Q.Promise<Object>}
     */
    DOMBeforeEach (dynamicStrings = {}) {
        Async.beforeEach(() => {
            if (this.view.template == null) {
                let template = `<script id="page-tpl" type="text/template">
                        <div class="form-group">
                            <input id="username" type="text" />
                        </div>
                        <button id="submit" type="button" disabled="disabled">Submit</button>
                    </script>`;

                $('body').append(template);

                this.view.template = '#page-tpl';
            }

            return this.view.buildHTML(dynamicStrings);
        });
    }

    /**
     * Invoked after each DOM related unit test is executed.
     */
    DOMAfterEach () {
        afterEach(() => {
            if (this.view.template == null) {
                $('#page-tpl').remove();
            }
        });
    }

    /**
     * Tests the ID property of the view.
     * @param {String} [id=page] The id to test for.
     * @example
     * suite.testId('dashboard-view');
     */
    testId (id) {
        it(`should have an id of ${id}.`, () => {
            expect(this.view.id).toBe(id);
            expect(this.view.$el.attr('id')).toBe(id);
        });
    }

    /**
     * Tests the tagName property of the view.
     * @param {String} [tagName=div] The tag name.
     * @example
     * suite.testTagName('span');
     */
    testTagName (tagName) {
        it(`should have a tagName of ${tagName}.`, () => {
            expect(this.view.tagName).toBe(tagName);
        });
    }

    /**
     * Tests a property on the view.
     * @param {String} name The name of the property to test.
     * @param {Any} value The value to test for.
     * @example
     * suite.testProperty('template', '#dashboard-template');
     */
    testProperty (name, value) {
        it(`should have a property ${name} of ${value}.`, () => {
            expect(this.view[name]).toEqual(value);
        });
    }

    /**
     * Tests the className of the view.
     * @param {String} className The class to test for.
     * @example
     * suite.testClass('view');
     */
    testClass (className) {
        it(`should have a class of ${className}.`, () => {
            expect(this.view.className).toBe(className);
            expect(this.view.$el.attr('class')).toBe(className);
        });
    }

    /**
     * Tests for an attribute (view.attributes[attribute]).
     * @param {String} attribute The name of the attribute.
     * @param {Any} value The value of the attribute.
     * @example
     * suite.testAttribute('data-role', 'model');
     */
    testAttribute (attribute, value) {
        it(`should have an attribute ${attribute} with a value of ${value}.`, () => {
            expect(this.view.attributes[attribute]).toBe(value);
            expect(this.view.$el.attr(attribute)).toBe(value);
        });
    }

    /**
     * Tests for view's unique key.
     * @example suite.testKey();
     */
    testKey () {
        it('should have a 10-digit key.', () => {
            expect(this.view.key.length).toBe(10);
        });
    }

    /**
     * Tests the view's close method.
     * @example suite.testClose();
     */
    testClose () {
        describe('method:close', () => {
            it('should close the view.', () => {
                spyOn(this.view, 'remove');
                spyOn(this.view, 'undelegateEvents');

                this.view.close();

                expect(this.view.remove).toHaveBeenCalled();
                expect(this.view.undelegateEvents).toHaveBeenCalled();
            });
        });
    }

    /**
     * Tests the view's displayMessage method.
     * @example suite.testDisplayMessage();
     */
    testDisplayMessage () {
        describe('method:displayMessage', () => {
            Async.it('should display the message', () => {
                if (this.view.spinner.show.and == null) {
                    spyOn(this.view.spinner, 'show');
                }

                let request = this.view.displayMessage('Hello World');

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.spinner.show).toHaveBeenCalled();
                });
            });
        });
    }

    /**
     * Tests the view's removeMessage method.
     * @example suite.testRemoveMessage();
     */
    testRemoveMessage () {
        describe('method:removeMessage', () => {
            Async.it('should remove the message', () => {
                if (this.view.spinner.hide.and == null) {
                    spyOn(this.view.spinner, 'hide');
                }

                return this.view.displayMessage('Hello World!')
                    .then(() => {
                        let request = this.view.removeMessage(null);

                        expect(request).toBePromise();

                        return request.then(() => {
                            expect(this.view.spinner.hide).toHaveBeenCalled();
                        });
                    });
            });
        });
    }

    /**
     * Tests the view's transmitRetry method.
     * @example suite.testTransmitRetry();
     */
    testTransmitRetry () {
        describe('method:transmitRetry', () => {
            let header = 'ERROR',
                message = 'ERROR_MESSAGE',
                type = 'error',
                httpRespCode = '500';

            beforeEach(() => {
                if (LF.security == null) {
                    LF.security = jasmine.createSpyObj('security', ['restartSessionTimeOut']);
                } else if (LF.security.restartSessionTimeOut == null) {
                    LF.security.restartSessionTimeOut = $.noop;
                }
            });

            Async.it('should notify the user.', () => {
                let isOnline = true;

                spyOn(this.view, 'notify').and.resolve(true);

                return this.view.transmitRetry({ header, message, type, isOnline, httpRespCode })
                .tap(() => {
                    expect(this.view.notify).toHaveBeenCalled();
                });
            });

            Async.it('should ask the user for confirmation (CANCEL).', () => {
                let isOnline = false;

                spyOn(this.view, 'confirm').and.resolve(false);

                return this.view.transmitRetry({ header, message, type, isOnline, httpRespCode })

                .tap(() => {
                    expect(this.view.confirm).toHaveBeenCalled();
                });
            });
        });
    }

    /**
     * Tests the view's page method.
     * @example suite.testPage();
     */
    testPage () {
        describe('method:page', () => {
            it('should display the view to the page.', () => {
                this.view.page();

                let $app = $(`#${this.view.id}`);

                expect($app.length).toBe(1);
            });
        });
    }

    /**
     * Tests the view's title method.
     * @example suite.testTitle();
     */
    testTitle () {
        describe('method:title', () => {
            it('should set the title of the browser.', () => {
                this.view.title('Hello World');
                expect(document.title).toBe('Hello World');
            });
        });
    }

    /**
     * Test the view's getTemplate method.
     * @param {String} [selector=#page-tpl] The selector to use.
     */
    testGetTemplate (template) {
        describe('method:getTemplate', () => {
            beforeEach(() => {
                if (this.view.template == null) {
                    $('body').append(`<script id="page-tpl" type="text/template">
                        <h1>{{ greeting }}</h1>
                    </script>`);
                }
            });

            afterEach(() => $('#page-tpl').remove());

            it('should get the template (string).', () => {
                let tpl = this.view.getTemplate(template);

                expect(tpl).toBeDefined();
                expect(_.isFunction(tpl)).toBe(true);
            });

            it('should get the template ($).', () => {
                let tpl = this.view.getTemplate($(template));

                expect(tpl).toBeDefined();
                expect(_.isFunction(tpl)).toBe(true);
            });
        });
    }

    /**
     * Tests the view's buildHTML method.
     * @param {Object} [dynamicStrings={count:10}] Strings required to render the view.
     * @example
     * suite.testBuildHTML({ username: 'admin' });
     */
    testBuildHTML (dynamicStrings) {
        describe('method:buildHTML', () => {
            let template = `<script id="page-tpl" type="text/template">
                    <span>{{ count }}</span>
                </script>`;

            beforeEach(() => {
                if (this.view.template == null) {
                    $('body').append(template);
                    this.view.template = '#page-tpl';
                }
            });

            afterEach(() => $('#page-tpl').remove());

            Async.it('should fail to compile the template.', () => {
                spyOn(this.view, 'i18n').and.reject('DOMError');

                let request = this.view.buildHTML(dynamicStrings);

                expect(request).toBePromise();

                return request.then(() => fail('Expected buildHTML to fail'))
                    .catch((e) => {
                        expect(e).toBe('DOMError');
                    });
            });

            Async.it('should compile the template.', () => {
                let request = this.view.buildHTML(dynamicStrings);

                expect(request).toBePromise();

                return request.tap((strings) => {
                    expect(this.view.$el.attr('id')).toBe(this.view.id);
                });
            });

            Async.it('should render the view to the page.', () => {
                // Page is already tested.  We just want to make sure it was called.
                spyOn(this.view, 'page').and.stub();

                let request = this.view.buildHTML(dynamicStrings, true);

                expect(request).toBePromise();

                return request.tap(() => {
                    expect(this.view.$el.attr('id')).toBe(this.view.id);
                    expect(this.view.page).toHaveBeenCalled();
                });
            });
        });
    }

    /**
     * Tests the view's navigate method.
     * @example suite.testNavigate();
     */
    testNavigate () {
        describe('method:navigate', () => {
            LF.router = new RouterBase();
            it('should navigate to a different view.', () => {
                spyOn(Backbone.history, 'navigate').and.stub();
                this.view.navigate('dashboard', true);
                expect(Backbone.history.navigate).toHaveBeenCalledWith('dashboard', true);
            });
        });
    }

    /**
     * Test the view's reload method.
     * @example suite.testReload();
     */
    testReload () {
        describe('method:reload', () => {
            it('should reload the view.', () => {
                spyOn(Backbone.history, 'loadUrl').and.stub();
                this.view.reload();
                expect(Backbone.history.loadUrl).toHaveBeenCalled();
            });
        });
    }

    /**
     * Tests the view's i18n method.
     * @param {Object|Array|String} [translationKeys=TRANSMISSIONS] Translation keys to pass to the i18n method.
     * @example
     * suite.testI18({ COUNT: 'Count' });
     */
    testI18n (translationKeys) {
        describe('method:i18n', () => {
            Async.it('should attempt to translate.', () => {
                // We really only care that it's being called.
                spyOn(LF.strings, 'display').and.resolve();
                return this.view.i18n(translationKeys, $.noop, { }).tap(() => {
                    expect(LF.strings.display)
                        .toHaveBeenCalledWith(translationKeys, jasmine.any(Function), jasmine.any(Object));
                });
            });
        });
    }

    /**
     * Tests the view's buildSelectors method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @example
     * suite.testBuildSelectors({ count: 10 });
     */
    testBuildSelectors (dynamicStrings) {
        describe('method:buildSelectors', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should build the selectors.', () => {
                if (this.view.selectors == null) {
                    this.view.selectors = {
                        submit: '#submit',
                        username: '#username'
                    };
                }

                this.view.buildSelectors();

                _.forEach(this.view.selectors, (value, key) => {
                    expect(this.view[`$${key}`]).toBeDefined();
                });
            });
        });
    }

    /**
     * Test the view's clearSelectors method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @example
     * suite.testClearSelectors({ count: 10 });
     */
    testClearSelectors (dynamicStrings) {
        describe('method:clearSelectors', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should clear the selectors.', () => {
                if (this.view.selectors == null) {
                    this.view.selectors = {
                        submit: '#submit',
                        username: '#username'
                    };
                }

                this.view.buildSelectors();
                _.forEach(this.view.selectors, (value, key) => {
                    expect(this.view[`$${key}`]).toBeDefined();
                });

                this.view.clearSelectors();
                _.forEach(this.view.selectors, (value, key) => {
                    expect(this.view[`$${key}`]).toBeFalsy();
                });
            });
        });
    }

    /**
     * Test the view's enableElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The button of the element to enable.
     * @example
     * suite.testEnableElement({ count: 10 }, '#sync');
     */
    testEnableElement (dynamicStrings, button) {
        describe('method:enableElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => {
                this.view.$(button).attr('disabled', 'disabled');
            });

            it('should enable the element ($).', () => {
                let $ele = this.view.$(button);

                expect($ele.attr('disabled')).toBe('disabled');
                this.view.enableElement($ele);
                expect($ele.attr('disabled')).toBeFalsy();

                $ele = null;
            });

            it('should enable the element (string).', () => {
                let $ele = this.view.$(button);

                expect($ele.attr('disabled')).toBe('disabled');
                this.view.enableElement(button);
                expect($ele.attr('disabled')).toBeFalsy();

                $ele = null;
            });
        });
    }

    /**
     * Test the view's disableElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the element to disable.
     * @example
     * suite.testDisableElement({ count: 10 }, '#sync');
     */
    testDisableElement (dynamicStrings, button) {
        describe('method:disableElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => this.view.$(button).removeAttr('disabled'));

            it('should disabled the element ($).', () => {
                let $btn = this.view.$(button);

                expect($btn.attr('disabled')).toBe(undefined);
                this.view.disableElement($btn);
                expect($btn.attr('disabled')).toBe('disabled');

                $btn = null;
            });

            it('should disabled the element (string).', () => {
                let $btn = this.view.$(button);

                expect($btn.attr('disabled')).toBe(undefined);
                this.view.disableElement(button);
                expect($btn.attr('disabled')).toBe('disabled');

                $btn = null;
            });
        });
    }

    /**
     * Test the view's enableButton method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the button to enable.
     * @example
     * suite.testEnableButton({ count: 10 }, '#sync');
     */
    testEnableButton (dynamicStrings, button) {
        describe('method:enableButton', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should enable the button.', () => {
                // enableButton is just an alias for enableElement.  No need to re-test.
                spyOn(this.view, 'enableElement');

                this.view.enableButton(button);
                expect(this.view.enableElement).toHaveBeenCalledWith(button);
            });
        });
    }

    /**
     * Test the view's disableButton method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the button to disable.
     * @example
     * suite.testDisableButton({ count: 10 }, '#sync');
     */
    testDisableButton (dynamicStrings, button) {
        describe('method:disableButton', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should disable the button.', () => {
                // disableButton is just an alias for disableElement.  No need to re-test.
                spyOn(this.view, 'disableElement');

                this.view.disableButton(button);
                expect(this.view.disableElement).toHaveBeenCalledWith(button);
            });
        });
    }

    /**
     * Test the view's showElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the element to show.
     * @example
     * suite.testShowElement({ count: 10 }, '#sync');
     */
    testShowElement (dynamicStrings, button) {
        describe('method:showElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => this.view.$(button).addClass('hidden'));

            it('should show the element ($).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(true);
                this.view.showElement($btn);
                expect($btn.hasClass('hidden')).toBe(false);

                $btn = null;
            });

            it('should show the element (string).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(true);
                this.view.showElement(button);
                expect($btn.hasClass('hidden')).toBe(false);

                $btn = null;
            });
        });
    }

    /**
     * Test the view's hideElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the element to hide.
     * @example
     * suite.testHideElement({ count: 10 }, '#sync');
     */
    testHideElement (dynamicStrings, button) {
        describe('method:hideElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();
            beforeEach(() => this.view.$(button).removeClass('hidden'));

            it('should hide the element ($).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(false);
                this.view.hideElement($btn);
                expect($btn.hasClass('hidden')).toBe(true);

                $btn = null;
            });

            it('should hide the element (string).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(false);
                this.view.hideElement(button);
                expect($btn.hasClass('hidden')).toBe(true);

                $btn = null;
            });
        });
    }

    /**
     * Test the view's clearInputState method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of input to clear.
     * @example
     * suite.testClearInputState({ count: 10 }, '#username');
     */
    testClearInputState (dynamicStrings, input) {
        describe('method:clearInputState', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should clear the input state.', () => {
                let $input = this.view.$(input),
                    $parent = this.view.getValidParent($input);

                this.view.inputSuccess($input);
                expect($parent.hasClass('has-success')).toBe(true);

                this.view.clearInputState($input);
                expect($parent.hasClass('has-success')).toBe(false);

                this.view.inputError($input);
                expect($parent.hasClass('has-error')).toBe(true);

                this.view.clearInputState($input);
                expect($parent.hasClass('has-error')).toBe(false);

                $input = null;
                $parent = null;
            });
        });
    }

    /**
     * Test the view's validateInput method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the input to validate.
     * @example
     * suite.testValidateInput({ count: 10 }, '#username');
     */
    testValidateInput (dynamicStrings, input) {
        describe('method:validateInput', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => {
                spyOn(this.view, 'inputSuccess');
                spyOn(this.view, 'inputError');
                spyOn(this.view, 'addHelpText');
                spyOn(this.view, 'removeHelpText');
            });

            it('should validate as false.', () => {
                let $input = this.view.$(input);
                $input.val('QWERTY');

                let result = this.view.validateInput($input, /^[0-9]{8}$/);

                expect(result).toBe(false);
                expect(this.view.inputSuccess).not.toHaveBeenCalled();
                expect(this.view.removeHelpText).not.toHaveBeenCalled();
                expect(this.view.inputError).toHaveBeenCalledWith($input);
                expect(this.view.addHelpText).not.toHaveBeenCalled();

                $input = null;
            });

            it('should validate as false and display help text.', () => {
                let $input = this.view.$(input);
                $input.val('QWERTY');

                let result = this.view.validateInput($input, /^[0-9]{8}$/, 'INVALID_USERNAME');

                expect(result).toBe(false);
                expect(this.view.inputSuccess).not.toHaveBeenCalled();
                expect(this.view.removeHelpText).not.toHaveBeenCalled();
                expect(this.view.inputError).toHaveBeenCalledWith($input);
                expect(this.view.addHelpText).toHaveBeenCalledWith($input, 'INVALID_USERNAME');

                $input = null;
            });

            it('should validate as true.', () => {
                let $input = this.view.$(input);
                $input.val('12345678');

                let result = this.view.validateInput($input, /^[0-9]{8}$/);

                expect(result).toBe(true);
                expect(this.view.inputError).not.toHaveBeenCalled();
                expect(this.view.addHelpText).not.toHaveBeenCalled();
                expect(this.view.inputSuccess).toHaveBeenCalledWith($input);
                expect(this.view.removeHelpText).not.toHaveBeenCalled();

                $input = null;
            });

            it('should validate as true and display help text.', () => {
                let $input = this.view.$(input);
                $input.val('12345678');
                this.view.addHelpText('INVALID_USERNAME');

                let result = this.view.validateInput($input, /^[0-9]{8}$/, 'INVALID_USERNAME');

                expect(result).toBe(true);
                expect(this.view.inputError).not.toHaveBeenCalled();
                expect(this.view.inputSuccess).toHaveBeenCalledWith($input);
                expect(this.view.removeHelpText).toHaveBeenCalledWith($input);
                $input = null;
            });
        });
    }

    /**
     * Test the view's addHelpText method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the input to add text to.
     * @example
     * suite.testAddHelpText({ count: 10 });
     */
    testAddHelpText (dynamicStrings, input) {
        describe('method:addHelpText', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            Async.it('should add help text.', () => {
                let $input = this.view.$(input);

                spyOn(this.view, 'i18n').and.callFake(key => Q.resolve(key));
                this.view.addHelpText($input, 'INVALID_USERNAME');

                $input = null;

                return Q.delay().tap(() => {
                    expect(this.view.$('.help-block').text()).toBe('INVALID_USERNAME');
                });
            });

            it('should not add help text (already exists).', () => {
                let $input = this.view.$(input);

                // We can assume as long as i18n isn't called, no text is appended.
                spyOn(this.view, 'i18n').and.callFake(key => Q.resolve(key));
                this.view.$('.form-group, .input-group').append('<p class="help-block">INVALID_USERNAME</p>');
                this.view.addHelpText($input, 'INVALID_USERNAME');
                this.view.addHelpText($input, 'INVALID_USERNAME');

                // If dom is cleared, this will be called once... but verify that it is never called twice
                expect(this.view.i18n).not.toHaveBeenCalled();
                $input = null;
            });
        });
    }

    /**
     * Test the view's removeHelpText method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the input to add text to.
     * @example
     * suite.testRemoveHelpText({ count: 10 }, '#username');
     */
    testRemoveHelpText (dynamicStrings, input) {
        describe('method:removeHelpText', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should remove the help text.', () => {
                let $input = this.view.$(input),
                    $parent = this.view.getValidParent($input);

                $parent.append('<p class="help-block">INVALID_USERNAME</p>');
                expect($parent.find('.help-block').length).toBe(1);
                this.view.removeHelpText($input);
                expect($parent.find('.help-block').length).toBe(0);

                $input = null;
            });
        });
    }

    /**
     * Test the view's getValidParent method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the input.
     * @example
     * suite.testGetValidParent({ count: 10 }, '#username');
     */
    testGetValidParent (dynamicStrings, input) {
        describe('method:getValidParent', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should return the parent element.', () => {
                let $input = this.view.$(input),
                    $parent = this.view.getValidParent($input);

                expect($parent.hasClass('form-group') || $parent.hasClass('input-group')).toBe(true);
            });
        });
    }

    /**
     * Test the view's inputSuccess method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the input to update to successful state.
     * @example
     * suite.testInputSuccess({ count: 10 }, '#username');
     */
    testInputSuccess (dynamicStrings, input) {
        describe('method:inputSuccess', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should set the input element to a valid state.', () => {
                let $input = this.view.$(input);

                this.view.inputSuccess($input);

                expect(this.view.$('.form-group, .input-group').is('.has-success')).toBe(true);
                $input = null;
            });
        });
    }

    /**
     * Test the view's inputError method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the input to update to errant state.
     * @example
     * suite.testInputError({ count: 10 }, '#username');
     */
    testInputError (dynamicStrings, input) {
        describe('method:inputError', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should set the input element to a valid state.', () => {
                let $input = this.view.$(input);

                this.view.inputError($input);

                expect(this.view.$('.form-group, .input-group').is('.has-error')).toBe(true);
                $input = null;
            });
        });
    }

    /**
     * Test the view's showInputError method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [input=input] The selector of the errant input
     * @example
     * suite.testShowInputError({ count: 10 }, '#username');
     */
    testShowInputError (dynamicStrings, input) {
        describe('method:showInputError', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => {
                MessageRepo.add({
                    type: 'Banner',
                    key: 'INPUT_ERROR_INVALID_USERNAME_TEST',
                    message: () => Banner.show('INVALID_USERNAME')
                });
            });

            afterEach(() => {
                MessageRepo.remove({
                    type: 'Banner',
                    key: 'INPUT_ERROR_INVALID_USERNAME_TEST'
                });
            });

            Async.it('should show an error.', () => {
                // spyOn(this.view, 'i18n').and.resolve('Invalid username.');
                spyOn(Banner, 'show').and.stub();

                let request = this.view.showInputError(this.view.$(input), 'INVALID_USERNAME_TEST');

                expect(request).toBePromise();

                return request.delay()
                    .tap(() => {
                        expect(Banner.show).toHaveBeenCalled();
                    });
            });

            // @TODO RTE: Fix these
            // Async.it('should fail to show an error.', () => {
            //     spyOn(this.view, 'i18n').and.reject(new Error('An unknow exception was thrown.'));
            //
            //     let request = this.view.showInputError(input, 'INVALID_USERNAME_TEST');
            //
            //     expect(request).toBePromise();
            //
            //     return request.then(() => fail('Expected showInputError to fail.'))
            //         // Expect error to be swallowed.
            //         .catch(e => {});
            // });
        });
    }

    /**
     * Test the view's skipSubmit method.
     * @example suite.testSkipSubmit();
     */
    testSkipSubmit () {
        describe('method:skipSubmit', () => {
            it('should return false.', () => {
                expect(this.view.skipSubmit({ preventDefault: $.noop })).toBe(false);
            });
        });
    }

    /**
     * Test the view's hideKeyboardOnEnter method.
     * @example suite.testHideKeyboardOnEnter();
     */
    testHideKeyboardOnEnter () {
        describe('method:hideKeyboardOnEnter', () => {
            let template = `<script id="page-tpl" type="text/template">
                    <input id="username" type="text" />
                    <button id="submit" type="button">Submit</button>
                </script>`;

            Async.beforeEach(() => {
                $('body').append(template);
                this.view.template = '#page-tpl';

                return this.view.buildHTML({ }, true);
            });

            afterEach(() => $('#page-tpl').remove());

            it('should do nothing (wrong keyCode).', () => {
                this.view.$('input').focus();

                expect(document.activeElement.tagName).toBe('INPUT');
                expect(document.activeElement).toBeDefined();

                spyOn(document.activeElement, 'blur');
                spyOn(document.activeElement, 'setSelectionRange');

                this.view.hideKeyboardOnEnter({ keyCode: 10 });

                expect(document.activeElement.setSelectionRange).not.toHaveBeenCalled();
                expect(document.activeElement.blur).not.toHaveBeenCalled();
            });

            xit('should hide the keyboard (wrong active element).', () => {
                this.view.$('button').focus();

                expect(document.activeElement.tagName).toBe('BUTTON');
                expect(document.activeElement).toBeDefined();
                expect(document.activeElement.setSelectionRange).toBeUndefined();

                spyOn(document.activeElement, 'blur');

                this.view.hideKeyboardOnEnter({ keyCode: 13 });

                expect(document.activeElement.blur).toHaveBeenCalled();
            });

            it('should hide the keyboard (wrong active element).', () => {
                this.view.$('input').focus();
                expect(document.activeElement.tagName).toBe('INPUT');
                expect(document.activeElement).toBeDefined();

                spyOn(document.activeElement, 'blur');
                spyOn(document.activeElement, 'setSelectionRange');

                this.view.hideKeyboardOnEnter({ keyCode: 13 });

                expect(document.activeElement.setSelectionRange).toHaveBeenCalledWith(0, 0);
                expect(document.activeElement.blur).toHaveBeenCalled();
            });
        });
    }
}
