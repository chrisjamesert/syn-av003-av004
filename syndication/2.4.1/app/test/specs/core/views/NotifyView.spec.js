import NotifyView from 'core/views/NotifyView';
import ModalViewTests from './ModalView.specBase';
import * as specHelpers from 'test/helpers/SpecHelpers';

class NotifyViewTests extends ModalViewTests {
    beforeEach () {
        this.view = new NotifyView();

        this.removeTemplate = specHelpers.renderTemplateToDOM('core/templates/notify.ejs');

        return Q();
    }

    afterEach () {
        this.removeTemplate();

        return Q();
    }

    testShow (strings) {
        describe('method:show', () => {
            Async.it('should show the modal.', () => {
                spyOn(this.view, 'render').and.stub();
                spyOn(this.view, 'isShown').and.returnValue(false);

                let request = this.view.show(strings);

                expect(request).toBePromise();

                return Q.delay(600).then(() => {
                    // We don't need a resolve test, because it's tested here.
                    this.view.resolve();

                    return request.then(res => {
                        expect(res).toBe(true);
                        expect(this.view.render).toHaveBeenCalledWith(strings);
                    });
                });
            });
        });
    }
}

describe('NotifyView', () => {
    let strings = {
            header: 'Hello World',
            body: 'Hellow World',
            ok: 'OK'
        };

    let suite = new NotifyViewTests();

    suite.testId('modal');
    suite.testClass('modal');
    suite.testAttribute('role', 'dialog');
    suite.testAttribute('aria-hidden', 'true');
    suite.testAttribute('data-backdrop', 'static');
    suite.testGetTemplate();
    suite.testTeardown();
    suite.testShow(strings);
    suite.testAddGlyphicon();
    suite.testFindModalElement(strings);
    suite.testAddModalClass();
    suite.testApplyStyles({ type: 'error', icon: 'minus-circle'});
    suite.testApplyStyles({ type: 'success', icon: 'check-circle'});
    suite.testApplyStyles({ type: 'warning', icon: 'exclamation-triangle'});
    suite.testHide();
});
