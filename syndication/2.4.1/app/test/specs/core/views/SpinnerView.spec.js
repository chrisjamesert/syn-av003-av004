import SpinnerView from 'core/views/SpinnerView';
import ModalViewTests from './ModalView.specBase';
import * as specHelpers from 'test/helpers/SpecHelpers';

class SpinnerViewTests extends ModalViewTests {

    beforeEach () {
        this.view = new SpinnerView();

        this.removeTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        return Q();
    }

    afterEach () {
        this.removeTemplate();

        return Q();
    }

    testShow (strings) {
        describe('method:show', () => {
            Async.it('should show the modal.', () => {
                spyOn($.fn, 'modal');
                spyOn(this.view, 'render').and.stub();
                spyOn(this.view, 'isShown').and.returnValue(false);
                spyOn(this.view, 'i18n').and.resolve(strings);

                let request = this.view.show(strings);

                expect(request).toBePromise();

                return request.then(() => {
                    expect($.fn.modal).toHaveBeenCalled();
                    expect(this.view.render).toHaveBeenCalledWith(strings);
                });
            });
        });
    }
}

describe('SpinnerView', () => {
    let strings = { text: 'Please wait...' };

    let suite = new SpinnerViewTests();

    suite.testId('modal');
    suite.testClass('modal');
    suite.testAttribute('role', 'dialog');
    suite.testAttribute('aria-hidden', 'true');
    suite.testAttribute('data-backdrop', 'static');
    suite.testGetTemplate();
    suite.testTeardown();
    suite.testShow(strings);
    suite.testAddGlyphicon();
    suite.testAddModalClass();
    suite.testApplyStyles({ type: 'error', icon: 'minus-circle'});
    suite.testApplyStyles({ type: 'success', icon: 'check-circle'});
    suite.testApplyStyles({ type: 'warning', icon: 'exclamation-triangle'});
    suite.testHide();
});
