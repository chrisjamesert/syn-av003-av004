import { refresh } from 'core/actions/refresh';

xdescribe('refresh', () => {
    beforeAll(() => {
        spyOn(window.location, 'reload').and.stub();
    });

    it('window.location.refresh should have been called', () => {
        refresh();
        expect(window.location.reload).toHaveBeenCalled();
    });
});
