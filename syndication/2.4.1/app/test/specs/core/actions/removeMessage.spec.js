import { removeMessage } from 'core/actions/removeMessage';
import Spinner from 'core/Spinner';

describe('removeMessage', () => {
    beforeAll(() => {
        spyOn(Spinner, 'hide');
    });

    Async.it('Spinner.hide() should have been called', () => {
        return removeMessage({})
        .then(() => {
            expect(Spinner.hide).toHaveBeenCalled();
        });
    });
});
