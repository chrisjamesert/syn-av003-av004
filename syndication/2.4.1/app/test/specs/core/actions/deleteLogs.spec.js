import { deleteLogs } from 'core/actions/deleteLogs';
import Logs from 'core/collections/Logs';

describe('deleteLogs', ()=> {
    let deleteBatch,
        deleteBatchWithoutLogs,
        deleteBatchWithZeroID,
        test = {
            cbFunction: () => {
                return 'someDummyValue';
            }
        };

    beforeEach(() => {
        deleteBatch = {
            attributes: {
                params: new Logs([
                    {
                        id: 100,
                        type: 'MINOR',
                        level: 'DEBUG',
                        message: 'This is a test log message1',
                        clientTime: '2017-02-10T20:26:57.429Z'
                    },
                    {
                        id: 101,
                        type: 'MINOR',
                        level: 'WARN',
                        message: 'This is a test log message2',
                        clientTime: '2017-02-10T20:26:57.429Z'
                    }
                ])
            }
        }

        deleteBatchWithoutLogs = {
            attributes: {
                params: new Logs()
            }
        }

        deleteBatchWithZeroID = {
            attributes: {
                params: new Logs([
                    {
                        id: 0,
                        type: 'MINOR',
                        level: 'DEBUG',
                        message: 'This is a test log message1',
                        clientTime: '2017-02-10T20:26:57.429Z'
                    }
                ])
            }
        }
    });

    Async.it('Should call the Logs.prototype.destroy function', () => {
        spyOn(Logs.prototype, 'destroy');
        return Q.promise(resolve => {
            deleteLogs(deleteBatch, () => {});
            resolve();
        }).then(() => {
            expect(Logs.prototype.destroy).toHaveBeenCalled();
        });
    });

    Async.it('Should not call the Logs.prototype.destroy function', () => {
        spyOn(Logs.prototype, 'destroy');
        return Q.Promise(resolve => {
            deleteLogs(deleteBatchWithoutLogs, () => {});
            resolve();
        }).then(() => {
            expect(Logs.prototype.destroy).not.toHaveBeenCalled();
        });
    });

    Async.it('Should not call the Logs.prototype.destroy function', () => {
        spyOn(Logs.prototype, 'destroy');
        spyOn(test, 'cbFunction');
        return Q.Promise(resolve => {
            deleteLogs(deleteBatchWithZeroID, test.cbFunction);
            resolve();
        }).then(() => {
            expect(Logs.prototype.destroy).not.toHaveBeenCalled();
            expect(test.cbFunction).toHaveBeenCalled();
        });
    });
});

