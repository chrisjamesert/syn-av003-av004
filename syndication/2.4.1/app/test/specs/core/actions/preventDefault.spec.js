import { preventDefault } from 'core/actions/preventDefault';

describe('preventDefault', () => {
    it('should return an object literal with preventDefault set to true.', () => {
        expect(preventDefault()).toEqual({ preventDefault: true });
    });
});
