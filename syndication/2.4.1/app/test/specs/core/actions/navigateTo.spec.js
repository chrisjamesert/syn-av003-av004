import { navigateTo } from 'core/actions/navigateTo'

describe('navigateTo', () => {
    beforeEach(() => {
        LF.router = jasmine.createSpyObj('router', ['navigate']);
    });

    Async.it('Should navigate to dashboard view', () => {
        return Q.promise(resolve => {
            navigateTo('dashboard', () => {
                resolve();
            });
        })
        .then(() => {
            expect(LF.router.navigate).toHaveBeenCalledWith('dashboard', true);
        });
    });
});
