import { removeRules } from 'core/actions/removeRules'

describe('removeRules', () => {
    Async.it('Should do nothing when data does not have any value', () => {
        spyOn(ELF.rules,'remove');
        return Q.Promise(resolve => {
            removeRules({}, () => {
                expect(ELF.rules.remove).not.toHaveBeenCalled();
                resolve();
            });
        });
    });

    Async.it('Should remove specific rules included in the data', () => {
        spyOn(ELF.rules,'remove').and.stub();
        let data = {
            rules: ['NewPatientCancel', 'QuestionnaireTransmitNewPatient']
        };

        return Q.promise(resolve => {
            removeRules(data, () => {
                resolve();
            })
        })
        .then(() => {
            expect(ELF.rules.remove).toHaveBeenCalledWith('NewPatientCancel');
            expect(ELF.rules.remove).toHaveBeenCalledWith('QuestionnaireTransmitNewPatient');
        });
    });
});
