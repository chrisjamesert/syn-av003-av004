import { syncUsers } from 'core/actions/syncUsers';
import * as syncHistoricalData from 'core/actions/syncHistoricalData';

describe('syncUsers', () => {
    beforeAll(() => {
        spyOn(syncHistoricalData, 'syncHistoricalData').and.resolve();
    });

    Async.it('should have been called syncHistoricalData with', () => {
        let params = {
            collection: 'Users',
            webServiceFunction: 'syncUsers',
            activation: false
        };

        return syncUsers().then(() => {
            expect(syncHistoricalData.syncHistoricalData).toHaveBeenCalledWith(params, false);
        });
    });
});
