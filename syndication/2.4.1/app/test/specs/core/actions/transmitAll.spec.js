import { transmitAll } from 'core/actions/transmitAll';
import Transmissions from 'core/collections/Transmissions';

describe('transmitAll', () => {

    it('Should invoke the pullQueue method', () => {
        spyOn(Transmissions.prototype, 'pullQueue');
        transmitAll({}, () => {});
        expect(Transmissions.prototype.pullQueue).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('Should called the LF.Wrapper.exec method', () => {
        spyOn(LF.Wrapper, 'exec');
        transmitAll({}, () => {});
        expect(LF.Wrapper.exec).toHaveBeenCalledWith(jasmine.any(Object));
    });
});
