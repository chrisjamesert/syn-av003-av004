import Answers from 'core/collections/Answers';
import Spinner from 'core/Spinner';
import COOL from 'core/COOL';
import Transmit from 'core/transmit';
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';

import { timeSlipCheck } from 'core/actions/timeSlipCheck';

const millisPerHour = 1000 * 60 * 60;
const millisPerMinute = 60 * 1000;

describe('Action:timeSlipCheck', () => {
    let context,
        savedMessageRepoDialog;

    beforeEach(() => {
        spyOn(localStorage, 'getItem').and.returnValue('en-US');
        spyOn(LF.Wrapper, 'exec');

        spyOn(Logger.prototype, 'error').and.callThrough();
        spyOn(Logger.prototype, 'operational').and.callThrough();

        LF.security = jasmine.createSpyObj('LF.security', ['pauseSessionTimeOut', 'restartSessionTimeOut']);
        LF.content = jasmine.createSpyObj('lF.content', ['setContextLanguage']);
        LF.Actions = jasmine.createSpyObj('LF.Actions', ['navigateTo']);
        savedMessageRepoDialog = MessageRepo.Dialog;
        MessageRepo.Dialog = { TIMESLIP_ERROR: 'Timeslip error' };
        spyOn(MessageRepo, 'display');
        spyOn(Spinner, 'hide').and.returnValue(Q());
        spyOn(Spinner, 'show').and.returnValue(Q());
        spyOn(ELF, 'trigger');
        spyOn(lStorage, 'getItem');
        spyOn(Transmit, 'getServerTime').and.returnValue;

        COOL.service('Transmit', Transmit);

        context = { data: {} };
    });

    afterEach(() => {
        MessageRepo.Dialog = savedMessageRepoDialog;
    });

    describe('preliminaries', () => {
        Async.it('should give up when the app has no serviceBase', () => {
            LF.Wrapper.exec.and.callFake((obj) => {
                obj.execWhenWrapped();
            });
            lStorage.getItem.and.returnValue(null);

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let message = 'No server info is available. There will be no check for time slip.';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(message);
            });
        });
    });

    describe('server time is not retrievable', () => {
        let testNow,
            serverTime;

        beforeEach(() => {
            LF.Wrapper.exec.and.callFake((obj) => {
                obj.execWhenWrapped();
            });
            lStorage.getItem.and.returnValue('http://www.example.com');

            testNow = new Date();
            serverTime = new Date(testNow.getTime() - 3 * millisPerMinute);
            let rejectMessage = `Time is an illusion, especially ${serverTime}`;
            Transmit.getServerTime.and.returnValue(Q.reject(rejectMessage));
        });

        Async.it('should report the failure to find server time', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                const expectedMessage = 'Retrieving server time failed, proceding with blind confirmation';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });

        Async.it('should only perform one time check since the server is unavailable', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                const firstCheckLog = 'Time is incorrect, performing another check to attempt to fix.';
                const secondCheckLog = 'Time is incorrect, it must be fixed before continuing.';

                expect(Logger.prototype.operational).not.toHaveBeenCalledWith(firstCheckLog);
                expect(Logger.prototype.operational).not.toHaveBeenCalledWith(secondCheckLog);
            });
        });

        Async.it('should immediately trigger the questionnaire and a blind check', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                expect(LF.Actions.navigateTo).toHaveBeenCalledWith('time-confirmation');
            });
        });
    });

    describe('device time is close to server time', () => {
        let testNow,
            serverTime;

        beforeEach(() => {
            LF.Wrapper.exec.and.callFake((obj) => {
                obj.execWhenWrapped();
            });
            lStorage.getItem.and.returnValue('http://www.example.com');

            testNow = new Date();
            serverTime = new Date(testNow.getTime() - 3 * millisPerMinute);
            Transmit.getServerTime.and.returnValue(Q(serverTime));
        });

        Async.it('should report the device time when it considers it', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let partialMessage = 'Device time:';
                let matchingLogEntries = Logger.prototype.operational.calls.all()
                    .map(e => e.args[0])
                    .filter(e => e.indexOf(partialMessage) !== -1);
                expect(matchingLogEntries.length).toBeGreaterThan(0);
            });
        });

        Async.it('should report the server time it finds', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = `Time from the server: ${serverTime}`;
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });

        Async.it('should report an error if there is a problem comparing dates', () => {
            // cause the server date log entry message to trigger an exception
            Logger.prototype.operational.and.callFake((entry) => {
                if (entry.indexOf('Device time:') !== -1) {
                    throw new Error('And time is out of joint! O cursed spite');
                }
            });

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedError = 'Error comparing dates';
                expect(Logger.prototype.error).toHaveBeenCalledWith(expectedError, jasmine.any(Object));
            });
        });

        Async.it('should accept the device time as close enoughs', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = 'Time is within 15 minutes of the device time. Continuing application.';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });
    });

    describe('device time is far off from server time - before questionnaire', () => {
        let testNow,
            serverTime;

        beforeEach(() => {
            LF.Wrapper.exec.and.callFake((obj) => {
                obj.execWhenWrapped();
            });
            lStorage.getItem.and.returnValue('http://www.example.com');

            testNow = new Date();
            serverTime = new Date(testNow.getTime() + 2 * millisPerHour);
            Transmit.getServerTime.and.returnValue(Q(serverTime));
        });

        Async.it('should report the device time when it considers it', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let partialMessage = 'Device time:';
                let matchingLogEntries = Logger.prototype.operational.calls.all()
                    .map(e => e.args[0])
                    .filter(e => e.indexOf(partialMessage) !== -1);
                expect(matchingLogEntries.length).toBeGreaterThan(0);
            });
        });

        Async.it('should report the server time it finds', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = `Time from the server: ${serverTime}`;
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });

        Async.it('should check the server time twice, logging both attempts', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                const firstCheckLog = 'Time is incorrect, performing another check to attempt to fix.';
                const secondCheckLog = 'Time is incorrect, it must be fixed before continuing.';

                expect(Logger.prototype.operational).toHaveBeenCalledWith(firstCheckLog);
                expect(Logger.prototype.operational).toHaveBeenCalledWith(secondCheckLog);
            });
        });

        Async.it('should only check the server time once if it is a blind test', () => {
            let request = timeSlipCheck.call(context, { isBlind: true });
            expect(request).toBePromise();

            return request.then(() => {
                const firstCheckLog = 'Time is incorrect, performing another check to attempt to fix.';
                const secondCheckLog = 'Time is incorrect, it must be fixed before continuing.';

                expect(Logger.prototype.operational).not.toHaveBeenCalledWith(firstCheckLog);
                expect(Logger.prototype.operational).toHaveBeenCalledWith(secondCheckLog);
            });
        });

        Async.it('should recognize when the time has corrected itself by the second attempt', () => {
            const firstCheckLog = 'Time is incorrect, performing another check to attempt to fix.';
            const secondCheckLogFail = 'Time is incorrect, it must be fixed before continuing.';
            const checkLogSuccess = 'Time is within 15 minutes of the device time. Continuing application.';

            // stubbing/mocking the Javascript Date constructor is ill-advised: theoretically
            // it should be simple, but in practice there are way too many subtle gotchas.
            // A better solution would be to inject Date() as a dependency in the original code,
            // but changing the mock server time is probably cloe enough

            Logger.prototype.operational.and.callFake((entry) => {
                if (entry === firstCheckLog) {
                    serverTime = new Date(testNow.getTime() - 3 * millisPerMinute);
                    Transmit.getServerTime.and.returnValue(Q(serverTime));
                }
            });

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                expect(Logger.prototype.operational).toHaveBeenCalledWith(firstCheckLog);
                expect(Logger.prototype.operational).toHaveBeenCalledWith(checkLogSuccess);
                expect(Logger.prototype.operational).not.toHaveBeenCalledWith(secondCheckLogFail);
            });
        });
    });

    describe('the user has entered a time', () => {
        let testNow,
            serverTime,
            userTime,
            userTzOffset;

        let makeAnswers = (dateTimeValue, timeZoneValue) => {
            return new Answers([{
                SW_Alias: 'Time_Confirmation.0.DATE_TIME',
                question_id: 'TIME_CONFIRMATION_Q_1',
                questionnaire_id: 'Time_Confirmation',
                response: dateTimeValue
            }, {
                SW_Alias: 'Time_Confirmation.0.TIME_ZONE',
                question_id: 'TIME_CONFIRMATION_Q_2',
                questionnaire_id: 'Time_Confirmation',
                response: timeZoneValue
            }]);
        };

        let formatOffset = (offset) => {
            return `{ "timezone": "{ \\"offset\\": ${offset * millisPerHour} }" }`;
        };

        beforeEach(() => {
            LF.Wrapper.exec.and.callFake((obj) => {
                obj.execWhenWrapped();
            });
            lStorage.getItem.and.returnValue('http://www.example.com');

            testNow = new Date();
            serverTime = new Date(testNow.getTime() + 2 * millisPerHour);
            Transmit.getServerTime.and.returnValue(Q(serverTime));
            userTime = new Date(testNow.getTime() - 3 * millisPerMinute);
            userTzOffset = new Date().getOffset();
            context.answers = makeAnswers(userTime.toISOString(), formatOffset(userTzOffset));
        });

        Async.it('should report the device time when it considers it', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let partialMessage = 'Device time:';
                let matchingLogEntries = Logger.prototype.operational.calls.all()
                    .map(e => e.args[0])
                    .filter(e => e.indexOf(partialMessage) !== -1);
                expect(matchingLogEntries.length).toBeGreaterThan(0);
            });
        });

        Async.it('should report the time the user provided', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            const partialTimeMessage = 'Input time: ';
            const partialTimezoneMessage = 'Input timezone offset: ';

            return request.then(() => {
                let logEntries = Logger.prototype.operational.calls.all()
                    .map(e => e.args[0]);
                let partialCount = {};
                [partialTimeMessage, partialTimezoneMessage].forEach((tag) => {
                    partialCount[tag] = logEntries.filter(entry => entry.indexOf(tag) !== -1).length;
                });

                expect(partialCount[partialTimeMessage]).toBe(1);
                expect(partialCount[partialTimezoneMessage]).toBe(1);
            });
        });

        Async.it('should report an error if there is a problem comparing dates', () => {
            context.answers = makeAnswers('ceci n\'est pas d\'horodatage', 'ni décalage ni fuseau horaire');

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedError = 'Error comparing dates';
                expect(Logger.prototype.error).toHaveBeenCalledWith(expectedError, jasmine.any(Object));
            });
        });

        Async.it('should accept the user time as close enoughs', () => {
            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = 'Time is within 15 minutes of the device time. Continuing application.';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });

        Async.it('should log when the user time is not close enough', () => {
            userTime = new Date(testNow.getTime() - 6 * millisPerHour);
            context.answers = makeAnswers(userTime.toISOString(), formatOffset(userTzOffset));

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = 'Time is incorrect, performing another check to attempt to fix.';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });

        Async.it('should format the timezone offset as +HH:MM, ex. 1 (DE17749)', () => {
            userTime = new Date(testNow.getTime() - 6 * millisPerHour);
            userTzOffset = 13.75;
            context.answers = makeAnswers(userTime.toISOString(), formatOffset(userTzOffset));

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = 'Input timezone offset: +13:45';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });

        Async.it('should format the timezone offset as +HH:MM, ex 2 (DE17749)', () => {
            userTime = new Date(testNow.getTime() - 6 * millisPerHour);
            userTzOffset = -2.5;
            context.answers = makeAnswers(userTime.toISOString(), formatOffset(userTzOffset));

            let request = timeSlipCheck.call(context, {});
            expect(request).toBePromise();

            return request.then(() => {
                let expectedMessage = 'Input timezone offset: -2:30';
                expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedMessage);
            });
        });
    });
});
