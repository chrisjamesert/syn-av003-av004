import populateFieldsByModel from 'core/actions/populateFieldsByModel';
import Answers from 'core/collections/Answers';
import * as utils from 'core/utilities';
import Subject from 'core/models/Subject';

describe('populateFieldsByModel', () => {
    let config = [{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
            model: 'subject',
            // Use the subject model's subject_id property to populate the response.
            // e.g. this.subject.get('subject_id');
            property: 'subject_id'
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
            model: 'subject',
            // Use the subject model's initials property to populate the response.
            // e.g. this.subject.get('initials');
            property: 'initials'
        }],
        context = {
            subject: new Subject({ subject_id: '001', initials: 'BW' }),
            data: {}
        };

    Async.it('should return configured answers', () => {
        spyOn(utils, 'getNested').and.callThrough();
        spyOn(Answers.prototype, 'add').and.callThrough();
        return populateFieldsByModel.call(context, config)
        .then(() => {
            expect(context.data.answers.size()).toBe(2);
            expect(context.data.answers.at(0).get('response')).toBe('001');
            expect(context.data.answers.at(0).get('SW_Alias')).toBe('PT.0.Patientid');
            expect(context.data.answers.at(0).get('question_id')).toBe('EDIT_PATIENT_ID');
            expect(context.data.answers.at(0).get('questionnaire_id')).toBe('Edit_Patient');

            expect(context.data.answers.at(1).get('response')).toBe('BW');
            expect(context.data.answers.at(1).get('SW_Alias')).toBe('PT.0.Initials');
            expect(context.data.answers.at(1).get('question_id')).toBe('EDIT_PATIENT_INITIALS');
            expect(context.data.answers.at(1).get('questionnaire_id')).toBe('Edit_Patient');

            expect(utils.getNested).toHaveBeenCalled();
            expect(Answers.prototype.add).toHaveBeenCalled();
        });
    });
});
