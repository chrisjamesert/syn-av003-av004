import { syncHistoricalData } from 'core/actions/syncHistoricalData';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';

describe('syncHistoricalData', () => {
    let collection,
        params;

    beforeEach(() => {
        collection = new Transmissions();
        params = {
            collection: 'LastDiaries',
            subject: {
                get: param => param
            }
        };
        collection.reset();
        spyOn(Transmission.prototype, 'save');
        spyOn(LF.Collection.Transmissions, 'fetchCollection');
        spyOn(Transmissions.prototype, 'findWhere').and.callThrough();
        spyOn(Transmissions.prototype, 'execute');
    });

    describe('successfully called syncHistoricalData', () => {
        beforeEach(() => {
            Transmission.prototype.save.and.callFake(function (model) {
                this.set(model);
                collection.add(this);
                return Q();
            });
            LF.Collection.Transmissions.fetchCollection.and.callFake(() => {
                return Q(collection);
            });
            Transmissions.prototype.execute.and.resolve();
        });

        Async.it('should have set up params.krpt', () => {
            return syncHistoricalData(params, false)
            .then(() => {
                expect(params.krpt).toEqual('krpt');
            });
        });

        Async.it('should have saved historical data', () => {
            return syncHistoricalData(params, false)
            .then(() => {
                expect(Transmission.prototype.save).toHaveBeenCalled();
                let model = collection.at(0);
                expect(model.get('params')).toBe(JSON.stringify(params));
                expect(model.get('created')).toEqual(jasmine.any(Number));
            });
        });

        Async.it('should have executed', () => {
            return syncHistoricalData(params, false)
            .then(() => {
                expect(LF.Collection.Transmissions.fetchCollection).toHaveBeenCalled();
                expect(Transmissions.prototype.findWhere).toHaveBeenCalled();
                expect(Transmissions.prototype.execute).toHaveBeenCalledWith(0);
            });
        });
    });

    Async.it('should have rejected from model.save', () => {
        Transmission.prototype.save.and.reject();
        LF.Collection.Transmissions.fetchCollection.and.callFake(() => {
            return Q(collection);
        });
        Transmissions.prototype.execute.and.resolve();

        return syncHistoricalData(params, false)
        .catch((e) => {
            expect(Transmission.prototype.save).toHaveBeenCalled();
            expect(LF.Collection.Transmissions.fetchCollection).not.toHaveBeenCalled();
            expect(Transmissions.prototype.findWhere).not.toHaveBeenCalled();
            expect(Transmissions.prototype.execute).not.toHaveBeenCalled();
        });
    });

    Async.it('should have rejected from fetchCollection', () => {
        Transmission.prototype.save.and.callFake(function (model) {
            this.set(model);
            collection.add(this);
            return Q();
        });
        LF.Collection.Transmissions.fetchCollection.and.reject();
        Transmissions.prototype.execute.and.resolve();

        return syncHistoricalData(params, false)
        .catch((e) => {
            expect(Transmission.prototype.save).toHaveBeenCalled();
            expect(LF.Collection.Transmissions.fetchCollection).toHaveBeenCalled();
            expect(Transmissions.prototype.findWhere).not.toHaveBeenCalled();
            expect(Transmissions.prototype.execute).not.toHaveBeenCalled();
        });
    });

    Async.it('should have rejected from rejectOnError and response.error', () => {
        Transmission.prototype.save.and.callFake(function (model) {
            this.set(model);
            collection.add(this);
            return Q();
        });
        LF.Collection.Transmissions.fetchCollection.and.callFake(() => {
            return Q(collection);
        });
        Transmissions.prototype.execute.and.reject({ error: 500 });

        return syncHistoricalData(params, true)
        .then(() => {
            fail('method syncHistoricalData should have been rejected.');
        })
        .catch((e) => {
            expect(Transmission.prototype.save).toHaveBeenCalled();
            expect(LF.Collection.Transmissions.fetchCollection).toHaveBeenCalled();
            expect(Transmissions.prototype.findWhere).toHaveBeenCalled();
            expect(Transmissions.prototype.execute).toHaveBeenCalled();
        });
    });
});
