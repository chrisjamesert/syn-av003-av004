import User from 'core/models/User';
import Users from 'core/collections/Users';
import handleUserSyncData from 'core/actions/handleUserSyncData';
import Logger from 'core/Logger';

TRACE_MATRIX('US6600').
describe('handleUserSyncData', () => {
    let collection = new Users(),
        adminUser = {
            userType: 'SiteUser',
            username: 'Thor',
            language: 'en-US',
            password: '36e368ca5df356e82efa7887d9176848',
            salt: '1e321604-d830-480e-9f34-298178f27c50',
            syncValue: 'DOM.287206.246',
            userId: 73,
            secretQuestion: '0',
            secretAnswer: 'hammer',
            active: 1,
            role: 'admin'
        },
        doSync = (users) => handleUserSyncData({ res: users, collection });

    beforeEach(() => {
        collection.reset();
    });

    describe('handleUserSyncData', () => {
        beforeAll(() => {
            // fake the save method of User model to prevent db operations
            spyOn(User.prototype, 'save').and.callFake(function (model) {
                return Q.Promise(resolve => {
                    this.set(model);
                    collection.add(this);
                    resolve();
                });
            });

            spyOn(Users.prototype, 'updateUserStatus');
        });

        Async.it('should save User', () => {
            return doSync([adminUser])
            .then(() => {
                let model = collection.at(0);

                expect(model.get('userType')).toBe(adminUser.userType);
                expect(model.get('username')).toBe(adminUser.username);
                expect(model.get('language')).toBe(adminUser.language);
                expect(model.get('password')).toBe(adminUser.password);
                expect(model.get('salt')).toBe(adminUser.salt);
                expect(model.get('syncValue')).toBe(adminUser.syncValue);
                expect(model.get('userId')).toBe(adminUser.userId);
                expect(model.get('secretQuestion')).toBe(adminUser.secretQuestion);
                expect(model.get('secretAnswer')).toBe(adminUser.secretAnswer);
                expect(model.get('active')).toBe(adminUser.active);
                expect(model.get('role')).toBe(adminUser.role);
            });
        });

        Async.it('should update User', () => {
            let data = _.clone(adminUser);

            _.extend(data, {
                userType: 'SomeUserType',
                language: 'en-US',
                password: 'ffffffffffffffffffffffffffffffff',
                salt: 'temp',
                syncValue: 'DOM.000000.000',
                secretQuestion: 'secret',
                secretAnswer: 'answer',
                active: 1,
                role: 'admin'
            });

            return doSync([adminUser])
            .then(() => {
                expect(collection.length).toBe(1);
            })
            .then(() => doSync([data]))
            .then(() => {
                let model = collection.at(0);

                expect(collection.length).toBe(1);
                expect(model.get('userType')).toBe(data.userType);
                expect(model.get('username')).toBe(data.username);
                expect(model.get('language')).toBe(data.language);
                expect(model.get('password')).toBe(data.password);
                expect(model.get('salt')).toBe(data.salt);
                expect(model.get('syncValue')).toBe(data.syncValue);
                expect(model.get('userId')).toBe(data.userId);
                expect(model.get('secretQuestion')).toBe(data.secretQuestion);
                expect(model.get('secretAnswer')).toBe(data.secretAnswer);
                expect(model.get('active')).toBe(data.active);
                expect(model.get('role')).toBe(data.role);
            });
        });

        Async.it('should call updateUserStatus', () => {
            return doSync([adminUser])
            .then(() => {
                expect(collection.size()).toBe(1);
                expect(Users.prototype.updateUserStatus).toHaveBeenCalled();
            });
        });
    });

    Async.it('should log an error ypon failure', () => {
        spyOn(Logger.prototype, 'error');
        spyOn(User.prototype, 'save').and.callFake(function (model) {
            return Q.Promise((resolve, reject) => {
                reject();
            });
        });

        return doSync([adminUser])
        .then(() => {
            expect(collection.size()).toBe(0);
            expect(Logger.prototype.error).toHaveBeenCalled();
        });
    });
});