import { defaultAction } from 'core/actions/defaultAction';

describe('defaultAction', () => {
    Async.it('should do nothing', () => {
        return Q.Promise(resolve => {
            defaultAction({}, () => {
               resolve();
            });
        })
    });
});
