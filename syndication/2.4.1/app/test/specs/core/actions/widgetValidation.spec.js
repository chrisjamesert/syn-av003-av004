import { widgetValidation } from 'core/actions/widgetValidation';
import Widget from 'core/models/Widget';
import Answer from 'core/models/Answer';

describe('widgetValidation', () => {

    let testCallback = {
        testCbFunction: (value) => {
            return value;
        }
    };
    let context, context2;

    beforeEach(() => {

        LF.Widget = {
            ValidationFunctions: jasmine.createSpyObj('ValidationFunctions', ['validate'])
        };
        context = {
            model: new Widget({
                validation: {
                    validationFunc: 'validate',
                    params: { name: 'Test'}
                }
            }),
            answer: new Answer()
        };
        context2 = {
            model: new Widget({
                validation: {
                    validationFunc: '',
                    params: { name: 'Test'}
                }
            }),
            answer: new Answer()
        };
    });

    Async.it('Should call the validation function', () => {
        LF.Widget.ValidationFunctions.validate.and.callFake((answer, params, callback) => {
            callback(params);
        });
        return Q.Promise(resolve => {
            widgetValidation.call(context, { }, (params) => {
                expect(params.name).toBe('Test');
                resolve();
            });
        });

    });

    Async.it('Should call the callback function', () => {
        spyOn(testCallback, 'testCbFunction');
        return Q.Promise(resolve => {
            widgetValidation.call(context2, {}, (res) => {
                expect(res).toBe(true);
            });
            resolve();
        });
    });
});
