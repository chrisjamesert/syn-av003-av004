import clearBanner from 'core/actions/clearBanner';
import { Banner } from 'core/Notify';

describe('clearBanner', () => {
    beforeAll(() => {
        spyOn(Banner, 'closeAll');
    });

    Async.it('Banner.closeAll should have been called', () => {
        return clearBanner()
        .then(() => {
            expect(Banner.closeAll).toHaveBeenCalled();
        });
    });
});
