import { openQuestionnaire } from 'core/actions/openQuestionnaire';

describe('openQuestionnaire', () => {
    var params = {
        questionnaire_id: 'Daily_Diary',
        schedule_id: 'Daily_DiarySchedule1'
    };

    beforeEach(() => {
        LF.router = jasmine.createSpyObj('router', ['navigate']);
    });

    it('Causes a timeout to be called', () => {
        jasmine.clock().install();

        openQuestionnaire(params, () => {});
        expect(LF.router.navigate).not.toHaveBeenCalled();
        jasmine.clock().tick(151);
        expect(LF.router.navigate).toHaveBeenCalled();

        jasmine.clock().uninstall();
    });

    Async.it('LF.router.navigate should have been called with', () => {
        return Q.promise(resolve => {
            openQuestionnaire(params, ()=> {
                expect(LF.router.navigate).toHaveBeenCalledWith('questionnaire/Daily_Diary/Daily_DiarySchedule1', true);
                resolve();
            });
        });
    });
});
