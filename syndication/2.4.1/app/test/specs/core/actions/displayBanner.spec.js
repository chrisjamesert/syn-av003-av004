import { MessageRepo } from 'core/Notify';
import displayBanner from 'core/actions/displayBanner';

describe('displayBanner', () =>{
    beforeAll(() => {
        spyOn(MessageRepo, 'display');
    });

    Async.it('MessageRepo.display should have been called with Success!', () =>{
        MessageRepo.Banner = { success: 'Success!' };
        return displayBanner('success')
        .then(() => {
            expect(MessageRepo.display).toHaveBeenCalledWith('Success!');
        });
    });
});
