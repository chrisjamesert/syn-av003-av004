import { updateCurrentContext } from 'core/actions/updateCurrentContext';
import Context from 'core/models/Context';
import CurrentContext from 'core/CurrentContext';
import Subjects from 'core/collections/Subjects';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('updateCurrentContext', () => {

    beforeAll(() => {
        resetStudyDesign();
        CurrentContext.init();
        spyOn(Context.prototype, 'get').and.callThrough();
        spyOn(Subjects.prototype, 'fetch');
        spyOn(LF.Collection.Users.prototype, 'fetch');
    });

    Async.it('CurrentContext().get().fetch() should have been called', () => {
        return updateCurrentContext()
        .then(() => {
            expect(Context.prototype.get).toHaveBeenCalledWith('subjects');
            expect(Context.prototype.get).toHaveBeenCalledWith('users');
            expect(Subjects.prototype.fetch).toHaveBeenCalled();
            expect(LF.Collection.Users.prototype.fetch).toHaveBeenCalled();
        });
    });
});
