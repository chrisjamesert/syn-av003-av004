import { updateAlarm } from 'core/actions/updateAlarm.js';
import * as helpers from 'core/Helpers';
import Schedules from 'core/collections/Schedules';
import { query } from 'core/utilities';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import ActiveAlarm from 'core/models/ActiveAlarm';
import AlarmManager from 'core/AlarmManager';
import Logger from 'core/Logger';

const sched = new Schedules([{
    id: 'LogPadSchedule',
    target: {
        objectType: 'questionnaire',
        id: 'LogPad'
    },
    scheduleFunction: 'checkRepeatByDateAvailability',
    phase: [
        'SCREENING'
    ],
    scheduleParams: {
        startAvailability: '01:00',
        endAvailability: '19:00'
    },
    alarmFunction: 'addAlwaysAlarm',
    alarmParams: {
        id: 1,
        time: '9:00',
        repeat: 'daily',
        subjectConfig: {
            minAlarmTime: '9:00',
            maxAlarmTime: '15:00',
            alarmRangeInterval: 30,
            alarmOffSubject: true
        }
    }
}, {
    id: 'LogPad1Schedule_01',
    target: {
        objectType: 'questionnaire',
        id: 'LogPad1'
    },
    scheduleFunction: 'checkRepeatByDateAvailability',
    phase: [
        'SCREENING'
    ],
    scheduleParams: {
        repeat: 'once',
        startAvailability: '00:01',
        endAvailability: '23:58'
    }
}]);

describe('updateAlarm', () => {
    let collection, params;
    beforeEach(() => {
        collection = new ActiveAlarms();
        params = {
            id: 'LogPad'
        };
        spyOn(ActiveAlarms, 'fetchCollection').and.callFake(() => {
            let alarm = {
                id: 1,
                schedule_id: 'LogPadSchedule',
                date: new Date(),
                repeat: 'daily',
                alarmTrigger: ''
            };

            collection.add(alarm);
            return Q(collection);
        });
        spyOn(_, 'find').and.callThrough();
        spyOn(helpers, 'getScheduleModels').and.returnValue(sched.models);
        spyOn(ActiveAlarms.prototype,'map').and.callThrough();
        spyOn(AlarmManager, 'cancelAlarm').and.callThrough();
        spyOn(ActiveAlarm.prototype, 'destroy');
        spyOn(Logger.prototype, 'error').and.callThrough();
    });

    afterEach(() => {
        collection.reset();
    });

    Async.it('should have update alarm when found matched schedule', () => {
        return updateAlarm(params)
        .then(() => {
            expect(ActiveAlarms.fetchCollection).toHaveBeenCalled();
            expect(helpers.getScheduleModels).toHaveBeenCalled();
            expect(_.find).toHaveBeenCalled();
            expect(ActiveAlarms.prototype.map).toHaveBeenCalled();
            expect(AlarmManager.cancelAlarm).toHaveBeenCalledWith(1);
            expect(ActiveAlarm.prototype.destroy).toHaveBeenCalled();
        });
    });

    Async.it('should not have been called destroy when repeat is once', () => {
        ActiveAlarms.fetchCollection.and.callFake(() => {
            let alarm = {
                id: 1,
                schedule_id: 'LogPadSchedule',
                date: new Date(),
                repeat: 'once',
                alarmTrigger: ''
            };
            collection.add(alarm);
            return Q(collection);
        });
        return updateAlarm(params)
            .then(() => {
                expect(ActiveAlarms.fetchCollection).toHaveBeenCalled();
                expect(helpers.getScheduleModels).toHaveBeenCalled();
                expect(_.find).toHaveBeenCalled();
                expect(ActiveAlarms.prototype.map).toHaveBeenCalled();
                expect(AlarmManager.cancelAlarm).toHaveBeenCalledWith(1);
                expect(ActiveAlarm.prototype.destroy).not.toHaveBeenCalled();
            });
    });

    Async.it('should do nothing when scheduleId does not matched ', () => {
        return updateAlarm('differentScheduleId')
        .then(() => {
            expect(ActiveAlarms.fetchCollection).toHaveBeenCalled();
            expect(helpers.getScheduleModels).toHaveBeenCalled();
            expect(_.find).toHaveBeenCalled();
            expect(ActiveAlarms.prototype.map).not.toHaveBeenCalled();
            expect(AlarmManager.cancelAlarm).not.toHaveBeenCalledWith(1);
        });
    });

    Async.it('should catch an error with rejected promise', () => {
        ActiveAlarms.fetchCollection.and.reject();
        return updateAlarm(params)
        .then(() => {
            expect(Logger.prototype.error).toHaveBeenCalledWith('Error updating alarms', undefined);
        });
    });

    Async.it('should catch an error with undefined params', () => {
        return updateAlarm()
        .then(() => {
            expect(Logger.prototype.error).toHaveBeenCalled();
        });
    });

    it('should have return a promise', () => {
        expect(updateAlarm(params)).toBePromise();
    });
});

