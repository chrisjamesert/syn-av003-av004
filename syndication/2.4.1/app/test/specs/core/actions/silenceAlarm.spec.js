import { silenceAlarm } from 'core/actions/silenceAlarm'

describe('silenceAlarm', () => {
    let test = {
        fakeFunction: (someObj) => {
            return someObj;
        }
    };
    beforeEach(() => {
        spyOn(test, 'fakeFunction');
    });
    it('Should do nothing when there is no callback function', () =>{
        silenceAlarm();
        expect(test.fakeFunction).not.toHaveBeenCalled();
    });
    it('Should execute the the callback function.', () => {
        silenceAlarm('', test.fakeFunction);
        expect(test.fakeFunction).toHaveBeenCalled();
    });
});
