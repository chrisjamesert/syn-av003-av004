import newUserSave from 'core/actions/newUserSave';
import Answers from 'core/collections/Answers';
import User from 'core/models/User';
import Users from 'core/collections/Users';
import Transmission from 'core/models/Transmission';
import Logger from 'core/Logger';
import { Banner, MessageRepo } from 'core/Notify';
import setupCoreMessages from 'core/resources/Messages';

import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('Action:newUserSave', () => {
    let context;

    beforeAll(() => {
        setupCoreMessages();
    });

    afterAll(() => {
        MessageRepo.clear();
    });

    beforeEach(() => {
        setupCoreMessages();
        resetStudyDesign();

        spyOn(Logger.prototype, 'error').and.stub();

        context = { data: {} };

        context.data.answers = new Answers([
            { id: 1, question_id: 'AFFIDAVIT', SW_Alias: 'New_User.0.AFFIDAVIT' },
            { response: JSON.stringify({ username: 'Site Administrator' }) },
            { response: JSON.stringify({ role: 'admin' }) },
            { response: JSON.stringify({ secretQuestion: '0' }) },
            { response: JSON.stringify({ secretAnswer: 'qqqq' }) },
            //{ response: { language: 'en-US' }},
            { response: JSON.stringify({ password: '12345' }) }
        ]);

        LF.StudyDesign.defaultUserValues = { language: 'en-US' };
    });

    Async.it('should fail to create the user.', () => {
        spyOn(User.prototype, 'save').and.reject('DatabaseError');

        let request = newUserSave.call(context, {});

        expect(request).toBePromise();

        return request.then((evt) => {
            expect(User.prototype.save).toHaveBeenCalledWith({
                username: 'Site Administrator',
                role: 'admin',
                password: jasmine.any(String),

                // Tests default value set in study design.
                language: 'en-US',
                secretQuestion: '0',
                secretAnswer: hex_sha512('qqqq'),

                userType: 'SiteUser',
                active: 1,
                salt: jasmine.any(String),
                syncValue: ''
            });

            expect(Logger.prototype.error).toHaveBeenCalledWith('Action:newUserSave failed.', 'DatabaseError');
        });
    });

    Async.it('should fail to fetch existing users.', () => {
        spyOn(User.prototype, 'save').and.callFake(function (answers) {
            this.set(answers);

            return Q();
        });
        spyOn(Users.prototype, 'fetch').and.reject('DatabaseError');

        let request = newUserSave.call(context, {});

        expect(request).toBePromise();

        return request.then((evt) => {
            expect(Users.prototype.fetch).toHaveBeenCalledWith({
                search: { where: { role: 'admin' } }
            });
            expect(Logger.prototype.error).toHaveBeenCalledWith('Action:newUserSave failed.', 'DatabaseError');
        });
    });

    Async.it('should fail to create the transmission record.', () => {
        spyOn(User.prototype, 'save').and.callFake(function (answers) {
            this.set(answers);
            this.set('id', 1);

            return Q();
        });
        spyOn(Users.prototype, 'fetch').and.resolve();
        spyOn(Transmission.prototype, 'save').and.reject('DatabaseError');

        let request = newUserSave.call(context, {});

        expect(request).toBePromise();

        return request.then(evt => {
            expect(Transmission.prototype.save).toHaveBeenCalled();
            expect(Logger.prototype.error).toHaveBeenCalledWith('Action:newUserSave failed.', 'DatabaseError');
        });
    });

    Async.it('should display a banner and resolve.', () => {
        spyOn(User.prototype, 'save').and.callFake(function (answers) {
            this.set(answers);
            this.set('id', 1);

            return Q();
        });
        spyOn(Users.prototype, 'fetch').and.resolve();
        spyOn(Transmission.prototype, 'save').and.resolve();
        spyOn(Banner, 'success').and.stub();

        let request = newUserSave.call(context, {});

        expect(request).toBePromise();

        return request.then(evt => {
            expect(Banner.success).toHaveBeenCalledWith('NEW_USER_CREATED', jasmine.anything());
            expect(Logger.prototype.error).not.toHaveBeenCalled();
        });
    });
});
