import Cookie from 'core/classes/Cookie';
import Logger from 'core/Logger';

describe('Cookie', () => {
    // use unique values on every test run
    let someValue = Date.now().toString()
        .concat('=+-/*%foo'); // and throw in some special chars

    describe('Deprecated API', () => {
        it('warns on construct', () => {
            spyOn(Logger.prototype, 'warn');
            let c = new Cookie();
            expect(Logger.prototype.warn).toHaveBeenCalled();
        });

        it('forwards to setCookie to Cookie.set', () => {
            spyOn(Logger.prototype, 'warn');
            let c = new Cookie();
            spyOn(Cookie, 'set').and.callThrough();
            c.setCookie('foo', someValue, 9);
            expect(Cookie.set).toHaveBeenCalledWith('foo', someValue, 9);
        });

        it('forwards to getCookie to Cookie.get', () => {
            spyOn(Logger.prototype, 'warn');
            let c = new Cookie();
            spyOn(Cookie, 'get').and.callThrough();
            let foo = c.getCookie('foo');
            expect(Cookie.get).toHaveBeenCalledWith('foo');
            expect(foo).toEqual(someValue);
        });
    });

    it('should create a cookie', () => {
        expect(Cookie.set('autoTestCookie', someValue, 1)).toBeFalsy();
    });

    it('should retrieve a cookie value', () => {
        expect(Cookie.get('autoTestCookie')).toEqual(someValue);
    });

    // This test is pointless, it proves nothing. Don't know how to prove
    // that cookie won't expire. Or that it does expire after n days, for that matter.
    //it('should create a cookie that doesn\'t expire', () => {
    //    expect(Cookie.set('autoTestCookie2', someValue)).toBeFalsy();
    //});

});
