import Logger from 'core/Logger';
import Log from 'core/models/Log';

describe('Logger', () => {
    let env = {};
    let startingStatics;

    beforeAll(() => {
        startingStatics = Logger.getGlobalsForTesting();
        spyOn(Logger, 'toConsole').and.stub();
        spyOn(Logger, 'getPath')
            .and.callFake(dotPath => env[dotPath]);
    });
    beforeEach(() => {
        Logger.setGlobalsForTesting(startingStatics);
        env = {};
    });

    afterAll(() => {
        Logger.setGlobalsForTesting(startingStatics);
    });

    let logger = new Logger('TestNvps');

    xit('rejects invalid log level for consoleLevel', () => {
        // todo
        // create logger with consoleLevel: 'FOOBAR'
        // should result in consoleLevel: defaultLogLevel
        // should call logger.warn
    });

    xit('rejects invalid log level for subject', () => {
        // todo
        // change subject log level to 'FOOBAR'
        // should result in subjectLogLevel: defaultLogLevel
        // should call logger.warn
    });

    it('can retrieve static globals, for testing purposes', () => {
        let statics = Logger.getGlobalsForTesting();
        expect(_.difference(
                 ['t0', 'subjectLogLevel', 'logDepth'],
                _(statics).keys())
        )
        .toEqual([]);
    });

    it('can set indiviudual static globals, for testing purposes', () => {
        let starting = Logger.getGlobalsForTesting();

        Logger.setGlobalsForTesting({t0:123});
        expect(Logger.getGlobalsForTesting()).toEqual(jasmine.objectContaining(_.defaults({t0:123}, starting)));

        Logger.setGlobalsForTesting({subjectLogLevel:'TESTING', logDepth:55, garbage:'ignored'});
        expect(Logger.getGlobalsForTesting()).toEqual(
            jasmine.objectContaining(_.defaults({subjectLogLevel:'TESTING', logDepth:55}, {t0:123}, starting))
        );

        Logger.setGlobalsForTesting(starting);
        expect(Logger.getGlobalsForTesting()).toEqual(starting);
    });

    it('lets you set the global subject log level', () => {
        Logger.setSubjectLoggingLevel('ALL');
        expect(Logger.getGlobalsForTesting()).toEqual(jasmine.objectContaining({subjectLogLevel: 'ALL'}));
    });

    describe('method:isLevelPrintable', () => {
        it('considers operationaltoConsole setting', () => {
            let logger1 = new Logger('TestLogger2', {consoleLevel: 'INFO', operationalToConsole: false });
            expect(logger1.isLevelPrintable('OPERATIONAL')).toBe(false);
            let logger2 = new Logger('TestLogger2', {consoleLevel: 'INFO', operationalToConsole: true });
            expect(logger2.isLevelPrintable('OPERATIONAL')).toBe(true);
        });

        it('compares level against this.consoleLevel', () => {
            Logger.setGlobalsForTesting({logToConsole: true});
            Logger.levels.forEach(consoleLevel => {
                //console.log(consoleLevel);
                let satisfied = false;
                let logger2 = new Logger('TestLogger2', {consoleLevel: consoleLevel, operationalToConsole: true });
                Logger.levels.forEach(testLevel => {
                    if (testLevel === consoleLevel) {
                        satisfied = true;
                    }
                    //console.log('    ', testLevel, satisfied, logger2.isLevelPrintable(testLevel));
                    expect(logger2.isLevelPrintable(testLevel)).toBe(satisfied, [testLevel, consoleLevel].join(' vs. '));
                });
            });
        });
    });

    describe('method:isLevelStorable', () => {
        it('falls back to OPERATIONAL if not otherwise set', () => {
            // env = {
            //     'LF.StudyDesign.defaultLogLevel': undefined
            // };
            Logger.setGlobalsForTesting({defaultLogLevel: undefined});
            expect(logger.isLevelStorable('OPERATIONAL')).toBe(true);
        });

        it('compares level against defaultLogLevel', () => {
            Logger.levels.forEach(sysLevel => {
                let satisfied = false;
                //env = {'LF.StudyDesign.defaultLogLevel': sysLevel};
                Logger.setGlobalsForTesting({defaultLogLevel: sysLevel});

                Logger.levels.forEach(testLevel => {
                    if (testLevel === sysLevel) {
                        satisfied = true;
                    }
                    let priority = testLevel === 'OPERATIONAL';
                    //console.log(sysLevel, testLevel, satisfied || priority);
                    expect(logger.isLevelStorable(testLevel)).toBe(satisfied || priority);
                });
            });
        });

        it('uses subjectLogLevel if it is finer than sysLevel', () => {
            let l1 = new Logger('TestsubjectLogLevel');
            //env = {'LF.StudyDesign.defaultLogLevel': 'ERROR'};
            Logger.setGlobalsForTesting({
                subjectLogLevel:'INFO',
                defaultLogLevel: 'ERROR'
            });
            expect(l1.isLevelStorable('ERROR')).toBe(true);
            expect(l1.isLevelStorable('INFO')).toBe(true);
            expect(l1.isLevelStorable('DEBUG')).toBe(false);
        });
    });

    describe('method:nvpsForStorage', () => {

        let logger = new Logger('TestNvps');

        it('gives back undefined for undefined or null', () => {
            expect(logger.nvpsForStorage(null)).toBeUndefined();
            expect(logger.nvpsForStorage(undefined)).toBeUndefined();
        });

        it('wraps unknown types in an object with Data member', () => {
            expect(logger.nvpsForStorage('foo').Data).toEqual('foo');
            expect(logger.nvpsForStorage(5).Data).toEqual('5');
            expect(logger.nvpsForStorage(true).Data).toEqual('true');
        });

        it('stringifies and wraps arrays in an object with Data member', () => {
            expect(logger.nvpsForStorage(['a',1,true,{a:1}]).Data)
                        .toEqual('a,1,true,[object Object]');
        });

        it('simplifies objects to {str:str,...} form', () => {
            let data = {
                num: 1, bool: true,  none: null, str: 'FOO', obj: { x: 1, y: 2 },
                arr: [{ a: 3, b: 4 }, 'yes', false]
            };
            let simplified = logger.nvpsForStorage(data);
            expect(simplified.num).toEqual('1');
            expect(simplified.bool).toEqual('true');
            expect(simplified.none).toEqual('null');
            expect(simplified.str).toEqual('FOO');
            expect(simplified.obj).toEqual('[object Object]');
            expect(simplified.arr).toEqual('[object Object],yes,false');
        });

        it('simplifies an Error to {str:str,...} form' ,() => {
            let error;
            try {
                // Generate an error with a stack trace and all
                throw new Error('This is normal');
            } catch (e) {
                error = e;
            }

            let simplified = logger.nvpsForStorage(error);
        });
    });

    it('simplifies incoming NVPs before passing them on to db storage', () => {
        let error;
        Logger.getPath.and.returnValue({});
        env = {
            'LF.dataAccess': {}
        };
        try {
            // Generate an error with a stack trace and all
            throw new Error('This is normal');
        } catch (e) {
            error = e;
        }
        spyOn(logger, 'nvpsForStorage');
        spyOn(Log.prototype, 'save').and.stub();
        logger.operational('Here is an error', error);
        expect(logger.nvpsForStorage).toHaveBeenCalledWith(error);
    });

    it('disables storage when consoleOnly:true option is given', () => {
        env = {
            'LF.dataAccess': {}
        };
        // first, let's make sure the 'normal' case works
        let logger3 = new Logger('TestLogger3');
        spyOn(logger3, 'tryStore');
        logger3.error('important');
        expect(logger3.tryStore).toHaveBeenCalled();

        // now the real test
        let logger4 = new Logger('TestLogger3', {consoleOnly:true});
        spyOn(logger4, 'tryStore');
        logger4.error('important');
        expect(logger4.tryStore).not.toHaveBeenCalled();
    });

});

