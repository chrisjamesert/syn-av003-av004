import ELF from 'core/ELF';
import Logger from 'core/Logger';

describe('ELF.listen', () => {
    let methods = {
        normalAction (context) {
            return Q();
        },

        anotherNormalAction (context) {
            return Q();
        },

        REJECT (context) {
            return Q.reject(new Error('kinda badness'));
        },

        THROW (context) {
            throw new Error('real badness');
        },

        STOP_ACTIONS (context) {
            return Q({stopActions: true});
        },

        STOP_RULES (context) {
            return Q({stopRules: true});
        },

        PREVENT_DEFAULT (context) {
            return Q({preventDefault: true});
        }
    };

    beforeAll(() => {
        jasmine.addMatcher('toBeNormal', 'Expected result (NOT) to be normal: {}',
            (elfRes) => !elfRes.preventDefault && !elfRes.problems && !elfRes.stopRules
        );
        Logger.findLoggerNamed('ELF').reconfigure({consoleLevel: 'DEBUG', consoleOnly: false});
    });

    beforeEach(() => {
        ELF.rules.clear();
        _(methods).keys().forEach((name) => {
            spyOn(methods, name).and.callThrough();
            ELF.action(name, methods[name]);
        });
    });

    afterAll(() => {
        ELF.rules.clear();
    });

    Async.it('Hello World: listen and trigger', () => {
        ELF.listen('UNITTEST:Simple', methods.normalAction);
        return ELF.trigger('UNITTEST:Simple', {})
            .then((res) => {
                expect(methods.normalAction).toHaveBeenCalled();
                expect(res).toBeNormal();
            });
    });

    Async.it('Actions can be simple functions, not registered', () => {
        let myFunc = jasmine.createSpy('x', () => Q());
        ELF.listen('UNITTEST:Simple', myFunc);
        return ELF.trigger('UNITTEST:Simple', {})
            .then((res) => {
                expect(myFunc).toHaveBeenCalled();
                expect(myFunc.calls.count()).toBe(1);
                expect(res).toBeNormal();
            });
    });

    Async.it('Can listen to an array of events', () => {
        ELF.listen(['UNITTEST:Foo', 'UNITTEST:Simple', 'UNITTEST:Bar'], 'normalAction');
        return ELF.trigger('UNITTEST:Simple', {})
            .then((res) => {
                expect(methods.normalAction).toHaveBeenCalled();
                expect(res).toBeNormal();
            });
    });

    Async.it('Does not execute rules with non-matching trigger names', () => {
        ELF.listen('UNITTEST:Simple', 'normalAction');
        return ELF.trigger('UNITTEST:SomethingElse')
            .then((res) => {
                expect(methods.normalAction).not.toHaveBeenCalled();
                expect(res).toBeNormal();
            });
    });

    Async.it('Executes a list (array) of actions on a single rule', () => {
        ELF.listen('UNITTEST:Simple', ['normalAction', 'normalAction', 'normalAction']);
        return ELF.trigger('UNITTEST:Simple', {})
            .then((res) => {
                expect(methods.normalAction).toHaveBeenCalled();
                expect(methods.normalAction.calls.count()).toBe(3);
                expect(res).toBeNormal();
            });
    });

    Async.it('Handles multiple rules with same trigger', () => {
        ELF.listen('UNITTEST:Simple', ['normalAction']);
        ELF.listen('UNITTEST:Simple', ['anotherNormalAction']);
        return ELF.trigger('UNITTEST:Simple', {})
            .then((res) => {
                expect(methods.normalAction).toHaveBeenCalled();
                expect(methods.normalAction.calls.count()).toBe(1);
                expect(methods.anotherNormalAction).toHaveBeenCalled();
                expect(methods.anotherNormalAction.calls.count()).toBe(1);
                expect(res).toBeNormal();
            });
    });

    // DE19405
    Async.xit('Does not allow actions to return arbitrary data to triggerer', () => {
        ELF.listen('UNITTEST:Simple', () => Q({garbage: 'junk'}));
        return ELF.trigger('UNITTEST:Simple', {})
            .then((res) => {
                expect(res.garbage).not.toBeDefined();
            });
    });


    // In the following tests, the 'x' is there because at one point
    // multiple rules with same trigger was not working, and it was
    // invalidating these tests. The 'x' makes these tests independent
    // of the uniqueness issue.
    describe('-> abnormal terminations', () => {
        Async.it('propagates preventDefault to core (or whatever triggerer), does not affect any rules', () => {
            ELF.listen('UNITTEST:Simple', ['PREVENT_DEFAULT', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.normalAction).toHaveBeenCalled();
                    expect(methods.anotherNormalAction).toHaveBeenCalled();
                    expect(res.preventDefault).toBe(true, 'res.preventDefault');
                });
        });

        Async.it('A rejection from an action stops an action chain', () => {
            ELF.listen('UNITTEST:Simple', ['normalAction', 'REJECT', 'anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.normalAction).toHaveBeenCalled();
                    expect(methods.anotherNormalAction).not.toHaveBeenCalled();
                });
        });

        Async.it('A rejection from an action does not prevent further rules from running', () => {
            ELF.listen('UNITTEST:Simple', ['REJECT', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.anotherNormalAction).toHaveBeenCalled();
                });
        });

        Async.it('A rejection from an action propagates as "problems" to the triggerer', () => {
            ELF.listen('UNITTEST:Simple', ['REJECT', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(res.problems).toBeArray('res.problems');
                    expect(res.problems.length).toEqual(1, 'problems.length');
                    expect(String(res.problems[0])).toMatch(/.*kinda badness.*/);
                });
        });

        Async.it('An error thrown by an action stops the current action chain', () => {
            ELF.listen('UNITTEST:Simple', ['THROW', 'normalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.normalAction).not.toHaveBeenCalled();
                    expect(res.problems).toBeDefined('res.problems');
                });
        });

        Async.it('An error thrown by an action does not prevent further rules from running', () => {
            ELF.listen('UNITTEST:Simple', ['THROW', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.anotherNormalAction).toHaveBeenCalled();
                });
        });

        Async.it('An error thrown by an action propagates as "problems" to the triggerer', () => {
            ELF.listen('UNITTEST:Simple', ['THROW', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(res.problems).toBeArray('res.problems');
                    expect(res.problems.length).toEqual(1, 'problems.length');
                    expect(String(res.problems[0])).toMatch(/.*real badness.*/);
                });
        });

        Async.it('stopActions ends current action chain', () => {
            ELF.listen('UNITTEST:Simple', ['STOP_ACTIONS', 'normalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.normalAction).not.toHaveBeenCalled();
                    expect(res).toBeNormal();
                });
        });

        Async.it('stopActions does not prevent further rules from running', () => {
            ELF.listen('UNITTEST:Simple', ['STOP_ACTIONS', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.anotherNormalAction).toHaveBeenCalled();
                    expect(res).toBeNormal();
                });
        });

        Async.it('stopRules prevents further rules from being processsed, but current rule finishes', () => {
            ELF.listen('UNITTEST:Simple', ['STOP_RULES', 'normalAction']);
            ELF.listen(['UNITTEST:Simple', 'x'], ['anotherNormalAction']);
            return ELF.trigger('UNITTEST:Simple', {})
                .then((res) => {
                    expect(methods.normalAction).toHaveBeenCalled();
                    expect(methods.anotherNormalAction).not.toHaveBeenCalled();
                    expect(res).toBeNormal();
                });
        });
    });

    afterEach(() => {
        //console.log(JSON.stringify(ELF.rules.all(), null, 2));
    });
});

