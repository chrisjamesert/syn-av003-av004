import * as transmitLogsAction from 'core/actions/transmitLogs';
import WebService from 'core/classes/WebService';
import * as helpers from 'test/helpers/SpecHelpers';
import Logs from 'core/collections/Logs';
import Session from 'core/classes/Session';
import User from 'core/models/User';
import Logger from 'core/Logger';
import ObjectRef from 'core/classes/ObjectRef';

describe('transmitLogs', () => {
    let logs = new Logs();

    const generateLogs = (levels = []) => {
        return Q.all(levels.map(({ name, number = 10 }) => {
            Logger.setGlobalsForTesting({
                defaultLogLevel: name.toUpperCase()
            });

            return helpers.generateLogs(name, number);
        }))
        .catch(fail);
    };

    const testSpyCalls = (spy, expectLogNumber) => {
        expect(spy).toHaveBeenCalled();

        // There are a couple extra logs
        expect(spy.calls.all().reduce((totalLength, call) => {
            return totalLength + call.args[0].Json.length;
        }, 0)).toBeGreaterThan(expectLogNumber);
    };

    Async.beforeAll(() => {
        LF.appName = 'SitePad App';
        LF.coreVersion = '2.0.0';
        LF.StudyDesign = {
            studyProtocol: {
                defaultProtocol: 0,
                protocolList: new ObjectRef({
                    0: 'development'
                })
            },
            studyVersion: '1.0.0',
            clientName: 'ERT',
            logToConsole: false
        };
        LF.Preferred = { language: 'en', locale: 'US' };

        LF.security = new Session();
        LF.security.activeUser = new User({
            id: 1,
            role: 'admin'
        });

        return logs.clear()
        .catch(fail);
    });

    Async.afterEach(() => {
        return logs.clear()
        .catch(fail);
    });

    describe('transmit success', () => {
        let spy;

        beforeAll(() => {
            spy = spyOn(WebService.prototype, 'sendLogs').and.resolve();
        });

        afterEach(() => {
            spy.calls.reset();
        });

        Async.it('should transmit the logs.', () => {
            return generateLogs([{ name: 'error' }])
            .then(() => transmitLogsAction.transmitLogs({ level: 'ERROR' }))
            .then(() => {
                testSpyCalls(spy, 10);
            })
            .then(() => logs.fetch())
            .then(() => {
                expect(logs.size()).toBe(0);
            })
            .catch(fail);
        });

        Async.it('should transmit the logs (level[]).', () => {
            return generateLogs([{ name: 'error' }, { name: 'info' }])
            .then(() => transmitLogsAction.transmitLogs({ level: ['ERROR', 'INFO'] }))
            .then(() => {
                testSpyCalls(spy, 20);
            })
            .catch(fail);
        });

        Async.it('should transmit the logs (batch).', () => {
            return generateLogs([{ name: 'error', number: 30 }])
            .then(() => transmitLogsAction.transmitLogs({ level: 'ERROR' }))
            .then(() => {
                expect(JSON.stringify(spy.calls.argsFor(0)[0].Json).length <= 8000).toBe(true);
                testSpyCalls(spy, 31);
            })
            .catch(fail);
        });

        Async.it('should transmit the logs (batch, level[]).', () => {
            let number = 15;

            return generateLogs([{ name: 'error', number }, { name: 'info', number }])
            .then(() => transmitLogsAction.transmitLogs({ level: ['ERROR', 'INFO'] }))
            .then(() => {
                testSpyCalls(spy, 31);
            })
            .catch(fail);
        });
    });

    describe('transmit failure', () => {
        let spy,
            collection;

        beforeAll(() => {
            spy = spyOn(WebService.prototype, 'sendLogs').and.reject();
        });

        afterEach(() => {
            spy.calls.reset();
        });

        beforeEach(() => {
            collection = new Logs();
        });

        Async.it('shouldn\'t transmit any logs.', () => {
            return transmitLogsAction.transmitLogs({ level: 'ERROR' })
            .finally(() => {
                expect(spy).not.toHaveBeenCalled();
            })
            .catch(fail);
        });

        Async.it('should fail to transmit the logs.', () => {
            return generateLogs([{ name: 'error' }])
            .then(() => transmitLogsAction.transmitLogs({ level: 'ERROR' }))
            .then(() => collection.fetch())
            .then(() => {
                // Collection won't be cleared if transmission failed
                expect(collection.size()).toBe(10);
                testSpyCalls(spy, 10);
            });
        });

        Async.it('should fail to transmit the logs (batch).', () => {
            return generateLogs([{ name: 'error', number: 30 }])
            .then(() => transmitLogsAction.transmitLogs({ level: 'ERROR' }))
            .then(() => collection.fetch())
            .then(() => {
                // Collection won't be cleared if transmission failed
                expect(collection.size()).toBeGreaterThan(27);
                testSpyCalls(spy, 31);
            });
        });
    });
});
