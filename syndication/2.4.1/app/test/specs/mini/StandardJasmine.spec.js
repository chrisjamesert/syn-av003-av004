// This prevents Karma error from karma:mini due to no tests found
describe('Standard Jasmine Async Testing Style', () => {

    // The MANDATORY async spec style
    // runs a single Promise, terminates with .catch().done()
    it('passes your test when all is well', (done) => {
        Q()
        .delay(100)
        .tap(() => {
            expect(0).toEqual(0);
        })
        .catch(e => fail(e))
        .done(done);
    });

    // No good; badness happens outside the promise chain; done never called.
    xit('does not wrap your entire spec in try/catch', (done) => {
        // eslint-disable-next-line no-undef
        let badness = nonexistent.model.get('id');
        Q()
        .delay(100)
        .done(done);
    });

    // This spec does async stuff, but Jasmine doesn't know it.
    it('lets you do async stuff in a sync spec', () => {
        Q().delay(100);
    });

    // This spec does async stuff, but never terminates
    xit('does not guarantee done() will be called', (done) => {
        Q()
        .then(() => {
            // eslint-disable-next-line no-undef
            oopsie();
            done();
        });
    });

});
