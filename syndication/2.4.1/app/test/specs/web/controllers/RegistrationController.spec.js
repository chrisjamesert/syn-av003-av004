import RegistrationController from 'web/controllers/RegistrationController';
import State from 'web/classes/State';
import WebEULAView from 'web/views/WebEULAView';
import UnlockCodeView from 'web/views/UnlockCodeView';

describe('RegistrationController', () => {
    let ctrl;

    beforeEach(() => {
        ctrl = new RegistrationController();
    });

    TRACE_MATRIX('DE20704').
    describe('method:verifyAndGo', () => {
        Async.it('should navigate to the default route.', () => {
            spyOn(ctrl, 'navigate').and.stub();
            spyOn(State, 'get').and.returnValue(State.states.locked);

            let request = ctrl.verifyAndGo(State.states.endUserLicenseAgreements, 'WebEULAView', WebEULAView);

            expect(request).toBePromise();

            return request.then(() => {
                expect(ctrl.navigate).toHaveBeenCalledWith('');
            });
        });

        Async.it('should verify and navigate to the route.', () => {
            spyOn(State, 'get').and.returnValue(State.states.endUserLicenseAgreements);
            spyOn(ctrl, 'go').and.resolve();

            let request = ctrl.verifyAndGo(State.states.endUserLicenseAgreements, 'WebEULAView', WebEULAView);

            expect(request).toBePromise();

            return request.then(() => {
                expect(ctrl.go).toHaveBeenCalledWith('WebEULAView', WebEULAView, undefined);
            });
        });
    });

    TRACE_MATRIX('DE20704').
    describe('method:unlockCode', () => {
        Async.it('should display the UnlockCodeView.', () => {
            spyOn(ctrl, 'verifyAndGo').and.resolve();

            ctrl.unlockCode();

            // We need this to wait for the next application lifecycle tick.
            return Q().then(() => {
                expect(ctrl.verifyAndGo).toHaveBeenCalledWith(State.states.locked, 'UnlockCodeView', UnlockCodeView);
            });
        });
    });
});
