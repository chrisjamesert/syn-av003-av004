import LoginView from 'web/views/LoginView';
import SitePadLogin from 'sitepad/views/LoginView';
import CoreLogin from 'core/views/LoginView';
import Site from 'core/models/Site';
import User from 'core/models/User';
import Users from 'core/collections/Users';
import Transmissions from 'core/collections/Transmissions';
import { MessageRepo } from 'core/Notify';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class LoginViewSuite extends PageViewSuite {
    beforeAll () {
        resetStudyDesign();
        return specHelpers.initializeContent();
    }

    beforeEach () {
        // jscs:disable requireArrowFunctions
        // Long hand functions used in this case to retain the context of this to the spied upon objects.
        spyOn(SitePadLogin.prototype, 'resolve').and.callFake(function () {
            this.transmissions = new Transmissions();
            this.site = new Site({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });

        spyOn(Users.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                username: 'System Admin',
                role: 'admin'
            });

            return Q();
        });

        spyOn(LF.security, 'logout');

        this.view = new LoginView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('web/templates/login.ejs');
        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    DOMBeforeEach (options = { }) {
        let fullOptions = _.defaults(options, {
            inputType: 'text',
            siteNumber: '001',
            key: ''
        });
        return super.DOMBeforeEach(fullOptions);
    }

    afterEach () {
        this.removeTemplate();
        this.removeSpinnerTemplate();
        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(LoginViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testConstructor () {
        TRACE_MATRIX('DE20498')
        .describe('#constructor', () => {
            it('generates the proper event', () => {
                expect(this.view.events['input #user']).toBe('validate');
            });

            it('should log out of the session.', () => {
                expect(LF.security.logout).toHaveBeenCalled();
            });
        });
    }

    testLoginB () {
        TRACE_MATRIX('DE20887')
        .describe('method:login', () => {
            Async.it('should login to the application.', () => {
                spyOn(SitePadLogin.prototype, 'login').and.resolve();
                spyOn(this.view, 'filterSortUsers').and.callFake(() => {
                    return Q([new User({
                        id: 1,
                        username: 'System Admin'
                    })]);
                });

                this.view.$user.val('System Admin');

                let request = this.view.login();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(SitePadLogin.prototype.login).toHaveBeenCalled();
                });
            });

            Async.it('should invoke the render callback on entering wrong password for a correct user for any role', () => {
                spyOn(SitePadLogin.prototype, 'login').and.callFake((e, callback) => callback());
                spyOn(this.view, 'render').and.resolve();

                spyOn(this.view, 'filterSortUsers').and.callFake(() => {
                    return Q([new User({
                        id: 1,
                        username: 'System Admin',
                        role: 'admin'
                    })]);
                });

                this.view.$user.val('System Admin');
                this.view.$role.val('admin');

                let request = this.view.login();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(SitePadLogin.prototype.login).toHaveBeenCalled();
                    expect(this.view.render).toHaveBeenCalled();
                });
            });
        });
    }

    testResolveB () {
        describe('method:resolve', () => {
            Async.it('should resolve the request.', () => {
                let request = this.view.resolve();

                this.view.user = new User({ id: 1 });
                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.user).toBe(undefined);
                });
            });
        });
    }

    testRenderRoles () {
        TRACE_MATRIX('DE20551')
        .describe('method:renderRoles', () => {
            Async.it('should render the roles.', () => {
                spyOn($.fn, 'select2').and.stub();
                spyOn(this.view, 'i18n').and.resolve('Role');

                let request = this.view.renderRoles();

                return request.then(() => {
                    expect($.fn.select2).toHaveBeenCalled();
                });
            });
        });
    }

    testRoleChanged () {
        TRACE_MATRIX('DE20551')
        .describe('method:roleChanged', () => {
            it('should clear the password field.', () => {
                this.view.$password.val('Hello World');

                this.view.roleChanged();

                expect(this.view.$password.val()).toBe('');
            });
        });
    }

    testForgotPasswordB () {
        TRACE_MATRIX('DE21315')
        .describe('method:forgotPassword', () => {
            beforeEach(() => {
                spyOn(MessageRepo, 'display');
            });

            it('should display an error banner.', () => {
                this.view.forgotPassword();

                expect(MessageRepo.display).toHaveBeenCalled();
            });

            it('should display an error due to invalid role.', () => {
                spyOn(CoreLogin.prototype, 'forgotPassword');
                this.view.$user.val('System Admin');
                this.view.$role.val('site');

                this.view.forgotPassword();

                expect(MessageRepo.display).toHaveBeenCalled();
                expect(CoreLogin.prototype.forgotPassword).not.toHaveBeenCalled();
            });

            it('should invoke the CoreLogin.forgotPassword.', () => {
                spyOn(CoreLogin.prototype, 'forgotPassword');
                this.view.$user.val('System Admin');
                spyOn(this.view.$role, 'val').and.returnValue('admin');

                this.view.forgotPassword();

                Q.delay(200).then(() => {
                    expect(MessageRepo.display).not.toHaveBeenCalled();
                    expect(CoreLogin.prototype.forgotPassword).toHaveBeenCalled();
                });
            });
        });
    }
}

TRACE_MATRIX('US7650')
.describe('LoginView', () => {
    let suite = new LoginViewSuite();

    suite.executeAll({
        id: 'login-view',
        tagName: 'div',
        template: '#login-tpl',
        button: '#login',
        input: 'input#user',
        key: '',
        exclude: [
            'testAttribute'
        ]
    });
});
