import InactiveTabView from 'web/views/InactiveTabView';
import TabManager from 'web/classes/TabManager';
import { MessageRepo } from 'core/Notify';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class InactiveTabViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        this.removeTemplate = specHelpers.renderTemplateToDOM('web/templates/inactive-tab.ejs');

        this.view = new InactiveTabView();

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(InactiveTabViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testActivate () {
        TRACE_MATRIX('DE21784')
        .describe('method:activate', () => {
            beforeEach(() => {
                spyOn(MessageRepo, 'display').and.resolve();
                spyOn(TabManager.prototype, 'activateTab').and.stub();
                spyOn(this.view, 'refresh').and.stub();
            });

            Async.it('should activate the tab and navigate to the root of the application.', () => {
                let request = this.view.activate();

                expect(request).toBePromise();

                return request.finally(() => {
                    expect(this.view.refresh).toHaveBeenCalled()
                    expect(TabManager.prototype.activateTab).toHaveBeenCalled();
                });
            });

            Async.it('should not activate the tab.', () => {
                MessageRepo.display.and.reject();

                let request = this.view.activate();

                expect(request).toBePromise();

                return request.catch(() => {
                    expect(TabManager.prototype.activateTab).not.toHaveBeenCalled();
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request
                .then(() => fail('Method render should have been rejected.'))
                .catch((e) => {
                    expect(e).toBe('DOMError');
                });
            });
        });
    }
}

describe('InactiveTabView', () => {
    let suite = new InactiveTabViewSuite();

    suite.executeAll({
        id: 'inactive-tab-view',
        template: '#inactive-tab-template',
        button: '#activate',
        dynamicStrings: {

        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',

            // InactiveTabView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter',
            'testClearInputState'
        ]
    });
});
