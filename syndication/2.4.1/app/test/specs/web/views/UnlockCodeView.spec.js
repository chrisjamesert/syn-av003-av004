import UnlockCodeView from 'web/views/UnlockCodeView';
import Sites from 'core/collections/Sites';
import Site from 'core/models/Site';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import WebService from 'web/classes/WebService';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class UnlockCodeViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        spyOn($.fn, 'select2').and.stub();
        spyOn(Sites, 'fetchFirstEntry').and.callFake(() => {
            let site = new Site({
                id: 1,
                site_code: '001',
                siteCode: '001'
            });

            return Q(site);
        });

        // This is a workaround for a failing unit test.
        // UnlockCodeLockManager has a promise in the constructor, causing the failure.
        return Q()
        .then(() => {
            this.view = new UnlockCodeView();

            this.removeTemplate = specHelpers.renderTemplateToDOM('web/templates/unlock-code.ejs');

            return specHelpers.initializeContent()
        })
        .then(() => this.view.resolve())
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(UnlockCodeViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should resolve the promise and assign a site.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.site.toJSON()).toEqual({
                        id: 1,
                        site_code: '001',
                        siteCode: '001'
                    });
                });
            });

            Async.it('should reject the promise.', () => {
                Sites.fetchFirstEntry.and.reject('Database Error');
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Method resolve should have been rejected.'))
                .catch((err) => {
                    expect(err).toBe('Database Error');
                });
            });
        });
    }

    testInitForm () {
        describe('method:initForm', () => {
            it('should init the unlock code form.', () => {
                spyOn($.fn, 'submit').and.stub();

                this.view.initForm();

                expect(this.view.$form.submit).toHaveBeenCalledWith(jasmine.any(Function));
            });
        });
    }


    testChangeSite () {
        describe('method:changeSite', () => {
            it('should trigger an uninstall event.', () => {
                spyOn(ELF, 'trigger').and.resolve();

                this.view.changeSite();

                expect(ELF.trigger).toHaveBeenCalledWith('APPLICATION:Uninstall', {}, this.view);
            });
        });
    }

    testHandleFailedAttempt () {
        xdescribe('method:handleFailedAttempt', () => {
            beforeEach(() => {
                spyOn(MessageRepo, 'display').and.stub();
            });

            Async.it('should handle the failed entry attempt.', () => {
                spyOn(this.view, 'inputError').and.stub();
                
                let request = this.view.handleFailedAttempt();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.inputError).toHaveBeenCalledWith(this.view.$unlockCode);
                    expect(MessageRepo.display).toHaveBeenCalled();
                });
            });
        });
    }

    testSubmitHandler () {
        describe('method:submitHandler', () => {
            it('should submit the form.', () => {
                spyOn(this.view, 'submit').and.resolve();

                this.view.submitHandler();

                expect(this.view.submit).toHaveBeenCalled();
            });
        });
    }

    testSubmit () {
        describe('method:submit', () => {
            beforeEach(() => {
                // These are mocked to reduce running test time.
                spyOn(this.view.spinner, 'show').and.resolve();
                spyOn(this.view.spinner, 'hide').and.resolve();
            });

            Async.it('should navigate to the setup user view (valid unlock code).', () => {
                spyOn(WebService.prototype, 'validateUnlockCode').and.resolve({ res: true });
                spyOn(this.view, 'navigate').and.resolve();

                let request = this.view.submit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('setup-user');
                });
            });
        });
    }
}

describe('UnlockCodeView', () => {
    let suite = new UnlockCodeViewSuite();

    suite.executeAll({
        id: 'unlock-code-view',
        template: '#unlock-code-tpl',
        button: '#submit',
        dynamicStrings: {
            siteCode: '001'
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
