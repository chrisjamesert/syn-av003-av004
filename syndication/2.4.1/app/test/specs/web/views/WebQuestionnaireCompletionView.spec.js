import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import WebQuestionniareCompletionView from 'web/views/WebQuestionnaireCompletionView';

class WebQuestionnaireCompletionViewSuite {
    beforeEach () {
        beforeEach(() => {
            this.view = new WebQuestionniareCompletionView();
        });
    }

    testHistoryReplaceState () {
        TRACE_MATRIX('DE22251')
            .TRACE_MATRIX('DE22327')
            .describe('this.view.replaceState(null, null)', () => {
                Async.it('should replace the current history.state', () => {
                    spyOn(this.view, 'replaceState').and.stub();
                    spyOn(QuestionnaireCompletionView.prototype, 'transmitQuestionnaire').and.resolve();

                    let request = this.view.transmitQuestionnaire();
                    expect(request).toBePromise();
                    return request.then(() => {
                        expect(this.view.replaceState).toHaveBeenCalled();
                        expect(QuestionnaireCompletionView.prototype.transmitQuestionnaire).toHaveBeenCalled();
                    });
                });
            });
    }
}

describe('WebQuestionnaireCompletionViewSuite', () => {
    let suite = new WebQuestionnaireCompletionViewSuite();
    suite.beforeEach();
    suite.testHistoryReplaceState();
});
