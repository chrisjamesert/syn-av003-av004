import SiteAssignmentView from 'web/views/SiteAssignmentView';
import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';
import Site from 'core/models/Site';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class SiteAssignmentViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        this.serviceSpy = jasmine.createSpyObj('WebService', ['getSites', 'register']);
        this.serviceSpy.getSites.and.callFake(() => Q([]));

        this.transmitSpy = jasmine.createSpyObj('Transmit', ['getCountryCode']);

        spyOn(COOL, 'new').and.returnValue(this.serviceSpy);
        spyOn(COOL, 'getService').and.returnValue(this.transmitSpy);
        spyOn($.fn, 'select2').and.stub();
        spyOn($.fn, 'val').and.returnValue('en-US');

        this.view = new SiteAssignmentView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('web/templates/site-assignment.ejs');

        // TODO - This method throws an error due to missing localized strings.
        spyOn(this.view, 'createLanguageSelectionData').and.stub();
        spyOn(this.view, 'updateLanguage').and.stub();

        CurrentContext.init();

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SiteAssignmentViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testNextHandler () {
        TRACE_MATRIX('DE22430')
        .TRACE_MATRIX('DE22450')
        .describe('method:nextHandler', () => {
            Async.it('should invoke the next method and disable the next button.', () => {
                spyOn(this.view, 'next').and.resolve();
                spyOn(this.view, 'disableButton').and.stub();

                this.view.nextHandler({ preventDefault: $.noop });

                return Q().then(() => {
                    expect(this.view.next).toHaveBeenCalled();
                    expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$next);
                });
            });

            Async.it('should enable the next button upon error.', () => {
                spyOn(this.view, 'next').and.reject();
                spyOn(this.view, 'enableButton').and.stub();

                this.view.nextHandler({ preventDefault: $.noop });

                return Q().then(() => {
                    expect(this.view.next).toHaveBeenCalled();
                    expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$next);
                });
            });
        });
    }

    testNext () {
        TRACE_MATRIX('DE22241')
        .TRACE_MATRIX('DE22743')
        .describe('method:next', () => {
            Async.it('should register the browser as a device.', () => {
                this.view.sites.add({ siteCode: '0001' });
                $.fn.val.and.returnValue('0001');
                spyOn(Site.prototype, 'save').and.resolve();
                spyOn(lStorage, 'setItem').and.stub();
                this.serviceSpy.register.and.callFake(() => Q({
                    res: {
                        apiToken: 'apiToken',
                        regDeviceId: 'regDeviceId',
                        clientId: 'clientId'
                    }
                }));

                let request = this.view.next();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.transmitSpy.getCountryCode).toHaveBeenCalled();
                    expect(lStorage.setItem).toHaveBeenCalledWith('ucode', 'apiToken');
                    expect(lStorage.setItem).toHaveBeenCalledWith('apiToken', 'apiToken');
                    expect(lStorage.setItem).toHaveBeenCalledWith('deviceId', 'regDeviceId');
                    expect(lStorage.setItem).toHaveBeenCalledWith('clientId', 'clientId');
                    expect(lStorage.setItem).toHaveBeenCalledWith('IMEI', jasmine.any(String));
                });
            });
        });
    }
}

describe('SiteAssignmentView', () => {
    let suite = new SiteAssignmentViewSuite();

    suite.executeAll({
        id: 'site-assignment-view',
        template: '#site-assignment-tpl',
        button: '#next',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
