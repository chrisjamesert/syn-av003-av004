import SetSiteTimeZoneView from 'web/views/SetSiteTimeZoneView';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import Logger from 'core/Logger';
import Sites from 'core/collections/Sites';

import SetTimeZoneViewSuite from 'test/specs/core/views/SetTimeZoneView.specBase';

let preventDefault = $.noop;

class SetSiteTimeZoneViewSuite extends SetTimeZoneViewSuite {
    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        resetStudyDesign();
        this.removeTemplate = specHelpers.renderTemplateToDOM('web/templates/set-site-time-zone.ejs');

        this.view = new SetSiteTimeZoneView();

        this.sites = new Sites();

        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                krdom: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });
            return Q();
        });

        return specHelpers.initializeContent()
            .then(() => this.view.resolve())
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    /**
     * Execute all class unit tests and those of the parent class.
     * @param {Object} context Context used to annotate method parameters.cd
     */
    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SetSiteTimeZoneViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    /**
     * Tests the back method of the SetSiteTimeZoneView.
     */
    testBack () {
        describe('method:back', () => {
            it('should navigate to the login view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back({ preventDefault });

                expect(this.view.navigate).toHaveBeenCalledWith('login');
            });
        });
    }

    /**
     * Tests the chooseZone method of the SetSiteTimeZoneView.
     */
    testChooseZone () {
        describe('method:chooseZone', () => {
            beforeEach(() => {
                LF.Wrapper.platform = 'web';
                LF.StudyDesign.web = {
                    timeZoneOptions: [
                        { tzId: 'America/New_York', swId: 21 }
                    ]
                };
            });

            it('should enable next button.', () => {
                spyOn(this.view, 'enableButton');

                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$next);
            });
        });
    }

    /**
     * Tests the setTimeZone method of the SetSiteTimeZoneView.
     */
    testSetTimeZoneChild () {
        TRACE_MATRIX('DE20990').
        describe('method:setTimeZone', () => {
            beforeEach(() => {
                LF.Wrapper.platform = 'web';
                LF.StudyDesign.web = {
                    timeZoneOptions: [
                        { tzId: 'America/New_York', swId: 21 }
                    ]
                };
            });

            Async.it('should cancel to use the devices Time Zone.', () => {
                spyOn(this.view, 'confirm').and.resolve(false);
                spyOn(Logger.prototype, 'operational').and.stub();

                this.view.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    let tzFormat = moment.tz('America/New_York').format('G\\MT Z');
                    expect(Logger.prototype.operational).toHaveBeenCalledWith(`User canceled setting the time zone to: (${tzFormat}) America/New_York`);
                });
            });

            Async.it('should cancel to use the new Time Zone (new time zone selected).', () => {
                spyOn(this.view, 'confirm').and.resolve(false);
                spyOn(Logger.prototype, 'operational').and.stub();

                this.view.currentTimeZone = { id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone' };
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    let tzFormat = moment.tz('America/New_York').format('G\\MT Z');
                    expect(Logger.prototype.operational).toHaveBeenCalledWith(`User canceled setting the time zone to: (${tzFormat}) America/New_York`);
                });
            });
        });

        Async.it('should trigger a rule if user chooses to use a time zone same as that of the device', () => {
            spyOn(this.view, 'confirm').and.resolve(true);
            spyOn(ELF, 'trigger').and.resolve();
            spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                params.execWhenWrapped();
            });
            spyOn(Logger.prototype, 'operational').and.resolve();
            spyOn(Logger.prototype, 'error').and.stub();
            window.plugin.TimeZoneUtil.setTimeZone.and.callFake(onSuccess => onSuccess());
            window.plugin.TimeZoneUtil.restartApplication.and.callFake(onSuccess => onSuccess());

            this.view.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };
            this.view.$selectTimeZone.val('America/New_York').trigger('change');

            let request = this.view.setTimeZone({ preventDefault });

            expect(request).toBePromise();

            return request.then(() => {
                expect(ELF.trigger).toHaveBeenCalledWith('SETSITETIMEZONE:Transmit', {}, this.view);
                expect(Logger.prototype.error).not.toHaveBeenCalled();
            });
        });
    }
}

TRACE_MATRIX('US7712')
.describe('SetSiteTimeZoneView', () => {
    let suite = new SetSiteTimeZoneViewSuite();

    suite.executeAll({

        id: 'set-site-tz-view',
        template: '#set-site-time-zone-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',

            // PageView has no class name.
            'testClass',

            // This method doesn't apply to the child class due to DOM structure and configuration.
            'testChangeSelection',

            // This method is overidden by the child class and should not be tested.
            'testSetTimeZone',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
