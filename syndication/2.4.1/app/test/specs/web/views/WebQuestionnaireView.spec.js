import QuestionnaireView from 'core/views/QuestionnaireView';
import WebQuestionnaireView from 'web/views/WebQuestionnaireView';
import Visit from 'core/models/Visit';
import Dashboard from 'core/models/Dashboard';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

class WebQuestionnaireViewSuite {
    beforeEach () {
        beforeEach(() => {
            let visit = new Visit();

            resetStudyDesign();

            this.view = new WebQuestionnaireView({ visit });
            this.view.data.dashboard = new Dashboard();
        });
    }

    testPrepDashboardData () {
        TRACE_MATRIX('DE22236')
        .describe('method:prepDashboardData', () => {
            Async.it('should prefix the sig_id with "SA".', () => {
                spyOn(QuestionnaireView.prototype, 'prepDashboardData').and.resolve();

                let request = this.view.prepDashboardData();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(QuestionnaireView.prototype.prepDashboardData).toHaveBeenCalled();
                    expect(this.view.data.dashboard).toBeDefined();
                    expect(this.view.data.dashboard.get('sig_id').substr(0, 2)).toEqual('SA');
                });
            });

            Async.it('should prefix the sig_id with a custom value: "WB".', () => {
                spyOn(QuestionnaireView.prototype, 'prepDashboardData').and.resolve();
                LF.StudyDesign.web = { sigIdPrefix: 'WB' };

                let request = this.view.prepDashboardData();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(QuestionnaireView.prototype.prepDashboardData).toHaveBeenCalled();
                    expect(this.view.data.dashboard).toBeDefined();
                    expect(this.view.data.dashboard.get('sig_id').substr(0, 2)).toEqual('WB');
                });
            });
        });
    }
}

describe('WebQuestionnaireViewSuite', () => {
    let suite = new WebQuestionnaireViewSuite();

    suite.beforeEach();
    suite.testPrepDashboardData();
});
