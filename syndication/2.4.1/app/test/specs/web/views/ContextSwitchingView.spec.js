import ContextSwitchingView from 'web/views/ContextSwitchingView';
import SitePadContextSwitchingView from 'sitepad/views/ContextSwitchingView';
import User from 'core/models/User';
import * as lStorage from 'core/lStorage';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

class ContextSwitchingViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());
    }

    beforeEach () {
        resetStudyDesign();

        return specHelpers.initializeContent()
        .then(() => {
            this.view = new ContextSwitchingView();
        });
    }

    testForgotPassword () {
        TRACE_MATRIX('DE21169')
        .describe('forgotPassword', () => {
            it('should set the User_Login value in localStorage.', () => {
                spyOn(SitePadContextSwitchingView.prototype, 'forgotPassword').and.stub();

                this.view.user = new User({ id: 1 });
                this.view.forgotPassword();

                expect(SitePadContextSwitchingView.prototype.forgotPassword).toHaveBeenCalled();
            });
        });
    }

    testLogin () {
        TRACE_MATRIX('DE22267')
        .TRACE_MATRIX('DE22278')
        .describe('login', () => {
            it('should call PreventOffline rule on login.', () => {
                let preventOfflineCalled = false;

                spyOn(ELF, 'trigger').and.callFake((ruleName, input = {}, context = {}) => {
                    if (ruleName === 'WEB:PreventOffline') {
                        preventOfflineCalled = true;
                    }
                    return Q();
                });

                this.view.login();

                expect(preventOfflineCalled).toBeTruthy();
            });
        });
    }
}

TRACE_MATRIX('DE20581')
.describe('ContextSwitchingView', () => {
    const suite = new ContextSwitchingViewSuite();

    suite.testForgotPassword();
    suite.testLogin();
});
