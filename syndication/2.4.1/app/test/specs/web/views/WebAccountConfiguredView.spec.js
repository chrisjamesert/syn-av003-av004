import AccountConfiguredView from 'web/views/WebAccountConfiguredView';
import SitePadAccountConfiguredView from 'sitepad/views/AccountConfiguredView';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { MessageRepo } from 'core/Notify';
import ELF from 'core/ELF';

class AccountConfiguredViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());
    }

    beforeEach () {
        resetStudyDesign();

        return specHelpers.initializeContent()
        .then(() => {
            this.view = new AccountConfiguredView();
        });
    }

    testNext () {
        describe('method:next', () => {
            let preventOffline = {
                id: 'preventOffline',
                trigger: [
                    'WEB:PreventOffline',
                    'QUESTIONNAIRE:Navigate'
                ],
                salience: Number.MAX_SAFE_INTEGER,

                // mock offline scenario
                evaluate: false,
                resolve: [],
                reject: [
                    { action: 'displayBanner', data: 'CONNECTION_REQUIRED' },
                    { action: 'preventAll' }
                ]
            };

            MessageRepo.Banner = { CONNECTION_REQUIRED: 'CONNECTION_REQUIRED' };

            it('should trigger an ELF rule.', () => {
                spyOn(ELF, 'trigger').and.resolve();
                this.view.next();

                expect(ELF.trigger).toHaveBeenCalledWith('WEB:PreventOffline', {}, this.view);
            });

            Async.it('should display the banner.', () => {
                ELF.rules.add(preventOffline);
                MessageRepo.Banner = { CONNECTION_REQUIRED: 'CONNECTION_REQUIRED' };
                spyOn(MessageRepo, 'display').and.stub();

                return ELF.trigger('WEB:PreventOffline', {}, this.view).then(() => {
                    expect(MessageRepo.display).toHaveBeenCalledWith('CONNECTION_REQUIRED');
                });
            });
        });
    }
}

TRACE_MATRIX('DE22357')
.describe('AccountConfiguredView', () => {
    const suite = new AccountConfiguredViewSuite();

    suite.testNext();
});
