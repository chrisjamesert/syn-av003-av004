import URLParser from 'web/classes/URLParser';

class URLParserSuite {
    testParse () {
        describe('method:parse', () => {
            it('should parse the URL and output the correct values.', () => {
                let parsed = URLParser.parse('https://127.0.0.1:3010/users/1?sort=username&order=ASC');

                expect(parsed.protocol).toBe('https');
                expect(parsed.host).toBe('127.0.0.1:3010');
                expect(parsed.hostname).toBe('127.0.0.1');
                expect(parsed.port).toBe('3010');
                expect(parsed.pathname).toBe('/users/1');
                expect(parsed.search).toBe('?sort=username&order=ASC')
                expect(parsed.hash).toBe('');
                expect(parsed.searchObject).toEqual({
                    sort: 'username',
                    order: 'ASC'
                });

                parsed = URLParser.parse('https://127.0.0.1:3010#login');
                expect(parsed.hash).toBe('#login');
            });
        });
    }
}

describe('URLParser', () => {
    let suite = new URLParserSuite();

    suite.testParse();
});