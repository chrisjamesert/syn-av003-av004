import TabManager from 'web/classes/TabManager';
import { clearTabState } from 'web/actions/clearTabState';

TRACE_MATRIX('DE22527').
describe('clearTabState', () => {
    Async.it('should clear the tab state.', () => {
        spyOn(TabManager.prototype, 'clear').and.stub();

        let request = clearTabState();

        expect(request).toBePromise();

        return request.then(() => {
            expect(TabManager.prototype.clear).toHaveBeenCalled();
        });
    });
});