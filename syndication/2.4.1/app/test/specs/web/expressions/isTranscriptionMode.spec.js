import { isTranscriptionMode } from 'web/expressions/isTranscriptionMode';

describe('expression:isTranscriptionMode', () => {
    Async.it('shoud return true.', () => {
        let ctx = { transcriptionMode: true },
            request = isTranscriptionMode.call(ctx);

        expect(request).toBePromise();

        return request.then((res) => {
            expect(res).toBe(true);
        });
    });

    Async.it('shoud return true.', () => {
        let ctx = { transcriptionMode: false },
            request = isTranscriptionMode.call(ctx);

        expect(request).toBePromise();

        return request.then((res) => {
            expect(res).toBe(false);
        });
    });

    Async.it('shoud return false (undefined).', () => {
        let ctx = {},
            request = isTranscriptionMode.call(ctx);

        expect(request).toBePromise();

        return request.then((res) => {
            expect(res).toBe(false);
        });
    });
});