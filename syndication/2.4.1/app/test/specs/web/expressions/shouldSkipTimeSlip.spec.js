import shouldSkipTimeSlip from 'web/expressions/shouldSkipTimeSlip';

TRACE_MATRIX('DE22795')
.describe('expression:shouldSkipTimeSlip', () => {
    Async.it('should resolve true when checkForTimeSlip is false.', () => {
        LF.environment = { checkForTimeSlip: false };
        let request = shouldSkipTimeSlip();

        expect(request).toBePromise();

        return request.then((res) => {
            expect(res).toBe(true);
        });
    });

    Async.it('should resolve false by default.', () => {
        LF.environment = { };
        let request = shouldSkipTimeSlip();

        expect(request).toBePromise();

        return request.then((res) => {
            expect(res).toBe(false);
        });
    });
});
