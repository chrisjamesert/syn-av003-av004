#!/bin/bash

SIZES=(60 120 76 152 40 80 57 114 72 144 29 58 50 100 167 180)

for size in "${SIZES[@]}"; do
    convert source.png -resize "${size}x${size}" "icon-${size}.png"
done

mv icon-180.png icon-60@3x.png
mv icon-167.png icon-83.5@2x.png
mv icon-152.png icon-76@2x.png
mv icon-144.png icon-72@2x.png
mv icon-120.png icon-60@2x.png
mv icon-114.png icon-57@2x.png
cp icon-57.png icon.png
cp icon-57@2x.png icon@2x.png
mv icon-100.png icon-50@2x.png
mv icon-80.png icon-40@2x.png
mv icon-58.png icon-29@2x.png
cp icon-29.png icon-small.png
cp icon-29@2x.png icon-small@2x.png
