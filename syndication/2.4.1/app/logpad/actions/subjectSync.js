import COOL from 'core/COOL';
import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';
import Transmit from 'logpad/transmit';
import WebService from 'core/classes/WebService';

/**
 * Syncs subject data with the Web Service.
 * @memberOf ELF.actions/logpad
 * @method subjectSync
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{
 *     action: 'subjectSync'
 * }]
 */
export function subjectSync () {
    let users,
        service = COOL.new('WebService', WebService);

    // Since SW takes a while to process the diaries we need to delay the subject sync call.
    // This will allow narrowing window for sync failure.
    return Q.delay(500)
    .then(() => service.getkrDom({}))
    .then(({ res }) => COOL.getService('Transmit', Transmit).subjectSync(res))
    .then(() => {
        users = CurrentContext().get('users');
        return users.syncAdminPassword();
    })
    .then(() => users.updateAdminUser());
}

ELF.action('subjectSync', subjectSync);

// @todo remove
LF.Actions.subjectSync = subjectSync;
