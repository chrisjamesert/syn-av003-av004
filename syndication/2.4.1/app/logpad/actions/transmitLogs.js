import * as CoreTransmitLogs from 'core/actions/transmitLogs';
import CurrentSubject from 'core/classes/CurrentSubject';

/**
 * @memberOf ELF.actions/logpad
 * @method transmitLogs
 * @description
 * Get the correct device id and send it to transmitlogs
 * @param {Object} params - Parameters
 * @param {string} [params.device] - The device id.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'transmitLogs' }]
 */
export default function transmitLogs (params) {
    return CurrentSubject.getSubject()
    .then((subject) => {
        params.device = subject && subject.get('device_id');
        return CoreTransmitLogs.transmitLogs(params);
    });
}

ELF.action('transmitLogs', transmitLogs);

// @todo remove
LF.Actions.transmitLogs = transmitLogs;
