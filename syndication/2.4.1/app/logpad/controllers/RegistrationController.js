import BaseController from 'core/controllers/BaseController';

import InstallView from 'logpad/views/InstallView';
import CodeEntryView from 'logpad/views/CodeEntryView';
import ActivationView from 'logpad/views/ActivationView';
import ReactivationView from 'logpad/views/ReactivationView';
import PrivacyPolicyActivationView from 'logpad/views/PrivacyPolicyActivationView';
import LogPadEULAView from 'logpad/views/LogPadEULAView';
import ForgotPasswordActivationView from 'logpad/views/ForgotPasswordActivationView';
import SecretQuestionView from 'logpad/views/SecretQuestionView';
import ResetSecretQuestionActivationView from 'logpad/views/ResetSecretQuestionActivationView';
import ResetPasswordActivationView from 'logpad/views/ResetPasswordActivationView';
import UnlockCodeView from 'logpad/views/UnlockCodeView';
import HandoffView from 'logpad/views/HandoffView';
import SetTimeZoneActivationView from 'core/views/SetTimeZoneActivationView';
import { checkInstall, checkTimeZoneSet } from 'core/Helpers';
import Data from 'core/Data';
import Logger from 'core/Logger';

let logger = new Logger('RegistrationController');

/**
 * Controller for LogPad App registration workflow.
 * @class RegistrationController
 * @extends BaseController
 */
export default class RegistrationController extends BaseController {
    constructor (options = {}) {
        logger.traceSection('RegistrationController');
        logger.traceEnter('constructor');
        super(options);
        logger.traceExit('constructor');
    }

    /**
     * Displays the installation page.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#install
     * @returns {Q.Promise<void>}
     */
    install (options) {
        const force = options && options.force;
        logger.traceEnter('install');

        this.clear();
        Data.Subject = JSON.parse(localStorage.getItem('PHT_TEMP') || false) || false;

        return Q()
        .then(() => {
            if (!force && (!Data.code || !Data.Subject)) {
                this.navigate('code_entry', true);
                return Q();
            }

            // TODO: Investigate if authenticateThenGo could be used instead.
            // All functions in this file should be refactored accordingly
            // If the local database is installed, navigate to the login page; Otherwise, render the page.
            return checkInstall()
            .then((isInstalled) => {
                if (isInstalled && !force) {
                    this.navigate('login');
                    return Q();
                }

                return this.go('InstallView', InstallView, {
                    skipInstall: options && options.force
                });
            });
        })
        .catch(e => logger.error(e))
        .finally(() => {
            logger.traceExit('install');
        });
    }

    /**
     * Displays the set time zone activation view
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#set_time_zone_activation
     * @returns {Q.Promise<void>}
     */
    setTimeZone (options) {
        const force = options && options.force;
        logger.traceEnter('setTimeZone');

        return checkInstall()
        .then((isInstalled) => {
            if (isInstalled && !force) {
                this.navigate('#login');
                return Q();
            }
            return this.go('SetTimeZoneActivationView', SetTimeZoneActivationView)
            .catch(e => logger.error(e));
        })
        .finally(() => {
            logger.traceExit('setTimeZone');
        });
    }

    /**
     * The default application route. Displays the code entry view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#code_entry
     * @returns {Q.Promise<void>}
     */
    codeEntry (options) {
        const force = options && options.force;

        logger.traceEnter('codeEntry');

        // when backing from activation or reactivation page, clear url_setup_code
        if (LF.router && LF.router.view()) {
            localStorage.removeItem('url_setup_code');
        }

        return Q.all([checkInstall(), checkTimeZoneSet()])
        .spread((isInstalled, timeZoneSet) => {
            if (isInstalled && !force) {
                this.navigate('#login');
                return Q();
            } else if (timeZoneSet && !force) {
                this.navigate('handoff');
                return Q();
            }

            return this.go('CodeEntryView', CodeEntryView)
            .catch(e => logger.error(e));
        })
        .finally(() => {
            logger.traceExit('codeEntry');
        });
    }

    /**
     * The route for a new activation. Requires new password entry.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#activation
     * @returns {Q.Promise<void>}
     */
    activation (options) {
        const force = options && options.force;
        logger.traceEnter('activation');

        return Q()
        .then(() => {
            if (Data.code || force) {
                return this.go('ActivationView', ActivationView, options)
                .catch(e => logger.error(e));
            }

            return checkInstall()
            .then((isInstalled) => {
                if (isInstalled) {
                    this.navigate('login');
                } else {
                    this.navigate('code_entry');
                }
            });
        })
        .finally(() => {
            logger.traceExit('activation');
        });
    }

    /**
     * The route for a repeat activation. Requires entry of the old password.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#reactivation
     * @returns {Q.Promise<void>}
     */
    reactivation (options) {
        const force = options && options.force;

        logger.traceEnter('reactivation');

        return Q()
        .then(() => {
            if (Data.code || force) {
                return this.go('ReactivationView', ReactivationView, options)
                .catch(e => logger.error(e));
            }
            return checkInstall()
            .then((isInstalled) => {
                if (isInstalled) {
                    this.navigate('login');
                } else {
                    this.navigate('code_entry');
                }
            });
        })
        .finally(() => {
            logger.traceExit('reactivation');
        });
    }

    /**
     * Displays the unlock code view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#unlock_code_activation
     * @returns {Q.Promise<void>}
     */
    unlockCode (options) {
        const force = options && options.force;
        logger.traceEnter('unlockCode');

        return Q()
        .then(() => {
            if (Data.code || force) {
                return this.go('UnlockCodeView', UnlockCodeView)
                .catch(e => logger.error(e));
            }

            this.navigate('code_entry');
            return Q();
        })
        .finally(() => {
            logger.traceExit('unlockCode');
        });
    }

    /**
     * The route for a Privacy Policy.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#privacy_policy
     * @returns {Q.Promise<void>}
     **/
    privacyPolicy (options) {
        const force = options && options.force;
        logger.traceEnter('privacyPolicy');

        return Q()
        .then(() => {
            if (Data.code || force) {
                return this.go('PrivacyPolicyActivationView', PrivacyPolicyActivationView)
                .catch(e => logger.error(e));
            }

            this.navigate('code_entry');
            return Q();
        })
        .finally(() => {
            logger.traceExit('privacyPolicy');
        });
    }

    /**
     * The route for a Handoff View .
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#handoff
     * @returns {Q.Promise<void>}
     **/
    handoff (options) {
        const force = options && options.force;
        logger.traceEnter('handoff');

        this.clear();
        Data.code = Data.code || localStorage.getItem('activationCode');

        return Q()
        .then(() => {
            if (!Data.code && !force) {
                this.navigate('code_entry', true);
                return Q();
            }
            return this.go('HandoffView', HandoffView, options)
            .catch(e => logger.error(e));
        })
        .finally(() => {
            logger.traceExit('handoff');
        });
    }

    /**
     * Display the EULA view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#end_user_license_agreements
     * @returns {Q.Promise<void>}
     **/
    endUserLicenseAgreements (options = {}) {
        logger.traceEnter('endUserLicenseAgreements');

        return Q()
        .then(() => {
            if (Data.code || options.force) {
                return this.go('LogPadEULAView', LogPadEULAView, options)
                .catch(e => logger.error(e));
            }
            this.navigate('code_entry');
            return Q();
        })
        .finally(() => {
            logger.traceExit('endUserLicenseAgreements');
        });
    }

    /**
     * Display the secret question view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#secret_question
     * @returns {Q.Promise<void>}
     */
    secretQuestion (options) {
        const force = options && options.force;
        logger.traceEnter('secretQuestion');

        this.clear();

        return Q()
        .then(() => {
            if ((!Data.code || !Data.password) && !force) {
                this.navigate('code_entry');
                return Q();
            }
            return this.go('SecretQuestionView', SecretQuestionView)
            .catch(e => logger.error(e));
        })
        .then(() => {
            logger.traceExit('secretQuestion');
        });
    }

    /**
     * Displays the forgot password activation view
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#forgot-password
     * @returns {Q.Promise<void>}
     **/

    forgotPassword (options) {
        const force = options && options.force;
        logger.traceEnter('forgotPassword');

        return Q()
        .then(() => {
            if (!Data.code && !force) {
                this.navigate('code_entry');
                return Q();
            } else if (!Data.question && !force) {
                this.navigate('reactivation');
                return Q();
            }
            return this.go('ForgotPasswordActivationView', ForgotPasswordActivationView)
            .catch(e => logger.error(e));
        })
        .then(() => {
            logger.traceExit('forgotPassword');
        });
    }

    /**
     * Display the reset secret question activation view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /#reset_secret_question
     * @returns {Q.Promise<void>}
     */
    resetSecretQuestion (options) {
        const force = options && options.force;
        logger.traceEnter('resetSecretQuestion');

        return Q()
        .then(() => {
            if (Data.code || force) {
                return this.go('ResetSecretQuestionActivationView', ResetSecretQuestionActivationView)
                .catch(e => logger.error(e));
            }

            this.navigate('code_entry');
            return Q();
        })
        .then(() => {
            logger.traceExit('resetSecretQuestion');
        });
    }

    /**
     * Display the reset password activation view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /activation/#reset_password
     * @returns {Q.Promise<void>}
     */
    resetPassword (options) {
        const force = options && options.force;
        logger.traceEnter('resetPassword');

        return Q()
        .then(() => {
            if (Data.code || force) {
                return this.go('ResetPasswordActivationView', ResetPasswordActivationView)
                .catch(e => logger.error(e));
            }

            this.navigate('code_entry');
            return Q();
        })
        .then(() => {
            logger.traceExit('resetPassword');
        });
    }
}
