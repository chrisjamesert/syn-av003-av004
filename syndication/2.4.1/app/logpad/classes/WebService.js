import COOL from 'core/COOL';
import CoreWebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';

export default class WebService extends COOL.getClass('WebService', CoreWebService) {
    /**
     * Returns the auhorization token
     * @returns {Q.Promise<string>}
     */
    static getAuthorizationToken () {
        return CurrentSubject.getSubject()
        .then((subject) => {
            if (!subject) {
                return '';
            }

            return [
                subject.get('subject_id'),
                subject.get('service_password'),
                subject.get('device_id')
            ].join(':');
        });
    }

    /**
     * Returns the source string required by some of the APIs.
     * @returns {string}
     */
    getSource () {
        return 'LogPadApp';
    }

    /**
     * Gets site access code hash from server by sending the stored procedure name and the required parameters
     * @param {Object} params Includes the authentication token
     * @returns {Q.Promise<Object>}
     */
    getSiteAccessCode (params = {}) {
        let subjectData,
            ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            handleResult = ({ res }) => {
                let xml,
                    record,
                    resData;

                try {
                    if (res) {
                        xml = $($.parseXML(res));
                        record = xml.find('UAC')[0];
                    }
                } catch (err) {
                    return Q.reject(err);
                }

                resData = {
                    site_code: $(record).attr('SiteCode'),
                    study_name: $(xml.find('UACs')).attr('Stdy'),
                    hash: $(record).attr('Hash')
                };

                if (resData.site_code && resData.study_name && resData.hash) {
                    return Q(resData);
                }

                return Q.reject();
            },
            getSiteCode = () => {
                if (params.activation) {
                    subjectData = JSON.parse(localStorage.getItem('PHT_TEMP'));
                    return Q(subjectData ? subjectData.site_code : '');
                }

                return CurrentSubject.getSubject()
                .then(subject => subject.get('site_code'));
            };

        return getSiteCode()
        .then((siteCode) => {
            let data = {
                StoredProc: 'PDE_LPA_SiteUserAccessCodes',
                Params: [siteCode]
            };

            return this.transmit(ajaxConfig, data)
            .then(handleResult);
        });
    }
}

COOL.add('WebService', WebService);
