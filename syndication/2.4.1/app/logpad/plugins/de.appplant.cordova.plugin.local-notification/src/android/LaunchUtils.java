package de.appplant.cordova.plugin.localnotification;

import android.content.Context;
import android.content.Intent;

/***
* ERT MOD
*/
public final class LaunchUtils {

    /***
    * Launch main intent from package.
    */
    public static void launchApp(Context context) {
        String pkgName  = context.getPackageName();

        Intent intent = context
                .getPackageManager()
                .getLaunchIntentForPackage(pkgName);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_RECEIVER_FOREGROUND |
                Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        context.startActivity(intent);
    }

}

