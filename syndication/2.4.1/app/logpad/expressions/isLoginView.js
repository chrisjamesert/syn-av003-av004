import ELF from 'core/ELF';

/**
 * Determines if the id of the view is login-view.
 * @memberOf ELF.expressions/logpad
 * @method isLoginView
 * @param {null} input - Not used in this expression.
 * @param {Function} done - Invoked upon completion of the expression.
 */

// DE21155
export function isLoginView (input, done) {
    done(this.id === 'login-view');
}

ELF.expression('isLoginView', isLoginView);
