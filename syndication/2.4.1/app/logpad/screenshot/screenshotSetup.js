import ScreenshotSetup from 'core/screenshot/screenshotSetupHelper';

/**
 * Function to set up ELF triggers to start sreenshot mode in logpad
 */
export default function setup () {
    const setupHelper = new ScreenshotSetup({
        strings: {
            header: 'APPLICATION_HEADER'
        },

        // Used to filter diaries so that diaries with sitepad specific widgets don't break.
        product: 'logpad'
    });

    // Skip Code Entry and resume to Screenshot mode if screenshot has been activated before
    ELF.rules.add({
        id: 'Screenshot_Resume',
        trigger: 'NAVIGATE:registration/codeEntry',
        evaluate: () => localStorage.getItem('screenshot') === 'true',
        resolve: setupHelper.createActionsList()
    });

    // The rule to enter screenshot mode
    ELF.rules.add({
        id: 'Screenshot_Tool',
        trigger: 'CODEENTRY:Submit',

        evaluate: filter => filter.value === 'screenshot',
        resolve: setupHelper.createActionsList()
    });
}
