// Legacy code exemptions:
// jscs:disable requireTemplateStrings, requireArrowFunctions

import RouterBase from 'core/classes/RouterBase';
import PatientSession from 'core/classes/PatientSession';
import ScheduleManager from 'core/classes/ScheduleManager';
import { checkInstall } from 'core/Helpers';
import trial from 'logpad/trialMerge'; //PDE UPDATE changed to make trial import modality specific
import Logger from 'core/Logger.js';
import Spinner from 'core/Spinner';
import COOL from 'core/COOL';

const logger = new Logger('Router');
const studyRoutes = trial.assets.studyRoutes;

export default class Router extends RouterBase {
    constructor (options = {}) {
        logger.traceSection('Router');
        logger.traceEnter('Constructor');

        const routes = {
            // ------------------------------------------------------
            // Registration Routes
            // ------------------------------------------------------

            // Route to display {@link LF.View.ActivationView}
            activation: 'registration#activation',

            // Route to display {@link LF.View.CodeEntryView}
            code_entry: 'registration#codeEntry',

            // Route to display {@link LF.View.SetTimeZoneView}
            set_time_zone_activation: 'registration#setTimeZone',

            // Route to display {@link LF.View.ReactivationView}
            reactivation: 'registration#reactivation',

            // Route to display {@link LF.View.SecretQuestionView}
            secret_question: 'registration#secretQuestion',

            // Route to display {@link LF.View.InstallView}
            install: 'registration#install',

            // Route to display {@link LF.View.PrivacyPolicyActivationView}
            privacy_policy_activation: 'registration#privacyPolicy',

            // Route to display {@link LF.View.HandoffView}
            handoff: 'registration#handoff',

            // Route to display {@link LF.View.LogPadEULAView}
            end_user_license_agreements: 'registration#endUserLicenseAgreements',

            // Route to display {@link LF.View.ForgotPasswordActivationView}
            forgot_password_activation: 'registration#forgotPassword',

            // Route to display {@link LF.View.UnlockCodeView}
            unlock_code_activation: 'registration#unlockCode',

            // Route to display {@link LF.View.ResetPasswordActivationView}
            reset_password_activation: 'registration#resetPassword',

            // Route to display {@link LF.View.ResetSecretQuestionActivationView}
            reset_secret_question_activation: 'registration#resetSecretQuestion',

            // ------------------------------------------------------
            // Application Routes
            // ------------------------------------------------------

            // Route to display {@link LF.View.DashboardView}
            dashboard: 'application#dashboard',

            // Route to display {@link LF.View.QuestionnaireView}
            'questionnaire/:page': 'application#questionnaire',

            // Route to display {@link LF.View.QuestionnaireView}
            'questionnaire/:page/:scheduleId': 'application#questionnaire',

            // Route to display {@link LF.View.LoginView}
            login: 'application#login',

            // Route to display {@link LF.View.ForgotPasswordView}
            'forgot-password': 'application#forgotPassword',

            // Route to display {@link LF.View.UnlockCodeView}
            unlock_code: 'application#unlockCode',

            // Route to display {@link LF.View.ResetPasswordView}
            reset_password: 'application#resetPassword',

            // Route to display {@link LF.View.QuestionnaireCompletionView}
            'questionnaire-completion': 'application#questionnaireCompletion',
            'time-confirmation': 'application#timeConfirmation',
            blank: 'application#blank',

            // ------------------------------------------------------
            // Settings Routes
            // ------------------------------------------------------

            // Route to display {@link LF.View.ToolboxView}
            toolbox: 'settings#toolbox',

            // Route to display {@link LF.View.ChangePasswordView}
            change_password: 'settings#changePassword',

            // Route to display {@link LF.View.ChangePasswordView}
            change_temporary_password: 'settings#changeTemporaryPassword',

            // Route to display {@link LF.View.PrivacyPolicyApplicationView}
            privacy_policy: 'settings#privacyPolicy',

            // Route to display {@link LF.View.ChangeSecretQuestionView}
            change_secret_question: 'settings#changeSecretQuestion',

            // Route to display {@link LF.View.ResetSecretQuestionView}
            reset_secret_question: 'settings#resetSecretQuestion',

            // Route to display {@link LF.View.AboutView}
            about: 'settings#about',

            // Route to display {@link LF.View.SetAlarmsView}
            support: 'settings#support',

            // Route to display {@link LF.View.SetAlarmsView}
            set_alarms: 'settings#setAlarms',

            // Route to display {@link LF.View.setTimeZone}
            set_time_zone: 'settings#setTimeZone'
        };

        /**
         * Routes determine what view to render.
         * @readonly
         * @enum {String}
         */
        this.routes = _.extend({}, routes, options.routes, studyRoutes.logpad, {
            // This route needs to be last otherwise anything added after will match this first.
            '*path': 'registration#activation'
        });

        super(_.extend(options, {
            routes: this.routes
        }));

        LF.spinner = Spinner;

        if (!LF.security) {
            LF.security = new PatientSession();
        }
        LF.schedule = new ScheduleManager();

        logger.traceExit('Router constructor');
    }

    /**
     * Creates a new Application Router.
     * @constructs
     * @example LF.router = new LF.Router.ApplicationRouter();
     */
    initialize () {
        let checkOnline = () => {
            COOL.getClass('Utilities').isOnline()
            .then(() => {
                logger.trace('initialize navigating to code_entry view');
                this.navigate('code_entry', true);
            }, () => {
                LF.Actions.notify({ key: 'CONNECTION_REQUIRED_ERROR' }, (result) => {
                    logger.trace(`checkOnLine  User result: ${result}`);
                    if (result) {
                        checkOnline();
                    }
                });
            });
        };

        logger.trace('initialize...');

        // Check the install.  Test if online, if not installed, then navigate to activation page.
        checkInstall()
        .then(() => logger.trace('initialize done and happy'))
        .catch(checkOnline)
        .done();

        logger.trace('initialize returning');
    }
}
