
/*
 * This file is read by the build process which ultimately generates the runtime.bundle.js full
 * application.  The build process follows all the imports (and imports of imports etc).
 *
 * The goal of this particular level is to:
 * - Bring-in some core requisite class/widgets
 * - Then the main core "Startup" class (which brings in more classes) and has the main
 *   startup-time logic.
 * - Then the 'trial' tree (lots more imports) which may overwrite some core abilities
 */
import Logger from 'core/Logger';
let logger = new Logger('Index.js', { consoleOnly: true });

logger.traceSection('Index.js');
logger.traceEnter('Imports');

logger.trace('Import phase I - Core requisites');

// Import all Core and LogPad actions/expressions.
import 'core/classes/WebService';
import 'logpad/classes/WebService';
import 'core/actions';
import 'logpad/actions';
import 'core/expressions';
import 'logpad/expressions';
import 'logpad/screenshot';

import COOL from 'core/COOL';
import 'logpad/resources/Rules';
import 'logpad/resources/Templates';
import 'logpad/views/ContextSwitchingView';

// Widgets
logger.trace('Import phase II - Widgets');
import 'core/widgets/BarcodeScanner';
import 'core/widgets/CheckBox';
import 'core/widgets/CustomCheckBox';
import 'core/widgets/CustomRadioButton';
import 'core/widgets/VAS/HorizontalVAS';
import 'core/widgets/TextBoxWidgetBase';
import 'core/widgets/TextBox';
import 'core/widgets/AddUserTextBox';
import 'core/widgets/TempPasswordTextBox';
import 'core/widgets/ConfirmationTextBox';
import 'core/widgets/LanguageSelectWidget';
import 'core/widgets/RoleSelectWidget';
import 'core/widgets/PasswordMessageWidget';

import 'core/branching/branchingHelpers';

logger.trace('Import Phase II - BASIC STARTUP');
import Startup from 'logpad/classes/Startup';
COOL.add('Startup', Startup);

logger.trace('Import Phase III - Trial');
import 'trial/handheld';
import 'trainer/logpad';

logger.traceEnter('SyndicationClass');

COOL.new('Startup')
.startup()
.done(() => {
    logger.trace('SyndicationClass is satisfied');
    logger.traceExit('SyndicationClass');
});
