import PrivacyPolicyBaseView from 'logpad/views/PrivacyPolicyBaseView';

export default class PrivacyPolicyActivationView extends PrivacyPolicyBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - Privacy Policy view's event list.
         * @readonly
         */
        this.events = {
            /** Click event for going to the CodeEntryView */
            'click #back': 'back',

            /** Click event for going to the ActivationView */
            'click #next': 'activation'
        };
    }

    /**
     * @property {string} id - The id of the root element.
     * @readonly
     * @default 'privacy-policy-activation-view'
     */
    get id () {
        return 'privacy-policy-activation-view';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#privacy-policy-activation-tpl'
     */
    get template () {
        return '#privacy-policy-activation-tpl';
    }

    /**
     * Navigate to the activation view
     */
    activation () {
        this.navigate('activation');
    }

    /**
     * Navigate to the Code Entry view
     */
    back () {
        ELF.trigger('PRIVACYPOLICYACTIVATION:Backout', {}, this);
    }
}
