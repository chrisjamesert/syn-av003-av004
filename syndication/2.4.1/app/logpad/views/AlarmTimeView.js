import { zeroPad } from 'core/utilities';
import Logger from 'core/Logger';

const logger = new Logger('AlarmTimeView');

export default class AlarmTimeView extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * @property {string} tagName - The root element of the view.
         * @readonly
         * @default 'a'
         */
        this.tagName = 'a';

        /**
         * @property {string} className - The class of the view's root element.
         */
        this.className = 'table-view-cell';

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            // tap event for opening the datebox
            'click a.list-group-item': 'open',

            // respond to alarm time change
            'change input.alarm-input': 'respond'
        };

        /**
         * @property {Object<string,(Object|string)>} templateStrings - Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = {
            diaryName: {
                key: '',
                namespace: ''
            },
            clearBtnTxt: 'ALARM_TURN_OFF',
            setTimeLblTxt: 'SET_TIME',
            setTimeBtnTxt: 'OK',
            alarmOffLabel: 'ALARM_OFF_LBL'
        };

        // Initialize...
        let alarmConfig = this.model.get('alarmParams');
        let returnedDates = _.extend(LF.strings.dates({ dates: {} }), LF.strings.dates({ dateConfigs: {} }));
        let dateLang = { default: returnedDates };
        let time = $('<input data-role="datebox" />');

        dateLang.default.isRTL = (LF.strings.getLanguageDirection() === 'rtl');

        /**
         * @property {Object} subjectConfig - The alarm's configuration for subjects.
         */
        this.subjectConfig = alarmConfig.subjectConfig;

        /**
         * @property {Object<string,(boolean|string|number)>} config - Configuration for the view's datebox widget.
         */
        this.config = {
            mode: 'timebox',
            useSetButton: true,
            alarmMode: true,
            useHeader: true,
            useFocus: true,
            useModal: true,
            useButton: false,
            minuteStep: this.subjectConfig.alarmRangeInterval
        };

        if (this.subjectConfig.alarmOffSubject && this.subjectConfig.alarmOffSubject === true) {
            _.defaults(this.config, {
                useClearButton: true,
                overrideClearButton: ''
            });
        }

        /**
         * @property {string} alarmTime - The time of the alarm.
         */
        this.alarmTime = '';

        if (alarmConfig.time !== '') {
            this.alarmTime = alarmConfig.time;
            time.datebox(dateLang);
            _.defaults(this.config, {
                defaultValue: `${alarmConfig.time}:00`
            });
        } else {
            if (alarmConfig.originalTime) {
                _.defaults(this.config, {
                    defaultValue: `${alarmConfig.originalTime}:00`
                });
            }
        }

        this.render();
    }

    /**
     * The views click event.
     */
    open () {
        this.inputTime.datebox('open');
    }

    /**
     * Renders the view.
     * @returns {AlarmTimeView}
     */
    render () {
        let alarmPickerElement,
            alarmLbl,
            questionnaireModel = LF.StudyDesign.questionnaires.where({ id: this.model.get('target').id })[0];

        this.templateStrings.diaryName.key = questionnaireModel.get('displayName');
        this.templateStrings.diaryName.namespace = questionnaireModel.get('id');

        LF.getStrings(this.templateStrings)
        .then((strings) => {
            this.config.minTime = this.subjectConfig.minAlarmTime;
            this.config.maxTime = this.subjectConfig.maxAlarmTime;

            this.config.overrideTitleTimeDialogLabel = strings.setTimeLblTxt;
            this.config.overrideSetTimeButtonLabel = strings.setTimeBtnTxt;

            if (this.config.useClearButton === true) {
                this.config.overrideClearButton = strings.clearBtnTxt;
            }

            this.alarmOffLbl = strings.alarmOffLabel;
            alarmLbl = this.alarmTime === '' ? strings.alarmOffLabel : this.alarmTime;

            alarmPickerElement = LF.Resources.Templates.display('DEFAULT:AlarmPicker', {
                id: `alarm${this.model.get('id')}`,
                alarmTime: alarmLbl,
                diaryName: strings.diaryName,
                configuration: _.escape(JSON.stringify(this.config))
            });

            this.$el.attr('data-icon', 'false');
            this.$el.html(alarmPickerElement);
            this.$('input').val(this.alarmTime);
            this.delegateEvents();
            this.$el.trigger('create');

            this.inputTime = this.$('input.alarm-input');
            this.inputTime.datebox(this.config);

            this.$alarmDisplayElem = this.$('span.direction-right > time');

            this.$iconContainer = this.$('span.direction-right > i');
            this.bellIconClass = 'fa fa-bell-o';

            if (this.alarmTime !== '') {
                this.$iconContainer.addClass(this.bellIconClass);
            }
        })
        .catch(err => logger.error('render Exception', err))
        .done();

        return this;
    }

    /**
     * Responds to the alarm time change.
     */
    respond () {
        let alarmInputVal = this.$('input').val(),
            bellIcon = this.bellIconClass,
            $alarmDisplayElem = this.$alarmDisplayElem,
            triggerAlarmChanged = (newAlarmTime) => {
                this.$el.trigger('alarmChanged', {
                    id: this.model.get('alarmParams').id,
                    schedule: this.model,
                    time: newAlarmTime
                });
            };

        if (alarmInputVal !== this.alarmTime) {
            this.alarmTime = alarmInputVal;

            this.displayAlarmValue(alarmInputVal, $alarmDisplayElem, bellIcon)
            .then(triggerAlarmChanged)
            .catch(err => logger.warn('An error occured', err))
            .done();
        }
    }

    /**
     * Displays the alarm value
     * @param  {string} alarmTime - The alarm time to be displayed
     * @param  {Object} elem - jQuery element containing alarm value
     * @param  {string} alarmIconClass - CSS class for glyphicon
     * @param  {string} alarmOffLbl - Text to display when alarm is turned off
     * @return {Q.Promise<string>} The new alarm time
     */
    displayAlarmValue (alarmTime, elem, alarmIconClass, alarmOffLbl = this.alarmOffLbl) {
        return Q.promise((resolve, reject) => {
            let newAlarmTime;

            if (alarmTime === null || alarmTime === undefined) {
                reject(new Error('Alarm time has an invalid type'));
            }

            if (alarmTime !== '') {
                let [hour, min] = this.alarmTime.split(':');

                elem.text(alarmTime);
                this.$iconContainer.addClass(alarmIconClass);

                newAlarmTime = `${zeroPad(hour)}:${zeroPad(min)}`;
            } else {
                elem.text(alarmOffLbl);
                this.$iconContainer.removeClass(alarmIconClass);

                newAlarmTime = '';
            }

            resolve(newAlarmTime);
        });
    }
}

// TODO (TECH): Refactor this file
window.LF.View.AlarmTimeView = AlarmTimeView;
