import ELF from 'core/ELF';
import { openQuestionnaire } from 'core/actions/openQuestionnaire';

export default class UpcomingQuestionnaire extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - When the view is clicked, open the questionnaire.
         * @readonly
         */
        this.events = {

            // tap event for opening the questionnaire
            click: _.debounce((e) => {
                this.open(e);
            }, 300, true)
        };

        this.options = options;

        this.listenTo(this.model, 'remove', this.remove);
    }

    /**
     * @property {string} tagName - The root element of the view.
     * @readonly
     * @default 'li'
     */
    get tagName () {
        return 'a';
    }

    /**
     * @property {Object<string,string>} attributes - Attributes to populate on the root element upon render.
     */
    get attributes () {
        return { class: 'list-group-item' };
    }

    /**
     * The views click event.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    open (e) {
        let id = this.model.get('id'),
            scheduleId = this.options.scheduleId;

        if ($(`.list-group-item#${id}`).attr('disabled')) {
            return;
        }
        e.preventDefault();

        ELF.trigger(`DASHBOARD:OpenQuestionnaire/${id}`, {
            questionnaire: id,
            schedule_id: scheduleId
        }, this.options.parent)
        .then((res) => {
            if (!res.preventDefault) {
                openQuestionnaire({
                    questionnaire_id: id,
                    schedule_id: scheduleId
                }, $.noop);
            }
        })
        .done();
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        return LF.getStrings(this.model.get('displayName'), $.noop, {
            namespace: this.model.get('id')
        })
        .then((title) => {
            let questionnaire = LF.templates.display('DEFAULT:Questionnaire', { title });
            this.$el.html(questionnaire);
            this.delegateEvents();
        });
    }
}

window.LF.View.UpcomingQuestionnaire = UpcomingQuestionnaire;
