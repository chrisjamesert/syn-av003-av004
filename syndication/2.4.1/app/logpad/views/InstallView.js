import PageView from 'core/views/PageView';
import Subject from 'core/models/Subject';
import ELF from 'core/ELF';
import Users from 'core/collections/Users';
import Data from 'core/Data';
import DatabaseVersion from 'core/models/DatabaseVersion';
import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import Spinner from 'core/Spinner';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';

const logger = new Logger('InstallView');

export default class InstallView extends PageView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {boolean} skipInstall - Set to skip doing the install and only render the vies.
         * @type boolean
         */
        this.skipInstall = options.skipInstall || false;

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function.
         * @type Object
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'INSTALLING'
        };
    }

    // noinspection JSMethodCanBeStatic
    /**
     * @property {string} id - The default id of the root element
     * @readonly
     * @default 'install-page'
     */
    get id () {
        return 'install-page';
    }

    /**
     * @property {string} template - Id of template to render.
     * @readonly
     * @default '#install-template'
     */
    get template () {
        return '#install-template';
    }

    /**
     * Calls super.resolve, starts the install(asynchronously) and returns
     * @returns {Q.Promise<void>}
     */
    resolve () {
        const ret = super.resolve();

        // kick off the install and let it run asynchronously, keeping a handle to it.
        this.installPromise = this.doInstall();
        return ret;
    }

    /**
     * Renders the view and kicks off the install
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML({}, true)
        .then(() => {
            // Prevent the user from moving the UI and disrupting installation.
            this.$el.bind('touchmove', (e) => {
                e.preventDefault();
            });

            return Spinner.show();
        })
        .finally(() => {
            // set this to true to just render the screen without installing.
            if (this.skipInstall) {
                return Spinner.hide();
            }

            // allow install to complete now that you won't be racing
            // for UI  purposefully don't return,  Allow to finish async (David Peterson).
            this.installPromise
            .then(() => Spinner.hide())
            .then(() => {
                this.navigate('login', true);
            })
            .catch((err) => {
                logger.error('Error installing data', err);

                return Spinner.hide()
                .then(() => MessageRepo.display(MessageRepo.Dialog.INSTALL_FAILED))
                .then(() => ELF.trigger('APPLICATION:Uninstall', {}, this))
                .catch((err) => {
                    logger.error('An error occurred.', err);
                });
            })
            .done();

            return Q();
        });
    }

    /**
     * Performs the install.
     * @returns {Q.Promise<void>}
     */
    doInstall () {
        return this.installSubjectData()
        .then(() => {
            return this.installDatabaseVersion();
        })
        .then(() => {
            return ELF.trigger('INSTALL:ModelsInstalled', {}, this);
        })
        .then(() => {
            // signals loginview to set alarms
            lStorage.setItem('rescheduleAlarms', true);
        });
    }

    /**
     * Install database versions to the database
     * @returns {Q.Promise<void>}
     */
    installDatabaseVersion () {
        const versions = [{
            versionName: 'DB_Core',
            version: LF.coreDbVersion
        }, {
            versionName: 'DB_Study',
            version: LF.StudyDesign.studyDbVersion
        }];

        // Upgrade versions sequentially.
        return versions.reduce((chain, version) => {
            const model = new DatabaseVersion(version);

            return chain
            .then(() => {
                return model.save();
            })
            .catch((err) => {
                err && logger.error('Error saving model for databaseVersion', err);
            });
        }, Q());
    }

    /**
     * Install historical data to the database
     * @param {Object} params - includes temporary Storage name and the model name that will be installed.
     * @returns {Q.Promise<void>}
     */
    installHistoricalData (params) {
        const data = JSON.parse(localStorage.getItem(params.tempStorage));
        const install = (chain, modelData) => {
            // TODO: Can we do this without using the LF.Model namespace?
            const model = new LF.Model[params.model](modelData);
            return chain
            .then(() => {
                return model.save();
            });
        };

        return Q()
        // eslint-disable-next-line consistent-return
        .then(() => {
            if (!_.isEmpty(data)) {
                // If data is an array, save each sequentially, otherwise save the single object.
                return _.isArray(data) ? data.reduce(install, Q()) : install(Q(), data);
            }
        })
        .catch((err) => {
            logger.error('Model could not save in installHistoricalData', err);
        })
        .then(() => {
            localStorage.removeItem(params.tempStorage);
        });
    }

    /**
     * Installs subject data to the local database then navigates to the Subject Login.
     * @example this.installSubjectData();
     * @returns {Q.Promise<void>}
     */
    installSubjectData () {
        // performed in registration controller
        Data.Subject = JSON.parse(localStorage.getItem('PHT_TEMP') || false) || false;

        let model = new Subject(Data.Subject);

        return model.save()
        .then(() => Users.updateUsers())
        .then(() => COOL.getClass('Utilities').isOnline())
        .then(() => {
            localStorage.removeItem('PHT_TEMP');
            localStorage.setItem('Subject_Login', true);
            Data = {};
        });
    }
}

COOL.add('InstallView', InstallView);
