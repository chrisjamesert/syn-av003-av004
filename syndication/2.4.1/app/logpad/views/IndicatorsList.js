import ListBaseView from 'logpad/views/ListBaseView';
import IndicatorView from 'logpad/views/IndicatorView';

export default class IndicatorsList extends ListBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {string} objectType - Type of this list. (used in schedule targets)
         * @readonly
         * @default 'indicator'
         */
        this.objectType = 'indicator';

        /**
         * @property {Object} options - Options provided to the constructor.
         */
        this.options = options;
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'indicatorsList'
     */
    get id () {
        return 'indicatorsList';
    }

    /**
     * Filters schedules to only the ones that matter to this view.
     * @param {Object} schedules - Collection of schedules to filter
     * @return {Array} returns the list of the schedules of which target objectType is "indicator"
     */
    getSchedules (schedules) {
        return LF.schedule.getIndicatorSchedules(schedules);
    }

    /**
     * Creates a new subview (LF.View.IndicatorView)
     * @param {Indicator} indicator - indicator model used to create new subview
     * @param {tring} scheduleId - scheduleId of the schedule attached to the indicator
     */
    createSubview (indicator, scheduleId) {
        let studyRule = `DASHBOARD:AddedIndicator/${indicator.get('id')}`;

        const indicatorView = new IndicatorView({
            id: indicator.get('id'),
            model: indicator,
            parent: LF.router.view(),
            scheduleId
        });

        return indicatorView.render()
        .then(() => ({
            view: indicatorView,
            ruleString: studyRule
        }));
    }
}

window.LF.View.IndicatorsList = IndicatorsList;
