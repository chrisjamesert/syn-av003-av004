const { View } = Backbone;

import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('IndicatorView');

export default class IndicatorView extends View {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events 0 When the view is clicked, trigger the rule.
         * @readonly
         */
        this.events = { click: 'trigger' };

        /**
         * @property {Object} options - Options provided in the constructor.
         */
        this.options = options;

        /**
         * @property {Backbone.View} - The parent view of the indicator.
         */
        this.parent = this.options.parent;

        this.listenTo(this.model, 'remove', this.remove);
    }

    /**
     * @property {string} tagName - The root element of the view.
     * @readonly
     * @default 'div'
     */
    get tagName () {
        return 'div';
    }

    /**
     * The views click event.
     * @param {Event} e Event Object
     */
    trigger (e) {
        let id = this.model.get('id');

        if (e) {
            e.preventDefault();
        }

        ELF.trigger(`DASHBOARD:TriggerIndicator/${id}`, { indicator: id }, this.parent)
        .done();
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let stringsToFetch = {},
            label = this.model.get('label'),
            image = this.model.get('image'),
            content;

        if (label) {
            stringsToFetch.label = label;
        }

        if (image) {
            stringsToFetch.image = {
                key: image,
                namespace: 'ICONS'
            };
        }

        if (image || label) {
            return LF.getStrings(stringsToFetch)
            .then((strings) => {
                this.$el.html('').attr('id', this.model.get('id'));
                this.$el.html('').addClass('indicator');
                this.$el.html('').addClass(this.model.get('className'));

                if (image && label) {
                    content = LF.templates.display('DEFAULT:IndicatorFull', {
                        image: strings.image,
                        label: strings.label
                    });
                } else {
                    if (label) {
                        content = LF.templates.display('DEFAULT:IndicatorLabel', { label: strings.label });
                    }

                    if (image) {
                        content = LF.templates.display('DEFAULT:IndicatorImage', { image: strings.image });
                        this.$el.html('').addClass('top');
                    }
                }

                this.$el.append(content);
                this.delegateEvents();
            })
            .catch((err) => {
                logger.error('Error rendering indicator', err);
            });
        }

        logger.warn(`Indicator ${this.model.get('id')} missing label and image.`);
        return Q();
    }
}

window.LF.View.IndicatorView = IndicatorView;
