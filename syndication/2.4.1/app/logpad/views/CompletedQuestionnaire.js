// TODO - Defunct - Dead code.
export default class CompletedQuestionnaire extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * The root element of the view.
         * @readonly
         * @type String
         * @default 'li'
         */
        this.tagName = 'li';

        // Initialize...
        this.options = options;
        this.dashboard = this.options.dashboard;
        this.render();
    }

    /**
     * Renders the view.
     * @returns {Object} this ({@link LF.View.CompletedQuestionnaire})
     */
    render () {
        let title = LF.string.display(this.model.get('displayName'), { namespace: this.model.get('id') }),
            questionnaire = LF.templates.display('DEFAULT:CompletedQuestionnaire', { title });

        this.$el.html(questionnaire);

        return this;
    }
}

window.LF.View.CompletedQuestionnaire = CompletedQuestionnaire;
