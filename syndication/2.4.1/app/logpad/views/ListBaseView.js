import { getScheduleModels } from 'core/Helpers';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('ListBaseView');

export default class ListBaseView extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * @property {Array} subviews - Array of subviews appended to this view.
         */
        this.subviews = [];
    }

    /**
     * Renders the view and create subviews from models in this.collection.
     * @returns {Q.Promise<void>}
     */
    render () {
        let schedules = this.options.schedules;

        this.clear();
        return schedules.models.reduce((chain, schedule) => {
            return chain.then(() => this.add(schedule));
        }, Q())
        .then(() => {
            this.options.container.append(this.$el);
            this.$el.show();
        });
    }

    /**
     * Add a new subview after original render with the correct order from study.demo.
     * @param {Schedule} addedSchedule - schedule of item to be added.
     * @returns {Q.Promise} Returns promise that is resolved with the added subview.
     */
    addInOrder (addedSchedule) {
        let availableSchedules = LF.schedule.availableSchedules,
            numRendered = 0;

        // This function gets called for every instance of this view (QuestionnairesList and IndicatorsList in some cases).
        //  So, check if the schedule pertains to this list view before adding and rendering new item.
        if (addedSchedule.get('target').objectType !== this.objectType) {
            return Q();
        }

        const allSchedules = this.getSchedules(getScheduleModels());
        const isTarget = schedule => schedule.get('id') === addedSchedule.get('id');

        const schedule = _.find(allSchedules, (schedule) => {
            if (isTarget(schedule)) {
                return true;
            }

            const availableSchedule = availableSchedules.findWhere({
                id: schedule.get('id')
            });

            if (availableSchedule) {
                const rendered = _.findWhere(this.subviews, {
                    id: schedule.get('target').id
                });

                if (rendered) {
                    numRendered++;
                }
            }

            // Continue loop
            return false;
        });

        const item = this.collection.findWhere({ id: addedSchedule.get('target').id });

        return this.createSubview(item, schedule.get('id'))
        .then(({ view, ruleString }) => {
            let children = this.$el.children();

            if (numRendered > 0) {
                children.eq(numRendered - 1).after(view.$el);
            } else if (children.size() !== 0) {
                children.eq(children.size() - 1).before(view.$el);
            } else {
                this.$el.prepend(view.$el);
            }

            this.subviews.splice(numRendered, 0, view);

            logger.operational(`Displaying ${this.objectType} ${item.id} to user.`);

            return Q((this.refresh || $.noop)())
            .then(() => ELF.trigger(ruleString, {}, LF.router.view()))
            .then(() => view);
        });
    }

    /**
     * Add a new subview if the schedule's target type matches
     * @param {Schedule} schedule - schedule of item to be added
     * @returns {Q.Promise} returns promise that resolves with the added subview
     */
    add (schedule) {
        let subviews = this.subviews,
            target = schedule.get('target'),
            item = this.collection.where({
                id: target.id
            })[0];

        if (item && target.objectType === this.objectType) {
            return this.createSubview(item, schedule.get('id'))
            .then(({ view, ruleString }) => {
                this.$el.append(view.$el);
                subviews.push(view);

                return ELF.trigger(ruleString, {}, LF.router.view())
                .then(() => LF.security.checkLogin())
                .catch((err) => {
                    err && logger.error('Error checking login', err);
                    return false;
                })
                .then((isLoggedIn) => {
                    if (isLoggedIn || target.objectType === 'indicator') {
                        logger.operational(`Displaying ${this.objectType} ${item.id} to user.`, {
                            objectType: this.objectType,
                            id: item.id
                        });
                    }
                })
                .then(() => view);
            });
        }

        return Q();
    }

    /**
     * Delete a subview from the list by schedule
     * @param {Schedule} schedule - schedule of item to be removed
     */
    removeSubview (schedule) {
        if (schedule.get('target').objectType === this.objectType) {
            let subview = _.filter(this.subviews, (view) => {
                return view.model.get('id') === schedule.get('target').id;
            })[0];

            if (subview) {
                subview.remove();
                this.subviews.splice(this.subviews.indexOf(subview), 1);
            }
        }
    }

    /**
     * Filters schedules to only the ones that matter to this view.
     *   Currently a passthrough made to be overwritten if filtering is needed
     * @param {Object} schedules - Collection of schedules to filter
     * @return {Array} returns filtered list of schedules
     */
    getSchedules (schedules) {
        return schedules;
    }

    /**
     * Close and remove each subview from the DOM to ensure listener cleanup
     */
    close () {
        this.clear();
        this.remove();
    }

    /**
     * Clear subviews without removing this view
     */
    clear () {
        _.each(this.subviews, (view) => {
            view.remove();
        });
        this.subviews = [];
        this.unbind();
    }
}

window.LF.View.ListBaseView = ListBaseView;
