import ActivationBaseView from 'core/views/ActivationBaseView';

// - Core Modules
import { Banner } from 'core/Notify';
import Data from 'core/Data';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';
import * as lStorage from 'core/lStorage';
import { getNested } from 'core/utilities';

const logger = new Logger('ReactivationView');

export default class ReactivationView extends ActivationBaseView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {Object<string,string>} selectors - A list of selectors to generate.
         */
        this.selectors = {
            code: '#txtReCode',
            password: '#txtPassword',
            submit: '#submit'
        };

        /**
         * @property {Object<string,string>} events - Reactivation view's event list.
         * @readonly
         */
        this.events = {
            // Submit form event for reactivation
            'click #submit': (e) => {
                this.submit(e)
                .done();
            },

            // Tap event for entering new code
            'click #back': 'back',

            // Input event for password
            'input #txtPassword': 'onInput',

            // Click event for forgot password link
            'click #forgot-password': 'forgotPassword'
        };

        /**
         * @property {Object<string,(Object|string)>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'DEVICE_REACTIVATION',
            password: 'PASSWORD',
            back: 'BACK',
            submit: 'OK',
            activationCode: 'ACTIVATION_CODE',
            forgotPassword: 'FORGOT_PASSWORD',
            submitIcon: {
                key: 'LOGIN',
                namespace: 'ICONS'
            }
        };

        /**
         * @property {Object<string, string>} stringValues - Override strings with passed in values
         */
        this.stringValues = options.stringValues || {};
    }

    /**
     * @property {string} id - The id of the root element.
     * @readonly
     * @default 'reactivation-page'
     */
    get id () {
        return 'reactivation-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#reactivation-template'
     */
    get template () {
        return '#reactivation-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<number>}
     */
    resolve () {
        // @TODO: Figure out a better way to handle bad password attempts. :(
        let users = new Users();
        return users.fetch()
        .then(() => {
            let user = users.findWhere({ id: 0 });
            this.user = user || new User({
                id: 0,
                active: 0
            });
            return this.user.save();
        });
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        const { setupCode } = this.stringValues;

        return this.buildHTML({
            code: setupCode || Data.code,
            key: this.key,
            inputType: this.getRoleInputType(getNested(this, 'user.attributes.role') || lStorage.getItem('activationRole'))
        }, true);
    }

    /**
     * Submits the form
     * @param {Event} e Event data.
     * @returns {Q.Promise}
     */
    submit (e) {
        Banner.closeAll();

        e.preventDefault();

        this.$password.blur();

        if (this.isValidForm('#txtReCode')) {
            return this.reactivate();
        }

        this.sound.play('error-audio');
        return Q();
    }

    /**
     * Navigates to the ForgotPasswordActivationView or UnlockCodeView
     * @returns {Q.Promise}
     */
    forgotPassword () {
        let unlockCode = () => {
                return Spinner.hide()
                .then(() => this.navigate('unlock_code_activation'));
            },
            forgotPassword = () => {
                return Spinner.hide()
                .then(() => this.navigate('forgot_password_activation'));
            };

        if (!LF.StudyDesign.askSecurityQuestion) {
            return unlockCode();
        }

        return Spinner.show()
        .then(() => this.getSecretQuestion())
        .then(forgotPassword)
        .catch(unlockCode);
    }

    /**
     * Fetch the Security Question selected by the user or navigate to the unlock code screen
     * @returns {Q.Promise}
     */
    getSecretQuestion () {
        return Q.Promise((resolve, reject) => {
            let collection = LF.StudyDesign.securityQuestions,
                handleResult = ({ res }) => {
                    if (res[0] && res[0].Q !== undefined) {
                        _(collection).each((value) => {
                            if (value.key === res[0].Q) {
                                Data.question = value;
                                resolve();
                            }
                        });
                    } else {
                        reject();
                    }
                };

            LF.webService.getSubjectQuestion(Data.code)
            .then(handleResult)
            .catch(reject)
            .done();
        });
    }

    /**
     * Attempt to reactivate the subject by verifying the password with the server.
     * Handles transmission result and error.
     * @returns {Q.Promise}
     */
    reactivate () {
        let password = this.$password.val(),
            handleResult = ({ res }) => {
                let token;

                LF.security.resetFailureCount(this.user);

                Data.question = res.Q;
                Data.answer = res.A;
                Data.deviceID = res.D;
                Data.service_password = res.W;
                Data.password = password;

                token = `${Data.code}:${Data.service_password}:${Data.deviceID}`;

                return ELF.trigger('REACTIVATION:Transmit', {}, this)
                .finally(() => this.getSubjectData(token));
            },
            handleError = ({ errorCode, httpCode }) => {
                this.removeMessage();

                if (httpCode === LF.ServiceErr.HTTP_UNAUTHORIZED) {
                    LF.security.setLoginFailure(LF.security.getLoginFailure(this.user) + 1, this.user);

                    this.showInputError(this.$password, 'PASSWORD_INVALID');
                    this.$password.val('');
                    this.disableButton(this.$submit);

                    if (LF.StudyDesign.maxLoginAttempts === LF.security.getLoginFailure(this.user)) {
                        if (LF.security.accountDisable(this.user)) {
                            logger.error('User has been locked out.');
                            return this.forgotPassword();
                        }
                    }
                } else {
                    return this.showTransmissionError({ errorCode, httpCode });
                }

                this.sound.play('error-audio');
                return Q();
            };

        if (LF.security.checkLock(this.user)) {
            return this.forgotPassword();
        }

        return this.attemptTransmission()
        .then((online) => {
            if (!online) {
                return Q();
            }

            let subjectData = {
                U: this.$code.val(),
                W: hex_sha512(password + localStorage.getItem('krpt'))
            };

            if (localStorage.getItem('PHT_IMEI')) {
                subjectData.S = localStorage.getItem('PHT_IMEI');
                subjectData.M = device.model;
            }
            return LF.webService.getDeviceID(subjectData)
            .then(handleResult)
            .catch(handleError);
        });
    }

    /**
     * Enables the OK button on password input.
     */
    onInput () {
        let password = this.$password.val();

        this[password.length === 0 ? 'disableButton' : 'enableButton'](this.$submit);
    }

    /**
     * Navigate to the Code Entry view
     */
    back () {
        ELF.trigger('REACTIVATION:Backout', {}, this);
    }
}
