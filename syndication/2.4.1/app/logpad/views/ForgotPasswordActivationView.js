import ActivationBaseView from 'core/views/ActivationBaseView';
import { Banner } from 'core/Notify';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Users from 'core/collections/Users';
import Data from 'core/Data';
import COOL from 'core/COOL';
import WebService from 'logpad/classes/WebService';

let logger = new Logger('ForgotPasswordActivationView');

export default class ForgotPasswordActivationView extends ActivationBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {string} id - Id of template to render
         * @readonly
         * @default '#forgot-password-activation-tpl'
         */
        this.template = '#forgot-password-activation-tpl';

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate
         * @type {Object}
         */
        this.selectors = {
            back: '#back',
            newPassword: '#txtNewPassword',
            confirmPassword: '#txtConfirmPassword',
            secretAnswer: '#secret_answer',
            submit: '#submit',
            passwordRules: '#display-password-rules'
        };

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {

            // Click event for back button
            'click #back': 'back',

            // Tap event for submit form
            'click #submit': (e) => {
                this.submit(e)
                .done();
            },

            // Click event for Unlock Code link
            'click #unlock-code': 'unlockCode',

            // Input event for the new password
            'input input#txtNewPassword': 'validateNewPassword',

            // Input event for the confirmation password
            'input input#txtConfirmPassword': 'validatePasswords'
        };

        /**
         * @property {Object<string,(Object|string)>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'FORGOT_PASSWORD',
            disabledLoginText: 'PASSWORD_ATTEMPT_EXCEEDED',
            pleaseAnswerText: 'PLEASE_ANSWER_SQ',
            unlockCode: 'FORGOT_SECRET_ANSWER',
            newPassword: 'ENTER_NEW_PASSWORD',
            confirmPassword: 'CONFIRM_PASSWORD',
            secretQuestion: '',
            submit: 'OK',
            back: 'BACK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            },
            passwordRules: 'PASSWORD_RULES'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'forgot-password-activation-page'
     */
    get id () {
        return 'forgot-password-activation-page';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Users.fetchCollection()
        .then((users) => {
            this.user = users.findWhere({ id: 0 });
        })
        .then(() => super.resolve());
    }

    /**
     * Navigates back to the reactivation view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('reactivation');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        this.templateStrings.secretQuestion = Data.question ? Data.question.text : '';

        return this.buildHTML({
            key: this.key,
            inputType: this.getRoleInputType(this.user && this.user.get('role'))
        }, true)
        .then(() => {
            LF.StudyDesign.showPasswordRules ?
                this.showElement(this.$passwordRules) :
                this.hideElement(this.$passwordRules);

            // DE8275: Adding focus to prevent blue pointer from displaying erroneously in Android
            this.enableElement(this.$submit)
                .focus()
                .attr('disabled', 'disabled');
        });
    }

    /**
     * Submits the form
     * @param {Event} e Event data.
     * @returns {Q.Promise}
     */
    submit (e) {
        // Close banner notification after submitting the form
        Banner.closeAll();
        this.$confirmPassword.blur();
        e.preventDefault();

        if (!!LF.StudyDesign.maxSecurityQuestionAttempts && LF.security.getSecurityQuestionFailure(this.user) >= LF.StudyDesign.maxSecurityQuestionAttempts) {
            LF.security.setSecurityQuestionFailure(LF.security.getSecurityQuestionFailure(this.user) + 1, this.user);

            // redirect to the Unlock Code Screen and break from validation
            this.unlockCode();
            return Q();
        }

        return this.reactivate();
    }

    /**
     * Attempt to reactivate
     * @returns {Q.Promise}
     */
    reactivate () {
        let password = this.$newPassword.val(),
            answer = this.$secretAnswer.val(),
            handleResult = ({ res }) => {
                Data.question = res.Q;
                Data.answer = res.A;
                Data.deviceID = res.D;
                Data.service_password = res.W;
                Data.password = password;

                // Create token
                let token = `${Data.code}:${Data.service_password}:${Data.deviceID}`;

                logger.operational('Correct secret answer entered on activation');

                return ELF.trigger('REACTIVATION:Transmit', {}, this)
                .finally(() => this.getSubjectData(token));
            },
            handleError = ({ errorCode, httpCode }) => {
                this.removeMessage();

                this.sound.play('error-audio');

                if (httpCode !== LF.ServiceErr.HTTP_UNAUTHORIZED) {
                    return this.showTransmissionError({ errorCode, httpCode });
                }

                this.showInputError('#secret_answer', 'INCORRECT_ANSWER');

                LF.security.setSecurityQuestionFailure(LF.security.getSecurityQuestionFailure(this.user) + 1, this.user);

                if (LF.StudyDesign.maxSecurityQuestionAttempts === LF.security.getSecurityQuestionFailure(this.user)) {
                    this.unlockCode();
                }

                return Q();
            };

        return this.attemptTransmission()
        .then((online) => {
            if (!online) {
                return Q();
            }

            let subjectData = {
                U: Data.code,
                W: hex_sha512(password + localStorage.getItem('krpt')),
                Q: Data.question.key,
                A: hex_sha512(answer)
            };

            if (localStorage.getItem('PHT_IMEI')) {
                subjectData.S = localStorage.getItem('PHT_IMEI');
                subjectData.M = device.model;
            }

            let service = COOL.new('WebService', WebService);
            return service.getDeviceID(subjectData)
            .then(handleResult)
            .catch(handleError);
        });
    }

    /**
     * Navigates to the UnlockCodeView
     */
    unlockCode () {
        this.navigate('unlock_code_activation');
    }
}
