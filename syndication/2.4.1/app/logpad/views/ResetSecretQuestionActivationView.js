import ActivationBaseView from 'core/views/ActivationBaseView';
import ELF from 'core/ELF';

export default class ResetSecretQuestionActivationView extends ActivationBaseView {
    constructor () {
        super();

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            // Tap event for submit form
            'click #submit': 'reactivate',

            // Input event for the security answer field
            'input #secret_answer': 'onInput',

            // Change event for security question dropdown list
            'change #secret_question': 'clearSecretAnswer'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'RESET_SECRET_QUESTION',
            secretAnswer: 'SECRET_ANSWER',
            secretQuestion: 'SECRET_QUESTION',
            submit: 'OK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            }
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'reset-secret-question-page'
     */
    get id () {
        return 'reset-secret-question-activation-page';
    }

    /**
     * @property {string} template - Id of template to render.
     * @readonly
     * @default '#reset-secret-question-template'
     */
    get template () {
        return '#reset-secret-question-activation-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let questions = LF.StudyDesign.securityQuestions;

        _(questions).each((question) => {
            if (question.display) {
                this.templateStrings[question.text] = question.text;
            }
        });

        this.needToRespond = { value: '' };

        return this.buildHTML({ key: this.key }, true)
        .then((strings) => {
            // Append the question options to the select element.
            questions.forEach((question) => {
                if (question.display) {
                    this.$('#secret_question').append(`<option value="${question.key}">${strings[question.text]}</option>`);
                }
            });

            this.$('#secret_question').select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });

            this.$('#secret_question').val(this.needToRespond.value).trigger('change');

            this.delegateEvents();
        });
    }

    /**
     * Submits the new password and security question and answer to the web service.
     * @param {Event} e Event data.
     */
    reactivate (e) {
        let password = LF.Data.password,
            answer = hex_sha512(this.$('#secret_answer').val()),
            question = this.$('#secret_question').val(),
            handleResult = _(function ({ res }) {
                LF.Data.question = question;
                LF.Data.answer = answer;
                LF.Data.deviceID = res.D;
                LF.Data.service_password = res.W;
                LF.Data.password = password;

                let token = `${LF.Data.code}:${LF.Data.service_password}:${LF.Data.deviceID}`;

                ELF.trigger('REACTIVATION:Transmit', { }, this)
                .finally(() => {
                    this.getSubjectData(token);
                });
            }).bind(this),
            handleError = _(({ errorCode, httpCode }) => {
                this.removeMessage();
                this.showTransmissionError({ errorCode, httpCode });
            }).bind(this);

        e.preventDefault();

        localStorage.removeItem('Reset_Password_Secret_Question');
        localStorage.removeItem('Reset_Password');

        this.attemptTransmission()
        .then((online) => {
            if (online) {
                let subjectData = {
                    U: LF.Data.code,
                    W: hex_sha512(password + localStorage.getItem('krpt')),
                    Q: question,
                    A: answer,
                    code: LF.Data.unlockCode
                };

                if (localStorage.getItem('PHT_IMEI')) {
                    subjectData.S = localStorage.getItem('PHT_IMEI');
                    subjectData.M = device.model;
                }

                LF.webService.getDeviceID(subjectData)
                .then(handleResult)
                .catch(handleError)
                .done();
            }
        })
        .done();
    }

    /**
     * Enables the OK button on secret_answer input.
     */
    onInput () {
        let $btn = this.$('#submit'),
            $input = this.$('#secret_answer'),
            $parent = $input.parent('.input-textbox'),
            value = $input.val(),
            codeFormat = new RegExp(LF.StudyDesign.answerFormat || '.{2,}');

        if (codeFormat.test(value)) {
            this.inputSuccess($parent);
            $btn.removeAttr('disabled');
        } else {
            this.inputError($parent);
            $btn.attr('disabled', 'disabled');
        }
    }

    /**
     * Clears the secret answer field and remove icons.
     */
    clearSecretAnswer () {
        let $secretAnswer = this.$('#secret_answer');

        $secretAnswer.val('');
        $secretAnswer.parent('.input-textbox').removeClass('valid-icon invalid-icon');
        this.$('#submit').attr('disabled', 'disabled');
    }
}

window.LF.View.ResetSecretQuestionActivationView = ResetSecretQuestionActivationView;

