import PageView from 'core/views/PageView';

export default class PrivacyPolicyBaseView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch prior to render.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'PRIVACY_POLICY',
            textLabel: 'PRIVACY_POLICY_TEXT_LABEL',
            text: 'PRIVACY_POLICY_TEXT',
            next: 'NEXT',
            back: 'BACK'
        };
    }

    /**
     * @property {string} template - Id of template to render.
     * @readonly
     * @default '#privacy-policy-template'
     */
    get template () {
        return '#privacy-policy-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<object>} The translated template strings.
     */
    render () {
        return this.buildHTML({}, true);
    }
}
