import COOL from 'core/COOL';
import CoreTransmit from 'core/transmit';

import subjectSync from './subjectSync';

let Transmit = _.extend({ }, CoreTransmit, {
    subjectSync
});

COOL.service('Transmit', Transmit);

LF.Transmit = Transmit;

export default Transmit;
