
/**
 * Determines if the file reader is supported.
 * @returns {boolean} True if supported, false if not.
 */
export function isFileReaderSupported () {
    return LF.Wrapper.platform === 'windows';
}

/**
 * Read a file from the app directory.
 * @param {string} fileName - The name of the file to read.
 * @returns {Q.Promise<string>} The contents of the file.
 */
export function readFileFromAppFolder (fileName) {
    return Q.Promise((resolve) => {
        if (isFileReaderSupported()) {
            Windows.Storage.KnownFolders.savedPictures.getFileAsync(fileName)
            .then((file) => {
                if (file) {
                    Windows.Storage.FileIO.readTextAsync(file)
                    .done((fileContent) => {
                        resolve(fileContent.trim() ? fileContent.trim() : null);
                    }, () => {
                        resolve(null);
                    });
                } else {
                    resolve(null);
                }
            }, () => {
                resolve(null);
            });
        } else {
            resolve(null);
        }
    });
}
