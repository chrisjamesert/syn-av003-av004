import Logger from 'core/Logger';
import BaseConfigScreen from './BaseConfigScreen';

const logger = new Logger('ScreenshotConfigDiary');

/**
 * @class ScreenshotConfigDiary
 * @description Screenshot Configuration view.
 */
/**
 *
 *
 * @export
 * @class ScreenshotConfigDiary
 * @extends {BaseConfigScreen}
 */
export default class ScreenshotConfigDiary extends BaseConfigScreen {
    /**
     * Creates an instance of ScreenshotConfigDiary.
     * @param {object} options options for this object constructor
     */
    constructor (options) {
        super(options);

        this.template = '#screenshot-template-diary-selection';
        this.currentSelectionName = 'diaries';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER',
            mode: 'diaries'
        }, this.templateStrings);
    }

    get id () {
        return 'screenshot-config-diary';
    }

    /**
     * Render the view
     * @returns {Q.Promise}
     */
    render () {
        return super.render({
            mode: this.templateStrings.mode,
            title: this.templateStrings.title
        })
        .then(() => {
            let diaries = _.findWhere(this.model.get('screens'), {
                name: 'diary'
            }).data;
            return this.createCheckboxListItems({ list: diaries });
        })
        .then(() => this.showPrevCheckedItems())
        .catch((err) => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Render the checkbox list.
     * @param {Object} options
     * @param {string[]} options.list - List of diaries to display
     * @returns {Q.Promise}
     */
    createCheckboxListItems ({ list }) {
        // Translate all displayNames then feed list into base createCheckboxListItems
        return Q.all(list.map((item) => {
            return LF.getStrings(item.get('displayName'), $.noop, { namespace: item.get('id') })
            .then((diaryName) => {
                return {
                    id: item.id || '',
                    displayName: diaryName || item.displayName || item.get('displayName') || item,
                    displayProp: 'id'
                };
            });
        }))
        .spread((...items) => {
            return super.createCheckboxListItems({
                list: items,
                displayProp: 'displayName',
                valueProp: 'id'
            });
        })
        .catch(err => logger.error('An error occurred', err));
    }
}
