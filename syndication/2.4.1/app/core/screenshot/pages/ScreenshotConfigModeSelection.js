import Logger from 'core/Logger';
import BaseConfigScreen from './BaseConfigScreen';

const logger = new Logger('ScreenshotConfigModeSelection');

/**
 * @class ScreenshotConfigModeSelection
 * @description Screenshot Configuration view.
 */
export default class ScreenshotConfigModeSelection extends BaseConfigScreen {
    constructor (options) {
        super(options);
        this.currentSelectionName = 'modes';

        this.template = '#screenshot-template-mode-selection';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER',
            mode: 'mode'
        }, this.templateStrings);
    }

    get id () {
        return 'screenshot-config-mode-selection';
    }

    /**
     * Go to the next config mode.
     * @param {string} url - The url to navigate to.
     */
    next (url) {
        this.model.set('modeIndex', 0);
        this.navigate(url);
    }

    /**
     * Navigate back to the previous screen.
     */
    back () {
        this.setModelSelected();
        this.model.prevMode();
        this.navigate('screenshot');
    }

    /**
     * Render the view
     * @returns {Q.Promise}
     */
    render () {
        return super.render({
            mode: this.templateStrings.mode,
            title: this.templateStrings.title
        })
        .then(() => {
            return this.createCheckboxListItems({
                list: this.model.get('modeToDisplay'),
                displayProp: 'displayName'
            });
        })
        .then(() => this.showPrevCheckedItems())
        .catch((err) => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }
}
