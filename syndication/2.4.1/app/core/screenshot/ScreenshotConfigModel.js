import * as coreUtilities from 'core/utilities';

/**
 * A Model representing the various selections in screenshot mode.
 * @class ScreenshotSelectionModel
 * @extends {Backbone.Model}
 */
export class ScreenshotSelectionModel extends Backbone.Model {
    /**
     * Default everything to an empty array.
     */
    get defaults () {
        return {
            languages: [],
            modes: [],
            diaries: [],
            core_screens: [],
            messageboxes: []
        };
    }
}

/**
 * The model representing the navigation state and other data of the screenshot mode.
 * @class ScreenshotConfigModel
 * @extends {Backbone.Model}
 */
export default class ScreenshotConfigModel extends Backbone.Model {
    constructor (options = {}) {
        super(options);

        this.listenTo(this, 'Model:ScreenshotsCompleted', this.resetSelection);


        this.diaryList = options.diaryList || LF.StudyDesign.questionnaires;

        this.coreScreens = options.coreScreens || (LF.router && LF.router.routes) || [];

        // Remove this once sitepad pages screenshots is implemented
        if (coreUtilities.isSitePad()) {
            this.coreScreens = _.omit(this.coreScreens, screen => screen.toLowerCase().indexOf('support') < 0);
        }

        // Omit questionnaire screen for core screen list.
        this.coreScreens = _.omit(this.coreScreens, value => value === 'application#questionnaire');

        this.defaults = {
            languages: ['English'],

            modeIndex: 0,

            modeToDisplay: [{
                id: 'diary',
                displayName: 'Diaries'
            }, {
                id: 'messagebox',
                displayName: 'Messagebox'
            }, {
                id: 'page',
                displayName: 'Pages'
            }],

            screens: [{
                name: 'language',
                displayName: 'Languages',
                screen_id: 0,
                data: [],
                skipScreen: false
            }, {
                name: 'mode',
                displayName: 'Mode',
                screen_id: 1,
                data: [],
                skipScreen: false
            }, {
                name: 'diary',
                displayName: 'Diaries',
                screen_id: 2,
                data: this.diaryList,
                skipScreen: false
            }, {
                name: 'page',
                displayName: 'Pages',
                screen_id: 3,
                data: this.coreScreens,
                skipScreen: false
            }, {
                name: 'messagebox',
                displayName: 'Messagebox',
                screen_id: 4,
                data: [],
                skipScreen: false
            }],

            selection: new ScreenshotSelectionModel()
        };

        this.set(this.defaults);
    }

    getSelection (type) {
        return type ? this.get('selection').get(type) : [];
    }

    setSelection (type, list) {
        this.get('selection').set(type, list);
    }

    /**
     * Change the current mode to the previous mode in the workflow.
     */
    prevMode () {
        this.set('modeIndex', this.get('modeIndex') - 1);
    }

    /**
     * Change the current mode to the next mode in the workflow.
     */
    nextMode () {
        const modeIndex = this.get('modeIndex');

        this.set('modeIndex', modeIndex + 1);
    }

    /**
     * Reset all selections to default (empty).
     * @returns {Q.Promise}
     */
    resetSelection () {
        return Q().then(() => {
            this.set('selection', new ScreenshotSelectionModel());
        });
    }
}
