/**
 * A detector that will detect when a page requires scrolling
 *  then loop through taking a screenshot of every part of the page.
 */
export default class ScrollingDetector {
    constructor (renderFunc, takeScreenshot, cleanup = $.noop) {
        this.renderFunc = renderFunc;
        this.takeScreenshot = takeScreenshot;
        this.cleanup = cleanup;

        this.isDetected = false;

        this.windowHeight = 0;
        this.scrollHeight = 0;
        this.documentHeight = 0;
        this.scrollPercentage = 0.75;
    }

    /**
     * Loop through scrolling sections and take a screenshot.
     * @param {number} sections - number of scrolling sections to loop through.
     * @param {number} index - the current index.
     * @returns {Q.Promise<void>}
     */
    loopSections (sections, index) {
        return Q()
        .then(() => {
            return this.delay > 0 ? Q.delay(this.delay) : Q();
        })
        .then(() => this.takeScreenshot({ suffix: `_SCROLL${index}` }))
        .then(() => {
            if (index < sections) {
                window.scrollBy(0, this.scrollHeight);
                return this.loopSections(sections, ++index);
            }

            return Q();
        });
    }

    /**
     * Start the detector, determine if the screen requires scrolling and if so, loop through sections.
     * @param {number} [options.documentHeight] - Use to spoof height.
     * @param {number} [options.delay] - Delay in ms between loops.
     * @returns {Q.Promise<void>}
     */
    run ({ documentHeight, delay } = {}) {
        this.windowHeight = $(window).height();
        this.documentHeight = documentHeight || $(document).height();

        // If there is no scrolling, skip.
        if (this.windowHeight === this.documentHeight) {
            return Q();
        }

        this.isDetected = true;
        this.delay = delay >= 0 ? delay : 1000;

        // We want to scroll only 75% of the screen to prevent cutoff text.
        this.scrollHeight = this.windowHeight * this.scrollPercentage;

        const sections = 1 + Math.ceil((this.documentHeight - this.windowHeight) / this.scrollHeight);
        return this.loopSections(sections, 1);
    }
}
