import QuestionnaireView from 'core/views/QuestionnaireView';
import NotifyView from 'core/views/NotifyView';
import BaseVas from 'core/widgets/VAS/BaseVas';
import Answer from 'core/models/Answer';
import User from 'core/models/User';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import * as utils from 'core/utilities';
import screenConfigurations from './screenSpecificConfigurations';

import { MessageRepo, Banner } from 'core/Notify';

import ScreenshotUtils from './screenshotUtils';
import * as DetectFeatures from './detectFeatures';
import ScrollingDetector from './detectFeatures/detectScrolling';

const logger = new Logger('ScreenshotTool');
const screenIdCache = {};

/**
 * Handles setting up and looping through screens for screenshots
 * @class ScreenshotTool
 */
export default class ScreenshotTool {
    constructor ({ controller, delay = 200, screenOptions }) {
        _.extend(this, { controller, delay });
        this.screenConfigurations = screenOptions || screenConfigurations;
    }

    /**
     * Returns a delayed promise. This function is needed so that it can be spied on in unit tests
     * @param {number} [delayTime=this.delay] the delay time in milliseconds
     * @returns {Q.Promise<void>}
     */
    delayIt (delayTime = this.delay) {
        return Q.delay(delayTime);
    }

    /**
     * Shows the completion notification.
     * @returns {Q.Promise<void>}
     */
    showNotification () {
        return new NotifyView().show({
            header: '',
            body: 'Screenshots capture is complete.',
            ok: 'OK',
            type: 'success'
        });
    }

    /**
     * Loops through each selected Diary for each selected Language
     * @returns {Q.Promise<void>}
     */
    takeDiaryScreenshots () {
        const diaryList = this.controller.model.get('selection').get('diaries');
        const languages = this.controller.model.get('selection').get('languages');

        const reduceDiaries = language => (diaryChain, diary) => {
            return diaryChain
            .then(() => {
                CurrentContext().setContextLanguage(language);
            })
            .then(() => {
                return this.prepDiary(diary);
            })
            .then(() => {
                return this.loopDiaryScreens(diary, language);
            })
            .then(() => {
                this.controller.clear();
            })
            .catch((err) => {
                logger.error(`Error taking screenshot of ${diary}`, err);
            });
        };

        const reduceLanguages = (languageChain, language) => diaryList.reduce(reduceDiaries(language), languageChain);

        return languages.reduce(reduceLanguages, Q())
        .catch((err) => {
            logger.error('Error completing screenshot capture', err);
        });
    }

    /**
     * Prep data needed to render the Diary.
     * @param  {string} id the string identifier for the diary.
     * @returns {Q.Promise<void>}
     */
    prepDiary (id) {
        let diariesWithCancel = this.screenConfigurations.diaries.diariesWithCancel,
            options = { id, subject: Data.subject };

        this.controller.clear();

        if (diariesWithCancel.indexOf(id) > -1) {
            options = { id, subject: Data.subject, showCancel: true };
        }

        this.controller.view = new QuestionnaireView(options);
        this.controller.view.Answer = Answer;
        Data.Questionnaire = this.controller.view;

        LF.security.activeUser = LF.security.activeUser || new User();

        return Q(this.controller.view)
        .tap((questionnaireView) => {
            return questionnaireView.prepScreens();
        })
        .tap((questionnaireView) => {
            return questionnaireView.prepQuestions();
        })
        .tap((questionnaireView) => {
            return questionnaireView.configureAffidavit();
        })
        .tap((questionnaireView) => {
            return questionnaireView.prepAnswerData(null, true);
        });
    }

    /**
     * Loop through all the screens configured for the diary in the given language.
     * @param {string} id the string identifier for the diary to render
     * @param {string} lang the language to render the diary screen in.
     * @param {number} [delay=this.delay] the amount of time in milliseconds to wait between screens to account for async problems.
     * @returns {Q.Promise<void>}
     */
    loopDiaryScreens (id, lang, delay = this.delay) {
        let numberOfScreensInDiary = this.controller.view.data.screens.length - 1;
        let diaryName = this.controller.view.id;

        const renderScreen = (screenId, questionIndex) => {
            LF.security.set('Last_Active', new Date().getTime());
            logger.trace(`Rendering screen: ${screenId} of Diary: ${diaryName}`);

            return Q(this.controller.view)
            .tap(questionnaireView => questionnaireView.render(screenId))
            .then((questionnaireView) => {
                // Fix for DE18951.
                // Convert canvases to an <img> tag before taking a screenshot.
                // https://github.com/gitawego/cordova-screenshot/issues/87
                const questionView = questionnaireView.questionViews && questionnaireView.questionViews[questionIndex];
                const widget = questionView && questionView.widget;

                if (widget && widget instanceof BaseVas) {
                    const canvases = widget.$('canvas');
                    if (canvases.length > 0) {
                        _.each(canvases, (canvas) => {
                            const imageMask = document.createElement('img');
                            const canvasEl = widget.$(canvas);

                            canvasEl.wrapAll('<div id="img-wrap"></div>');

                            imageMask.classList.add('image-mask');
                            imageMask.src = canvas.toDataURL('png');

                            $(imageMask).attr({
                                style: canvasEl.attr('style'),
                                height: canvasEl.attr('height'),
                                width: canvasEl.attr('width'),
                                id: canvasEl.attr('id')
                            })
                            .css({
                                display: 'block'
                            });

                            canvas.parentNode.removeChild(canvas);
                            widget.$('#img-wrap').append(imageMask);
                        });
                    }
                }
                return Q();
            })

            // Async problems =(
            .delay(delay)
            .catch((err) => {
                logger.error('Error rendering diary', err);
            });
        };

        const takeScreenshot = ({ screenId, indx, dText, subScreenId, suffix, paramFunc }) => {
            return this.createFileAndFolderName({
                diaryId: id,
                lang,
                fileOrder: indx + 1,
                screenId,
                dText,
                subScreenId,
                suffix,
                paramFunc
            })
            .then((names) => {
                const [filename, foldername] = names;

                logger.trace(`Taking screenshot of screen: ${screenId} from questionnaire: ${id}`);

                return ScreenshotUtils.takeScreenshot({ filename, foldername });
            })
            .catch((err) => {
                logger.error(`Error taking screenshot of screen: ${screenId} from questionnaire: ${id}`, err);
            });
        };

        // US6879: Take screenshots of expanded DatePicker widget and the DropDown list in diary
        const takeDatePickerScreenshots = (screenId, indx, subScreenId) => {
            let selectTag,
                dateBoxClass,
                screenIdOfLastScreen = `DATEPICKER_DIARY_S_${numberOfScreensInDiary}`;

            const takeScreenshotIndex = () => takeScreenshot({ screenId, indx, subScreenId: subScreenId++ });

            return Q(this.controller.view)
            .tap(() => renderScreen(screenId))
            .tap(() => takeScreenshotIndex())
            .tap((questionnaireView) => {
                if (screenId !== 'AFFIDAVIT') {
                    dateBoxClass = $('input.date-input');
                    dateBoxClass.datebox('open');

                    return Q.delay(1000)
                    .then(() => takeScreenshotIndex())
                    .then(() => {
                        selectTag = questionnaireView.getQuestions()[0].widget.$('select');
                        $(selectTag[0]).select2('open');
                    })
                    .delay(1000)
                    .then(() => takeScreenshotIndex())
                    .then(() => {
                        $(selectTag[0]).select2('close');
                        $(selectTag[1]).select2('open');
                    })
                    .delay(1000)
                    .then(() => takeScreenshotIndex())
                    .then(() => {
                        $(selectTag[1]).select2('close');
                    })
                    .then(() => {
                        dateBoxClass.datebox('close');
                        if (screenId === screenIdOfLastScreen) {
                            let countOfDays = [1, 1, 1, 1, 1, 1, 1];

                            return countOfDays.reduce((chain, currentDay) => {
                                return chain
                                .then(() => {
                                    let dateToSet = dateBoxClass.datebox('getTheDate');

                                    if (!(dateToSet instanceof Date)) {
                                        dateToSet = new Date();
                                    }

                                    dateToSet.setDate(dateToSet.getDate() + currentDay);
                                    dateBoxClass.datebox('setTheDate', dateToSet);

                                    return Q.delay(1000)
                                    .then(() => takeScreenshotIndex());
                                });
                            }, Q());
                        }
                        dateBoxClass = null;
                        return Q();
                    });
                }
                return Q();
            });
        };

        return this.controller.view.data.screens.reduce((chain, screen, indx) => {
            return chain
            .then(() => {
                const screenId = screen.get('id');

                // US6879: Added to check if fake DatePicker diary is selected.
                if (id.indexOf('DatePicker_Screenshot') !== -1) {
                    return takeDatePickerScreenshots(screenId, indx, 1);
                }

                return DetectFeatures.runDetectors({
                    render: () => renderScreen(screenId, indx),
                    takeScreenshot: ({ dText, subScreenId, suffix, paramFunc } = {}) => takeScreenshot({ screenId, indx, dText, subScreenId, suffix, paramFunc }),
                    cleanup: $.noop
                });
            });
        }, Q());
    }

    /**
     * Decide the name of the screenshot file and folder to place the screenshot in.
     * @param {Object} options object containing options
     * @param {string} [options.diaryId] - The id of the diary rendered, if we are not rendering a diary leave empty.
     * @param {string} [options.name] - The name to use for the file if we are not rendering a diary.
     * @param {string} options.lang - The language that the screen was rendered in.
     * @param {number} [options.fileOrder] - The order number of the file to be used for naming. Optional.
     * @param {number} [options.dText] - The order number of the Dynamic Text entry. Optional.
     * @param {number} [options.suffix] - Suffix to append to the end of the filename. Optional.
     * @param {string} options.screenId - The id of the screen.
     * @returns {Q.Promise<void>}
     */
    createFileAndFolderName (options) {
        let { diaryId, name, lang, fileOrder, screenId, dText, subScreenId, suffix, paramFunc } = options,
            maxStrLength = 3,
            prependZeros = (fileOrder, numOfZeros) => {
                return numOfZeros === 1 ? `0${fileOrder}` : `00${fileOrder}`;
            },
            fetchDiaryDisplayName = (diaryId) => {
                let cachedId = screenIdCache[diaryId];

                // DE18894 - String was being fetched w/key DISPLAY_NAME, which isn't the key
                // used for the display name for all diaries.  Instead, get the displayName
                // property from the questionnaire's configuration to determine the correct key to use.
                let displayName = utils.getNested(this, 'controller.view.model.attributes.displayName') || 'DISPLAY_NAME';

                return cachedId || LF.getStrings(displayName, (name) => {
                    screenIdCache[diaryId] = (() => {
                        return Q(name);
                    })();
                    return name;
                }, {
                    namespace: diaryId,
                    language: 'en',
                    locale: 'US'
                });
            };

        return Q()
        .then(() => {
            if (diaryId) {
                return fetchDiaryDisplayName(diaryId);
            }
            return name;
        })
        .then((name) => {
            let file,
                folder,
                fileOrderLength;

            if (fileOrder) {
                fileOrderLength = fileOrder.toString().length;

                if (fileOrderLength < maxStrLength) {
                    fileOrder = prependZeros(fileOrder, maxStrLength - fileOrderLength);
                }
            }

            if (fileOrder && screenId) {
                file = `${name.replace(/[\s+\/]/g, '')}_${fileOrder}_${screenId}`;
            } else {
                file = `${name.replace(/[\s+\/]/g, '')}`;
            }

            folder = lang.replace('-', '_');

            if (dText) {
                file += `_DynamicText${dText}`;
            }

            if (paramFunc) {
                file += `_ParamFunction${paramFunc}`;
            }
            if (subScreenId) {
                // case: if there're more than 26(the 26 alphabet) screen shots, keep adding Alphabet to the end of the filename.
                let alphaSuffix = '';
                while (subScreenId > 0) {
                    let remainder = subScreenId % 26;
                    subScreenId = Math.floor(subScreenId / 26);
                    if (remainder === 0) {
                        remainder = 26;
                        subScreenId--;
                    }
                    remainder = String.fromCharCode(64 + remainder);
                    alphaSuffix = `${remainder}${alphaSuffix}`;
                }
                file += alphaSuffix;
            }

            if (suffix) {
                file += suffix;
            }

            return [file, folder];
        })
        .catch(e => logger.error('An error occured fetching diary display name', e));
    }

    /**
     * Loop through all banners and notifications and take a screenshot.
     * @param {number} [delay=1000] The amount of time, in milliseconds, to wait between messages to account for async issues.
     * @returns {Q.Promise<void>}
     */
    takeMessageScreenshots (delay = 1000) {
        const languages = this.controller.model.get('selection').get('languages');

        const cleanup = () => {
            Banner && Banner.closeAll();

            // triggers 'hidden.bs.modal': 'teardown'
            $('.modal').modal('hide');
        };

        const renderMessage = (language, message, type) => {
            CurrentContext().setContextLanguage(language);

            switch (type) {
                case 'Dialog': {
                    // We can't wait for dialogs to render because they don't resolve until closed.
                    MessageRepo.display(message);
                    return Q.delay(delay);
                }
                case 'Banner': {
                    // noty uses callbacks, wrap in a resolved Promise.
                    Banner.clearcache();
                    return Q.delay(delay)
                    .then(() => Q.Promise((resolve) => {
                        MessageRepo.display(message, { afterShow: resolve });
                    }))
                    .then(() => Banner.flush());
                }
                default: {
                    logger.error(`Unsupported message type: ${type}`);
                }
            }
            return Q.resolve();
        };

        const takeScreenshot = (key, type, language, dText) => {
            return this.createFileAndFolderName({ name: key, lang: language, dText })
            .then((names) => {
                const [filename, foldername] = names;
                const name = `${type}_${filename}`;

                logger.trace(`Taking screenshot of message: ${key} of type: ${type}`);

                return ScreenshotUtils.takeScreenshot({ filename: name, foldername });
            })
            .catch((err) => {
                logger.error(`Error taking screenshot of message: ${key} of type: ${type}`, err);
            });
        };

        const reduceMessageTypes = language => (messageChain, type) => {
            const messageKeys = MessageRepo[type];

            if (!messageKeys) {
                return messageChain;
            }

            // Go through each message of this message type.
            return Object.keys(messageKeys).reduce((chain, key) => {
                return chain
                .then(() => {
                    return DetectFeatures.runDetectors({
                        cleanup,
                        render: () => renderMessage(language, messageKeys[key], type),
                        takeScreenshot: ({ dText } = {}) => takeScreenshot(key, type, language, dText)
                    });
                });
            }, messageChain);
        };

        const reduceLanguages = (languageChain, language) => {
            return MessageRepo.types.reduce(reduceMessageTypes(language), languageChain);
        };

        $('#application').hide();
        return languages.reduce(reduceLanguages, Q())
        .then(() => {
            $('#application').show();
        })
        .catch((err) => {
            logger.error('Error taking screenshots.', err);
        });
    }

    /**
     * Loops through each selected core screen for each selected Language
     * @param {number} [delay=1000] the amount of time in milliseconds to wait between screens to account for async problems.
     * @returns {Q.Promise<void>}
     */
    takeCoreScreenshots (delay = 1000) {
        const coreScreenList = this.controller.model.get('selection').get('pages');
        const languages = this.controller.model.get('selection').get('languages');
        let user;

        const renderScreen = ({ screen, routeMethod, routeController }) => {
            return Q()
            .then(() => {
                const options = {
                    force: true
                };

                const args = [];
                Object.keys(this.screenConfigurations.pages).forEach((page) => {
                    const config = this.screenConfigurations.pages[page];
                    if (screen.indexOf(page) >= 0) {
                        options.stringValues = config.stringValues;

                        if (config.args) {
                            args.push(config.args);
                        }
                    }
                });

                this.controller.clear();
                LF.security.set('Last_Active', new Date().getTime());

                return { options, args };
            })
            .then((screenArgs) => {
                // Set the current user to be Site administrator for login view.
                if (screen.indexOf('login') >= 0) {
                    let users = new Users();

                    // users.updateAdminUser requires this to be set for now.
                    localStorage.setItem('PHT_admin', JSON.stringify({ hash: '1111' }));

                    return users.updateAdminUser()
                    .then((adminUser) => {
                        user = adminUser;
                        screenArgs.options.adminUser = user;
                        return screenArgs;
                    });
                }
                return screenArgs;
            })
            .tap(() => this.delayIt(delay))
            .then(({ options, args }) => {
                LF.router.flash(options);
                return routeMethod.call(routeController, ...args, options);
            });
        };

        const loopCoreScreens = lang => (screenChain, currentScreen) => {
            const [routeMethod, routeController] = LF.router.getRouteMethodAndController(currentScreen);


            if (!routeMethod || !routeController) {
                return screenChain;
            }

            return screenChain
            .then(() => {
                CurrentContext().setContextLanguage(lang);
            })
            .delay(delay)
            .then(() => {
                const saveScreenshot = ({ lang, screenName, indx = -1, suffix }) => {
                    const name = indx >= 0 ? `${screenName}_${indx}` : screenName;

                    return this.createFileAndFolderName({ lang, name, suffix })
                    .then((names) => {
                        const [filename, foldername] = names;

                        logger.trace(`Taking screenshot of page: ${name}`);

                        return ScreenshotUtils.takeScreenshot({ filename, foldername });
                    })
                    .catch(err => logger.error('An error occured', err));
                };

                return DetectFeatures.runDetectors({
                    render: () => renderScreen({ screen: currentScreen, routeController, routeMethod }),
                    takeScreenshot: ({ suffix } = {}) => {
                        if (currentScreen.toLowerCase().indexOf('support') >= 0) {
                            let $dropdown = routeController.view.$('#support_dropdown');

                            return Q()
                            .then(() => _.reduce($dropdown.children('option'), (chain, option, indx) => {
                                return chain.then(() => {
                                    const val = $(option).val();

                                    routeController.view.$('#support_dropdown').val(val).change();

                                    // Extra delay for DE19018
                                    return Q.delay(500)
                                    .then(() => {
                                        return DetectFeatures.runDetectors({
                                            defaultDetectorClasses: [
                                                ScrollingDetector
                                            ],
                                            takeScreenshot (supportOptions = {}) {
                                                return saveScreenshot({
                                                    lang,
                                                    suffix: supportOptions.suffix,
                                                    screenName: currentScreen,
                                                    indx
                                                });
                                            }
                                        });
                                    });
                                });
                            }, Q()))
                            .then(() => {
                                $dropdown = null;
                            });
                        }

                        return saveScreenshot({ lang, screenName: currentScreen, suffix })
                        .catch((err) => {
                            logger.error(`Error taking screenshot of page: ${currentScreen}`, err);
                        });
                    },
                    cleanup: () => {
                        // Some views may set this during render and it needs to be removed.
                        localStorage.removeItem('PHT_User_Login');
                        if (user) {
                            user.destroy();
                            user = undefined;
                        }

                        return Q();
                    },
                    defaultDetectorClasses: [
                        ScrollingDetector
                    ]
                });
            })
            .catch((err) => {
                logger.error(`Error taking screenshot of screen: ${currentScreen}`, err);
            });
        };

        const reduceLanguages = (languageChain, language) => coreScreenList.reduce(loopCoreScreens(language), languageChain);

        return languages.reduce(reduceLanguages, Q())
        .then(() => {
            this.controller.clear();
        })
        .catch((err) => {
            logger.error('Error completing screenshot capture', err);
        });
    }

    /**
     * Take screenshots for selected mode.
     * @param {string[]} modes The selected modes to take screenshots of.
     * @returns {Q.Promise<void>}
     */
    takeModeScreenshot (modes = []) {
        const takeSSForMode = (mode) => {
            switch (mode) {
                case 'Diaries':
                    return this.takeDiaryScreenshots();
                case 'Messagebox':
                    return this.takeMessageScreenshots();
                case 'Pages':
                    return this.takeCoreScreenshots();
                default:
                    return Q();
            }
        };

        return modes.reduce((chain, mode) => chain.then(() => takeSSForMode(mode)), Q())
        .then(() => this.controller.model.trigger('Model:ScreenshotsCompleted'))
        .then(() => this.showNotification())
        .catch((err) => {
            logger.error('Error taking screenshots', err);
        });
    }
}
