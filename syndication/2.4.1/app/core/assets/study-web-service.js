import WebService from 'core/classes/WebService';
import COOL from 'core/COOL';

export default class StudyWebService extends COOL.getClass('WebService', WebService) {
}

COOL.add('WebService', StudyWebService);
