export default {

    // The default log level of the study
    // This controls what level of logs are sync'ed back to the server
    defaultLogLevel: 'ERROR',

    // Display any logs in the JavaScript developer's console.
    logToConsole: true,

    // If true, log entries are prefixed with timing information, relative to app startup
    showElapsed: false,

    // The key for each sub-object here is matched (currently EXACT MATCH ONLY)
    // against Logger names in the app code. Settings under that key are applied
    // to that logger, specifically.
    // The "*" contains default setting that are applied to all loggers.
    loggers: {
        // Examples:
        ELF: {
            // squelch some noise
            consoleLevel: 'INFO'
        },
        DataAccess: {
            // squelch some noise
            consoleLevel: 'WARN'
        },
        Subjects: {
            consoleLevel: 'INFO'
        },
        '*': {
            // If true, all TRACE logs include stack traces; otherwise only ERROR's do.
            traceStacks: false,

            // Independent of what log level is sent to server
            consoleLevel: 'TRACE',

            // Whether OPERATIONAL logs should also go to console
            operationalToConsole: false
        }
    }
};
