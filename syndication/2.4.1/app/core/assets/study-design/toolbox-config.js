// Settings toolbox items configuration

import ArrayRef from 'core/classes/ArrayRef';

export default {
    toolboxConfig: new ArrayRef([{
        id: 'set-alarms',
        toolboxRole: ['subject'],
        itemFunction: 'showSetAlarm'
    }, {
        id: 'change-password',
        toolboxRole: ['subject', 'caregiver', 'site'],
        itemFunction: 'showChangePassword'
    }, {
        id: 'change-secret-question',
        toolboxRole: ['subject', 'caregiver', 'site'],
        itemFunction: 'showChangeSecurityQuestion'
    }, {
        id: 'set-time-zone',
        toolboxRole: ['subject', 'site'],
        itemFunction: 'showSetTimeZone'
    }])
};
