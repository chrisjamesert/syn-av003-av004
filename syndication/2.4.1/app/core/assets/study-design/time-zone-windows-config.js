import ArrayRef from 'core/classes/ArrayRef';

export default {
    windows: {
        timeZoneOptions: new ArrayRef([{
            tzId: 'Hawaiian Standard Time',
            swId: 0
        }, {
            tzId: 'Alaskan Standard Time',
            swId: 2
        }, {
            tzId: 'Eastern Standard Time',
            swId: 21
        }, {
            tzId: 'Central European Standard Time',
            swId: 45
        }, {
            tzId: 'Tokyo Standard Time',
            swId: 60
        }, {
            tzId: 'Pacific Standard Time (Mexico)',
            swId: 4
        }, {
            tzId: 'Pacific Standard Time',
            swId: 6
        }, {
            tzId: 'US Mountain Standard Time',
            swId: 10
        }, {
            tzId: 'Mountain Standard Time',
            swId: 12
        }, {
            tzId: 'Central Standard Time',
            swId: 20
        }, {
            tzId: 'Central Standard Time (Mexico)',
            swId: 19
        }, {
            tzId: 'SA Pacific Standard Time',
            swId: 24
        }, {
            tzId: 'Eastern Standard Time (Mexico)',
            swId: 14
        }, {
            tzId: 'Atlantic Standard Time',
            swId: 25
        }, {
            tzId: 'Newfoundland Standard Time',
            swId: 27
        }, {
            tzId: 'E. South America Standard Time',
            swId: 29
        }, {
            tzId: 'UTC-02',
            swId: 30
        }, {
            tzId: 'GMT Standard Time',
            swId: 32
        }, {
            tzId: 'W. Europe Standard Time',
            swId: 43
        }, {
            tzId: 'Central Europe Standard Time',
            swId: 38
        }, {
            tzId: 'Romance Standard Time',
            swId: 40
        }, {
            tzId: 'GTB Standard Time',
            swId: 48
        }, {
            tzId: 'South Africa Standard Time',
            swId: 46
        }, {
            tzId: 'FLE Standard Time',
            swId: 52
        }, {
            tzId: 'Turkey Standard Time',
            swId: 50
        }, {
            tzId: 'Israel Standard Time',
            swId: 47
        }, {
            tzId: 'Kaliningrad Standard Time',
            swId: 51
        }, {
            tzId: 'Russian Standard Time',
            swId: 53
        }, {
            tzId: 'Ekaterinburg Standard Time',
            swId: 54
        }, {
            tzId: 'Omsk Standard Time',
            swId: 55
        }, {
            tzId: 'North Asia Standard Time',
            swId: 56
        }, {
            tzId: 'North Asia East Standard Time',
            swId: 57
        }, {
            tzId: 'W. Australia Standard Time',
            swId: 59
        }, {
            tzId: 'Taipei Standard Time',
            swId: 58
        }, {
            tzId: 'Yakutsk Standard Time',
            swId: 61
        }, {
            tzId: 'AUS Central Standard Time',
            swId: 63
        }, {
            tzId: 'AUS Eastern Standard Time',
            swId: 67
        }, {
            tzId: 'Vladivostok Standard Time',
            swId: 65
        }, {
            tzId: 'Magadan Standard Time',
            swId: 68
        }, {
            tzId: 'New Zealand Standard Time',
            swId: 69
        }, {
            tzId: 'Chatham Islands Standard Time',
            swId: 70
        }])
    }
};

/*
LIST OF TIMEZONES WITHOUT VALID SWID
 [{
 tzId: 'Dateline Standard Time',
 swId: 100
 }, {
 tzId: 'UTC-11',
 swId: 100
 }, {
 tzId: 'Aleutian Standard Time',
 swId: 100
 }, {
 tzId: 'Marquesas Standard Time',
 swId: 100
 }, {
 tzId: 'UTC-09',
 swId: 100
 }, {
 tzId: 'UTC-08',
 swId: 100
 }, {
 tzId: 'Mountain Standard Time (Mexico)',
 swId: 100
 }, {
 tzId: 'Central America Standard Time',
 swId: 100
 }, {
 tzId: 'Easter Island Standard Time',
 swId: 100
 }, {
 tzId: 'Canada Central Standard Time',
 swId: 100
 }, {
 tzId: 'Haiti Standard Time',
 swId: 100
 }, {
 tzId: 'Cuba Standard Time',
 swId: 100
 }, {
 tzId: 'US Eastern Standard Time',
 swId: 100
 }, {
 tzId: 'Paraguay Standard Time',
 swId: 100
 }, {
 tzId: 'Venezuela Standard Time',
 swId: 100
 }, {
 tzId: 'Central Brazilian Standard Time',
 swId: 100
 }, {
 tzId: 'SA Western Standard Time',
 swId: 100
 }, {
 tzId: 'Pacific SA Standard Time',
 swId: 100
 }, {
 tzId: 'Turks And Caicos Standard Time',
 swId: 100
 }, {
 tzId: 'Tocantins Standard Time',
 swId: 100
 }, {
 tzId: 'SA Eastern Standard Time',
 swId: 100
 }, {
 tzId: 'Argentina Standard Time',
 swId: 100
 }, {
 tzId: 'Greenland Standard Time',
 swId: 100
 }, {
 tzId: 'Montevideo Standard Time',
 swId: 100
 }, {
 tzId: 'Saint Pierre Standard Time',
 swId: 100
 }, {
 tzId: 'Bahia Standard Time',
 swId: 100
 }, {
 tzId: 'Azores Standard Time',
 swId: 100
 }, {
 tzId: 'Cape Verde Standard Time',
 swId: 100
 }, {
 tzId: 'UTC',
 swId: 100
 }, {
 tzId: 'Morocco Standard Time',
 swId: 100
 }, {
 tzId: 'Greenwich Standard Time',
 swId: 100
 }, {
 tzId: 'W. Central Africa Standard Time',
 swId: 100
 }, {
 tzId: 'Namibia Standard Time',
 swId: 100
 }, {
 tzId: 'Jordan Standard Time',
 swId: 100
 }, {
 tzId: 'Middle East Standard Time',
 swId: 100
 }, {
 tzId: 'Egypt Standard Time',
 swId: 100
 }, {
 tzId: 'E. Europe Standard Time',
 swId: 100
 }, {
 tzId: 'Syria Standard Time',
 swId: 100
 }, {
 tzId: 'West Bank Standard Time',
 swId: 100
 }, {
 tzId: 'Libya Standard Time',
 swId: 100
 }, {
 tzId: 'Arabic Standard Time',
 swId: 100
 }, {
 tzId: 'Arab Standard Time',
 swId: 100
 }, {
 tzId: 'Belarus Standard Time',
 swId: 100
 }, {
 tzId: 'E. Africa Standard Time',
 swId: 100
 }, {
 tzId: 'Iran Standard Time',
 swId: 100
 }, {
 tzId: 'Arabian Standard Time',
 swId: 100
 }, {
 tzId: 'Astrakhan Standard Time',
 swId: 100
 }, {
 tzId: 'Azerbaijan Standard Time',
 swId: 100
 }, {
 tzId: 'Russia Time Zone 3',
 swId: 100
 }, {
 tzId: 'Mauritius Standard Time',
 swId: 100
 },{
 tzId: 'Saratov Standard Time',
 swId: 100
 }, {
 tzId: 'Georgian Standard Time',
 swId: 100
 }, {
 tzId: 'Caucasus Standard Time',
 swId: 100
 }, {
 tzId: 'Afghanistan Standard Time',
 swId: 100
 }, {
 tzId: 'West Asia Standard Time',
 swId: 100
 }, {
 tzId: 'Pakistan Standard Time',
 swId: 100
 }, {
 tzId: 'India Standard Time',
 swId: 100
 }, {
 tzId: 'Sri Lanka Standard Time',
 swId: 100
 }, {
 tzId: 'Nepal Standard Time',
 swId: 100
 }, {
 tzId: 'N. Central Asia Standard Time',
 swId: 100
 },{
 tzId: 'Central Asia Standard Time',
 swId: 100
 }, {
 tzId: 'Bangladesh Standard Time',
 swId: 100
 }, {
 tzId: 'Myanmar Standard Time',
 swId: 100
 }, {
 tzId: 'SE Asia Standard Time',
 swId: 100
 }, {
 tzId: 'Altai Standard Time',
 swId: 100
 }, {
 tzId: 'W. Mongolia Standard Time',
 swId: 100
 }, {
 tzId: 'Tomsk Standard Time',
 swId: 100
 }, {
 tzId: 'China Standard Time',
 swId: 100
 }, {
 tzId: 'Singapore Standard Time',
 swId: 100
 }, {
 tzId: 'Ulaanbaatar Standard Time',
 swId: 100
 }, {
 tzId: 'North Korea Standard Time',
 swId: 100
 }, {
 tzId: 'Aus Central W. Standard Time',
 swId: 100
 }, {
 tzId: 'Transbaikal Standard Time',
 swId: 100
 }, {
 tzId: 'Korea Standard Time',
 swId: 100
 }, {
 tzId: 'Cen. Australia Standard Time',
 swId: 100
 }, {
 tzId: 'E. Australia Standard Time',
 swId: 100
 }, {
 tzId: 'West Pacific Standard Time',
 swId: 100
 }, {
 tzId: 'Tasmania Standard Time',
 swId: 100
 }, {
 tzId: 'Lord Howe Standard Time',
 swId: 100
 }, {
 tzId: 'Bougainville Standard Time',
 swId: 100
 }, {
 tzId: 'Russia Time Zone 10',
 swId: 100
 }, {
 tzId: 'Norfolk Standard Time',
 swId: 100
 }, {
 tzId: 'Sakhalin Standard Time',
 swId: 100
 }, {
 tzId: 'Central Pacific Standard Time',
 swId: 100
 }, {
 tzId: 'Russia Time Zone 11',
 swId: 100
 }, {
 tzId: 'UTC+12',
 swId: 100
 }, {
 tzId: 'Fiji Standard Time',
 swId: 100
 }, {
 tzId: 'Tonga Standard Time',
 swId: 100
 }, {
 tzId: 'Samoa Standard Time',
 swId: 100
 },{
 tzId: 'UTC+13',
 swId: 100
 }, {
 tzId: 'Line Islands Standard Time',
 swId: 100
 }]
 */

/*
COMPLETE LIST OF ALL TIMEZONES (from WINDOWS 10 device)
[{
    "displayName": "(UTC-12:00) International Date Line West ",
    "id": "Dateline Standard Time"
},
{
    "displayName": "(UTC-11:00) Coordinated Universal Time-11 ",
    "id": "UTC-11"
},
{
    "displayName": "(UTC-10:00) Aleutian Islands ",
    "id": "Aleutian Standard Time"
},
{
    "displayName": "(UTC-10:00) Hawaii ",
    "id": "Hawaiian Standard Time"
},
{
    "displayName": "(UTC-09:30) Marquesas Islands ",
    "id": "Marquesas Standard Time"
},
{
    "displayName": "(UTC-09:00) Alaska ",
    "id": "Alaskan Standard Time"
},
{
    "displayName": "(UTC-09:00) Coordinated Universal Time-09 ",
    "id": "UTC-09"
},
{
    "displayName": "(UTC-08:00) Baja California ",
    "id": "Pacific Standard Time (Mexico)"
},
{
    "displayName": "(UTC-08:00) Coordinated Universal Time-08 ",
    "id": "UTC-08"
},
{
    "displayName": "(UTC-08:00) Pacific Time (US & Canada) ",
    "id": "Pacific Standard Time"
},
{
    "displayName": "(UTC-07:00) Arizona ",
    "id": "US Mountain Standard Time"
},
{
    "displayName": "(UTC-07:00) Chihuahua, La Paz, Mazatlan ",
    "id": "Mountain Standard Time (Mexico)"
},
{
    "displayName": "(UTC-07:00) Mountain Time (US & Canada) ",
    "id": "Mountain Standard Time"
},
{
    "displayName": "(UTC-06:00) Central America ",
    "id": "Central America Standard Time"
},
{
    "displayName": "(UTC-06:00) Central Time (US & Canada) ",
    "id": "Central Standard Time"
},
{
    "displayName": "(UTC-06:00) Easter Island ",
    "id": "Easter Island Standard Time"
},
{
    "displayName": "(UTC-06:00) Guadalajara, Mexico City, Monterrey ",
    "id": "Central Standard Time (Mexico)"
},
{
    "displayName": "(UTC-06:00) Saskatchewan ",
    "id": "Canada Central Standard Time"
},
{
    "displayName": "(UTC-05:00) Bogota, Lima, Quito, Rio Branco ",
    "id": "SA Pacific Standard Time"
},
{
    "displayName": "(UTC-05:00) Chetumal ",
    "id": "Eastern Standard Time (Mexico)"
},
{
    "displayName": "(UTC-05:00) Eastern Time (US & Canada) ",
    "id": "Eastern Standard Time"
},
{
    "displayName": "(UTC-05:00) Haiti ",
    "id": "Haiti Standard Time"
},
{
    "displayName": "(UTC-05:00) Havana ",
    "id": "Cuba Standard Time"
},
{
    "displayName": "(UTC-05:00) Indiana (East) ",
    "id": "US Eastern Standard Time"
},
{
    "displayName": "(UTC-04:00) Asuncion ",
    "id": "Paraguay Standard Time"
},
{
    "displayName": "(UTC-04:00) Atlantic Time (Canada) ",
    "id": "Atlantic Standard Time"
},
{
    "displayName": "(UTC-04:00) Caracas ",
    "id": "Venezuela Standard Time"
},
{
    "displayName": "(UTC-04:00) Cuiaba ",
    "id": "Central Brazilian Standard Time"
},
{
    "displayName": "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan ",
    "id": "SA Western Standard Time"
},
{
    "displayName": "(UTC-04:00) Santiago ",
    "id": "Pacific SA Standard Time"
},
{
    "displayName": "(UTC-04:00) Turks and Caicos ",
    "id": "Turks And Caicos Standard Time"
},
{
    "displayName": "(UTC-03:30) Newfoundland ",
    "id": "Newfoundland Standard Time"
},
{
    "displayName": "(UTC-03:00) Araguaina ",
    "id": "Tocantins Standard Time"
},
{
    "displayName": "(UTC-03:00) Brasilia ",
    "id": "E. South America Standard Time"
},
{
    "displayName": "(UTC-03:00) Cayenne, Fortaleza ",
    "id": "SA Eastern Standard Time"
},
{
    "displayName": "(UTC-03:00) City of Buenos Aires ",
    "id": "Argentina Standard Time"
},
{
    "displayName": "(UTC-03:00) Greenland ",
    "id": "Greenland Standard Time"
},
{
    "displayName": "(UTC-03:00) Montevideo ",
    "id": "Montevideo Standard Time"
},
{
    "displayName": "(UTC-03:00) Saint Pierre and Miquelon ",
    "id": "Saint Pierre Standard Time"
},
{
    "displayName": "(UTC-03:00) Salvador ",
    "id": "Bahia Standard Time"
},
{
    "displayName": "(UTC-02:00) Coordinated Universal Time-02 ",
    "id": "UTC-02"
},
{
    "displayName": "(UTC-01:00) Azores ",
    "id": "Azores Standard Time"
},
{
    "displayName": "(UTC-01:00) Cabo Verde Is. ",
    "id": "Cape Verde Standard Time"
},
{
    "displayName": "(UTC) Coordinated Universal Time ",
    "id": "UTC"
},
{
    "displayName": "(UTC+00:00) Casablanca ",
    "id": "Morocco Standard Time"
},
{
    "displayName": "(UTC+00:00) Dublin, Edinburgh, Lisbon, London ",
    "id": "GMT Standard Time"
},
{
    "displayName": "(UTC+00:00) Monrovia, Reykjavik ",
    "id": "Greenwich Standard Time"
},
{
    "displayName": "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna ",
    "id": "W. Europe Standard Time"
},
{
    "displayName": "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague ",
    "id": "Central Europe Standard Time"
},
{
    "displayName": "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris ",
    "id": "Romance Standard Time"
},
{
    "displayName": "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb ",
    "id": "Central European Standard Time"
},
{
    "displayName": "(UTC+01:00) West Central Africa ",
    "id": "W. Central Africa Standard Time"
},
{
    "displayName": "(UTC+01:00) Windhoek ",
    "id": "Namibia Standard Time"
},
{
    "displayName": "(UTC+02:00) Amman ",
    "id": "Jordan Standard Time"
},
{
    "displayName": "(UTC+02:00) Athens, Bucharest ",
    "id": "GTB Standard Time"
},
{
    "displayName": "(UTC+02:00) Beirut ",
    "id": "Middle East Standard Time"
},
{
    "displayName": "(UTC+02:00) Cairo ",
    "id": "Egypt Standard Time"
},
{
    "displayName": "(UTC+02:00) Chisinau ",
    "id": "E. Europe Standard Time"
},
{
    "displayName": "(UTC+02:00) Damascus ",
    "id": "Syria Standard Time"
},
{
    "displayName": "(UTC+02:00) Gaza, Hebron ",
    "id": "West Bank Standard Time"
},
{
    "displayName": "(UTC+02:00) Harare, Pretoria ",
    "id": "South Africa Standard Time"
},
{
    "displayName": "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius ",
    "id": "FLE Standard Time"
},
{
    "displayName": "(UTC+02:00) Jerusalem ",
    "id": "Israel Standard Time"
},
{
    "displayName": "(UTC+02:00) Kaliningrad ",
    "id": "Kaliningrad Standard Time"
},
{
    "displayName": "(UTC+02:00) Tripoli ",
    "id": "Libya Standard Time"
},
{
    "displayName": "(UTC+03:00) Baghdad ",
    "id": "Arabic Standard Time"
},
{
    "displayName": "(UTC+03:00) Istanbul ",
    "id": "Turkey Standard Time"
},
{
    "displayName": "(UTC+03:00) Kuwait, Riyadh ",
    "id": "Arab Standard Time"
},
{
    "displayName": "(UTC+03:00) Minsk ",
    "id": "Belarus Standard Time"
},
{
    "displayName": "(UTC+03:00) Moscow, St. Petersburg, Volgograd ",
    "id": "Russian Standard Time"
},
{
    "displayName": "(UTC+03:00) Nairobi ",
    "id": "E. Africa Standard Time"
},
{
    "displayName": "(UTC+03:30) Tehran ",
    "id": "Iran Standard Time"
},
{
    "displayName": "(UTC+04:00) Abu Dhabi, Muscat ",
    "id": "Arabian Standard Time"
},
{
    "displayName": "(UTC+04:00) Astrakhan, Ulyanovsk ",
    "id": "Astrakhan Standard Time"
},
{
    "displayName": "(UTC+04:00) Baku ",
    "id": "Azerbaijan Standard Time"
},
{
    "displayName": "(UTC+04:00) Izhevsk, Samara ",
    "id": "Russia Time Zone 3"
},
{
    "displayName": "(UTC+04:00) Port Louis ",
    "id": "Mauritius Standard Time"
},
{
    "displayName": "(UTC+04:00) Saratov ",
    "id": "Saratov Standard Time"
},
{
    "displayName": "(UTC+04:00) Tbilisi ",
    "id": "Georgian Standard Time"
},
{
    "displayName": "(UTC+04:00) Yerevan ",
    "id": "Caucasus Standard Time"
},
{
    "displayName": "(UTC+04:30) Kabul ",
    "id": "Afghanistan Standard Time"
},
{
    "displayName": "(UTC+05:00) Ashgabat, Tashkent ",
    "id": "West Asia Standard Time"
},
{
    "displayName": "(UTC+05:00) Ekaterinburg ",
    "id": "Ekaterinburg Standard Time"
},
{
    "displayName": "(UTC+05:00) Islamabad, Karachi ",
    "id": "Pakistan Standard Time"
},
{
    "displayName": "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi ",
    "id": "India Standard Time"
},
{
    "displayName": "(UTC+05:30) Sri Jayawardenepura ",
    "id": "Sri Lanka Standard Time"
},
{
    "displayName": "(UTC+05:45) Kathmandu ",
    "id": "Nepal Standard Time"
},
{
    "displayName": "(UTC+06:00) Astana ",
    "id": "Central Asia Standard Time"
},
{
    "displayName": "(UTC+06:00) Dhaka ",
    "id": "Bangladesh Standard Time"
},
{
    "displayName": "(UTC+06:00) Omsk ",
    "id": "Omsk Standard Time"
},
{
    "displayName": "(UTC+06:30) Yangon (Rangoon) ",
    "id": "Myanmar Standard Time"
},
{
    "displayName": "(UTC+07:00) Bangkok, Hanoi, Jakarta ",
    "id": "SE Asia Standard Time"
},
{
    "displayName": "(UTC+07:00) Barnaul, Gorno-Altaysk ",
    "id": "Altai Standard Time"
},
{
    "displayName": "(UTC+07:00) Hovd ",
    "id": "W. Mongolia Standard Time"
},
{
    "displayName": "(UTC+07:00) Krasnoyarsk ",
    "id": "North Asia Standard Time"
},
{
    "displayName": "(UTC+07:00) Novosibirsk ",
    "id": "N. Central Asia Standard Time"
},
{
    "displayName": "(UTC+07:00) Tomsk ",
    "id": "Tomsk Standard Time"
},
{
    "displayName": "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi ",
    "id": "China Standard Time"
},
{
    "displayName": "(UTC+08:00) Irkutsk ",
    "id": "North Asia East Standard Time"
},
{
    "displayName": "(UTC+08:00) Kuala Lumpur, Singapore ",
    "id": "Singapore Standard Time"
},
{
    "displayName": "(UTC+08:00) Perth ",
    "id": "W. Australia Standard Time"
},
{
    "displayName": "(UTC+08:00) Taipei ",
    "id": "Taipei Standard Time"
},
{
    "displayName": "(UTC+08:00) Ulaanbaatar ",
    "id": "Ulaanbaatar Standard Time"
},
{
    "displayName": "(UTC+08:30) Pyongyang ",
    "id": "North Korea Standard Time"
},
{
    "displayName": "(UTC+08:45) Eucla ",
    "id": "Aus Central W. Standard Time"
},
{
    "displayName": "(UTC+09:00) Chita ",
    "id": "Transbaikal Standard Time"
},
{
    "displayName": "(UTC+09:00) Osaka, Sapporo, Tokyo ",
    "id": "Tokyo Standard Time"
},
{
    "displayName": "(UTC+09:00) Seoul ",
    "id": "Korea Standard Time"
},
{
    "displayName": "(UTC+09:00) Yakutsk ",
    "id": "Yakutsk Standard Time"
},
{
    "displayName": "(UTC+09:30) Adelaide ",
    "id": "Cen. Australia Standard Time"
},
{
    "displayName": "(UTC+09:30) Darwin ",
    "id": "AUS Central Standard Time"
},
{
    "displayName": "(UTC+10:00) Brisbane ",
    "id": "E. Australia Standard Time"
},
{
    "displayName": "(UTC+10:00) Canberra, Melbourne, Sydney ",
    "id": "AUS Eastern Standard Time"
},
{
    "displayName": "(UTC+10:00) Guam, Port Moresby ",
    "id": "West Pacific Standard Time"
},
{
    "displayName": "(UTC+10:00) Hobart ",
    "id": "Tasmania Standard Time"
},
{
    "displayName": "(UTC+10:00) Vladivostok ",
    "id": "Vladivostok Standard Time"
},
{
    "displayName": "(UTC+10:30) Lord Howe Island ",
    "id": "Lord Howe Standard Time"
},
{
    "displayName": "(UTC+11:00) Bougainville Island ",
    "id": "Bougainville Standard Time"
},
{
    "displayName": "(UTC+11:00) Chokurdakh ",
    "id": "Russia Time Zone 10"
},
{
    "displayName": "(UTC+11:00) Magadan ",
    "id": "Magadan Standard Time"
},
{
    "displayName": "(UTC+11:00) Norfolk Island ",
    "id": "Norfolk Standard Time"
},
{
    "displayName": "(UTC+11:00) Sakhalin ",
    "id": "Sakhalin Standard Time"
},
{
    "displayName": "(UTC+11:00) Solomon Is., New Caledonia ",
    "id": "Central Pacific Standard Time"
},
{
    "displayName": "(UTC+12:00) Anadyr, Petropavlovsk-Kamchatsky ",
    "id": "Russia Time Zone 11"
},
{
    "displayName": "(UTC+12:00) Auckland, Wellington ",
    "id": "New Zealand Standard Time"
},
{
    "displayName": "(UTC+12:00) Coordinated Universal Time+12 ",
    "id": "UTC+12"
},
{
    "displayName": "(UTC+12:00) Fiji ",
    "id": "Fiji Standard Time"
},
{
    "displayName": "(UTC+12:45) Chatham Islands ",
    "id": "Chatham Islands Standard Time"
},
{
    "displayName": "(UTC+13:00) Coordinated Universal Time+13 ",
    "id": "UTC+13"
},
{
    "displayName": "(UTC+13:00) Nuku\u0027alofa ",
    "id": "Tonga Standard Time"
},
{
    "displayName": "(UTC+13:00) Samoa ",
    "id": "Samoa Standard Time"
},
{
    "displayName": "(UTC+14:00) Kiritimati Island ",
    "id": "Line Islands Standard Time"
}]
*/

