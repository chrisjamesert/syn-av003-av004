// This is a core questionnaire required for the Skip Visit workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Skip_Visits)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [
        {
            id: 'Skip_Visits',
            SU: 'Skip_Visits',
            displayName: 'SKIP_VISITS',
            className: 'SKIP_VISIT',
            affidavit: undefined,
            screens: ['SKIP_VISIT_S_1'],
            product: ['sitepad'],
            accessRoles: ['site', 'admin']
        }
    ],

    screens: [
        {
            id: 'SKIP_VISIT_S_1',
            className: 'SKIP_VISIT_S_1',

            // DE16954 - Disable the back button.
            disableBack: true,
            questions: [
                { id: 'SKIP_VISIT_Q_1', mandatory: true }
            ]
        }
    ],

    questions: [
        {
            id: 'SKIP_VISIT_Q_1',
            IG: 'SkipVisit',
            IT: 'SKIP_VISIT',
            text: 'SKIP_VISITS_QUESTION_1',
            className: 'SKIP_VISIT_Q_1',
            widget: {
                id: 'SKIP_VISIT_Q1',
                type: 'SkipReason',
                className: 'SKIP_VISIT',
                answers: [
                    { text: 'SKIP_VISITS_REASON_0', value: '0' },
                    { text: 'SKIP_VISITS_REASON_1', value: '1' },
                    { text: 'SKIP_VISITS_REASON_2', value: '2' },
                    { text: 'SKIP_VISITS_REASON_3', value: '3' },
                    { text: 'SKIP_VISITS_REASON_4', value: '4' }
                ]
            }
        }
    ]
};
