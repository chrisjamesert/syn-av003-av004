import CurrentContext from 'core/CurrentContext';

/**
 * Checks the repeating diary schedule.
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param {LF.Collection.Dashboards} completedQuestionnaires list of all completed diaries.
 * @param {Object} parentView The view to append this scheduled item to.
 * @param {Object} context an object that contains subject model and visit model (sitepad only)
 * @param {Function} callback A callback function invoked upon.
 * @param {Boolean} isAlarm States if this scheduling is for an alarm
 */
LF.Schedule.schedulingFunctions.checkRepeatByDateAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    let nowUtcMillis = new Date().getTime(),
        startTimeUtc = LF.Utilities.parseTime(schedule.get('scheduleParams').startAvailability, true),
        endTimeUtc = LF.Utilities.parseTime(schedule.get('scheduleParams').endAvailability, true),
        questionnaireID = schedule.get('target').id,
        diary;

    if (nowUtcMillis >= startTimeUtc && nowUtcMillis < endTimeUtc) {
        diary = _.first(completedQuestionnaires.where({ questionnaire_id: questionnaireID }));

        if (!diary) {
            callback(true);
            return;
        }

        LF.Collection.Users.fetchCollection()
        .then((users) => {
            let userRoles = _.uniq(_.invoke(users.models, 'get', 'role')),
                accessRoles = questionnaireID ? _.findWhere(LF.StudyDesign.questionnaires.models, { id: questionnaireID }).get('accessRoles') : undefined,
                scheduleRoles = schedule.get('scheduleRoles'),
                rolesToCheck = isAlarm ? _.intersection(userRoles, scheduleRoles) : accessRoles ? _.intersection(userRoles, accessRoles) : CurrentContext().role.split(' '),
                isShared = !!schedule.get('sharedAcrossRoles'),
                completedRole = diary.get('role'),
                startedDate;

            if (!completedRole) {
                completedRole = ['FIRST'];
            }
            completedRole = (typeof completedRole === 'string') ? JSON.parse(completedRole) : completedRole;

            // Note:  Fix to DE16302.  shiftToNewLocal will float the value passed to be its equivalent time in
            //  the current time zone (e.g. noon will always be noon, regardless of the time zone)
            //  since we are using this date in relation to the range in local time.
            startedDate = new Date(LF.Utilities.shiftToNewLocal(diary.get('lastStartedDate'))).getTime();

            if (!(startTimeUtc < startedDate && endTimeUtc > startedDate)) {
                diary.unset('role');
                diary.save();
                callback(true);
            } else if (!isShared && !!_.difference(rolesToCheck, completedRole).length && (isAlarm || !_.contains(completedRole, CurrentContext().role))) {
                callback(true);
            } else {
                callback(false);
            }
        });
    } else {
        callback(false);
    }
};

export default LF.Schedule.schedulingFunctions.checkRepeatByDateAvailability;
