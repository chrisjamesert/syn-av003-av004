/* eslint-disable no-unreachable,new-cap,no-use-before-define */
import DABL from 'core/dataaccess/DABL';
import 'core/dataaccess/WebSQL';
import 'core/dataaccess/IndexedDB';
import Logger from 'core/Logger';
import ELF from 'core/ELF';

let logger = new Logger('DataAccess', { consoleOnly: true });

let query = (collection, params) => {
    if (params.where) {
        collection = _(collection).where(params.where);
    }

    if (params.order) {
        collection = _(collection).sortBy(obj => obj[params.order.field]);

        if (params.order.modifier === 'DESC') {
            collection = collection.reverse();
        }
    }

    if (params.limit) {
        collection = collection.splice(0, params.limit);
    }

    return collection;
};

export let eCoaDB;
export let LogDB;

export let createDatabases = () => {
    let prefer = ['IndexedDB', 'WebSQL'];

    // If the application is running as a native Windows or iOS application,
    // reverse the preferred plugin order so WebSQL is first.  Both windows and iOS have
    // flaws in their IndexedDB implementation.
    if (window.cordova && (window.cordova.platformId === 'windows' || window.cordova.platformId === 'ios')) {
        prefer.reverse();
    }

    eCoaDB = new DABL({
        name: 'eCoaDB',
        prefer,
        debug: false,
        showSql: false
    });

    LogDB = new DABL({
        name: 'LogDB',
        prefer,
        debug: false,
        showSql: false
    });
};


Backbone.sync = (method, object, options = {}) => {
    let db = object.storage === 'Log' ? LogDB : eCoaDB;

    // DE17884 - Increased the retry delay to 1/2 a second.  It's a rare case that the delay is ever hit,
    // so there is little to no performance hit.
    const delay = 500;

    switch (db[object.storage]) {
        case undefined: {
            logger.trace(`Initiating creation of storage for ${object.storage}`);
            return db.collection(object.storage)
                .delay(delay)

                // DE17884 - Calling Backbone.sync to initiate retry, in the case that the storage
                // object still does not exist.  This only seems to be a Windows (SQLite) issue.
                .then(() => Backbone.sync(method, object, options));
            break;
        }
        case 'pending': {
            logger.trace(`Waiting for creation of storage for ${object.storage} to settle`);
            return Q()
                .delay(delay)
                .then(() => Backbone.sync(method, object, options));
            break;
        }
        default: {
            return doSync(method, object, options);
        }
    }
};

let doSync = (method, object, options = { }) => {
    logger.debug(`Initiating ${method} for ${object}`); // assume objects have reasonable .toString()
    logger.trace(`Object data: ${JSON.stringify(object)}`);

    let db = object.storage === 'Log' ? LogDB : eCoaDB,
        storage = db[object.storage],
        deferred = Q.defer(),
        resolve,
        reject;

    resolve = (res) => {
        deferred.resolve(res);
        (options.onSuccess || $.noop)(res);
    };

    reject = (err) => {
        ELF.trigger('APPLICATION:DatabaseError', { err }, this)
        .then(() => {
            deferred.reject(err);
            (options.onError || $.noop)(err);
        })
        .done();
    };

    switch (method) {
        case 'read':
            // The object is a collection.
            if (object.length >= 0) {
                // A query has been provided...
                if (options.search) {
                    storage.all()
                    .then((result) => {
                        let output = [],
                            search = options.search || { };

                        // Loop thru each record and decrypt it.
                        result.each((item) => {
                            let model = new object.model(item);

                            model.decrypt && model.decrypt();

                            output.push(model.toJSON());
                        });

                        // Query the decrypted data.
                        output = query(output, search);

                        // Reset the collection with the query results.
                        object.reset(output);
                        resolve(object.models);

                        return object.models;
                    }, reject)
                    .done();
                } else {
                    // Fetch all data for the collection.
                    storage.all()
                    .then((result) => {
                        // Loop thru each record and decrypt it.
                        result.each((item) => {
                            let model = new object.model(item);

                            model.decrypt && model.decrypt();

                            object.add(model.toJSON());
                        });

                        resolve(object.models);

                        return object.models;
                    }, reject)
                    .done();
                }

            // The object is a model.
            } else {
                // If there is a query...
                if (options.search && options.search.where) {
                    storage.all()
                    .then((result) => {
                        let output = [],
                            search = options.search || { };

                        // Loop thru each record and decrypt it.
                        result.each((item) => {
                            let model = object.clone();

                            model.set(item);
                            model.decrypt && model.decrypt();

                            output.push(model.toJSON());
                        });

                        // Query the decrypted data.
                        output = query(output, search);

                        // Set the first data set to the model or an empty object literal.
                        object.set(output[0] || { });

                        resolve(object.attributes);
                    }, reject)
                    .done();

                // Otherwise, get by id...
                } else {
                    let id = object.get('id');

                    storage.get(id)
                    .then((res) => {
                        // Instead of encrypting directly on the model in question, we clone it and do so there.
                        let model = object.clone();

                        model.set(res);
                        model.decrypt && model.decrypt();

                        let result = model.toJSON();

                        object.set(result);
                        resolve(result);
                    })
                    .catch(reject)
                    .done();
                }
            }
            break;

        case 'create':
        case 'update': {
            let clone = object.clone();

            if (clone.isValid()) {
                storage.save(clone[clone.encrypt ? 'encrypt' : 'toJSON']())
                .then((result) => {
                    if (result) {
                        object.set({
                            id: result
                        });
                    }

                    resolve(result);

                    return result;
                }, reject);
            } else {
                reject(clone.validationError);
            }

            break;
        }
        case 'delete':

            if (object.length >= 0) {
                let requests = [];

                _.forEach(object.pluck('id'), (id) => {
                    requests.push(storage.remove(id));
                });

                Q.allSettled(requests)
                .then(resolve, reject)
                .done();
            } else {
                storage.remove(object.get('id'))
                .then(resolve, reject)
                .done();
            }
            break;

        case 'clear' :
            storage.clear()
            .then(resolve, reject)
            .done();
            break;

        case 'count' :
            storage.count()
            .then(resolve, reject)
            .done();
            break;

        // This method is deprecated.  Just resolves the promise to support legacy code.
        case 'createStorage':
            resolve();
            break;

        case 'removeStorage':
            storage.drop()
            .then(resolve, reject)
            .done();
            break;

        // This method is deprecated.  Just resolves the promise to support legacy code.
        case 'checkInstall':
            resolve();
            break;

        // No default
    }

    return deferred.promise
    .tap(() => logger.trace(`Finished ${method} for ${object}`));
};

createDatabases();

// @todo phase this namespace out.
LF.dataAccess = eCoaDB;
