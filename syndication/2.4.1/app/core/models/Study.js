// @todo Defunct - Appears to be dead code
import StorageBase from './StorageBase';

/**
 * A model that defines study level data.
 * @class Study
 * @extends StorageBase
 * @example let model = new Study();
 */
export default class Study extends StorageBase {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'Study'
     */
    get name () {
        return 'Study';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            studyName: {
                type: String,
                required: true
            },
            created: {
                type: String,
                required: true
            },
            modified: {
                type: String,
                required: true
            }
        };
    }
}

window.LF.Model.Study = Study;
