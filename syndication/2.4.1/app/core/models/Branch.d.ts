/*
 * Represents a branch in StudyDesign
 */
interface Branch {
    branchFrom: string,
    branchTo: string,
    clearBranchedResponses: boolean,
    branchFunction: string | Function,
    branchParams?: any
}
