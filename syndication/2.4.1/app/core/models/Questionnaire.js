import Base from './Base';

/**
 * A model that defines a questionnaire configuration.
 * @class Models.Questionnaire
 * @extends Models.Base
 * @example let model = new Questionnaire();
 */
export default class Questionnaire extends Base {
    /**
     * @memberof Models.Questionnaire
     * @property {Object} defaults - Default values of every questionnaire model.
     * @property {string} defaults.className - The default class assigned to the questionnaire's root element.
     * @property {boolean} defaults.previousScreen - Allows the user to use the back button while taking the questionnaire
     * @property {boolean} defaults.allowTranscriptionMode - Disables transcription mode for the questionnaire
     * @readonly
     */
    get defaults () {
        return {
            className: 'questionnaire',
            previousScreen: true,
            allowTranscriptionMode: false
        };
    }

    /**
     * The model's schema used for validation.
     * @readonly
     * @type {Object}
     * @memberof Models.Questionnaire
     * @property {Models.SchemaType} id - {@link Models.Questionnaire.id}
     * @property {Models.SchemaType} SU - {@link Models.Questionnaire.SU}
     * @property {Models.SchemaType} displayName - {@link Models.Questionnaire.displayName}
     * @property {Models.SchemaType} className - {@link Models.Questionnaire.className}
     * @property {Models.SchemaType} previousScreen - {@link Models.Questionnaire.previousScreen}
     * @property {Models.Relationship} affidavit - {@link Models.Questionnaire.affidavit}
     * @property {Models.Relationship} screens - {@link Models.Questionnaire.screens}
     * @property {Models.Relationship} schedules - {@link Models.Questionnaire.schedules}
     * @property {Models.SchemaType} triggerPhase - {@link Models.Questionnaire.triggerPhase}
     * @property {Models.Relationship} branches - {@link Models.Questionnaire.branches}
     * @property {Models.Relationship} accessRoles - {@link Models.Questionnaire.accessRoles}
     * @property {Models.SchemaType} noRoleFound - {@link Models.Questionnaire.noRoleFound}
     * @property {Models.SchemaType} product - {@link Models.Questionnaire.product}
     * @property {Models.SchemaType} allowTranscriptionMode - {@link Models.Questionnaire.allowTranscriptionMode}
     */
    static get schema () {
        return {
            /**
             * The unique identifier of the Questionnaire. This is a required property with a maximum string length of 48 characters.
             * @typedef {string} Models.Questionnaire.id
             * @example
             * questionnaires: [{
             *     id: 'New_User',
             *     ...
             * }]
             */
            id: {
                type: String,
                width: 48,
                required: true
            },

            /**
             * The Signing Unit (SU) of the questionnaire. e.g. 'Daily'.
             * This value is stored in each Dashboard record and transmitted to StudyWorks.
             * This is a required property.
             * @typedef {string} Models.Questionnaire.SU
             * @example
             * questionnaires: [{
             *     SU: 'New_User',
             *     ...
             * }]
             */
            SU: {
                type: String,
                required: true
            },

            /**
             * The display name of the questionnaire.
             * This is a translation key that should be in the questionnaire's resourse strings.
             * This is a required property.
             * @typedef {string} Models.Questionnaire.displayName
             * @example
             * questionnaires: [{
             *     displayName: 'NEW_USER',
             *     ...
             * }]
             */
            displayName: {
                type: String,
                required: true
            },

            /**
             * A css class appended to the questionnaire view's root DOM node.
             * @typedef {string} Models.Questionnaire.className
             * @example
             * questionnaires: [{
             *     className: 'ADD_USER',
             *     ...
             * }]
             */
            className: {
                type: String,
                required: false
            },

            /**
             * Determines if the user can navigate back to the previous question
             * @typedef {string} Models.Questionnaire.previousScreen
             * @example
             * questionnaires: [{
             *     previousScreen: true,
             *     ...
             * }]
             */
            previousScreen: {
                type: Boolean,
                required: false
            },

            /**
             * The ID of the affidavit to display.
             * Must match id of an existing affidavit
             *
             * Affidavit can be set to false to indicate that no affidavit should be shown.
             * A null or undefined value will fallback a role's default affidavit.
             * @typedef {string|boolean} Models.Questionnaire.affidavit
             * @example
             * questionnaires: [{
             *     affidavit: 'NewUserAffidavit',
             *     ...
             * }]
             */
            affidavit: {
                type: [Boolean, String],
                relationship: {
                    to: 'Affidavits',
                    multiplicity: '0..1',

                    // We have to inform the validator to ignore any case where the affidavit might be set to false.
                    // Any value other than null or undefined will be matched against affidavit IDs.
                    ignore: [false]
                }
            },

            /**
             * An array of Screen IDs that belong to the questionnaire.
             * Screen IDs must match a configured screen the the study design.
             * There must be at least one item in the given array.
             * @typedef {string[]} Models.Questionnaire.screens
             * @example
             * questionnaires: [{
             *     screens: ['DAILY_S1', 'DAILY_S2'],
             *     ...
             * }]
             */
            screens: {
                relationship: {
                    to: 'Screens',
                    multiplicity: '1..*'
                }
            },

            /**
             * The configuration of the questionnaire's schedules. {@link Models.Schedule}
             * @typedef {string[]} Models.Questionnaire.schedules
             * @example
             * questionnaires: [{
             *     schedules: ?,
             *     ...
             * }]
             */
            schedules: {
                relationship: {
                    to: 'Schedules',
                    multiplicity: '0..*'
                }
            },

            /**
             * TODO: Move this to study design typings once immplemented.
             * The phase used by the study.
             * @typedef {Object} studyPhase
             * @example <caption>Default configuration</caption>
             * studyPhase: {
             *     SCREENING     : 10,
             *     RANDOMIZATION : 20,
             *     TREATMENT     : 30,
             *     FOLLOWUP      : 40,
             *     TERMINATION   : 999
             * },
             */

            /**
             * If set, will trigger a phase change for the subject upon completion of the questionnaire. See {@link studyPhase} or your configured studyPhase for available options
             * @typedef {string[]} Models.Questionnaire.triggerPhase
             * @example
             * questionnaires: [{
             *     triggerPhase: 'FOLLOWUP',
             *     ...
             * }]
             */
            triggerPhase: {
                type: String,
                required: false
            },

            /**
             * TODO: implement branching typings and move to it's own file.
             * @typedef {Object} BranchingConfig
             */

            /**
             * Branching configuration
             * @typedef {BranchingConfig[]} Models.Questionnaire.branches
             * @example
             * questionnaires: [{
             *     branching: [{
             *         branchFrom: 'DAILY_DIARY_S_4',
             *         branchTo: 'AFFIDAVIT',
             *         clearBranchedResponses: true,
             *         branchFunction: 'isCurrentPhase',
             *         branchParams: {
             *             value: 'SCREENING'
             *         }
             *     }],
             *     ...
             * }]
             */
            branches: {
                relationship: {
                    to: 'Branches',
                    multiplicity: '0..*',
                    embedded: true
                }
            },

            /**
             * Configure the roles that have access to this questionnaire.
             * Must match the id of a role in the Roles configuration.
             * @typedef {string[]} Models.Questionnaire.accessRoles
             * @example
             * questionnaires: [{
             *     accessRoles: ['site', 'subject'],
             *     ...
             * }]
             */
            accessRoles: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..*'
                }
            },

            /**
             * Key to configured modal for display if none of the access roles are found.
             * If blank, no dialog will be displayed.
             * @typedef {string} Models.Questionnaire.noRoleFound
             * @example
             * questionnaires: [{
             *     noRoleFound: 'NO_ROLE_FOUND_DEFAULT',
             *     ...
             * }]
             */
            noRoleFound: {
                type: String,
                required: false
            },

            /**
             * The Product(s) which should display the diary.
             * @typedef {string[]} Models.Questionnaire.product
             * @example
             * questionnaires: [{
             *     product: ['sitepad'],
             *     ...
             * }]
             */
            product: {
                required: false,
                type: Array
            },

            /**
             * Indicates whether the given questionnaire can be run in
             * transcription mode.
             * @typedef {Boolean} Models.Questionnaire.allowTranscriptionMode
             * @example
             * questionnaires: [{
             *      allowTranscriptionMode: true
             * }]
             */
            allowTranscriptionMode: {
                required: false,
                type: Boolean
            }
        };
    }
}

window.LF.Model.Questionnaire = Questionnaire;
