/* eslint-disable consistent-return */
import StorageBase from './StorageBase';

/**
 * A model that defines the last diary completed.
 * @class LastDiary
 * @extends StorageBase
 * @example let model = new LastDiary();
 */
export default class LastDiary extends StorageBase {
    constructor (options) {
        super(options);

        this.dateKeys = {
            lastStartedDate: true,
            lastCompletedDate: true,
            updated: true
        };

        // Iterate through keys to explicitly call set for field formatting/validation.
        // We should be sure we only process items in the schema, though
        for (let k in options) {
            if (this.schema[k]) {
                this.set(k, options[k]);
            }
        }
    }

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'LastDiary';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: Number
            },

            // The krpt of the subject the questionnaire belongs to.
            // e.g. 'NP.bd832fd9e78b4b22854bde3d4795dabe'
            krpt: {
                type: String,
                required: true
            },

            // The ID of the questionnaire as defined by the study design. e.g. 'P_Daily_Diary'
            questionnaire_id: {
                type: String,
                required: true
            },

            // The date on which the questionnaire was last started.
            // e.g. '2016-03-14T09:50:15-04:00'
            lastStartedDate: {
                type: String,
                required: false
            },

            // The date on which the questionnaire was last completed.
            // e.g. '2016-03-14T13:50:15Z'
            lastCompletedDate: {
                type: String,
                required: false
            },

            // The role of the last user that completed the questionnaire. e.g. 'subject';
            role: {
                relationship: {
                    to: 'Role',
                    multiplicity: '0..1'
                }
            },

            // When the record was last updated.  e.g. '2016-04-27T14:13:31Z'
            updated: {
                type: String,
                required: false
            }
        };
    }

    /**
     * Validates the LastDiary model.
     * @param {Object} attributes The model attributes to validate.
     * @returns {String} A validation error message.
     */
    validate (attributes) {
        let message = super.validate(attributes);

        // If super failed with a message, return that message.
        if (message) {
            return message;
        }

        // Using find, instead of each, since it can be broken.
        _.find(attributes, (attr, key) => {
            if (key in this.dateKeys) {
                // Ok with null or undefined attr, but not OK with one that doesn't parse as a date.
                if (attr != null && !Date.parse(attr.toString())) {
                    message = new Error(`Attribute: ${key}. Invalid DataType. Expected date-parsable string. ` +
                        `Received "${attr.toString()}", which cannot be parsed as a date.`);
                }
            }

            return message;
        });

        if (message) {
            return message;
        }
    }

    set (key, value) {
        // This switch attempts to ensure that the lastStartedDate is always in the ISO8601 format containing
        // local time with a time zone offset.
        switch (typeof key) {
            case Object:
                this.dateKeys && Object.keys(this.dateKeys).forEach((dateKey) => {
                    // skip if date is not in dateKeys, is null, or is an invalid date
                    // or if it is already in ISO format
                    //  validation function will decide if the last 2 cases are acceptable.
                    if (dateKey in key &&
                            typeof key[dateKey] === 'string' &&
                            !key[dateKey].match(/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})[+-](\d{2}):(\d{2})/g) &&
                            !isNaN(Date.parse(key[dateKey]))) {
                        key[dateKey] = LF.Utilities.dateToISO8601WithTZ(new Date(key[dateKey]));
                    }
                });
                break;
            case String:
                // skip if date is not in dateKeys, is null, or is an invalid date
                // or if it is already in ISO format
                //  validation function will decide if the last 2 cases are acceptable.
                if (key in this.dateKeys && typeof value === 'string' &&
                        !value.match(/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})[+-](\d{2}):(\d{2})/g) &&
                        !isNaN(Date.parse(value))) {
                    value = LF.Utilities.dateToISO8601WithTZ(new Date(value));
                }
                break;
            default:

            // do nothing
        }

        // eslint-disable-next-line prefer-rest-params
        let args = Array.prototype.slice.call(arguments);
        return super.set.apply(this, [key, value].concat(args.slice(2)));
    }
}

window.LF.Model.LastDiary = LastDiary;
