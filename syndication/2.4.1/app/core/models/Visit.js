import Base from './Base';

/**
 * A model that defines a visit configuration.
 * @class Models.Visit
 * @extends Models.Base
 * @example let model = new Visit({
 *   id    : 'visit3',
 *   studyEventId: '30',
 *   displayName  : 'VISIT_3',
 *   visitType : 'ordered',
 *   visitOrder : 3,
 *   forms : ['P_Welcome_SitePad', 'P_Meds_SitePad'],
 *   delay: '0'
 * });
 */
class Visit extends Base {
    /**
     * Get the schema properties, which define the study design configurations and are used in the Study Design validation.
     * @memberof Models.Visit
     * @readonly
     * @type {Object}
     * @see Models.SchemaType
     * @property {Models.SchemaType} id - {@link Models.Visit.id}
     * @property {Models.SchemaType} studyEventId - {@link Models.Visit.studyEventId}
     * @property {Models.SchemaType} displayName - {@link Models.Visit.displayName}
     * @property {Models.SchemaType} visitType - {@link Models.Visit.visitType}
     * @property {Models.SchemaType} visitOrder - {@link Models.Visit.visitOrder}
     * @property {Models.Relationship} forms - {@link Models.Visit.forms}
     * @property {Models.SchemaType} delay - {@link Models.Visit.delay}
     */
    static get schema () {
        return {
            /**
             * The unique identifier of the Visit. This is a required property.
             * @typedef {string} Models.Visit.id
             * @example
             * visits: [{
             *     id: 'visit1',
             *     ...
             * }]
             */
            id: {
                type: String,
                required: true
            },

            /**
             * Identifier that is sent as a response for visit start and end forms. Matches up with SW Events
             * @typedef {string} Models.Visit.studyEventId
             * @example
             * visits: [{
             *     studyEventId: '10',
             *     ...
             * }]
             */
            studyEventId: {
                type: String
            },

            /**
             * The display name for the visit. Must be a translation key string value.
             * @typedef {string} Models.Visit.displayName
             * @example
             * visits: [{
             *     displayName: 'VISIT_01',
             *     ...
             * }]
             */
            displayName: {
                type: String
            },

            /**
             * Unscheduled visit can be started anytime but will automatically close if an ordered visit or another unscheduled visit is started.
             * @typedef {string} Models.Visit.visitTypeUnscheduled
             */

            /**
             * Container visit can be started anytime and will always be available unless explicitly hidden.
             * @typedef {string} Models.Visit.visitTypeContainer
             */

            /**
             * The visit will be ordered by {@link Models.Visit.visitOrder} configuration. Will be unavailable if a visit later in the order is started.
             * @typedef {string} Models.Visit.visitTypeOrdered
             */

            /**
             * Determines the structure of the visit TODO: Better explanation
             * @typedef {
             *     Models.Visit.visitTypeUnscheduled |
             *     Models.Visit.visitTypeContainer |
             *     Models.Visit.visitTypeOrdered
             * } Models.Visit.visitType
             * @example
             * visits: [{
             *     visitType: 'unscheduled',
             *     ...
             * }]
             */
            visitType: {
                type: String
            },

            /**
             * If the {@link Models.Visit.visitType} is configured to be {@link Models.Visit.visitTypeOrdered}, this  determines the display order on the gateway.
             * @typedef {number} Models.Visit.visitOrder
             * @example
             * visits: [{
             *     visitOrder: 1,
             *     ...
             * }]
             */
            visitOrder: {
                type: Number
            },

            /**
             * An array of Questionnaire IDs that belong  to the form. Relates to any number of Questionnaire ids
             * @typedef {Models.Questionnaire.id} Models.Visit.forms
             * @example
             * visits: [{
             *     forms: ['EQ5D', 'FieldTypeSamples'],
             *     ...
             * }]
             */
            forms: {
                relationship: {
                    to: 'Questionnaires',
                    multiplicity: '0..*'
                }
            },

            /**
             *
             * @typedef {string} Models.Visit.delay
             */
            delay: {
                type: String
            }
        };
    }
}

window.LF.Model.Visit = Visit;
export default Visit;
