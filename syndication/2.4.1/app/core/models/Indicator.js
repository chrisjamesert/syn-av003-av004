import Base from './Base';

/**
* A model that defines an Indicator configuration.
* @class Indicator
* @extends Base
* @example let model = new Indicator();
*/
export default class Indicator extends Base {
    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: String,
                width: 30
            },
            className: {
                type: String,
                required: false
            },
            image: {
                type: String,
                required: false
            },
            label: {
                type: String,
                required: false
            }
        };
    }
}

window.LF.Model.Indicator = Indicator;
