/**
 * @namespace Models
 * @description Holds types used for study design validation.
 */

/**
 * The terminology 'relationship', and 'multiplicity', as well as the notation used for multiplicity, are taken from the UML standard.
 * They are like 'foreign keys' in database terms.
 * Multiplicity is also known as 'cardinality' (not in UML, though).
 * Multiplicity specifies a lower bound, and an upper bound on the number of objects that should be referenced.
 * @typedef {Object} Relationship
 * @memberof Models
 * @property {Object} relationship - The relationship configuration
 * @property {!string} relationship.to - The Collection in the study design that this has a relationship with.
 * @property {!string|string[]} relationship.multiplicity - The multiplicity bounds of the relationship. '0..1' will mean that it can refer to 0 or 1 of the items in the 'to' configuration. Since the max is one, the input must be a string instead of an array. '1..*' would mean that there must be a relationship and cannot be null since the min is one. '*' as the max means that there can be any number of items which also means that the input must be an array of strings even if there is only a single element.
 * @property {?string} relationship.via - In the study-design, the relationship from Screen to Questions allows properties on the relationship itself.
 * e.g. is this question mandatory/optional in the context of this particular screen.
 * One property in this intermediate object must be the id of the ultimate target Element:
 * @property {?boolean} relationship.embeddded - Marks if there is a relationship within the current object configuration structure, such as the embedded usage example below
 * @property {?any[]} relationship.ignore - Ignore errors if outside of normal bounds.
 * @example <caption>Basic minimal example</caption>
 * affidavit: {
 *    relationship: {
 *        to: 'Affidavits',
 *        multiplicity: '0..1'
 *    }
 * },
 * @example <caption>"via" usage</caption>
 * screens: [
 *    {
 *        id          : 'ADD_PATIENT_S_1',
 *        className   : 'ADD_PATIENT_S_1',
 *        questions   : [
 *            { id: 'ADD_PATIENT_ID', mandatory: true },
 *            { id: 'ADD_PATIENT_INITIALS', mandatory: true }
 *        ]
 *    },
 * ...
 *
 * In the schema, this is indicated by 'via' in the relationship definition.
 *
 *     questions: {
 *         relationship: {
 *             to: 'Questions',
 *             via: 'id',
 *             multiplicity: '1..*'
 *         }
 *     },
 * Here, via: ‘id’ refers to the fact that to get from the Screen
 *  to the Question you have to pass through an intermediate object’s
 *  ‘id’ property.  All other properties of this intermediate object are
 *  ignored and not validated
 * @example <caption>"embedded" usage</caption>
 * questions: [{
 *     id: 'ACTIVATE_USER_USERNAME',
 *     ...
 *     widget: {
 *         id: 'ACTIVATE_USER_W_1',
 *          type: 'StaticField',
 *          label: 'FULL_NAME',
 *          field: 'username'
 *     }
 * }]
 * @example <caption>"ignore" usage</caption>
 * affidavit: {
 *     type: [Boolean, String],
 *     relationship: {
 *         to: 'Affidavits',
 *         multiplicity: '0..1',
 *         // We have to inform the validator to ignore any case where the affidavit might be set to false.
 *         // Any value other than null or undefined will be matched against affidavit IDs.
 *         ignore: [false]
 *     }
},
 */

/**
 * @description Javascript type/class. Type can be String, Number, Boolean, Array, Object, or an array of any listed.
 * @typedef SchemaType
 * @memberof Models
 * @type {StringConstructor|NumberConstructor|BooleanConstructor|ArrayConstructor|ObjectConstructor|Models.Relationship|Array}
 */

/**
 * @typedef {Object} SchemaProperty
 * @memberof Models
 * @property {!Models.SchemaType} type - The type of value permitted for the property
 * @property {?boolean} required - if true, a non-null value must be supplied. If false, the value is not thus constrained.
 * @property {?number} width - If set to a number, and the property is of type String, then only string values up to this number of characters are valid. If width is unset, the property value is not constrained to any size.
 */
