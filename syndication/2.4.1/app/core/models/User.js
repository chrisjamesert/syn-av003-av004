import StorageBase from './StorageBase';
import UserSync from 'core/classes/UserSync';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Subjects from 'core/collections/Subjects';

const logger = new Logger('Model.User');

/**
 * A model to represent users.
 * @class User
 * @extends StorageBase
 */
export default class User extends StorageBase {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'User'
     */
    get name () {
        return 'User';
    }

    /**
     * @property {Object} defaults - Default values to populate on the model.
     */
    get defaults () {
        return {
            failedLoginAttempts: 0,
            failedQuestionAttempts: 0,
            enableDeviceUtc: 0
        };
    }

    /**
     * Create and save new User model from given subject model, then save the id to the subjects user field.
     * @static
     * @param {Subject} subject - The subject to create a user from.
     * @param {Object} [options] - Extra options to pass to the user creation.
     * @returns {Q.Promise<User>} Promise that resovles with the newly created user.
     */
    static createUserFromSubject (subject, options = {}) {
        const user = new this(_.extend({
            role: 'subject'
        }, options));

        return user;
    }

    /**
    * @property {Object} schema - The model's schema used for validation and storage construction.
    * @static
    * @readonly
    */
    static get schema () {
        return {
            id: {
                type: Number
            },

            // The user's username, which may vary depending on type of user (Site vs. Subject).
            // Currently, on the LPA the a subject's username is SUBJECT_ROLE.
            username: {
                type: String,
                required: false, // temp saved w/o username
                encrypt: true
            },

            // A encrypted password.
            // Encryption should be done as follows: hex_sha512(password + salt);
            password: {
                type: String,
                required: false, // temp saved w/o password
                encrypt: true
            },

            // The salt used when encrypting the user's password.
            // If the password is temporary, the salt will be 'temp'.
            // If the user is a subject, the salt is the subject's krpt.
            // All others have a GUID generated at time of creation.
            salt: {
                type: String,
                required: false,
                encrypt: true
            },

            // The key of the user's selected security question parsed as a string. e.g. 0
            // See trial/study-design/security-questions-config.js for the configuration.
            secretQuestion: { // to secret_question.
                relationship: {
                    to: 'SecurityQuestions',
                    multiplicity: '0..1'
                },
                encrypt: true
            },

            // The user's encrypted security answer.
            secretAnswer: { // to secret_answer
                type: String,
                encrypt: true
            },

            // The language and locale of the user. e.g. 'en-US'
            language: {
                type: String
            },

            // The user's role. e.g. 'site'
            role: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..1'
                }
            },

            // The number of failed login attempts. e.g. 0
            failedLoginAttempts: {
                type: Number
            },

            // The number of failed attempts to enter the security question. e.g. 0
            failedQuestionAttempts: {
                type: Number
            },

            // This is a timestamp used to determine when a user's account should be unlocked.
            enableDeviceUtc: {
                type: Number
            },

            // The date at which the user last logged in. e.g. 'Wed May 04 2016 15:06:59 GMT-0400 (EDT)'
            lastLogin: {
                type: String
            },

            // TODO - This value doesn't seem to be used. Do we still need it?
            lastSync: {
                type: String
            },

            // Determines if the user is active. 1 for active, 0 for inactive.
            active: {
                type: Number
            },

            // A unique identifier generated server-side via a stored procedure.
            // See LPA_AddUser.sql - [userId] int identity (1,1) primary key.
            userId: {
                type: Number
            },

            // The type of user can really be anything, but we have a few core types.
            // 'Admin' is restricted LPA use of it's administator role.
            // 'Subject' is for all subject records.
            // 'SiteUser' is used for all other site-level users.
            userType: {
                type: String
            },

            // This value is used to query for which users belong to a device.
            // For example, if the user's role has a syncLevel of 'site', the sync value will be
            // the site's KrDOM. e.g. 'DOM.108974.410'
            syncValue: {
                type: String
            },

            // TODO - Can we remove this?
            eMail: {
                type: String,
                encrypt: true
            },

            // Determines if user needs to create a new password.
            permanentPasswordCreated: {
                type: Boolean
            }
        };
    }

    /**
     * checks if the passed in object has all of the required Schema fields
     * @param {Object} objectToCheck - The target object to check.
     * @returns {Boolean}
     */
    isSchemaSet (objectToCheck) {
        for (let name in this.schema) {
            if (this.schema[name].required && objectToCheck[name] == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Return the user's role found in the study design.
     * @returns {Role}
     */
    getRole () {
        return LF.StudyDesign.roles.findWhere({ id: this.get('role') });
    }

    /**
     * Get the syncLevel of this user's role
     * @returns {String} Sync Level of this user's role
     */
    getSyncLevel () {
        let role = this.getRole();
        return role && role.get('syncLevel');
    }

    /**
     * Create transmission for changing this user's password and/or security question and answer
     * @param {Object} params - Password credentials to update.
     * @param {string} params.password - User's new password
     * @param {string} params.salt - salt for hash of user's new password
     * @param {string} params.secretQuestion - User's new security question
     * @param {string} params.secretAnswer - User's new security answer
     * @param {string} [params.auth] - token for webservice authentication
     * @returns {Q.Promise<boolean>}
     */
    changeCredentials (params) {
        // TODO: Better to split the two functionalities in this function (save & transmit).

        let fields = ['password', 'salt', 'secretQuestion', 'secretAnswer'],
            syncLevel = this.getSyncLevel(),
            values = _.defaults({},
                _.pick(params, fields),
                _.pick(this.toJSON(), fields)),
            onSuccess = () => {
                // this.fetch() needed to refresh model's data changed with triggered Rule (DE18747).
                return this.fetch()
                .then(() => this.save(values))
                .then(() => true)
                .catch(() => false);
            };

        if (!syncLevel) {
            return onSuccess();
        }

        return UserSync.getValue(syncLevel)
        .then((value) => {
            let parameters = _.extend({}, values, {
                id: this.get('id'),
                userId: this.get('userId')
            });

            if (!value && value !== '') {
                return onSuccess();
            }

            return ELF.trigger(`USER:ChangeCredentials/${this.get('userId')}`, {
                method: 'updateUserCredentials',
                params: JSON.stringify(parameters),
                created: new Date().getTime()
            }, this)

            // TODO: Handle the returned value since it could be an error code
            .then(onSuccess)
            .catch((err) => {
                logger.error('Error transmitting updateUserCredentials', err);
            });
        })
        .catch((err) => {
            logger.error('Transmission did not save, error retrieving sync value.', err);
            return false;
        });
    }

    /**
     * Deactivate this user.
     * @returns {Q.Promise<void>}
     */
    deactivate () {
        return this.save({ active: 0 })
        .then(() => {
            if (this.get('userType') === 'Admin') {
                logger.operational('Admin user became inactive');
            } else {
                logger.operational(`${this.get('username')} became inactive`);
            }
        })
        .catch((err) => {
            err && logger.error('Error deactivating user', err);
        });
    }

    /**
     * Activate this user
     * @returns {Q.Promise<void>}
     */
    activate () {
        return this.save({ active: 1 })
        .then(() => {
            if (this.get('userType') === 'Admin') {
                logger.operational('Admin user became active');
            } else {
                logger.operational(`${this.get('username')} became active`);
            }
        })
        .catch((err) => {
            err && logger.error('Error activating user', err);
        });
    }

    /**
     * Check if this user is still active, if not then deactivate
     * @return {Q.Promise<void>}
     */
    checkActive () {
        let syncLevel = this.getSyncLevel();

        if (!syncLevel) {
            return Q();
        }

        return UserSync.getValue(syncLevel)
        .then((val) => {
            if (val === false) {
                return false;
            }

            // if syncvalue has changed and does not match this users syncvalue, deactivate.
            if (this.get('syncValue') !== val && this.get('userType') !== 'Admin') {
                return this.deactivate();
            }
            return this.activate();
        })
        .catch((err) => {
            err && logger.error('Error checking user active status', err);
        });
    }

    /**
     * Returns whether the user is a subject user or not
     * @returns {Boolean} true if the user is a subject user
     */
    isSubjectUser () {
        return this.get('userType') === 'Subject';
    }

    /**
     * Returns the subject of the user if it is a subject user
     * @returns {Q.Promise<Subject>}
     */
    getSubject () {
        if (this.isSubjectUser()) {
            let subjects = new Subjects();

            return subjects.fetch({ search: { where: { user: this.get('id') } } })
            .then(() => subjects.at(0));
        }
        return Q(null);
    }
}

window.LF.Model.User = User;
