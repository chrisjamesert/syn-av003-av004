import Base from './Base';

/**
 * A model that defines a Schedule configuration.
 * @class Schedule
 * @extends Base
 * @example var model = new Schedule();
 */
export default class Schedule extends Base {
    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: String,
                width: 68, // These are constructed with concatenation of other id's
                required: true
            },
            dbid: {
                type: Number,
                required: false
            },
            target: {
                // TODO: there is a limitation here.  target is supposed to be
                // a relationship to one of many types. What we really want to say
                // is something like:
                // relationship: {
                //    to: {
                //        selector: 'objectType',
                //        cases: {
                //            questionnaire: 'Questionnaires',
                //            indicator: 'Indicators'
                //        }
                //    },
                //    via: 'id',
                //    multiplicity: '1'
                // },
                // but no such capability is implemented; so we'll use generic Object type,
                // which means no validation :-(
                type: Object,
                required: true
            },
            scheduleFunction: {
                type: String,
                required: true
            },
            scheduleParams: {
                type: Object,
                required: false
            },
            scheduleRoles: {
                type: Array,
                required: false
            },
            alarmFunction: {
                type: String,
                required: false
            },
            alarmParams: {
                type: Object,
                required: false
            },
            phase: {
                type: Array,
                required: false
            }
        };
    }
}

window.LF.Model.Schedule = Schedule;
