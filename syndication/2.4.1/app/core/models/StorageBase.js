import Base from './Base';
import { eCoaDB } from '../dataAccess';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';

let logger = new Logger('models/StorageBase', { consoleOnly: true });

/**
 * The base model for all storage-based models. Not a stand-alone model.
 * @class StorageBase
 * @extends Base
 * @example export default class MyModel extends StorageBase { }
 */
export default class StorageBase extends Base {
    constructor (options) {
        super(options);

        /**
         * @property {string} The storage object is automatically generate by the name of the class.
         */
        this.storage = this.constructor.name;

        // handle browsers that don't support constructor names
        if (this.storage === undefined) {
            let funcNameRegex = /function\s([^(]{1,})\(/;
            this.storage = funcNameRegex.exec(this.constructor.toString())[1].trim();
        }

        if (!eCoaDB[this.storage]) {
            eCoaDB.collection(this.storage);
        }
    }

    /**
     * Calls directly out to Backbone.Model.save, but disables validation.
     * Validation is instead called in Backbone.sync, before the model is saved.
     * If validation fails, the returned promise will be rejected.
     * @param {object=} attrs Additional attributes to update.
     * @param {object=} opts Options. See Backbone.Model.save();
     * @example model.save().then(onSuccess, onError);
     * @returns {Q.Promise}
     */
    save (attrs = undefined, opts = {}) {
        opts.validate = false;

        return super.save(attrs, opts);
    }

    // separated out for testing purposes
    getKey () {
        // ucode stands for Unlock Code. Since it is being used as the encryption key
        // in sitepad, it is better to use a rather undescriptive localstorage key.
        let key = localStorage.getItem('krpt') || lStorage.getItem('ucode');

        if (!key) {
            logger.debug('krpt or ucode (Unlock Code) not found');

            // But do not throw; let null key be used, because current
            // activation process will otherwise break
        }

        return key;
    }

    /**
     * Encrypts attributes determined by the model's schema.
     * @returns {Object} The encrypted data.
     * @example model.encrypt();
     */
    encrypt () {
        let key = this.getKey();

        GibberishAES.size(128);

        this.attributes = this.parse(this.attributes);

        _(this.attributes).each((value, index) => {
            let encrypt = this.schema[index] && this.schema[index].encrypt;

            if (encrypt && value != null) {
                try {
                    this.attributes[index] = GibberishAES.enc(value, key);
                } catch (err) {
                    ELF.trigger('APPLICATION:EncryptionError', { err }, this)
                    .then((evt) => {
                        if (!evt.preventDefault) {
                            logger.error('Unable to encrypt model.', new Error(err.toString()));
                        }
                    })
                    .done();
                }
            }
        });

        return this.attributes;
    }

    /**
     * Decrypts any encrypted attributes.
     * @returns {Object} The decrypted data.
     * @example model.decrypt();
     */
    decrypt () {
        let key = this.getKey();

        GibberishAES.size(128);

        _(this.attributes).each((value, index) => {
            let encrypt = this.schema[index] && this.schema[index].encrypt;

            if (encrypt && value != null) {
                try {
                    this.attributes[index] = GibberishAES.dec(value, key);
                } catch (err) {
                    ELF.trigger('APPLICATION:DecryptionError', { err }, this)
                    .then((evt) => {
                        if (!evt.preventDefault) {
                            logger.error('Unable to decrypt model.', new Error(err.toString()));
                        }
                    })
                    .done();
                }
            }
        });

        return this.attributes;
    }
}
