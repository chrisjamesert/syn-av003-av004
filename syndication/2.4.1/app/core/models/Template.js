/**
 * A model that defines a template configuration.
 * @class Template
 * @extends Backbone.Model
 * @example
 * let model = new Template({
 *     name: 'Input',
 *     namespace: 'CORE',
 *     language: 'en',
 *     locale: 'US',
 *     template: '<input id="{{ id }}" type="text" value="{{ value }}" />'
 * });
 */
export default class Template extends Backbone.Model {
    /**
     * @property {Object} defaults - Default values of every Template model.
     * @readonly
     */
    get defaults () {
        return {
            // The default namespace of the template.
            namespace: 'DEFAULT'
        };
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {

            // The name of the template. e.g 'Input'.
            // Must be unique per namespace/language & locale.
            name: {
                type: String,
                required: true
            },

            // Used in combination with the templates name to select a template.
            // Namespaces can point to a Questionnaire's ID. e.g. 'DAILY'
            // Defaults to 'DEFAULT'
            namespace: {
                type: String
            },

            // ISO 639-1 format. e.g. 'en'
            language: {
                type: String,
                required: false
            },

            // ISO 3166-1 alpha-2 format. e.g. 'US'
            locale: {
                type: String,
                required: false
            },

            // Stringified HTML template. See Underscore's _.template().
            template: {
                type: String,
                required: true
            }
        };
    }
}

window.LF.Model.Template = Template;
