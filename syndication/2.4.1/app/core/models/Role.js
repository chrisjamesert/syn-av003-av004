import Base from './Base';

/**
 * Model that defines role data.
 * @augments Base
 */
export default class Role extends Base {
    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type {Object}
     */
    static get schema () {
        return {

            // The ID of the role.  Must be unique. e.g. 'subject'
            id: {
                type: String,
                width: 30,
                required: true
            },

            // A class appended to the root DOM node upon user's login. e.g. 'theme-a'
            theme: {
                type: String,
                required: false
            },

            // A translation key to be used for the display of the role. e.g. 'SUBJECT'
            displayName: {
                type: String,
                required: false
            },

            // A map of translation keys to use within the application.
            text: {
                type: Object,
                required: false
            },

            // A map of dialog keys to use within the application.
            dialogs: {
                type: Object,
                required: false
            },

            // The sync level of the role. See the User model. e.g. 'site'
            syncLevel: {
                type: String,
                required: false
            },

            unlockCodeConfig: {
                type: Object,
                required: false
            },

            // Which affidavit to display for the role.
            defaultAffidavit: {
                relationship: {
                    to: 'Affidavits',
                    multiplicity: '0..1'
                }
            },

            // This value is used as a response on an Answer record when saving the LastDiary. e.g. 0
            lastDiaryRoleCode: {
                type: Number,
                required: true
            },

            // An array of roles available when creating a new user. e.g. ['site']
            addPermissionsList: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..*',
                    ignore: ['ALL']
                }
            },

            // The icon used on the login screen to represent the user. e.g. 'admin-icon'
            userSelectIconClass: {
                type: String,
                required: false
            },

            // Used to display a list of selectable languages from the LoginView
            supportedLanguages: {
                type: Array,
                required: false
            },

            // Determines if the user can add user's offline.
            offlineSync: {
                type: Boolean,
                required: false
            },

            permissions: {
                type: Array,
                required: false
            },

            // The product the role should be use for. // e.g. ['sitepad', 'logpad']
            product: {
                type: Array,
                required: true
            },

            // An optional password format to be user for all users of said role.
            // eg. { min: 2, max: 8 }
            passwordFormat: {
                type: Object,
                required: false
            }
        };
    }

    /**
     * Check if this role is able to add or update while offline.
     * @returns {boolean}
     */
    canAddOffline () {
        return this.get('id') === 'subject' ||

            // stakutis
            // I am not a fan of this, but it solves the problem of a transmission happening after a newly added user sets their password and secret question.
            // This should be fixed or removed in the future... Users collections has already been imported so the model cannot be overwritten.
            localStorage.getItem('trainer') || this.get('offlineSync') === true;
    }
}

window.LF.Model.Role = Role;
