import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method logout
 * @description
 * Logout of the application
 * @param {null|undefined} params - Not used in this action.
 * @param {Function} callback - Invoked to retain the action chain.
 * @example
 * // Example of logout configured as an ELF action.
 * resolve: [{
 *     action: 'logout'
 * }]
 * @example
 * // Example of importing and invoking logout.
 * import { logout } from 'core/actions/logout';
 *
 * logout();
 */
export function logout (params, callback) {
    LF.security.logout(true);

    // TODO - Update to use promise.
    callback();
}

ELF.action('logout', logout);

// @todo remove
LF.Actions.logout = logout;
