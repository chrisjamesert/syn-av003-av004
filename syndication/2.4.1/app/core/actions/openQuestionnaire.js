import Data from 'core/Data';
import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method openQuestionnaire
 * @description
 * Open a specified questionnaire.
 * @param {Object} params - Includes the id of the questionnaire and schedule id
 * @param {string} params.questionnaire_id - The ID of the questionnaire to open.
 * @param {string} params.schedule_id - The schedule ID of the questionnaire.
 * @param {Function} callback - Callback invoke to retain the action chain.
 * @example
 * // Example of configuring openQuestionnaire as an ELF action.
 * resolve: [{
 *    action: 'openQuestionnaire',
 *    data: { questionnaire_id: 'Daily_Diary', schedule_id: 'Daily_DiarySchedule1' }
 * }]
 * @example
 * // Example of importing and invoking openQuestionnaire.
 * import { openQuestionnaire } from 'core/actions/openQuestionnaire';
 *
 * openQuestionnaire({
 *     questionnaire_id: 'Daily_Diary',
 *     schedule_id     : 'Daily_DiarySchedule1'
 * }, () => {
 *    // The questionnaire has been opened...
 * });
 */
export function openQuestionnaire (params, callback) {
    // TODO: This is only used in LogPad and won't work for SitePad. Move it from core to logpad

    // DE12483: Delay opening questionnaire to prevent the issue of questionnaire screen display
    _.delay(() => {
        Data.Questionnaire = {};

        LF.router.navigate(`questionnaire/${params.questionnaire_id}/${params.schedule_id}`, true);

        // TODO - Update to use promise.
        callback();
    }, 150);
}

ELF.action('openQuestionnaire', openQuestionnaire);

// TODO - remove
LF.Actions.openQuestionnaire = openQuestionnaire;
