import Logs from 'core/collections/Logs';
import Sites from 'core/collections/Sites';
import * as lStorage from 'core/lStorage';
import { createGUID } from 'core/utilities';
import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';
import { getCurrentProtocol } from 'core/Helpers';

// Big base for constant-width value;
let uniqueTail = 10000;

class LogXmitItem {
    constructor (params) {
        this.id = new Date().getTime() + uniqueTail++;
        this.jsonrpc = '2.0';
        this.method = 'logEntry';

        // params.id is reserved on the LogServer sending it causes logs to get stuck in netpro
        // moved the value to processid (we don't provide process ids)
        // to be used later when we want to delete the record from the localdb
        params.processId = params.id;
        delete params.id;
        this.params = [params];
    }
}

class LogXmitBundle {
    constructor ({ siteCode = null, subject = null, device = null }) {
        const Site = siteCode || (subject ? subject.get('site_code') : null);

        this.SessionID = createGUID();
        lStorage.setItem('logGUID', this.SessionId);

        const nameValuePairs = {
            SessionID: this.SessionID,
            Study: getCurrentProtocol(subject),
            SubjectID: subject ? subject.get('subject_id') : null,
            krpt: subject ? subject.get('krpt') : null,
            Locale: `${LF.Preferred.language}_${LF.Preferred.locale}`,
            ProdVer: LF.coreVersion,
            StudyVer: LF.StudyDesign.studyVersion,
            Sponsor: LF.StudyDesign.clientName,
            Device: device,
            Site
        };

        // Create the SessionStart item
        const item = new LogXmitItem({
            source: LF.appName,
            location: 'actions.transmitLogs',
            clientTime: new Date().toISOString(),
            type: 'SessionStart',
            level: 'INFO',
            message: 'Session',

            // DE18595: Value in name value pair should not be null.
            nameValuePairs: _.omit(nameValuePairs, value => value === null)
        });

        this.items = [item];
    }

    getItems () {
        return { Json: this.items };
    }

    addItem (item) {
        // Ensure there is a SessionID NVP present
        item.params[0].nameValuePairs = item.params.nameValuePairs || {};
        item.params[0].nameValuePairs.SessionID = this.SessionID;
        this.items.push(item);
    }
}

/**
 * @memberOf ELF.actions
 * @method transmitLogs
 * @description
 * Approach: Pull ALL logs from the database. Now, create a "batch" of tansmissions.
 * Each "batch" is a "bundle" of messages that can fit into a xmit.
 * Each "message" is an "item" the describes the log, and the first one in a batch described the batch (session)
 * @param {Object} params - Parameters
 * @param {string} [params.level] - The level of logs to grab else uses system defined level
 * @param {string} [params.device] - The device id.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'transmitLogs' }]
 */
export function transmitLogs (params) {
    let service = COOL.new('WebService'),
        col = new Logs(),
        subject,
        siteCode,
        fetch,
        deleteFromStorage,
        transmit,
        addLogsToBundlesInBatch;

    // Fetch all of the logs in from the database and return JSON.
    fetch = () => {
        return Q.all([col.fetch(), CurrentSubject.getSubject(), Sites.fetchFirstEntry()])
        .spread((logs, currentSubject, site) => {
            subject = currentSubject;

            if (site) {
                siteCode = site.get('siteCode');
            }

            return col.toJSON();
        });
    };

    addLogsToBundlesInBatch = (logs = []) => {
        // If there aren't any logs, cancel the action.
        if (!logs.length) {
            Q.reject();
            return;
        }

        let structLength = struct => JSON.stringify(struct).length;

        // eslint-disable-next-line consistent-return
        return logs.reduce((batch, log) => {
            // If the combined length of the current bundle and this log is over 8000/2...start a new bundle
            let bundle = _.last(batch);
            const overhead = 200;
            const logXmitItem = new LogXmitItem(log);

            if (!bundle || structLength(bundle) + structLength(logXmitItem) >= (8000 / 2) - overhead) {
                bundle = new LogXmitBundle({
                    device: params.device || null,
                    siteCode,
                    subject
                });

                batch.push(bundle);
            }

            bundle.addItem(logXmitItem);

            return batch;
        }, []);
    };

    transmit = (batch = []) => {
        // Submit each bundle in the batch to the server
        let extractIDs = (batchLog) => {
            // Produce an array of the 'id's (database IDs) for later deletion
            return _.chain(batchLog.items)
                .pluck('params')

                // because this is an array-of-1
                .map(x => x[0])

                // only select items that have a processId
                .filter(log => log.processId)
                .pluck('processId')
                .value();
        };

        return Q.allSettled(batch.map((bundle) => {
            return service.sendLogs(bundle.getItems())
            .then(() => extractIDs(bundle));
        }))
        .then((res) => {
            // Once all the xmits are complete, hunt-out the good ones and
            // then build an array of IDs to delete from the database
            return _.chain(res)
                .where({ state: 'fulfilled' })
                .map(item => item.value)
                .flatten()
                .value();
        });
    };

    deleteFromStorage = (ids) => {
        // Erase the logs from the database given an array of IDs
        if (ids && ids.length) {
            return Q.allSettled(ids.map(id => col.destroy(id)));
        }
        return ids;
    };

    // If anything goes wrong, let's catch it and console.log it because
    // we can't really use the logging system if something is broken....:<

    return fetch()
    .then(addLogsToBundlesInBatch)
    .then(transmit)
    .then(deleteFromStorage)
    .catch((err) => {
        // eslint-disable-next-line no-console
        console.log(`Logger.js exception: ${err}`);
    });
}
