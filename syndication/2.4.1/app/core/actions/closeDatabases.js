import ELF from 'core/ELF';
import { eCoaDB, LogDB } from 'core/dataAccess';

/**
 * @memberOf ELF.actions
 * @method closeDatabases
 * @description
 * Closes open database connections
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'closeDatabases' }]
 */
export function closeDatabases () {
    eCoaDB && eCoaDB.close();
    LogDB && LogDB.close();

    return Q();
}

ELF.action('closeDatabases', closeDatabases);
