import ELF from 'core/ELF';
import Spinner from 'core/Spinner';

/**
 * Removes the loading message and enables the view.
 * @memberOf ELF.actions
 * @method removeMessage
 * @returns {Q.Promise}
 * @example
 * resolve: [{ action: 'removeMessage' }];
 */
export function removeMessage () {
    return Q()
    .then(() => Spinner.hide());
}

ELF.action('removeMessage', removeMessage);

// @todo remove
LF.Actions.removeMessage = removeMessage;
