import ELF from '../ELF';

/**
 * @memberOf ELF.actions
 * @method defaultAction
 * @description
 * Fall back to the event's default action.
 * @param {Object} param - Not in use.
 * @param {Function} cb - Callback function invoked upon completion of the action.
 */
export function defaultAction (param, cb) {
    cb({}); // This is a complete NO-OP
}

ELF.action('defaultAction', defaultAction);

// @todo remove
LF.Actions.defaultAction = defaultAction;
