import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method  navigateTo
 * @description
 * Navigate to a defined view.
 * @param {string} destination - The destined view.
 * @param {Function} [callback] - Invoked to retain the action chain.
 * @example
 * // ELF action configuration to navigate to the ToolboxView
 * resolve: [{
 *     action: 'navigateTo',
 *     data: 'toolbox'
 * }]
 * @example
 * // Example of an imported and invoked action outside of ELF.
 *  import { navigateTo } from 'core/actions/navigateTo';
 *
 *  navigateTo('toolbox');
 */
export function navigateTo (destination, callback) {
    LF.router.navigate(destination, true);

    // TODO - Update to use promise.
    (callback || $.noop)();
}

ELF.action('navigateTo', navigateTo);

// @todo remove
LF.Actions.navigateTo = navigateTo;
