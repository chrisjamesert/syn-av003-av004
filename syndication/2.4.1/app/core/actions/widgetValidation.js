import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method widgetValidation
 * @description
 * Execute the validation function attached to a widget if present.
 * @param {Object} param - contains the validation object as well as the answer
 * @param {Function} callback Invoked to retain the action chain.
 * @example
 * resolve: [{ action: 'widgetValidation' }],
 */
export function widgetValidation (param, callback) {
    let validationObj = this.model.get('validation'),
        answer = this.answer;

    if (LF.Widget.ValidationFunctions[validationObj.validationFunc]) {
        LF.Widget.ValidationFunctions[validationObj.validationFunc].call(this, answer, validationObj.params, callback);
    } else {
        callback(true);
    }
}

ELF.action('widgetValidation', widgetValidation);

LF.Actions.widgetValidation = widgetValidation;
