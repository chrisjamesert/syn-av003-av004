import ELF from '../ELF';

/**
 * @memberOf ELF.actions
 * @method silenceAlarm
 * @description
 * Silence Alarm action
 * @param {Object} param The destined view.
 * @param {Function} callback Invoked to retain the action chain.
 */
export function silenceAlarm (param, callback) {
    if (callback) {
        callback({ preventDefault: true });
    }
}

ELF.action('silenceAlarm', silenceAlarm);

// @todo remove
LF.Actions.silenceAlarm = silenceAlarm;
