import ELF from 'core/ELF';
import Logger from 'core/Logger';

let logger = new Logger('handleLastDiariesResult');

/**
 * Filter the last recieved diary.
 * @private
 * @param {Object} lastDiary - The last diary to filter.
 * @returns {Object} the filtered last diary.
 */
export function filterReceivedLastDiary (lastDiary) {
    let incDiary = {
        krpt: lastDiary.krpt,
        questionnaire_id: lastDiary.questionnaire_id,
        lastStartedDate: lastDiary.lastStartedDate,
        lastCompletedDate: lastDiary.lastCompletedDate,
        deleted: lastDiary.deleted
    };

    if (lastDiary.updated !== undefined) {
        incDiary.updated = lastDiary.updated;
    }

    if (lastDiary.role !== undefined) {
        incDiary.role = lastDiary.role;
    }

    return incDiary;
}

/**
 * @memberOf ELF.actions
 * @method handleLastDiariesResult
 * @description
 * Handle results from a last diary sync.
 * @param {Object} input - Input from the Last Diary sync.
 * @param {Function} resume - Invoked upon completion of the action.
 * @example
 * trigger: 'HISTORICALDATASYNC:Received/LastDiaries',
 * resolve: [{ action: 'handleLastDiariesResult' }]
 */
export function handleLastDiariesResult (input, resume) {
    let length,
        step = (counter) => {
            let incDiary = filterReceivedLastDiary(input.res[counter]),
                lastDiary = _.first(input.collection.match({
                    krpt: incDiary.krpt,
                    questionnaire_id: incDiary.questionnaire_id
                })),
                incDiaryStartDate = new Date(incDiary.lastStartedDate),
                incDiaryCompletedDate = new Date(incDiary.lastCompletedDate),
                deletedFlag = incDiary.deleted,
                lastDiaryStartDate,
                lastDiaryCompletedDate,
                lastDiaryUpdated;

            logger.debug(`Server sync record: ${JSON.stringify(incDiary)}`);
            logger.debug(`Last diary record: ${JSON.stringify(lastDiary)}`);

            if (!deletedFlag || isNaN(parseInt(deletedFlag, 10))) {
                logger.error(`Incorrect flag type: ${deletedFlag} value of deleted flag is not a number.`);

                if (counter + 1 < length) {
                    step(counter + 1);
                } else {
                    resume(true);
                }

                return;
            }

            if (lastDiary) {
                // Diary already exists in LastDiaries table
                lastDiaryStartDate = new Date(lastDiary.get('lastStartedDate'));
                lastDiaryCompletedDate = new Date(lastDiary.get('lastCompletedDate'));

                if (incDiary.deleted === '0') {
                    // Incoming diary has not been deleted
                    if (incDiaryStartDate.getTime() > lastDiaryStartDate.getTime() || incDiaryCompletedDate.getTime() === lastDiaryCompletedDate.getTime()) {
                        // Incoming diary is more recent or is the same diary and may have a changed timestamp; update it in the table
                        incDiary.lastCompletedDate = incDiaryCompletedDate.ISOStamp();
                        delete incDiary.deleted;

                        lastDiaryUpdated = lastDiary.get('updated');
                        if (!!lastDiaryUpdated && !!incDiary.updated && incDiary.updated < lastDiaryUpdated) {
                            delete incDiary.role;
                            delete incDiary.updated;
                        }

                        lastDiary.save(incDiary, {
                            onSuccess: () => {
                                logger.info('Incoming diary is more recent or may have a changed timestamp:Saved incoming diary');
                                if (counter + 1 < length) {
                                    step(counter + 1);
                                } else {
                                    resume(true);
                                }
                            },
                            onError: (err) => {
                                logger.error(`Saving last diary Failed on record ${JSON.stringify(incDiary)} with error ${err.toString()}`);
                                if (counter + 1 < length) {
                                    step(counter + 1);
                                } else {
                                    resume(true);
                                }
                            }
                        });
                    } else {
                        // Incoming diary is an older instance; ignore it.
                        if (counter + 1 < length) {
                            step(counter + 1);
                        } else {
                            resume(true);
                        }
                    }
                } else {
                    // Incoming diary has been deleted
                    if (incDiaryCompletedDate.getTime() === lastDiaryCompletedDate.getTime()) {
                        // The incoming diary is the same diary currently stored in the LastDiaries table; delete it.
                        lastDiary.destroy({
                            onSuccess: () => {
                                logger.info('Incoming diary has been deleted in the server:Deleted last diary');
                                if (counter + 1 < length) {
                                    step(counter + 1);
                                } else {
                                    resume(true);
                                }
                            },
                            onError: (err) => {
                                logger.error(`Deleting last diary Failed on client record ${JSON.stringify(lastDiary)} with error ${err.toString()}`);
                                if (counter + 1 < length) {
                                    step(counter + 1);
                                } else {
                                    resume(true);
                                }
                            }
                        });
                    } else {
                        if (counter + 1 < length) {
                            step(counter + 1);
                        } else {
                            resume(true);
                        }
                    }
                }
            } else {
                // Diary does not already exist in the LastDiaries table
                if (incDiary.deleted === '0') {
                    // Incoming Diary has not been deleted, so it needs to be added to the LastDiaries table
                    delete incDiary.deleted;

                    // eslint-disable-next-line new-cap
                    lastDiary = new input.collection.model();

                    incDiary.lastCompletedDate = incDiaryCompletedDate.ISOStamp();

                    lastDiary.save(incDiary, {
                        onSuccess: () => {
                            logger.info('Incoming diary does not already exist in the table:Saved last diary');
                            if (counter + 1 < length) {
                                step(counter + 1);
                            } else {
                                resume(true);
                            }
                        },
                        onError: (err) => {
                            logger.error(`Saving last diary Failed on ${JSON.stringify(incDiary)} with error ${err.toString()}`);
                            if (counter + 1 < length) {
                                step(counter + 1);
                            } else {
                                resume(true);
                            }
                        }
                    });
                } else {
                    // Incoming Diary has been deleted; ignore it.
                    if (counter + 1 < length) {
                        step(counter + 1);
                    } else {
                        resume(true);
                    }
                }
            }
        };

    if (_.isArray(input.res)) {
        length = input.res.length;
        step(0);
    } else {
        resume(false);
    }
}

ELF.action('handleLastDiariesResult', handleLastDiariesResult);
