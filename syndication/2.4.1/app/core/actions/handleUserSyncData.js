import ELF from 'core/ELF';
import Logger from 'core/Logger';

let logger = new Logger('handleUserSyncData');

/**
 * @memberOf ELF.actions
 * @method handleUserSyncData
 * @description
 * Handle user sync data from a historical sync.
 * @param {Object} params - Data provided by the triggered event.
 * @param {Array<Object>} params.users - A collection of user data from the service.
 * @param {Users} params.collection - A collection of existing users.
 * @returns {Q.Promise<(number|ELF~ActionResult)>}
 * @example
 * ELF.rules.add({
 *      id: 'handleUserSyncData',
 *      // Triggered when the device receives User data from the web service.
 *      trigger: 'HISTORICALDATASYNC:Received/Users',
 *      resolve: [{ action: 'handleUserSyncData' }]
 * });
 */
export default function handleUserSyncData ({ res: users, collection }) {
    return Q.all(_.map(users, (userData) => {
        let userModel = collection.findWhere({
            // Matching by username creates a superfluous user record on cases of a duplicate user
            // existing on the server.
            // username: userData.username
            userId: userData.userId
            // eslint-disable-next-line new-cap
        }) || new collection.model();

        return userModel.save(userData)
        .then(() => collection.add(userModel));
    }))
    .then(() => collection.updateUserStatus())
    .catch((err) => {
        logger.error('Unexpected error', err);
        return { stopActions: true };
    });
}

ELF.action('handleUserSyncData', handleUserSyncData);
