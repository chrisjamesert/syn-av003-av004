import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method refresh
 * @description
 * Complete reload of the application.
 * Not to be confused with PageView#reload, which reloads and renders the current view.
 * @returns {Q.Promise}
 * @example
 * resolve: [{ action: 'refresh' }]
 * @example
 * import { refresh } from 'core/actions/refresh';
 *
 * refresh();
 */
export function refresh () {
    window.location.reload();

    return Q();
}

ELF.action('refresh', refresh);
