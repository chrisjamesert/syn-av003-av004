import Logger from 'core/Logger';

let logger = new Logger('recoverOrphanDiaries');

/**
 * @memberOf ELF.actions
 * @method recoverOrphanDiaries
 * @description
 * Creates transmission records for orphan Dashboard and Answer records.
 * @param {null|undefined} notUsed - Not used in this action.
 * @param {Function} resume - Invoked to retain the action chain.
 * @example
 * resolve: [{ action: 'recoverOrphanDiaries' }]
 */
export function recoverOrphanDiaries (notUsed, resume) {
    let dashboards = new LF.Collection.Dashboards(),
        transmissions = new LF.Collection.Transmissions(),
        createTransmission = function (params, callback) {
            let model = new LF.Model.Transmission();

            try {
                // TODO - Deprecated onSuccess/onError code. Use returned promise.
                model.save({
                    method: 'transmitQuestionnaire',
                    params: JSON.stringify(params),
                    created: new Date().getTime()
                }, {
                    onSuccess: () => {
                        logger.operational(`Recovered transmission for orphan dashboard. Dashboard ID: ${params.dashboardId}, Dashboard sigID: ${params.sigId}`);
                        callback();
                    },
                    onError: (err) => {
                        logger.error(`Saving transmission failed for Dashboard ID: ${params.dashboardId}, Dashboard sigID: ${params.sigId} with error: ${err.toString()}`);
                        callback();
                    }
                });
            } catch (err) {
                logger.error(err.toString());
                callback();
            }
        },
        checkDashboard = (index) => {
            let params,
                dashboard = dashboards.at(index);

            if (dashboard == null) {
                resume(true);
                return;
            }

            params = {
                dashboardId: dashboard.get('id'),
                sigId: dashboard.get('sig_id')
            };

            if (transmissions.findWhere({ params: JSON.stringify(params) }) == null) {
                createTransmission(params, () => {
                    checkDashboard(index + 1);
                });
            } else {
                checkDashboard(index + 1);
            }
        };

    // TODO - Deprecated onSuccess/onError code. Use returned promise.
    dashboards.fetch({
        onSuccess: () => {
            transmissions.fetch({
                onSuccess: () => {
                    checkDashboard(0);
                },
                onError: (err) => {
                    logger.error(`Fetching transmissions failed with error: ${err.toString()}`);
                    resume(true);
                }
            });
        },
        onError: (err) => {
            logger.error(`Fetching dashboards failed with error: ${err.toString()}`);
            resume(true);
        }
    });
}

ELF.action('recoverOrphanDiaries', recoverOrphanDiaries);

// @todo remove
LF.Actions.recoverOrphanDiaries = recoverOrphanDiaries;
