import COOL from 'core/COOL';
import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';
import Transmit from 'core/transmit';
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';

const logger = new Logger('timeSlipCheck');

const millisecondsPerMinute = 60000;
const acceptableWindow = 15 * millisecondsPerMinute;
const hourToMs = 3600000;

/**
 * @memberOf ELF.actions
 * @method timeSlipCheck
 * @description
 * Check for time slip on the device.
 * @param {Object} params - ELF action configuration.
 * @param {boolean} params.isBlind - true if we are calling this from the "blind confirmation" that appears when doing the check offline.
 * @return {Q.Promise<Object>} Q Promise
 * resolve: [{
 *     action: 'timeSlipCheck',
 *     data: { isBlind: true }
 * }]
 */
export function timeSlipCheck ({ isBlind = false }) {
    let prefLangLoc = localStorage.getItem('preferredLanguageLocale');

    const findAnswer = (append) => {
        const ans = this.answers.findWhere({ SW_Alias: `Time_Confirmation.0.${append}` });

        if (!ans) {
            throw new Error('Time Confirmation Answers not provided.');
        }

        return ans && ans.get('response');
    };

    const dialog = () => MessageRepo.display(MessageRepo.Dialog.TIMESLIP_ERROR);

    const formatTzOffset = (offset) => {
        const offsetSign = offset < 0 ? '-' : '+';
        offset = Math.abs(offset);
        const hourOffset = Math.floor(offset / hourToMs);
        const minuteOffset = `00${(offset - hourOffset * hourToMs) * 60 / hourToMs}`.slice(-2);
        return `${offsetSign}${hourOffset}:${minuteOffset}`;
    };

    const compareDates = () => {
        return Q()
        .then(() => {
            if (this.answers) {
                const dateTimeAnswer = findAnswer('DATE_TIME'),
                    res = JSON.parse(findAnswer('TIME_ZONE')).timezone,
                    tz = JSON.parse(res),
                    tzForLog = formatTzOffset(tz.offset),
                    currentDate = new Date();

                let time = new Date(dateTimeAnswer);

                const timeUTCOffset = time.getTimezoneOffset(),
                    nowUTCOffset = currentDate.getTimezoneOffset();

                // DE18295 fix
                time = new Date(time.getTime() - ((timeUTCOffset - nowUTCOffset) * 60 * 1000));

                // DE17749: timezone offsets should be expressed in hours and mionutes, not decimal hours
                logger.operational(`Input time: ${dateTimeAnswer}`);
                logger.operational(`Input timezone offset: ${tzForLog}`);

                // users input a time and a timezone, add their offset to the input time.
                // (INPUT -> UTC) + (Device's current offset) - (input offset) = input time UTC.
                return new Date(time).getTime() + (new Date().getOffset() * hourToMs) - parseInt(tz.offset, 10);
            }
            LF.security.pauseSessionTimeOut();

            return COOL.getService('Transmit', Transmit).getServerTime()
            .then((time) => {
                logger.operational(`Time from the server: ${time}`);

                return time.getTime();
            })
            .catch(() => {
                logger.operational('Retrieving server time failed, proceding with blind confirmation');

                // DE17567, DE17610 - Server call fails even though device is online do perform blind confirmation
                if (prefLangLoc) {
                    LF.content.setContextLanguage(prefLangLoc);
                }

                LF.Actions.navigateTo('time-confirmation');
                return Q.reject({ isHandled: true, preventActions: true, serverFail: true });
            });
        })
        .then((time) => {
            const deviceTime = new Date();
            let devTimeUTC = deviceTime.getTime();

            logger.operational(`Device time: ${deviceTime}`);

            // DE17498 fix -> don't take seconds and milliseconds in the calculation.
            time -= time % millisecondsPerMinute;
            devTimeUTC -= devTimeUTC % millisecondsPerMinute;

            return time >= (devTimeUTC - acceptableWindow) && time <= (devTimeUTC + acceptableWindow);
        })
        .catch((err) => {
            if (err.isHandled) {
                return Q.reject(err);
            }
            logger.error('Error comparing dates', err);
            return Q.reject({ isHandled: true, preventActions: true });
        });
    };

    const check = (firstRun = true) => {
        return compareDates()
        .then((withinWindow) => {
            if (!withinWindow) {
                return Q()
                .then(() => {
                    // Performing a second check while offline won't fix anything.
                    if (firstRun && !isBlind) {
                        logger.operational('Time is incorrect, performing another check to attempt to fix.');

                        // The first attempt may have fixed the time automatically. Check time again
                        return check(false);
                    }
                    logger.operational('Time is incorrect, it must be fixed before continuing.');

                    if (prefLangLoc) {
                        LF.content.setContextLanguage(prefLangLoc);
                    }

                    Spinner.hide()
                    .then(() => dialog())
                    .then(() => ELF.trigger('TIMESLIP:Retry'))
                    .tap((res) => {
                        // DE19033 if serverFail is true do not reload
                        if (!res.serverFail && this.reload) {
                            this.reload();
                        }
                    })
                    .done();

                    // prevent navigation
                    return { preventDefault: true, stopActions: true };
                });
            }
            LF.security.restartSessionTimeOut();
            logger.operational('Time is within 15 minutes of the device time. Continuing application.');
            return Spinner.hide();
        });
    };

    return Q()
    // eslint-disable-next-line consistent-return
    .then(() => {
        const serviceBase = lStorage.getItem('serviceBase');

        if (!serviceBase || serviceBase === 'null') {
            logger.operational('No server info is available. There will be no check for time slip.');
            return Q.reject({ isHandled: true });
        }
    })
    .then(() => Spinner.show())

    // DE17532 - This is a temporary fix for the problem at hand.  Spinner.show() returns a promise,
    // but it isn't resolved upon the display of the UI component as it should.  I'll log another defect
    // to resolve this issue.
    .delay(300)
    .then(() => {
        logger.operational('Checking for time slip...');
        return check();
    })
    // eslint-disable-next-line consistent-return
    .catch((e) => {
        if (!(e && e.isHandled)) {
            return Q.reject(e);
        }

        if (e.preventActions) {
            // We handle navigation inside this action, so prevent the action in the core rules from navigating.
            return { preventDefault: true, stopActions: true, serverFail: e.serverFail || false };
        }
    });
}

ELF.action('timeSlipCheck', timeSlipCheck);

// @todo remove
LF.Actions.timeSlipCheck = timeSlipCheck;
