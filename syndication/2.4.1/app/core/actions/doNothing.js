import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method doNothing
 * @description
 * Do nothing.
 * @returns {Q.Promise<void>}
 */
export function doNothing () {
    return Q();
}

ELF.action('doNothing', doNothing);
