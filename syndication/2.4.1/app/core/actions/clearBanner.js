import { Banner } from 'core/Notify';
import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method clearBanner
 * @description
 * Clears any displayed banners.
 * @returns {Q.Promise<void>}
 * @example
 * // Example of clearBanner configured as an ELF action.s
 * resolve: [{
 *    action: 'clearBanner'
 * }]
 * @example
 * // Example of importing and invoking clearBanner.
 * import clearBanner from 'core/actions/clearBanner';
 *
 * clearBanner();
 */
export default function clearBanner () {
    Banner.closeAll();

    return Q();
}

ELF.action('clearBanner', clearBanner);
