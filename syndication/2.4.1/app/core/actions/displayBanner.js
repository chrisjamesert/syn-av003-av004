import { MessageRepo } from 'core/Notify';

/**
 * @memberOf ELF.actions
 * @method displayBanner
 * @description
 * Displays a banner by key.
 * @param {string} key - The key of the banner to display.
 * @returns {Q.Promise<void>}
 * @example
 *  resolve :[{
 *      action: 'displayBanner',
 *      data: 'USER_EDITED'
 *  }]
 * @example
 * import displayBanner from 'core/actions/displayBanner';
 *
 * displayBanner().done();
 */
export default function displayBanner (key) {
    MessageRepo.display(MessageRepo.Banner[key]);

    return Q();
}

ELF.action('displayBanner', displayBanner);
