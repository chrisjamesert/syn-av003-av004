import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method preventDefault
 * @description
 * Prevent the default action from executing.
 * <pre><code>
 * { preventDefault: true }
 * </code></pre>
 * @returns {Q.Promise<ELF~ActionResult>} flag to ELF.trigger'er
 * @example
 * resolve: [{
 *     action: 'preventDefault'
 * }]
 */
export function preventDefault () {
    // TODO - Update to use promise.
    return { preventDefault: true };
}

ELF.action('preventDefault', preventDefault);
