import CurrentContext from 'core/CurrentContext';

/**
 * @memberOf ELF.actions
 * @method updateCurrentContext
 * @description
 * Updates the current context data.
 * @returns {Q.Promise}
 * @example
 * resolve: [{ action: 'updateCurrentContext' }]
 */
export function updateCurrentContext () {
    return Q.all([CurrentContext().get('subjects').fetch(), CurrentContext().get('users').fetch()]);
}

ELF.action('updateCurrentContext', updateCurrentContext);
