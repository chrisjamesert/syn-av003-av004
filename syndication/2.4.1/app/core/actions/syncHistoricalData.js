import Transmission from 'core/models/Transmission';

// TODO - Transmissions is defined, but the code uses LF.Collection.Transmissions
// eslint-disable-next-line no-unused-vars
import Transmissions from 'core/collections/Transmissions';
import ELF from 'core/ELF';

/**
 * Syncs historical data with the Web Service using transmission queue.
 * @memberOf ELF.actions
 * @method syncHistoricalData
 * @param {Object} params - Includes collection and web service method names.
 * @param {string} params.collection - The name of the collection to sync.
 * @param {string} params.webServiceFunction - The name of the web service function to invoke the sync.
 * @param {boolean} params.activation - Assume activation.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{
 *     action: 'syncHistoricalData',
 *     data: {
 *         collection: 'LastDiaries',
 *         webServiceFunction: 'syncLastDiary',
 *         activation: false
 *     }
 * }]
 */
export function syncHistoricalData (params) {
    let method = 'historicalDataSync',
        model = new Transmission(),
        subjectKrpt = params.subject && params.subject.get('krpt');

    if (params.collection === 'LastDiaries' && subjectKrpt) {
        params.krpt = subjectKrpt;
    }

    return model.save({
        method,
        params: JSON.stringify(params),
        created: new Date().getTime()
    })

    // TODO: should we change LF.Collection.Transmissions to transmissions?
    // May be it is there to prevent a circular dependency issue.
    .then(() => LF.Collection.Transmissions.fetchCollection())
    .then((transmissions) => {
        let syncTransmission = transmissions.findWhere({ method });
        return transmissions.execute(transmissions.indexOf(transmissions.get(syncTransmission)));
    });
}

ELF.action('syncHistoricalData', syncHistoricalData);
