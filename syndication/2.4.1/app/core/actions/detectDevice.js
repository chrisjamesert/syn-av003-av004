import { Fullscreen } from 'core/Notify';
import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method detectDevice
 * @description
 * Determines if the device is supported.
 * @param {Object[]} devices An list of supported devices.
 * @param {Function} callback The callback function invoked upon completion of the action.
 */
export function detectDevice (devices, callback) {
    let supported = false;

    if (devices) {
        _(devices).each((device) => {
            if (navigator.userAgent.indexOf(device.name) !== -1) {
                if (device.match) {
                    _(device.match).each((params) => {
                        let matches = 0,
                            length = params.length;
                        _(params).each((param) => {
                            if (navigator.userAgent.indexOf(param) !== -1) {
                                matches += 1;
                            }
                        });

                        if (matches === length) {
                            supported = true;
                        }
                    });
                } else {
                    supported = true;
                }
            }
        });

        if (!supported) {
            Fullscreen.show('DEVICE_NOT_SUPPORTED');
        }

        callback(supported);
    } else {
        callback(true);
    }
}

ELF.action('detectDevice', detectDevice);

// @todo remove
LF.Actions.detectDevice = detectDevice;
