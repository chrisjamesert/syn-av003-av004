import { Banner, MessageRepo } from 'core/Notify';
import * as MessageHelpers from './MessageHelpers';

/**
 * Add core banners to the MessageRepo.
 */
export default function setupCoreBanners () {
    MessageRepo.add(
        MessageHelpers.createErrorBanner('INVALID_ENTRY'),
        MessageHelpers.createErrorBanner('PASSWORD_MISMATCH'),
        MessageHelpers.createErrorBanner('USER_NAME_INVALID_CHARACTERS'),
        MessageHelpers.createErrorBanner('REQUIRED_ANSWER'),
        MessageHelpers.createErrorBanner('PASSWORD_INVALID'),

        // Core/UnlockCodeView
        MessageHelpers.createErrorBanner('INCORRECT_UNLOCK_CODE'),
        {
            type: 'Banner',
            key: 'NEW_USER_CREATED',
            message: ({ afterShow = $.noop }) => Banner.success('NEW_USER_CREATED', { afterShow })
        }, {
            type: 'Banner',
            key: 'CONNECTION_REQUIRED',
            message ({ delay = 900, afterShow = $.noop }) {
                return LF.getStrings('CONNECTION_REQUIRED')
                .delay(delay)
                .then((text) => {
                    Banner.show({
                        text,
                        type: 'error',
                        callback: {
                            afterShow,
                            onClose () {
                                $.noty.clearQueue();
                            }
                        }
                    });
                });
            }
        }
    );

    const coreInputErrorKeys = [
        'INCORRECT_ANSWER'
    ];

    _.each(coreInputErrorKeys, MessageHelpers.createInputErrorBanner);
}
