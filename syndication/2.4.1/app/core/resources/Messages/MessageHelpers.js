import NotifyView from 'core/views/NotifyView';
import ConfirmView from 'core/views/ConfirmView';
import { Banner } from 'core/Notify';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

const logger = new Logger('Dialog');


/**
 * Create an error banner.
 * @param {string} key the key used to reference the stored Banner.
 * @param {string} [stringKey=key] Optional string key, defaults to "key"
 * @returns {object} An object representing the Message to be placed in the MessageRepo.
 */
export const createErrorBanner = (key, stringKey = key) => {
    return {
        key,
        type: 'Banner',
        message: ({ delay = 500, afterShow = $.noop }) => {
            return LF.getStrings(stringKey)
            .delay(delay)
            .then((text) => {
                Banner.show({ text: ` ${text}`, afterShow, type: 'error' });
            });
        }
    };
};

/**
 * Create an error banner that prepends 'INPUT_ERROR' to the key.
 * @param {string} key - The string key.
 */
export const createInputErrorBanner = (key) => {
    const id = 'INPUT_ERROR';
    MessageRepo.add(createErrorBanner(`${id}_${key}`, key));
};

/**
 * The function created after the first notifyDialogCreator call.
 * @typedef {function} RuntimeNotifyCreator
 * @param {[Object]} runtimeParams - params for runtime use.
 * @returns {Q.Promise}
 */

/**
 * Create a notification dialog.
 * @param  {[Object]} params - parameters
 * @returns {RuntimeNotifyCreator}
 */
export const notifyDialogCreator = (params = {}) => (runtimeParams = {}) => {
    _.extend(params, runtimeParams);

    // Was actions/notify.js

    let modal = new NotifyView(),
        stringsToFetch = {
            header: params.header || 'PLEASE_CONFIRM',
            ok: params.ok || 'OK'
        };

    if (!params.coreString) {
        stringsToFetch.message = params.message;
    }

    return LF.getStrings(stringsToFetch)
    .then((strings) => {
        // @TODO SCREENSHOT Maybe we should use dynamic text instead for these built messages, that way we can screenshot them better
        let showMessage = () => {
            let httpErrorMsg,
                errorObj = params.httpRespCode || '',
                message = strings.message;

            if (_.isArray(params.message)) {
                message = strings.message.join('<br /><br />');
            }

            if (errorObj.httpCode && errorObj.errorCode) {
                httpErrorMsg = `HTTP Error: ${errorObj.httpCode}-${errorObj.errorCode}`;
            } else if (errorObj.httpCode) {
                httpErrorMsg = `HTTP Error: ${errorObj.httpCode}`;
            }

            message = `${params.coreString || message} <br/> ${httpErrorMsg || ''}`;

            return message;
        };

        return modal.show({
            header: strings.header,
            body: showMessage(),
            ok: strings.ok,
            type: params.type || ''
        });
    });
};

/**
 * [description]
 * @param  {[type]} params =             {} [description]
 * @return {[type]}        [description]
 */
export const confirmDialogCreator = (params = {}) => (runtimeParams = {}) => {
    _.extend(params, runtimeParams);

    // Was actions/confirm.js

    const stringsToFetch = {
        header: params.header || 'PLEASE_CONFIRM',
        message: params.message,
        ok: params.ok || 'OK',
        cancel: params.cancel || 'CANCEL'
    };

    const confirmView = new ConfirmView();

    return LF.getStrings(stringsToFetch)
    .then((strings) => {
        return confirmView.show({
            header: strings.header,
            body: strings.message,
            ok: strings.ok,
            cancel: strings.cancel,
            styleClass: params.styleClass || '',
            type: params.type || ''
        });
    })
    .tap(() => {
        if (params.message === 'BACK_OUT_CONFIRM') {
            logger.operational('Diary abandoned - User backed out of diary.');
        }
    });
};
