import FileHandler from 'core/classes/FileHandler';
import * as CoreUtilities from 'core/utilities';

/**
 * Shorthand for extracting a zip file, then retrieving its
 * @param {FileHandler} fh file handler object, constructed with root directory.
 * @param {string} langCode language code (used as a base path)
 * @param {string} pagePath path to the index page
 * @returns {Q.Promise<string>} promise resolving with string value for URL (blank if none).
 */
function getExtractedURL (fh, langCode, pagePath) {
    let zipPath = `${langCode}.zip`;
    return fh.getIndexURL(langCode, pagePath)

    // Do nothing if the index retrieval worked.  If not, attempt to extract and try again.
    .then(null, () => {
        return fh.extractZipFile(zipPath)
        .then(() => {
            return fh.getIndexURL(langCode, pagePath);
        });
    });
}

/**
 * Get the URL for the rater training file.
 * This method looks up rater training by language (falling back to language without locale and default language).
 * If no files are locally cached, it falls back to the remote link to the page.
 * @param {string} [pageName='default'] key for the pages object in the rater training configuration.
 * @returns {Q.Promise<string>} promise resolving with the URL of the page to use.
 */
LF.Widget.ParamFunctions.getRaterTrainingURL = function (pageName = 'default') {
    let langCode = `${LF.Preferred.language}-${LF.Preferred.locale}`.replace(/-$/, ''),
        config = CoreUtilities.getRaterTrainingConfig(),
        defLangCode = `${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`.replace(/-$/, ''),
        pagePath = config.pages[pageName],
        fallBackURL = `${config.fallBackBaseURL}/${langCode}/${pagePath}`;

    if (!LF.Wrapper.isWrapped) {
        return new Q.Promise((resolve) => {
            resolve(fallBackURL);
        });
    }
    let fh = new FileHandler(config.rootDirectory),
        fallThrough = () => null,
        returnDefault = () => fallBackURL;

    return Q()
    .then(() => {
        return getExtractedURL(fh, langCode, pagePath);
    }, fallThrough)
    .then((url) => {
        return url || getExtractedURL(fh, langCode.split('-')[0], pagePath);
    }, fallThrough)
    .then((url) => {
        return url || getExtractedURL(fh, defLangCode, pagePath);
    }, fallThrough)
    .then((url) => {
        return url || getExtractedURL(fh, defLangCode.split('-')[0], pagePath);
    }, fallThrough)
    .then((url) => {
        return url || fallBackURL;
    }, returnDefault);
};

export default LF.Widget.ParamFunctions.getRaterTrainingURL;
