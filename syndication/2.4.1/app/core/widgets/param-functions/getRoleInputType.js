/**
 * Gets the preferred type of input for a current role.
 * @param {string} [roleId=subject] The role to get the type for, defaults to subject.
 * @example this.getRoleInputType('subject');
 * @returns {string} Input type string
 */
export function getRoleInputType (roleId = 'subject') {
    if (LF.Wrapper.isBrowser()) {
        return 'text';
    }
    let roleSettings = LF.StudyDesign.roles.get(roleId),
        passwordType = roleSettings ? roleSettings.get('passwordInputType') : 'text';
    return passwordType === 'number' ? 'number' : 'text';
}
export default getRoleInputType;
