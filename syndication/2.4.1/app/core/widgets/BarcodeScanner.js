/** #depends Base.js
 * @file Defines the barcode (or QR Code) scanner widget
 * @author <a href="mailto:jhendy@phtcorp.com">James Hendy</a>
 * @version 1.6
 */

import WidgetBase from './WidgetBase';
import { isLogPad } from 'core/utilities';

class BarcodeScanner extends WidgetBase {
    /**
     * Constructs the barcode scanner widget.
     * @param {Object} options The options used to render the widget.
     * @param {Object} options.model The model that contains the actual widget properties (represented by the "widget" property in the question JSON)
     * @param {string} options.model.id {dev-only} The widget's id
     * @param {string} [options.model.regexToMatch] The regular expression for a valid value.
     * @param {boolean} [options.model.hideManualInput=false] Indicates whether the textbox for manually entering the QR code should be displayed.
     * @param {Object} [options.model.templates] {dev-only} Contains any templates that should be used to render the widget.
     * @param {string} [options.model.templates.container] {dev-only} The template to be used for the widget's container element.
     *
     */
    constructor (options) {
        /**
         * The template of the container
         * @type String
         * @default 'DEFAULT:BarcodeScanner'
         */
        this.container = 'DEFAULT:BarcodeScanner';

        /**
         * List of the widgets events
         * @readonly
         * @enum {Event}
         */
        this.events = {
            /** scanBtn click event */
            'click .scanBtn': 'scan',

            /** scannerData text field input event */
            'input .scannerData': 'manualInputChange'

            /** intercept form submit */
            // DE15951 - Disabled as part of defect.
            // 'submit'                : 'goToNextQuestion'
        };

        this.options = options;

        this.matchesRegex = false;

        super(options);
    }

    /**
     * Triggers the next button after submitting from a virtual keyboard
     * @param {Event} e The event args
     */
    goToNextQuestion (e) {
        e.preventDefault();
        this.parent.parent.nextHandler(e);
    }

    /**
     * Saves an answer after every key press in the text field.
     */
    manualInputChange () {
        let inputText = this.$('.scannerData').val(),
            answered = inputText !== undefined;

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        if (inputText !== '') {
            // if user manually update the string, but the string is not empty
            this.respondHelper(this.answer, inputText, answered);
        } else {
            // if user manually delete the entire string, remove the added answer
            this.removeAnswer(this.answer);
        }
    }

    /**
     * runs validation on this widget checking against the passed in regex
     * @returns {boolean}
     */
    validate () {
        return super.validate() && (!this.mandatory || this.matchesRegex);
    }

    /**
     * Overridden this to also check for a passed in regex to match against the returned barcode
     * @param {Object} model The answer record to modify.
     * @param {String} [response=null] The response to be stored.
     * @param {Boolean} [completed=true] True if the response should set the widget to a completed state.
     * @example LF.Widget.Base.prototype.respond.call(this, this.answer, "25", true);
     * @protected
     */
    respondHelper (model, response, completed) {
        super.respondHelper(model, response, completed);
        this.doesScannedMatchRegex(response);
    }

    /**
     * checks to see if the scanned barcode matches the regex passed in
     * @param {String} scannedVal the passed in value from the barcode scanner
     */
    doesScannedMatchRegex (scannedVal) {
        let regexToMatch = this.model.get('regexToMatch');

        if (regexToMatch instanceof RegExp) {
            this.matchesRegex = regexToMatch.test(scannedVal);
        } else {
            this.matchesRegex = true;
        }
    }

    /**
     * Launches the cordova plugin to scan a barcode and to save the result of the scan
     * @returns {Q.Promise}
     */
    scan () {
        let config = {
            orientation: isLogPad() ? 'portrait' : 'landscape'  // Android only
        };

        return new Q.Promise((resolve) => {
            LF.Wrapper.exec({
                execWhenWrapped: cordova.plugins.barcodeScanner.scan((result) => {
                    if (result.text) {
                        this.$('.scannerData').val(result.text);

                        if (!this.answers.size()) {
                            this.answer = this.addAnswer();
                        }

                        this.respondHelper(this.answer, result.text, true);
                    }
                    resolve();
                }, $.noop, config)
            });
        });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        return new Q.Promise((resolve) => {
            let templates = this.model.get('templates') || {},
                container = templates.container || this.container,
                widgetView = LF.templates.display(container);

            this.$el.empty();
            this.$el.append(widgetView);

            if (!LF.Wrapper.isWrapped) {
                this.$('.scanBtn').addClass('hidden');
            }

            if (!!this.answer && this.completed) {
                this.$('.scannerData').val(this.answers.at(0).get('response'));
            }

            this.$el.appendTo(this.parent.$el);

            if (this.model.get('hideManualInput') === true) {
                $('.scannerData').hide();
                $('.scannerData').prop('readonly', true);
            }

            this.delegateEvents();

            this.$el.trigger('create');

            resolve();
        });
    }
}

window.LF.Widget.BarcodeScanner = BarcodeScanner;
export default BarcodeScanner;
