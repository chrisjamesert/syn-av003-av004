import BaseSpinner from './BaseSpinner';
import ValueSpinnerInput from './input/ValueSpinnerInput';
import { toStrictRTL } from 'core/utilities/localizationUtils';
import Logger from 'core/Logger';

const logger = new Logger('IntervalSpinnerBase');
/* globals _ */

let { Model } = Backbone;

const DEFAULT_SPINNER_TEMPLATE = 'DEFAULT:NumberSpinnerControl',
    DEFAULT_SPINNER_ITEM_TEMPLATE = 'DEFAULT:NumberItemTemplate',
    DEFAULT_CLASS_NAME = 'DateSpinner',
    DEFAULT_STORAGE_LOCALE = 'en-US',

    DEFAULT_NUMBER_OF_SPINNER_ITEMS = 2.5,

    DEFAULT_DATE_FORMAT = 'LL',
    DEFAULT_TIME_FORMAT = 'LT',
    DEFAULT_DATE_TIME_FORMAT = 'LLL',

    DEFAULT_DATE_YEAR_RANGE = 30,

    // Date where each date part has a unique value.
    //  This is used to generate a test date that can be displayed, reordered for RTL,
    //  and then re-populated with the date pattern parts (e.g. DD, MMM, etc.)
    DATE_WITH_UNIQUE_PARTS = '08 Nov 1970 23:24';

/**
 * Enum for flow options.
 * @enum {Number}
 */
// eslint-disable-next-line no-unused-vars
const FlowOptions = {
    /**
     * <b>1.</b> Will use LTR if the user's language is LTR.  In RTL, will evaluate the display of a sample date, inspect the order,
     * and mirror it, so that it will appear properly if BDO is forced to RTL.  Then all the separated DIVs will appear in the same order HTML would have ordered them.
     */
    CALCULATE: 1,

    /**
     * <b>2.</b> Align spinners and separators in the flow of the document.  Note, this will <b>NOT</b> always be the same as when printed in HTML or in a textbox,
     * especially when RTL and LTR characters are mixed.  For an exact representation of printed value, use <i>calculate</i>
     */
    NATURAL: 2,

    /**
     * <b>3.</b> Force an RTL flow of the flexbox elements.
     */
    RTL: 3,

    /**
     * <b>4.</b> Force an LTR flow of the flexbox elements.
     */
    LTR: 4
};

/**
 * A variation of the spinner control that allows for a date and time spinner.
 * Only difference in spinners for "date", "date-time" and "time" are the Modal Templates
 * @interface
 */
class IntervalSpinnerBase extends BaseSpinner {
    /**
     * Override value getter.  Transform to storage value.
     */
    get value () {
        let curVal = super.value,
            displayFormat = this.model.get('displayFormat'),
            displayLocale = this.moment.locale(),
            storageFormat = this.model.get('storageFormat'),
            storageLocale = this.model.get('storageLocale');

        return moment(curVal, displayFormat, displayLocale).locale(storageLocale).format(storageFormat);
    }

    /**
     * Override value setter.  Transform storage value to display value.
     * @param {*} val The value
     */
    set value (val) {
        let displayFormat = this.model.get('displayFormat'),
            displayLocale = this.moment.locale(),
            storageFormat = this.model.get('storageFormat'),
            storageLocale = this.model.get('storageLocale');

        super.value = moment(val, storageFormat, storageLocale).locale(displayLocale).format(displayFormat);
    }

    /**
     * default modal template
     */
    get defaultModalTemplate () {
        throw new Error('Invalid Interval Spinner Implementation.  A Modal Template is required');
    }

    get defaultDisplayFormat () {
        throw new Error('Invalid Interval Spinner Implementation.  A Display Format is required');
    }

    get defaultStorageFormat () {
        throw new Error('Invalid Interval Spinner Implementation.  A Storage Format is required');
    }

    get defaultDateFormat () {
        return DEFAULT_DATE_FORMAT;
    }

    get defaultTimeFormat () {
        return DEFAULT_TIME_FORMAT;
    }

    get defaultDateTimeFormat () {
        return DEFAULT_DATE_TIME_FORMAT;
    }

    get defaultStorageLocale () {
        return DEFAULT_STORAGE_LOCALE;
    }

    /**
     * default spinner template
     * @returns {string}
     */
    get defaultSpinnerTemplate () {
        return DEFAULT_SPINNER_TEMPLATE;
    }

    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * default spinner item template
     * @returns {string}
     */
    get defaultSpinnerItemTemplate () {
        return DEFAULT_SPINNER_ITEM_TEMPLATE;
    }

    get defaultYear () {
        return this.model.get('initialDate').getFullYear();
    }

    get defaultMonth () {
        return this.model.get('initialDate').getMonth();
    }

    get defaultDay () {
        return this.model.get('initialDate').getDate();
    }

    get defaultHour () {
        return this.model.get('initialDate').getHours();
    }

    get defaultMinute () {
        return this.model.get('initialDate').getMinutes();
    }

    get defaultInitialDate () {
        return this.constructedDate;
    }

    get defaultMinDate () {
        let tempDt = this.constructedDate;
        return new Date(tempDt.setFullYear(tempDt.getFullYear() - DEFAULT_DATE_YEAR_RANGE));
    }

    get defaultMaxDate () {
        let tempDt = this.constructedDate;
        return new Date(tempDt.setFullYear(tempDt.getFullYear() + DEFAULT_DATE_YEAR_RANGE));
    }

    get year () {
        let ret = null;
        if (typeof this.yearIndex === 'number') {
            ret = this.spinners[this.yearIndex].value;
        }
        ret = parseInt(ret, 10);
        return isNaN(ret) === false ? ret : this.defaultYear;
    }

    get month () {
        let ret = null;
        if (typeof this.monthIndex === 'number') {
            ret = this.spinners[this.monthIndex].value;
        }
        ret = parseInt(ret - 1, 10);

        // ret becomes -1 when null.  So check for -1 instead of NaN.
        return ret !== -1 || ret === undefined ? ret : this.defaultMonth;
    }

    get day () {
        let ret = null;
        if (typeof this.dayIndex === 'number') {
            ret = this.spinners[this.dayIndex].value;
        }
        ret = parseInt(ret, 10);
        return isNaN(ret) === false ? ret : this.defaultDay;
    }

    get hour () {
        let ret = null;
        if (typeof this.hourIndex === 'number') {
            ret = this.spinners[this.hourIndex].value;
        }
        ret = parseInt(ret, 10);
        if (typeof this.meridianIndicatorIndex === 'number') {
            ret += 12 * parseInt(this.spinners[this.meridianIndicatorIndex].value, 10);
        }
        return isNaN(ret) === false ? ret : this.defaultHour;
    }

    get minute () {
        let ret = null;
        if (typeof this.minuteIndex === 'number') {
            ret = this.spinners[this.minuteIndex].value;
        }
        ret = parseInt(ret, 10);
        return isNaN(ret) === false ? ret : this.defaultMinute;
    }

    /**
     * getter for constructed date.  Creates a new Date() so that the object is cloned each time, and cannot be modified.
     */
    get constructedDate () {
        return new Date(this._constructedDate);
    }

    /**
     * Verify that the UI is acceptable for this flavor of IntervalSpinner (i.e. date/time/datetime)
     * @abstract
     */
    validateUI () {
        throw new Error('IntervalSpinnerBase.validateUI() must be overridden by subclass');
    }

    /**
     * Return date object or null if it cannot be parsed
     * @param {Date|string|function} date, date string or function to parse
     * @returns {Date|null} date object from parsed date, or null
     */
    static dateOrNull (date) {
        // Blank or null dates should return null, instead of being an empty constructor for moment.
        if (!date) {
            return null;
        } else if (typeof date === 'function') {
            date = date();
        }
        let m = moment(date);
        if (m.isValid()) {
            return m.toDate();
        }
        return null;
    }

    /**
     * Construct the interval spinner
     * @param {Object} options options for this widget (see super class for option definitions).
     */
    constructor (options) {
        super(options);

        /**
         * Date that the widget was constructed.  This is used as the basis for default dates/times, etc.
         * so that they will be consistent regardless of when the getters are called.
         * @private
         * @type {Date}
         */
        this._constructedDate = new Date();

        /**
         * Array of number spinners.
         * @type {Array<BaseSpinnerInput>}
         */
        this.spinners = [];

        this.yearIndex = null;
        this.monthIndex = null;
        this.dayIndex = null;
        this.hourIndex = null;
        this.minuteIndex = null;
        this.meridianIndicatorIndex = null;

        /**
         * Whether or not to suppress update events.  Typically false, unless we are forcibly stopping the spinners.
         */
        this.suppressUpdateEvents = false;

        this.model.set('initialDate', IntervalSpinnerBase.dateOrNull(this.options.model.get('initialDate')) || this.defaultInitialDate);
        this.model.set('minDate', IntervalSpinnerBase.dateOrNull(this.options.model.get('minDate')) || this.defaultMinDate);
        this.model.set('maxDate', IntervalSpinnerBase.dateOrNull(this.options.model.get('maxDate')) || this.defaultMaxDate);

        this.model.set('displayFormat', this.options.model.get('displayFormat') || this.defaultDisplayFormat);
        this.model.set('storageFormat', this.options.model.get('storageFormat') || this.defaultStorageFormat);
        this.model.set('storageLocale', this.options.model.get('storageLocale') || this.defaultStorageLocale);

        this.model.set('dateFormat', this.options.model.get('dateFormat') || this.defaultDateFormat);
        this.model.set('timeFormat', this.options.model.get('timeFormat') || this.defaultTimeFormat);
        this.model.set('dateTimeFormat', this.options.model.get('dateTimeFormat') || this.defaultDateTimeFormat);

        let flow = this.options.model.get('flow');
        if (typeof flow === 'string') {
            flow = FlowOptions[flow.toUpperCase()];
        } else if (typeof flow !== 'number' && typeof flow !== 'undefined') {
            throw new Error('Flow is invalid.  Accepts only numbers and strings.');
        }

        this.options.model.set('flow', flow || FlowOptions.CALCULATE);

        this.moment = moment();

        let localeInfo = [];
        if (LF.Preferred.language) {
            localeInfo.push(LF.Preferred.language);
        }
        if (LF.Preferred.locale) {
            localeInfo.push(LF.Preferred.locale);
        }

        this.moment.locale(localeInfo.join('-'));
    }

    /**
     * Get the string value for the text box from an array of spinner values.
     * @returns {Promise<string>} promise returning a string to be used for our textbox.
     */
    getModalValuesString () {
        this.suppressUpdateEvents = true;

        for (let i = 0; i < this.spinners.length; ++i) {
            this.spinners[i].stop();
            this.spinners[i].pushValue();
        }

        this.suppressUpdateEvents = false;
        return this.setSpinnerLimits()
        .then(() => {
            let dt = this.getDate(),
                storageLocale = this.model.get('storageLocale'),
                storageFormat = this.model.get('storageFormat');

            return moment(dt).locale(storageLocale).format(storageFormat);
        });
    }

    /**
     * Get the array of spinner values from the textbox entry.
     * @returns {Array<Number>} the array to be passed along to our spinners.
     */
    getSpinnerValuesArray () {
        let textBox = this.$(`#${this.model.get('id')}`),
            valArray = [],
            defaultVal = this.model.get('initialDate') || '',
            val;

        val = textBox.val();
        if (val === '' || val === undefined) {
            val = moment(defaultVal).locale(this.moment.locale()).format(this.model.get('displayFormat'));
        }
        val = moment(val, this.model.get('displayFormat'), this.moment.locale()).toDate();

        if (typeof this.yearIndex === 'number') {
            valArray[this.yearIndex] = val.getFullYear();
        }

        if (typeof this.monthIndex === 'number') {
            valArray[this.monthIndex] = val.getMonth() + 1;
        }

        if (typeof this.dayIndex === 'number') {
            valArray[this.dayIndex] = val.getDate();
        }

        if (typeof this.hourIndex === 'number') {
            valArray[this.hourIndex] = val.getHours();
        }

        if (typeof this.meridianIndicatorIndex === 'number') {
            if (valArray[this.hourIndex] >= 12) {
                valArray[this.hourIndex] = valArray[this.hourIndex] % 12;
                valArray[this.meridianIndicatorIndex] = 1;
            } else {
                valArray[this.meridianIndicatorIndex] = 0;
            }
        }

        if (typeof this.minuteIndex === 'number') {
            valArray[this.minuteIndex] = val.getMinutes();
        }

        return valArray;
    }

    adjustDatePart (datePart) {
        let datePartIndex = this[`${datePart}Index`];

        if (datePartIndex === null || this.spinners.length === 0) {
            return Q(false);
        }

        let chain = Q(),
            scrollTotal = 0;

        let startNdx,
            endNdx,
            endComp;
        switch (datePart) {
            case 'month':
                startNdx = 1;
                endNdx = 12;
                break;
            case 'day':
                startNdx = 1;
                endNdx = 31;
                break;
            case 'meridianIndicator':
                startNdx = 0;
                endNdx = 1;
                break;
            case 'hour':
                startNdx = 0;
                endNdx = 23;
                break;
            case 'minute':
                startNdx = 0;
                endNdx = 59;
                break;

            // No default
        }

        if (datePart === 'hour' && typeof this.meridianIndicatorIndex === 'number') {
            startNdx = 0;
            endNdx = 11;
            endComp = endNdx + 12 * this.spinners[this.meridianIndicatorIndex].value;
        } else {
            endComp = endNdx;
        }

        // Iterate backwards.  The reason for this is that unHideItem() will determine whether or not it needs to scroll based on the current scroll position.
        //      The current scroll position is altered by adding things that take up space, so new unhidden items need to be at a lower scroll position (0) to work.
        //          so that unHideItem knows that it should adjust this offset accordingly.
        for (let i = endNdx, comp = endComp; i >= startNdx; --i, --comp) {
            if (!this.testDate(datePart, comp)) {
                chain = chain.then(() => {
                    return this.spinners.length > 0 ?
                        this.spinners[datePartIndex].hideItem(i, false) :
                        Q();
                });
            } else {
                chain = chain.then(() => {
                    return this.spinners.length > 0 ?
                        this.spinners[datePartIndex].unHideItem(i, false) :
                        Q();
                });
            }
            chain = chain.then((scrollVal) => {
                scrollTotal += scrollVal;
            });
        }

        return chain.then(() => {
            return this.spinners[datePartIndex].adjustScroll(scrollTotal);
        })
        .then(() => {
            this.spinners[datePartIndex].pushValue();
        });
    }

    /**
     * Update divs containing meridian indicators with strings that correspond to our time now and our time if we were
     * to adjust 12 hours
     * @returns {boolean}
     */
    // eslint-disable-next-line consistent-return
    setMeridianDisplayValues () {
        let curTime = moment(this.getDate(), null, this.moment.locale()),
            lowTime,
            highTime,
            $meridianContainer,
            meridianFormat;

        if (typeof this.meridianIndicatorIndex !== 'number') {
            return false;
        }

        $meridianContainer = $(this.spinners[this.meridianIndicatorIndex].parent);

        meridianFormat = $meridianContainer.data('format') || 'A';

        if (curTime.hours() < 12) {
            lowTime = curTime;
            highTime = moment(curTime, null, this.moment.locale()).add(12, 'h');
        } else {
            highTime = curTime;
            lowTime = moment(curTime, null, this.moment.locale()).add(-12, 'h');
        }

        // jscs:disable requireArrowFunctions
        $meridianContainer.find('.item').each(function () {
            let $this = $(this);
            switch ($this.data('value')) {
                case 0:
                    $this.html(lowTime.format(meridianFormat));
                    break;
                case 1:
                    $this.html(highTime.format(meridianFormat));
                    break;

                // No default
            }
        });
        // jscs:enable requireArrowFunctions
    }

    // Figure out the max and min of each spinner, based on the current values of the other spinners
    // disable options we can't use
    setSpinnerLimits () {
        if (this.suppressUpdateEvents || this.spinners.length === 0) {
            return Q();
        }

        for (let i = 0; i < this.spinners.length; ++i) {
            if (!this.spinners[i].isRendered || this.spinners[i].value === null) {
                // Some spinner is in flux.  Wait for all of them to finish spinning before adjusting the limits.
                return Q();
            }
        }

        // Make sure this is the only chain doing this right now.
        this.suppressUpdateEvents = true;

        // Start from largest value and work way to smallest.
        // Short circuits on each adjustment (if value changes, this function will be re-entered).
        return this.adjustDatePart('month')
        .then(() => {
            return this.adjustDatePart('day');
        })
        .then(() => {
            return this.adjustDatePart('meridianIndicator');
        })
        .then(() => {
            return this.adjustDatePart('hour');
        })
        .then(() => {
            return this.adjustDatePart('minute');
        })
        .then(() => {
            // Last step.  Change meridian display values to sync up with the current date and time on the spinner.
            return this.setMeridianDisplayValues();
        })
        .then(() => {
            this.suppressUpdateEvents = false;
        });
    }

    addCustomEvents () {
        super.addCustomEvents();

        if (!this.spinners) {
            return;
        }

        this.setSpinnerLimits = _.bind(this.setSpinnerLimits, this);
        for (let i = 0; i < this.spinners.length; ++i) {
            this.listenTo(this.spinners[i].model, 'change:value', this.setSpinnerLimits);
        }
    }

    removeCustomEvents () {
        for (let i = 0; this.spinners && i < this.spinners.length; ++i) {
            this.stopListening(this.spinners[i].model);
        }
        super.removeCustomEvents();
    }

    testDate (customProperty, customValue) {
        let testDt = this.getDate(customProperty, customValue),
            minDt = new Date(testDt),
            maxDt = new Date(testDt);

        // Determine min and max date/times for range checks
        // noinspection FallThroughInSwitchStatementJS
        switch (customProperty) {
            case 'month':
                minDt.setDate(1);
                maxDt.setMonth(maxDt.getMonth() + 1);
                maxDt.setDate(0);
            // eslint-disable-next-line no-fallthrough
            case 'day':
                minDt.setHours(0);
                maxDt.setHours(23);
            // eslint-disable-next-line no-fallthrough
            case 'meridianIndicator':
                if (customProperty === 'meridianIndicator') {
                    if (customValue === 1) {
                        minDt.setHours(12);
                        maxDt.setHours(23);
                    } else {
                        minDt.setHours(0);
                        maxDt.setHours(11);
                    }
                }
            // eslint-disable-next-line no-fallthrough
            case 'hour':
                minDt.setMinutes(0);
                maxDt.setMinutes(59);
            // eslint-disable-next-line no-fallthrough
            case 'minute':
                minDt.setSeconds(0);
                maxDt.setSeconds(59);
                break;

            // No default
        }

        // Months are not the same.  Day is past max day for the month.
        // Also check if our time is in the specified range.
        if (
            (
                testDt.getMonth() !== this.month &&
                customProperty === 'day'
            ) ||
            (
                testDt.getHours() !== customValue &&
                customProperty === 'hour'
            ) ||
            minDt.getTime() > (new Date(this.model.get('maxDate'))).getTime() ||
            maxDt.getTime() < (new Date(this.model.get('minDate'))).getTime()
        ) {
            return false;
        }
        return true;
    }

    /**
     * Get date from spinners.  Optionally allows a custom property to perform tests.  Short circuits after the
     * custom property is applied (for instance if the property is 'month', it will not set days, hours, or minutes
     * and will default them to 1, 0, and 0.
     * @param {*} customProperty The custom property
     * @param {*} customValue The custom value
     * @returns {*}
     */
    getDate (customProperty = null, customValue = null) {
        if (this.spinners.length === 0) {
            // Return null... no spinners yet, so no date.
            return null;
        }
        let m = moment('1970-01-01', null, 'en');

        m.year(customProperty === 'year' ? customValue : this.year);
        if (customProperty === 'year') {
            return m.toDate();
        }

        m.month(customProperty === 'month' ? customValue - 1 : this.month);
        if (customProperty === 'month') {
            return m.toDate();
        }

        m.date(customProperty === 'day' ? customValue : this.day);
        if (customProperty === 'day') {
            return m.toDate();
        }

        m.hour(customProperty === 'hour' ? customValue : this.hour);
        if (customProperty === 'hour') {
            return m.toDate();
        }

        m.minute(customProperty === 'minute' ? customValue : this.minute);

        return m.toDate();
    }

    getYearItemsArray (format) {
        let minYear = (new Date(this.model.get('minDate'))).getFullYear(),
            maxYear = (new Date(this.model.get('maxDate'))).getFullYear(),
            items = [];

        for (let i = minYear; i <= maxYear; ++i) {
            let m = moment(`01 Jan ${i}`, 'DD MMM YYYY', 'en-US');
            m.locale(this.moment.locale());
            items.push({
                value: i,
                text: m.format(format)
            });
        }
        return items;
    }

    getMonthItemsArray (format) {
        let items = [],
            m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
        m.locale(this.moment.locale());

        for (let i = 0; i < 12; ++i) {
            items.push({
                value: i + 1,
                text: m.format(format)
            });
            m.add(1, 'M');
        }
        return items;
    }

    getDayItemsArray (format) {
        let items = [];
        let m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
        m.locale(this.moment.locale());

        for (let i = 1; i <= 31; ++i) {
            // pad only if necessary
            items.push({
                value: i,
                text: m.format(format)
            });
            m.add(1, 'd');
        }
        return items;
    }

    getHourItemsArray (format) {
        let items = [];
        let m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
        m.locale(this.moment.locale());

        for (let i = 0; i <= 23; ++i) {
            // pad only if necessary
            items.push({
                value: i,
                text: m.format(format)
            });
            m.add(1, 'h');
        }
        return items;
    }

    getMinuteItemsArray (format) {
        let items = [];
        let m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
        m.locale(this.moment.locale());

        for (let i = 0; i <= 59; ++i) {
            // pad only if necessary
            items.push({
                value: i,
                text: m.format(format)
            });
            m.add(1, 'm');
        }
        return items;
    }

    getMeridiemItemsArray () {
        // value changed handler will set text for the meridian displays based on exact time of day.
        // No need to do it here, just make an array of size 2
        return [{
            value: 0,
            text: ''
        }, {
            value: 1,
            text: ''
        }];
    }

    parseDateParts (format) {
        let dateParts = [],
            i = 0,
            part;
        while (i < format.length) {
            let token = format.charAt(i),
                acceptableChars = {};
            switch (token) {
                case 'D':
                    // Allow 'Do' which is an ordinate day
                    acceptableChars.o = true;
                // eslint-disable-next-line no-fallthrough
                case 'M':
                case 'Y':
                case 'H':
                case 'h':
                case 'm':
                case 'A':
                case 'd':
                case 'a':
                    acceptableChars[token] = true;
                    part = '';
                    while (acceptableChars[format.charAt(i)]) {
                        part += format.charAt(i);
                        ++i;
                    }
                    dateParts.push(part);

                    break;
                case 'L':
                case 'T':
                    part = '';
                    while (format.charAt(i) === 'L' || format.charAt(i) === 'T') {
                        part += format.charAt(i);
                        ++i;
                    }

                    dateParts = dateParts.concat(this.parseDateParts(this.moment.localeData().longDateFormat(part)));
                    break;
                default: {
                    // Not a date or time formatter.  Make a node for all the extraneous chars (part of formatting)
                    let validDateChars = /[DMYHhmAa]/;
                    part = '';

                    while (!validDateChars.test(format.charAt(i)) && i < format.length) {
                        part += format.charAt(i);
                        ++i;
                    }
                    dateParts.push(part);
                    break;
                }
            }
        }
        return dateParts;
    }

    /**
     * Generate a test date and force strict RTL on it.
     * Then take deconstructed date parts to reinsert formatted sections.
     * This will result in a new dateParts array that will exactly match the flow of the RTL language, when placed into separate spans
     * (with bdo dir set to RTL)
     * @param {Array<String>} dateParts array of date parts to process.
     * @returns {Array<String>} dateParts reordred (in RTL) to work with a strict RTL document flow.
     */
    datePartsWithDocumentFlow (dateParts) {
        if (LF.strings.getLanguageDirection() !== 'rtl' || this.model.get('flow') !== FlowOptions.CALCULATE) {
            return dateParts;
        }

        let oldFormat = dateParts.join(''),
            displayLocale = this.moment.locale();

        let testMoment = moment(DATE_WITH_UNIQUE_PARTS).locale(displayLocale),
            newFormat = toStrictRTL(testMoment.format(oldFormat));

        // Find where date parts are being used, and put them back.
        _.each(dateParts, (part) => {
            let formatted = testMoment.format(part);
            if (formatted !== part) {
                newFormat = newFormat.replace(formatted, part);
                newFormat = newFormat.replace(formatted.split('').reverse().join(''), part);
            }
        });

        // Now test date should be a new format string, forcing strict RTL for all characters
        return this.parseDateParts(newFormat);
    }

    buildDynamicTemplate () {
        let that = this;

        // jscs:enable requireArrowFunctions
        // jscs:disable requireArrowFunctions
        // eslint-disable-next-line consistent-return
        that.$modal.find('.date-container, .time-container, .date-time-container').each(function () {
            let $this = $(this);

            // jscs:disable maximumLineLength
            if ($this.children('.day-spinner-container, .month-spinner-container, .year-spinner-container, .hour-spinner-container, .minute-spinner-container, .meridian-indicator-spinner-container').length !== 0) {
                // Date or time children inside here already.  Don't build a template
                return false;
            }
            // jscs:enable maximumLineLength

            let format = $this.data('format');
            if (!format && $this.hasClass('date-container')) {
                format = that.model.get('dateFormat');
            } else if (!format && $this.hasClass('time-container')) {
                format = that.model.get('timeFormat');
            } else if (!format && $this.hasClass('date-time-container')) {
                format = that.model.get('dateTimeFormat');
            }

            // use "expanded" date format for "LL/LT", etc., defaulting to the literal passed in format.
            let dateparts = that.parseDateParts(format);
            dateparts = that.datePartsWithDocumentFlow(dateparts);

            for (let i = 0; i < dateparts.length; ++i) {
                let datePart = dateparts[i];
                switch (datePart.substring(0, 1)) {
                    case 'M':
                        $this.append(`<div class="month-spinner-container" data-format="${datePart}"></div>`);
                        break;
                    case 'D':
                        $this.append(`<div class="day-spinner-container" data-format="${datePart}"></div>`);
                        break;
                    case 'Y':
                        $this.append(`<div class="year-spinner-container" data-format="${datePart}"></div>`);
                        break;
                    case 'H':
                    case 'h':
                        $this.append(`<div class="hour-spinner-container" data-format="${datePart}"></div>`);
                        break;
                    case 'm':
                        $this.append(`<div class="minute-spinner-container" data-format="${datePart}"></div>`);
                        break;
                    case 'A':
                        $this.append(`<div class="meridian-indicator-spinner-container" data-format="${datePart}"></div>`);
                        break;
                    default:
                        datePart = datePart.replace(/\s+/g, '&nbsp;');
                        datePart = datePart.replace(/[\[\]]/g, '');
                        $this.append(`<div class="separator">${datePart}</div>`);
                        break;
                }
            }
        });
    }

    /**
     * Set the flow style class for the modal dialog
     */
    setModalFlow () {
        let className;
        switch (this.model.get('flow')) {
            case FlowOptions.CALCULATE:
                className = 'flow-calculate';
                break;
            case FlowOptions.NATURAL:
                className = 'flow-natural';
                break;
            case FlowOptions.LTR:
                className = 'flow-ltr';
                break;
            case FlowOptions.RTL:
                className = 'flow-rtl';
                break;
            default:
                logger.error(`Flow provided is not a member of the enumerable.  Value provided was ${this.model.get('flow')}`);
        }
        this.$modal.addClass(className);
    }

    /**
     * Place spinners into place
     */
    injectSpinnerInputs () {
        let spinnerInputOptions = this.model.get('spinnerInputOptions') || [],
            that = this;

        this.buildDynamicTemplate();
        this.setModalFlow();

        // jscs:disable requireArrowFunctions
        let i = -1;
        that.$modal.find('.year-spinner-container').each(function () {
            let spinnerTemplate,
                spinnerItemTemplate,
                $this = $(this);

            i++;
            that.yearIndex = i;
            spinnerTemplate = that.getSpinnerTemplate(i);
            spinnerItemTemplate = that.getSpinnerItemTemplate(i);

            let items = that.getYearItemsArray($this.data('format') || 'YYYY');

            that.spinners[i] = new ValueSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate,
                    numItems: DEFAULT_NUMBER_OF_SPINNER_ITEMS,
                    items
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        that.$modal.find('.month-spinner-container').each(function () {
            let spinnerTemplate,
                spinnerItemTemplate,
                $this = $(this);

            i++;
            that.monthIndex = i;
            spinnerTemplate = that.getSpinnerTemplate(i);
            spinnerItemTemplate = that.getSpinnerItemTemplate(i);

            let items = that.getMonthItemsArray($this.data('format') || 'MMM');

            that.spinners[i] = new ValueSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate,
                    numItems: DEFAULT_NUMBER_OF_SPINNER_ITEMS,
                    items
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        that.$modal.find('.day-spinner-container').each(function () {
            let spinnerTemplate,
                spinnerItemTemplate,
                $this = $(this);

            i++;
            that.dayIndex = i;
            spinnerTemplate = that.getSpinnerTemplate(i);
            spinnerItemTemplate = that.getSpinnerItemTemplate(i);

            let items = that.getDayItemsArray($this.data('format') || 'DD');

            that.spinners[i] = new ValueSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate,
                    numItems: DEFAULT_NUMBER_OF_SPINNER_ITEMS,
                    items
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        that.$modal.find('.hour-spinner-container').each(function () {
            let spinnerTemplate,
                spinnerItemTemplate,
                $this = $(this);

            i++;
            that.hourIndex = i;
            spinnerTemplate = that.getSpinnerTemplate(i);
            spinnerItemTemplate = that.getSpinnerItemTemplate(i);

            let items = that.getHourItemsArray($this.data('format') || 'hh');

            that.spinners[i] = new ValueSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate,
                    numItems: DEFAULT_NUMBER_OF_SPINNER_ITEMS,
                    items
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        that.$modal.find('.minute-spinner-container').each(function () {
            let spinnerTemplate,
                spinnerItemTemplate,
                $this = $(this);

            i++;
            that.minuteIndex = i;
            spinnerTemplate = that.getSpinnerTemplate(i);
            spinnerItemTemplate = that.getSpinnerItemTemplate(i);

            let items = that.getMinuteItemsArray($this.data('format') || 'mm');

            that.spinners[i] = new ValueSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate,
                    numItems: DEFAULT_NUMBER_OF_SPINNER_ITEMS,
                    items
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        that.$modal.find('.meridian-indicator-spinner-container').each(function () {
            let spinnerTemplate,
                spinnerItemTemplate;

            i++;
            that.meridianIndicatorIndex = i;
            spinnerTemplate = that.getSpinnerTemplate(i);
            spinnerItemTemplate = that.getSpinnerItemTemplate(i);

            // Update hour spinner to be 12 hour now.
            let hourItems = that.spinners[that.hourIndex].model.get('items');
            hourItems.splice(12, 12);
            that.spinners[that.hourIndex].model.set('items', hourItems);

            let items = that.getMeridiemItemsArray();

            that.spinners[i] = new ValueSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate,
                    numItems: DEFAULT_NUMBER_OF_SPINNER_ITEMS,
                    items
                }, spinnerInputOptions[i] || {}
                )
            );
        });
        // jscs:enable requireArrowFunctions

        this.validateUI();
    }
}

// Exports
export { FlowOptions };
export default IntervalSpinnerBase;
