/** #depends Base.js
 * @file Defines horizontal visual analog scale widget.
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.4
 */

// - Core Modules
import BaseVas from './BaseVas';

export default class HorizontalVAS extends BaseVas {
    get events () {
        return {
            /** vmouse down event */
            'mousedown canvas': 'vmouseHandler',

            /** vmouse out event */
            'mouseleave canvas': 'vmouseHandler',

            /** vmouse up event */
            'mouseup canvas': 'vmouseHandler',

            /** vmouse move event */
            'mousemove canvas': 'vmouseHandler',
            'touchstart canvas': 'mouseTouchHandler',
            'touchmove canvas': 'mouseTouchHandler'
        };
    }

    get valueBoxWidth () {
        if (this._valueBoxWidth === null) {
            switch (this.selectedValue.location) {
                case 'dynamic':
                case 'both':
                    if (!isNaN(this.answerBoxSize())) {
                        this._valueBoxWidth = this.answerBoxSize();
                    }
                    break;
                default:
                    this._valueBoxWidth = 0;
            }
        }
        return this._valueBoxWidth;
    }

    get maxMarkerWidth () {
        if (this._maxMarkerWidth === null) {
            // eslint-disable-next-line new-cap
            this._maxMarkerWidth = this.MeasureText(this.anchors.max.value.toString(),
                false,
                this.anchors.font.name,
                this.anchors.font.size).width;
        }
        return this._maxMarkerWidth;
    }

    get unitPixelSize () {
        return Math.floor((this.canvasWidth - Math.max(this.maxMarkerWidth, this.valueBoxWidth)) / 100);
    }

    get defaultVasLineWidth () {
        if (this.unitPixelSize === 1) {
            return this.unitPixelSize * 101;
        }
        return this.unitPixelSize * 100;
    }

    get defaultVasLineHeight () {
        return this.unitPixelSize > 5 ? 5 : 3;
    }

    get defaultPointerLocation () {
        return 'left';
    }

    get defaultMinAnchorLocation () {
        return 'top';
    }

    get defaultPointerHeight () {
        return 40;
    }

    get defaultPointerWidth () {
        let pPointerWidth = this.strikeMark.width * 4;
        pPointerWidth = this.vasPosition.getLineStartX() < pPointerWidth / 2 ? this.vasPosition.getLineStartX() * 2 : pPointerWidth;
        return pPointerWidth;
    }

    get defaultAnchorHeight () {
        return 37;
    }

    get defaultAnchorWidth () {
        let _anchorWidget = 0;

        if (this.vasLine.height === 5) {
            _anchorWidget = 5;
        } else if (this.vasLine.height === 3) {
            _anchorWidget = 3;
        }

        return _anchorWidget;
    }

    get defaultStrikeMarkHeight () {
        return 33;
    }

    get defaultStrikeMarkWidth () {
        let _markWidth = 0;
        if (this.vasLine.height === 5) {
            _markWidth = 7;
        } else if (this.vasLine.height === 3) {
            _markWidth = 5;
        }

        return _markWidth;
    }

    get vasLineEnd () {
        return this.vasLine.width;
    }

    constructor (widget) {
        super(widget);

        let _anchorTextWidth;

        this._valueBoxWidth = null;
        this._maxMarkerWidth = null;

        /**
         * The template of the text labels
         * @type String
         * @default 'DEFAULT:VasTextLabels'
         */
        this.textLabels = 'DEFAULT:VasTextLabels';

        // this.options = options;
        // super(options);

        Object.defineProperty(this, 'anchorTextWidth', {
            get: () => {
                if (_anchorTextWidth === undefined) {
                    // eslint-disable-next-line new-cap
                    _anchorTextWidth = this.MeasureText(this.anchors.min.text,
                        false,
                        this.anchors.font.name,
                        this.anchors.font.size).width;
                }

                return _anchorTextWidth;
            }
        });

        Object.defineProperty(this, 'tickSize', {
            get: () => {
                let _tickMarkWidth;
                if (_tickMarkWidth === undefined) {
                    _tickMarkWidth = (this.vasPosition.getLineEndX() - this.vasPosition.getLineStartX()) / (this.anchors.max.value - this.anchors.min.value);
                }

                return _tickMarkWidth;
            }
        });

        let reverseOnRtl = this.model.get('reverseOnRtl');
        this.model.set('reverseOnRtl', typeof reverseOnRtl === 'boolean' ? reverseOnRtl : false);

        this.vasConfig = this.getVasConfig();
    }

    /**
     * This method returns an object containing some configurations of vas
     * @return {Object} The object contains configuration for label position, arrow and label justification
     * @example this.getVasConfig();
     */
    getVasConfig () {
        let labelProperties = this.customProperties.labels,
            defaultPosition = 'below',
            defaultArrow = true,
            defaultLabelSpaceAllowed = 20,
            defaultJustification = 'screenEdge';

        if (labelProperties === undefined) {
            return {
                labelPosition: defaultPosition,
                labelArrow: defaultArrow,
                labelJustified: defaultJustification,
                labelSpaceAllowed: defaultLabelSpaceAllowed,
                postValue: this.selectedValue.isVisible,
                pointer: this.pointer.isVisible
            };
        }
        return {
            labelPosition: labelProperties.position === undefined ? defaultPosition : labelProperties.position,
            labelArrow: labelProperties.arrow === undefined ? defaultArrow : labelProperties.arrow,
            labelJustified: labelProperties.justified === undefined ? defaultJustification : labelProperties.justified,
            labelSpaceAllowed: labelProperties.spaceAllowed === undefined ? defaultLabelSpaceAllowed : labelProperties.spaceAllowed,
            postValue: this.selectedValue.isVisible,
            pointer: this.pointer.isVisible
        };
    }

    /**
     * This method binds questionnaire:resize event for VAS
     */
    bindResize () {
        // If browser is resized, render the canvas again
        $('#questionnaire').bind('questionnaire:resize', _(this.resizeHandler).bind(this));
    }

    /**
     * The handler triggered when resize events are fired on the application.
     */
    resizeHandler () {
        if (this.resizeTimeout) {
            clearTimeout(this.resizeTimeout);
        }

        this.resizeTimeout = setTimeout(() => {
            this.render();
        }, 100);
    }

    getCanvasWidth () {
        return this.$('canvas#vasCanvas').width();
    }

    drawTheVas () {
        return Q()
        .then(() => {
            this.createCanvas();
        })
        .then(() => {
            return this.buildHTML();
        })
        .then(() => {
            return Q.Promise((resolve) => {
                this.canvasWidth = this.getCanvasWidth();

                this.setRtl();

                this.createVAS(this.canvasWidth);
                this.setCanvasPixels(this.canvasWidth);
                this.setPositionTextLabel();

                this.drawArrows();
                this.renderScale();
                this.renderNumericLabels();
                this.drawInitialUnitCursor();
                resolve();
            });
        });
    }

    getDirection () {
        return LF.strings.getLanguageDirection();
    }

    setRtl () {
        // If configured to, reverse entire scale when in a rtl language
        let dir = this.getDirection();
        if (!this.model.get('reverseOnRtl') && dir === 'rtl') {
            // Swap values
            this.anchors.swapMinMaxLocation = true;

            this.rtl = true;
        } else if (dir === 'rtl') {
            // language is rtl, but we're not supposed to reverse the layout.  Tell the container to ignore the reverse.
            $('.vas-label').addClass('ignore-rtl');
        }
    }

    /**
     * This method creates canvas elements, their contents and
     * adds touch event listeners
     * @example this.createCanvas();
     * @returns {Q.Promise<void>}
     */
    createCanvas () {
        return Q.Promise((resolve) => {
            this.numLabelCanvas = document.createElement('canvas');
            this.vasCanvas = document.createElement('canvas');
            this.unitCursorCanvas = document.createElement('canvas');
            this.arrowsCanvas = document.createElement('canvas');

            $(this.numLabelCanvas).attr('id', 'numLabelCanvas');
            $(this.vasCanvas).attr('id', 'vasCanvas');
            $(this.unitCursorCanvas).attr('id', 'unitCursorCanvas');
            $(this.arrowsCanvas).attr('id', 'arrowsCanvas');

            // Set canvas height with an initial value in order to keep initial calculations valid.
            this.numLabelCanvas.height = 10;
            this.vasCanvas.height = 10;
            this.unitCursorCanvas.height = 10;
            this.arrowsCanvas.height = 10;

            this.ctxNumLabels = this.numLabelCanvas.getContext('2d');
            this.ctxVas = this.vasCanvas.getContext('2d');
            this.ctxUnitCursor = this.unitCursorCanvas.getContext('2d');
            this.ctxArrows = this.arrowsCanvas.getContext('2d');

            $(this.vasCanvas).css('position', 'absolute').css('z-index', '0');
            $(this.unitCursorCanvas).css('z-index', '1').css('opacity', '0.99');

            $(this.unitCursorCanvas).addClass('vas-pointer');

            resolve();
        });
    }

    /**
     * This method builds html structure for vas and its labels.
     * @example this.buildHTML();
     * @returns {Q.Promise<void>}
     */
    buildHTML () {
        $('#questionnaire').addClass('HVAS');

        let stringsToFetch = {
                min: {
                    key: this.anchors.min.text,
                    namespace: this.getQuestion().getQuestionnaire().id
                },
                max: {
                    key: this.anchors.max.text,
                    namespace: this.getQuestion().getQuestionnaire().id
                }
            },
            textLabelHtml;

        return new Q.Promise((resolve) => {
            LF.getStrings(stringsToFetch, (strings) => {
                textLabelHtml = LF.Resources.Templates.display(this.textLabels, {
                    min: strings.min,
                    max: strings.max
                });

                if (this.vasConfig.labelPosition === 'above') {
                    this.$el.html(textLabelHtml);
                    if (this.vasConfig.labelArrow) {
                        this.$el.append(this.arrowsCanvas);
                    }
                    this.$el
                        .append(this.vasCanvas)
                        .append(this.unitCursorCanvas)
                        .append(this.numLabelCanvas);
                } else {
                    this.$el.html(this.numLabelCanvas)
                        .append(this.vasCanvas)
                        .append(this.unitCursorCanvas);
                    this.$el.append(textLabelHtml);
                }
                this.$el.appendTo(this.parent.$el);
                resolve();
            });
        });
    }


    /**
     * This method sets pixel sizes of canvas elements
     * @param {number} canvasWidth Width of canvas element
     * @example this.setCanvasPixels(this.$('canvas:first').width());
     */
    setCanvasPixels (canvasWidth) {
        let fontString = this.fontString(this.anchors.font);
        // eslint-disable-next-line new-cap
        let ht = this.MeasureText(this.anchors.max.value, false, this.selectedValue.font.name,
            this.selectedValue.font.size).height;
        // eslint-disable-next-line new-cap
        let wdt = this.MeasureText(this.anchors.max.value, false, this.selectedValue.font.name,
            this.selectedValue.font.size).width;

        let staticAnswerSize = ht > wdt ? ht + 10 : wdt + 10;

        this.$('.vas-label p').css('font-size', fontString);

        let numLabelHeight = this.anchors.font.size + 4,
            arrowsHeight = this.pointer.height * 1.5;

        this.numLabelCanvas.height = numLabelHeight;
        $(this.numLabelCanvas).height(this.numLabelCanvas.height);

        if ((this.selectedValue.location === 'static' || this.selectedValue.location === 'both') &&
            this.numLabelCanvas.height < staticAnswerSize) {
            this.numLabelCanvas.height = staticAnswerSize;
            $(this.numLabelCanvas).height(this.numLabelCanvas.height);
        }

        this.arrowsCanvas.height = arrowsHeight;
        $(this.arrowsCanvas).height(this.arrowsCanvas.height);

        // Either the bottom of the pointer, or the bottom of the cursor, depending on label position.
        let minVasSize;
        if (this.vasConfig.labelPosition === 'below') {
            minVasSize = this.pointer.isVisible ?
                this.vasPosition.getCursorEndY() + this.cursorWidth * 2 :
                0;
        } else {
            minVasSize = this.pointer.isVisible ?
                this.vasPosition.getAnchorEndY() :
                0;
        }

        this.vasCanvas.height = !this.vasConfig.labelArrow ?
            this.anchors.height + (this.pointer.isVisible ? this.pointer.height : 0) :
            this.arrowsCanvas.height + this.anchors.height + 3;

        if (this.vasCanvas.height < minVasSize) {
            this.vasCanvas.height = minVasSize;
        }

        $(this.vasCanvas).height(this.vasCanvas.height);

        this.unitCursorCanvas.height = this.vasCanvas.height;
        $(this.unitCursorCanvas).height(this.unitCursorCanvas.height);

        this.numLabelCanvas.width = canvasWidth;
        this.vasCanvas.width = canvasWidth;
        this.unitCursorCanvas.width = canvasWidth;
        this.arrowsCanvas.width = canvasWidth;
        $(this.numLabelCanvas).width(this.numLabelCanvas.width);
        $(this.vasCanvas).width(this.vasCanvas.width);
        $(this.unitCursorCanvas).width(this.unitCursorCanvas.width);
        $(this.arrowsCanvas).width(this.arrowsCanvas.width);
    }

    /**
     * This method positions text labels using css styles.
     * @example this.setPositionTextLabel();
     */
    setPositionTextLabel () {
        let leftLabelHeight,
            rightLabelHeight,
            textLength;

        // Other justifications could be added to this switch block, if needed in the future.
        switch (this.vasConfig.labelJustified) {
            case 'center':
                this.$('.vas-label').addClass('center');
                this.$('.vas-label .ui-block-b p').addClass('right');

                // Text cannot exceed the labelSpaceAllowed unit pixel size.
                // If the margin of the start point is more than labelSpaceAllowed unit pixel size, fit the labelSpaceAllowed unit pixel size area.
                if (this.vasPosition.getLineStartX() > this.vasLine.getUnitPixelSize() * this.vasConfig.labelSpaceAllowed) {
                    textLength = this.vasLine.getUnitPixelSize() * this.vasConfig.labelSpaceAllowed * 2;
                } else {
                    textLength = this.vasPosition.getLineStartX() * 2;
                }
                this.$('p').width(textLength);
                this.$('.vas-label').width(this.vasPosition.getLineEndX() - this.vasPosition.getLineStartX() + textLength);
                break;
            default:
                // By default it is screenEdge justified.  Set the width, so that it cannot extend past the "spaceAllowed" point.
                this.$('.vas-label').addClass('vas-label-screenEdge');
                this.$('p').width(this.vasPosition.getLineStartX() + this.vasLine.getUnitPixelSize() * this.vasConfig.labelSpaceAllowed);
                break;
        }

        // Push down one of the text label if text labels have different heights
        if (this.vasConfig.labelPosition === 'above') {
            this.$('.vas-label').addClass('vas-label-above');
            leftLabelHeight = this.$('.ui-block-a p').height();
            rightLabelHeight = this.$('.ui-block-b p').height();
            if (leftLabelHeight > rightLabelHeight) {
                this.$('.ui-block-b').css('margin-top', leftLabelHeight - rightLabelHeight);
            } else if (leftLabelHeight < rightLabelHeight) {
                this.$('.ui-block-a').css('margin-top', rightLabelHeight - leftLabelHeight);
            }
        } else {
            this.$('.vas-label').addClass('vas-label-below');
        }
    }

    /**
     * This method draws initial unit cursor
     * @example this.drawInitialUnitCursor();
     */
    drawInitialUnitCursor () {
        let unitCursorPoint,
            answer = this.answer ? this.answer.get('response') : false,
            save = false,
            post = true;

        // If there is a previous response then get the unit cursor point.
        if (answer) {
            unitCursorPoint = this.getCursorPoint(answer);

            // If there is no previous response and initial unit cursor is on then get the middle point of canvas
        } else if (this.model.get('initialCursorDisplay')) {
            unitCursorPoint = this.getCursorPoint((parseFloat(this.anchors.max.value) + parseFloat(this.anchors.min.value)) / 2);
            post = false;
        }

        // If there is an unit cursor point then draw it.
        if (unitCursorPoint) {
            this.unitCursorMove = true;
            this.moveUnitCursor(unitCursorPoint, save, post);
            this.unitCursorMove = false;
        }
    }

    /**
     *  @param {number} canvasWidth Width of canvas
     */
    setVasLine () {
        let firstUnitPixelSize;

        if (this.unitPixelSize === 1) {
            firstUnitPixelSize = 1;
        } else {
            firstUnitPixelSize = Math.floor(this.unitPixelSize / 2);
        }

        this.vasLine.getUnitPixelSize = () => {
            return this.unitPixelSize;
        };

        this.vasLine.getFirstUnitPixelSize = () => {
            return firstUnitPixelSize;
        };
    }

    /**
     * Anchor object that serves anchor sizes, center pixel and extension
     * @namespace
     * @memberOf LF.Widget.HorizontalVAS
     */
    setVasAnchor () {
        let anchorWidth,
            anchorCenterPixel,
            anchorExtension,
            anchorHeight = this.anchors.height;
        if (this.vasLine.height === 5) {
            anchorWidth = 5;
            anchorExtension = (anchorHeight - 5) / 2;
        } else if (this.vasLine.height === 3) {
            anchorWidth = 3;
            anchorExtension = (anchorHeight - 3) / 2;
        }
        anchorCenterPixel = anchorWidth === 5 ? 3 : 2;

        this.anchors.getCenterPixel = () => {
            return anchorCenterPixel;
        };

        this.anchors.getExtension = () => {
            return anchorExtension;
        };
    }

    get cursorHeight () {
        let val;

        if (this.answer === undefined) {
            val = '0';
        } else {
            val = this.answer.get('response');
        }

        // eslint-disable-next-line new-cap
        let ht = this.MeasureText(val,
            false,
            this.selectedValue.font.name,
            this.selectedValue.font.size).height;

        return ht;
    }

    get cursorWidth () {
        // eslint-disable-next-line new-cap
        let wdt = this.MeasureText(`${this.anchors.max.value}0`,
            false,
            this.selectedValue.font.name,
            this.selectedValue.font.size).width;

        return wdt;
    }

    /**
     * Unit cursor object that serves unit cursor sizes, center pixel and extension
     * @namespace
     * @memberOf LF.Widget.HorizontalVAS
     */
    setUnitCursor () {
        let cursorWidth,
            cursorCenterPixel,
            cursorExtension,
            cursorHeight = this.strikeMark.height;
        if (this.vasLine.height === 5) {
            cursorWidth = 7;
            cursorExtension = (cursorHeight - 5) / 2;
        } else if (this.vasLine.height === 3) {
            cursorWidth = 5;
            cursorExtension = (cursorHeight - 3) / 2;
        }
        cursorCenterPixel = cursorWidth === 7 ? 4 : 3;

        this.strikeMark.getCenterPixel = () => {
            return cursorCenterPixel;
        };

        this.strikeMark.getExtension = () => {
            return cursorExtension;
        };

        // this.unitCursor = {
        //     /**
        //      * Get unit cursor height
        //      * @return {number} Unit cursor height
        //      * @memberOf LF.Widget.HorizontalVAS.unitCursor
        //      * @example this.unitCursor.getHeight();
        //      */
        //     getHeight: function () {
        //         return cursorHeight;
        //     },
        //     /**
        //      * Get unit cursor width
        //      * @return {number} Cursor width
        //      * @memberOf LF.Widget.HorizontalVAS.unitCursor
        //      * @example this.unitCursor.getWidth()
        //      */
        //     getWidth: function () {
        //         return cursorWidth;
        //     },
        //     /**
        //      * Get unit cursor center pixel
        //      * @return {number} Center pixel of unit cursor
        //      * @memberOf LF.Widget.HorizontalVAS.unitCursor
        //      * @example this.unitCursor.getCenterPixel()
        //      */
        //     getCenterPixel: function () {
        //         return cursorCenterPixel;
        //     },
        //     /**
        //      * Get unit cursor extension from vas line
        //      * @return {number}  Unit cursor extension
        //      * @memberOf LF.Widget.HorizontalVAS.unitCursor
        //      * @example this.unitCursor.getExtension()
        //      */
        //     getExtension: function () {
        //         return cursorExtension;
        //     }
        // };
    }

    /**
     * vasPosition object serves the positions of all vas components and vas margin
     * @namespace
     * @param {number} canvasWidth - The width of the canvas.
     * @memberOf LF.Widget.HorizontalVAS
     */
    setVasPosition (canvasWidth) {
        let _this = this,
            cursorX,
            margin = Math.floor((canvasWidth - (_this.vasLine.width +
                (2 * this.anchors.getCenterPixel()))) / 2),
            leftAnchorX = margin + 1,
            lineStartX = leftAnchorX + this.anchors.getCenterPixel() - 1,
            lineEndX = lineStartX + _this.vasLine.width - 1,
            cursorStartY = this.anchors.getExtension() - this.strikeMark.getExtension(),
            cursorEndY = cursorStartY + this.strikeMark.height,

            // since the leftAnchor x is + 1, this needs to be -2 (rather than -1) to be inside the line.
            rightAnchorX = lineEndX - (this.strikeMark.getCenterPixel() - 2);

        this.vasPosition = {
            /**
             * Get margin of vas
             * @return {number} Margin of vas
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getMargin();
             */
            getMargin () {
                return margin;
            },

            /**
             * Get x coordinate of vas line start point
             * @return {number} x coordinate of vas line start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getLineStartX();
             */
            getLineStartX () {
                return lineStartX;
            },

            /**
             * Get y coordinate of vas line start point
             * @return {number} y coordinate of vas line start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getLineStartY();
             */
            getLineStartY () {
                return _this.anchors.getExtension() + this.getAnchorY();
            },

            /**
             * Get x coordinate of vas line end point
             * @return {number} x coordinate of vas line end point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getLineEndX();
             */
            getLineEndX () {
                return lineEndX;
            },

            /**
             * Get y coordinate of anchors end point
             * @return {number} y coordinate of anchors end point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getAnchorEndY();
             */
            getAnchorEndY () {
                return this.getLeftAnchorY() + _this.anchors.height;
            },

            /**
             * Set and returns unit cursor start point's x coordinate
             * @param {number} x coordinate of unit cursor center pixel
             * @return {number} x coordinate of unit cursor start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.setCursorStartX(20);
             */
            setCursorStartX (x) {
                cursorX = x - ((_this.strikeMark.width - 1) / 2);
                return cursorX;
            },

            /**
             * Get unit cursor start point's y coordinate
             * @return {number} y coordinate of unit cursor start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getCursorStartY();
             */
            getCursorStartY () {
                return cursorStartY + this.getAnchorY();
            },

            /**
             * Get unit cursor end point's y coordinate
             * @return {number} y coordinate of unit cursor end point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getCursorEndY();
             */
            getCursorEndY () {
                return cursorEndY + this.getAnchorY();
            },

            /**
             * Get left anchor start point's x coordinate
             * @return {number} x coordinate of anchor's start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getLeftAnchorX()
             */
            getLeftAnchorX () {
                return leftAnchorX;
            },

            /**
             * Get left anchor start point's y coordinate
             * @return {number} y coordinate of left anchor start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getLeftAnchorY()
             */
            getLeftAnchorY () {
                return this.getAnchorY();
            },

            /**
             * Get right anchor start point's x coordinate
             * @return {number} x coordinate of right anchor start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getRightAnchorX()
             */
            getRightAnchorX () {
                return rightAnchorX;
            },

            /**
             * Get right anchor start point's y coordinate
             * @return {number} y coordinate of right anchor's start point
             * @memberOf LF.Widget.HorizontalVAS.vasPosition
             * @example this.vasPosition.getRightAnchorY()
             */
            getRightAnchorY () {
                return this.getAnchorY();
            },

            getAnchorY () {
                if (_this.vasConfig.labelPosition === 'above' && _this.vasConfig.pointer) {
                    return _this.cursorWidth * 2;
                }
                return 0;
            }
        };
    }

    /**
     * Pointer object serves the height and width of pointer(control handler for unit cursor)
     * @namespace
     * @memberOf LF.Widget.HorizontalVAS
     */
    // setPointer() {
    // let pointerWidth = this.unitCursor.getWidth() * 4,
    //     color = this.$('.vas-pointer').css('color'),
    // pointerHeight = 40;

    // Check if the pointer fits in canvas. If not, assign its width as the maximum available area.
    // pointerWidth = ((this.vasPosition.getLineStartX() < pointerWidth / 2) ? (this.vasPosition.getLineStartX() * 2) : (pointerWidth));

    // this.pointer = {
    /**
     * Get height of pointer
     * @return {number} Pointer height
     * @memberOf LF.Widget.HorizontalVAS.pointer
     * @example this.pointer.height;
     */
    // getHeight: function () {
    //     return pointerHeight;
    // },
    /**
     * Get width of pointer
     * @return {number} Pointer width
     * @memberOf LF.Widget.HorizontalVAS.pointer
     * @example this.pointer.getWidth();
     */
    // getWidth: function () {
    //     return pointerWidth;
    // },
    /**
     * Get color of pointer
     * @return {String} Hexadecimal code of color
     * @memberOf LF.Widget.HorizontalVAS.pointer
     * @example this.pointer.getColor();
     */
    // getColor: function () {
    //     return color;
    // }
    // };
    // }
    // ;

    /**
     * This method create a vas by calculating all its components sizes and position points based on vas
     *     functionality standards documentation.
     * @param {number} canvasWidth Pixel size of canvas width
     * @example this.createVAS(this.$('canvas:first').width());
     */
    createVAS (canvasWidth) {
        let firstUnitPixelSize;

        if (this.unitPixelSize === 1) {
            firstUnitPixelSize = 1;
        } else {
            firstUnitPixelSize = Math.floor(this.unitPixelSize / 2);
        }

        this.vasLine.getUnitPixelSize = () => {
            return this.unitPixelSize;
        };

        this.vasLine.getFirstUnitPixelSize = () => {
            return firstUnitPixelSize;
        };

        this.setVasAnchor();
        this.setUnitCursor();
        this.setVasPosition(canvasWidth);

        // this.setPointer();
    }

    render () {
        this.$el.addClass('hvas');
        return super.render();
    }

    /**
     * This method renders a scale containing left anchor, right anchor and line
     * @example this.renderScale();
     */
    renderScale () {
        // Draw left anchor
        this.ctxVas.fillRect(this.vasPosition.getLeftAnchorX(), this.vasPosition.getLeftAnchorY(), this.anchors.width, this.anchors.height);

        // Draw right anchor
        this.ctxVas.fillRect(this.vasPosition.getRightAnchorX(), this.vasPosition.getRightAnchorY(), this.anchors.width, this.anchors.height);

        // Draw vas line
        this.ctxVas.fillRect(this.vasPosition.getLineStartX(), this.vasPosition.getLineStartY(), this.vasLine.width, this.vasLine.height);
    }

    /**
     * This method renders unit cursor in a particular point on canvas
     * @param {number} x  X axis of point
     * @example this.renderUnitCursor(100);
     */
    renderUnitCursor (x) {
        // LEFT OFF AT -> See if the new StrikeMark property for height and width that you defined in BaseVas.js work.
        //     If so, implement the same thing in Vertical Vas.

        // Draw unit cursor
        this.ctxUnitCursor.fillRect(this.vasPosition.setCursorStartX(x),
            this.vasPosition.getCursorStartY(),
            this.strikeMark.width, // unitCursor.getWidth(),
            this.strikeMark.height); // this.unitCursor.getHeight());

        this.ctxUnitCursor.fillStyle = '#fff';

        // Clear the center pixel of unit cursor
        this.ctxUnitCursor.fillRect(x, this.vasPosition.getCursorStartY(), 1, this.strikeMark.height);

        this.ctxUnitCursor.fillStyle = '#000';
    }

    get minPointerHeight () {
        // eslint-disable-next-line new-cap
        let _minHeight = this.MeasureText(this.anchors.max.value, true, this.anchors.font.name, 20);
        return _minHeight;
    }

    /**
     * This method renders pointer
     * @param {number} x  X axis of point
     * @example this.renderPointer(100);
     */
    renderPointer (x) {
        if (this.vasConfig.pointer) {
            let yStart,
                yEnd,
                fourthCursHeight;

            if (this.vasConfig.labelPosition === 'below') {
                yStart = this.vasPosition.getCursorEndY() + 8;
                yEnd = this.vasPosition.getCursorEndY() + this.cursorWidth * 2;
                fourthCursHeight = (yEnd - yStart) * 0.25;

                if (yEnd > this.vasCanvas.height) {
                    yEnd = this.vasCanvas.height - 4;
                }

                yEnd = yEnd < (yStart + this.anchors.height) ? this.anchors.height + yStart : yEnd;
            } else {
                // OK, the label position is above.  Draw the same thing, but upside-down, anchored to the top
                yStart = this.vasPosition.getCursorStartY() - 8;
                yEnd = 0;
            }

            // Note, this is negative if the pointer is upside down.
            fourthCursHeight = (yEnd - yStart) * 0.25;

            // Use ABS of fourthCursHeight, so that the extra height is always applied to the direction of the text.
            this.postingYLocation = (yEnd - fourthCursHeight - yStart) / 2 + yStart + fourthCursHeight;

            this.ctxUnitCursor.fillStyle = this.pointer.color;
            this.ctxUnitCursor.strokeStyle = '#000000';
            this.ctxUnitCursor.beginPath();
            this.ctxUnitCursor.moveTo(x, yStart);
            this.ctxUnitCursor.lineTo(x - this.cursorWidth / 2, yStart + fourthCursHeight);
            this.ctxUnitCursor.lineTo(x - this.cursorWidth / 2, yEnd);
            this.ctxUnitCursor.lineTo(x + this.cursorWidth / 2, yEnd);
            this.ctxUnitCursor.lineTo(x + this.cursorWidth / 2, yStart + fourthCursHeight);
            this.ctxUnitCursor.lineTo(x, yStart);
            this.ctxUnitCursor.fill();
            this.ctxUnitCursor.stroke();
        }
    }

    /**
     * This method draws labels based on the configurations.
     * @example this.drawArrows();
     */
    drawArrows () {
        let margin = 3,
            arrowHeadHeight = 12,
            context,
            startY,
            endY,
            arrowHeadStartY,
            arrowHeadEndY;

        if (this.vasConfig.labelArrow) {
            if (this.vasConfig.labelPosition === 'above') {
                context = this.ctxArrows;
                startY = 0;
                endY = this.arrowsCanvas.height - margin;
                arrowHeadStartY = endY;
                arrowHeadEndY = this.arrowsCanvas.height - arrowHeadHeight;
            } else {
                context = this.ctxVas;
                startY = this.vasPosition.getAnchorEndY() + margin;
                endY = this.vasCanvas.height;
                arrowHeadStartY = this.vasPosition.getAnchorEndY() + margin;
                arrowHeadEndY = this.vasPosition.getAnchorEndY() + arrowHeadHeight;
            }

            this.renderArrows(context, startY, endY, arrowHeadStartY, arrowHeadEndY);
        }
    }

    /**
     * This method renders left or right arrow.
     * @param {Object} ctx Canvas context where arrows are rendered
     * @param {number} startY  Y axis of arrow line start point
     * @param {number} endY  Y axis of arrow line end point
     * @param {number} arrowHeadStartY Y axis of arrow head start point
     * @param {number} arrowHeadEndY Y axis of arrow head end point
     * @example this.renderArrows(this.ctxArrows, 0, 50, 50, 75);
     */
    renderArrows (ctx, startY, endY, arrowHeadStartY, arrowHeadEndY) {
        // draw left arrow
        ctx.beginPath();
        ctx.moveTo(this.vasPosition.getLineStartX(), startY);
        ctx.lineTo(this.vasPosition.getLineStartX(), endY);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(this.vasPosition.getLineStartX(), arrowHeadStartY);
        ctx.lineTo(this.vasPosition.getLineStartX() - this.strikeMark.width, arrowHeadEndY);
        ctx.lineTo(this.vasPosition.getLineStartX() + this.strikeMark.width, arrowHeadEndY);
        ctx.closePath();
        ctx.fill();

        // draw right arrow
        ctx.beginPath();
        ctx.moveTo(this.vasPosition.getLineEndX(), startY);
        ctx.lineTo(this.vasPosition.getLineEndX(), endY);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(this.vasPosition.getLineEndX(), arrowHeadStartY);
        ctx.lineTo(this.vasPosition.getLineEndX() - this.strikeMark.width, arrowHeadEndY);
        ctx.lineTo(this.vasPosition.getLineEndX() + this.strikeMark.width, arrowHeadEndY);
        ctx.closePath();
        ctx.fill();
    }

    /**
     * This method renders numeric labels
     * @example this.renderNumericLabels();
     */
    renderNumericLabels () {
        let numericLabelPad = this.vasConfig.labelPosition === 'above' ? 4 : -4;

        this.ctxNumLabels.textBaseline = 'bottom';
        this.ctxNumLabels.font = `${this.anchors.font.size}px ${this.anchors.font.name}`;
        this.ctxNumLabels.fillStyle = this.anchors.font.color;

        this.ctxNumLabels.textAlign = 'center';
        this.ctxNumLabels.fillText(!this.anchors.swapMinMaxLocation ? this.anchors.min.value : this.anchors.max.value,
            this.vasPosition.getLineStartX(),
            this.numLabelCanvas.height + numericLabelPad);
        this.ctxNumLabels.fillText(!this.anchors.swapMinMaxLocation ? this.anchors.max.value : this.anchors.min.value,
            this.vasPosition.getLineEndX(),
            this.numLabelCanvas.height + numericLabelPad);
    }

    /**
     *  @param {number} x - The current x position.
     *  @returns {number}
     */
    getVASNumericValue (x) {
        let xval = x - this.vasPosition.getLineStartX();
        let val = Math.round(xval / this.tickSize) + parseFloat(this.anchors.min.value);

        if (this.anchors.swapMinMaxLocation) {
            val = this.anchors.max.value - val;
        }
        return val;
    }

    /**
     * This method renders numeric value on canvas if it is enabled
     * @param {number} value  The current value of the pointer.
     * @param {number} xpos - The x position of the pointer.
     * @example this.renderPostingValue(99, 99);
     */
    renderPostingValue (value, xpos) {
        let that = this,

            /**
             * renders the selected value in one location on screen
             */
            renderStatic = () => {
                that.ctxNumLabels.strokeStyle = this.selectedValue.selectionBox.borderColor;
                that.ctxNumLabels.fillStyle = this.selectedValue.selectionBox.fillColor;

                // get the height and width of the current value
                // eslint-disable-next-line new-cap
                let ht = that.MeasureText(value, false, that.selectedValue.font.name,
                    that.selectedValue.font.size).height;
                // eslint-disable-next-line new-cap
                let wdt = that.MeasureText(value, false, that.selectedValue.font.name,
                    that.selectedValue.font.size).width;

                // DE17776. At max value, value is usually physically larger than lesser values, and
                // if so caused incorrect re-rendering of the display box. This should prevent that.
                // eslint-disable-next-line new-cap
                let maxWidth = that.MeasureText(that.anchors.max.value, false, that.selectedValue.font.name, that.selectedValue.font.size).width;

                // if the max is larger than max-1, e.g. 100 vs. 99 or 10 vs. 9, use non-max width.
                // eslint-disable-next-line new-cap
                let maxMinusOne = that.MeasureText(that.anchors.max.value - 1, false, that.selectedValue.font.name, that.selectedValue.font.size).width;
                if ((maxWidth === wdt) && (maxWidth > maxMinusOne)) {
                    wdt = maxMinusOne;
                }

                // set the answer size to whichever is larger...the height or the width
                let selectedAnswerSize = ht > wdt ? ht + 10 : wdt + 10;

                // sets the line width of the box that surrounds the selected value
                that.ctxNumLabels.lineWidth = that.selectedValue.selectionBox.borderWidth;

                // draws a solid rectangle for the selected value box
                that.ctxNumLabels.fillRect(Math.round(that.numLabelCanvas.width / 2) - (selectedAnswerSize / 2),
                    Math.round(that.numLabelCanvas.height / 2) - (selectedAnswerSize / 2), selectedAnswerSize, selectedAnswerSize);

                // draws another rectangle for the border
                that.ctxNumLabels.rect(Math.round(that.numLabelCanvas.width / 2) - (selectedAnswerSize / 2),
                    Math.round(that.numLabelCanvas.height / 2) - (selectedAnswerSize / 2), selectedAnswerSize, selectedAnswerSize);

                that.ctxNumLabels.font = this.fontString(this.selectedValue.font);
                that.ctxNumLabels.fillStyle = that.selectedValue.font.color;
                that.ctxNumLabels.textBaseline = 'middle';
                that.ctxNumLabels.textAlign = 'center';

                // draw the selected value to the screen
                that.ctxNumLabels.fillText(value, Math.round(that.numLabelCanvas.width / 2), that.numLabelCanvas.height / 2);
                that.ctxNumLabels.stroke();
                that.ctxNumLabels.closePath();
            },

            /**
             * renders the selected value next to the current pointer/cursor
             */
            renderDynamic = () => {
                that.ctxUnitCursor.beginPath();
                that.ctxUnitCursor.font = that.fontString(that.selectedValue.font);
                that.ctxUnitCursor.fillStyle = that.selectedValue.font.color;
                that.ctxUnitCursor.textBaseline = 'middle';
                that.ctxUnitCursor.textAlign = 'center';
                that.ctxUnitCursor.fillText(value, xpos, that.postingYLocation);
                that.ctxUnitCursor.closePath();
            };

        if (this.answerTextWidth === undefined) {
            this.answerTextWidth = this.ctxVas.measureText(value);
        }

        this.ctxVas.fillStyle = '#fff';
        this.ctxVas.textBaseline = 'middle';

        if (this.selectedValue.isVisible) {
            if (this.selectedValue.location === 'static') {
                renderStatic();
            } else if (this.selectedValue.location === 'dynamic') {
                renderDynamic();
            } else if (this.selectedValue.location === 'both') {
                renderStatic();
                renderDynamic();
            } else {
                renderStatic();
            }
        }

        if (xpos !== undefined) {
            this.saveAnswer(value, xpos);
        }
    }

    /**
     * This method clears canvas
     * @example this.clearCanvas();
     */
    clearCanvas () {
        this.unitCursorCanvas.width = this.canvasWidth;
    }

    /**
     * This method checks if a point is inside or around vas area on canvas.
     * Note:  This
     * @param {number} pageX  X axis of event
     * @param {number} pageY  Y axis of event
     * @example this.checkUnitCursorMove(100, 200);
     */
    checkUnitCursorMove (pageX, pageY) {
        let $vasCanvas = $(this.vasCanvas),
            $cursorCanvas = $(this.unitCursorCanvas);

        let mainPos = $vasCanvas.offset(),
            cursorPos = $cursorCanvas.offset(),
            mainHeight = $cursorCanvas.outerHeight(),
            cursorHeight = $cursorCanvas.outerHeight();

        let minY = Math.min(mainPos.top, cursorPos.top),
            maxY = Math.max(mainPos.top + mainHeight, cursorPos.top + cursorHeight);

        this.unitCursorMove = false;

        if (pageY > minY && pageY < maxY) {
            this.unitCursorMove = true;
        }
    }

    /**
     * This method moves unit cursor to a point on vas
     * @param {number} x X axis of point
     * @param {boolean} respond Determines if the response should be saved.
     * @param {boolean} postValue  Determines if the value should be posted
     * @example this.moveUnitCursor(124, true);
     */
    moveUnitCursor (x, respond, postValue = true) {
        let respondValue;

        if (this.unitCursorMove) {
            if (x <= this.vasPosition.getLineStartX()) {
                x = this.vasPosition.getLineStartX();
            } else if (x >= this.vasPosition.getLineEndX()) {
                x = this.vasPosition.getLineEndX();
            }
            this.clearCanvas();
            this.renderUnitCursor(x);
            this.renderPointer(x);

            respondValue = this.getVASNumericValue(x);
            if (postValue) {
                this.renderPostingValue(respondValue, x);
            }
            if (!this.answers.size()) {
                this.answer = this.addAnswer();
            }

            if (respond) {
                this.respondHelper(this.answer, respondValue.toString());
            }
        }
    }

    /**
     * This method calculates the x axis of unit cursor for a numeric value
     * @param {number} value Numeric value of vas (0 - 100)
     * @return {number} x axis of the unit cursor point
     * @example this.getCursorPoint(44);
     */
    getCursorPoint (value) {
        // Determine 0-100 factor for this value
        let x = (value - this.anchors.min.value) * this.tickSize;

        if (this.anchors.swapMinMaxLocation) {
            x = this.vasPosition.getLineEndX() - x;
        } else {
            x = x + this.vasPosition.getLineStartX();
        }

        if (x > this.vasPosition.getLineEndX()) {
            return this.vasPosition.getLineEndX();
        } else if (x < this.vasPosition.getLineStartX()) {
            return this.vasPosition.getLineStartX();
        }
        return x;
    }

    /**
     * Handles all touch and mouse movements.
     * @param {Event} e Event Object
     */
    vmouseHandler (e) {
        let canvasX;

        let pageX = this.getPageX(e);
        let pageY = this.getPageY(e);
        let canvasOffset = $(this.vasCanvas).offset();

        // Disable the default browser behaviour
        e.preventDefault();

        switch (e.type) {
            case 'mousemove':
            case 'touchmove':
                canvasX = pageX - canvasOffset.left;
                this.moveUnitCursor(Math.floor(canvasX), true);
                break;
            case 'mousedown':
            case 'touchstart':
                this.checkUnitCursorMove(pageX, pageY);
                canvasX = pageX - canvasOffset.left;
                this.moveUnitCursor(Math.floor(canvasX), true);
                break;
            case 'mouseup':
            case 'mouseleave':
                this.unitCursorMove = false;
                break;

            // No default
        }
    }

    mouseMoveHandler (e, mousedown) {
        if (mousedown) {
            return this.vmouseHandler(e);
        }
        return false;
    }

    destroy () {
        $('#questionnaire').removeClass('HVAS');
    }
}

window.LF.Widget.HorizontalVAS = HorizontalVAS;
