/**
 * Created by mark.matthews on 8/20/2016.
 */
export const defaultPointerColor = '#00ACE6';

export default class Pointer {
    constructor (vasModel) {
        let _isVisible = false,
            _displayInitially = false,
            _location,
            _color = defaultPointerColor;

        Object.defineProperty(this, 'isVisible', {
            get: () => {
                return _isVisible;
            }
        });

        Object.defineProperty(this, 'displayInitially', {
            get: () => {
                return _displayInitially;
            }
        });

        Object.defineProperty(this, 'location', {
            get: () => {
                return _location;
            }
        });

        Object.defineProperty(this, 'color', {
            get: () => {
                return _color;
            }
        });

        if (vasModel !== undefined) {
            if (vasModel.has('pointer')) {
                let tmpPointer = vasModel.get('pointer'),
                    pointerIsVisible = tmpPointer.isVisible,
                    pointerLocation = tmpPointer.location,
                    pointerDisplayInitially = tmpPointer.displayInitially,
                    pointerColor = tmpPointer.color;

                _isVisible = pointerIsVisible === undefined ? _isVisible : pointerIsVisible;

                if (pointerLocation !== undefined) {
                    _location = pointerLocation;
                }

                _displayInitially = pointerDisplayInitially === undefined ? _displayInitially : pointerDisplayInitially;
                _color = pointerColor === undefined ? _color : pointerColor;
            }
        }
    }
}
