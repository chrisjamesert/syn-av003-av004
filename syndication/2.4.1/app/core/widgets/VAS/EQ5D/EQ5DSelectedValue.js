import SelectedValue from '../CommonProperties/SelectedValue';

/**
 * Created by mark.matthews on 8/21/2016.
 */
export default class EQ5DSelectedValue extends SelectedValue {
    constructor (vasModel) {
        super(vasModel);

        // if the configuration has defined a font, then read that in instead
        if (vasModel !== undefined && vasModel.has('selectedValue')) {
            // eslint-disable-next-line
            let tmpSelectedValue = vasModel.get('selectedValue');
        }
    }
}
