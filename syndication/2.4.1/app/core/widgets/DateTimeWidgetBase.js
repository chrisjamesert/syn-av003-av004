/** #depends Base.js
 * @file Defines a base date picker widget.
 * @author <a href="mailto:dimitar.vukman@ert.com">Dimitar Vukman</a>
 * @version 1.0
 */

import WidgetBase from './WidgetBase';

// Core Modules
import { Banner } from 'core/Notify';

export default class DateTimeWidgetBase extends WidgetBase {
    get config () {
        let val = this.model.get('configuration');
        if (val) {
            return val;
        }
        this.model.set('configuration', {});
        return this.model.get('configuration');
    }

    constructor (options) {
        super(options);

        /**
         * Array of strings for the month values which studyworks expects when accepting data
         * @readonly
         * @type String[]
         */
        this.swMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        /**
         * Default configuration options
         * @readonly
         * @type Object
         */
        this.defaultConfig = {
            useAnimation: false,
            useFocus: true,
            themeDatePick: 'z',
            themeHeader: 'b',
            useHeader: false,
            centerHoriz: true,
            centerVert: true,
            useModal: true,
            useNewStyle: true
        };

        /**
         * The template of the DatePicker input element
         * @type String
         * @default 'DEFAULT:DatePicker'
         */
        this.datePicker = 'DEFAULT:DatePicker';

        /**
         * The template of the DatePicker input Label
         * @type String
         * @default 'DEFAULT:DatePickerLabel'
         */
        this.datePickerLabel = 'DEFAULT:DatePickerLabel';

        /**
         * The template of the TimePicker input element
         * @type String
         * @default 'DEFAULT:TimePicker'
         */
        this.timePicker = 'DEFAULT:TimePicker';

        /**
         * The template of the TimePicker input Label
         * @type String
         * @default 'DEFAULT:TimePickerLabel'
         */
        this.timePickerLabel = 'DEFAULT:TimePickerLabel';

        /**
         * DatePicker widget events
         * @readonly
         * @enum {Event}
         */
        this.events = {

            /** Date box input change event */
            'change input[data-role="datebox"]': 'respond',

            /** Date box input focus event */
            'focus input[data-role="datebox"]': 'clearBanner'
        };
    }

    /**
     * Reset configuration function.
     * This method is run in the render method of the widgets extended from this base widget.
     * Main purpose is to reset the default language which could have changed due to context switching
     */
    resetConfig () {
        let returnedDates = _.extend(LF.strings.dates({ dates: {} }), LF.strings.dates({ dateConfigs: {} })),
            dateLang = { default: returnedDates };

        // Set the isRTL property based on the direction property in core strings. direction can be "rtl" or "ltr"
        // dateLang['default'].isRTL = (LF.strings.getLanguageDirection() === 'rtl');
        // _.defaults(config, this.defaultConfig);
        if (returnedDates) {
            this.config.lang = dateLang;
        }
    }

    /**
     * This method binds questionnaire:resize event for Datepicker datebox
     */
    bindResize () {
        // If browser is resized, render the datebox again to correct popup placement on orientation change
        $('#questionnaire').bind('questionnaire:resize', () => {
            $(window).trigger('resize.datebox');
        });
    }

    /**
     * Clear all the banner notification
     * @example this.clearBanner();
     */
    clearBanner () {
        Banner.closeAll();
    }
}

window.LF.Widget.DateTimeWidgetBase = DateTimeWidgetBase;
