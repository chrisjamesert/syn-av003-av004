/** #depends WidgetBase.js
 * @file Defines the language selection widget
 * This is a simple extension of the standard RadioButton widget that simply loads the participant languages from the study design as the widget answers.
 * @author <a href="mailto:brian.janaszek@ert.com">bjanaszek</a>
 * @version 1.0
 */
import RadioButton from './RadioButton.js';

export default class PatientLangListRadioButton extends RadioButton {
    // noinspection JSMethodCanBeStatic
    get className () {
        return 'RadioButton';
    }

    // noinspection JSMethodCanBeStatic
    get container () {
        return 'DEFAULT:VerticalButtonGroup';
    }

    // noinspection JSMethodCanBeStatic
    get wrapper () {
        return 'DEFAULT:RadioButtonWrapper';
    }

    // noinspection JSMethodCanBeStatic
    get input () {
        return 'DEFAULT:RadioButton';
    }

    // noinspection JSMethodCanBeStatic
    get label () {
        return 'DEFAULT:RadioButtonLabel';
    }

    /**
     * @param {Object} options The options used to generate the class.
     */
    constructor (options) {
        this.options = options;
        super(options);

        this.loadAnswers();
    }

    /**
     * Loads the model's answers property via LF.StudyDesign.participantSettings.languageList.
     */
    loadAnswers () {
        let answers = LF.StudyDesign.participantSettings.languageList,
            modelAnswers = [];

        // eslint-disable-next-line no-unused-vars
        _.each(answers, (item, index) => {
            let lang = `${item.language}-${item.locale}`;

            modelAnswers.push({
                text: item.localized,
                value: lang
            });
        });

        this.model.set({ answers: modelAnswers });
    }
}

window.LF.Widget.PatientLangListRadioButton = PatientLangListRadioButton;
