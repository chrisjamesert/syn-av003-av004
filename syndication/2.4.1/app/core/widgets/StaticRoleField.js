import StaticField from './StaticField';

/**
 * Displays a static role text field.
 * Note: The answer record must be pre-populated prior to render.
 * @example
 * {
 *   id          : 'ACTIVATE_USER_ROLE',
 *   IG          : 'Activate_User',
 *   IT          : 'ACTIVATE_USER_2',
 *   className   : 'ACTIVATE_USER',
 *   widget      : {
 *       id              : 'ACTIVATE_USER_W_2',
 *       type            : 'StaticRoleField',
 *       label           : 'ROLE',
 *       field           : 'role'
 *   }
 * }
 */
export default class StaticRoleField extends StaticField {
    constructor (options) {
        super(options);

        // We always want the role's display name to be translated.
        this.model.set('translatable', true);
    }

    /**
     * The text to display when rendering the widget.
     * @returns {string} The text to display.
     */
    get displayText () {
        let field = this.model.get('field');
        let id = JSON.parse(this.answer.get('response'))[field];

        let role = LF.StudyDesign.roles.findWhere({ id });

        // DE17443 - If the role does not exist, just display an empty string.
        return role != null ? role.get('displayName') : '';
    }
}

window.LF.Widget.StaticRoleField = StaticRoleField;
