/** #depends CustomRadioButton.js
 * @file Defines a Numeric Rating Scale (NRS) widget.
 * @author <a href="mailto:reldridge@phtcorp.com">Ryan Eldridge</a>
 * @version 1.6
 */

import RadioButton from './RadioButton';

// - Core Modules
import Widget from 'core/models/Widget';

class NumericRatingScale extends RadioButton {
    // noinspection JSMethodCanBeStatic
    get className () {
        return 'NumericRatingScale';
    }

    // noinspection JSMethodCanBeStatic
    get container () {
        return 'DEFAULT:NRSButtonGroup';
    }

    /**
     * Constructs a NumericRatingScale widget.
     * @param {Object} options The options used to build the widget.
     * @param {Object} options.model The model that contains the specific configuration for the widget, represented by the "widget" property of the question configuration in the study design.
     * @param {number} options.model.id {dev-only} The ID assigned to the widget.
     * @param {string} options.model.className {dev-only} The CSS classname assigned to the widget.
     * @param {boolean} [options.model.reverseOnRtl=false] {dev-only} Indicates whether the scale should be reversed when displaying in an RtL language.
     * @param {string} [options.model.width=100] {dev-only} The overall width of the scale.
     * @param {Object} [options.model.markers] {dev-only} Contains information about the various markers (left, right, arrow, position) that can be rendered with scale.
     * @param {string<translated>} [options.model.markers.left] The string to display at the left end of the scale.
     * @param {string<translated>} [options.model.markers.right] The string to display on the right end of the scale.
     * @param {boolean} [options.model.markers.arrow=true] Indicates whether an arrow should be drawn from the end of the scale to the marker text at each end of the scale.
     * @param {string} [options.model.markers.justification=center] (inside|outside|center) Indicates the justification to be used for the markers.
     * @param {string} [options.model.markers.position=below] (above|below) Indicates the location of the markers relative to the scale.
     * @param {number} [options.model.markers.spaceAllowed=2] amount of buttons the marker text can expand to.
     * @param {Array} options.model.answers The collection of answers used to build the buttons on the scale.  Each element should be an object containing a "text" property and a "value" property (i.e. <code>{ text: 'NRS_1', value: '0' }</code>)
     * @param {Object} [options.model.templates] {dev-only} Contains any templates that can be used to override the defaults used by the widget.
     * @param {string} [options.model.templates.container=DEFAULT:NRSButtonGroup] {dev-only} The default container template for the widget.
     * @param {string} [options.model.templates.markers=DEFAULT:NRSMarkers] {dev-only} The template used to render the markers for the scale.
     */
    constructor (options) {
        /**
         * The template of the marker element
         * @type String
         * @default 'DEFAULT:NRSMarkers'
         */
        this.markers = 'DEFAULT:NRSMarkers';

        this.options = options;
        super(options);

        this.config = this.getConfig();

        this.$container = null;
    }

    /**
     * Create configuration object
     * @return {Object} configuration object
     */
    getConfig () {
        let markers,
            answers;

        // If configured to, reverse entire scale in right-to-left languages
        if (this.model.get('reverseOnRtl') && LF.strings.getLanguageDirection() === 'rtl') {
            // Create deep copy of this.model so that we can change it for this instance of this view only
            this.model = new Widget(JSON.parse(JSON.stringify(this.model.toJSON())));
            markers = this.model.get('markers');
            answers = this.model.get('answers');

            // Flip marker values and reverse answers array
            markers.right = [markers.left, markers.left = markers.right][0];
            answers = answers.reverse();

            // Tell markers to ignore their rtl directional stylings
            this.$el.addClass('ignore-rtl');
        }

        return {
            markers: markers || this.model.get('markers'),
            answers: answers || this.model.get('answers')
        };
    }

    /**
     * This method binds questionnaire:resize event for NRS
     */
    bindResize () {
        // If browser is resized, render the NRS again
        $('#questionnaire').bind('questionnaire:resize', _(this.resizeHandler).bind(this));
    }

    /**
     * The handler triggered when resize events are fired on the application.
     */
    resizeHandler () {
        if (this.resizeTimeout) {
            clearTimeout(this.resizeTimeout);
        }
        this.resizeTimeout = setTimeout(() => {
            this.render();
        }, 100);
    }

    /**
     * Adjusts NRS and container field
     * @returns {Q.Promise} when adjustment is finished
     */
    setNRS () {
        let width;

        this.$container = this.$('[data-container]').first();

        this.$container.prepend('<div class="NRS-Placeholder left"></div>');
        this.$container.append('<div class="NRS-Placeholder right"></div>');

        // Width of the fieldset container in percent of the parent element or in exact pixels
        width = this.model.get('width') || '100';

        return Q.Promise((resolve) => {
            if (width.match('px')) {
                this.$container.css('width', width);
            } else {
                this.$container.css('width', `${width}%`);
            }

            this.$container.ready(resolve);
        });
    }

    /**
     * Displays the text markers
     * @returns {Q.Promise}
     */
    displayMarkers () {
        return new Q.Promise((resolve) => {
            let templates = this.model.get('templates') || {},
                markersTmpl = templates.markers || this.markers,
                that = this;

            LF.getStrings({
                textLeft: {
                    key: that.config.markers.left,
                    namespace: that.question.getQuestionnaire().id
                },
                textRight: {
                    key: that.config.markers.right,
                    namespace: that.question.getQuestionnaire().id
                }
            }, (strings) => {
                if (that.config.markers.position === 'above') {
                    that.$el.prepend(LF.templates.display(markersTmpl, {
                        left: strings.textLeft,
                        right: strings.textRight
                    })).ready(resolve);
                } else {
                    that.$el.append(LF.templates.display(markersTmpl, {
                        left: strings.textLeft,
                        right: strings.textRight
                    })).ready(resolve);
                }
            });
        });
    }

    /**
     * Adjusts the marker positions based on the justification and size of the NRS
     * @returns {Q.Promise}
     */
    adjustMarkers () {
        let
            nrsWidth = this.$('.NRS-Container').outerWidth(),

            // text justification set in study.demo
            justification = this.config.markers.justification || 'center',

            // amount of buttons the marker text can expand to
            textMarkerLength = this.config.markers.spaceAllowed || 2,

            showArrows = typeof this.config.markers.arrow !== 'boolean' ? true : this.config.markers.arrow,

            // specific size of the textMarkerLength
            maxMarkerWidth;

        // to avoid overlap
        if (this.$('.btn').length <= textMarkerLength * 2) {
            maxMarkerWidth = this.$container.outerWidth() / 2;
        } else {
            maxMarkerWidth = this.$('.btn').outerWidth() * textMarkerLength;
        }

        switch (justification) {
            case 'inside':
                this.$('.markers').addClass('inside');
                this.$('.markers').addClass(this.config.markers.position);
                this.$('.markers').css('width', nrsWidth);

                this.$('.markers .marker-left').css('max-width', maxMarkerWidth);
                this.$('.markers .marker-right').css('max-width', maxMarkerWidth);

                break;

            case 'center': {
                this.$('.markers').addClass('center');
                this.$('.NRS-Placeholder').width(maxMarkerWidth);
                nrsWidth = nrsWidth + maxMarkerWidth * 2;
                this.$('.NRS-Container').width(nrsWidth);

                this.$('.markers').css('width', nrsWidth);

                // recalculate nrsWidth and maxMarkerWidth now that things have changed.
                let oldMaxMarkerWidth = maxMarkerWidth;
                maxMarkerWidth = this.$('.btn').outerWidth() * textMarkerLength;

                this.$('.markers').css('padding-left', oldMaxMarkerWidth - maxMarkerWidth);
                this.$('.markers').css('padding-right', oldMaxMarkerWidth - maxMarkerWidth);

                this.$('.markers .marker-left').css('width', maxMarkerWidth * 2);
                this.$('.markers .marker-right').css('width', maxMarkerWidth * 2);

                break;
            }
            case 'outside':
                nrsWidth = nrsWidth + maxMarkerWidth * 2;
                this.$('.NRS-Container').width(nrsWidth);

                if (this.config.markers.position === 'middle') {
                    this.$('.NRS-Placeholder.left').append(this.$('.markers .marker-left'));
                    this.$('.NRS-Placeholder.right').append(this.$('.markers .marker-right'));
                } else {
                    this.$('.NRS-Placeholder').width(maxMarkerWidth);
                    this.$('.markers .marker-left').css('width', maxMarkerWidth);
                    this.$('.markers .marker-right').css('width', maxMarkerWidth);
                    this.$('.markers').css('width', nrsWidth);
                }

                this.$('.markers').addClass('outside');
                break;

            default:
                break;
        }
        if (showArrows) {
            let arrowDirection = false;
            switch (this.config.markers.position) {
                case 'above':
                    arrowDirection = 'down';
                    break;
                case 'below':
                    arrowDirection = 'up';
                    break;

                // Never show arrows if "Middle" (on same row as NRS)
                // No default
            }
            arrowDirection && this.$(`.arrow-container.${arrowDirection}`).css('display', 'block');
        }

        return Q();
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise}
     */
    render () {
        return super.render()
        .then(() => this.setNRS())
        .then(() => this.displayMarkers())
        .then(() => this.adjustMarkers());
    }
}


window.LF.Widget.NumericRatingScale = NumericRatingScale;
export default NumericRatingScale;
