import TextBox from './TextBox';
import { Banner } from '../Notify';
import { makeWindowsNumericInputCover, getNested } from '../utilities/coreUtilities';

/**
  * @file Defines a text box for confirmation
  * @version 1.0
  */
export default class ConfirmationTextBox extends TextBox {
    /**
     * Constructor
     * @param {Object} options An object containing options
     */
    constructor (options) {
        this.options = options;
        super(options);
        this.validation = this.model.get('validation');
        this.isUserTextBox = true;
        this.events = {
            'input input': 'keyPressed',
            'focus input': 'onFocus'
        };
        this.fieldToConfirmWidget = this.questionnaire.questionViews[this.questionnaire.questionViews.length - 1].widget;
    }

    /**
     * Responsible for displaying the widget
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.input,
            toTranslate = {};

        this.$el.empty();

        // Optionally populate the toTranslate object.
        this.model.get('label') && (toTranslate.label = this.model.get('label'));
        this.model.get('placeholder') && (toTranslate.placeholder = this.model.get('placeholder'));

        return this.i18n(toTranslate)
        .then((strings) => {
            let wrapperElement,
                labelElement,
                textBoxElement;

            wrapperElement = this.renderTemplate(templates.wrapper || this.wrapper);

            // If a template label is configured, render it.
            if (strings.label) {
                labelElement = this.renderTemplate(templates.label || this.label, {
                    link: this.model.get('id'),
                    text: strings.label
                });
            }
            let inputType = this.model.get('getRoleInputType') || 'text',
                obfuscateInput = getNested('model.attributes.configuration.obfuscate', this);

            if (inputType === 'text' && obfuscateInput) {
                inputType = 'password';
            }
            textBoxElement = this.renderTemplate(textBox, {
                id: this.model.get('id'),
                placeholder: strings.placeholder || '',
                name: `${this.model.get('id')}-${this.uniqueKey}`,
                className: this.model.get('className'),
                maxLength: this.model.get('maxLength'),
                inputType
            });

            // Append the wrapper to the local DOM and find where to append other elements.
            let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

            if (labelElement) {
                $wrapper.append(labelElement);
            }

            // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
            $wrapper.append(textBoxElement)
                .appendTo(this.$el);

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            this.value = this.displayText;

            // If the widget has a disabled property, disable the input element.
            if (this.model.get('disabled')) {
                this.$(`#${this.model.get('id')}`).attr('disabled', 'disabled');
            }

            this.delegateEvents();

            if (this.displayText.length > 0) {
                // Trigger the key-up event so that the answer is properly formatted:
                this.$(`#${this.model.get('id')}`).trigger('input');
            }

            if (LF.Wrapper.platform === 'windows' && inputType === 'number' && obfuscateInput) {
                makeWindowsNumericInputCover(this.model.get('id'), $wrapper);
            }
        });
    }

    /**
     *  Responds to a key press event.
     *  @param {Event} e Event data
     * @returns {boolean}
     */
    // eslint-disable-next-line consistent-return
    keyPressed (e) {
        let keyCode = e.which || e.keyCode;

        if (keyCode === 13 || keyCode === 10) {
            let $notDateboxInputs = $(':input').not('[data-role = "datebox"]'),
                numOfInputs = $notDateboxInputs.size(),
                nextIndex = $notDateboxInputs.index(e.target) + 1;

            if (nextIndex < numOfInputs) {
                $('input').blur();
                e.preventDefault();
                $notDateboxInputs.eq($notDateboxInputs.index(e.target) + 1).focus();
                return false;
            }

            this.questionnaire.nextHandler(e);
            return true;
        }
        let $target = $(e.target),
            passwordConfirmation = $target.val() + String.fromCharCode(keyCode),
            parent = $target.parent('.input-group'),
            tempPassword = $(`#${this.model.get('fieldToConfirm')}`).val(),
            doPasswordsMatch = passwordConfirmation.localeCompare(tempPassword) === 0,
            icon = doPasswordsMatch && !this.fieldToConfirmWidget.hasError ?
                '<span class="glyphicon glyphicon-ok-sign form-control-feedback" aria-hidden="true"></span>' :
                '<span class="glyphicon glyphicon-minus-sign form-control-feedback" aria-hidden="true"></span>',
            classesToAdd = doPasswordsMatch && !this.fieldToConfirmWidget.hasError ? 'has-feedback has-success' : 'has-feedback has-error';

        if (parent.hasClass('has-feedback')) {
            $target.siblings('.form-control-feedback').remove();
            $target.removeClass('ui-state-error-border');
            parent.removeClass('has-feedback has-success has-warning has-error');
        }

        this.hasError = this.fieldToConfirmWidget.hasError;

        parent.addClass(classesToAdd).append(icon);
        super.sanitize(e);
    }

    /**
     *  Handles user input on the widget.
     *  @param {Event} e Event data
     */
    respond (e) {
        let fieldToConfirm = this.model.get('fieldToConfirm'),
            $target = $(e.target),
            field = this.model.get('field'),
            value = $target.val(),
            hasError = this.hasError || this.fieldToConfirmWidget.hasError;

        this.completed = !hasError && value.localeCompare('') !== 0;
        this.isValid = value.localeCompare('') === 0 ? true : !hasError;

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        this.respondHelper(this.answer, JSON.stringify({ [field]: value, fieldToConfirm }), this.completed);
    }

    /**
     * removes error class and banners on focus
     */
    onFocus () {
        this.$el.find('input').removeClass('ui-state-error-border');
        Banner.closeAll();
    }

    /**
     * clears the textbox and removes the answer
     */
    clear () {
        this.completed = false;
        this.removeAnswer(this.answer);
        $(`#${this.model.get('id')}`).val('');
    }
}

ConfirmationTextBox.prototype.input = 'DEFAULT:LogpadPasswordTextBox';
window.LF.Widget.ConfirmationTextBox = ConfirmationTextBox;
