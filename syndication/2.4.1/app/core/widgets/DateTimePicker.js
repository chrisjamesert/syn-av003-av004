import DateTimeWidgetBase from './DateTimeWidgetBase';

class DateTimePicker extends DateTimeWidgetBase {
    get timeConfig () {
        let val = this.config.timeConfiguration;
        if (val) {
            return val;
        }
        this.config.timeConfiguration = {};
        return this.config.timeConfiguration;
    }

    /**
     * Constructs a DateTimePicker
     * @param {Object} options The options used to construct the class.
     * @param {Object} options.model The model that contains the widget configuration, as represented by the "widget" property for the question in the study design.
     * @param {string} options.model.id {dev-only} The ID to be associated with the widget.
     * @param {Date} [options.model.min] (DateTime) The minimum date and time accepted by the widget.
     * @param {Date} [options.model.max] (DateTime) The maximum date and time accepted by the widget.
     * @param {boolean} [options.model.showLabels=false] {dev-only} Indicates whether the picker labels are rendered.
     * @param {Object} [options.model.templates] {dev-only} Container to override default templates for rendering the widget.
     * @param {string} [options.model.templates.datePicker=DEFAULT:DatePicker] {dev-only} Container template for the date picker component.
     * @param {string} [options.model.templates.datePickerLabel=DEFAULT:DatePickerLabel] {dev-only} Template for the date picker label.
     * @param {string} [options.model.templates.timePicker=DEFAULT:TimePicker] {dev-only} Container template for the time picker.
     * @param {string} [options.model.templates.timePickerLabel=DEFAULT:TimePickerLabel] {dev-only} Template for the time picker label.
     * @param {Object} [options.model.configuration] {dev-only} Contains specific configuration settings for the DateBox library.  The full API is available to via <a href="http://dev.jtsage.com/jQM-DateBox/api/">here</a>.
     */
    constructor (options) {
        super(options);

        this.defaultConfig = {
            mode: 'calbox',
            datetimeMode: true,
            calUsePickers: 'true',
            calNoHeader: true,
            useFocus: true,
            calHighToday: false,
            calHighPick: false
        };

        this.defaultTimeConfig = _.defaults({
            mode: 'timebox',
            datetimeMode: true,
            useSetButton: true,
            useHeader: true,
            setTimeLabel: 'OK'
        }, this.defaultConfig);
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            datePicker = templates.datePicker || this.datePicker,
            datePickerLabel = templates.datePickerLabel || this.datePickerLabel,
            timePicker = templates.timePicker || this.timePicker,
            timePickerLabel = templates.timePickerLabel || this.timePickerLabel,
            datePickerElement,
            datePickerElementLabel,
            timePickerElement,
            timePickerElementLabel,
            showLabels,
            stringsToFetch,
            attributes = this.model.attributes,
            strDefaultMinTime = this.timeConfig.minTime || (this.timeConfig.minHour ? `${this.timeConfig.minHour}:00` : '00:00'),
            strDefaultMaxTime = this.timeConfig.maxTime || (this.timeConfig.maxHour ? `${this.timeConfig.maxHour}:59` : '23:59');

        _.defaults(this.config, this.defaultConfig);

        _.defaults(this.timeConfig, this.defaultTimeConfig);
        _.defaults(this.timeConfig, {
            minTime: strDefaultMinTime,
            maxTime: strDefaultMaxTime
        });

        this.resetConfig();

        if (this.config.defaultValue) {
            this.timeConfig.defaultValue = this.config.defaultValue;
        }

        if (this.config.lang) {
            this.timeConfig.lang = this.config.lang;
        }

        stringsToFetch = {
            dateLabel: 'DATE_LBL',
            timeLabel: 'TIME_LBL',
            setTimeLabel: this.timeConfig.setTimeLabel
        };

        return Q()
        .then(() => {
            showLabels = this.model.get('showLabels') && this.model.get('showLabels') === true;
            return LF.getStrings(stringsToFetch);
        })
        .then((strings) => {
            this.timeConfig.overrideSetTimeButtonLabel = strings.setTimeLabel;

            if (showLabels) {
                datePickerElementLabel = LF.templates.display(datePickerLabel, {
                    id: `${this.model.get('id')}_date_input`,
                    dateLabel: strings.dateLabel
                });
                timePickerElementLabel = LF.templates.display(timePickerLabel, {
                    id: this.model.get('id'),
                    timeLabel: strings.timeLabel
                });
            }

            datePickerElement = LF.templates.display(datePicker, {
                id: this.model.get('id'),
                min: this.config.min || attributes.min,
                max: this.config.max || attributes.max,
                configuration: _.escape(JSON.stringify(this.config))
            });
            timePickerElement = LF.templates.display(timePicker, {
                id: this.model.get('id')
            });

            this.$el.empty();
            if (showLabels) {
                this.$el.append(datePickerElementLabel);
            }
            this.$el.append(datePickerElement);
            if (showLabels) {
                this.$el.append(timePickerElementLabel);
            }
            this.$el.append(timePickerElement);

            // /If this has been answered, load back in input value
            if (this.localizedDate) {
                this.$('input.date-input').val(this.localizedDate);
            }
            if (this.localizedTime) {
                this.$('input.time-input').val(this.localizedTime);
            }
            this.$el.appendTo(this.parent.$el);

            this.delegateEvents();

            this.$el.trigger('create');

            // call datebox on input
            this.inputDate = this.$('input.date-input');
            this.inputDate.datebox(this.config);
            this.inputTime = this.$('input.time-input');
            this.inputTime.datebox(this.timeConfig);

            if (this.dateObj) {
                this.$('input.date-input').datebox('setTheDate', this.dateObj);
            }

            if (this.answers.size()) {
                this.answer = this.answers.at(0);
            }
        });
    }

    /**
     * Constructs StudyWorks formatted date string
     * @param {Date} date A date object to build StudyWorks string from
     * @returns {string} a StudyWorks formatted date string dd Mmm yyyy HH:mm:ss
     * @example this.buildStudyWorksString(new Date());
     * @example this.buildStudyWorksString(new Date(1995,11,17));
     */
    buildStudyWorksString (date) {
        // TODO: Use the utility function zeropad
        let swDate = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate(),
            swMonth = this.swMonths[date.getMonth()],
            swYear = date.getFullYear(),
            swHours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours(),
            swMinutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes(),
            swTime = `${swHours}:${swMinutes}:00`;

        return `${swDate} ${swMonth} ${swYear} ${swTime}`;
    }

    /**
     * Responds to user input.
     * @returns {Q.Promise<void>}
     */
    respond (e) {
        // TODO:  See if we can simplify this function, by using datebox to retrieve the time values.
        let dateInput = this.$('input.date-input'),
            timeInput = this.$('input.time-input'),
            timeInputVal = this.$('input.time-input').val(),
            date,
            meridiem = this.timeConfig.lang && this.timeConfig.lang.default ? this.timeConfig.lang.default.meridiem : ['AM', 'PM'],
            value,
            year,
            month,
            day,
            hours,
            minutes;

        // Get date from datebox only if we are responding to the calendar (not the time).
        // If the current target is the time, just get the cached dateObj copy and ensure it is floored.
        date = e && e.target === dateInput[0] ? dateInput.datebox('getTheDate') : new Date(this.dateObj);
        date.setHours(0, 0, 0, 0);

        // Store localized input into widget property to load back in
        this.localizedDate = dateInput.val();
        this.localizedTime = timeInputVal;

        if (timeInputVal !== '') {
            let splittedTimeStr = timeInputVal.split(':'),
                isAM = timeInputVal.indexOf(meridiem[0]) > 0,
                isPM = timeInputVal.indexOf(meridiem[1]) > 0;

            hours = parseInt(splittedTimeStr[0], 10);
            minutes = parseInt(splittedTimeStr[1], 10);

            if (isAM || isPM) {
                if (isPM && hours >= 1 && hours !== 12) {
                    hours += 12;
                } else if (isAM && hours === 12) {
                    hours = 0;
                }
            }
        }

        if (dateInput.val() !== '') {
            this.dateObj = date;
            year = date.getFullYear();
            month = date.getMonth();
            day = date.getDate();

            // When date is changed, update time input widget's date
            let minDateTime = this.config.min || this.model.attributes.min,
                timeInputDate = this.inputTime.datebox('getTheDate'),
                newTimeInputDate = date.copy(),
                strDefaultMinTime = this.timeConfig.minTime || (this.timeConfig.minHour ? `${this.timeConfig.minHour}:00` : '00:00'),
                strDefaultMaxTime = this.timeConfig.maxTime || (this.timeConfig.maxHour ? `${this.timeConfig.maxHour}:59` : '23:59');

            newTimeInputDate.setHours(timeInputDate.getHours());
            newTimeInputDate.setMinutes(timeInputDate.getMinutes());
            newTimeInputDate.setSeconds(timeInputDate.getSeconds());
            newTimeInputDate.setMilliseconds(timeInputDate.getMilliseconds());
            this.inputTime.datebox('ertSetTheDate', newTimeInputDate);

            // Check for min and max time attributes
            if (minDateTime) {
                let minYear,
                    minMonth,
                    minDay;
                if (minDateTime.indexOf('T') > -1) {
                    let parts = minDateTime.split('T'),
                        dateParts = parts[0].split('-'),
                        timeSubParts = parts[1].split(':');

                    minYear = Number(dateParts[0]);
                    minMonth = Number(dateParts[1]) - 1;
                    minDay = Number(dateParts[2]);
                    if (minDay === day && minMonth === month && minYear === year) {
                        let minTime = this.inputTime.datebox('getOption', 'minTime');
                        if (minTime === strDefaultMinTime) {
                            let minHour = parseInt(timeSubParts[0], 10),
                                minMinute = parseInt(timeSubParts[1], 10);
                            if ((hours < minHour) || ((hours === minHour) && (minutes < minMinute))) {
                                this.inputTime.val('');
                                hours = undefined;
                                minutes = undefined;
                            }
                            this.inputTime.datebox({ minTime: `${timeSubParts[0]}:${timeSubParts[1]}` });
                        }
                    } else {
                        // reset to 00:00
                        this.inputTime.datebox({ minTime: strDefaultMinTime });
                    }
                }
            }

            let maxDateTime = this.config.max || this.model.attributes.max;

            if (maxDateTime) {
                let maxYear,
                    maxMonth,
                    maxDay;
                if (maxDateTime.indexOf('T') > -1) {
                    let parts = maxDateTime.split('T'),
                        dateParts = parts[0].split('-'),
                        timeSubParts = parts[1].split(':');

                    maxYear = Number(dateParts[0]);
                    maxMonth = Number(dateParts[1]) - 1;
                    maxDay = Number(dateParts[2]);
                    if (maxDay === day && maxMonth === month && maxYear === year) {
                        let maxTime = this.inputTime.datebox('getOption', 'maxTime');
                        if (maxTime === strDefaultMaxTime) {
                            let maxHour = parseInt(timeSubParts[0], 10),
                                maxMinute = parseInt(timeSubParts[1], 10);
                            if ((hours > maxHour) || ((hours === maxHour) && (minutes > maxMinute))) {
                                this.inputTime.val('');
                                hours = undefined;
                                minutes = undefined;
                            }
                            this.inputTime.datebox({ maxTime: `${timeSubParts[0]}:${timeSubParts[1]}` });
                        }
                    } else {
                        // reset to 23:59
                        this.inputTime.datebox({ maxTime: strDefaultMaxTime });
                    }
                }
            }
        } else {
            this.dateObj = undefined;
            this.inputTime.datebox('ertSetTheDate', undefined);
        }

        if (hours !== undefined &&
            minutes !== undefined &&
            year !== undefined &&
            month !== undefined &&
            day !== undefined) {
            this.datetimeVal = new Date(year, month, day, hours, minutes, 0, 0);

            // <ERT MOD dst change check>
            if (!timeInput.datebox('checkLimits', this.datetimeVal) ||
                (this.datetimeVal.getHours() !== hours ||
                this.datetimeVal.getMinutes() !== minutes)) {
                // time not needed, but keeping to match Leapfrog... dst happened
                timeInput.val('');
            }

            // </ERT MOD dst change check>
            value = this.buildStudyWorksString(this.datetimeVal);
            if (!this.answers.size()) {
                this.answer = this.addAnswer();
            } else {
                this.answer = this.answers.at(0);
            }

            // @todo this should call thru to WidgetBase.

            return this.respondHelper(this.answer, value, value);
        }

        // not answered
        this.removeAllAnswers();
        return Q();
    }

    /**
     * Set a record as skipped. Clears input fields.
     * @param {string} swAlias - Skipped IT
     * @returns {boolean} Returns true if the skipped record is set, otherwise false.
     */
    skip (swAlias) {
        if (!this.completed) {
            this.localizedDate = '';
            this.localizedTime = '';
            this.dateObj = undefined;
        }

        return super.skip(swAlias);
    }
}

window.LF.Widget.DateTimePicker = DateTimePicker;
export default DateTimePicker;
