/**
 *    @file Base class for text entry controls
 *    @author <a href="mailto:brian.janaszek@ert.com">bjanaszek</a>
 *    @version 1.0.0
 */

import WidgetBase from './WidgetBase';

export default class TextBoxWidgetBase extends WidgetBase {
    /**
     * Extension of widget for a TextBox control.  Allows validation for the full value, as well as for individual
     * key presses.
     * @param {Object} options The options
     * @param {RegExp} [options.validateRegex] Regular expression used to validate the expression before
     * setting complete.  Otherwise will set complete to true for any non empty string.
     * @param {RegExp} [options.allowedKeyRegex] Regular expression used to validate a specific key press.  If the
     * resulting character passes this regular expression, the key is allowed.  If blank, allows all keys unless
     * they are in disallowedKeyRegex
     * @param {RegExp} [options.disallowedKeyRegex] Regular expression used to validate a specific key press.  If the
     * resulting character passes this regular expression, the key is blocked.
     */
    constructor (options) {
        super(options);

        /**
         *    List of widget events
         */
        this.events = {
            'keypress input[type=text]': 'keyPressed',
            'keypress input[type=password]': 'keyPressed',
            'keypress input[type=tel]': 'keyPressed',

            // DE19828: Listen to "input" as some devices don't use keyup for backspace
            'input input[type=text]': 'sanitize',
            'input input[type=tel]': 'sanitize',
            'input input[type=password]': 'sanitize'
        };
        this.options = options;
    }

    sanitize (uiElement) {
        let iNdx,
            isChanged = false,
            value = uiElement.val(),
            initVal = value,
            newVal = '';

        // Filter any emoji/unicode, and be sure to note that the value has changed:
        value = LF.Utilities.removeEmoji(value);
        if (value !== initVal) {
            isChanged = true;
        }

        for (iNdx = 0; iNdx < value.length; ++iNdx) {
            if (this.checkEnteredKey(value[iNdx])) {
                newVal += value[iNdx];
            } else {
                isChanged = true;
            }
        }

        if (isChanged) {
            uiElement.val(newVal);
            uiElement.blur();
            Q()
            .then(() => {
                uiElement.focus();
            })
            .done();
        }
    }

    /**
     *  Responds to a key press event.
     *  @param {Event} e Event data
     * @return {*}
     */
    keyPressed (e) {
        return e.returnValue;
    }

    checkEnteredKey (key) {
        let isValid = true,
            allowedKeyRegex = this.model.get('allowedKeyRegex'),
            disallowedKeyRegex = this.model.get('disallowedKeyRegex');

        if (allowedKeyRegex instanceof RegExp) {
            isValid = allowedKeyRegex.test(key);
        }

        if (disallowedKeyRegex instanceof RegExp) {
            isValid = isValid && !disallowedKeyRegex.test(key);
        }

        return isValid;
    }

    /**
     *
     * @param {Object} model The model
     * @param {string} responseString The response string
     * @param {boolean} completed Indicates completion
     * @returns {Q.Promise.<void>}
     */
    respondHelper (model, responseString, completed) {
        let validateRegex = this.model.get('validateRegex'),

            // completed is true by default for our AND's below.
            comp = typeof completed === 'boolean' ? completed : true;

        // If responseString is not a string, or 0 length, set completed to false and remove the whole answer.
        // Same thing if we are passing in a false already.
        if (typeof responseString !== 'string' || responseString.length === 0) {
            this.completed = false;
            this.removeAnswer(model);
            return Q();
        }

        if (!(validateRegex instanceof RegExp)) {
            this.completed = comp;
        } else {
            this.completed = validateRegex.test(responseString) && comp;
        }

        // super.respond(model, responseString, this.completed);
        return super.respondHelper(model, responseString, this.completed);
    }
}

window.LF.Widget.TextBoxWidgetBase = TextBoxWidgetBase;
