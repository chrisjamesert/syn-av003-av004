import CheckBox from './CheckBox';

/**
 * CustomCheckBox is identical to the CheckBox except that it has a different CSS class that creates buttons with no icons and centered text.
 */
class CustomCheckBox extends CheckBox {
    /**
     * Constructs a CustomCheckBox Class.
     * @param {Object} options Contains the configuration for the widget.
     * @param {Object} options.model The model containing the specific configuration for the widget, as represented by the "widget" property in the question configuration in the study design.
     * @param {number} options.model.id {dev-only} The ID associated with the widget.
     * @param {string} [options.model.className=CustomCheckBox] {dev-only} The CSS classname applied to the widget.
     * @param {Object} [options.model.templates] {dev-only} Contains any template overrides for rendering the widget.
     * @param {string} [options.model.templates.wrapper=DEFAULT:CheckBoxWrapper] {dev-only} The template used as the overall widget wrapper element.
     * @param {string} [options.model.templates.input=DEFAULT:CheckBox] {dev-only} The template used to render the input control.
     * @param {string} [options.model.templates.label=DEFAULT:CheckboxLabel] {dev-only} The template used to render the label attached to the checkboxes.
     * @param {Array} options.model.answers The list of answer options.  Each element in the array should be an object of the form <code>{ text: 'TEXT', value: '1', IT: 'CB_IT_XYZ' }</code>
     */
    // eslint-disable-next-line no-useless-constructor
    constructor (options) {
        super(options);
    }


    // noinspection JSMethodCanBeStatic
    get className () {
        return 'CustomCheckBox';
    }
}

window.LF.Widget.CustomCheckBox = CustomCheckBox;
export default CustomCheckBox;
