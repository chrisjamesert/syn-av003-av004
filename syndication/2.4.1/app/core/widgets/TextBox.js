import TextBoxWidgetBase from './TextBoxWidgetBase';

class TextBox extends TextBoxWidgetBase {
    get value () {
        if (this.model && this.$(`#${this.model.get('id')}`).length > 0) {
            return this.$(`#${this.model.get('id')}`).val();
        } else if (this.model) {
            return this.model.get('defaultVal');
        }
        return null;
    }

    set value (val) {
        this.$(`#${this.model.get('id')}`).val(val);
    }

    /**
     * Extension of TextBoxWidgetBase for a particular instance of a TextBox control.
     * Adds rendering capabilities and the ability to pass a maxLength to the input field.
     * @param {Object} options flags for this widget.
     * @param {Object} options.model The model containing the widget configuration (from the "widget" property of the question).
     * @param {int} [options.model.maxLength] Maximum length of the input field.  Null or blank will make it infinite.
     * @param {string} options.model.id {dev-only} The ID of the widget.
     * @param {string} options.model.className {dev-only} The CSS classname to be applied to the widget.
     * @param {string} [options.model.validateRegex] {dev-only} The regular expression used to validate user input.
     * @param {string} [options.model.disallowedKeyRegex] {dev-only} The regular expression to filter characters that cannot be entered into the textbox.
     * @param {string} [options.model.defaultVal=""] {dev-only} The default value for the textbox.
     * @param {string<translated>} [options.model.placeholder=""] Placeholder text in the textbox itself.  This will be deleted as soon as the user interacts with the textbox.  Can be used as "hint" text.
     * @param {string<translated>} [options.model.label=""] {dev-only} The text label to be attached to the textbox.
     * @param {boolean} [options.model.disabled=false] {dev-only} Indicates whether the textbox should be disabled.
     */
    constructor (options) {
        super(options);
        this.options = options;
    }

    /**
     * gets the text to display within the UI.  This property allows what is saved to be different than what is
     * displayed
     * @returns {string} the text to display
     */
    get displayText () {
        let answer,
            defaultVal = '';

        if (this.model) {
            defaultVal = this.model.get('defaultVal') || defaultVal;
        }

        if (this.answer) {
            answer = this.isUserTextBox ?
                JSON.parse(this.answer.get('response'))[this.model.get('field')] :
                this.answer.get('response');
        }
        return answer || defaultVal;
    }

    /**
     *  Responsible for displaying the widget
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.input,
            toTranslate = {};

        this.$el.empty();

        // Optionally populate the toTranslate object.
        this.model.get('label') && (toTranslate.label = this.model.get('label'));
        this.model.get('placeholder') && (toTranslate.placeholder = this.model.get('placeholder'));

        return this.i18n(toTranslate)
        .then((strings) => {
            let wrapperElement,
                labelElement,
                textBoxElement;

            wrapperElement = this.renderTemplate(templates.wrapper || this.wrapper);

            // If a template label is configured, render it.
            if (strings.label) {
                labelElement = this.renderTemplate(templates.label || this.label, {
                    link: this.model.get('id'),
                    text: strings.label
                });
            }

            textBoxElement = this.renderTemplate(textBox, {
                id: this.model.get('id'),
                placeholder: strings.placeholder || '',
                name: `${this.model.get('id')}-${this.uniqueKey}`,
                className: this.model.get('className'),
                maxLength: this.model.get('maxLength')
            });

            // Append the wrapper to the local DOM and find where to append other elements.
            let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

            if (labelElement) {
                $wrapper.append(labelElement);
            }

            // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
            $wrapper.append(textBoxElement)
                .appendTo(this.$el);

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            this.value = this.displayText;

            // If the widget has a disabled property, disable the input element.
            if (this.model.get('disabled')) {
                this.$(`#${this.model.get('id')}`).attr('disabled', 'disabled');
            }

            this.delegateEvents();

            if (this.displayText.length > 0) {
                this.$(`#${this.model.get('id')}`).trigger('input');
            }
        });
    }

    removeAnswer (model) {
        this.answer = undefined;
        super.removeAnswer(model);
    }

    /**
     *  Responds to user input
     * @returns {Q.Promise<void>}
     */
    respond () {
        let value = this.value;

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, value);
    }

    /**
     * Responds to a key press event.
     * @param {Event} e Event data
     * @returns {boolean}
     */
    keyPressed (e) {
        let keyCode = e.which || e.keyCode;

        // Always allow backspace/delete as well as tab for transferring cursor to next control
        // Always allow 229 as well (some android devices fire this for all keys)
        if (keyCode === 8 || keyCode === 9 || keyCode === 229) {
            return true;
        }

        if (!this.checkEnteredKey(String.fromCharCode(keyCode))) {
            e.returnValue = false;
            e.preventDefault();
        } else {
            e.returnValue = true;
        }
        return e.returnValue;
    }

    /**
     * Sanitizes the value, based on allowed characters
     * @param {Object} e event object
     */
    sanitize (e) {
        let uiElement = this.$(e.target),
            value = this.value;

        if (value.length === 0) {
            this.respond(e);
            return;
        }

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        super.sanitize(uiElement);

        // Always call respond after sanitizing value
        this.respond(e);
    }
}

export default TextBox;

TextBox.prototype.wrapper = 'DEFAULT:FormGroup';
TextBox.prototype.label = 'DEFAULT:Label';
TextBox.prototype.input = 'DEFAULT:TextBox';
window.LF.Widget.TextBox = TextBox;
