import TextBoxWidgetBase from './TextBoxWidgetBase';
import { Banner } from '../Notify';

const DEFAULT_WRAPPER = 'DEFAULT:FormGroup',
    DEFAULT_LABEL = 'DEFAULT:Label',
    DEFAULT_INPUT = 'DEFAULT:NumericTextBox';

/**
 *  NumericTextBox Widget extends the TextBoxWidgetBase class.
 *
 *  This is a textbox that limits input to numeric characters only,
 *  and forces a numeric keyboard on the device.  The widget can be created via the "NumberPad" widget name.
 */
class NumericTextBox extends TextBoxWidgetBase {
    /**
     *  Default wrapper template.
     *  @returns {string}
     */
    get defaultWrapperTemplate () {
        return DEFAULT_WRAPPER;
    }

    /**
     *  Default label template.
     *  @returns {string}
     */
    get defaultLabelTemplate () {
        return DEFAULT_LABEL;
    }

    /**
     *  Default input template.
     *  @returns {string}
     */
    get defaultInputTemplate () {
        return DEFAULT_INPUT;
    }

    /**
     * Constructs the <strong>NumberPad</strong> widget.
     * @param {Object} options Options for the widget.
     * @param {Object} options.model The model containing the specific widget configuration.  This represents the "widget" node in the study design for the question.
     * @param {string} options.model.id {dev-only} The ID associated with the widget.
     * @param {number} options.model.min  The minimum acceptable numeric value.  This property is required, and must be a number.  If these conditions are not met, an exception will be thrown.
     * @param {number} options.model.max The maximum acceptable numeric value.  This property is required, and must be a number.  If these conditions are not met, an exception will be thrown.
     * @param {number} options.model.step Indicates the number of decimal places allowed (this attribute name is taken from the existing HTML5 number input element).   To only allow whole numbers, the step value should be 1.  To allow 2 decimal places, the step should be .01. This property must be a number.  If this condition is not met, an exception will be thrown.
     * @param {Array} options.model.validationErrors {dev-only} This should contain a single element, checking the <code>isInputValid</code> property.
     * @param {string} options.model.className {dev-only} The CSS classname for the widget.
     * @param {string<translated>} options.model.label {dev-only} The text label to be attached to the widget.
     * @param {string<translated>} options.model.placeholder The "placeholder" text that can be rendered within the widget as a hint to the user.  This text will be removed as soon as the usser interacts with the widget.
     */
    constructor (options) {
        super(options);

        this._currentValue = '';

        // Set the default for the step
        if (this.model) {
            this.step = _.has(this.model.attributes, 'step') ? this.model.get('step') : 1;

            // Validate the rest of the model:
            this.validateModel();

            // Set the validation regex for our super
            this.setAllowedKeys();
        }

        this.events = {
            'input input[type=number]': 'sanitize',
            'focus input[type=number]': 'onFocus'
        };
    }

    /**
     * gets the text to display within the UI.  This property allows what is saved to be different than what is
     * displayed
     * @returns {string} the text to display
     */
    get displayText () {
        let answer;
        if (this.answer) {
            answer = this.answer.get('response');
        }
        return answer || '';
    }

    /**
     * Property indicates if the state of the control is valid.
     *
     * @return {boolean} - The state of the control's validity
     */
    get isInputValid () {
        // return this.$(`#${this.model.get('id')}`)[0].checkValidity();

        let isValid = true;
        const val = parseFloat(this.$(`#${this.model.get('id')}`).val());

        // If this is in decimal mode, be sure we have the appropriate number of decimal places
        if (this.step !== 1) {
            isValid = this.checkDecimalPlaces(val);
        }

        if (isValid) {
            // Now check min/max
            isValid = (val >= this.model.get('min') && val <= this.model.get('max'));
        }

        return isValid;
    }

    /**
     *  Gets the last known value of the text input.
     *  @returns {string} The value of _currentValue
     */
    get currentValue () {
        return this._currentValue;
    }

    /**
     *  Stores off the current value of the input.
     *  @param {String} val - The value to store
     */
    set currentValue (val) {
        this._currentValue = val;
    }

    /**
     *  Checks if the value has the appropriate number of decimal places.
     *  @param {Number} val - The value to check
     *  @returns {Boolean}
     */
    checkDecimalPlaces (val) {
        let valStr = val.toString();

        // Number of decimal will be step - 1 (excludes decimal)
        let decimalPlaces = this.model.get('step').toString().length - 1;
        let regex = new RegExp(`^\\d+(\\.\\d{0,${decimalPlaces}})?$`);

        return regex.test(valStr);
    }

    /**
     *  Sets the allowedKeyRegex in the model, for use when calling super.sanitize()
     */
    setAllowedKeys () {
        if (this.step === 1) {
            this.model.set('allowedKeyRegex', /[\d]/);
        } else {
            this.model.set('allowedKeyRegex', /[\d.]/);
        }
    }

    /**
     *  Validates the minx/max attributes on the model
     */
    validateModel () {
        if (!_.has(this.model.attributes, 'min')) {
            throw new Error('NumericTextBox Widget must have a min attribute');
        }

        if (isNaN(parseInt(this.model.get('min'), 10))) {
            throw new Error('NumericTextBox Widget min attribute must be a number');
        }

        if (!_.has(this.model.attributes, 'max')) {
            throw new Error('NumericTextBox Widget must have a max attribute');
        }

        if (isNaN(parseInt(this.model.get('max'), 10))) {
            throw new Error('NumericTextBox Widget max attribute must be a number');
        }

        if (isNaN(this.step)) {
            throw new Error('NumericTextBox Widget step attribute must be a number');
        }
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.defaultInputTemplate,
            toTranslate = {},
            answer = null;

        this.$el.empty();

        // Optionally populate the toTranslate object.
        this.model.get('label') && (toTranslate.label = this.model.get('label'));
        this.model.get('placeholder') && (toTranslate.placeholder = this.model.get('placeholder'));

        return this.i18n(toTranslate, () => null, { namespace: this.getQuestion().getQuestionnaire().id })
        .then((strings) => {
            let wrapperElement,
                labelElement,
                textBoxElement;

            wrapperElement = this.renderTemplate(templates.wrapper || this.defaultWrapperTemplate);

            // If a template label is configured, render it.
            if (strings.label) {
                labelElement = this.renderTemplate(templates.label || this.defaultLabelTemplate, {
                    link: this.model.get('id'),
                    text: strings.label
                });
            }

            // Note: the min/max values aren't really required by the
            // element, as we can't rely on the input element for validation.  But, if, in the future, we can, we may as well chuck them in there.
            textBoxElement = this.renderTemplate(textBox, {
                id: this.model.get('id'),
                placeholder: strings.placeholder || '',
                name: `${this.model.get('id')}-${this.uniqueKey}`,
                min: this.model.get('min'),
                max: this.model.get('max'),
                className: this.model.get('className'),
                step: this.step
            });

            // Append the wrapper to the local DOM and find where to append other elements.
            let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

            if (labelElement) {
                $wrapper.append(labelElement);
            }

            // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
            $wrapper.append(textBoxElement)
                .appendTo(this.$el);

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            this.$(`#${this.model.get('id')}`).val(this.displayText);

            this.delegateEvents();

            if (answer !== null) {
                // Trigger the key-up event so that the answer is properly formatted:
                this.$(`#${this.model.get('id')}`).input();
            }
        });
    }

    /**
     * removeAnswer - Removes the current answer from the model
     *
     * @param  {Object} model The Backbone model
     */
    removeAnswer (model) {
        this.answer = undefined;
        super.removeAnswer(model);
    }

    /**
     * validate - Validate the current response
     *
     * @return {boolean} - True if the widget response is va`lid
     */
    validate () {
        if (this.answer) {
            return super.validate() && this.isInputValid;
        }
        return !this.mandatory;
    }

    /**
     * respond - Handles the respond to widget input.
     *
     * @param  {Event} e description
     * @returns {Q.Promise<void>}
     */
    respond (e) {
        let target = this.$(e.target),
            value = target.val() || '';

        if (this.step === 1) {
            value = parseInt(value, 10);
        } else {
            value = parseFloat(value);
        }

        value = isNaN(value) ? '' : value.toString();

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        if (value.length === 0) {
            target.val('');
        }

        return this.respondHelper(this.answer, value);
    }

    /**
     * onFocus - Handles the focus event of the widget control, which removes the error styling, if applicable.
     */
    onFocus () {
        this.$el.find('input').removeClass('ui-state-error-border');
        Banner.closeAll();
    }

    /**
     * Sanitizes the value, based on allowed characters.
     * This doesn't really "sanitize" anything, but it's worth calling it.
     * @param {Object} e event object
     */
    sanitize (e) {
        let uiElement = this.$(e.target),
            value = uiElement.val();

        if (value.length === 0) {
            this.respond(e);
            return;
        }

        super.sanitize(uiElement);

        // Always call respond after sanitizing value
        this.respond(e);
    }
}

window.LF.Widget.NumericTextBox = NumericTextBox;
export default NumericTextBox;
