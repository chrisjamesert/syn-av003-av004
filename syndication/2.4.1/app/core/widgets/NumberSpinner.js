import BaseSpinner from './BaseSpinner';
import NumberSpinnerInput from './input/NumberSpinnerInput';

let { Model } = Backbone;

const DEFAULT_MODAL_TEMPLATE = 'DEFAULT:NumberSpinnerModal',
    DEFAULT_SPINNER_TEMPLATE = 'DEFAULT:NumberSpinnerControl',
    DEFAULT_SPINNER_ITEM_TEMPLATE = 'DEFAULT:NumberItemTemplate',
    DEFAULT_CLASS_NAME = 'NumberSpinner';

/**
 * The NumberSpinner input control implements the BaseSpinner interface, to create a numeric control.
 * <ul>
 *      <li>NumberSpinnerInput is the input.</li>
 *      <li>Spinners are injected into containers with the 'number-spinner-container' CSS class in the modal template.</li>
 *      <li>Creating an array to send to spinners splits the string on '.' and sends an array of size 2 to a decimal spinner.</li>
 *      <li>Values are concatenated together when returning from the spinner.</li>
 * </ul>
 * <p><i>Note: 3 things passed in from study-design.js must be arrays in this widget.  They depend
 * on there being the same number of HTML elements with the class of 'number-spinner-container' in the modal template.
 * Each index of these arrays will apply to each spinner, respectively, based on the order they occur in the template.
 * <ul>
 *     <li>spinnerInputOptions</li>
 *     <li>templates.spinnerTemplates</li>
 *     <li>templates.spinnerItemTemplates</li>
 * </ul></i>
 */
class NumberSpinner extends BaseSpinner {
    /**
     * default modal template
     * @returns {string}
     */
    get defaultModalTemplate () {
        return DEFAULT_MODAL_TEMPLATE;
    }

    /**
     * default spinner template
     * @returns {string}
     */
    get defaultSpinnerTemplate () {
        return DEFAULT_SPINNER_TEMPLATE;
    }

    /**
     * default spinner item template
     * @returns {string}
     */
    get defaultSpinnerItemTemplate () {
        return DEFAULT_SPINNER_ITEM_TEMPLATE;
    }

    /**
     * default class name.
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * Construct the number spinner
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.  This represents the "widget" node in the study-design for this question.
     * @param {string<translated>} options.model.label {dev-only} The text label for the spinner
     * @param {string<translated>} options.model.modalTitle The title string for the modal dialog.
     * @param {number} [options.model.min] The overall min value for combined spinners in this control. (Note, min still needs to be set to reasonable levels for each spinner instance)
     * @param {number} [options.model.max] The overall max value for combined spinners in this control. (Note, max still needs to be set to reasonable levels for each spinner instance)
     * @param {Array<NumberSpinnerInputOptions>} options.model.spinnerInputOptions Array of options for each spinner in the template.
     * @param {number} options.model.spinnerInputOptions[].min min value for this spinner instance.
     * @param {number} options.model.spinnerInputOptions[].max max value for this spinner instance.
     * @param {number} options.model.spinnerInputOptions[].precision number of decimal places to show in this spinner instance.
     * @param {boolean} [options.model.spinnerInputOptions[].showLeadingZeros=false] {dev-only} whether or not to show leading zeroes in this spinner instance.
     * @param {number} options.model.spinnerInputOptions[].step step intervals for this spinner instance.
     * @param {string} options.model.id {dev-only} The ID property for this widget.
     * @param {string} [options.model.type=NumberSpinner] {dev-only} The widget type.
     * @param {Object} [options.model.labels={labelOne: '', labelTwo: ''}] {dev-only} The label strings.  Used to replace the label placeholders in the modal dialog template.
     * @param {string} [options.model.okButtonText=OK] {dev-only} OK button text.
     * @param {string} [options.model.className=NumberSpinner] {dev-only} The CSS classname for the widget.
     */
    constructor (options) {
        super(options);

        /**
         * Array of number spinners.
         * @type {Array<NumberSpinnerInput>}
         */
        this.spinners = [];
    }

    /**
     * Get the string value for the text box from an array of spinner values.
     * @returns {Promise<string>} promise returning a string to be used for our textbox.
     */
    getModalValuesString () {
        for (let i = 0; i < this.spinners.length; ++i) {
            this.spinners[i].stop();
            this.spinners[i].pushValue();
        }

        return this.setSpinnerLimits()
        .then(() => {
            return this.getNumericValue().toString();
        });
    }

    /**
     * Gets the numeric value for these spinners.
     * Has optional params for custom indexes and values.
     * Those can be used to test the state of the spinners with
     * a different value selected (for instance, to evaluate overall min/max values for each spinner)
     * @param {Array<Object>} [customValues=[]] Custom values
     * @param {number} customValues[].index index of a custom value
     * @param {number} customValues[].value index of a custom value
     * @param {number} [customValue=null] The custom value
     * @returns {number} number representing all the values of the numeric spinners in the modal dialog, concatenated.
     */
    getNumericValue (customValues = []) {
        let valueArray = [];
        if (this.spinners.length === 0) {
            // Return null... no spinners yet, so no value.
            return null;
        }

        for (let i = 0; i < this.spinners.length; ++i) {
            valueArray.push(this.spinners[i].itemDisplayValueFunction(this.spinners[i].value));
        }
        _.each(customValues, (customValue) => {
            let index = customValue.index,
                value = customValue.value;
            valueArray[index] = this.spinners[index].itemDisplayValueFunction(value);
        });

        if (isNaN(parseFloat(valueArray[0]))) {
            return '';
        }
        return parseFloat(valueArray.reduce((x, y) => x.toString() + y.toString()));
    }

    /**
     * Override addCustomEvents.  Bind a chane event to run setSpinnerLimits.
     */
    addCustomEvents () {
        super.addCustomEvents();

        if (!this.spinners) {
            return;
        }

        this.setSpinnerLimits = _.bind(this.setSpinnerLimits, this);
        for (let i = 0; i < this.spinners.length; ++i) {
            this.listenTo(this.spinners[i].model, 'change:value', this.setSpinnerLimits);
        }
    }

    /**
     * Remove base events and overridden custom events.
     */
    removeCustomEvents () {
        for (let i = 0; this.spinners && i < this.spinners.length; ++i) {
            this.stopListening(this.spinners[i].model);
        }
        super.removeCustomEvents();
    }

    /**
     * Verify that possible numeric values with our custom properties
     * fall in range with our overall max and min.
     * @param {number} customIndex custom index to test
     * @param {number} customValue custom value to test for this index.
     * @returns {boolean} true if there are possible values with this indexes value, that fall within our max/min
    */
    testNumericValue (customIndex, customValue) {
        let currentCustom = {
            index: customIndex,
            value: customValue
        };
        let customMinValues = [currentCustom],
            customMaxValues = [currentCustom];

        for (let i = customIndex + 1; i < this.spinners.length; ++i) {
            customMinValues.push({
                index: i,
                value: this.spinners[i].model.get('min')
            });
            customMaxValues.push({
                index: i,
                value: this.spinners[i].model.get('max')
            });
        }

        let minValue = this.getNumericValue(customMinValues),
            maxValue = this.getNumericValue(customMaxValues),
            minAllowed = parseFloat(this.model.get('min')),
            maxAllowed = parseFloat(this.model.get('max')),
            greaterThanMin =
                    isNaN(minAllowed) ||
                    maxValue >= minAllowed,

            lessThanMax =
                    isNaN(maxAllowed) ||
                    minValue <= maxAllowed
                    ;

        // Kind of confusing.  Return true if it is greater than the min and less than the max.
        //  OR if it fails both.  The reason it would fail both is that the entirety of the range is under this level
        //  (i.e. our number is 7, and the range is 7.25 to 7.75)
        return (greaterThanMin && lessThanMax) || (!greaterThanMin && !lessThanMax);
    }

    /**
     * Get the array of spinner values from the textbox entry.
     * @returns {Array<number>} the array to be passed along to our spinners.
     */
    getSpinnerValuesArray () {
        let textBox = this.$(`#${this.model.get('id')}`),
            valArray,
            defaultVal = this.model.get('defaultVal'),
            val;

        val = textBox.val();

        if (val === '' || val === undefined) {
            val = defaultVal !== undefined ? defaultVal.toString() : '';
        }

        valArray = val.split('.');

        if (valArray.length > 1) {
            valArray[1] = parseFloat(`.${valArray[1]}`);
        }

        return valArray;
    }

    /**
     * Place spinners into place.  For the NumberSpinner implementation, they go into elements
     * with the CSS class 'number-spinner-container'.
     */
    injectSpinnerInputs () {
        let foundSpinner = false,
            that = this;

        // jscs:disable requireArrowFunctions
        that.$modal.find('.number-spinner-container').each(function (i) {
            let spinnerTemplate = that.getSpinnerTemplate(i),
                spinnerItemTemplate = that.getSpinnerItemTemplate(i),
                spinnerInputOptions;

            // For Item, delay evaluate, so we can replace placeholders with actual values and display values
            spinnerInputOptions = that.model.get('spinnerInputOptions') || [];

            foundSpinner = true;
            that.spinners[i] = new NumberSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        // jscs:enable requireArrowFunctions
        if (!foundSpinner) {
            throw new Error(`Invalid template for NumberSpinner widget.  Expected an element with
                "number-spinner-container" class`);
        }
    }

    testAllValuesForIndex (index) {
        let spinner = this.spinners[index],
            min = spinner.itemValueFunction(spinner.model.get('min')),
            max = spinner.itemValueFunction(spinner.model.get('max'));

        return this.testNumericValue(index, min) && this.testNumericValue(index, max);
    }

    /**
     * Adjust the spinner at this index.  Find its allowable min/max values and hide ones out of range.
     * @param {number} index spinner index
     * @returns {Q.Promise<void>} promise resolving when adjustment is finished.
     */
    adjustSpinner (index) {
        let spinner = this.spinners[index],
            lastItemIndex = spinner.$items.length - 1,

            // first actual item (index 0 has an empty item)
            adjustSpinnerPromises = [],
            lastEventWasHide = true,
            oldValue = spinner.itemValueFunction(spinner.value),
            firstIndexValue = this.spinners[0].value,
            allRangeValuesValid = this.testAllValuesForIndex(index) && (firstIndexValue !== '' || index === 0),
            valueWhenFinished = null;

        if (allRangeValuesValid) {
            spinner.unHideAll();
            valueWhenFinished = oldValue;
        }

        // Iterate backwards.  The reason for this is that unHideItem() will determine whether or not it needs to scroll based on the current scroll position.
        //      The current scroll position is altered by adding things that take up space, so new unhidden items need to be at a lower scroll position (0) to work.
        //          so that unHideItem knows that it should adjust this offset accordingly.
        for (let itemNdx = lastItemIndex; itemNdx >= 1 && !allRangeValuesValid; --itemNdx) {
            let itemValue = spinner.itemValueFunction($(spinner.$items[itemNdx]).data('value')),
                shouldHide = !this.testNumericValue(index, itemValue) ||
                                (firstIndexValue === '' && index !== 0);
            if (shouldHide) {
                adjustSpinnerPromises.push(() => {
                    lastEventWasHide = true;
                    return this.spinners.length > 0 ?
                        spinner.hideItem(itemValue, false, false) :
                        Q();
                });
            } else {
                adjustSpinnerPromises.push(() => {
                    if (lastEventWasHide) {
                        // When switching from hiding to unhiding, check to make sure all our items are not display:none
                        //  If they are, the scroll position will be off, and this always means we should scroll to the max visible value.
                        //  (since we are iterating this list backwards, the user has previously selected something that is larger and out of range)
                        if (valueWhenFinished === null) {
                            valueWhenFinished = itemValue;
                        }
                        lastEventWasHide = false;
                    } else if (itemValue >= oldValue || oldValue === '') {
                        valueWhenFinished = itemValue;
                    }

                    return this.spinners.length > 0 ?
                        spinner.unHideItem(itemValue, false, false) :
                        Q();
                });
            }
        }

        if (firstIndexValue !== '') {
            spinner.hideItem('', false, false);
            if (valueWhenFinished === '') {
                valueWhenFinished = spinner.itemValueFunction(spinner.model.get('min'));
            }
        }

        adjustSpinnerPromises.push(() => {
            if (oldValue === '' && this.spinners[0].value === '') {
                valueWhenFinished = '';
            }
            return spinner.setValue(valueWhenFinished);
        });
        adjustSpinnerPromises.push(() => {
            spinner.pushValue();
            spinner.scroller.refresh();
            return Q();
        });

        return adjustSpinnerPromises.reduce(Q.when, Q());
    }

    // Figure out the max and min of each spinner, based on the current values of the other spinners
    // disable options we can't use
    setSpinnerLimits () {
        if (this.spinnerLimitsPromise || this.spinners.length === 0) {
            return this.spinnerLimitsPromise || Q();
        }

        for (let i = 0; i < this.spinners.length; ++i) {
            if (!this.spinners[i].isRendered || this.spinners[i].value === null) {
                // Some spinner is in flux.  Wait for all of them to finish spinning before adjusting the limits.
                return this.spinnerLimitsPromise || Q();
            }
        }

        let adjustSpinnerPromiseFactories = [];

        // Start from largest value and work way to smallest.
        _.each(this.spinners, (spinner, iNdx) => {
            adjustSpinnerPromiseFactories.push(() => {
                return this.adjustSpinner(iNdx);
            });
        });

        this.spinnerLimitsPromise = adjustSpinnerPromiseFactories.reduce(Q.when, Q())
        .then(() => {
            this.spinnerLimitsPromise = false;
        });
        return this.spinnerLimitsPromise;
    }
}

window.LF.Widget.NumberSpinner = NumberSpinner;
export default NumberSpinner;
