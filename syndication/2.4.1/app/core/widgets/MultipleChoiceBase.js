/** #depends Base.js
 * @file Defines a base class for multiple choice question's widget.
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.4
 */

import WidgetBase from './WidgetBase';

export default class MultipleChoiceBase extends WidgetBase {
    // noinspection JSMethodCanBeStatic
    get container () {
        return 'DEFAULT:VerticalButtonGroup';
    }

    constructor (options) {
        this.options = options;
        super(options);
    }

    /**
     * Build the html structure of widget
     * @param {String} type The type of the widget
     * @example this.buildHTML('checkbox');
     * @example this.buildHTML('radio')'
     * @returns {Q.Promise} resolved when HTML is built and ready
     */
    buildHTML (type) {
        let modelId = this.model.get('id'),
            templates = this.model.get('templates') || {},
            container = templates.container || this.container,
            input = templates.input || this.input,
            label = templates.label || this.label,
            wrapper = templates.wrapper || this.wrapper,
            curPromise = Q();

        this.$el.html(LF.templates.display(container));
        const AddOptionFactory = (answer, i) => {
            return LF.getStrings(answer.text, $.noop, { namespace: this.question.getQuestionnaire().id })
            .then((string) => {
                return Q.Promise((resolve) => {
                    let $ele,

                        // Append options within a container
                        appendOption = function ($target) {
                            return $target.find('[data-container]').addBack('[data-container]')
                                .first()
                                .append(LF.templates.display(input, {
                                    id: `${modelId}_${type}_${i}`,
                                    name: modelId,
                                    value: answer.value
                                }), LF.templates.display(label, {
                                    link: `${modelId}_${type}_${i}`,
                                    className: `${type}_${i}`,
                                    text: string
                                }));
                        };

                    if (wrapper != null) {
                        $ele = $(LF.templates.display(wrapper, {
                            id: `${modelId}_${type}_${i}`
                        }));
                        appendOption($ele);
                        this.$('[data-container]')
                            .first()
                            .append($ele)
                            .ready(resolve);
                    } else {
                        appendOption(this.$el).ready(resolve);
                    }
                });
            });
        };

        _(this.model.get('answers')).each((answer, i) => {
            curPromise = curPromise.then(() => {
                // eslint-disable-next-line new-cap
                return AddOptionFactory(answer, i);
            });
        });
        return curPromise;
    }

    /**
     * Set the height of the multiple choice buttons evenly distributed.
     * @example this.setLabelHeight();
     * @returns {Q.Promise} resolved when height is done being set
     */
    setLabelHeight () {
        let maxLabelHeight = 0,
            that = this;
        return Q()
        .then(() => {
            let promises = [];

            // JQuery relies on "function()" for this scope.
            // jscs:disable requireArrowFunctions
            // Find the maximum height
            that.$('.btn').each(function () {
                let height = $(this).height();
                maxLabelHeight = height > maxLabelHeight ? height : maxLabelHeight;
            });

            // Set all labels height as the maximum
            that.$('.btn').each(function () {
                let elem = $(this);
                promises.push(new Q.Promise((resolve) => {
                    elem.height(maxLabelHeight).ready(resolve);
                }));
            });

            // jscs:enable requireArrowFunctions

            return Q.allSettled(promises);
        });
    }
}

window.LF.Widget.MultipleChoiceBase = MultipleChoiceBase;
