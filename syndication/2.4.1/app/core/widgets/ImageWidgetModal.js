import ImageWidget from './ImageWidget';
import ModalLauncherMixin from './ModalLauncherMixin';
import { mix } from 'core/utilities/languageExtensions';

const defaultModalTemplate = 'DEFAULT:MultipleChoiceModal';

/**
 * This widget allows for creating a multi-select SVG image (see {@link ImageWidget}) that also launches a modal dialog to allow a user
 * to select a value for that node.
 * @extends ImageWidget
 */

class ImageWidgetModal extends mix(ImageWidget).with(ModalLauncherMixin) {
    get defaultModalTemplate () {
        return defaultModalTemplate;
    }

    /**
     * Construct the image widget with a modal dialog
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.  This represents the "widget" node in the study-design for this question.
     * @param {boolean} [options.model.singleSelect=false] Whether or not the control is single select.  <ul><li>True will act as a radio-button (returning the node ID as the single answer).</li>
     *              <li>False will act like a checkbox.  Each node translates to an IT (by pre-pending "{QuestionID}_"), and those all are entered as separate answers to the collection,
     *              with responses of 1 or 0.</li></ul>
     * @param {Array} [options.model.options] array of options, usually defaulting to an array of 2... a selected (value 0) option and an unselected (value 1) option.
     *      This is also where CSS for different selected states is defined.  Making this array larger than 2 items will do nothing in single select mode, but will
     *      make click events cycle through the available options in multi-select mode.
     * @param {string} [options.model.options[0].css.fill=#FFFFFF] Style fill color for an unselected node.
     * @param {string} [options.model.options[0].value=0] {dev-only} value for an unselected node.
     * @param {string} [options.model.options[1].css.fill=#0000FF] Style fill color for a selected node.
     * @param {string} [options.model.options[1].value=1] {dev-only} value for a selected node.
     * @param {Object} [options.model.inputOptions] options for the modal widget
     * @param {string} [options.model.inputOptions.itemTemplate] {dev-only} item template for the input options
     * @param {string} [options.model.inputOptions.type] (MultipleChoiceInput|NumberSpinnerInput) type of modal input
     * @param {Array} [options.model.inputOptions.items] (MultipleChoiceInput ONLY) Array of items to be used as options for this pushbutton
     * @param {string} [options.model.inputOptions.items[].text] (MultipleChoiceInput ONLY) text of the item
     * @param {string} [options.model.inputOptions.items[].value] (MultipleChoiceInput ONLY) value of the item, typically starting with '0' to indicate an unselected node.
     * @param {number} [options.model.inputOptions.min] (NumberSpinnerInput ONLY) minimum value of the spinner in the modal dialog
     * @param {number} [options.model.inputOptions.max] (NumberSpinnerInput ONLY) maximum value of the spinner in the modal dialog
     * @param {Object} [options.model.imageText] object of key-value pairs.  Keys are rect IDs in the SVG file, and the values of these are the lookups for text in the translated text area.
     * @param {Object} options.model.imageName {dev-only} Image name of the SVG inside the configured imageFolder.
     * @param {string} options.model.nodeIDRegex {dev-only} regular expression.  When this expression passes with a path ID in the SVG file, this indicates that it is selectable.
     * @param {string} [options.model.imageFolder=IMAGE_FOLDER] {dev-only} Image folder to be used by the config.  This key is looked up as translated text, and defaults to the standard image folder.
     * @param {Object} [options.model.templates] {dev-only} templates for this widget.  The only template to be set is the imageTemplate, which defaults to DEFAULT:ImageTemplate.
     */

    constructor (options) {
        super(options);
        this.$selectedNode = null;
    }

    /**
     * Render the control
     * @returns {Q.Promise<void>} promise resolving when render is complete.
     */
    render () {
        return super.render()
        .then(() => {
            return this.i18n(this.getModalTranslationsObject(),
                () => null,
                { namespace: this.getQuestion().getQuestionnaire().id }
            );
        })
        .then((modalStrings) => {
            return this.renderModal(modalStrings);
        })
        .then(() => {
            this.delegateEvents();
        });
    }

    /**
     * Overrideable hook for when the dialog is closing.
     * @param {Event} e The event args
     * @returns {Q.Promise<void>} the promise if not coming from an event handler
     *      , or undefined if it is.
     */
    dialogClosing (e) {
        let ret = Q(),
            options = this.model.get('options');

        ret = this.getModalValuesString()
        .then((answer) => {
            if (this.$selectedNode) {
                let selectedOption = options[this.getOptionIndex(answer)];
                this.$selectedNode.data('state', selectedOption.value);
                this.$selectedNode.css(selectedOption.css);

                this.$selectedNode = null;
            }
        })
        .then(() => {
            return this.handleMultiSelect();
        });

        // If called directly from an event handler, close the promise,
        // and return undefined (value of ret.done())
        //  Otherwise return the promise to be handled by the caller.
        return e instanceof $.Event ? ret.done() : ret;
    }

    /**
     * Get array of input values
     * @returns {Array} array of values for inputs
     */
    getInputValuesArray () {
        return [this.$selectedNode ? this.$selectedNode.data('state') || '0' : '0'];
    }

    /**
     * Get the string value for the text box from an array of spinner values.
     * @returns {Promise<string>} promise returning a string to be used for our textbox.
     */
    getModalValuesString () {
        return Q()
        .then(() => {
            for (let i = 0; i < this.inputs.length; ++i) {
                this.inputs[i].pushValue();
            }
        })
        .then(() => {
            let valueArray = [];
            for (let i = 0; i < this.inputs.length; ++i) {
                valueArray.push(this.inputs[i].itemDisplayValueFunction(this.inputs[i].value));
            }
            if (isNaN(parseFloat(valueArray[0]))) {
                return '';
            }
            return valueArray.reduce((x, y) => x.toString() + y.toString());
        });
    }

    /**
     * Respond to a click event by launching the modal dialog.
     * @param {Event} e event object from click event.
     * @returns {Q.Promise<void>} promise resolving after respond is finished.
     */
    respond (e) {
        this.$selectedNode = $(e.target);
        return this.openDialog();
    }
}

window.LF.Widget.ImageWidgetModal = ImageWidgetModal;

export default ImageWidgetModal;
