/**
 * Widget Validation Function for a Password Textbox Widget
 * @param {Object} answer the answer object from the widget
 * @param {Object} params the validation object form the widget model
 * This function can be used to check anything you wish to validate upon clicking next
 */

import { Banner } from 'core/Notify';

/**
 * @param {*} answer The answer
 * @param {*} params The params
 * @param {*} callback The function callback
 */
export function checkPasswordFieldWidget (answer, params, callback) {
    let errorBanner = function (error, callback) {
        let errorString = error || 'ERROR';

        LF.getStrings(errorString, (string) => {
            Banner.show({
                text: string,
                type: 'error'
            });
        });

        $(`#${answer.attributes.question_id}`).find('input').addClass('ui-state-error-border');

        callback(false);
    };

    let response = JSON.parse(answer.get('response')).password;

    if (!response) {
        errorBanner('INVALID_ENTRY', callback);
        return;
    }

    callback(true);
}

LF.Widget.ValidationFunctions.checkPasswordFieldWidget = checkPasswordFieldWidget;

export default checkPasswordFieldWidget;
