// CORE FILES
import checkEditPatientID from './checkEditPatientID';
import checkPasswordFieldWidget from './checkPasswordFieldWidget';
import checkPatientID from './checkPatientID';
import checkPatientRange from './checkPatientRange';
import checkRoleFieldWidget from './checkRoleFieldWidget';
import checkLanguageFieldWidget from './checkLanguageFieldWidget';
import checkUserNameField from './checkUserNameField';
import confirmFieldValue from './confirmFieldValue';
import checkAffidavitCredentials from './checkAffidavitCredentials';

// END CORE FILES

export default { checkEditPatientID, checkPasswordFieldWidget, checkPatientID, checkPatientRange,
    checkRoleFieldWidget, checkUserNameField, confirmFieldValue, checkAffidavitCredentials, checkLanguageFieldWidget };
