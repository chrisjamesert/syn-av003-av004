import Users from 'core/collections/Users';
import { Banner } from 'core/Notify';
import { getSpecialCharacterRegExp, stringCompareCI, getNested } from 'core/utilities';

/**
 * Widget Validation Function for a Confirmation Textbox Widget
 * @param {Object} answer the answer object from the widget
 * @param {Object} params the validation object form the widget model
 * @param {Function} callback The callback function to called upon completion.
 */
export function checkUserNameField (answer, params, callback) {
    // Get a list of responses from all the answer models.
    let responses = _.invoke(params.answers.models, 'get', 'response');

    // Get the unparsed role response.
    let unparsedRoleResponse = _.find(responses, response => JSON.parse(response).role);

    // Parse the role response as JSON.
    let answerResponsesRole = JSON.parse(unparsedRoleResponse).role;

    // Parse out the username.
    let username = JSON.parse(answer.get('response')).username;

    let errorBanner = function (errorString) {
        LF.getStrings(errorString)
        .then((text) => {
            Banner.show({ text, type: 'error' });
        });

        $(`#${answer.attributes.question_id}`)
        .find('input')
        .addClass('ui-state-error-border');
    };

    if (!getSpecialCharacterRegExp().test(username)) {
        errorBanner('USER_NAME_INVALID_CHARACTERS');
        callback(false);
    } else {
        Users.fetchCollection()
        .then((users) => {
            // Determine if the user exists by comparing username values.
            let userExists = users.find((user) => {
                return user.get('role') === answerResponsesRole && stringCompareCI(user.get('username'), username);
            });

            // Determine if there is a target user.
            let targetUser = getNested('parent.parent.user', this);

            // This value is used to determine if the username is identical to the
            // username of the user being edited.
            let identicalId = false;

            // If there is a target user, determine if the set username is the same as the targets.
            // If this is true, then we don't want to display an error message.
            if (targetUser) {
                identicalId = targetUser.get('role') === answerResponsesRole && stringCompareCI(targetUser.get('username'), username);
            }

            // If the username exists and is not identical to the target user's existing username,
            // display a banner.
            userExists && !identicalId && errorBanner('USER_ALREADY_EXISTS');

            callback(!userExists || userExists && identicalId);
        });
    }
}

LF.Widget.ValidationFunctions.checkUserNameField = checkUserNameField;

export default checkUserNameField;
