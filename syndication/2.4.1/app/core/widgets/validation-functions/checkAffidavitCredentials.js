import NotifyView from 'core/views/NotifyView';
import Logger from 'core/Logger';

const logger = new Logger('validation-functions/checkAffidavitCredentials');

/**
 * Widget validation function for password affidavit.
 * @param {Object} answer - The answer provided by the user.
 * @param {Object} params - Parameters passed to validation function (error strings, etc).
 * @param {function} callback - The function to be called at the end of processing.
 */
export function checkAffidavitCredentials (answer, params, callback) {
    let responseTextbox = this.$(`#${this.model.get('id')}`),
        response = responseTextbox.val(),
        namespace = this.questionnaire.id,
        getActiveUser = () => {
            let user = {};
            if (LF && LF.security && LF.security.activeUser) {
                user = LF.security.activeUser;
            }

            return user;
        },
        errorPopup = (errorString) => {
            let notifyStrings = {};

            notifyStrings.header = {
                key: errorString.header || 'VALIDATION_HEADER',
                namespace
            };

            notifyStrings.body = {
                key: errorString.errString,
                namespace
            };

            notifyStrings.ok = {
                key: errorString.ok || 'VALIDATION_OK',
                namespace
            };

            LF.getStrings(notifyStrings)
            .then((strings) => {
                (new NotifyView()).show(strings);
            });
        },
        failedAttempt = () => {
            let user = getActiveUser();

            logger.error(`Failed password affidavit attempt by ${user.get('username')}`);

            LF.security.setLoginFailure(LF.security.getLoginFailure(user) + 1, user);

            errorPopup(params.errorStrings.failed);

            responseTextbox.val('');
            this.completed = false;
        },
        handleLock = () => {
            const trigger = `QUESTIONNAIRE:AffidavitFailure/${namespace}`;

            ELF.trigger(trigger, {}, this);
        };

    LF.security.login(getActiveUser().get('id'), response)
    .then((user) => {
        if (user) {
            // Reset the answer so the password is not transmitted:
            answer.set('response', '1');

            callback(true);
        } else {
            failedAttempt();
            if (LF.StudyDesign.maxLoginAttempts === LF.security.getLoginFailure(getActiveUser())) {
                logger.error(`User ${getActiveUser().get('username')} has been locked out.`);
                if (LF.security.accountDisable(getActiveUser())) {
                    handleLock();
                }
            }
            callback(false);
        }
    });
}

LF.Widget.ValidationFunctions.checkAffidavitCredentials = checkAffidavitCredentials;

export default checkAffidavitCredentials;
