import { Banner, MessageRepo } from 'core/Notify';
import COOL from 'core/COOL';

/**
 * Widget Validation Function for a Confirmation Textbox Widget
 * @param {object} answer the answer object from the widget
 * @param {object} params the validation object form the widget model
 * @param {Function} callback A callback function invoked upon completion.
 */
export function checkRoleFieldWidget (answer, params, callback) {
    let errorBanner = (error) => {
        let errorString = error || 'ERROR';

        LF.getStrings(errorString, (string) => {
            Banner.show({
                text: string,
                type: 'error'
            });
        });

        $(`#${answer.attributes.question_id}`).find('input').addClass('ui-state-error-border');
    };

    let roleAnswer = JSON.parse(answer.get('response')).role,
        role = _(LF.StudyDesign.roles.models).findWhere({ id: roleAnswer });

    if (!roleAnswer) {
        MessageRepo.display(MessageRepo.Banner.USER_SELECT_ROLE_VALIDATION);
        callback(false);
    }

    COOL.getClass('Utilities').isOnline((isOnline) => {
        if (!isOnline && !role.canAddOffline()) {
            errorBanner(params.errorString);
            callback(false);
        } else {
            callback(true);
        }
    });
}

LF.Widget.ValidationFunctions.checkRoleFieldWidget = checkRoleFieldWidget;

export default checkRoleFieldWidget;
