import WidgetBase from './WidgetBase';

/**
 * Displays a static text field.
 * Note: The answer record must be pre-populated prior to render.
 * @example
 * {
 *   id          : 'ACTIVATE_USER_USERNAME',
 *   IG          : 'Activate_User',
 *   IT          : 'ACTIVATE_USER_1',
 *   text        : ['ACTIVATE_USER_CONFIRM_INFORMATION'],
 *   className   : 'ACTIVATE_USER',
 *   widget      : {
 *       id              : 'ACTIVATE_USER_W_1',
 *       type            : 'StaticField',
 *       label           : 'FULL_NAME',
 *       field           : 'username'
 *   }
 * }
 */
export default class StaticField extends WidgetBase {
    constructor (options) {
        super(options);

        this.templateStrings = {};
    }

    /**
     * The default wrapper template.
     * @returns {string}
     */
    get wrapper () {
        return 'DEFAULT:FormGroup';
    }

    /**
     * The default lebel template.
     * @returns {string}
     */
    get label () {
        return 'DEFAULT:Label';
    }

    /**
     * The default input template.
     * @returns {string}
     */
    get input () {
        return 'DEFAULT:StaticField';
    }

    /**
     * The text to display when rendering the widget.
     * @returns {string} The text to display.
     */
    get displayText () {
        let field = this.model.get('field');
        let response = this.answer.get('response');

        if (field) {
            return JSON.parse(response)[field];
        }
        return response;
    }

    /**
     * Render the StaticField widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {};

        // Empty out the widget's DOM at each render.
        this.$el.empty();

        // Optionally populate the templateStrings object.
        this.model.get('label') && (this.templateStrings.label = this.model.get('label'));

        // If the response is set to be translated, add it to the map of translation keys.
        if (this.model.get('translatable')) {
            this.templateStrings.response = this.displayText;
        }

        return this.i18n(this.templateStrings)
        .then((strings) => {
            let wrapperEle = this.renderTemplate(templates.wrapper || this.wrapper);
            let labelEle;

            // If a template label is configured, render it.
            if (strings.label) {
                labelEle = this.renderTemplate(templates.label || this.label, {
                    link: this.model.get('id'),
                    text: strings.label
                });
            }

            // Append the wrapper to the local DOM and find where to append other elements.
            let $wrapper = this.$el.append(wrapperEle).find('[data-container]');

            if (labelEle) {
                $wrapper.append(labelEle);
            }

            let inputEle = this.renderTemplate(templates.input || this.input, {
                response: strings.response || this.displayText
            });

            $wrapper.append(inputEle);

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            // Clear out $wrapper to prevent a memory leak via DOM reference in closure.
            $wrapper = null;
        });
    }
}

window.LF.Widget.StaticField = StaticField;
