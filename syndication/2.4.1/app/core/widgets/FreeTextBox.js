import TextBox from './TextBox';

const DEFAULT_WRAPPER = 'DEFAULT:FormGroup',
    DEFAULT_LABEL = 'DEFAULT:Label',
    DEFAULT_INPUT = 'DEFAULT:FreeTextBox';

/**
 * FreeTextBox Widget
 * This is a textbox that allows full configuration of the input, based on model settings.
 * @author <a href="mailto:brian.janaszek@ert.com">bjanaszek</a>
 * @version 1.00
 */
class FreeTextBox extends TextBox {
    /**
     * Default wrapper template
     * @returns {string}
     */
    get defaultWrapperTemplate () {
        return DEFAULT_WRAPPER;
    }

    /**
     * Default label template
     * @returns {string}
     */
    get defaultLabelTemplate () {
        return DEFAULT_LABEL;
    }

    /**
     * Default input element template
     * @returns {string}
     */
    get defaultInputTemplate () {
        return DEFAULT_INPUT;
    }

    /**
     * Construct the free text area
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.  This represents the "widget" node in the study-design for this question.
     * @param {boolean} [options.model.showCount=false] whether or not to show the count of remaining characters
     * @param {string<translated>} [options.model.text=""] label for remaining characters in "showCount" (i.e. "characters left: ")
     * @param {string} [options.model.defaultVal=""] {dev-only} Default value for this control.
     * @param {number} [options.model.rows] number of rows to show on the text area.  Default is the default number of rows for a text area (or whatever
     * the control template uses), on the particular platform and OS.
     * @param {number} options.model.maxLength number of characters allowed in the text field.  Also used for display when showCount is true.
     * @param {string<translated>} [options.model.placeholder=""] placeholder string, shown as gray in the textbox when it is empty.
     * @param {string} [options.model.resize=both] (none|both|horizontal|vertical) {dev-only} resize property for text area.  Which directions, if any, to allow the user
     * to resize the text area.
     * @param {string} options.model.id {dev-only} The ID property for this widget.
     * @param {string} options.model.className {dev-only} The CSS classname for the widget.
     * @param {Object} options.model.templates {dev-only} templates to use.  Use the following object properties to override templates:
     * <i>wrapper, label, input</i>, which default to <i>DEFAULT:FormGroup, DEFAULT:Label, DEFAULT:FreeTextBox</i>, respectively.
     */
    constructor (options) {
        super(options);

        if (this.model) {
            this.validation = this.model.get('validation');
            this.showCount = this.model.has('showCount') ? this.model.get('showCount') === 'true' || this.model.get('showCount') === true : false;
            this.id = this.model.get('id');
            this.placeholder = this.model.has('placeholder') ? this.model.get('placeholder') : '';
        }

        this.validateModel();

        this._currentValue = '';

        this.events = _.extend(this.events, {
            'input textarea': 'inputHandler',
            'cut textarea': 'editHandler',
            'copy textarea': 'editHandler',
            'paste textarea': 'editHandler'
        });
    }

    /**
     * Responsible for rendering the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.defaultInputTemplate,
            toTranslate = {};

        this.$el.empty();

        this.placeholder && (toTranslate.placeholder = this.placeholder);
        this.model.get('text') && (toTranslate.text = this.model.get('text'));

        return this.i18n(toTranslate, () => null, { namespace: this.getQuestion().getQuestionnaire().id })
        .then((strings) => {
            let wrapperElement,
                labelElement,
                textBoxElement,
                charsLeft = parseInt(this.model.get('maxLength'), 10);

            wrapperElement = this.renderTemplate(templates.wrapper || this.wrapper);

            // If a template label is configured, render it.
            if (strings.label) {
                labelElement = this.renderTemplate(templates.label || this.label, {
                    link: this.id,
                    text: strings.label
                });
            }

            textBoxElement = this.renderTemplate(textBox, {
                id: this.id,
                placeholder: strings.placeholder || '',
                name: `${this.id}-${this.uniqueKey}`,
                className: this.model.get('className'),
                text: strings.text || ''
            });

            // Append the wrapper to the local DOM and find where to append other elements.
            let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

            if (labelElement) {
                $wrapper.append(labelElement);
            }

            // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
            $wrapper.append(textBoxElement)
                .appendTo(this.$el);

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            let $textarea = this.$(`#${this.id}`);

            this.setAttributes($textarea);
            this.setCSS($textarea);
            this.delegateEvents();

            this.value = this.displayText;
            if (this.displayText.length > 0 && this.showCount) {
                charsLeft = parseInt(this.model.get('maxLength'), 10) - this.answer.get('response').length;
            }

            if (this.displayText.length > 0) {
                this.$(`#${this.model.get('id')}`).trigger('input');
            }

            this.setCounter(charsLeft);
        });
    }

    /**
     * Sets any attributes on the text area element.
     * @param {Object} $textarea - The jQuery object representing the text area element
     */
    setAttributes ($textarea) {
        if (this.model.has('rows')) {
            $textarea.attr('rows', this.model.get('rows'));
        }
        if (this.model.has('maxLength')) {
            $textarea.attr('maxlength', this.model.get('maxLength'));
        }
    }

    /**
     * Sets the CSS attributes for the textarea.
     * @param {Object} $textarea - The jQuery object pointing to the textarea element.
     */
    setCSS ($textarea) {
        // Options are: none, both, horizontal, vertical, and inherit (Default is "both")
        if (this.model.has('resize')) {
            $textarea.css('resize', this.model.get('resize'));
        }

        if (!this.showCount) {
            this.$(`#${this.id}-counter`).css('visibility', 'hidden');
        }
    }

    /**
     * Responds to user input
     * @param {Event} e Event data
     * @returns {Q.Promise<void>} A Promise
     */
    respond (e) {
        let value = this.$(e.target).val();

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, value);
    }

    /**
     * Get the state of the control's validation.
     * @returns {boolean}
     */
    validate () {
        if (this.answer) {
            return super.validate();
        }
        return !this.mandatory;
    }

    /**
     * Handles the input event in the textarea.
     * This will update the character count text.
     * @param {Event} e - Key up event objects
     * @returns {string} The return value from the event.
     */
    inputHandler (e) {
        let max = parseInt(this.model.get('maxLength'), 10),
            elem = $(e.target);

        this.sanitize(e);

        let val = elem.val(),
            count = val.length;

        if (count > max) {
            val = val.substr(0, max);
            elem.val(val.substr(0, max));
            count = max;
            elem.blur();
            Q()
            .then(() => {
                elem.focus();
            })
            .done();
        }

        this.setCounter(max - count);

        this.respond(e);

        return e.returnValue;
    }

    /**
     * Handles "edit" events like cut, copy, paste.  At this point, these are disabled, so this just does a preventDefault().
     * @param {Event} e - The event object.
     */
    editHandler (e) {
        e.preventDefault();
    }

    /**
     * Updates the character counter text.
     * @param {number} charsLeft - The text to insert in the counter.
     */
    setCounter (charsLeft) {
        if (this.showCount) {
            $(`#${this.id}-char-count`).html(charsLeft.toString());
        }
    }

    /**
     * Validates particular properties of the model to be sure they
     * conform to particular types.
     */
    validateModel () {
        if (!_.has(this.model.attributes, 'maxLength')) {
            throw new Error('FreeTextBox widget must have a maxLength defined');
        }

        if (isNaN(parseInt(this.model.get('maxLength'), 10))) {
            throw new Error('FreeTextBox widget must have a maxLength value of type integer');
        }

        if (_.has(this.model.attributes, 'rows')) {
            if (isNaN(parseInt(this.model.get('rows'), 10))) {
                throw new Error('FreeTextBox widget must have a rows property of type integer');
            }
        }
    }
}

window.LF.Widget.FreeTextBox = FreeTextBox;

export default FreeTextBox;
