import IntervalSpinnerBase from './IntervalSpinnerBase';

const DEFAULT_MODAL_TEMPLATE = 'DEFAULT:DateSpinnerModal';
const DEFAULT_DISPLAY_FORMAT = 'LL';
const DEFAULT_STORAGE_FORMAT = 'DD MMM YYYY';

/**
 * Variation of Interval Spinner for Date only.
 * This is enforced by the validateUI() function, so that a spinner containing a time or one not containing a date is disallowed.
 */
class DateSpinner extends IntervalSpinnerBase {
    /**
     * default modal template
     * @returns {string}
     */
    get defaultModalTemplate () {
        return DEFAULT_MODAL_TEMPLATE;
    }

    get defaultDisplayFormat () {
        return DEFAULT_DISPLAY_FORMAT;
    }

    get defaultStorageFormat () {
        return DEFAULT_STORAGE_FORMAT;
    }

    get defaultInitialDate () {
        let dt = new Date();

        // Use floor of initial date.
        dt.setHours(0, 0, 0, 0);
        return dt;
    }

    /**
     * Construct the number spinner
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model model for this widget.
     * @param {string<translated>} [options.model.label] {dev-only} label for the spinner.
     * @param {Date} [options.model.initialDate=new Date()] (Date) the initial date of this control.
     * @param {Date} [options.model.minDate=new Date() - 30 years] (Date) the minimum date allowed by the spinner.
     * @param {Date} [options.model.maxDate=new Date() + 30 years] (Date) the maximum date allowed by the spinner.
     * @param {string} [options.model.displayFormat=LL] {dev-only} Format (See moment.js documentation)
     *                  used for the textbox after a date has been selected.
     * @param {string} [options.model.storageFormat=YYYY-MM-DD] {dev-only} Format (See moment.js documentation)
     *                  used to store the answer for this question.
     * @param {string} [options.model.dateFormat=LL] {dev-only} Format (See moment.js documentation)
     *                  used to create and populate the spinners.  NOTE:  Spinners will be created with
     *                  orders and values corresponding to the current user's locale settings
     * @param {number} options.model.id {dev-only} id for this widget.
     * @param {FlowOptions} [options.model.flow=calculate] {dev-only} Choice of flow option ("calcuate" | "natural" | "rtl" | "ltr")
     * @param {string} [options.model.type=NumberSpinner] {dev-only} The widget type.
     * @param {Object} [options.model.labels={}] {dev-only} The label strings.  Used to replace the label placeholders in the modal dialog template.
     *                                                      There are no labels on the default templates, so a custom template will be necessary to do this.
     * @param {string<translated>} [options.model.okButtonText=OK] {dev-only} OK button text.
     * @param {string} [options.model.className=NumberSpinner] {dev-only} The CSS classname for the widget.
     * @param {Object} [options.model.templates] {dev-only} The templates object.
     * @param {string} [options.model.templates.input] {dev-only} The template for the input control.  Defaults to a normal looking textbox.
     * @param {string} [options.model.templates.modal] {dev-only} The template for the modal dialog control.  Defaults to a single container containing a date-container div.
     *                                                      This dialog can be customized as follows (see core templates for examples):
     *                                                      <ol>
     *                                                          <li>Using date-container CSS class with NO data-format will result in it using dateFormat (passed in).</li>
     *                                                          <li>Using date-container CSS class with data-format will use that format instead of the parameter passed in.</li>
     *                                                          <li>Each date part can also be placed separately, using divs with the following CSS classes.  Note, each of these containers
     *                                                                  can also have a data-format to specify the number format of that date part (i.e. YYYY or YY for the year container).
     *                                                                  <ul>
     *                                                                      <li>year-spinner-container:  Will contain year spinner.</li>
     *                                                                      <li>month-spinner-container:  Will contain year spinner.</li>
     *                                                                      <li>day-spinner-container:  Will contain year spinner.</li>
     *                                                                  <ul>
     *                                                          </li>
     *                                                      </ol>
     */
    // eslint-disable-next-line
    constructor (options) {
        super(options);
    }

    /**
     * Verify all necessary containers exist.
     */
    validateUI () {
        if (this.yearIndex === null && this.monthIndex === null && this.dayIndex === null) {
            throw new Error('Invalid template for DateSpinner widget. Expected day + month + year containers');
        }
        if (this.hourIndex !== null || this.minuteIndex !== null) {
            throw new Error(`Invalid template for DateSpinner widget. Hour and minute containers must not exist.
            Use TimeSpinner or DateTimeSpinner if you wish to include these.`);
        }
    }
}

window.LF.Widget.DateSpinner = DateSpinner;

export default DateSpinner;
