import TextBox from './TextBox';
import ModalLauncherMixin from './ModalLauncherMixin';
import { mix } from 'core/utilities/languageExtensions';

const DEFAULT_WRAPPER_TEMPLATE = 'DEFAULT:FormGroup',
    DEFAULT_CLASS_NAME = 'Modal',
    DEFAULT_LABEL_TEMPLATE = 'DEFAULT:Label',
    DEFAULT_INPUT_TEMPLATE = 'DEFAULT:TextBox',
    DEFAULT_LABELS = {};

class ModalWidgetBase extends mix(TextBox).with(ModalLauncherMixin) {
    /**
     * default class name for this widget, unless overridden by model.
     * @type {string}
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * default labels for modal dialog (keys to be translated)
     * @type {Object}
     */
    get defaultLabels () {
        return DEFAULT_LABELS;
    }

    /**
     * default template for the wrapper of this widget, unless overidden by model.
     * @type {string}
     */
    get defaultWrapperTemplate () {
        return DEFAULT_WRAPPER_TEMPLATE;
    }

    /**
     * default template for the label of this widget, unless overidden by model.
     * @type {string}
     */
    get defaultLabelTemplate () {
        return DEFAULT_LABEL_TEMPLATE;
    }

    /**
     * default template for the input of this widget, unless overidden by model.
     * @type {string}
     */
    get defaultInputTemplate () {
        return DEFAULT_INPUT_TEMPLATE;
    }

    constructor (options) {
        super(options);

        /**
         * Input element jQuery selector
         * @type {jQuery}
         */
        this.$input = null;

        _.extend(this.events, {
            'focus input[type=tel], input[type=text]': 'openDialog'
        });
    }

    /**
     * Overridden to have handler for modal dialog.  Doesn't work as a regular event
     * (in the events object) so create and destroy manually
     */
    delegateEvents () {
        super.delegateEvents();
    }

    /**
     * Overridden to also cancel custom events.
     */
    undelegateEvents () {
        super.undelegateEvents();
    }

    /**
     * Add custom events that could not be set the Backbone way
     */
    addCustomEvents () {
        super.addCustomEvents();
        if (this.$modal) {
            this.$input.on('remove', this.destroy);
        }
    }

    /**
     * Remove custom events that could not be set the Backbone way
     */
    removeCustomEvents () {
        super.removeCustomEvents();
        if (this.$modal) {
            this.$input.off('remove', this.destroy);
        }
    }

    /**
     * Overrideable hook for when the dialog is closing.
     * @param {Event} e The event args
     * @returns {Q.Promise<void>} the promise if not coming from an event handler
     *      , or undefined if it is.
     */
    dialogClosing (e) {
        let ret = Q();

        ret = this.getModalValuesString()
        .then((answer) => {
            this.value = answer;
            this.$input.trigger('input');
        });

        // If called directly from an event handler, close the promise,
        // and return undefined (value of ret.done())
        //  Otherwise return the promise to be handled by the caller.
        return e instanceof $.Event ? ret.done() : ret;
    }

    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.defaultInputTemplate,
            $wrapper,
            answer = null;

        return Q()
        .then(() => {
            this.$el.empty();

            let trans = {};
            this.model.get('label') && (trans.label = this.model.get('label'));
            this.model.get('placeholder') && (trans.placeholder = this.model.get('placeholder'));

            return this.i18n(
                trans,
                () => null,
                { namespace: this.getQuestion().getQuestionnaire().id }
            );
        })
        .then((strings) => {
            let wrapperElement,
                labelElement,
                textBoxElement;

            strings.label = strings.label || '';
            strings.placeholder = strings.placeholder || '';

            wrapperElement = this.renderTemplate(templates.wrapper || this.defaultWrapperTemplate, {});

            // If a template label is configured, render it.
            if (strings.label) {
                labelElement = this.renderTemplate(templates.label || this.defaultLabelTemplate, {
                    link: this.model.get('id'),
                    text: strings.label
                });
            }

            textBoxElement = this.renderTemplate(textBox, {
                id: this.model.get('id'),
                placeholder: strings.placeholder || '',
                name: `${this.model.get('id')}-${this.uniqueKey}`,
                className: this.model.get('className') || this.defaultClassName,
                maxLength: this.model.get('maxLength')
            });

            // Append the wrapper to the local DOM and find where to append other elements.
            $wrapper = this.$el.append(wrapperElement).find('[data-container]');

            if (labelElement) {
                $wrapper.append(labelElement);
            }

            // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
            $wrapper.append(textBoxElement)
                .appendTo(this.$el);

            this.$input = $wrapper.find('input');

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            if (this.answer && this.completed) {
                answer = this.isUserTextBox ?
                    JSON.parse(this.answers.at(0).get('response'))[this.model.get('field')] :
                    this.answers.at(0).get('response');
                this.value = answer;
            }

            if (answer !== null) {
                // Trigger the key-up event so this the answer is properly formatted:
                this.$(`#${this.model.get('id')}`).trigger('input');
            }

            return this.i18n(this.getModalTranslationsObject(),
                () => null,
                { namespace: this.getQuestion().getQuestionnaire().id }
            );
        })
        .then((modalStrings) => {
            return this.renderModal(modalStrings);
        })
        .then(() => {
            this.delegateEvents();
        });
    }
}

export default ModalWidgetBase;
