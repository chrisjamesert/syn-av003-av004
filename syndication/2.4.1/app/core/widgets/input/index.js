import MultipleChoiceInput from './MultipleChoiceInput';
import NumberSpinnerInput from './NumberSpinnerInput';
import ValueSpinnerInput from './ValueSpinnerInput';

export default { MultipleChoiceInput, NumberSpinnerInput, ValueSpinnerInput };
