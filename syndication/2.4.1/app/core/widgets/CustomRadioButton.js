import RadioButton from './RadioButton';

/**
 * CustomRadioButton is identical to the RadioButton except that it has a different CSS class that creates buttons with no icons and centered text.
 */
class CustomRadioButton extends RadioButton {
    /**
     * Constructs a CustomRadioButton Class.
     * @param {Object} options The options used to create the RadioButton
     * @param {Object} options.model The model containing the specific configuration for the widget.  This is represented by the "widget" property in the question definition in the study design.
     * @param {string} options.model.id {dev-only} The ID associated with the model.
     * @param {string} [options.model.className=CustomRadioButton] {dev-only} The CSS classname applied to the widget.
     * @param {Array} options.model.answers The list of answer options used to render the widget.  Each element of the array should an object of the form <code>{ text: 'ONE', value: '1' }</code>
     * @param {Object} options.model.templates {dev-only} Contains any display templates to override the widget's defaults.
     * @param {string} [options.model.templates.wrapper=DEFAULT:RadioButtonWrapper] {dev-only} The template used to render the wrapper element for the widget.
     * @param {string} [options.model.templates.input=DEFAULT:RadioButton] {dev-only} The template used to render the input elements for the widget.
     * @param {string} [options.model.templates.label=DEFAULT:RadioButtonLabel] {dev-only} The template used to render the labels attached to the inputs of the widget.
     */
    // eslint-disable-next-line no-useless-constructor
    constructor (options) {
        super(options);
    }

    // noinspection JSMethodCanBeStatic
    /**
     * The class of the root element.
     * @returns {String}
     */
    get className () {
        return 'CustomRadioButton';
    }
}

window.LF.Widget.CustomRadioButton = CustomRadioButton;
export default CustomRadioButton;
