/** #depends MultipleChoiceBase.js
 * @file Defines a checkbox widget.
 * @author <a href="mailto:chopkins@phtcorp.com">Corey Hopkins</a>
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.7.0
 */
// jscs:disable requireShorthandArrowFunctions

import MultipleChoiceBase from './MultipleChoiceBase';

// - Core Modules
import AnswerOptions from '../collections/AnswerOptions';

class CheckBox extends MultipleChoiceBase {
    // noinspection JSMethodCanBeStatic
    get className () {
        return 'CheckBox';
    }

    // noinspection JSMethodCanBeStatic
    get wrapper () {
        return 'DEFAULT:CheckBoxWrapper';
    }

    // noinspection JSMethodCanBeStatic
    get input () {
        return 'DEFAULT:CheckBox';
    }

    // noinspection JSMethodCanBeStatic
    get label () {
        return 'DEFAULT:CheckboxLabel';
    }

    /**
     * Constructs the checkbox widget class.
     * @param {Object} options Contains the configuration for the widget.
     * @param {Object} options.model The model containing the specific configuration for the widget, as represented by the "widget" property in the question configuration in the study design.
     * @param {number} options.model.id {dev-only} The ID associated with the widget.
     * @param {string} [options.model.className=CheckBox] {dev-only} The CSS classname applied to the widget.
     * @param {Object} [options.model.templates] {dev-only} Contains any template overrides for rendering the widget.
     * @param {string} [options.model.templates.wrapper=DEFAULT:CheckBoxWrapper] {dev-only} The template used as the overall widget wrapper element.
     * @param {string} [options.model.templates.input=DEFAULT:CheckBox] {dev-only} The template used to render the input control.
     * @param {string} [options.model.templates.label=DEFAULT:CheckboxLabel] {dev-only} The template used to render the label attached to the checkboxes.
     * @param {Array} options.model.answers The list of answer options.  Each element in the array should be an object of the form <code>{ text: 'TEXT', IT: 'CB_IT_XYZ', value: '1', exclude: [Array of answers to exclude] }</code>
     */
    constructor (options) {
        /**
         * List of the widgets events
         * @readonly
         * @enum {Event}
         */
        this.events = {

            /** Checkbox input change event*/
            'change input[type=checkbox]': 'respond'
        };

        this.options = options;
        super(options);

        /**
         * Local property caching the answers collection
         * Defer populating this until setupAnswers() is called.
         * @type {AnswerOptions}
         */
        this.answerOptions = new AnswerOptions();
    }

    /**
     * Setup our answers with options either passed in or generated dynamically from a function.
     * Write those answers into the local collection of answerOptions, and create the mutual exclusion matrix.
     * @returns {Q.Promise<void>} when the answer setup is complete
     */
    setupAnswers () {
        return Q()
        .then(() => {
            // Array of previous answer values
            let previousAnswerValues = null,
                widgetCompleted = false;

            this.answerOptions = new AnswerOptions(this.model.get('answers'));

            // Get previous answers, and store them before removing our collection and rebuilding.
            if (this.answers && this.completed) {
                previousAnswerValues = new Array(this.answers.length);
                for (let i = 0, len = this.answers.length; i < len; ++i) {
                    previousAnswerValues[i] = this.answers.at(i).get('response') || '0';
                }
            }

            // Remove answers so we can use the ones generated dynamically.
            this.removeAllAnswers();

            // Re-populate all options, keeping old values if appropriate
            for (let i = 0, len = this.answerOptions.length; i < len; ++i) {
                let curOption = this.answerOptions.at(i),
                    response;

                // previously selected answers, or default to whatever is in the answer model
                if (previousAnswerValues != null && previousAnswerValues.length > i) {
                    response = previousAnswerValues[i];
                } else {
                    response = curOption.get('response') || '0';
                }

                this.addAnswer({
                    IT: curOption.get('IT'),
                    response
                });

                widgetCompleted = widgetCompleted || (response === '1');
            }

            this.completed = widgetCompleted;

            if (this.completed && this.skipped) {
                this.skipped = false;
            }

            this.createMutualExclusionMatrix();
        });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>} fulfilled when checkbox is finished being rendered
     */
    render () {
        let modelId = this.model.get('id'),
            that = this;

        return this.setupAnswers()
        .then(() => {
            return this.buildHTML('checkbox');
        })
        .then(
            () => {
                let promises = [];
                this.answers.each((model, i) => {
                    if (model.get('response') === '1') {
                        promises.push(Q.Promise((resolve) => {
                            that.$(`input[id=${modelId}_checkbox_${i}]`)
                                .attr('checked', true)
                                .closest('.btn')
                                .addClass('active')
                                .ready(() => {
                                    resolve();
                                });
                        }));
                    }
                });
                return Q.allSettled(promises);
            }
        )
        .then(() => {
            return Q.Promise((resolve) => {
                this.$el.appendTo(this.parent.$el)
                    .ready(() => {
                        resolve();
                    });
            });
        })
        .then(() => {
            that.setLabelHeight();
        })
        .then(() => {
            that.delegateEvents();
            return Q();
        });
    }

    /**
     * This method creates a matrix for mutual exclusion.
     * @example this.createMutualExclusionMatrix();
     */
    createMutualExclusionMatrix () {
        let length = this.answerOptions.size();

        this.matrix = [];

        this.answerOptions.each((item, i) => {
            let exclusives = [],
                answerOption = item,
                exclude = answerOption.get('exclude') || {},
                z = 0,
                j,
                idx;

            // crete exclusives with new Array(length) so that unassigned array elements exist with a value of
            // undefined (David Peterson)
            // array fill() doesn't work in android
            for (j = 0; j < length; j++) {
                exclusives[j] = undefined;
            }

            do {
                if (exclude[z] === 'All') {
                    for (idx = 0; idx < length; idx += 1) {
                        if (idx !== i) {
                            exclusives[idx] = true;
                        }
                    }
                } else {
                    j = this.getAnswerOptionIndex(exclude[z]);
                    if (j !== undefined && j !== i) {
                        exclusives[j] = true;
                    }
                }
                z += 1;
            } while (z < exclude.length && exclude[z - 1] !== 'All');

            this.matrix[i] = exclusives;
        });
    }

    /**
     * This method returns a answer option's order number.
     * @param {Number} val Answer option's value
     * @return {Number} Answer option order number
     */
    // eslint-disable-next-line consistent-return
    getAnswerOptionIndex (val) {
        for (let idx = 0, length = this.answerOptions.size(); idx < length; idx += 1) {
            if (this.answerOptions.at(idx).get('value') === val) {
                return idx;
            }
        }
    }

    /**
     * This method is for deselecting exclusive answer options
     * @param {Number} order This is the order number of a answer option
     */
    unselectExclusives (order) {
        let modelId = this.model.get('id');

        this.answerOptions.each((item, i) => {
            if (this.matrix[order][i] === true || this.matrix[i][order] === true) {
                this.$(`#${modelId}_checkbox_${i}`)
                    .prop('checked', false)

                    // Remove the active class from the label.
                    .closest('.btn')
                    .removeClass('active');
            }
        });
    }

    /**
     * Responds to user input.
     * @param {Event} e Event data
     * @return {Q.Promise<void>} promise resolved when complete.
     */
    respond (e) {
        let responses = [],
            completed = false,
            index = this.getAnswerOptionIndex(this.$(e.target).val());

        this.unselectExclusives(index);

        // "function" needed for scope.
        this.$('input').each((ndx, obj) => {
            if ($(obj).prop('checked')) {
                responses.push('1');
                completed = true;
            } else {
                responses.push('0');
            }
        });
        if (completed && this.skipped) {
            this.skipped = false;
        }

        let answerArray = [];
        this.answers.each((answer, index) => {
            answerArray.push(this.respondHelper(answer, responses[index], completed));
        });
        return Q.allSettled(answerArray);
    }
}

window.LF.Widget.CheckBox = CheckBox;
export default CheckBox;
