/**
 * @file Base class for matrix-based widgets.
 * @author <a href="mailto:brian.janaszek@ert.com">bjanaszek</a>
 * @version 1.0
 * @todo Specific styles for individual cells (maybe handled by the individual widgets?)
 */
import WidgetBase from './WidgetBase';
import Question from 'core/models/Question';

export default class MatrixBase extends WidgetBase {
    /**
     * Gets the default container template.
     * @returns {string}
     */
    get container () {
        return 'DEFAULT:MatrixContainer';
    }

    /**
     * Gets the default header template.
     * @returns {string}
     */
    get header () {
        return 'DEFAULT:MatrixHeaderContainer';
    }

    /**
     * The default header item template.
     * @returns {string}
     */
    get headerItem () {
        return 'DEFAULT:MatrixHeaderItem';
    }

    /**
     * The default row container template.
     * @returns {string}
     */
    get rowContainer () {
        return 'DEFAULT:MatrixRowContainer';
    }

    /**
     * The default item wrapper template.
     * @returns {string}
     */
    get wrapper () {
        return 'DEFAULT:MatrixItemWrapper';
    }

    /**
     * @param {Object} options - Widget construction optins
     * @param {Array} options.questions - Array of question models
     * @param {Array} [options.matrixAnswers] - Array of answer text to be used as the column headers.  Defaults to an empty array if not provided.
     */
    constructor (options) {
        this.options = options;
        super(options);

        this.matrixAnswers = this.model.get('matrixAnswers') || [];
        this.questionWidth = this.model.get('questionWidth') || null;
        this.rowColor = this.model.get('rowColor') || null;
        this.alternatingRowColor = this.model.get('alternatingRowColor') || null;

        this.headerRowColor = this.model.get('headerRowColor') || null;
    }

    /**
     * Handles the rendering of the matrix widget.
     * The widget's questions are rendered by iterating through the configuration and creating new QuestionViews for each question.  These QuestionViews are appended to this widget's container.
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            containerTemplate = templates.container || this.container,
            container;

        this.questionViews = this.buildQuestions();

        // Main container
        container = this.renderTemplate(containerTemplate, {
            className: this.model.get('className')
        });

        this.$container = this.$el.html(container);
        this.$el.appendTo(this.parent.$el);

        this.buildHeader();

        return this.renderQuestions()
        .then(() => {
            return this.setQuestionWidth();
        })
        .then(() => {
            return this.setRowColors();
        })
        .then(() => {
            return this.setHeaderRowColor();
        });
    }

    /**
     * Sets the row colors in the matrix.
     * @returns {Q.Promise<void>}
     */
    setRowColors () {
        return new Q.Promise((resolve) => {
            let altRowColor;

            if (this.rowColor) {
                this.$('div.question.table-row:nth-child(even)').css('background-color', this.rowColor);

                if (this.alternatingRowColor) {
                    altRowColor = this.alternatingRowColor;
                } else {
                    altRowColor = this.rowColor;
                }

                this.$('div.question.table-row:nth-child(odd)').css('background-color', altRowColor);
            }

            resolve();
        });
    }

    /**
     * Sets the background color for the header row, if applicable.
     * @returns {Q.Promise<void>}
     */
    setHeaderRowColor () {
        return new Q.Promise((resolve) => {
            if (this.headerRowColor) {
                this.$('div.header').css('background-color', this.headerRowColor);
            }

            resolve();
        });
    }

    /**
     * Sets the question column width.
     * @returns {Q.Promise<void>}
     */
    setQuestionWidth () {
        return new Q.Promise((resolve) => {
            if (this.questionWidth) {
                let width = `0 0 ${this.questionWidth}`;
                this.$('div.col0').css('flex', width);
            }
            resolve();
        });
    }

    /**
     * Renders the question views for the matrix.
     * @returns {Q.Promise<void>}
     */
    renderQuestions () {
        let addQuestionPromiseFactories = _(this.questionViews).map((questionView) => {
            return () => {
                let questionID = questionView.id;
                return questionView.render(`.${this.model.get('className').split(' ').join('.')}.container-fluid`)
                .then(() => {
                    // Tack on col classes
                    let $row = this.$(`#${questionID}`);

                    // Question text column
                    $($row.find('.matrix-question')[0]).addClass('col0');

                    // Button columns
                    let items = $row.find('.item.btn');
                    for (let i = 0; i < items.length; i += 1) {
                        $(items[i]).addClass(`col${i + 1}`);
                    }
                });
            };
        });

        return addQuestionPromiseFactories.reduce(Q.when, Q());
    }

    /**
     * Build out the container HTML for the widget.
     * @returns {Array} An array of QuestionViews
     */
    buildQuestions () {
        let qvs = [],
            view,
            questions = this.model.get('questions'),
            len = questions.length;

        for (let i = 0; i < len; i++) {
            view = new LF.View.QuestionView({
                id: questions[i].id,
                model: new Question(questions[i]),
                parent: this.getQuestion().parent,
                mandatory: this.options.mandatory,
                ig: this.parent.model.get('IG'),
                igr: this.parent.model.get('IG') ? this.getQuestion().getQuestionnaire().getCurrentIGR(this.parent.model.get('IG')) : null,
                localOnly: questions[i].localOnly || false
            });

            // Adjust the widget's $el to be a matrix-wrapper, too:
            view.widget.$el.addClass('matrix-wrapper');
            view.widget.$el.addClass('item');

            qvs.push(view);
        }

        return qvs;
    }

    /**
     * Generate the header, if applicable.
     * Any extensions of this class can handle this on their own.
     */
    buildHeader () {
        let templates = this.model.get('templates') || {},
            headerTemplate = templates.headerTemplate || this.header,
            headerItem = templates.headerItem || this.headerItem,
            $itemContainer,
            $headerContainer,
            className,
            stringsToFetch = [];

        let headerText = this.model.get('header') || [];

        // Create the header container:
        $headerContainer = $(this.renderTemplate(headerTemplate));
        $headerContainer.appendTo(`div.${this.model.get('className').split(' ').join('.')}.container-fluid`);

        // @todo: Need to look up the resources, not just print them.
        for (let i = 0; i < headerText.length; i += 1) {
            stringsToFetch[i] = {
                key: headerText[i],
                namespace: this.getQuestion().getQuestionnaire().id
            };
        }

        LF.getStrings(stringsToFetch, (strings) => {
            for (let i = 0; i < strings.length; i += 1) {
                className = i === 0 ? 'matrix-question' : 'item';

                if (strings[i] === '{{  }}') {
                    strings[i] = '';
                }

                $itemContainer = $(this.renderTemplate(headerItem, { text: strings[i], className }));

                $itemContainer.addClass(`col${i}`);
                $headerContainer.append($itemContainer);
            }
        });
    }

    /**
     * This widget won't actually handle any responses.  Rather, it will rely on the collection of widgets to handle their own responses.
     * @returns {Q.Promise<void>} Resolved promise.
     */
    respond () {
        return Q();
    }

    /**
     * Determines if the widget's response state is valid.
     * @return {boolean} True if the widget is valid.
     */
    validate () {
        // @todo: Iterate through all questions and check their state.
        let len = this.questionViews.length;

        for (let i = 0; i < len; i += 1) {
            if (!this.questionViews[i].validate()) {
                return false;
            }
        }

        this.completed = true;

        return true;
    }

    /**
     * Clean up this view and all questions within this view.
     * @returns {Q.Promise<void>}
     */
    remove () {
        return this.questionViews.reduce((chain, questionView) => {
            return chain
            .then(() => questionView.remove());
        }, Q())
        .then(() => {
            return super.remove();
        });
    }
}

window.LF.Widget.MatrixBase = MatrixBase;
