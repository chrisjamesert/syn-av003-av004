import SelectWidgetBase from './SelectWidgetBase';

export default class SecretQuestionList extends SelectWidgetBase {
    /**
     *  @constructs SecretQuestionList
     *  @param {Object} [options={}] The options for the class.
     */
    constructor (options = {}) {
        super(_.extend({

            /**
             * @property {Object} events A map of events to delegate.
             */
            events: {
                'change select': 'onAnswerChange',
                'click select': 'respond'
            }
        }, options));
    }

    get input () {
        return 'DEFAULT:SelectWidget';
    }

    /**
     * Extension of SelectWidgetBase for a particular instance of a Select Widget control.
     * @param {String} divToAppendTo the select ID that the options should be appended to
     */

    renderOptions (divToAppendTo) {
        let questions = LF.StudyDesign.securityQuestions,
            translationKeys = {};

        _(questions).each((value) => {
            if (value.display) {
                translationKeys[value.text] = value.text;
            }
        });

        return this.i18n(translationKeys)
        .then((translations) => {
            let answer = this.answer ? JSON.parse(this.answer.get('response'))[this.model.get('field')] : false;

            questions.forEach((question) => {
                if (question.display) {
                    let $el = $(`<option value="${question.key}">${translations[question.text]}</option>`);

                    // DE16369 - question.key is an integer, so we need to parse the stored answer (string) as an integer.
                    if (answer && question.key === parseInt(answer, 10) || this.$(divToAppendTo).children().size() === 0) {
                        this.needToRespond = { value: '' };
                        $el.attr('selected', 'selected');
                    }

                    this.$(divToAppendTo).append($el);

                    // Clear out the DOMElement to prevent memory leaks.
                    $el = null;
                }
            });
        });
    }

    /**
     * Clears any defined widgets on input change.
     * @param  {Event} e The event args.
     */
    onAnswerChange (e) {
        let clearOnChange = this.model.get('clearOnChange');

        if (clearOnChange && clearOnChange.length) {
            clearOnChange.forEach((id) => {
                let view = _.find(this.questionnaire.getQuestions(), (view) => {
                    return view.model.get('widget').id === id;
                });

                view.widget.clear();
            });
        }

        this.respond(e);
    }
}

window.LF.Widget.SecretQuestionList = SecretQuestionList;
