/* eslint-disable no-console */
import Log from './models/Log';

import coreLoggingConfig from 'core/assets/logging-config';
import trialLoggingConfig from 'trial/common_study/logging-config';
import { mergeObjects } from 'core/utilities/languageExtensions';

const config = mergeObjects(coreLoggingConfig, trialLoggingConfig);

let logDepth = 0;
let depthResetTimer;
let t0 = Date.now();
let subjectLogLevel = 'OFF'; // if not changed, sysLevel will always beat this
let defaultLogLevel;
let logToConsole;
let orderedLevels = ['ALL', 'TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'OPERATIONAL', 'FATAL', 'OFF'];
// eslint-disable-next-line no-sequences
let levelMap = _.reduce(orderedLevels, (prev, curr) => (prev[curr] = curr, prev), {});
let rootLogger; // initialized at bottom
let loggerLogger; // initialized at bottom
let sloppyType = 'AdHoc'; // If type is not specified, default to this type
let showElapsed = false;
let allLoggers = [];

export default class Logger {
    constructor (name, { consoleOnly, consoleLevel, traceStacks, operationalToConsole } = {}) {
        _.defaults(this, { name, consoleOnly, consoleLevel, traceStacks, operationalToConsole },

            // settings for this exact logger name
            config.loggers[name],

            // configured defaults
            config.loggers['*'],

            // hard-coded defaults
            {
                consoleOnly: false,
                consoleLevel: 'TRACE',
                traceStacks: false,
                operationalToConsole: true
            });

        // These are global, set them if not previously set
        ({ defaultLogLevel, logToConsole, showElapsed } = _.defaults({ defaultLogLevel, logToConsole }, config));

        // maps Logger levels to console levels
        this.levelToConsole = {
            // 'trace' may be dangerous, dumps stack traces
            TRACE: this.traceStacks ? 'trace' : 'debug',
            DEBUG: 'debug',
            INFO: 'info',
            WARN: 'warn',
            ERROR: 'error',
            OPERATIONAL: 'log',
            FATAL: 'error'
        };

        // Contains mapping of handlebars-style messages to executable lodash templates
        this.compiledTemplates = {};

        // this check must be done after Logger is fully constructed, because
        // it may result in a logger.warn, which requires this.stuff
        this.consoleLevel = this.ensureValidLevel(this.consoleLevel);
        allLoggers.push(this);
    }

    static get levels () {
        return orderedLevels;
    }

    static getAllLoggers () {
        return _.clone(allLoggers);
    }

    static findLoggerNamed (name) {
        let res = _.filter(allLoggers, logger => logger.name === name)[0];
        return res;
    }

    reconfigure ({ consoleOnly, consoleLevel, traceStacks, operationalToConsole } = {}) {
        _.extend(this, { consoleOnly, consoleLevel, traceStacks, operationalToConsole });
        console.log(this);
    }

    ensureValidLevel (level) {
        if (!levelMap[level]) {
            loggerLogger.warn(`Logger ${this.name} got invalid log level ${level}; will use default level ${defaultLogLevel} instead.`);
            return defaultLogLevel;
        }
        return level;
    }

    static setGlobalsForTesting (opts) {
        // The parens are required ES6 syntax; without them, that fist open curly
        // is considered to introduce a block statement.
        ({ logDepth, t0, subjectLogLevel, logToConsole, defaultLogLevel, showElapsed } = _.defaults({}, opts, {
            logDepth,
            t0,
            subjectLogLevel,
            logToConsole,
            defaultLogLevel,
            showElapsed
        }));
    }

    static getGlobalsForTesting () {
        return { logDepth, t0, subjectLogLevel, defaultLogLevel, logToConsole, showElapsed };
    }

    // Process templates of the form 'bla bla {{ foo }} bla bla', pulling value of foo from valMap
    // For performance reasons, the template is compiled only once, and cached
    processTemplate (templateString, valMap) {
        // valMap comes from nvp's of logging statement, and may not really be an Object
        if (typeof valMap !== 'object') {
            return templateString;
        } else if (templateString.indexOf('{{') === -1) {
            // don't bloat cache with plain strings
            return templateString;
        }
        let compiled;
        if (this.compiledTemplates[templateString]) {
            compiled = this.compiledTemplates[templateString];
        } else {
            compiled = _.template(templateString);
            this.compiledTemplates[templateString] = compiled;
        }
        return compiled(valMap);
    }

    /* Usage:
       Logger.getPath('SomeGlobalObj.path.with.that.obj')
       or
       Logger.getPath(someObj, 'path.within.obj')
       Returns undefined if path cannot be followed to end

       There's a function like this in Utilities.js, but that pulls in
       too many dependencies (which is a problem in and of itself.)

       (close to being able to get rid of this method now; only LF.dataAccess left)
    */
    static getPath (obj, dotPath) {
        // eslint-disable-next-line prefer-rest-params
        if (arguments.length === 1) {
            dotPath = obj;
            obj = window || global; // eslint-disable-line no-undef
        }
        return dotPath.split('.')
            .reduce((o, i) => {
                return o ? o[i] : undefined;
            }, obj);
    }

    static setSubjectLoggingLevel (level) {
        subjectLogLevel = rootLogger.ensureValidLevel(level);
    }

    isLevelPrintable (level) {
        if (level === 'OPERATIONAL' && this.operationalToConsole === false) {
            return false;
        }
        return orderedLevels.indexOf(level) >= orderedLevels.indexOf(this.consoleLevel);
    }

    isLevelStorable (level) {
        // Determine if we're at a logging-level that wants to store this log level.
        // We'll first see if we have a subject active and if so, use his value.
        // If not, we'll use the study default value.
        // If none at all, we'll assume TRACE level
        let sysLevel = defaultLogLevel || 'ERROR';

        // Use the MOST-verbose log level of either subject or system
        if (orderedLevels.indexOf(subjectLogLevel) < orderedLevels.indexOf(sysLevel)) {
            sysLevel = subjectLogLevel;
        }

        if (level === 'OPERATIONAL') {
            // ALWAYS process OPERATIONAL messages regardless of any level set
            return true;
        } else if (orderedLevels.indexOf(level) >= orderedLevels.indexOf(sysLevel)) {
            return true;
        }
        return false;
    }

    /*
        NVP's going to db must be simple string:string pairs
        if caller gave us complex object, we need to simplify it.
        JSON.stringify is an option, but it would choke on some
        inputs. And it might generate huge values.  The following
        is 'lossy', but prevents abuse of Logging Service.
    */
    nvpsForStorage (data) {
        if (_.isNull(data) || _.isUndefined(data)) {
            return undefined;
        } else if (_.isArray(data)) {
            // Not supposed to pass in arrays.
            // Arrays *are* objects, but you would not expect
            // their indexes to be treated as NVP keys
            // Logger.toConsole('warn', 'Got array value where NVP was expected', data);
            loggerLogger.warn(`Logger ${this.name} got array value where NVP was expected`);
            return { Data: data.toString() };
        } else if (_.isObject(data)) {
            // complex objects need to be simlified to str:str
            let nvp = {};
            _.keys(data).forEach((key) => {
                // convert to string; don't choke on null
                nvp[key] = `${data[key]}`.slice(0, 200); // and limit value size to 200ch
            });
            return nvp;
        }
        loggerLogger.warn(`Logger ${this.name} got string value where NVP was expected: ${data}`);

        // Logger.toConsole('warn', 'Got string value where NVP was expected', data);
        return { Data: data.toString() };
    }

    tryStore (level, message, nameValuePairs, type) {
        if (!Logger.getPath(LF, 'dataAccess')) {
            if (logToConsole !== false) {
                loggerLogger.warn(`Logger ${this.name} attempted to log to db, but dataAccess not ready.`);

                // Logger.toConsole('warn', 'Attempt to log to db, but dataAccess not ready.');
                this.print(level, message, nameValuePairs, type);
            }
            return null;
        }

        try {
            return this.store(level, message, nameValuePairs, type);
        } catch (e) {
            loggerLogger.error(`Logger ${this.name} attempt to store a Log failed`, e);
            this.print(level, message, nameValuePairs, type);
            return null;
        }
    }

    store (level, message, nameValuePairs, type = sloppyType) {
        // sanitize nvp's
        nameValuePairs = this.nvpsForStorage(nameValuePairs);

        // substitue {{expr}} with nvp values
        message = this.processTemplate(message, nameValuePairs);

        let model = new Log({
            type,
            level,
            message,
            source: `${LF.appName} ${LF.coreVersion}`,
            location: this.name,
            clientTime: new Date().toISOString(),
            nameValuePairs: nameValuePairs || null
        });

        return model.save();
    }

    static elapsed () {
        if (showElapsed) { // TODO: not here
            let elapsed = new Date(Date.now() - t0)
                .toISOString()
                .slice(-10, -1); // e.g. 01:23.456
            return `T0+${elapsed}`;
        }
        return '';
    }

    /*
      Output to console, using error/warn/info/debug/trace
      method.  If that method is not available, fall back
      to console.log.
      Had attempted different implementation using function
      pointers, ran into this crazy stuff. In Chrome:

        > console.log('hello')
        hello
        > c_l = console.log
        log() { [native code] }
        > c_l('hello')
        Uncaught TypeError: Illegal invocation(…)(anonymous function)
        c_l === console.log
        true
    */
    static toConsole (method = 'log', ...args) {
        // If preferred method does not exist, degrade to something that does
        method = console[method] ? method : 'log';
        console[method](...args);
    }

    print (level, message, nameValuePairs, type = sloppyType) {
        // substitue {{expr}} with nvp values
        message = this.processTemplate(message, nameValuePairs);

        let args = [Logger.elapsed(), level, `|${this.name}|`, message];

        if (type && type !== sloppyType) {
            args.push(`[${type}]`);
        }

        if (nameValuePairs) {
            args.push(nameValuePairs);

            // The stack trace hyperlink is hard to spot...
            if (nameValuePairs instanceof Error) {
                args.push('\u2190 Click for stack');
            }
        }
        try {
            Logger.toConsole(this.levelToConsole[level], ...args);
        } catch (e) {
            // what to do now?
            // console.error('Logger Error: ', e, consoleMethod);
            // console.log(text);
            // debugger;
        }
    }

    printAndOrStore (level, message, nameValuePairs, type = sloppyType) {
        level = this.ensureValidLevel(level);

        // Log to console, depending on level vs. consoleLevel.
        if (logToConsole !== false) {
            if (this.isLevelPrintable(level)) {
                this.print(level, message, nameValuePairs, type);
            }
        }

        if (this.consoleOnly && level === 'OPERATIONAL') {
            // WTH! You are not allowed to use OPERATIONAL logging on a consoleOnly
            // logger! There's no good solution here. If we try to store anyway, it'll
            // likely cause infinite recursion and crash the browser (have seen this).
            throw new Error(`OPERATIONAL level logging is forbidden from consoleOnly logger ${this.name}`);
        }

        // Are we required to log this? maybe not, depending on levels
        if (!this.consoleOnly && this.isLevelStorable(level)) {
            return this.tryStore(level, message, nameValuePairs, type);
        }
        return null;
    }

    info (message, nameValuePairs, type) {
        return this.printAndOrStore('INFO', message, nameValuePairs, type);
    }

    operational (message, nameValuePairs, type) {
        return this.printAndOrStore('OPERATIONAL', message, nameValuePairs, type);
    }

    debug (message, nameValuePairs, type) {
        return this.printAndOrStore('DEBUG', message, nameValuePairs, type);
    }

    warn (message, nameValuePairs, type) {
        return this.printAndOrStore('WARN', message, nameValuePairs, type);
    }

    fatal (message, nameValuePairs, type) {
        // Try to provide a stack trace
        if (!nameValuePairs && !(nameValuePairs instanceof Error)) {
            let ex = new Error('(forced stack trace)');

            // Attempt to remove junk frames from stack
            try {
                ex.stack = ex.stack.split('\n').slice(2).join('\n');
                ex.name = 'Stack';
                ex.message = '';
            } catch (e) {
                // we tried.
            }
            nameValuePairs = ex;
        }
        return this.printAndOrStore('FATAL', message, nameValuePairs, type);
    }

    error (message, nameValuePairs, type) {
        // If caller erroneously passed Error as first param,
        // generate proper message, and shift Error to NVP slot
        // provided that slot was not also filled
        if (message instanceof Error) {
            Logger.toConsole('warn', 'Logger.error() called with err as first param');
            if (!nameValuePairs) {
                nameValuePairs = message;
            }
            message = message.toString();
        }
        return this.printAndOrStore('ERROR', message, nameValuePairs, type);
    }

    trace (message, nameValuePairs, type) {
        return this.printAndOrStore('TRACE', message, nameValuePairs, type);
    }

    traceEnter (message, nameValuePairs, type) {
        // See to it that nesting resets on every clock tick
        if (depthResetTimer) {
            window.clearTimeout(depthResetTimer);
        }
        depthResetTimer = window.setTimeout(() => {
            for (; logDepth > 0; logDepth--) {
                if (logToConsole && this.isLevelPrintable('TRACE')) {
                    Logger.toConsole('warn', 'A Logger did not properly un-nest traceEnter with traceExit');
                }
                Logger.toConsole('warn', 'A Logger did not properly un-nest traceEnter with traceExit');
                this.traceExit('forced traceExit');
            }

        // We have to give this a bit more time than 0 for any async functions to resolve.
        // We still might encounter warnings for long transmissions to StudyWorks/Expert.
        }, 500);

        if (logToConsole && this.isLevelPrintable('TRACE')) {
            if (console.group) {
                if (nameValuePairs) {
                    console.group(Logger.elapsed(), 'TRACE', `|${this.name}|`, message, nameValuePairs);
                } else {
                    console.group(Logger.elapsed(), 'TRACE', `|${this.name}|`, message);
                }
            } else {
                this.print('TRACE', `ENTER vvvvv       ${message}       vvvvv`, nameValuePairs, type);
            }
        }

        let result = null;
        if (this.isLevelStorable()) {
            result = this.store('TRACE', message, nameValuePairs, type);
        }
        ++logDepth;
        return result;
    }

    traceExit (message) {
        if (logDepth > 0) {
            --logDepth;
        }

        if (logToConsole && this.isLevelPrintable('TRACE')) {
            if (console.groupEnd) {
                console.groupEnd();
            } else {
                this.print('TRACE', `EXIT ^^^^^       ${message}      ^^^^^`);
            }
        }
        let result = null;
        if (this.isLevelStorable()) {
            result = this.store('TRACE', message);
        }
        return result;
    }

    traceSection (name) {
        if (this.isLevelPrintable('TRACE')) {
            this.printAndOrStore('TRACE', `************************* ${name} *************************`);
        }
    }
}
rootLogger = new Logger('ROOT');
loggerLogger = new Logger('Logger', { consoleOnly: true }); // MUST be consoleOnly

LF.Class.Logger = Logger;
