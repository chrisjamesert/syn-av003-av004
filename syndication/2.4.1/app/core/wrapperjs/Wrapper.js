/* eslint-disable */
// This file isn't process via babelify. eslint is disabled until file is updated.

// import Logger from 'core/Logger.js';  THIS CAUSES SyntaxError in console

/**
 * Whether the app is running in the phonegap wrapper.
 * @type Boolean
 * @default false
 */
LF.Wrapper.isWrapped = false;

/**
 * The operating system platform which the wrapper is running on.
 * @type String
 * @default undefined
 */
LF.Wrapper.platform = undefined;

/**
 * The device running the wrapper. Includes device model, operating system, etc.
 * @type Object
 * @default undefined
 */
LF.Wrapper.device = undefined;

/**
 * Detects whether the app is running in the wrapper by looking for the wrapped flag in the URL
 */
LF.Wrapper.detectWrapper = function () {
    var wrappedPlatform = localStorage.getItem('url_wrapped');

    if (!wrappedPlatform) {
        if (typeof(cordova) !== 'undefined' && cordova.platformId) {
            wrappedPlatform = cordova.platformId;
        }
    }

    if (wrappedPlatform === 'ios' || wrappedPlatform === 'android' || wrappedPlatform === 'windows') {
        //Let the application know it is wrapped and on which platform.
        LF.Wrapper.savePlatform(wrappedPlatform);
    }
};

/**
 * Saves the wrapped platform to localstorage
 * @param {string} wrappedPlatform if supplied, the app sets that it is running in the wrapper on that platform.
 */
LF.Wrapper.savePlatform = function (wrappedPlatform) {
    LF.Wrapper.isWrapped = !!wrappedPlatform;
    LF.Wrapper.platform = wrappedPlatform;
    localStorage.setItem('isWrapped', wrappedPlatform);
};

//import Logger from 'core/Logger.js';

/**
 * Manages setting and getting device's serial number
 * @returns {Object} set and get functions for device serial number
 */
 LF.Wrapper.Utils.deviceSerialNumber = function () {
     var setSerial = function (callback) {
         return getSerial(callback);
     };

     var getSerial = function (callback) {
         callback || (callback = $.noop);

         var getAndroidSerial = function () {
             var handleError = function (err) {
                 logger.error(err || 'Failed to set IMEI');
             };
             /* Stakutis...try to support the older IMEI plugin and the new one */
             if (LF.Utilities.getNested('plugin.imei.get')) {
                 // Old
                 return plugin.imei.get(callback);
             } else if (LF.Utilities.getNested('window.plugins.IMEIPlugin.get')) {
                 // New plugin
                 return window.plugins.IMEIPlugin.get(callback, handleError);
             } else {
                 callback(null);
             }
         };

         var getIosSerial = function () {
             // If the app is running on iOS, we cannot use the IMEI plugin.
             // Instead we are stuck using the identifierForVendor property, which is unique
             // to the device across the same vendor.  If the user deletes the app, the UUID will
             // be reset to a different value if the app is installed again.

             // If the IMEI is not set...
             if (localStorage.getItem('PHT_IMEI') == null) {
                 // Check the iOS Keychain for any previously store UUID.
                 Keychain.get(function (uuid) {
                     // If the UUID exists, set it into localStorage.
                     if (uuid) {
                         callback(uuid);
                     } else {
                         // Get the UUID and strip any - chars from it.
                         uuid = window.device.uuid.replace(/[\-\/]/g, '');

                         // iPhone identifierForVendor is 32 chars. IMEI is only 15.
                         uuid = uuid.substr(0, 15);

                         Keychain.set(function () {
                             callback(uuid);
                         }, function () {
                             logger.error('Failed to set IMEI');
                         }, 'eCoaUUID', uuid, false);
                     }
                 }, function (err) {
                     logger.error(err);
                 }, 'eCoaUUID', 'TouchID Message');
             }
         };

         if (LF.Wrapper.isAndroid()) {
             return getAndroidSerial();
         }

         if (LF.Wrapper.isIOS()) {
             return getIosSerial();
         }
     };

     return {
         set: setSerial,
         get: getSerial
     };
 }

/**
 * Sets or determines if the app is running within the wrapper and sets up phonegap code if it is.
 */
LF.Wrapper.initialize = function () {
    // var logger = new Logger('Wrapper');
    // logger.traceSection('Wrapper');
    // logger.traceEnter('initialize');
    var defer = Q.defer();
    var deviceReady = function () {
            // logger.trace('deviceReady: Adding listener...');
            document.addEventListener('deviceready', function () {
                // logger.traceEnter('deviceready event');
                // Create a clear path to the device info (model, platform, version).
                LF.Wrapper.device = window.device;

                LF.Wrapper.Utils.deviceSerialNumber().set(function (serialNum) {
                    localStorage.setItem('PHT_IMEI', serialNum);
                });

                setTimeout(function () {
                    if (navigator.splashscreen) {
                        navigator.splashscreen.hide();
                    }
                }, 1000);

                document.addEventListener('backbutton',
                    function (e) {
                        e.preventDefault();
                    }, false);

                // Windows-specific case for handling the hardware back button
                if (window.WinJS && window.WinJS.Application) {
                    window.WinJS.Application.onbackclick = function (e) {
                        // Return true?  To suppress this?  Yep:
                        // https://msdn.microsoft.com/en-us/library/windows/apps/dn607962.aspx
                        return true;
                    };
                }
                //logger.traceExit('deviceready event');
                defer.resolve();
            }, false);
        };

    LF.Wrapper.platform = localStorage.getItem('isWrapped');
    LF.Wrapper.isWrapped = !!LF.Wrapper.platform;
    if (LF.Wrapper.isWrapped) {
        deviceReady();
    } else {
        defer.resolve();
    }
    return defer.promise;
};

/**
 * Executes execWhenWrapped function once the PhoneGap device is ready if the app is running in the wrapper.
 * Executes execWhenNotWrapped function if the app is not running in the wrapper.
 * @param {Object} params Has execWhenWrapped and execWhenNotWrapped functions.
 */
LF.Wrapper.exec = function (params) {
    if (LF.Wrapper.isWrapped) {
        // If this event has already fired, the function will be run instantly.
        document.addEventListener('deviceready', function () {
            if (_.isFunction(params.execWhenWrapped)) {
                params.execWhenWrapped();
            }
        }, false);
    } else {
        if (_.isFunction(params.execWhenNotWrapped)) {
            // This will be run if LF.Wrapper.exec is called outside of the wrapper.
            params.execWhenNotWrapped();
        }
    }
};

/**
 * Convenience function which determines if the wrapper is running on Android.
 * @returns Boolean True if it is running in the wrapper on Android, false otherwise.
 */
LF.Wrapper.isAndroid = function () {
    return LF.Wrapper.isWrapped && LF.Wrapper.platform === 'android';
};

/**
 * Convenience function which determines if the wrapper is running on iOS.
 * @returns Boolean True if it is running in the wrapper on iOS, false otherwise.
 */
LF.Wrapper.isIOS = function () {
    return LF.Wrapper.isWrapped && LF.Wrapper.platform === 'ios';
};

/**
 * Convenience function which determines if the wrapper is running on Windows.
 * @returns Boolean True if it is running in the wrapper on Windows, false otherwise.
 */
LF.Wrapper.isWindows = function () {
    return LF.Wrapper.isWrapped && LF.Wrapper.platform === 'windows';
};

/**
 * Convenience function which determines if the app is running in the browser.
 * @returns Boolean True if it is running in the wrapper on iOS, false otherwise.
 */
LF.Wrapper.isBrowser = function () {
    return !LF.Wrapper.isWrapped && (LF.Wrapper.platform !== 'ios' && LF.Wrapper.platform !== 'android' && LF.Wrapper.platform !== 'windows');
};

/*
* Convenience function which determines the device's network connection type
* @returns {String | Undefined} Connection type
*/
LF.Wrapper.getConnectionType = function () {
    var networkInfo,
        connectionProfile,
        connectionLevel,
        platform = LF.Wrapper.platform;

    switch (platform) {
        // TODO: Since we've updated the network-information plugin, we might be able to get rid of this check,
        // and just use navigator.connection.type.
        case 'windows':
            networkInfo = Windows.Networking.Connectivity.NetworkInformation;
            connectionProfile = networkInfo.getInternetConnectionProfile();

            // If the device is offline, getNetworkConnectivityLevel is not defined
            if (connectionProfile.getNetworkConnectivityLevel) {
                connectionLevel = connectionProfile.getNetworkConnectivityLevel();
            } else {
                connectionLevel = null;
            }
            return connectionLevel;
            break;
        case 'android':
        case 'ios':
            return navigator.connection.type;
            break;
        default:
            throw new Error('LF.Wrapper.getConnectionType is not supported on this platform: ' + platform);
    }
};

/**
 * Sets android shared preference.
 * @param {String} option preference to set
 * @param {String} value Preference value to set
 * @param {Function=$.noop} callback The callback function to be invoked upon completion.
 */
LF.Wrapper.Utils.setPreference = function (option, value, callback) {
    callback = callback || $.noop;

    if (option !== '' && LF.Wrapper.platform === 'android') {
        window.plugin.androidpreferences.set({
            preferenceLib: 'LogPad',
            preferenceName: option,
            preferenceValue: value
        }, callback, callback);
    }
};

/**
 * Gets android shared preference.
 * @param {String} option preference to get
 * @param {Function} callback The callback function to be invoked upon completion.
 */
LF.Wrapper.Utils.getPreference = function (option, callback) {
    callback = callback || $.noop;

    if (LF.Wrapper.platform === 'android') {
        window.plugin.androidpreferences.get({
            preferenceLib: 'LogPad',
            preferenceName: option,
            preferenceValue: ''
        }, function (returnValue) {
            callback(returnValue);
        }, callback);
    }
};

/**
 * Takes a screenshot of what is currently being displayed and saves it to the device
 * @param {Function} callback The callback function to be invoked upon completion.
 */
LF.Wrapper.Utils.takeScreenshot = function (callback) {
    navigator.screenshot.save(function (error, res) {
        if (error) {
            (callback || $.noop)('');
        } else {
            (callback || $.noop)(res);
        }
    }, 'png', 100);
};

/**
 * Gets the Battery Level
 * @param {Function} callback The callback function to be invoked upon completion.
 */
LF.Wrapper.Utils.getBatteryLevel = function (callback) {
    var plugin,
        batteryLevelNotAvailable = null;

    LF.Wrapper.exec({
        execWhenWrapped: function () {
            if (LF.Wrapper.platform === 'windows') {
                var battery = Windows.Devices.Power.Battery,
                    batteryReport = battery.aggregateBattery.getReport();

                if (!!batteryReport) {
                    callback(
                        ((batteryReport.remainingCapacityInMilliwattHours / batteryReport.fullChargeCapacityInMilliwattHours) * 100).toFixed() + ''
                    );
                } else {
                    callback(batteryLevelNotAvailable);
                }
            } else {
                // Both iOS and Android handled here
                plugin = window.plugin;
                if (plugin != null) {
                    if (!plugin.battery) {
                        callback(batteryLevelNotAvailable);
                        return;
                    }

                    plugin.battery.getInfo(function (info) {
                        callback(info.batteryLevel + '');
                    }, function () {
                        callback(batteryLevelNotAvailable);
                    });
                } else {
                    callback(batteryLevelNotAvailable);
                }
            }
        },
        execWhenNotWrapped: function () {
            callback(batteryLevelNotAvailable);
        }
    });
};
