/* eslint-disable */
// This file isn't process via babelify. eslint is disabled until file is updated.

/**
* Generate a list of ids for alarms and reminders
* @param {Number} start - a unique seed id
* @param {Number} reminders - the number of reminders
* @returns {Number[]} array of numbers to use for alarm ids
*/
LF.Wrapper.Utils.generateIds = function (start, reminders) {
    var idList = [],
        i = 0,
        nextId,
        numReminders = reminders || 0;

    for (i; i <= numReminders; i ++) {
        nextId = start * 1000 + i;
        idList.push(nextId);
    }

    return idList;
};

/**
 * Gets the number of reminders in future for an Alarm on assignment.
 * @param {Number} initialTime Initial time of an Alarm in milliseconds
 * @param {Number} reminders Number of reminders with the Alarm
 * @param {Number} interval Interval in minutes for the reminders
 * @returns {Number} Number of reminders in future.
 * @example var reminders = LF.Wrapper.Utils.getReminders(now.getTime(), 4, 15);
 */
LF.Wrapper.Utils.getReminders = function (initialTime, reminders, interval) {
    var startTime, endTime, now, minLeft,
        milliSecInMin = 60000;

    if (reminders == null || interval == null) {
        return 0;
    }

    startTime = Math.floor(initialTime / milliSecInMin);
    endTime = startTime + (reminders * interval);
    now = Math.floor(new Date().getTime() / milliSecInMin);

    if (now > endTime) {
        return 0;
    } else {
        minLeft = endTime - now;
        return Math.ceil(minLeft / interval);
    }
};
