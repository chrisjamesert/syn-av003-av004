/**
 * The list of study upgrade configurations.
 * You must include custom function per version upgrade.
 * Supports use of callback or returning a promise.
 * @example
 * LF.CoreDBUpgrade = [{
 * 	   version: 1,
 *     upgradeFunction (callback) {
 * 	       callback(isCompleted);
 * 	   }
 * 	 }, {
 *     version: 10,
 *     upgradeFunction () {
 *       return Q.delay(300)
 *       .then(() => true);
 *     }
 * }];
 * @type {*[]}
 */
const CoreDBUpgrade = [{
    version: 1,
    upgradeFunction () {
        return Q(true);
    }
}];

LF.CoreDBUpgrade = CoreDBUpgrade;
export default CoreDBUpgrade;
