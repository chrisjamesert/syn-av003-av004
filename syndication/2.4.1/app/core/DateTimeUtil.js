import Logger from 'core/Logger';

let logger = new Logger('DateTimeUtil');

/**
 * Convert local time to UTC time
 * @param {Date} dateTime in local time format.
 * @param {boolean} usePassedOffset in local time format.
 * @returns {Date} date and time in UTC format
 * @example var now = LF.Utilities.convertToUtc(new Date());
 */
export function convertToUtc (dateTime, usePassedOffset) {
    let offset = usePassedOffset ? dateTime.getTimezoneOffset() : new Date().getTimezoneOffset();

    return new Date(dateTime.getTime() + (offset * 60000));
}

LF.Utilities.convertToUtc = convertToUtc;

/**
 * Adjust timezone offset from a time from a different local time to be its equivalent in the current local time zone.
 * For instance, if the current local timezone is GMT -04:00, and it receives the noon at GMT -07:00, it returns noon
 * at GMT -04:00.
 * @param {string} dateString date string to parse and adjust.
 * ASSUMES: dateString is in a parse-able JavaScript date format
 * ENDING with (+/-)hh:mm, representing the hours and minutes of the timezone offset.
 * @returns {Date}
 */
export function shiftToNewLocal (dateString) {
    // handle null, undefined, and already parsed dates, by returning a new date object with whatever is passed in.
    if (typeof dateString !== 'string') {
        return new Date(dateString);
    }

    let ret = new Date(dateString),
        match = dateString.match(/([+-])(\d+):(\d+)$/),
        curOffset = -(new Date()).getTimezoneOffset(),
        oldOffset,
        delta;

    // No timezone offset in this date format.  Return unadjusted parsed date
    if (!match || match.length < 4) {
        return ret;
    }

    oldOffset = parseInt(match[1] + (match[2] * 60 + parseInt(match[3], 10)), 10);
    delta = oldOffset - curOffset;
    ret.setMinutes(ret.getMinutes() + delta);

    return ret;
}

LF.Utilities.shiftToNewLocal = shiftToNewLocal;

/* eslint-disable prefer-template */
/**
 * Convert the date and time combination to date
 * @param {Date} dateTime Date object that is passed as the parameter
 * @return {string} date in the format yyyy-mm-dd
 * @example LF.Utilities.convertToDate(new Date());
 */
export function convertToDate (dateTime) {
    let year = dateTime.getFullYear(),

        // TODO: use the utility function zeroPad in the next 2 lines
        month = (parseInt(dateTime.getMonth(), 10) + 1 < 10) ? '0' + (parseInt(dateTime.getMonth(), 10) + 1) : dateTime.getMonth() + 1,
        day = (dateTime.getDate() < 10) ? '0' + dateTime.getDate() : dateTime.getDate();

    return year + '-' + month + '-' + day;
}
/* eslint-enable prefer-template */

LF.Utilities.convertToDate = convertToDate;

/**
 * Get the timezone offset of a date.
 * @example var offset = new Date().getOffset(); // -4
 * @returns {number} The offset.
 */
export function getOffset () {
    return (this.getTimezoneOffset() / 60) * -1;
}

Date.prototype.getOffset = getOffset;

// LF.Utilities.getOffset = getOffset;

/**
 * Return an UTC ISO 8601 timestamp.
 * @returns {string} The ISO 8601 timestamp.
 * @example var now = new Date().ISOStamp(); // "2012-07-25T17:39:43Z"
 */
export function ISOStamp () {
    return this.toISOString().replace(/\.\d*/g, '');
}

Date.prototype.ISOStamp = ISOStamp;

// LF.Utilities.ISOStamp = ISOStamp;

/* eslint-disable prefer-template */
/**
 * Return an UTC ISO 8601 timestamp in local time zone.
 * @returns {string} The ISO 8601 timestamp.
 * @example var now = new Date().ISOStamp(); // "2012-07-25T17:39:43"
 */
export function ISOLocalTZStamp () {
    let zeroPrefix = function (item) {
        return (item < 10) ? '0' + item : '' + item;
    };

    return this.getFullYear() +
       '-' + zeroPrefix(this.getMonth() + 1) +
       '-' + zeroPrefix(this.getDate()) +
       'T' + zeroPrefix(this.getHours()) +
       ':' + zeroPrefix(this.getMinutes()) +
       ':' + zeroPrefix(this.getSeconds());
}
/* eslint-enable prefer-template */

Date.prototype.ISOLocalTZStamp = ISOLocalTZStamp;

// LF.Utilities.ISOLocalTZStamp = ISOLocalTZStamp;

/* eslint-disable prefer-template */
/**
 * Return a formatted timestamp.
 * @param {Object} dateTime is a Date object parameter
 * @returns {string} The formatted timestamp.
 * @example var now = LF.Utilities.timeStamp(dateTime); // "2012-08-24T15:40:07-04:00"
 */
export function timeStamp (dateTime) {
    // TODO: use the utility function zeroPad
    let zeroPrefix = function (item) {
            return (item < 10) ? '0' + item : '' + item;
        },
        formatTimezoneOffset = function () {
            // TODO: Math.abs is a temp. fix for DE7131.
            let offset = Math.abs(dateTime.getTimezoneOffset()),
                hours = 0,
                sign = (dateTime.getTimezoneOffset() < 0) ? '+' : '-';

            while (offset >= 60) {
                hours += 1;
                offset -= 60;
            }

            return '' + sign + zeroPrefix(hours) + ':' + zeroPrefix(offset);
        };

    return dateTime.getFullYear() +
           '-' + (dateTime.getMonth() < 9 ? '0' : '') + (dateTime.getMonth() + 1) +
           '-' + zeroPrefix(dateTime.getDate()) +
           'T' + zeroPrefix(dateTime.getHours()) +
           ':' + zeroPrefix(dateTime.getMinutes()) +
           ':' + zeroPrefix(dateTime.getSeconds()) +
           formatTimezoneOffset();
}
/* eslint-enable prefer-template */

LF.Utilities.timeStamp = timeStamp;

/**
 * Gets the timeStamp from string and returns
 * current time in milliseconds from epoch representing
 * time that is passed in.
 * @param {string} timeString hours in string format
 * @param {boolean} [useLocalTime] true to return local time, false or nothing to return UTC time
 * @returns {number} time in milliseconds from epoch with milliseconds and seconds rounded to zero
 * @example var timeStamp = LF.Utilities.parseTime('13:23', false);
 */
export function parseTime (timeString, useLocalTime = false) {
    let time = new Date(),
        [hours, minutes] = timeString.split(':');

    time.setHours(hours, minutes, 0, 0);

    if (!useLocalTime) {
        return convertToUtc(time).getTime();
    }
    return time.getTime();
}

LF.Utilities.parseTime = parseTime;

/**
 * Create new date and returns it.
 * @returns {Date} local date.
 * @example var now = LF.Utilities.getNow();
 */
export function getNow () {
    return new Date();
}

LF.Utilities.getNow = getNow;

/**
 * Converts the timeStamp to date object. Ignores timezone offset.
 * @param {string} timeString date time string
 * @returns {Date} dateTime in local time format.
 * @example var timeStamp1 = LF.Utilities.parseDateTimeIsoNoOffset('2012-10-06T04:13:00+00:00'),
 * timeStamp2 = LF.Utilities.parseDateTimeIsoNoOffset('2012-10-06T04:13:00Z');
 */
export function parseDateTimeIsoNoOffset (timeString) {
    // if string is formatted in YYYY-MM-DDThh:mm:ss{+|-}hh:mm format
    if (timeString.match(/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})[+-](\d{2}):(\d{2})/g)) {
        return LF.Utilities.dateLocalTZFromISO8601(timeString);
    }
    return new Date(timeString);
}

LF.Utilities.parseDateTimeIsoNoOffset = parseDateTimeIsoNoOffset;

/**
 * Converts the YYYY-MM-DDThh:mm:ss{+|-}hh:mm formatted timeStamp to date object. Does not account for original TZ offset
 * @param {string} timeString date time string in YYYY-MM-DDThh:mm:ss{+|-}hh:mm format
 * @returns {Date} dateTime in local time format.
 * @example let timeStamp = LF.Utilities.dateLocalTZFromISO8601('2012-10-06T04:13:00+00:00');
 */
export function dateLocalTZFromISO8601 (timeString) {
    let parts = timeString.match(/\d+/g);
    return new Date(Number(parts[0]), Number(parts[1]) - 1, Number(parts[2]), Number(parts[3]), Number(parts[4]), Number(parts[5]), 0);
}

/**
 * Format the provided date by locale.
 * @param {Date} dt - The date to format.
 * @param {Object} options config object
 * @param {boolean} options.includeTime - Indicates whether to include time or not
 * @param {boolean} options.useShortFormat - Indicates whether to use short date format or not
 * @returns {string} The formatted date string.
 */
export function getLocalizedDate (dt = new Date(), options = {}) {
    // Get the date options from the dateCofig.json.
    let dateOptions = _.extend({}, LF.strings.dates({ dates: {} }), LF.strings.dates({ dateConfigs: {} }));

    // Create a datebox input so we can use the jqm-datebox plugin to format the date by locale.
    let date = $('<input data-role="datebox" />');
    let dateLang = {
        // eslint-disable-next-line new-cap
        defaultValue: dt.ISOLocalTZStamp(),
        lang: {
            default: dateOptions,
            isRTL: LF.strings.getLanguageDirection() === 'rtl'
        },
        mode: 'datebox'
    };

    date.datebox(dateLang);

    // Get the date, and format it.
    let getTheDate = date.datebox('getTheDate');
    let formattedDate = date.datebox('callFormat', options.useShortFormat ? dateOptions.shortDateFormat : dateOptions.dateFormat, getTheDate);

    if (options.includeTime) {
        formattedDate += ` ${date.datebox('callFormat', dateOptions.timeOutput, getTheDate)}`;
    }

    // Clear out the date object to prevent memory leaks due to DOM fragment assignment,
    // and stranded event listeners.
    date.off();
    date = null;

    return formattedDate;
}

/**
 * Calculates time zone offset in milliseconds from date
 * @param {Date} date - A date object to get offset from
 * @returns {number} a time zone offset in milliseconds
 * @example this.tzOffsetInMillis(new Date());
 */
export function tzOffsetInMillis (date) {
    if (!(date instanceof Date)) {
        throw new Error(`Expected a Date, got ${date}`);
    }
    return date.getTimezoneOffset() * (-60000);
}

/**
 * Adds leading zero to 1 digit numbers. Used for formating date and time.
 * @param {number} number The number to be padded with leading zero.
 * @returns {string} The number as a zero-padded string
 */
function pad (number) {
    let ret,
        aNumber = Math.abs(number);
    if (aNumber < 10) {
        ret = `0${aNumber}`;
    } else {
        ret = number;
    }
    if (number < 0) {
        ret = `-${ret}`;
    }
    return ret;
}

/* eslint-disable prefer-template */
/**
 * Convert a Date to ISO8601 with TZ.
 * @param {Date} aDateObject - The date to convert.
 * @returns {string} The formatted date.
 */
export function dateToISO8601WithTZ (aDateObject) {
    let tz = -aDateObject.getTimezoneOffset(),
        plus = tz >= 0 ? '+' : '',
        tzDisplay = tz === 0 ? 'Z' : plus + pad(parseInt(tz / 60, 10)) + ':' + pad(Math.abs(tz) % 60);

    return aDateObject.getFullYear() +
        '-' + pad(aDateObject.getMonth() + 1) +
        '-' + pad(aDateObject.getDate()) +
        'T' + pad(aDateObject.getHours()) +
        ':' + pad(aDateObject.getMinutes()) +
        ':' + pad(aDateObject.getSeconds()) +
        '.' + (aDateObject.getMilliseconds() / 1000).toFixed(3).slice(2, 5) +
        tzDisplay;
}

/**
 * Utility function to add hours to a date obj. Does so through setTime.
 * @param {Date} dateObj object to be updated
 * @param {number} hours Positive or Negative integer number of hours to add /decrease date object by.
 * @returns {Date} updated date object
 */
function addHoursToDate (dateObj, hours) {
    dateObj.setTime(dateObj.getTime() + hours * 60 * 60 * 1000);
    return dateObj;
}

/**
 * Utility function to add Time (in milliseconds as per utc) to a date Obj. Uses setTime
 * @param {Date} dateObj object to be updated
 * @param {number} time number of milliseconds to add to the object (can be negative).
 * @returns {Date} updated date object.
 */
function addTimetoDate (dateObj, time) {
    dateObj.setTime(dateObj.getTime() + time);
    return dateObj;
}

/**
 * Verifies and parses an ISO8601 format string with timezone offset and return a local Date object
 * Adjusts for TZ offset difference between input string and local device datetime.
 * Accepts as input:
 * Ex. 2017-07-28T14:07:48Z
 * Ex. 2017-07-28T12:00:00+03:00
 * @param {string} dateString A ISO8601 format string: Ex. 2017-07-28T14:07:48Z, Ex. 2017-07-28T12:00:00+03:00
 * @returns {*}  A date object representing the ISO string in local time. Returns undefined if input does not pass inspection.
 */
export function parseDateTimeIso (dateString) {
    let dateObjValue;

    // if not passed a string, whether unexpected obj or undefined or null, just return current date object.
    if (typeof dateString !== 'string') {
        logger.error('parseDateTimeIso passed unexpected value.');
    } else {
        // if using TZ offset of +/- . If perfect match first item in parts[] will match passed in string exactly.
        let parts = dateString.match(/(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})([+-])(\d{2})\:(\d{2})/);
        if (parts != null && dateString === parts[0]) {
            dateObjValue = new Date(Number(parts[1]), Number(parts[2]) - 1, Number(parts[3]), Number(parts[4]), Number(parts[5]), Number(parts[6]), 0);

            // get the offset tz offset hours and add the difference here.
            let houroffset = parseInt(parts[8], 10);
            if (parts[7] === '-') {
                houroffset = parseInt(parts[8], 10) * -1;
            }
            let localoffset = -(new Date().getTimezoneOffset() / 60);
            dateObjValue = addHoursToDate(dateObjValue, localoffset - houroffset);

            // account for cases of timezone with a minute offset.
            if (parseInt(parts[9], 10) > 0) {
                dateObjValue = addTimetoDate(dateObjValue, parts[9] * 60 * 1000);
            }
        } else {
            // if using 'Z' format
            let zparts = dateString.match(/(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})[Z]/);
            if (zparts != null && dateString === zparts[0]) {
                dateObjValue = new Date(Date.UTC(Number(zparts[1]), Number(zparts[2]) - 1, Number(zparts[3]), Number(zparts[4]), Number(zparts[5]), Number(zparts[6]), 0));
            }
        }
    }

    // String was passed but not formatted correctly.
    if (dateObjValue === undefined) {
        logger.error('parseDateTimeIso passed unexpected value.');
    }
    return dateObjValue;
}

LF.Utilities.parseDateTimeIso = parseDateTimeIso;
LF.Utilities.dateLocalTZFromISO8601 = dateLocalTZFromISO8601;
LF.Utilities.dateToISO8601WithTZ = dateToISO8601WithTZ;
