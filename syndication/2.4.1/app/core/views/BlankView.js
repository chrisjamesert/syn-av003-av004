import PageView from 'core/views/PageView';
import Logger from 'core/Logger';

// eslint-disable-next-line no-unused-vars
let logger = new Logger('BlankView');

export default class BlankView extends PageView {
    constructor (options) {
        super(options);

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.stringsToFetchForRender = {
            header: 'APPLICATION_HEADER',
            title: 'PLEASE_WAIT'
        };
    }

    /**
     * The unique id of the root element.
     * @type {string}
     * @readonly
     * @default 'blank-page'
     */
    get id () {
        return 'blank-page';
    }

    /**
     * Id of template to render
     * @type {string}
     * @readonly
     * @default '#blank-template'
     */
    get template () {
        return '#blank-template';
    }

    resolve () {
        return super.resolve();
    }

    /**
     * Renders the view.
     * @returns {Object} this
     */
    render () {
        LF.getStrings(this.stringsToFetchForRender, (strings) => {
            this.buildHTML(strings, true)
            .done();
        });

        return this;
    }
}
