import SecretQuestionBaseView from './SecretQuestionBaseView';
import * as lStorage from 'core/lStorage';
import Spinner from 'core/Spinner';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';

let logger = new Logger('ResetSecretQuestionView');

export default class ResetSecretQuestionView extends SecretQuestionBaseView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: lStorage.getItem('changePassword') ? 'CHANGE_SECRET_QUESTION' : 'RESET_SECRET_QUESTION',
            secretAnswer: 'SECRET_ANSWER',
            secretQuestion: 'SECRET_QUESTION',
            submit: 'OK',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'reset-secret-question-page'
     */
    get id () {
        return 'reset-secret-question-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#reset-secret-question-template'
     */
    get template () {
        return '#reset-secret-question-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let questions = LF.StudyDesign.securityQuestions;

        _(questions).each((value) => {
            if (value.display) {
                this.templateStrings[value.text] = value.text;
            }
        });

        this.needToRespond = { value: '' };

        return this.buildHTML({ key: this.key }, true)
        .then((strings) => {
            _(questions).each((value) => {
                if (value.display) {
                    this.addQuestion(value.key, strings[value.text]);
                }
            });

            this.$('#secret_question').select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });

            this.$('#secret_question').val(this.needToRespond.value).trigger('change');

            if (!lStorage.getItem('changePassword')) {
                this.hideElement('#back');
            }
        });
    }

    /**
     * Back button event
     */
    back () {
        if (!lStorage.getItem('changePassword')) {
            this.navigate('login');
        } else {
            this.navigate('change_temporary_password');
        }
    }

    /**
     * Called upon submission after the secret question and answer is saved to local database.
     * @returns {Q.Promise<void>}
     * @example this.onSecretQuestionSaved();
     */
    onSecretQuestionSaved () {
        return Q.Promise((resolve) => {
            $('input').blur();

            let user = LF.security.activeUser;

            logger.operational(`Secret question and password set by ${user.get('role')} ${user.get('username')}`, {
                username: user.get('username'),
                role: user.get('role')
            }, 'ResetSecret');

            if (lStorage.getItem('changePassword')) {
                lStorage.removeItem('changePassword');
                this.navigate('dashboard');

                Spinner.hide();
                resolve();
            } else {
                Q.delay(200)
                .then(() => {
                    Spinner.hide();

                    return this.notify({
                        dialog: MessageRepo.Dialog && MessageRepo.Dialog.SECRET_QUESTION_CHANGE_SUCCESS
                    })
                    .then(() => {
                        localStorage.setItem('Subject_Login', true);
                        this.navigate('dashboard');
                    });
                })
                .done(resolve);
            }
        });
    }
}
