// Legacy code exemptions:
// jscs:disable requireArrowFunctions
import BaseQuestionnaireView from './BaseQuestionnaireView';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import * as utils from 'core/utilities';
import Dashboard from 'core/models/Dashboard';
import LastDiary from 'core/models/LastDiary';
import Transmission from 'core/models/Transmission';
import Dashboards from 'core/collections/Dashboards';
import LastDiaries from 'core/collections/LastDiaries';
import Answer from 'core/models/Answer';
// eslint-disable-next-line no-unused-vars
import COOL from 'core/COOL';
import CurrentContext from 'core/CurrentContext';

let logger = new Logger('QuestionnaireView');

export default class QuestionnaireView extends BaseQuestionnaireView {
    /**
     * @param {object } options .
     *       Required properties
     *              id { string } - the ID of the questionnaire
     *              subject {Subject} - the subject model for the subject taking the questionnaire
     */
    constructor (options = {}) {
        super(options);
        this.Answer = () => {
            throw new Error('Attempt to create an answer on QuestionnaireView without doing resolve.');
        };
    }

    /**
     * Performs the standard screen, Question, and affidavit prep work.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return this.prepDashboardData(this.id)
        .then(() => {
            return super.resolve();
        });
    }

    /**
     * Create a dashboard record.
     * @param {String} id The id of the current questionnaire.
     * @returns {Q.Promise<void>}
     */
    prepDashboardData (id) {
        return Q()
        .then(() => {
            let dashboards = new Dashboards();
            let create = () => {
                logger.traceEnter('create(internal)');
                let model,
                    subject = this.subject,
                    deferred = Q.defer();

                try {
                    // Determine if the device is a SitePad App.
                    let isSitePad = !!lStorage.getItem('apiToken'),
                        user = LF.security.activeUser;

                    logger.trace(`about to build model, subject is: ${JSON.stringify(subject)}`);
                    logger.trace(`get device_id: $[subject.get('device_id')}.device_id:${subject.device_id}`);

                    model = new Dashboard({
                        user_id: user ? user.get('id') : '',
                        subject_id: subject.get('subject_id'),
                        device_id: lStorage.getItem('deviceId') || lStorage.getItem('IMEI') ||
                        subject.device_id || subject.get('device_id'),
                        questionnaire_id: id,
                        SU: this.model.get('SU'),
                        instance_ordinal: this.ordinal,
                        started: this.data.started.ISOStamp(),
                        report_date: DateTimeUtil.convertToDate(this.data.started),
                        phase: subject.get('phase'),
                        phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset'),
                        study_version: LF.StudyDesign.studyVersion,
                        ink: '',
                        sig_id: `${isSitePad ? 'SA' : 'LA'}.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`
                    });

                    // If the device is a SitePad App, we need some additional values.
                    if (isSitePad) {
                        model.set('krpt', subject.get('krpt'));
                    }
                } catch (err) {
                    logger.error(`ERROR creating Dashboard: ${err}`);
                    deferred.reject(err);
                    return;
                }
                logger.trace(`Creating dashboard: ${JSON.stringify(model)}`);
                deferred.resolve(model);

                logger.traceExit('create(internal)');
                // eslint-disable-next-line consistent-return
                return deferred.promise;
            };

            if (this.editMode) {
                return create();
            }
            return dashboards.fetch({
                search: {
                    where: { questionnaire_id: id, instance_ordinal: this.ordinal },
                    limit: 1
                }
            })
            .then(() => dashboards.at(0));
        })
        .then((model) => {
            logger.trace(`postDataPrep model: ${JSON.stringify(model)}`);

            if (model.get('completed')) {
                this.navigate('dashboard', true, {
                    subject: this.subject,
                    visit: this.visit
                });
            }

            this.data.dashboard = model;

            this.Answer = (options) => {
                let ret = new Answer(options);
                ret.set('subject', this.subject.get('subject_id'));
                ret.set('instance_ordinal', this.data.dashboard.get('instance_ordinal'));
                return ret;
            };
        });
    }

    /**
     * This is run after the decision to whether show context switch login or not is made
     * @param {Object} e - The event parameters returned by the ELF engine.
     * @param {Object} params -  Parameters to pass into the context switch control.
     * @returns {Q.Promise<void>} resolved when complete
     */
    afterContextSwitchControl (e, params = {}) {
        return super.afterContextSwitchControl(e, params)
        .then(() => {
            // return a promise if the first screen has already been displayed
            if (this.isFirstScreenDisplayed) {
                return Q();
            }

            return this.onFirstDisplay();
        });
    }

    /**
     * This function is run on the display of a screen for the first time for the questionnaire
     * @returns {Q.Promise<void>} resolved when complete
     */
    onFirstDisplay () {
        // fetch the Subject because it might have been updated due to Password & Secret Q/A changes
        return this.subject.fetch()
        .then(() => Q.Promise((resolve) => {
            // save batteryLevel if this is the first time we are displaying a screen
            LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
                this.data.dashboard && this.data.dashboard.set({ battery_level: batteryLevel });
                resolve();
            });
        }))
        .then(() => {
            this.isFirstScreenDisplayed = true;
        });
    }

    /**
     * Save the dashboard model to the database.
     * @returns {Q.Promise}
     * @example this.saveDashboard();
     */
    saveDashboard () {
        let currentDate = new Date(),
            saveForm,
            saveDiary,
            saveInk = () => {
                this.data.ink && this.data.dashboard.set('ink', JSON.stringify(this.data.ink));
            },

            // TODO: We no longer need to return a promise with this function
            getResponsibleParty = () => {
                if (utils.getNested('security.activeUser', LF)) {
                    switch (LF.security.activeUser.get('userType')) {
                        case 'Subject':
                            return Q(this.subject.get('initials'));
                        default:
                            return Q(LF.security.activeUser.get('username'));
                    }
                } else {
                    logger.info('Invalid SignatureBox invocation.  No active user session.');
                    return Q(null);
                }
            };

        if (this.inSave) {
            return Q();
        }

        logger.trace('saveDashboard: start');
        saveForm = () => {
            logger.trace('saveForm: start');

            saveInk();
            return getResponsibleParty()
            .then((authorName) => {
                this.data.dashboard.set('responsibleParty', authorName);
                return this.data.dashboard.save({
                    completed: currentDate.ISOStamp(),
                    completed_tz_offset: currentDate.getOffset(),
                    diary_id: parseInt(currentDate.getTime().toString() +
                        currentDate.getMilliseconds().toString(), 10)
                });
            });
        };

        saveDiary = () => {
            logger.trace('saveDiary: start');
            let started = this.data.dashboard.get('started'),
                completed = this.data.dashboard.get('completed');

            return this.saveLastDiary(started, completed);
        };

        this.inSave = true;
        return saveForm()
        .then(saveDiary)
        .finally(() => {
            this.inSave = false;
        });
    }

    /**
     * Save the last diary model to the database.
     * @param {Date} startedDate the date the subject started the diary
     * @param {Date} completedDate the date the subject completed the diary
     * @returns {Q.Promise<void>}
     * @example this.saveLastDiary(localDate, callback);
     */
    saveLastDiary (startedDate, completedDate) {
        let addLastDiaryRolesIT,
            fetchDiaries,
            saveDiary;

        logger.trace('saveLastDiary: start');
        // eslint-disable-next-line consistent-return
        addLastDiaryRolesIT = (role) => {
            let model,
                roleCode = LF.StudyDesign.roles.where({ id: role })[0].get('lastDiaryRoleCode'),
                ldrConfig = LF.StudyDesign.lastDiaryRole,
                answer = this.queryAnswersByIGITAndIGR(ldrConfig.IG, ldrConfig.IT, 0);

            // if answer already in collection, just modify the response
            if (answer.length) {
                answer[0].set('response', String(roleCode));
            } else {
                model = new Answer({
                    subject_id: this.subject.get('subject_id'),
                    question_id: 'LastDiaryRole',
                    questionnaire_id: this.id,
                    response: String(roleCode),
                    SW_Alias: `${ldrConfig.IG}.0.${ldrConfig.IT}`,
                    instance_ordinal: this.data.dashboard.get('instance_ordinal')
                });

                return this.data.answers.add(model);
            }
        };

        // Fetch the last completed diaries...
        fetchDiaries = () => {
            let lastDiaries = new LastDiaries();

            logger.trace('saveLastDiary: fetchDiaries: start');

            return lastDiaries.fetch()
            .then(() => {
                logger.trace('saveLastDiary: fetchDiaries: lastDiaries.fetch.then');
                return lastDiaries;
            });
        };

        // Save a new 'Last' diary...
        saveDiary = (lastDiaries) => {
            let lastRole,
                id = this.data.dashboard.get('questionnaire_id'),

                // TODO: There shoudl always be a subject set for the questionnaire. Make sure to delete the localstorage part
                krpt = this.subject ? this.subject.get('krpt') : localStorage.getItem('krpt'),
                lastDiary = _.first(lastDiaries.match({
                    krpt,
                    questionnaire_id: id
                }));

            logger.trace('saveLastDiary: saveDiary: start');

            lastDiary = lastDiary || new LastDiary();

            if (lastDiary.get('role')) {
                lastRole = (typeof lastDiary.get('role') === 'string') ?
                    JSON.parse(lastDiary.get('role')) :
                    lastDiary.get('role');

                if (!_.contains(lastRole, CurrentContext().role)) {
                    lastRole.push(CurrentContext().role);
                }
            } else {
                lastRole = [CurrentContext().role];
            }

            return lastDiary.save({
                krpt,
                questionnaire_id: id,
                lastStartedDate: DateTimeUtil.timeStamp(new Date(startedDate)),
                lastCompletedDate: completedDate.toString(),
                role: JSON.stringify(lastRole)
            });
        };

        return fetchDiaries()
        .then(saveDiary)
        .then(addLastDiaryRolesIT(CurrentContext().role));
    }

    /**
     * Creates and saves a new transmission record.
     * @returns {Q.Promise }
     */
    createTransmission () {
        logger.trace('createTransmission');
        let model = new Transmission(),
            newTransmission = {
                method: this.transmissionMethod,
                params: JSON.stringify({
                    dashboardId: this.data.dashboard.get('id'),
                    sigId: this.data.dashboard.get('sig_id')
                }),
                created: new Date().getTime()
            };
        logger.trace('createTransmission: about to model.save');
        if (this.subject.get('isDuplicate')) {
            newTransmission.status = 'failed';
        }
        return model.save(newTransmission);
    }

    /**
     * Updates the change_phase and phase fields of the diary.
     * @param {Object} params Parameters passed into the action.
     * @param {Function} [callback] A optional callback function invoked upon the completion of the action.
     */
    changePhase (params, callback = $.noop) {
        this.phase = params.phase;

        this.subject.set({
            phase: params.phase,
            phaseStartDateTZOffset: params.phaseStartDateTZOffset,
            phaseTriggered: 'false'
        });

        this.data.dashboard.set({
            change_phase: params.change ? 'true' : 'false',
            phase: params.phase,
            phaseStartDateTZOffset: params.phaseStartDateTZOffset
        });

        // If the phase change was triggered, then set the flag to expect a sync with NP.
        // After transmitting, LPA sync with NP, but sometimes there is a race condition and
        // LPA syncs before the value is updated in SW.
        // See DE12754 for details.
        if (params.change) {
            lStorage.setItem('outOfSync', true);
        }

        callback();
    }

    /**
     * Saves all answers and views within this Questionnaire
     * @returns {Q.Promise<Array>}
     */
    saveAnswers () {
        let saveAnswers,
            saveViews;

        logger.trace('saveAnswers start');
        saveAnswers = () => {
            logger.trace('saveAnswers: saveAnswers(internal): start');
            let answers = this.data.answers;
            let models = answers.filter((model) => {
                return model.get('localOnly') === true;
            });
            answers.remove(models);
            return answers.save();
        };

        saveViews = () => {
            let requests = [];
            logger.trace('saveAnswers: saveViews: start');
            this.questionViews.forEach((view) => {
                logger.trace(`saveAnswers: saveViews: forEach-view save view: ${view}`);
                let request = view.save();
                requests.push(request);
            });

            return Q.all(requests);
        };

        return saveAnswers()
        .then(saveViews);
    }

    /**
     * Saves the dashboard model and all answer records, before creating a transmission record.
     * @returns {Q.Promise<void>}
     */
    save () {
        logger.traceEnter('save()');
        return this.onBeforeQuestionnaireCompleted()
        .then(() => {
            return this.saveDashboard();
        })
        .then(() => {
            return this.saveAnswers();
        })
        .then(() => {
            return this.createTransmission();
        })
        .then(() => {
            return this.onAfterQuestionnaireCompleted();
        })
        .catch((err) => {
            logger.error('exception in QuestionaireView.save: ', err);
        })
        .then(() => {
            return this.subject.save();
        })
        .finally(() => {
            logger.traceExit('save()');
        });
    }
}

window.LF.View.QuestionnaireView = QuestionnaireView;
