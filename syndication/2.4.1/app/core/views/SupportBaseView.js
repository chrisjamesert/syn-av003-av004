import PageView from 'core/views/PageView';
import Logger from 'core/Logger';

// eslint-disable-next-line no-unused-vars
let logger = new Logger('SupportView');

export default class SupportView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events -The view's events.
         * @readonly
         */
        this.events = {
            // Click event for back button
            'click #back': 'back',

            // Change event for support option drop down list
            'change #support_dropdown': 'showSupportNumber'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            title: 'SUPPORT',
            header: 'APPLICATION_HEADER',
            supportPageMessage: 'SUPPORT_MESSAGE',
            supportDropdownDefault: 'SUPPORT_DROPDOWN',
            back: 'BACK'
        };
    }

    /**
     * Navigates back to the dashboard view.
     * Abstract method. Expected to be overriden by the sub class when needed.
     */
    back () {
        // Do nothing
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let supportOptions = LF.StudyDesign.supportOptions;

        // Loop through the questions and add them to the template strings to be translated.
        _(supportOptions).each((value) => {
            if (value.display) {
                this.templateStrings[value.text] = value.text;
            }
        });

        return this.buildHTML({ key: this.key }, true)
        .then((strings) => {
            _(supportOptions).each((value) => {
                if (value.display) {
                    this.$('#support_dropdown').append(`<option value="${supportOptions.indexOf(value)}">  ${strings[value.text]} </option>`);
                }
            });
            this.$('#support_dropdown').select2({ minimumResultsForSearch: Infinity });
            this.delegateEvents();
        });
    }

    /**
     * Displays change password and change secret question items
     */
    showSupportNumber () {
        let selectedKey = this.$('#support_dropdown').val(),
            supportOptions = LF.StudyDesign.supportOptions;

        this.$('#support-list').empty();

        if (!isNaN(parseInt(selectedKey, 10))) {
            this.i18n(supportOptions[selectedKey].number)
            .then((strings) => {
                if (Array.isArray(strings)) {
                    _(strings).each((value) => {
                        this.$('#support-list').append(`<li class="list-group-item support-list-item">${value}</li>`);
                    });
                } else {
                    this.$('#support-list').append(`<li class="list-group-item support-list-item">${strings}</li>`);
                }
            })
            .done();
        }
    }
}
