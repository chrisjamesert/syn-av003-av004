import PageView from 'core/views/PageView';
import Logger from 'core/Logger';
import Data from 'core/Data';

let logger = new Logger('QuestionnaireCompletionView');

/**
 * Handles questionnaire completion workflow.
 * @class QuestionnaireCompletionView
 * @extends PageView
 */
export default class QuestionnaireCompletionView extends PageView {
    constructor (options = {}) {
        super(options);

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER'
        };

        this.questionnaire = options.questionnaire || Data.Questionnaire;
        this.forceRender = options.force || false;
        this.data = this.questionnaire && this.questionnaire.data;
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'questionnaireCompletion-page'
     */
    get id () {
        return 'questionnaireCompletion-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#questionnaireCompletion-template'
     */
    get template () {
        return '#questionnaireCompletion-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        logger.trace('render');

        if (!this.questionnaire && !this.forceRender) {
            let err = new Error('No Questionnaire Data found.');
            logger.error(err.message, err);
            return Q.reject(err);
        }

        return this.buildHTML({}, true)
        .then(() => {
            LF.security.pauseSessionTimeOut();
        })
        .then(() => this.spinner.show())
        .then(() => logger.info('Diary Completion process is started'))
        .then(() => {
            if (!this.forceRender) {
                return this.questionnaire.completeDiary()
                .then(() => this.handleTimeouts())
                .then((cont) => {
                    if (!cont) {
                        return Q();
                    }

                    return this.transmitQuestionnaire();
                });
            }

            return Q();
        })
        .catch((err) => {
            err && logger.error('Error rendering QuestionnaireCompletionView', err);
        })
        .finally(() => this.spinner.hide());
    }

    /**
     * Checks if a timeout occurred before transmitting Diary
     * @return {Q.Promise} resolves false if we navigated away and don't want to continue.
     */
    handleTimeouts () {
        let handleError = (err) => {
            err && logger.error('Error executing handleTimeouts', err);
        };

        if (this.questionnaire.sessionTimeout) {
            let trigger = 'COMPLETIONVIEW:SavedDiarySessionTimeout';
            Data.Questionnaire = false;

            return ELF.trigger(trigger, {}, this)
            .then(() => LF.security.restartSessionTimeOut())
            .then(() => LF.security.logout(true))
            .then(() => false)
            .catch(handleError);
        }

        if (this.questionnaire.questionnaireSessionTimeout) {
            let trigger = 'COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout';

            LF.security.restartSessionTimeOut();

            return ELF.trigger(trigger, {}, this)
            .then(() => {
                if (this.questionnaire.defaultRouteOnExit === 'dashboard') {
                    // TODO: Investigate why we need this?
                    localStorage.setItem('questionnaireToDashboard', true);
                }

                this.navigate(this.questionnaire.defaultRouteOnExit, true, this.questionnaire.defaultFlashParamsOnExit);
            })
            .then(() => false)
            .catch(handleError);
        }

        return Q(true);
    }

    /**
     * Transmit the questionnaire and navigate to dashboard
     * @return {Q.Promise} resolves when transmission is done.
     */
    transmitQuestionnaire () {
        let questionnaireID = this.questionnaire.id;

        logger.info('Diary Transmission process is started');

        return ELF.trigger(`QUESTIONNAIRE:Transmit/${questionnaireID}`, {
            questionnaire: questionnaireID,
            subject: this.questionnaire.subject,
            visit: this.questionnaire.visit
        }, this)
        .catch((err) => {
            err && logger.error('Error transmitting questionnaire', err);
        })
        .then((evt) => {
            logger.trace('after trigger Transmit');

            LF.security.restartSessionTimeOut();
            localStorage.removeItem('questionnaireCompleted');

            if (evt && evt.preventDefault) {
                return;
            }

            if (this.questionnaire.defaultRouteOnExit === 'dashboard') {
                // TODO: Investigate why we need this?
                localStorage.setItem('questionnaireToDashboard', true);
            }

            this.navigate(this.questionnaire.defaultRouteOnExit, {
                trigger: true,
                replace: true
            }, this.questionnaire.defaultFlashParamsOnExit);
        });
    }
}
