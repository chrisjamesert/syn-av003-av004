import PageView from 'core/views/PageView';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import TimeZoneUtil from 'core/classes/TimeZoneUtil';

let logger = new Logger('setTimeZone');

const _evt = { preventDefault: $.noop };

/*
 * @class SetTimeZoneView
 * @description View for setting the time zone
 * @extends PageView
 */
export default class SetTimeZoneView extends PageView {
    constructor (options) {
        super(options);

        /*
         * @property {boolean} [selectActive=true] - If set to true, selects the device's active timezone by default.
         */
        this.selectActive = true;

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            'click #back': (e) => {
                this.back(e)
                .done();
            },
            'click #set_button': 'nextHandler',
            'change #selectTimeZone': 'changeSelection'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            title: 'SET_TIMEZONE',
            header: 'APPLICATION_HEADER',
            timezone_msg: 'SET_TIMEZONE_MESSAGE',
            set: 'SET_TIMEZONE_BUTTON'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'set-time-zone-page'
     */
    get id () {
        return 'set-time-zone-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#set-time-zone-template'
     */
    get template () {
        return '#set-time-zone-template';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate upon render.
     * @type {Object}
     */
    get selectors () {
        return {
            selectTimeZone: '#selectTimeZone',
            set: '#set_button'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q.Promise((resolve) => {
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    Q.all([
                        TimeZoneUtil.getAllTimeZones(),
                        TimeZoneUtil.getCurrentTimeZone()
                    ])
                    .spread((timezones, currentTimeZone) => {
                        this.timeZonesDisplay = TimeZoneUtil.filterTimeZones(timezones);
                        this.currentTimeZone = currentTimeZone;
                    })
                    .catch((err) => {
                        logger.error('Error occurred while obtaining the time zone list.', err);
                        this.notify({
                            dialog: MessageRepo.Dialog && MessageRepo.Dialog.TIME_ZONE_ERROR
                        });
                    })
                    .done(resolve);
                },
                execWhenNotWrapped: () => {
                    let tzoptions = _.pluck(LF.StudyDesign.web.timeZoneOptions, 'tzId'),
                        tzList = moment.tz.names().filter((item) => {
                            return tzoptions.indexOf(item) !== -1;
                        }).map((element) => {
                            return { id: element, displayName: `(GMT ${moment.tz(element).format('Z')}) ${element}` };
                        }).sort((tz1, tz2) => {
                            let tz1_num = parseFloat(tz1.displayName.split(' ')[1].replace(':', '.'));
                            let tz2_num = parseFloat(tz2.displayName.split(' ')[1].replace(':', '.'));
                            if (tz1_num < tz2_num) {
                                return -1;
                            } else if (tz1_num > tz2_num) {
                                return 1;
                            }
                            let name1 = tz1.displayName.split(' ')[2];
                            let name2 = tz2.displayName.split(' ')[2];
                            return name1 < name2 ? -1 : 1;
                        });

                    this.timeZonesDisplay = tzList;
                    this.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };
                    resolve();
                }
            });
        });
    }

    /**
     * Enable/disables the Set button based on selection.
     */
    changeSelection () {
        let selected = this.$selectTimeZone.select2('data')[0],
            tzId = selected ? selected.id : this.currentTimeZone.id;

        if (this.currentTimeZone.id !== tzId) {
            this.enableButton(this.$set);
        } else {
            this.disableButton(this.$set);
        }
    }

    /**
     * Navigates back to the dashboard view.
     * @returns {Q.Promise<void>}
     */
    back () {
        this.sound.play('click-audio');

        this.navigate('toolbox');
        return Q();
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML({}, true)
        .then(() => {
            let tzTemplateFormat = (selected) => {
                return `<span class="tz-value-dropdown"> ${selected.displayName}</span>`;
            };

            this.$selectTimeZone.select2({
                minimumResultsForSearch: -1,
                escapeMarkup: markup => markup,
                data: this.timeZonesDisplay,
                templateSelection: item => tzTemplateFormat(item),
                templateResult: item => tzTemplateFormat(item),
                scrollTo: this.currentTimeZone ? this.currentTimeZone.displayName : {},

                // Time zone names will not be translated. See DE17309.
                dir: 'ltr'
            });

            // If set to true, display the active timezone by default.
            if (this.selectActive && this.currentTimeZone) {
                this.$selectTimeZone.val(`${this.currentTimeZone.id}`).trigger('change');
            }
        });
    }

    /**
     * Click event handler for the Next button.
     * @param {(MouseEvent|TouchEvent)} [evt=_evt] - A mouse or touch event.
     * @example
     * this.events = { 'click #next': 'nextHandler' };
     */
    nextHandler (evt = _evt) {
        evt.preventDefault();

        // We're using a handler to invoke SetTimeZoneActivationView.setTimeZone() so .done() can be called on
        // the returned promise.  This prevents any errors from being swallowed
        // and the error 'Detected 1 unhandled rejections of Q promises' from being thrown.
        this.setTimeZone()
        .done();
    }

    /**
     * Sets the Timezone selected.
     * @returns {Q.Promise<void>}
     */
    setTimeZone () {
        return Q.Promise((resolve, reject) => {
            let selected = this.$selectTimeZone.select2('data')[0];

            let reload = () => {
                this.reload();
                resolve();
            };

            this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_CONFIRM })
            // eslint-disable-next-line consistent-return
            .then((result) => {
                if (!result) {
                    logger.operational(`User canceled setting the Time zone to: ${selected.displayName}`);
                    return resolve();
                }
                LF.Wrapper.exec({
                    execWhenWrapped: () => {
                        TimeZoneUtil.setTimeZone(selected)
                        .then(() => {
                            return TimeZoneUtil.restartApplication();
                        }, (err) => {
                            logger.error('Failed to set the Time zone', err);
                            return this.notify({
                                dialog: MessageRepo.Dialog && MessageRepo.Dialog.TIME_ZONE_SET_ERROR
                            })
                            .then(() => reject(err));
                        })
                        .then(reload)
                        .done();
                    },
                    execWhenNotWrapped: resolve
                });
            })
            .done();
        });
    }
}
