import PageView from './PageView';
import WebService from '../classes/WebService';
import * as utils from '../utilities';
import * as helpers from '../Helpers';
import Data from '../Data';
import * as lStorage from '../lStorage';
import ELF from '../ELF';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';
import COOL from '../COOL';

const logger = new Logger('ActivationBaseView');

/**
 * ActivationBaseView constructor function.
 * @class ActivationBaseView
 * @extends PageView
 */
export default class ActivationBaseView extends PageView {
    constructor (options = {}) {
        super(options);

        /**
         * This list keeps the temp storage names for historical data and subject data
         * @type Array
         */
        this.tempStorageNameList = [];

        /**
         * Used to communicate with the configured middle-tier.
         * @type WebService
         */
        this.webService = COOL.new('WebService', WebService);

        /**
         * COOLified utility functions
         * @type Utilities
         */
        this.utilities = COOL.new('Utilities');

        this.passwordFormat = {};
    }

    resolve () {
        let result = null,
            userLoginID = lStorage.getItem('User_Login'),
            loadPasswordFormat = (roleId) => {
                let role = LF.StudyDesign.roles.get(roleId);
                if (role) {
                    this.passwordFormat = role.get('passwordFormat') || {};
                }
                _.defaults(this.passwordFormat, LF.StudyDesign.defaultPasswordFormat || {});
                _.defaults(this.passwordFormat, LF.CoreSettings.defaultPasswordFormat || {});
            };

        if (userLoginID) {
            result = LF.security.getUser(parseInt(userLoginID, 10), true)
            .then((user) => {
                this.user = user;
                loadPasswordFormat(user.get('role'));
            });
        } else {
            loadPasswordFormat(lStorage.getItem('activationRole'));
            result = Q();
        }

        return result;
    }

    /**
     * Checks if the application is already installed.  Redirects to the dashboard if so.
     * This seems to be a LPA only method.
     * @returns {Q.Promise<void>}
     * @example this.preInstallCheck();
     */
    preInstallCheck () {
        return helpers.checkInstall()
        .then((isInstalled) => {
            if (isInstalled) {
                this.navigate('#login');
            }
        });
    }

    /**
     * Initializes REST calls to the server. If the client is offline, displays "Connection Required" popup.
     * @returns {Q.Promise<boolean>} Resolves true if online, false if not.
     */
    attemptTransmission () {
        return COOL.getClass('Utilities').isOnline()
        .catch((e) => {
            logger.error('Online status check failed on attemptTransmission().');
            return Q.reject(e);
        })
        .then((online) => {
            if (online) {
                this.displayMessage('PLEASE_WAIT');
                return true;
            }
            const { Dialog } = MessageRepo;

            this.sound.play('error-audio');

            return MessageRepo.display(Dialog && Dialog.CONNECTION_REQUIRED_ERROR)
            .then(() => {
                return false;
            });
        });
    }

    /**
     * Creates a transmission error message based on parameters and displays it.
     * @param {Object} params The object includes the http error Code, SWIS error code and the message that will be concatenated to the general error message.
     * @param {function} [callback] The callback function invoked when the the user's input is submitted.
     * @returns {Q.Promise<void>}
     */
    showTransmissionError (params = {}, callback = $.noop) {
        const key = (() => {
            if (!MessageRepo.Dialog) {
                return;
            }

            if (params) {
                // TODO: Use switch case here
                if (params.httpCode === LF.ServiceErr.HTTP_TIMEOUT) {
                    // eslint-disable-next-line consistent-return
                    return MessageRepo.Dialog.TRANSMISSION_ERROR_HTTP_TIMEOUT;
                } else if (params.httpCode && params.errorCode) {
                    // eslint-disable-next-line consistent-return
                    return MessageRepo.Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR;
                } else if (params.httpCode) {
                    // eslint-disable-next-line consistent-return
                    return MessageRepo.Dialog.TRANSMISSION_ERROR_HTTP_CODE;
                }
                // eslint-disable-next-line consistent-return
                return MessageRepo.Dialog.TRANSMISSION_ERROR_ERROR_CODE;
            }
            // eslint-disable-next-line consistent-return
            return MessageRepo.Dialog.TRANSMISSION_ERROR_ERR;
        })();

        return MessageRepo.display(params.customDialog ? `${key}_${params.customDialog}` : key, params)
        .then(() => {
            callback();
        });
    }

    /**
     * Gets subject data from the server and saves subject data in the local storage.
     * @param {string} token The authentication token that contains setup code, service password and device id.
     * @param {string} [errorMessage] The resource key of the error message that will be displayed on the error popup if transmission fails.
     * @param {function} [onError] The callback function invoked if the transmission fails.
     * @returns {Q.Promise<void>} resolved when returned.
     */
    getSubjectData (token, errorMessage, onError) {
        const deferred = Q.defer();

        let handleError = ({ errorCode, httpCode }) => {
            let errorObj = {
                errorCode,
                httpCode,
                message: errorMessage || false
            };

            this.sound.play('error-audio');
            this.removeMessage();
            this.showTransmissionError(errorObj, onError);
            deferred.resolve();
        };

        this.webService.doSubjectSync(Data.code, token)
        .then(({ res, syncID }) => {
            let rule;

            localStorage.setItem('krpt', res.K);
            logger.trace(`getSubjectData  Setting syncID to:${syncID}`);

            if (syncID == null || syncID === 'null') {
                logger.error(`getSubjectData syncID invalid:${syncID}`);
            } else {
                localStorage.setItem('PHT_syncID', syncID);
            }

            this.webService.getSingleSubject(res.K, token)
            .then(({ res: result }) => {
                localStorage.setItem('PHT_TEMP', JSON.stringify({
                    subject_active: 1,
                    subject_id: Data.code,
                    subject_password: hex_sha512(Data.password + res.K),
                    service_password: Data.service_password,
                    initials: res.I,
                    krpt: res.K,
                    subject_number: res.N,
                    site_code: res.C,
                    device_id: Data.deviceID,
                    secret_question: LF.StudyDesign.askSecurityQuestion ? Data.question.toString() : null,
                    secret_answer: LF.StudyDesign.askSecurityQuestion ? Data.answer : null,
                    phase: res.P,
                    phaseStartDateTZOffset: res.T,
                    phaseTriggered: 'false',
                    log_level: res.L,
                    enrollmentDate: res.E,
                    activationDate: res.A,
                    dob: result.dob,
                    custom1: result.custom1,
                    custom2: result.custom2,
                    custom3: result.custom3,
                    custom4: result.custom4,
                    custom5: result.custom5,
                    custom6: result.custom6,
                    custom7: result.custom7,
                    custom8: result.custom8,
                    custom9: result.custom9,
                    custom10: result.custom10
                }));

                this.tempStorageNameList.push('krpt', 'PHT_syncID', 'PHT_TEMP');

                rule = 'ACTIVATION:SubjectDataReceived';
                ELF.trigger(rule, {}, this)
                .finally(() => {
                    this.removeMessage();
                    this.navigate('install', true);
                })
                .done(() => {
                    deferred.resolve();
                });
            })
            .catch(handleError)
            .done();
        })
        .catch(handleError)
        .done();

        return deferred.promise;
    }

    /**
     * Request historical data. This action receives historical data from server and saves it in the local storage.
     * @param {Object} params - Including temporary storage and web service function names for historical data.
     * @returns {Q.Promise}
     */
    requestHistoricalData (params) {
        let token = `${Data.code}:${Data.service_password}:${Data.deviceID}`,
            handleResult = ({ res }) => {
                let tempStorage = params.tempStorage,
                    filtered = params.filterFunction && res.map(item => params.filterFunction(item));

                filtered = filtered || res;

                localStorage.setItem(tempStorage, JSON.stringify(filtered));
                this.tempStorageNameList.push(tempStorage);
            },
            handleError = ({ errorCode, httpCode }) => {
                _.each(this.tempStorageNameList, (item) => {
                    localStorage.removeItem(item);
                });

                if (Data.activation) {
                    this.showTransmissionError({
                        errorCode,
                        httpCode,
                        customDialog: 'ACTIVATION_SUCCESSFUL'
                    }, () => {
                        this.codeEntry();
                    });
                } else {
                    this.showTransmissionError({
                        errorCode,
                        httpCode
                    });
                }

                this.sound.play('error-audio');
                this.removeMessage();
            };

        if (params.tempStorage == null) {
            return Q.reject(new Error('Missing Argument: tempStorage is null or undefined.'));
        }

        if (!LF.studyWebService[params.webServiceFunction]) {
            return Q.reject(new Error('Missing Argument: webServiceFunction is null or undefined.'));
        }

        return LF.studyWebService[params.webServiceFunction]({
            auth: token,
            activation: params.activation
        })
        .then(handleResult)
        .catch(handleError);
    }

    /**
     * Validate the form by checking the format of the setup code.
     * @param {string} elementName The name of the element containing the setup code, preceded with a #
     * @returns {boolean} Returns true if the setup code is valid, otherwise false
     * @example this.isValidForm("#txtCode");
     * @example this.isValidForm('#txtReCode');
     */
    isValidForm (elementName) {
        let isValid = true;
        if (!this.isValidCode(this.$(elementName).val().trim())) {
            this.$('#submitNext').button('disable');
            this.inputError(elementName);
            isValid = false;
        }

        return isValid;
    }

    /**
     * Verify code format.
     * @param {string} code The activation code inputted by the user
     * @returns {boolean} if the code is valid, return true. Otherwise false.
     * @example  this.isValidCode("26723469");
     */
    isValidCode (code) {
        // @todo This was a temp. fix to work with ERT's middleware. Needs to be configurable.
        let codeFormat = /^\d+$/;

        return codeFormat.test(code);
    }

    /**
     * Respond to users password input by validating the new password.
     * @param {Event} e Event data
     */
    validateNewPassword (e) {
        let $target = this.$(e.target),
            $confirm = this.$('#txtConfirmPassword');

        // Check if the changed password input is valid based on the regular expression
        if (!utils.isValidPassword($target.val(), this.passwordFormat)) {
            this.inputError($target);
        } else {
            this.inputSuccess($target);
        }

        if ($confirm.val()) {
            this.validatePasswords();
        }

        $target = null;
        $confirm = null;
    }

    /**
     * Returns true if both password fields match and pass validation.
     * @param {boolean} [updateSubmit=true] - If true, the submit button's disabled state will reflect the result of validation.
     * @returns {boolean} Returns true if both password fields match and pass validation, otherwise false
     */
    validatePasswords (updateSubmit = true) {
        let $newPassword = this.$('#txtNewPassword'),
            newPasswordIsValid = utils.isValidPassword($newPassword.val(), this.passwordFormat),
            $confirmPassword = this.$('#txtConfirmPassword'),
            confirmPasswordIsValid = utils.isValidPassword($confirmPassword.val(), this.passwordFormat),
            validPasswords = newPasswordIsValid && confirmPasswordIsValid,
            passwordsMatch = $newPassword.val() === $confirmPassword.val();

        // If the new password field is empty...
        if (!$newPassword.val()) {
            this.inputError($newPassword);
        }

        // If confirm password is not empty, check whether it matches the password
        if ($confirmPassword.val()) {
            if (!passwordsMatch) {
                this.inputError($confirmPassword);

                // If passwords don't match and the password is empty, show an error icon for the password
                if (!$newPassword.val()) {
                    this.inputError($newPassword);
                }
            } else {
                // Passwords matched. Also check whether confirm password is valid
                if (confirmPasswordIsValid) {
                    this.inputSuccess($confirmPassword);
                }
            }
        } else {
            this.inputError($confirmPassword);
        }

        // If both passwords are valid and match, enable the activation button otherwise disable it.
        if (passwordsMatch && validPasswords) {
            updateSubmit && this.$('#submit').removeAttr('disabled');
            return true;
        }
        updateSubmit && this.$('#submit').attr('disabled', 'disabled');
        return false;
    }

    /**
     * Activate the subject by setting password and/or secret question and answer on the server.
     * Handles transmission result and error.
     */
    activate () {
        let handleResult = ({ res }) => {
                let token;
                Data.deviceID = res.D;
                Data.service_password = res.W;

                token = `${Data.code}:${Data.service_password}:${Data.deviceID}`;

                ELF.trigger('ACTIVATION:Transmit', {}, this)
                .finally(() => {
                    this.getSubjectData(token, 'ERROR_AFTER_ACTIVATION_SUCCESSFUL', () => this.codeEntry());
                });
            },
            handleError = ({ errorCode, httpCode }) => {
                this.sound.play('error-audio');
                this.submitted = false;
                this.removeMessage();
                this.showTransmissionError({ errorCode, httpCode }, () => this.codeEntry());
            };

        this.attemptTransmission()
        .then((online) => {
            if (online) {
                let subjectData = {
                    U: Data.code,
                    W: hex_sha512(Data.password + localStorage.getItem('krpt'))
                };

                if (LF.StudyDesign.askSecurityQuestion) {
                    subjectData.Q = Data.question;
                    subjectData.A = Data.answer;
                }

                if (localStorage.getItem('PHT_IMEI')) {
                    subjectData.S = localStorage.getItem('PHT_IMEI');
                    subjectData.M = device ? device.model : 'Unavailable';
                }

                return this.webService.getDeviceID(subjectData)
                .then(handleResult)
                .catch(handleError);
            }
            this.submitted = false;
            return Q();
        })
        .catch((err) => {
            logger.error('activate attemptTransimission Exception ', err);
        })
        .done();
    }

    /**
     * Navigate back to the code entry page
     */
    codeEntry () {
        this.navigate('code_entry', true);
    }

    /**
     * Get the salt and password values.
     * @param {string} newPassword The user's new password
     * @returns {Object} The new password parameters.
     */
    getPasswordChangeParams (newPassword) {
        let i,

            // Due to SW string length limitations, we have to lower the char length of the salt to a max of 36.
            // Ideally it should be larger. (Uint8Array(16)). If possible in the future, change this.
            max = 36,
            salt = '',
            fallback = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            rand = null;

        if (window.crypto && typeof window.crypto.getRandomValues === 'function') {
            rand = window.crypto.getRandomValues(new Uint8Array(12));
        }

        if (rand) {
            for (i = 0; i < rand.length; i++) {
                salt += rand[i].toString();
            }
        } else {
            for (i = 0; i < max; i++) {
                salt += fallback.charAt(Math.floor(Math.random() * fallback.length));
            }
        }

        return {
            password: hex_sha512(newPassword + salt),
            salt
        };
    }
}
