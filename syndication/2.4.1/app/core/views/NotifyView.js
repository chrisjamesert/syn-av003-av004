import ModalView from './ModalView';

export default class NotifyView extends ModalView {
    constructor () {
        super({

            /**
             * @property {Object} events A list of DOM events.
             * @readonly
             */
            events: {
                'click #ok': 'resolve',
                'click #close': 'resolve',
                'hidden.bs.modal': 'teardown'
            }
        });

        /**
         * @property {String} template A selector that points to the template, within the DOM, to render.
         */
        this.template = '#notify-template';

        /**
         * Creates an internal, de-bounced show method to prevent the same notification from being rendered twice.
         */
        this._show = _.debounce(options => super.show(options), 500);
    }

    /**
     * Show the notification dialog.
     * @param {Object} options Strings to pass into the modal's template.
     * @param {String} options.header The header text to display.
     * @param {String} options.body The body text to display.
     * @param {String} options.ok The OK button text.
     * @param {String} options.type The type of notification to display.
     * @returns {Q.Promise<void>}
     */
    show (options) {
        this.deferred = Q.defer();

        this._show(options);

        return this.deferred.promise;
    }

    /**
     * Invoked when the user accepts the confirmation.
     */
    resolve () {
        this.deferred.resolve(true);
        this.hide();
    }
}
