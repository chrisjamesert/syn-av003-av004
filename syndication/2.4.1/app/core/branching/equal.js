/**
 * @file
 * @author <a href="mailto:snovakovic@phtcorp.com">Stefan Novakovic</a>
 * @version 1.5.1
 */

/**
 * Evaluate if user response is the same as value from configuration
 * @param {Object} branchObj - extended branch object from the configuration
 * @return {Q.Promise<Boolean>} true - user response is equal as in branch configuration
 *                    false - user response in not equal as in branch configuration
 */
LF.Branching.branchFunctions.equal = function (branchObj) {
    return Q.Promise((resolve) => {
        let questionId = branchObj.branchParams.questionId,
            response;

        if (branchObj.currentScreen === branchObj.branchFrom) {
            response = LF.Branching.Helpers.responseForQuestionId(branchObj.answers, questionId);

            // eslint-disable-next-line eqeqeq
            if (typeof response !== 'undefined' && branchObj.branchParams.value == response) {
                resolve(true);
            } else {
                resolve(false);
            }
        } else {
            resolve(false);
        }
    });
};


/**
 * Always branch.
 * @param {Object} branchObj - extended branch object from the configuration
 * @returns {Q.Promise<boolean>} promise resolving with true
 */
LF.Branching.branchFunctions.always = function () {
    return new Q.Promise((resolve) => {
        resolve(true);
    });
};

export default LF.Branching.branchFunctions.equal;
