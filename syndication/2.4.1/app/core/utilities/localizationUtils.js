/**
 * Process a string and return its display equivalent in strict RTL.
 * The result of this, displayed in a &lt;bdo dir='rtl'&gt; block, is the equivalent display as it is naturally in RTL,
 * but items are all organized so that the flow is consistent if the string is parsed and separated into spans.
 * @param {string} inputString string to process, containing a mix of RTL and LTR characters
 * @returns {string} inputString in a format that will look correct with &lt;bdo dir="rtl"&gt;, all characters changed to RTL.
 */
export function toStrictRTL (inputString) {
    let inputHTML = '',
        $testContainer = $('<div>&nbsp;</div>'),
        $testElement,

        // Maintain parallel arrays of current positions and characters in order
        //	of appearance (left to right)
        positions = [],
        orderedText = [];

    $testContainer.css({
        direction: 'rtl',
        position: 'absolute',
        'background-color': 'green',
        visibility: 'hidden',
        'overflow-x': 'hidden'
    });

    for (let i = 0; i < inputString.length; ++i) {
        inputHTML += `<span>${inputString.charAt(i)}</span>`;
    }

    $testElement = $(`<div>${inputHTML}</div>`);
    $testContainer.append($testElement).appendTo($('body'));

    $testElement.children('span').each((index, elem) => {
        let x = $(elem).position().left,
            i = _.sortedIndex(positions, x);
        positions.splice(i, 0, x);
        orderedText.splice(i, 0, elem.innerHTML);
    });
    $testContainer.remove();
    $testElement.remove();
    $testContainer = null;
    $testElement = null;

    return orderedText.reverse().join('');
}
