import ArrayRef from 'core/classes/ArrayRef';
import ObjectRef from 'core/classes/ObjectRef';
const DEFAULT_EQUIVALENCY_PROPERTY = 'id';

/**
 * Concatenate arrays, but replace objects in arr1, whose equivalency property matches an object in arr2.
 * @param {Array} arr1 first array to merge
 * @param {Array} arr2 second array to merge
 * @param {String} [equivalencyProperty=id] property (such as ID) that determines if two objects are equivalent,
 * to overwrite, rather than merge.
 * @returns {Array} array with items concatenated and duplicate objects replaced.
 */
export function mergeArrays (arr1, arr2, equivalencyProperty = DEFAULT_EQUIVALENCY_PROPERTY) {
    let returnArray = [];

    // Create a reverse lookup object on values of our equivalency property, throughout array 1
    let propertyIndexMap = {};
    for (let i = 0; i < arr1.length; ++i) {
        let item = arr1[i];
        if ((_.isObject(item) && !_.isNull(item)) && typeof item[equivalencyProperty] !== 'undefined') {
            propertyIndexMap[item[equivalencyProperty]] = i;
        }
        returnArray.push(item);
    }

    // Loop on array 2.  Use its contents to replace things that match in equivalency or push to the end of the array.
    _.each(arr2, (item) => {
        let equivalency = _.isObject(item) && !_.isNull(item) ? item[equivalencyProperty] : null;
        if (typeof propertyIndexMap[equivalency] === 'number') {
            returnArray.splice(propertyIndexMap[equivalency], 1, item);
        } else {
            returnArray.push(item);
        }
    });

    return returnArray;
}

/**
 * Merge an object pair together, recursively.  Optionally merging arrays instead of replacing them.
 * @param {Object} objectA The first object to merge.
 * @param {Object} objectB The second object to merge.
 * @param {Boolean} [doArrayMerge=true] Whether or not to merge, rather than replace, arrays
 * @returns {Object}
 */
export function mergeObjectPair (objectA, objectB, doArrayMerge = true) {
    /* eslint-enable no-unused-vars */
    let keysB = _.keys(objectB),
        newB = {},
        ret;

    // find and resolve conflicts
    for (let i = 0, len = keysB.length; i < len; ++i) {
        let key = keysB[i];

        // Default behavior of extend.
        newB[key] = objectB[key];

        // Concat if an array, recursively merge if an object.
        if (typeof objectA[key] === 'object' &&
            typeof newB[key] === 'object' &&
            !(objectA[key] instanceof ArrayRef) &&
            !(objectA[key] instanceof ObjectRef)
        ) {
            if (_.isArray(objectB[key]) && _.isArray(objectA[key]) && doArrayMerge) {
                newB[key] = mergeArrays(objectA[key], objectB[key], DEFAULT_EQUIVALENCY_PROPERTY);
            } else if (!_.isArray(objectB[key]) && !_.isArray(objectA[key])) {
                newB[key] = mergeObjectPair(objectA[key], objectB[key], doArrayMerge);
            }
        }
    }
    ret = _.extend(objectA, newB);
    return ret;
}

/**
 * Essentially the same as _.extend() except that this method recurses down object properties.
 * (recursively).
 * @param {...Object} objectsToExtend objects to extend
 * @returns {Object} new object with all objects merged together (with right side winning, like _.extend()).
 */
export function deepExtend (...objectsToExtend) {
    return objectsToExtend.reduce((previous, current) => {
        return mergeObjectPair(previous, current, false);
    }, {});
}

/**
 * The same as deepExtend except that this method concats arrays it encounters instead of overwriting
 * (recursively).
 * @param {...Object} objectsToMerge objects to merge
 * @returns {Object} new object with all objects merged together (with right side winning, like _.extend()).
 */
export function mergeObjects (...objectsToMerge) {
    return objectsToMerge.reduce((previous, current) => {
        return mergeObjectPair(previous, current, true);
    }, {});
}

/**
 * Class for adding mixins to a super class
 * http://justinfagnani.com/2015/12/21/real-mixins-with-javascript-classes/
 */
class MixinBuilder {
    constructor (superclass) {
        this.superclass = superclass;
    }

    with (...mixins) {
        return mixins.reduce((c, mixin) => mixin(c), this.superclass);
    }
}

/**
 * Exposed method for creating a mixin object from a superclass.
 * @param {Object} superclass super class for the function
 * @returns {MixinBuilder} resulting mixin builder... with "with()" method exposed
 * for adding mixins.
 */
export function mix (superclass) {
    return new MixinBuilder(superclass);
}
