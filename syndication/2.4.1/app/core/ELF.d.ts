import Q from "q";

/**
 * This is an experiment. By having these type definitions in the project,
 * even though they are not imported from anywhere, WebStorm is able to
 * detect correct vs. uses of these methods and structures.
 */

declare module ELF {

    /**
     * Announce a system event that interested listeners may be notified of and respond to.
     * @param eventName The name of the event (possibly parameterized)
     * @param input An object containing data relevant to this event
     * @param context The system component that generated this event
     */
    function trigger(eventName: string, input: any, context?: any): Q.Promise<TriggerResult>;

    /**
     * Declare an interest in one or more system events, and some 'actions' to be invoked when such
     * an event occurs.
     * @param trigger one or more trigger spec strings (whole or partial event names)
     * @param action one or more actions to be invoked
     */
    function listen(trigger: (string | string[]), action: (Action | Action[])) : Q.Promise<TriggerResult>;

    /**
     * Register an action, giving it a name, and some code to be executed when this action is invoked
     * @param name the name of the action
     * @param action
     */
    function action(name: string, action: Action): void;

    type Action = ActionObject | ActionFunction;

    interface ActionFunction {
        (param: Object) : (void | ActionResult | Q.Promise<void> | Q.Promise<ActionResult>);
        (param: Object, callback: (boolean)=>void): void; // Legacy style, do not use; use above instead
    }

    interface ActionObject {
        action: ActionFunction | string;
        data?: any;
        builtIn?: boolean;
    }

    interface TriggerResult {
        preventDefault?: boolean;
        problems?: Error[];
    }

    interface ActionResult {
        preventDefault?: boolean;
        stopActions?: boolean;
        stopRules?: boolean;
        problems?: Error[];
    }
}

// ELF.rules namespace
declare module ELF.rules {
    function add(rule:Rule | Rule[] | LegacyRule):Rule;

    function find(id:string):Rule;

    function remove(id:String):void;

    function all():Rule[];

    interface Rule {
        id: string;
        trigger: string;
        evaluate: Expression | Expression[];
        resolve: Action[];
        reject?: Action[];
        salience?: number;
    }

    type Expression = string | boolean | ExpressionObject | ExpressionFunction;

    interface ExpressionFunction {
        (param:Object): (boolean | Q.Promise<boolean>);
        (param:Object, callback:(boolean)=>void): void; // Legacy style, do not use; use above instead
    }

    interface ExpressionObject {
        expression: boolean | string | ExpressionFunction;
        input?: any;
    }

    type LegacyAction = LegacyActionTrue | LegacyActionFalse;

    interface LegacyActionTrue {
        trueAction: any;
        trueArguments?: any;
    }

    interface LegacyActionFalse {
        falseAction: any;
        falseArguments?: any;
    }

    interface LegacyRule {
        id: string;
        trigger: string,
        expression: Expression | Expression[];
        actionData: LegacyAction[];
    }
}

export default ELF;
