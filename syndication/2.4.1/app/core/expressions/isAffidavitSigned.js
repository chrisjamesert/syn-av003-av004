import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method isAffidavitSigned
 * @description
 * Determines if the affidavit has been signed
 * This should only be used in the context of a questionnaire
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'isAffidavitSigned'
 */
export default function isAffidavitSigned () {
    let affidavitWidget = LF.Helpers.getWidget('AFFIDAVIT');

    return Q(!!affidavitWidget && affidavitWidget.isAffidavitAnswered());
}

ELF.expression('isAffidavitSigned', isAffidavitSigned);
