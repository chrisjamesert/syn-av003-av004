import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method  isDirectionBackward
 * @description
 * Determines if the navigation direction is backward.
 * This should only be used in the context of a questionnaire,
 * when a QUESTIONNAIRE:Navigate event is triggered.
 * @param {Object} input - Event data passed along by the triggered event.
 * @param {string} input.direction - The direction of the navigation. e.g. 'previous'
 * @returns {Q.Promise<boolean>}
 * @example
 * trigger: 'QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1',
 * evaluate: 'isDirectionBackward'
 */
export default function isDirectionBackward (input) {
    return Q(input.direction === 'previous');
}

ELF.expression('isDirectionBackward', isDirectionBackward);
