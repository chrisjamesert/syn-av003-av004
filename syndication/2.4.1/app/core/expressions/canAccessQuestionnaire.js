import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';

/**
 * @memberOf ELF.expressions
 * @method  canAccessQuestionnaire
 * @description
 * Determines if the current user can access a questionnaire.
 * This only works for top level accessRoles defined at the questionnaire level.
 * May need to expand functionality to parse screen-level configurations.
 * @param {string} questionnaireId The ID of the questionnaire in question.
 * @returns {Q.Promise<boolean>} True if user has access, false if not.
 * @example
 * evaluate : {
 *   action : 'canAccessQuestionnaire',
 *   input  : 'Deactivate_User'
 * }
 */
export default function canAccessQuestionnaire (questionnaireId) {
    return Q.Promise((resolve) => {
        // Get the role of the current user.
        let role = CurrentContext().role;

        // Get the questionnaire by ID from the study design.
        let questionnaire = LF.StudyDesign.questionnaires.get(questionnaireId);

        // Get the list of roles allowed to access the questionnaire.
        let accessRoles = questionnaire.get('accessRoles') || [];

        resolve(accessRoles.indexOf(role) !== -1);
    });
}

ELF.expression('canAccessQuestionnaire', canAccessQuestionnaire);
