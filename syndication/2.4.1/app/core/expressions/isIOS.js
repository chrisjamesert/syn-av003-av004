import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method isIOS
 * @description
 * Determines if the application is running on iOS.
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'isIOS'
 */
export default function isIOS () {
    return Q(LF.Wrapper.platform === 'ios');
}

ELF.expression('isIOS', isIOS);
