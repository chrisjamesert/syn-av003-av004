import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method isLogpad
 * @description
 * Determines if the running application is a LogPad.
 * @param {null} input - Not used with this expression.
 * @param {Function} done - Invoked upon completion of the expression.
 * @example
 * evaluate: 'isLogpad'
 */
export function isLogPad (input, done) {
    done(LF.appName === 'LogPad App');
}

ELF.expression('isLogPad', isLogPad);
