import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method  isWrapped
 * @description
 * Determines if the application is wrapped via cordova.
 * @param {null} input - Not used in this expression.
 * @param {Function} done - Invoked upon completion of expression.
 * @example
 * evaluate: 'isWrapped'
 */
export function isWrapped (input, done) {
    LF.Wrapper.exec({
        execWhenWrapped () {
            done(true);
        },
        execWhenNotWrapped () {
            done(false);
        }
    });
}

ELF.expression('isWrapped', isWrapped);
