import './canAccessQuestionnaire';
import './isBetweenTimeRange';
import './isDirectionForward';
import './isDirectionBackward';
import './isLogPad';
import './isOnline';
import './isQuestionnaireCompleted';
import './isSitePad';
import './isStudyOnline';
import './isWrapped';
import './isDayOfTheWeek';
import './hasFormChanged';
import './isAffidavitSigned';
import './isIOS';
import './isWindows';

