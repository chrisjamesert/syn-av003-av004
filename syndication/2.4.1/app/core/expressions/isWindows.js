import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method isWindows
 * @description
 * Determines if the application is running on Windows.
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'isWindows'
 */
export default function isWindows () {
    return Q(LF.Wrapper.platform === 'windows');
}

ELF.expression('isWindows', isWindows);
