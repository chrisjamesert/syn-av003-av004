import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method isSitePad
 * @description
 * Determines if the running application is a SitePad.
 * @param {null} input - Not used in this expression.
 * @param {Function} done - Invoked upon completion of the expression.
 * @example
 * evaluate: 'isSitePad'
 */
export function isSitePad (input, done) {
    done(LF.appName === 'SitePad App');
}

ELF.expression('isSitePad', isSitePad);
