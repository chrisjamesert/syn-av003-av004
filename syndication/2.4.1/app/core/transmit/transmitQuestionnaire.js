import COOL from 'core/COOL';
import Logger from 'core/Logger';
import Subjects from 'core/collections/Subjects';
import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import WebService from 'core/classes/WebService';
import { isSitePad } from 'core/utilities';

const logger = new Logger('Transmit.transmitQuestionnaire');

let dataCache = null;

/**
 * Generates the payload for the diary transmission
 * @param {Object} diaryJSON - a JSON object with the diary data
 * @returns {Object}
 */
function generatePayload (diaryJSON) {
    let payload = {
            I: diaryJSON.diary_id,
            U: diaryJSON.SU,
            S: diaryJSON.started,
            C: diaryJSON.completed,
            R: diaryJSON.report_date,
            O: diaryJSON.completed_tz_offset,
            P: diaryJSON.phase,
            T: diaryJSON.phaseStartDateTZOffset,
            B: diaryJSON.change_phase,
            D: diaryJSON.device_id,
            E: diaryJSON.core_version,
            V: diaryJSON.study_version,
            L: diaryJSON.battery_level,
            J: diaryJSON.sig_id
        },
        useJSONH = LF.StudyDesign.jsonh !== false;

    // If the device is a SitePad App, we need to modify the JSON.
    if (isSitePad()) {
        payload.K = diaryJSON.krpt;
        payload.M = diaryJSON.responsibleParty;
    }

    logger.traceEnter('transmitQuesionnaire: generatePayload');

    payload.A = _.map(diaryJSON.Answers, (answer) => {
        let answerPayload = {
            G: answer.response,
            F: answer.SW_Alias,
            Q: answer.question_id
        };

        if (answer.type) {
            answerPayload.T = answer.type;
            useJSONH = false;
        }

        return answerPayload;
    });

    logger.trace(`Setting payload.A: ${payload.A}`);

    if (diaryJSON.ink) {
        let ink = JSON.parse(diaryJSON.ink);
        payload.N = {
            D: ink.signatureData,
            X: ink.xSize,
            Y: ink.ySize,
            H: ink.author
        };
    }

    // compress answers collection with JSONH
    if (useJSONH) {
        payload.A = JSONH.pack(payload.A);
        logger.trace('Remapped after JSONH.pack');
    }

    return { payload, useJSONH };
}

/**
 * Fetches the diary related sata and populates the dataCache
 * @returns {Q.Promise}
 */
function fetchData () {
    // Optimize transmissions by avoiding having to decrypt the database for every transmission item.
    if (dataCache) {
        return Q();
    }

    dataCache = {};
    dataCache.dashboards = new Dashboards();
    dataCache.answers = new Answers();

    return dataCache.dashboards.fetch()
    .then(() => dataCache.answers.fetch());
}

/**
 * Returns the data for the specified diary record
 * @param {number} dashboardId - the id of the dashboard record for the diary data being requested
 * @param {string} sigId - the sigId of the dashboard record for the diary data being requested
 * @param {number} transmissionItemId - the id of the transmission record for the diary
 * @returns {Q.Promise}
 */
function getDiaryData (dashboardId, sigId, transmissionItemId) {
    // Find the correct dashboard record to transmit.
    let dashboardRecord = _.where(dataCache.dashboards.toJSON(), {
        id: dashboardId
    })[0];

    logger.trace(`transmitQuestionnaire: fetchData: dashboardRecord: ${dashboardRecord}`);

    if (!dashboardRecord) {
        logger.error(`Dashboard record does not exist. Dashboard ID: ${dashboardId}, sigID: ${sigId}`);
        logger.trace('transmitQuestionnaire: no dashboard record, destroying transmission item');
        return this.destroy(transmissionItemId)
        .then(() => Q.reject());
    }

    // Find the answer records that belong to the dashboard item.
    let diaryAnswers = _.where(dataCache.answers.toJSON(), {
        questionnaire_id: dashboardRecord.questionnaire_id,
        instance_ordinal: dashboardRecord.instance_ordinal
    });

    // If there are no answers...
    if (diaryAnswers.length === 0) {
        // StudyWorks will fail if "A" is []
        logger.error('transmitQuestionnaire failed to find answers');
    }

    // Copy the dashboard record data and the diary answers to the diaryJSON object
    let diaryJSON = _.extend({}, dashboardRecord);
    diaryJSON.Answers = diaryAnswers;

    return Q(diaryJSON);
}

/**
 * Bundles a questionnaire to be transmitted to the Web-Service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitQuestionnaire (transmissionItem) {
    let diaryJSON,
        params = JSON.parse(transmissionItem.get('params')),
        sendDiarySuccess = ({ isSubjectActive, isDuplicate }, diaryJSON) => {
            let subjectKrpt = diaryJSON.krpt || localStorage.getItem('krpt');

            logger.operational(`Diary transmitted: ${diaryJSON.SU} with Dashboard ID: ${params.dashboardId}, sigID: ${diaryJSON.sig_id}`);

            if (isDuplicate) {
                logger.error(`Duplicate transmission: ${diaryJSON.SU} with Dashboard ID: ${params.dashboardId}, sigID: ${diaryJSON.sig_id}`);
            }

            // Destroy the corresponding dashboard and answers records
            logger.trace(`transmitQuestionnaire: sendDiarySuccess: about delete item from database ${dataCache.dashboards.get(diaryJSON.id)}`);
            Backbone.sync('delete', dataCache.dashboards.get(diaryJSON.id));

            _.each(diaryJSON.Answers, (answer) => {
                logger.trace(`transmitQuestionnaire: sendDiarySuccess: _each: delete answer ${dataCache.answers.get(answer.id)}`);
                Backbone.sync('delete', dataCache.answers.get(answer.id));
            });

            this.destroy(transmissionItem.get('id'), $.noop);

            if (isSubjectActive) {
                return Q();
            }

            logger.trace('transmitQuestionnaire: sendDiarySuccess: !isSubjectActive saving subject not active');

            return Subjects.getSubjectBy({ krpt: subjectKrpt })
            .then(subject => subject.save({ subject_active: 0 }));
        },
        sendDiaryError = (resultSet) => {
            logger.trace('transmitQuestionnaire: sendDiaryError: removing transmissionItem');
            this.remove(transmissionItem);
            return Q.reject(resultSet);
        },
        handleCacheClear = () => {
            let isLastItem = !this.filter(item => item.get('method') === 'transmitQuestionnaire').length;

            if (isLastItem) {
                dataCache = null;
            }
        };

    return fetchData()
    .then(() => getDiaryData(params.dashboardId, params.sigId, transmissionItem.get('id')))
    .then((diaryData) => {
        let { payload, useJSONH } = generatePayload(diaryData),
            service = COOL.new('WebService', WebService);

        diaryJSON = diaryData;
        return service.sendDiary(payload, transmissionItem.get('token'), useJSONH);
    })
    .then(resultSet => sendDiarySuccess(resultSet, diaryJSON))
    .catch(sendDiaryError)
    .finally(() => {
        handleCacheClear();
        logger.traceExit('transmitQuesionnaire: generatePayload');
        logger.traceExit('transmitQuestionnaire');
    });
}
