import ELF from 'core/ELF';
import Logger from 'core/Logger';
import UserSync from 'core/classes/UserSync';
import Spinner from 'core/Spinner';
import { MessageRepo } from 'core/Notify';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';

const logger = new Logger('Transmit.transmitUserData');

/**
 * Bundles user data to be transmitted to the Web-Service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise<Object>}
 */
export default function transmitUserData (transmissionItem) {
    if (transmissionItem.get('status') === 'failed') {
        return Q.reject();
    }

    // Parse out the transmission item's parameters.
    let params = JSON.parse(transmissionItem.get('params'));

    // LF.Model.User may be used here to resolve a compile issue with import.
    let user = new LF.Model.User({ id: params.userId });

    // Fetch the user by ID.
    return user.fetch()
    .then(() => {
        let syncLevel = LF.StudyDesign.roles.findWhere({ id: user.get('role') }).get('syncLevel');

        if (!syncLevel) {
            // If no syncLevel, save user locally but not to SW.
            logger.operational(`User: ${user.get('username')} with role: ${user.get('role')} does not have a syncLevel and was not transmitted to SW`);

            // Destroy the transmission record and return rejected promise
            return this.destroy(transmissionItem.get('id'))
            .then(() => Q.reject());
        }

        let onError = (err) => {
            err && logger.error('Error Transmitting user data', err);

            this.remove(transmissionItem);

            return Q.reject(err);
        };

        let onSuccess = (resultSet) => {
            let { res } = resultSet,
                userRole = user.getRole();

            // 'E' = error
            if (res === 'E') {
                logger.error(`There was an error adding the user ${user.get('username')} with role ${user.get('role')}`);

                if (userRole && !userRole.canAddOffline()) {
                    // Destroy the transmission record...
                    return this.destroy(transmissionItem.get('id'))
                    .then(() => user.destroy())
                    .then(() => Q.reject());
                }

                this.remove(transmissionItem);
                return Q.reject();
            }

            // 'D' = Duplicate
            if (res === 'D') {
                logger.info(`Transmit User failed. User ${user.get('username')} with role ${user.get('role')} already exists`);

                return Spinner.hide()
                .then(() => ELF.trigger('TRANSMIT:Duplicate/User', { user, transmissionItem }, this))
                .then((evt) => {
                    if (evt.preventDefault) {
                        return Q();
                    }

                    const { Dialog } = MessageRepo;
                    return MessageRepo.display(Dialog && Dialog.USER_ALREADY_EXISTS_ERROR)
                    .then(() => this.destroy(transmissionItem.get('id')));
                })
                .then(() => resultSet);
            }

            return Q(parseInt(res, 10))
            .catch((err) => {
                logger.error('Error parsing result', err);
            })
            .then(userId => user.save({ userId }))
            .then(() => {
                logger.operational(`userId ${user.get('userId')} assigned to user ${user.get('username')}`);

                return this.destroy(transmissionItem.get('id'));
            })
            .then(() => resultSet)
            .catch((err) => {
                err && logger.error('Error saving user', err);
                this.remove(transmissionItem);

                return Q.reject(err);
            });
        };

        return UserSync.getValue(syncLevel)
        .then((syncValue) => {
            if (!syncValue && syncValue !== '') {
                // When there is no syncLevel, save user locally but do not transmit.
                logger.operational(`User: ${user.get('username')} with role: ${user.get('role')}
                    does not have a syncLevel and is only being stored locally`);

                return this.destroy(transmissionItem.get('id'))
                .then(() => Q.reject());
            }

            let userJSON = _.extend({},
                user.pick('username', 'userType', 'language', 'secretQuestion', 'secretAnswer', 'role', 'password', 'salt', 'active'), {
                    syncValue,
                    syncLevel
                });

            let service = COOL.new('WebService', WebService);

            return service.addUser(userJSON)

            // Let's first catch errors from servcie.addUser because onSuccess handler throws its own errors.
            .catch(onError)
            .then(onSuccess);
        });
    });
}
