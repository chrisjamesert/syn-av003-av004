import Logger from 'core/Logger';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';

const logger = new Logger('Transmit.updateUserCredentials');

/**
 * Handles updateUserCredentials transmission for non-subject users.
 * @param  {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise<Object>} Q Promise
 */
export default function updateUserCredentials (transmissionItem) {
    let onSuccess = (resultSet) => {
        let { res } = resultSet;

        logger.trace(`updateUserCredentials -> SUCCESS: ${res}`);

        return Q()
        .then(() => {
            if (res === 'E') {
                this.remove(transmissionItem);

                logger.error('Error updating credentials for user');
                return Q();
            }

            return this.destroy(transmissionItem.get('id'));
        })
        .then(() => resultSet);
    };

    let onError = ({ errorCode }) => {
        this.remove(transmissionItem);

        errorCode && logger.error('User credentials change failed with error code: {{errorCode}}', { errorCode });

        return Q.reject();
    };

    let service = COOL.new('WebService', WebService);

    return Q(JSON.parse(transmissionItem.get('params')))
    .catch((e) => {
        logger.error('transmissionItem.params is not valid JSON!', e);

        // Something went wrong building transmission. Remove transmission from queue and collection.
        return this.destroy(transmissionItem.get('id'))
        .then(() => Q.reject(e));
    })
    .then(transmitData => service.updateUserCredentials(transmitData))
    .then(onSuccess)
    .catch(onError);
}
