import Logger from 'core/Logger';
import ELF from 'core/ELF';
import WebService from 'core/classes/WebService';
import COOL from 'core/COOL';

const logger = new Logger('HistoricalDataSync');

/**
 * Handles historical data sync transmission to the web service
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function historicalDataSync (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params')),
        historicalDataSyncSuccess = ({ res }) => {
            // Don't update data unless we get a response
            if (_.isEmpty(res)) {
                return this.destroy(transmissionItem.get('id'));
            }
            let collection = new LF.Collection[params.collection]();

            return collection.fetch()
            .then(() => ELF.trigger(`HISTORICALDATASYNC:Received/${params.collection}`, { res, collection }))
            .then(() => this.destroy(transmissionItem.get('id')))
            .catch((err) => {
                err && logger.error('Error destroying transmission', err);
                return Q.reject(err);
            });
        },
        historicalDataSyncError = (err) => {
            // Remove from queue
            return this.destroy(transmissionItem.get('id'))
            .then(() => Q.reject(err));
        };

    return COOL.new('WebService', WebService)[params.webServiceFunction](params)
    .catch(historicalDataSyncError)
    .then(historicalDataSyncSuccess);
}
