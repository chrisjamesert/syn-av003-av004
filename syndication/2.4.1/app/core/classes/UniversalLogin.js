/* eslint-disable new-cap */
import Logger from 'core/Logger';
import LoginView from 'core/views/LoginView';
import ChangeTemporaryPasswordView from 'core/views/ChangeTemporaryPasswordView';
import ResetSecretQuestionView from 'core/views/ResetSecretQuestionView';
import ResetPasswordView from 'core/views/ResetPasswordView';
import ForgotPasswordView from 'core/views/ForgotPasswordView';
import UnlockCodeView from 'core/views/UnlockCodeView';
import AccountConfiguredView from 'sitepad/views/AccountConfiguredView';

let logger = new Logger('UniversalLogin');

export default class UniversalLogin {
    constructor (options = {}) {
        logger.traceEnter('constructor');

        // Allow for optional views to be used instead of the standard core views
        this.optLoginView = options.loginView;
        this.optChangeTempPasswordView = options.changeTempPasswordView;
        this.optResetSecretQuestionView = options.resetSecretQuestionView;
        this.optResetPasswordView = options.resetPasswordView;
        this.optForgotPasswordView = options.forgotPasswordView;
        this.optSiteSelectionView = options.siteSelectionView;
        this.optUnlockCodeView = options.unlockCodeView;
        this.noRoleFound = options.noRoleFound;
        this.optAcctConfiguredView = options.acctConfiguredView;

        // This is to fix a bad hard dependency in scheduling
        this.optAppController = options.appController;

        // Don't lose track of the subject and visit
        this.subject = options.subject;
        this.visit = options.visit;

        logger.traceExit('constructor');
    }

    newUserLogin ({ successfulLogin, filterUsers, sortUsers, backArrow, forwardArrow, options }) {
        logger.traceEnter('newUserLogin');

        if (!successfulLogin) {
            // throw error we need to have this function at a minimum
        }

        this.successfulLogin = successfulLogin;
        this.backArrow = backArrow;
        this.forwardArrow = forwardArrow;
        this.filterUsers = filterUsers;
        this.sortUsers = sortUsers;
        this.options = options || {};

        this.universalLoginNavigator = (route) => {
            switch (route) {
                case 'dashboard': {
                    // fix for DE16593 - removing this flag after user has reset their SQ and A
                    // rather than doing it on core/ResetSecretQuestionView which would cause SitePad issues
                    localStorage.removeItem('Reset_Password_Secret_Question');

                    this.clear();
                    return this.successfulLogin();
                }
                case 'forgot-password': {
                    return this.goToForgotPassword();
                }
                case 'select-site': {
                    return this.goToSiteSelection();
                }
                case 'change_temporary_password': {
                    return this.goToChangeTempPassword();
                }
                case 'reset_secret_question': {
                    return this.goToResetSecretQuestion();
                }
                case 'reset_password': {
                    return this.goToResetPassword();
                }
                case 'login': {
                    return this.goToLogin();
                }
                case 'unlock_code': {
                    return this.goToUnlockCode();
                }
                case 'account_configured': {
                    return this.goToAccountConfigured();
                }

                // no default
                default : {
                    return Q();
                }
            }
        };

        return this.goToLogin()
        .then(() => {
            logger.traceExit('newUserLogin');
        });
    }

    renderView (newView) {
        // Clear the View
        this.clear();

        this.view = newView;

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => {
            LF.Notify.Banner.closeAll();
        })
        .catch((err) => {
            err && logger.error(err);
        });
    }

    clear () {
        if (this.view) {
            this.view.remove();
        }
    }

    goTo (view) {
        view.navigate = this.universalLoginNavigator;
        return this.renderView(view);
    }

    goToLogin () {
        let viewOptions = _.extend(this.options, {
            backArrow: this.backArrow,
            forwardArrow: this.forwardArrow,
            filterUsers: this.filterUsers,
            sortUsers: this.sortUsers,
            noRoleFound: this.noRoleFound
        });

        let view = new (this.optLoginView || LoginView)(viewOptions);
        view.subject = this.subject;
        view.visit = this.visit;

        // To fix a bad hard dependency in scheduling
        if (this.optAppController) {
            this.optAppController.view = view;
        }

        return this.goTo(view);
    }

    goToForgotPassword () {
        let view = new (this.optForgotPasswordView || ForgotPasswordView)(this.options);

        return this.goTo(view);
    }

    goToSiteSelection () {
        if (!this.optSiteSelectionView) {
            throw new Error('Invalid setup.  goToSiteSelection() requires siteSelectionView to be passed into UniversalLogin.');
        }
        let view = new this.optSiteSelectionView(this.options);

        return this.goTo(view);
    }

    goToChangeTempPassword () {
        let view = new (this.optChangeTempPasswordView || ChangeTemporaryPasswordView)(this.options);

        return this.goTo(view);
    }

    goToResetSecretQuestion () {
        let view = new (this.optResetSecretQuestionView || ResetSecretQuestionView)(this.options);
        view.subject = this.subject;
        return this.goTo(view);
    }

    goToResetPassword () {
        let view = new (this.optResetPasswordView || ResetPasswordView)(this.options);

        return this.goTo(view);
    }

    goToUnlockCode () {
        let view = new (this.optUnlockCodeView || UnlockCodeView)(this.options);
        return this.goTo(view);
    }

    goToAccountConfigured () {
        let view = new (this.optAcctConfiguredView || AccountConfiguredView)(this.options);
        return this.goTo(view);
    }
}
