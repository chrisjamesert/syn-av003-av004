import * as Helpers from 'core/Helpers';
import Session from './Session';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import Spinner from 'core/Spinner';

const logger = new Logger('PatientSession');

export default class PatientSession extends Session {
    /**
     * Logs out of the application.
     * @param {Boolean} [navigate] If the application should display the login page after logout
     * @returns {Object} '{@link LF.Class.ApplicationSecurity}'
     */
    // eslint-disable-next-line consistent-return
    logout (navigate) {
        if (this.checkLogin()) {
            logger.operational(`${CurrentContext().role} user logout`);
        }

        localStorage.removeItem('PHT_isAuthorized');
        delete this.activeUser;

        localStorage.removeItem('Subject_Login');
        localStorage.removeItem('Site_Login');

        if (navigate) {
            // Close modal.
            $('#modal.in').modal('hide');

            // Close any modals, including number spinner modals
            $('.modal').modal('hide');

            // Close all mobiscroll widgets
            $('.mbsc-jqm').remove();

            // Close all modal backgrounds
            $('.modal-backdrop').remove();

            // Close Time/Date Picker
            $('input[data-role=datebox]').trigger('datebox', {
                method: 'close'
            });

            Spinner.hide()
            .then(() => {
                LF.router.navigate('login', true);
            })
            .done();

            return this;
        }
    }

    /**
     * Checks if user can access the questionnaire
     * @param {String} scheduleId unique schedule configuration id for this questionnaire.
     * @returns {boolean} True if site user tries to access site diary or subject user tries to access subject diary.
     * False if site user tries to access subject diary or subject user tries to access site diary.
     */
    checkQuestionnaireAccess (scheduleId) {
        let scheduleModels = Helpers.getScheduleModels(),
            schedule = _.find(scheduleModels, item => item.get('id') === scheduleId);

        if (schedule == null) {
            return false;
        }

        return _.contains(schedule.get('scheduleRoles'), CurrentContext().role) ||
                CurrentContext().role === 'subject' && typeof schedule.get('scheduleRoles') === 'undefined';
    }

    /**
     * Removes all the time traces from the localStorage.
     * @param {Function} [callback] If the callback is passed in, it will be executed.
     * @example LF.security.removeTimeTrace(function () { });
     */
    removeTimeTrace (callback) {
        // TODO: There should be no need for this function anymore. Can be removed
        localStorage.removeItem('PHT_Last_Active');
        localStorage.removeItem('PHT_Timeout_LastCheck');
        this.timerOn = false;

        if (callback) {
            callback();
        }
    }
}
