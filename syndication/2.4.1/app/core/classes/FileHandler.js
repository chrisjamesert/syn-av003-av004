import * as utils from 'core/utilities';
import Logger from 'core/Logger';

const logger = new Logger('FileHandler'),
    downloadOptions = {
        retry: [1000, 2000, 3000]
    };

/* globals zip */

/**
 * Object to conveniently handle filesystem requests in a promise-based manner.
 */
class FileHandler {
    /**
     * @property {FileSystem}
     * Getter for file system object.
     */
    get fs () {
        if (!this._fs) {
            this._fs = utils.createFilesystem();
        }
        return this._fs;
    }

    /**
     * Construct a file handler, starting at a root directory.
     * @param {string} [rootDir=''] root directory for this filesystem handler
     */
    constructor (rootDir = '') {
        this._fs = null;

        this._rootDir = rootDir;
    }

    /**
     * Generate a path by prepending the root directory, if it exists.
     * @param {string} path relative path from root directory
     * @returns {string} path, unaltered or prepended by root directory.
     */
    generatePath (path) {
        if (this._rootDir !== '') {
            return `${this._rootDir}/${path}`;
        }
        return path;
    }

    /**
     * Write a blob to a file.
     * @param {string} path path to file
     * @param {Blob} contents blob to write to file
     * @returns {Q.Promise<void>} promise resolving when write is finished.
     */
    write (path, contents) {
        return this.fs.write(this.generatePath(path), contents)
        .catch((e) => {
            logger.error(`Error writing file: ${this.generatePath(path)}: ${e}`);
        });
    }

    /**
     * Remove file at this path.
     * @param {string} path filePath for file or directory to remove.
     * @returns {Q.Promise<void>} promise resolving when path is removed.
     */
    remove (path) {
        return this.fs.remove(this.generatePath(path));
    }

    /**
     * Get file size for path.
     * @param {string} path path to file.
     * @returns {Q.Promise<number>} promise resolving with the file size... -1 if file does not exist.
     */
    getSize (path) {
        return this.fs.file(this.generatePath(path))
        .then((fileEntry) => {
            return Q.Promise((resolve) => {
                fileEntry.getMetadata((meta) => {
                    resolve(meta.size);
                });
            });
        }, () => {
            return -1;
        });
    }

    /**
     * Find first index.html file, and return its URL.
     * @param {string} path path to directory in which to find index file.
     * @param {string} [indexFile='index.html'] name of the index file.
     * @returns {Q.Promise<string>} promise resolving with string containing the URL to the index file.
     */
    getIndexURL (path, indexFile = 'index.html') {
        return this.fs.toURL(this.generatePath(`${path}/${indexFile}`));
    }

    /**
     * Get the full path (in the form of nativeURL) for a file.
     * @param {string} path path to file
     * @returns {Q.Promise<string>} Promise resolving with the full path to the file.  Resolves with null if nonexistent.
     */
    getFileNativeURL (path) {
        return this.fs.file(this.generatePath(path))
        .then((fileEntry) => {
            return fileEntry.nativeURL;
        }, () => null);
    }

    /**
     * Extract a zip file
     * @param {string} pathToZip Zip file to be extracted
     * @returns {Q.Promise<string>} promise resolving with the full extracted path
     */
    extractZipFile (pathToZip) {
        if (!pathToZip) {
            return Q.resolve('');
        }

        return this.getFileNativeURL(pathToZip)
        .then((fullPath) => {
            return Q.Promise((resolve, reject) => {
                let destination = fullPath.replace(/\.zip$/i, '');
                zip.unzip(fullPath, destination, (cb) => {
                    cb === 0 ? resolve(destination) : reject();
                });
            });
        });
    }

    /**
     * Download a file to the local filesystem.
     * @param {string} sourceURL URL for file retrieval
     * @param {string} path path to new file entry
     * @returns {Q.Promise<void>} promise resolving when download is complete.
     */
    download (sourceURL, path) {
        let genPath = this.generatePath(path);
        return this.fs.create(genPath)
        .then(() => {
            return this.fs.download(sourceURL, genPath, downloadOptions);
        });
    }
}

export default FileHandler;
