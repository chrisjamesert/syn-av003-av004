import AnswerOptions from '../collections/AnswerOptions';
import Questionnaires from '../collections/Questionnaires';
import Screens from '../collections/Screens';
import Questions from '../collections/Questions';
import Roles from '../collections/Roles';
import Visits from '../collections/Visits';
import Schedules from '../collections/Schedules';
import Indicators from '../collections/Indicators';
import Widgets from '../collections/Widgets';
import SecurityQuestions from '../collections/SecurityQuestions';
import Environments from '../collections/Environments';
import Branches from '../collections/Branches';
import Logger from 'core/Logger';
import ELF from 'core/ELF';
import COOL from 'core/COOL';

const logger = new Logger('StudyDesign');

const collectionMap = {
    affidavits: Questions,
    answeroptions: AnswerOptions,
    branches: Branches,
    environments: Environments,
    indicators: Indicators,
    questionnaires: Questionnaires,
    questions: Questions,
    roles: Roles,
    schedules: Schedules,
    screens: Screens,
    securityquestions: SecurityQuestions,
    visits: Visits,
    widgets: Widgets
};

const ID = 'id';

// TODO: there may be some ordering issues,
//   i.e. coll A references coll B, but coll B has not yet been defined.
//   have not seen a problem yet, but it may be lurking.
export default class StudyDesign {
    constructor (design) {
        this.failedValidation = false;

        // Create empty collections to start
        //  necessary for collections that are not explicitly created in
        // study-design.js, such as Widget
        _(collectionMap).forEach((Coll, name) => {
            if (!this[name]) {
                this[name] = new Coll();
            }
        });

        logger.traceEnter('Loading and validating study design');

        // key and value are raw as found in study-design.js
        // Some are to be converted to Collections, e.g. Screens;
        // some are not, e.g. studyDesignVersion
        _.forEach(design, (value, key) => {
            // if applicable, convert raw data to Collection
            if (collectionMap[key]) {
                this.validateIdsProvided(key, value);

                // This has to be done here, not in validate()
                // because subsequent backbone processing causes
                // duplicate id's to be lost
                this.validateIdUniqueness(key, value);

                // Instantiate the actual collection.
                this.assign(value, key);

                // Handle e.g. Widget embedded in Question
                this.loadEmbeddedModels(this[key]);
            } else {
                if (key !== 'rules') {
                    // non-collections these get stored as-is
                    this[key] = value;
                } else {
                    // Add the rules to ELF.
                    ELF.rules.add(value);
                }
            }
        });

        this.validate();

        logger.traceExit();
    }

    // Instantiate the actual collection. A lot happens here:
    // - An appropriate Collection gets created
    // - all the raw data gets converted to individual models
    // - these models get added to the Collection
    // - the Collection replaces the raw data in this[key]
    assign (data, collectionName) {
        let collection = new collectionMap[collectionName](data);
        this[collectionName] = collection;
    }

    // For embedded models, instantiate the nested models as well
    // e.g. Widget is embedded in Question
    // we dig it out, and add it to an appropriate Collection
    // Note: only works one sub-level deep.
    loadEmbeddedModels (collection) {
        collection.forEach((model) => {
            _(model.schema).forEach((propDef, propName) => {
                let relSpec = propDef.relationship;

                // If there is a relationship, and it is embedded...
                if (relSpec && relSpec.embedded) {
                    let propVal = model.get(propName);
                    if (!propVal) {
                        propVal = [];
                    } else if (!(propVal instanceof Array)) {
                        propVal = [propVal];
                    }

                    let toCollection = this.getCollection(relSpec.to),
                        tmpCollection = new toCollection.constructor();

                    propVal.forEach((subModelData, index) => {
                        // If child has no id, give it id constructed from parent obj id,
                        // rel name, and position within rel array
                        if (!subModelData[ID]) {
                            subModelData[ID] = `${model.id}_${propName}_${index}`;
                        }
                        // eslint-disable-next-line new-cap
                        let subModel = new toCollection.model(subModelData);
                        tmpCollection.add(subModel);
                    });

                    // If the embedded collection has other embedded collections, load them as well.
                    if (this.hasEmbeddedModels(tmpCollection)) {
                        this.loadEmbeddedModels(tmpCollection);
                    }
                    toCollection.add(tmpCollection.models);
                }
            });
        });
    }

    hasEmbeddedModels (collection) {
        let schema = collection.model.schema;

        return _(schema).any((propDef) => {
            let relSpec = propDef.relationship;

            return relSpec && relSpec.embedded;
        });
    }

    // operates on raw data, not Collection.
    // Validates that all objects have an ID.
    validateIdsProvided (collName, value) {
        let noIdEnts = _(value).filter(it => !it[ID]);
        noIdEnts.forEach((value, pos) => {
            logger.fatal(`${collName}[${pos}] has no ${ID}`);
            logger.debug(JSON.stringify(value));
            this.failedValidation = true;
        });
    }

    // operates on raw data, not Collection. Necessary because
    // Collection replaces objects when other one
    // with same id is inserted
    validateIdUniqueness (collName, value) {
        let counts = _(value).countBy(ID);
        let dupes = _(counts).keys().filter(key => counts[key] > 1);
        dupes.forEach((key) => {
            let where = `${this.getModelName(collName)}[${key}]`;
            logger.fatal(`${where} in collection ${collName} has duplicate id (${counts[key]} occurrences)`);
            this.failedValidation = true;
        });
    }

    /*
     * A hook-in function for PDEs to use in customizing QR display. This is expected to be overridden.
     * Takes a subject object to inspect and returns a boolean flag base on result.
     * Uses the global study parameter 'displaySubjectQRCodes' as first half of boolean check, with
     * expectation of study-specific inspection of the 'subject' object  as the second half.
     * @param {Subject} subject - Subject object to perform inspection on.
     * @return {boolean} Flag value resulting from inspection of subject.
     */
    enableQRForSubject (subject) {
        // This code expected to be customized by study team.
        let enable = (LF.StudyDesign.displaySubjectQRCodes === true) && (subject !== undefined);
        return enable;
    }

    validateRels (fromObj) {
        _(fromObj.schema).forEach((propDef, propName) => {
            let relSpec = propDef.relationship;
            if (relSpec) {
                // Instead of fromObj.get(ID), we can use fromObj.id.
                // This allows us to set the idAttribute property on the model to determine what should be used as the ID.
                // ex. { idAttribute: 'value' };
                let fromId = fromObj.id;
                let toEnt = relSpec.to;
                let propVal = fromObj.get(propName);
                let toIds;

                // For logging purposes, formatted name of prop being validated;
                // e.g. Questionnaire[MORNING_DIARY].screens
                let where = `${fromObj.modelName}[${fromId}].${propName}`;

                if (propVal !== null && propVal !== undefined) {
                    // Treat all rels like arrays, for simplicity
                    if (!(propVal instanceof Array)) {
                        propVal = [propVal];
                    }

                    // TODO: Bit hacky. Fix later
                    if (relSpec.embedded) {
                        relSpec = _.extend({}, relSpec, { via: ID }); // don't modify the actual schema
                    }

                    // Example of via:
                    // schema: { questions: { relationship: {to:Questions, via:id} } }
                    // SD.screens: [{ id:S1, questions: [ {id:Q1, mandatory:true} ] }
                    if (relSpec.via) {
                        // Test that each element of propVal:
                        //   - is an object
                        //   - has property named relSpec.via
                        if (_(propVal).any(pv => !pv[relSpec.via])) {
                            logger.fatal([
                                `${where} malformed;`,
                                `should be [ {${relSpec.via}: ${this.getModelName(relSpec.to)}ID, ...}, ...]`
                            ].join(' '));
                            logger.debug(JSON.stringify(fromObj));
                            this.failedValidation = true;
                        }
                        toIds = _(propVal).pluck(relSpec.via);
                    } else {
                        toIds = propVal;
                    }

                    // now propVal is an array of id's presumably of toEnt models
                    toIds.forEach((toId, pos) => {
                        if (toId === null || toId === undefined) {
                            logger.fatal(`${where} has missing ${ID} at index ${pos}`);
                            logger.debug(JSON.stringify(propVal[pos]));
                        } else {
                            if (relSpec.ignore && relSpec.ignore.indexOf(toId) !== -1) {
                                logger.info(`${where} ignored ${this.getModelName(toEnt)}[${toId}]`);
                            } else {
                                let toObj = this.get(toEnt, toId); // may or may not be found
                                if (!toObj) {
                                    logger.fatal(`${where} references non-existent ${this.getModelName(toEnt)}[${toId}]`);
                                    logger.debug(JSON.stringify(fromObj));
                                    this.failedValidation = true;
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    validate () {
        _(collectionMap).keys().forEach((elemType) => {
            let collection = this[elemType];
            collection.forEach((model) => {
                if (!model.isValid()) {
                    let errMsg = model.validationError.message;
                    logger.fatal(`${errMsg}`);
                    logger.debug(JSON.stringify(model));
                    this.failedValidation = true;
                }
                this.validateRels(model);
            });
        });
        return !this.failedValidation;
    }

    // Given an object type, e.g. Affidavits, and an id,
    // return the object of that type with that id
    get (objType, objId) {
        let collection = this.getCollection(objType);
        let model = collection.get(objId);
        return model;
    }

    getCollection (objType) {
        return this[objType.toLowerCase()]; // yeah, bit hacky
    }

    getModelName (objType) {
        let collection = this[objType.toLowerCase()];
        let modelClass = collection.model;
        let modelName = modelClass.name;
        return modelName;
    }
}

COOL.add('StudyDesign', StudyDesign);
