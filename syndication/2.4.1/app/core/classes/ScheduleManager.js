import Alarm from 'core/classes/Alarm';
import Schedule from 'core/models/Schedule';
import StoredSchedule from 'core/models/StoredSchedule';
import Schedules from 'core/collections/Schedules';
import LastDiaries from 'core/collections/LastDiaries';
import Dashboards from 'core/collections/Dashboards';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import * as DateTimeUtil from 'core/DateTimeUtil';

let logger = new Logger('ScheduleManager');

/**
 * A class that creates new Schedule Object.
 * @class ScheduleManager
 * @example let schedule = new ScheduleManager();
 */
export default class ScheduleManager {
    constructor () {
        this.availableSchedules = new Schedules();
    }

    /**
     * Dynamically add a new schedule and save it to the local db
     * @param {Object} schedule  new schedule to save
     * @param {Function} callback A callback function with schedule model as a parameter
     * @example LF.schedule.addSchedule({
     *              name : "Blank_Diary_Schedule_Again",
     *              target : {
     *                  objectType : "questionnaire",
     *                  id : "Blank_Diary"
     *              },
     *              scheduleFunction : "checkAlwaysAvailability"
     *          });
     */
    addSchedule (schedule, callback = $.noop) {
        let model = StoredSchedule.fromSchedule(new Schedule(schedule));

        model.save({})
        .then(() => {
            LF.schedules.add(model);
            callback(model);
        }, (err) => {
            logger.error('ScheduleManager.addSchedule failed to save the schedule model', err);
            callback();
        });
    }

    /**
     * Remove schedule from database
     * @param {Object} params   should include: scheduleId  id of schedule to be removed. Only the first found with this id is removed.
     *                                     and: [dbId]  optional param removes schedule with id : scheduleId and dbId : dbId
     * @param {Function} callback A callback function called after removing from the database.
     * @example  LF.schedule.removeSchedule( { scheduleId : 'Blank_Diary', dbId : 1});
     */
    removeSchedule (params, callback = $.noop) {
        let schedule,
            dbId = params.dbId,
            scheduleId = params.scheduleId;

        if (dbId) {
            schedule = LF.schedules.filter((schedule) => {
                return schedule.get('scheduleId') === scheduleId && schedule.get('id') === dbId;
            })[0];
        } else {
            schedule = LF.schedules.where({ scheduleId })[0];
        }

        if (schedule) {
            schedule.destroy()
            .then(() => {
                callback();
            }, (err) => {
                logger.error('ScheduleManager.removeSchedule failed to destroy the schedule model', err);
                callback();
            });
        } else {
            callback();
        }
    }

    /**
     * Determine the current schedules.
     * @param {LF.Collection.Schedules} schedules a collection holding all schedules to be determined
     * @param {Object} context Object that has Current subject and visit to evaluate
     * @param {Function} callback A callback function with phase number parameter.
     * @example LF.Schedule.determineSchedules(this, _(function (availableSchedules) {...}));
     */
    determineSchedules (schedules, context, callback) {
        let setNextScheduleRefresh = false;

        this.schedules = schedules;
        this.subject = context.subject;

        if (schedules !== undefined && schedules.length > 0) {
            LastDiaries.fetchCollection()
            .catch((err) => {
                logger.error('Fetch of completedQuestionnaires failed', err);
            })
            .then((completedQuestionnaires) => {
                let removeSchedules = new Dashboards(),
                    availableSchedules = [],
                    phaseInfo = this.getPhaseInformation(this.subject),
                    step = (counter) => {
                        this.evaluateSchedule(schedules.at(counter), phaseInfo, completedQuestionnaires, context, (availabilityCallback) => {
                            let scheduleParams = schedules.at(counter).get('scheduleParams');

                            if (availabilityCallback) {
                                availableSchedules.push(schedules.at(counter));
                            } else {
                                removeSchedules.add(schedules.at(counter));
                            }

                            // check if there is at least one availability object in schedules
                            if (scheduleParams != null && scheduleParams.startAvailability != null && scheduleParams.endAvailability != null) {
                                setNextScheduleRefresh = true;
                            }

                            if (schedules.length > counter + 1) {
                                step(counter + 1);
                            } else {
                                // if there is any availability object in schedules set next schedule refresh
                                if (setNextScheduleRefresh) {
                                    this.setNextScheduleRefresh(schedules, removeSchedules);
                                }

                                callback(availableSchedules);
                            }
                        });
                    };

                if (localStorage.getItem('alarmsReset')) {
                    this.alarmRefresh(completedQuestionnaires);
                }

                if (schedules.length) {
                    this.availableSchedules.trigger('beforeEvaluate');
                    step(0);
                }
            });
        } else {
            callback(null);
        }
    }

    /**
     * Determine the current alarm schedules.
     * @param {LF.Collection.Schedules} schedules a collection holding all schedules to be determined
     * @param {Object} context Object that has Current subject to evaluate
     * @param {Function} callback A callback function with phase number parameter.
     * @example LF.schedule.determineAlarmSchedules(this, _(function (availableSchedules) {...}));
     */
    determineAlarmSchedules (schedules, context, callback) {
        this.schedules = schedules;
        this.subject = context.subject;

        if (schedules !== undefined && schedules.length > 0) {
            LastDiaries.fetchCollection()
            .then((completedQuestionnaires) => {
                let removeSchedules = new Dashboards(),
                    availableSchedules = [],
                    phaseInfo = this.getPhaseInformation(this.subject),
                    step = (counter) => {
                        this.evaluateAlarmSchedule(schedules.at(counter), phaseInfo, completedQuestionnaires, context, (availabilityCallback) => {
                            if (availabilityCallback) {
                                availableSchedules.push(schedules.at(counter));
                            } else {
                                removeSchedules.add(schedules.at(counter));
                            }

                            if (schedules.length > counter + 1) {
                                step(counter + 1);
                            } else {
                                callback(availableSchedules);
                            }
                        });
                    };

                if (schedules.length) {
                    this.availableSchedules.trigger('beforeEvaluate');
                    step(0);
                }
            });
        } else {
            callback(null);
        }
    }

    /**
     * Sets the next nearest refresh change
     * based on scheduling of all questionnaires
     * @param {Array} schedules list of all schedules
     * @param {LF.Collection.Dashboards} removeSchedules collection of schedules that needs to be removed from refresh
     * @param {Function} [callback] A callback function invoked upon.
     */
    setNextScheduleRefresh (schedules, removeSchedules, callback = $.noop) {
        let nowUtcTimeStamp = new Date().getTime(),
            timeStampArray,
            removeTimeStampArray,
            num,
            index,
            tmpDate,
            tmpOffset,
            changeTime,
            changeHour = -1,
            currentDate = new Date(),
            hour = currentDate.getHours(),
            nextScheduleRefreshUtc = 0,
            nextPossibleRefreshes = [],
            checkDate = new Date(new Date().setHours(20, 0, 0, 0)),
            isDSTShiftDay = currentDate.getTimezoneOffset() !== checkDate.getTimezoneOffset();

        // Get complete array of start and end times for all questionnaires
        // eslint-disable-next-line consistent-return, array-callback-return
        timeStampArray = _.filter(_.flatten(schedules.map((schedule) => {
            let params = schedule.get('scheduleParams');

            if (params && params.startAvailability && params.endAvailability) {
                return [DateTimeUtil.parseTime(params.startAvailability, true), DateTimeUtil.parseTime(params.endAvailability, true)];
            }
        })));

        // Get end times of questionnaires to be removed
        // eslint-disable-next-line consistent-return, array-callback-return
        removeTimeStampArray = _.filter(_.flatten(removeSchedules.map((schedule) => {
            let params = schedule.get('scheduleParams');

            if (params && params.endAvailability) {
                return [DateTimeUtil.parseTime(params.endAvailability, true)];
            }
        })));

        // Remove end times of questionnaires to be removed from complete array
        for (num in removeTimeStampArray) {
            index = _.indexOf(timeStampArray, removeTimeStampArray[num]);
            if (index >= 0) {
                timeStampArray.splice(index, 1);
            }
        }

        // Determine next refresh based on edited array of time stamps
        if (timeStampArray.length !== 0) {
            nextPossibleRefreshes = _(timeStampArray).filter((time) => {
                return time > nowUtcTimeStamp;
            });

            // filter will return empty array if it doesn't find the time
            if (nextPossibleRefreshes.length === 0) {
                let timestamp = _.min(timeStampArray),
                    tempDate = new Date(timestamp);

                tempDate.setDate(tempDate.getDate() + 1);
                nextScheduleRefreshUtc = tempDate.getTime();
            } else {
                // get the nearest one that is left for current day
                nextScheduleRefreshUtc = _.min(nextPossibleRefreshes);
            }

            if (isDSTShiftDay) {
                while (changeHour === -1 && hour < 24) {
                    tmpDate = new Date(new Date().setHours(hour, 0, 0, 0));
                    tmpOffset = tmpDate.getTimezoneOffset();

                    if (tmpOffset === currentDate.getTimezoneOffset()) {
                        hour++;
                    } else {
                        changeHour = hour;
                        break;
                    }
                }

                changeTime = new Date(new Date().setHours(changeHour, 0, 0, 0)).getTime();
                nextScheduleRefreshUtc = Math.min(nextScheduleRefreshUtc, changeTime);
            }

            localStorage.setItem('Next_Schedule_Refresh', nextScheduleRefreshUtc);

            this.startTimer();

            callback();
        }
    }

    /**
     * Get the timeStamp of the next refresh
     * @return {Number | Boolean} next refresh timeStamp
     */
    getNextScheduleRefresh () {
        return localStorage.getItem('Next_Schedule_Refresh');
    }

    /**
     * Evaluates the scheduling configuration of the schedule object
     * @param {LF.Model.Schedule} schedule for evaluating the scheduling
     * @param {Object} phaseInfo object containing current phase information
     * @param {LF.Collection.LastDiaries} completedQuestionnaires list of all completed diaries
     * @param {Object} context - The current context to be passed into the schedule function.
     * @param {Function} callback A callback function invoked upon.
     * @example LF.Schedule.evaluateSchedule(scheduleObject, phaseInfo, completedQuestionnaires, parentView, callbackFunction)
     */
    evaluateSchedule (schedule, phaseInfo, completedQuestionnaires, context, callback) {
        let scheduleRoles = schedule.get('scheduleRoles') || ['subject'],
            scheduleFunction = LF.Schedule.schedulingFunctions[schedule.get('scheduleFunction')],
            alarm = new Alarm(schedule),
            target = schedule.get('target'),
            role = CurrentContext().role;

        logger.info(`Evaluating schedule for ${schedule.get('id')}`);

        // check if the user's role is configured for this schedule
        if (_.contains(scheduleRoles, role)) {
            // Check if schedule function exists
            if (scheduleFunction) {
                if (!phaseInfo || this.isScheduleInPhase(schedule, phaseInfo.phase)) {
                    const isLoginView = LF.router.view() && LF.router.view().id === 'login-view';
                    alarm.evaluateAlarm(schedule, completedQuestionnaires, LF.router.view());

                    if (target.objectType === 'indicator' && target.showOnLogin === false && isLoginView) {
                        // callback with false value since the indicator is not suppoes to be displayed on Login
                        callback(false);
                    } else {
                        // Call the schedule function that is defined within schedule configuration
                        scheduleFunction(schedule, completedQuestionnaires, LF.router.view(), context, (availabilityCallback) => {
                            callback(availabilityCallback);
                        });
                    }
                } else {
                    // Cancel alarm if the scheduling diary is not in the current phase
                    alarm.cancelAlarm();
                    callback(false);
                }
            } else {
                logger.error(`Missing schedule function in schedule configuration id: ${schedule.get('id')}`);
                callback(false);
            }
        } else {
            callback(false);
        }
    }

    /**
     * Evaluates the alarm scheduling configuration of the schedule object
     * @param {LF.Model.Schedule} schedule for evaluating the alarm
     * @param {Object} phaseInfo object containing current phase information
     * @param {LF.Collection.LastDiaries} completedQuestionnaires list of all completed diaries
     * @param {Object} context - The current context to be passed into the schedule function.
     * @param {Function} callback A callback function invoked upon.
     * @example LF.schedule.evaluateAlarmSchedule(scheduleObject, phaseInfo, completedQuestionnaires, callbackFunction)
     */
    evaluateAlarmSchedule (schedule, phaseInfo, completedQuestionnaires, context, callback) {
        let scheduleFunction = LF.Schedule.schedulingFunctions[schedule.get('scheduleFunction')],
            scheduleRoles = schedule.get('scheduleRoles') || ['subject'],
            target = schedule.get('target'),
            accessRoles = target.id && target.objectType === 'questionnaire' ? _.findWhere(LF.StudyDesign.questionnaires.models, {
                id: target.id
            }).get('accessRoles') : undefined,
            scheduleOrAccessRoles = accessRoles || scheduleRoles,
            alarm = new Alarm(schedule);

        Users.fetchCollection()
        .then((users) => {
            let usersRoles = _.uniq(_.invoke(users.models, 'get', 'role'));

            // Check if schedule function exists
            if (scheduleFunction) {
                if (!phaseInfo || this.isScheduleInPhase(schedule, phaseInfo.phase)) {
                    if (_.intersection(scheduleOrAccessRoles, usersRoles).length) {
                        scheduleFunction(schedule, completedQuestionnaires, LF.router.view, context, (availabilityCallback) => {
                            if (availabilityCallback) {
                                alarm.evaluateAlarm(schedule, completedQuestionnaires, LF.router.view);
                                callback(true);
                            } else {
                                callback(false);
                            }
                        }, true);
                    }
                } else {
                    // Cancel alarm if the scheduling diary is not in the current phase
                    alarm.cancelAlarm();
                    callback(false);
                }
            } else {
                logger.error(`Missing schedule function in schedule configuration id: ${schedule.get('id')}`);
                callback(false);
            }
        })
        .done();
    }

    /**
     * Evaluates the alarm configuration of the schedule object
     * @param {Schedule} schedule - model for evaluating the alarm
     * @param {Subject} subject - The subject the alarm is associated with.
     * @param {boolean} cancelAlarms - parameter that states whether alarms should be canceled or not
     * @returns {Q.Promise<void>}
     * @example LF.Schedule.evaluateAlarmForSchedule(schedule,)
     */
    evaluateAlarmForSchedule (schedule, subject, cancelAlarms = true) {
        let completedQuestionnaires = new LastDiaries(),
            alarm = new Alarm(schedule),
            evaluate = (phaseInfo) => {
                let scheduleFunction = LF.Schedule.schedulingFunctions[schedule.get('scheduleFunction')];

                if (scheduleFunction) {
                    if (!phaseInfo || this.isScheduleInPhase(schedule, phaseInfo.phase)) {
                        alarm.evaluateAlarm(schedule, completedQuestionnaires, {});
                    }
                } else {
                    logger.error(`Missing schedule function in schedule configuration id: ${schedule.get('id')}`);
                }
            };

        cancelAlarms && alarm.cancelAlarm();


        return completedQuestionnaires.fetch()
        .then(() => this.getPhaseInformation(subject))
        .then(evaluate);
    }

    /**
     * Resets the Active Alarms table for all schedules that have current users with the assigned roles
     * @param {Collection} completedQuestionnaires a collection of LastDiaries to check against
     * @example LF.Schedule.alarmRefresh(completedQuestionnaires)
     */
    alarmRefresh (completedQuestionnaires) {
        let alarmSchedules,
            schedules = LF.StudyDesign.schedules.clone();

        Users.fetchCollection()
        .then((users) => {
            let currentRoles = _.invoke(users.models, 'get', 'role'),
                alarmFilter = schedule => !!schedule.get('alarmFunction'),
                roleFilter = (schedule) => {
                    let scheduleRoles = schedule.get('scheduleRoles') || ['subject'];

                    return _.intersection(scheduleRoles, currentRoles).length !== 0;
                };

            alarmSchedules = _.intersection(_.filter(schedules.models, roleFilter), _.filter(schedules.models, alarmFilter));
            localStorage.removeItem('alarmsReset');
            _.each(alarmSchedules, (schedule) => {
                let alarm = new Alarm(schedule);
                alarm.evaluateAlarm(schedule, completedQuestionnaires, LF.router.view);
            });
        });
    }

    /**
     * Checks if schedule is in phase passed as parameter
     * @param {LF.Model.Schedule} schedule schedule model for which we are checking.
     * @param {number} phase A phase number.
     * @returns {boolean}
     * @example LF.Schedule.isScheduleInPhase(schedule, phase)
     */
    isScheduleInPhase (schedule, phase) {
        let scheduleAvailablePhases,
            isInPhase = false;

        if (schedule.get('phase')) {
            // map phase array in this schedule with appropriate study phase numbers/values
            scheduleAvailablePhases = _(schedule.get('phase')).map((phase) => {
                return LF.StudyDesign.studyPhase[phase];
            });

            // check if current phase number is one of the phases in this schedule
            isInPhase = _(scheduleAvailablePhases).find((phaseNumber) => {
                return phase === phaseNumber;
            });
        } else {
            // if no phase is configured than it's available in every phase
            isInPhase = true;
        }

        return isInPhase;
    }

    /**
     * Saves the current phase information. This method will change database record
     * @param {String} studyPhase string key of current phase.
     * @param {Object} subject Subject to be updated.
     * @param {Function} callback A callback function invoked upon.
     * @example LF.Schedule.setPhase(studyPhase, callbackFunction)
     */
    savePhase (studyPhase, subject, callback = $.noop) {
        let currentDate = new Date();

        if (studyPhase) {
            subject.save({
                phase: LF.StudyDesign.studyPhase[studyPhase],
                phaseStartDateTZOffset: DateTimeUtil.timeStamp(currentDate),
                phaseTriggered: 'true'
            })
            .then(callback);
        } else {
            callback();
        }
    }

    /**
     *  TODO: why not parse paseTriggered as boolean?
     *  TODO: why not parse phaseStartDateTZOffset as number?
     * @typedef {{phase:number, phaseStartDateTZOffset:string, phaseTriggered:string}} PhaseInfo
     * @typedef {{}} EmptyObject
     */

    /**
     * Gets the current phase information (number of phase, phaseStartDateTime
     * with offset and is phase triggered)
     * with phase information object.
     * params.phaseStartDateTZOffset, params.phaseTiggered.})
     * @param {Subject} subject - The subject to get the phase for.
     * @returns {Q.Promise<PhaseInfo|EmptyObject>}
     */
    getPhaseInformation (subject) {
        if (subject) {
            return {
                phase: subject.get('phase'),
                phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset'),
                phaseTriggered: subject.get('phaseTriggered')
            };
        }

        return {};
    }

    /**
     * Get the schedule list of diary schedules
     * @param {Array} schedules list of schedules
     * @return {Array} returns the list of the schedules of which target objectType is "questionnaire"
     */
    getDiarySchedules (schedules) {
        return _(schedules).filter((schedule) => {
            return schedule.get('target').objectType === 'questionnaire';
        });
    }

    /**
     * Get the schedule list of indicator schedules
     * @param {Array} schedules list of schedules
     * @return {Array} returns the list of the schedules of which target objectType is "indicator"
     */
    getIndicatorSchedules (schedules) {
        return _(schedules).filter((schedule) => {
            let scheduleRoles = schedule.get('scheduleRoles') || ['subject'],
                role = CurrentContext().role;

            return schedule.get('target').objectType === 'indicator' && _.contains(scheduleRoles, role);
        });
    }

    /**
     * Refresh the current view based on the scheduling.
     * Timer is checking each second for the actual
     * refresh.
     */
    startTimer () {
        let nowUtcTimestamp = new Date().getTime(),
            nextScheduleRefreshUtc = this.getNextScheduleRefresh();

        if (nowUtcTimestamp >= nextScheduleRefreshUtc) {
            logger.info(`Determine schedules. now >= utc ${nowUtcTimestamp} > ${nextScheduleRefreshUtc}`);
            this.determineSchedules(this.schedules, {
                subject: this.subject
            }, (availableSchedules) => {
                this.availableSchedules.set(availableSchedules);
            });
        } else {
            clearTimeout(this.timer);
            this.timer = setTimeout(_.bind(this.startTimer, this), 1000);
        }
    }

    /**
     * Stop the timer
     */
    stopTimer () {
        clearTimeout(this.timer);
    }
}

window.LF.Class.ScheduleManager = ScheduleManager;
