import * as lStorage from 'core/lStorage';

const store = 'EULAAccepted';

/**
 * Manage the End User License Agreement's (EULA) acceptance.
 * @class EULA
 */
export default class EULA {
    /**
     * Determine if the EULA has been accepted.
     * @returns {boolean} true if accepted, false if not.
     */
    static isAccepted () {
        return !!lStorage.getItem(store);
    }

    /**
     * Set the EULA to accepted.
     */
    static accept () {
        lStorage.setItem(store, true);
    }

    /**
     * Withdrawl the EULA's acceptance.
     */
    static decline () {
        lStorage.removeItem(store);
    }
}
