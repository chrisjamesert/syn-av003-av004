export default class BackboneRouteControl extends Backbone.Router {
    constructor (options = {}) {
        if (options.controllers) {
            this.controllers = options.controllers;
        }

        Backbone.Router.prototype.constructor.call(this, options);
    }

    getRouteMethodAndController (routeAction) {
        let routeParts = routeAction.split('#');

        // If the route is correctly formatted.
        if (routeParts.length === 2) {
            let controllerName = routeParts[0];
            let controller = this.controllers[controllerName];
            let methodName = routeParts[1];
            let method = controller[methodName];

            return [method, controller];
        }

        return [];
    }

    _bindRoutes () {
        if (!this.routes) {
            return;
        }

        this.routes = _.result(this, 'routes');

        let route,
            routes = _.keys(this.routes);

        // eslint-disable-next-line
        while ((route = routes.pop()) != null) {
            let routeAction = this.routes[route];
            let [method, controller] = this.getRouteMethodAndController(routeAction);

            if (method) {
                this.route(route, routeAction, _.bind(method, controller));
            } else {
                this.route(route, routeAction);
            }
        }
    }
}
