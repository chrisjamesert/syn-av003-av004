/**
 * Client for handling Amazon Web Services' S3 actions,
 * particularly session management and file listing and access.
 */
class AWSS3Client {
    /**
     * Construct the AWS S3 client.
     * @param {string} accessKeyID access key for the AWS requests.
     * @param {string} secretAccessKey secret access key for AWS requests
     * @param {string} region region for AWS requests
     * @param {string} bucket S3 bucket to use for file requests.
     * @param {Object} [extraConstructorParams] any extra params to be passed to the constructor.
     *  http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#constructor-property
     */
    constructor (accessKeyID, secretAccessKey, region, bucket, extraConstructorParams = {}) {
        let awsCredentials = new AWS.Credentials(accessKeyID, secretAccessKey),
            params = _.extend({},
                {
                    apiVersion: '2006-03-01',
                    signatureVersion: 'v4',
                    credentials: awsCredentials,
                    region,
                    maxRetries: 0,
                    maxRedirects: 0
                },
                extraConstructorParams
            );
        this._s3 = new AWS.S3(params);
        this._bucket = bucket;
    }

    /**
     * Get listings for all files in this directory (recursive).
     * @param {string} [prefix=''] prefix to use
     * @param {boolean} [excludeFolders=true] whether or not to exclude directory listings from resuling array
     * @returns {Q.Promise<Array<Object>>} Promise resolving with an array of file listing.  Format is AWS return format.
     *  callback.data.Contents here:  http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#listObjectsV2-property
     *  Nodes of importance are Key, LastModified, ETag, Size, StorageClass, Owner.
     */
    getFileListByPrefix (prefix = '', excludeFolders = true) {
        return Q.Promise((resolve, reject) => {
            let params = {
                    Bucket: this._bucket,
                    Prefix: prefix
                },
                allData = [],
                objectListCallback = (err, data) => {
                    data && (allData = allData.concat(data.Contents));
                    if (err) {
                        reject(_.extend({}, err, { location: 'getFileList' }));
                        return;
                    }

                    if (data && data.IsTruncated) {
                        this._s3.listObjectsV2(_.extend({}, params, { ContinuationToken: data.NextContinuationToken }), objectListCallback);
                    } else {
                        resolve(allData);
                    }
                };

            // eslint-disable-next-line no-unused-vars
            this._s3.listObjectsV2(params, objectListCallback);
        })
        .then((data) => {
            let fileList = [];
            _.each(data, (val) => {
                if ((val.Key.lastIndexOf('/') !== val.Key.length - 1) || !excludeFolders) {
                    fileList.push(val);
                }
            });
            return fileList;
        });
    }

    /**
     * Get contents for a particular file.
     * @param {string} key file path (key) of file to retrieve.
     * @returns {Q.Promise<Object>} promise resolving with data object in AWS format
     *  (callback in http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#getObject-property)
     *  Contains the following nodes of importance:  Body, ContentType, LastModified, Metadata
     */
    getFileContents (key) {
        return Q.Promise((resolve, reject) => {
            let params = {
                Bucket: this._bucket,
                Key: key
            };
            this._s3.getObject(params, (err, data) => {
                if (err) {
                    reject(_.extend({}, err, { location: 'getFileContents' }));
                    return;
                }
                resolve(data);
            });
        });
    }

    /**
     * Get URL for performing a getObject() call on a file.
     * @param {string} key file path (key) of file to retrieve.
     * @returns {Q.Promise<string>} promise resolving with the signed URL for the download function.
     */
    getDownloadURL (key) {
        return Q.Promise((resolve, reject) => {
            let params = {
                Bucket: this._bucket,
                Key: key
            };
            return this._s3.getSignedUrl('getObject', params, (err, url) => {
                if (err) {
                    reject(_.extend({}, err, { location: 'getDownloadURL' }));
                    return;
                }
                resolve(url);
            });
        });
    }
}

export default AWSS3Client;
