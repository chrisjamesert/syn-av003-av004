import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';

let logger = new Logger('Core State');

/*
 * @class State
 * The class for state management
 */
export default class State {
    /**
     * The states for the core
     * @property {Object} states
     * @property {string} states.new - The default state for a new device
     * @readonly
     * @static
     */
    static get states () {
        return { new: 'NEW' };
    }

    /**
     * The initial state for the core
     * @property {string} initialState
     * @readonly
     * @static
     */
    static get initialState () {
        return this.states.new;
    }

    /**
     * Returns whether the state is valid or not
     * @param {string} stateValue - The state to be validated
     * @returns {boolean}
     * @static
     */
    static isValid (stateValue) {
        return !!_.find(_.keys(this.states), key => this.states[key] === stateValue);
    }

    /**
     * Tries to convert the invalid state value becasue it could be an upgrade scenario from 2.3.2
     * @param {Object} state - The unexpected state value to be handled. Could be any type.
     * @returns {null} the converted state value or null if the conversion fails
     */
    static handleInvalidStateValue (state) {
        // The implementation of the state conversion is expected to be handled in the extended classes.
        // If the code happens to fall in this method we'll just log a fatal error and return null.
        logger.fatal(`The State value in the localstorage is not valid and can not be converted: ${state}`);
        return null;
    }

    /**
     * Returns the current state
     * @returns {(string|null)}
     * @static
     */
    static get () {
        let state = lStorage.getItem('state');

        // If there is no state, that means this is a first time use...
        if (state == null) {
            this.set(this.initialState);
            return this.initialState;
        }

        if (!this.isValid(state)) {
            return this.handleInvalidStateValue(state);
        }

        return state;
    }

    /**
     * Sets the current state
     * @param {string} state - The state to be set as current
     * @static
     */
    static set (state) {
        if (!this.isValid(state)) {
            logger.fatal(`Given state is not valid: ${state}`);
            return;
        }

        lStorage.setItem('state', state);
    }

    /**
     * Returns whether the state passed in equals the current state
     * @param {string} state - The state to be compared to current state
     * @returns {boolean}
     * @static
     */
    static isCurrentState (state) {
        return this.get() === state;
    }
}
