module.exports = {

    /**
     * Get the user's current time zone
     * @param {Function} onSuccess Invoked on successful get current time zone, containing a result in a callback value
     * @param {Function} onError Invoked if there is an error.
     */
    getCurrentTimeZone: function (onSuccess, onError) {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:12345/getCurrentTimeZone',
            cache: false,
            success: (res, status) => {
                onSuccess(JSON.parse(res));
            },
            error: (status) => {
                onError(status);
            }
        });
    },

    /**
     * Get the list of system installed time zones
     *
     * @param {Function} onSuccess Invoked on successful get, containing a result in a callback value
     * @param {Function} onError Invoked if there is an error.
     */
    getAvailableTimeZones: function (onSuccess, onError) {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:12345/getTimeZoneList',
            data: { time: new Date().getTime() },
            cache: false,
            success: (res, status) => {
                onSuccess(JSON.parse(res));
            },
            error: (status) => {
                onError(status);
            }
        });
    },

    /**
     * Get the list of system installed time zone IDs
     * TODO: Find the corresponding functionality on Windows 10
     * @param {Function} onSuccess Invoked on successful get, containing a result in a callback value
     * @param {Function} onError Invoked if there is an error.
     */
    getAutomaticTimezone: function (onSuccess, onError) {
        onError('Not yet developed on Windows platform');
    },

    /**
     * Set the OS time zone
     *
     * @param {Function} onSuccess Invoked on successful set.
     * @param {Function} onError Invoked if there is an error.
     * @param {string}   tzId Time zone ID to set.
     */
    setTimeZone: function (onSuccess, onError, tzId) {
        $.ajax({
            type: 'POST',
            url: 'http://localhost:12345/setTimeZone',
            data: tzId,
            success: (res, status) => {
                onSuccess(res);
            },
            error: (status) => {
                onError(status);
            }
        });
    },

    /**
     * Get the options for timezone
     * No need for this on Windows platform
     * @param {Function} onSuccess Invoked on successful set.
     * @param {Function} onError Invoked if there is an error.
     */
    getOptions: function (onSuccess, onError) {
        onSuccess();
    },

    /**
     * Restart the application if Android version less than 5
     * No need for this on Windows platform
     * @param {Function} onSuccess Invoked on successful execution
     * @param {Function} onError Invoked if there is an error.
     */
    restartApplication: function (onSuccess, onError) {
        onSuccess();
    }
}; // exports

require("cordova/exec/proxy").add("TimeZoneUtil", module.exports);
