#import <Cordova/CDV.h>

@interface TimeZoneUtil : CDVPlugin

- (void) getCurrentTimeZone:(CDVInvokedUrlCommand *)command;

- (void) getTimeZones:(CDVInvokedUrlCommand *)command;

- (void) getAutomaticTimezone:(CDVInvokedUrlCommand *)command;

- (void) setTimeZone:(CDVInvokedUrlCommand *)command;

- (void) getOptions:(CDVInvokedUrlCommand *)command;

- (void) restartApplication:(CDVInvokedUrlCommand *)command;

@end
