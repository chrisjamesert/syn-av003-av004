#import "TimeZoneUtil.h"

@implementation TimeZoneUtil
/**
 * Get the current time zone.
 */
- (void) getCurrentTimeZone:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = nil;
    NSTimeZone* currentTZ = [NSTimeZone localTimeZone];

    // The current timezone's name. e.g. America/New_York
    NSString* currentTZName = currentTZ.name;

    // Format the time zone offset. e.g. '-04:00'
    NSDateFormatter* localTimeZoneFormatter = [NSDateFormatter new];
    localTimeZoneFormatter.timeZone = [NSTimeZone localTimeZone];
    localTimeZoneFormatter.dateFormat = @"Z";
    NSMutableString *localTimeZoneOffset = [NSMutableString stringWithString:[localTimeZoneFormatter stringFromDate:[NSDate date]]];
    [localTimeZoneOffset insertString:@":" atIndex:3];

    // Construct the time zone object to return.
    // e.g. { id: 'America/New_York', displayName: '(GMT-04:00) America/New_York' }
    NSDictionary* tz = @{
        @"id": currentTZName,
        @"displayName": [NSString stringWithFormat:@"(GMT%@) %@", localTimeZoneOffset, currentTZName]
    };

    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:tz];

    [self.commandDelegate sendPluginResult:result callbackId:[command callbackId]];
}

/**
 * Get a list of all available time zones on the device.
 */
- (void) getTimeZones:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = nil;
    NSMutableArray* payload = [NSMutableArray new];

    // Loop through all known time zones on the device.
    for (NSString *name in [NSTimeZone knownTimeZoneNames]) {

        // Format the time zone offset. e.g. '-04:00'
        NSDateFormatter* localTimeZoneFormatter = [NSDateFormatter new];
        localTimeZoneFormatter.timeZone = [NSTimeZone timeZoneWithName:name];
        localTimeZoneFormatter.dateFormat = @"Z";
        NSMutableString *localTimeZoneOffset = [NSMutableString stringWithString:[localTimeZoneFormatter stringFromDate:[NSDate date]]];
        [localTimeZoneOffset insertString:@":" atIndex:3];
        // Generate the offset of the time zone in ms.
        NSInteger *offset = [[NSTimeZone timeZoneWithName:name] secondsFromGMT] * 1000;

        // Construct the time zone object to append to our result array.
        // e.g. { id: 'America/New_York', displayName: '(GMT-04:00) America/New_York', offset: -1440000 }
        NSDictionary *tzEntry = @{
            @"id": name,
            @"displayName": [NSString stringWithFormat:@"(GMT%@) %@", localTimeZoneOffset, name],
            @"offset": [NSString stringWithFormat:@"%zd", offset]   
        };

        [payload addObject:tzEntry];
    }

    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:payload];

    [self.commandDelegate sendPluginResult:result callbackId:[command callbackId]];
}

// Not supported for iOS.
- (void) getAutomaticTimezone:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = nil;
    NSString* payload = nil;

    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:payload];

    [self.commandDelegate sendPluginResult:result callbackId:[command callbackId]];
}

// Not supported for iOS.
// You cannot change system settings from an iOS application.
- (void) setTimeZone:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = nil;
    NSString* payload = nil;

    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:payload];

    [self.commandDelegate sendPluginResult:result callbackId:[command callbackId]];
}

// Not supported for iOS.
- (void) getOptions:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = nil;
    NSString* payload = nil;

    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:payload];

    [self.commandDelegate sendPluginResult:result callbackId:[command callbackId]];
}

// Not supported for iOS.
- (void) restartApplication:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = nil;
    NSString* payload = nil;

    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:payload];

    [self.commandDelegate sendPluginResult:result callbackId:[command callbackId]];
}
@end
