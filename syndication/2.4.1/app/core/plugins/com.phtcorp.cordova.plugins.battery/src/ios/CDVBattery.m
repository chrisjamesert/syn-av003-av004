/*

 <ERTMOD>

 NOTE: This is a very hacked up version of the existing Cordova battery plugin
 to match the conventions used by the existing custom Android battery plugin.

 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#import "CDVBattery.h"

@interface CDVBattery (PrivateMethods)
- (void)updateOnlineStatus;
@end

@implementation CDVBattery

@synthesize state, level, callbackId, isPlugged;

/* Get the current battery status and level.  Status will be unknown and level will be -1.0 if
 * monitoring is turned off (also the case for running on an emulator).
 */
- (void)getBatteryInfo:(CDVInvokedUrlCommand*)command
{
    if ([UIDevice currentDevice].batteryMonitoringEnabled == NO) {
        [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    }
    
    UIDevice* currentDevice = [UIDevice currentDevice];
    UIDeviceBatteryState currentState = [currentDevice batteryState];

    isPlugged = FALSE; // UIDeviceBatteryStateUnknown or UIDeviceBatteryStateUnplugged
    if ((currentState == UIDeviceBatteryStateCharging) || (currentState == UIDeviceBatteryStateFull)) {
        isPlugged = TRUE;
    }
    float currentLevel = [currentDevice batteryLevel];

    if ((currentLevel != self.level) || (currentState != self.state)) {
        self.level = currentLevel;
        self.state = currentState;
    }

    // W3C spec says level must be null if it is unknown
    NSObject* w3cLevel = nil;
    if ((currentState == UIDeviceBatteryStateUnknown) || (currentLevel == -1.0)) {
        w3cLevel = [NSNull null];
    } else {
        w3cLevel = [NSNumber numberWithFloat:(currentLevel * 100)];
    }
    NSMutableDictionary* batteryData = [NSMutableDictionary dictionaryWithCapacity:2];
    [batteryData setObject:[NSNumber numberWithBool:isPlugged] forKey:@"isPlugged"];
    [batteryData setObject:w3cLevel forKey:@"batteryLevel"];

    CDVPluginResult* pluginResult = nil;
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:batteryData];
    [self writeJavascript:[pluginResult toSuccessCallbackString:command.callbackId]];
    
}

/* turn off battery monitoring */
- (void)stop:(CDVInvokedUrlCommand*)command
{
    self.callbackId = nil;
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryLevelDidChangeNotification object:nil];
}

- (void)pluginInitialize
{
    self.state = UIDeviceBatteryStateUnknown;
    self.level = -1.0;
}

- (void)dealloc
{
    [self stop:nil];
}

- (void)onReset
{
    [self stop:nil];
}

@end