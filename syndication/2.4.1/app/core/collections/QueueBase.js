import StorageBase from './StorageBase';
import Logger from 'core/Logger';

const logger = new Logger('QueueBase');

/**
 * A class that creates a collection of QueueBase.
 * @class QueueBase
 * @extends StorageBase
 * @example var collection = new QueueBase();
 */
export default class QueueBase extends StorageBase {
    /**
     * Pull the transmission queue from the database.
     * @param {function} onSuccess - Callback function invoked upon a successful transaction.
     * @returns {Q.Promise}
     */
    pullQueue (onSuccess = $.noop) {
        return this.fetch()
        .then(onSuccess);
    }

    /**
     * Executes all action items until none are left.
     * @param {Function} [callback] - Callback function invoked upon completion.
     * @returns {Q.Promise}
     */
    executeAll (callback = $.noop) {
        let errorStack = [],
            handleError = (err) => {
                // If there is an error we can't stop the execution. Instead push it to the error stack
                errorStack.push(err);
            },
            step = () => {
                if (!this.size()) {
                    // Fire an ELF trigger if there were any errors during execution
                    if (errorStack.length) {
                        return ELF.trigger('EXECUTEALL:Failed', { errorStack }, this)
                        .then((flags) => {
                            // The default behavior of executeAll method is to resolve even if there is a failed execution
                            // The failed executions are expected to be handled through the ELF rules.
                            // If the preventDefault flag is true then the promise chain will be broken with a rejected promise.
                            if (flags && flags.preventDefault) {
                                logger.error('Some items had errors while running executeAll. Will return a rejected promise even if some of the items are successfully executed.');
                                callback({ errorStack });
                                return Q.reject({ errorStack });
                            }

                            callback();

                            return Q();
                        });
                    }

                    callback();

                    return Q();
                }

                return this.execute(0)
                .catch(handleError)
                .then(step);
            };

        return step();
    }

    /**
     * Remove an item from the queue.
     * @param {string} id - The id of the item to remove.
     * @param {function} onSuccess - Callback function invoked upon completion of the transaction.
     * @param {function} [onError] - A callback invoked upon errant action.
     * @throws {Object} If id or queue items are not found.
     * @returns {Q.Promise}
     */
    destroy (id, onSuccess = $.noop, onError = $.noop) {
        let item,
            deferred = Q.defer(),
            resolve = (res) => {
                onSuccess(res);
                deferred.resolve(res);
            },
            reject = (err) => {
                onError(err);
                deferred.reject(err);
            };

        // If no id has been specified.
        if (id == null) {
            throw new Error('Missing Argument: id is null or undefined.');
        }

        // Find the record in memory.
        item = this.get(parseInt(id, 10));

        if (item) {
            // Attempt to destroy the queue item.
            item.destroy()
            .then(resolve, reject);
        } else {
            throw new Error('Cannot destroy a nonexistent record.');
        }

        return deferred.promise;
    }
}

window.LF.Collection.QueueBase = QueueBase;
