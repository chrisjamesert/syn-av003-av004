import BaseCollection from './BaseCollection';
import Widget from '../models/Widget';

/**
 * A class that creates a collection of Widgets.
 * @class Widgets
 * @extends BaseCollection
 * @example let collection = new Widgets();
 */
export default class Widgets extends BaseCollection {
    /**
     * @property {Widget} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Widget}'
     */
    get model () {
        return Widget;
    }
}

window.LF.Collection.Widgets = Widgets;
