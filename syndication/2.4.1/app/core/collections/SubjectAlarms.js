import StorageBase from './StorageBase';
import ScheduleManager from '../classes/ScheduleManager';
import SubjectAlarm from '../models/SubjectAlarm';
import Logger from 'core/Logger';

const logger = new Logger('SubjectAlarms');

/**
 * A class that creates a collection of SubjectAlarms.
 * @class SubjectAlarms
 * @extends StorageBase
 * @example let collection = new SubjectAlarms();
 */
export default class SubjectAlarms extends StorageBase {
    /**
     * @property {SubjectAlarm} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link SubjectAlarm}'
     */
    get model () {
        return SubjectAlarm;
    }

    /**
     * Sets the subject configurable alarm within collection
     * @param {Object} params Alarm parameters including id, time, scheduleId and isActive
     * @param {Function} [callback] Callback function to be invoked when subject alarm is added/updated
     * @returns {Q.Promise}
     */
    setSubjectAlarm (params, callback = $.noop) {
        if (params.id == null) {
            throw new Error('Missing Argument: alarm id is null or undefined.');
        } else if (params.time == null) {
            throw new Error('Missing Argument: alarm time is null or undefined.');
        } else if (params.schedule == null) {
            throw new Error('Missing Argument: schedule id is null or undefined.');
        }

        return this.fetch()
        .then(() => {
            let subjectAlarm = new SubjectAlarm(),
                scheduleId = params.schedule.get('id');

            return subjectAlarm.save({
                id: params.id,
                schedule_id: scheduleId,
                time: params.time
            })
            .then(() => {
                let alarm = params.time === '' ? 'Off' : params.time,
                    schedule = new ScheduleManager();

                logger.info(`Alarm time changed to: ${alarm} for schedule: ${scheduleId}`);

                return schedule.evaluateAlarmForSchedule(params.schedule);
            });
        })
        .catch((err) => {
            err && logger.error('Error setting subject alarm', err);
        })
        .then(() => callback());
    }

    /**
     * Remove subject alarm
     * @param {String} scheduleId unique schedule id of the associated initial subject alarm
     * @param {Function} [callback] Callback function to be invoked when alarm is canceled
     * @returns {Q.Promise}
     */
    removeSubjectAlarm (scheduleId, callback = $.noop) {
        if (scheduleId == null) {
            throw new Error('Missing Argument: schedule id is null or undefined.');
        }

        return this.fetch()
        .then(() => {
            let item = this.match({
                schedule_id: scheduleId
            })[0];

            if (item) {
                return item.destroy()
                .finally(callback);
            }
            return callback();
        })
        .catch(() => callback());
    }
}

window.LF.Collection.SubjectAlarms = SubjectAlarms;
