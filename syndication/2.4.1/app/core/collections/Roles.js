import BaseCollection from './BaseCollection';
import Role from 'core/models/Role';
import { getNested, getProductName } from 'core/utilities';

/**
 * A collection of Roles
 * @class Roles
 */
export default class Roles extends BaseCollection {
    /**
     * The collection's associated model.
     * @readonly
     * @default '{@link Role}'
     */
    get model () {
        return Role;
    }

    /**
     * Get the administrator role from the collection.
     * @returns {Role}
     */
    getAdminRole () {
        let id = getNested('StudyDesign.adminUser.role', LF);

        return this.findWhere({ id });
    }

    /**
     * Get a list of roles available on the product.
     * @returns {Array<Role>}
     */
    getProductRoles () {
        return this.reject((role) => {
            return _.indexOf(role.get('product'), getProductName()) === -1;
        });
    }

    /**
     * Get an array of defined syncLevels.
     * @returns {Array}
     */
    getSyncLevels () {
        return _.chain(this.reduce((result, value) => {
            let product = value.get('product');
            if (product && product.indexOf(getProductName()) > -1) {
                result.push(value.get('syncLevel'));
            }
            return result;
        }, [])).compact().uniq().value();
    }
}

window.LF.Collection.Roles = Roles;
