import StorageBase from './StorageBase';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';

// eslint-disable-next-line no-unused-vars
let logger = new Logger('Subjects');


/**
 * A class that creates a collection of Subjects.
 * @class Subjects
 * @extends {StorageBase<Subject>}
 * @example let collection = new Subjects();
 */
export default class Subjects extends StorageBase {
    /**
     * @property {Subject} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Subject}'
     */
    get model () {
        return Subject;
    }

    /**
     * Fetches the subject matching the passed criteria
     * @param {Object} criteria The default criteria to match the subject to be returned with Promise
     * @returns {Q.Promise<Subject>}
     * @example Subjects.getSubjectBy({ krpt: 'NP.2d5f1486e843498fb3ca4ba29dd4eee8' });
     */
    static getSubjectBy (criteria) {
        let subjects = new this();

        return subjects.fetch({ search: { where: criteria } })
        .then(() => subjects.at(0));
    }
}

window.LF.Collection.Subjects = Subjects;
