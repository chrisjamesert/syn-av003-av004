import BaseCollection from 'core/collections/BaseCollection';
import SecurityQuestion from 'core/models/SecurityQuestion';

export default class SecurityQuestions extends BaseCollection {
    get model () {
        return SecurityQuestion;
    }
}
