import BaseCollection from './BaseCollection';
import Environment from '../models/Environment';

/**
 * A collection of environment configurations.
 * @class Environments
 * @extends BaseCollection
 */
export default class Environments extends BaseCollection {
    /**
     * @property {Environment} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Environment}'
     */
    get model () {
        return Environment;
    }
}

window.LF.Collection.Environments = Environments;
