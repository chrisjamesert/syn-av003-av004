import BaseCollection from './BaseCollection';
import Question from '../models/Question';

/**
 * A class that creates a collection of Questions.
 * @class Questions
 * @extends BaseCollection
 * @example let collection = new Questions();
 */
export default class Questions extends BaseCollection {
    /**
     * @property {Question} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Question}'
     */
    get model () {
        return Question;
    }
}

window.LF.Collection.Questions = Questions;
