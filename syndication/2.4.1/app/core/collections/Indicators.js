import BaseCollection from './BaseCollection';
import Indicator from '../models/Indicator';

/**
 * A class that creates a collection of Indicators.
 * @class Indicators
 * @extends BaseCollection
 * @example let collection = new Indicators();
 */
export default class Indicators extends BaseCollection {
    /**
     * @property {Indicator} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Indicator}'
     */
    get model () {
        return Indicator;
    }
}

window.LF.Collection.Indicators = Indicators;
