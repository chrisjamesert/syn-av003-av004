import StorageBase from './StorageBase';
import Study from '../models/Study';

/**
 * A class that creates a collection of Studies.
 * @class Studies
 * @extends StorageBase
 * @example let collection = new Studies();
 */
export default class Studies extends StorageBase {
    /**
     * @property {Study} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Study}'
     */
    get model () {
        return Study;
    }
}

window.LF.Collection.Studies = Studies;
