import BaseCollection from './BaseCollection';
import Template from 'core/models/Template';
import Logger from 'core/Logger';

let logger = new Logger('collections/templates');

/**
 * A class that creates a collection of Templates.
 * @class Templates
 * @extends BaseCollection
 * @example let collection = new Templates();
 */
export default class Templates extends BaseCollection {
    /**
     * @property {Template} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Template}'
     */
    get model () {
        return Template;
    }

    /**
     * Get a template resource from a key (template factory from model)
     * @param {string} key - The key of the resource to find.
     * @returns {Function} A template function.
     */
    getTemplateFromKey (key) {
        let model,
            name,
            namespace,
            language,
            locale,
            params,
            template;

        if (key == null) {
            let str = 'Missing argument: key is null or undefined';
            logger.error(str);
            throw new Error(str);
        } else if (typeof key !== 'string') {
            let str = `Invalid DataType: key.  Expected string, received ${typeof key}.`;
            logger.error(str);
            throw new Error(str);
        }

        params = key.split(':');

        if (params.length === 1) {
            name = params[0];
        } else {
            namespace = params[0];
            name = params[1];
        }

        if (namespace !== 'DEFAULT') {
            language = LF.Preferred.language;
            locale = LF.Preferred.locale;

            model = this.match({
                name,
                namespace,
                language,
                locale
            });

            if (!model) {
                model = this.match({
                    name,
                    namespace,
                    language,
                    locale: undefined
                });
            }

            if (!model) {
                model = this.match({
                    name,
                    namespace,
                    language: undefined,
                    locale: undefined
                });
            }
        }

        if (!model) {
            model = this.match({
                name,
                namespace: 'DEFAULT'
            }) || false;
        }

        if (model) {
            template = _.template(model.get('template'));

            return template;
        }
        let str = `No matching template found for "${key}".`;
        logger.error(str);
        throw new Error(str);
    }

    /**
     * Finds and returns the appropriate resource string.
     * @param {String} key The key of the resource to find.
     * @param {Object} [parameters] The values to pass into the template.
     * @throws {Object} If key argument is missing or if the template isn't found.
     * @returns {string}
     */
    // eslint-disable-next-line consistent-return
    display (key, parameters) {
        let template = this.getTemplateFromKey(key);
        if (template) {
            return template(parameters);
        }
    }

    /**
     * Returns matching model based on passed parameters.
     * @param {Object} params The attributes to match.
     * @returns {Object|Boolean} returns the matching model or false.
     * @throws {Object} If params are null or undefined.
     */
    // Return all matching models. If 0, return false.
    match (params) {
        let models,
            filter;

        if (params == null) {
            throw new Error('Missing argument: params is null or undefined');
        }

        filter = () => {
            // For each model in the collection.
            return _(this.models).filter((model) => {
                let matchCount = 0,
                    length = _.size(params);

                _(params).each((value, key) => {
                    if (model.get(key) === value) {
                        // If the field matches the value increase the match counter.
                        matchCount += 1;
                    }
                });

                // If the match counter and total param length match, return the model.
                return matchCount === length;
            });
        };

        models = filter(params);

        if (models.length > 0) {
            return models[0];
        }
        return false;
    }

    /**
     * Override to either get an object by ID or by namespace & name.
     * Used mainly for merging.
     * @param {(Backbone.Model|string|number)} obj - input to get function.  Either an ID or model
     * @returns {(Backbone.Model|string|number)}
     */
    get (obj) {
        let ret = super.get(obj);
        if (ret == null && obj && obj.get) {
            return this.find({ namespace: obj.get('namespace'), name: obj.get('name') });
        }
        return ret;
    }
}

window.LF.Collection.Templates = Templates;
