import QueueBase from './QueueBase';
import Log from '../models/Log';

/**
 * A class that creates a collection of Logs.
 * @class Logs
 * @extends QueueBase
 * @example let collection = new Logs();
 */
export default class Logs extends QueueBase {
    /**
     * @property {Log} model - The model that belongs to the collection
     * @readonly
     * @default '{@link Log}'
     */
    get model () {
        return Log;
    }
}

/**
 * Log collection for storing logs in memory prior to database save
 * @memberof LF
 * @type '{@link Logs}'
 */
window.LF.Collection.Logs = Logs;

