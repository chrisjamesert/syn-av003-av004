import StorageBase from './StorageBase';
import LastDiary from '../models/LastDiary';

/**
 * A class that creates a collection of LastDiaries.
 * @class LastDiaries
 * @extends StorageBase
 * @example var collection = new LastDiaries();
 */
export default class LastDiaries extends StorageBase {
    /**
     * @property {LastDiary} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link LastDiary}'
     */
    get model () {
        return LastDiary;
    }
}

window.LF.Collection.LastDiaries = LastDiaries;
