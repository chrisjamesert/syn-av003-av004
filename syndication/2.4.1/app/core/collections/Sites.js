import StorageBase from './StorageBase';
import Site from '../models/Site';

/**
 * A class that creates a collection of Sites.
 * @class Sites
 * @extends StorageBase
 * @example let collection = new Sites();
 */
export default class Sites extends StorageBase {
    /**
     * @property {Site} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Site}'
     */
    get model () {
        return Site;
    }
}

window.LF.Collection.Sites = Sites;
