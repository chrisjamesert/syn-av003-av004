/**
 * Get patient ID.
 */
export const getPatientIDForDuplicate = {
    id: 'getPatientIDForDuplicate',
    evaluate: () => {
        return LF.DynamicText.duplicateSubjectId;
    },
    screenshots: {
        values: ['1']
    }
};
