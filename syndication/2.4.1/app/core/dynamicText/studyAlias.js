const pinImg = '<img src="../css/images/studyNamePin.png" height=21px/>:';

/**
 * Get the pin icon if running in the wrapper, or the STUDY_URL string if not
 */
export const studyAlias = {
    id: 'studyAlias',
    evaluate: () => {
        const exec = Q.Promise(resolve => LF.Wrapper.exec({
            execWhenWrapped: () => resolve(true),
            execWhenNotWrapped: () => resolve(false)
        }));

        return exec()
        .then((wrapped) => {
            if (wrapped) {
                return pinImg;
            }
            return LF.getStrings('STUDY_URL');
        });
    },
    screenshots: {
        values: [pinImg],
        keys: {
            studyUrl: 'STUDY_URL'
        }
    }
};
