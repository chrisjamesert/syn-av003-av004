export const patientIDRange = {
    id: 'patientIDRange',
    evaluate: () => {
        return `${LF.StudyDesign.participantSettings.participantID.seed}  - ${LF.StudyDesign.participantSettings.participantID.max}`;
    },
    screenshots: {
        values: ['100 - 600']
    }
};
