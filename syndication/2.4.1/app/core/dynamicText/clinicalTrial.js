import { getCurrentProtocol } from 'core/Helpers';

export const clinicalTrial = {
    id: 'clinicalTrial',
    evaluate: () => {
        let currentProtocol = getCurrentProtocol();

        return `${LF.StudyDesign.clientName} ${currentProtocol}`;
    },
    screenshots: {
        values: ['clientName studyProtocol']
    }
};
