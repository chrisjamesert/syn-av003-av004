/**
 * Get the current localized date for the preferred language and locale.
 * @returns {Q.Promise<Object>} Resolves with the formatted date.
 */
function currentDate () {
    const dateOptions = _.extend({}, LF.strings.dates({ dates: {} }), LF.strings.dates({ dateConfigs: {} })),
        date = $('<input data-role="datebox" />'),
        dateLang = {
            lang: {
                default: dateOptions,
                isRTL: LF.strings.getLanguageDirection() === 'rtl'
            },
            mode: 'datebox'
        };

    date.datebox(dateLang);

    const getTheDate = date.datebox('getTheDate');
    const formattedDate = date.datebox('callFormat', dateOptions.dateFormat, getTheDate);

    return formattedDate;
}

export const currentDateText = {
    id: 'currentDate',
    evaluate: currentDate,
    screenshots: {
        values: ['1/1/01']
    }
};
