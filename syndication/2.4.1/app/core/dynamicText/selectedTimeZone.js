/**
 * Get the name of selected time zone.
 */
export const selectedTimeZone = {
    id: 'selectedTimeZone',
    evaluate: () => {
        return LF.DynamicText.selectedTZName ? LF.DynamicText.selectedTZName.trim() : LF.DynamicText.selectedTZName;
    },
    screenshots: {
        values: ['Africa/Abidjan', 'America/New_York']
    }
};
