import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import ConfirmView from 'core/views/ConfirmView';

export default function setupSitepadDialogs () {
    MessageRepo.add({
        type: 'Dialog',
        key: 'ERROR_PATIENT_SAVE',
        message: MessageHelpers.notifyDialogCreator({
            message: 'ERROR_PATIENT_SAVE'
        })
    }, {
        type: 'Dialog',
        key: 'INVALID_PERMISSION',
        message: MessageHelpers.notifyDialogCreator({
            message: 'INVALID_PERMISSION',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'CANCEL_PERMANENT_PASSWORD',
        message: () => {
            return LF.getStrings({
                header: 'CANCEL_PERMANENT_PASSWORD_TITLE',
                body: 'CANCEL_PERMANENT_PASSWORD_TEXT',
                cancel: 'NO',
                ok: 'YES'
            })
            .then((strings) => {
                const modal = new ConfirmView();

                return modal.show(strings);
            });
        }
    }, {
        type: 'Dialog',
        key: 'SUBJECT_ALREADY_EXISTS_ERROR',
        message: MessageHelpers.notifyDialogCreator({
            message: 'SUBJECT_ALREADY_EXISTS',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'DIARY_TIMEOUT',
        message: MessageHelpers.notifyDialogCreator({
            message: 'DIARY_TIMEOUT'
        })
    }, {
        type: 'Dialog',
        key: 'EDIT_USER_NO_CHANGES',
        message: MessageHelpers.confirmDialogCreator({
            header: 'EDIT_USER_NO_CHANGES_HEADER',
            message: 'EDIT_USER_NO_CHANGES_BODY',
            ok: 'YES',
            cancel: 'NO',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'SUBJECT_DELETED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PATIENT_DELETED',
            message: 'PATIENT_DELETED_MSG'
        })
    }, {
        type: 'Dialog',
        key: 'SUBJECT_DELETED_PATIENT',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PATIENT_DELETED',
            message: 'PATIENT_DELETED_SUBJECT_MSG'
        })
    }, {
        type: 'Dialog',
        key: 'SETUP_COMPLETE',
        message: MessageHelpers.notifyDialogCreator({
            header: 'SETUP_COMPLETE',
            message: 'SETUP_COMPLETE_DESC',
            type: 'success'
        })
    }, {
        type: 'Dialog',
        key: 'CONFIRM_SKIP_VISIT',
        message: MessageHelpers.confirmDialogCreator({
            header: 'SKIP_VISIT_TITLE',
            message: 'SKIP_VISIT_TEXT',
            ok: 'YES',
            cancel: 'NO',
            type: 'warning',
            styleClass: 'skipVisit'
        })
    }, {
        type: 'Dialog',
        key: 'VISIT_INACTIVE',
        message: MessageHelpers.notifyDialogCreator({
            header: 'VISIT_INACTIVE_HEADER',
            message: 'VISIT_INACTIVE_MESSAGE',
            ok: 'OK'
        })
    }, {
        type: 'Dialog',
        key: 'VISIT_INACTIVE_SUBJECT',
        message: MessageHelpers.notifyDialogCreator({
            header: 'VISIT_INACTIVE_HEADER',
            message: 'VISIT_INACTIVE_MESSAGE_SUBJECT',
            ok: 'OK'
        })
    }, {
        type: 'Dialog',
        key: 'VISIT_COMPLETED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'ALL_QUESTIONNAIRES_COMPLETED',
            message: 'VISIT_COMPLETED_MESSAGE'
        })
    }, {
        type: 'Dialog',
        key: 'VISIT_COMPLETED_SUBJECT',
        message: MessageHelpers.notifyDialogCreator({
            header: 'ALL_QUESTIONNAIRES_COMPLETED',
            message: 'REPORTS_COMPLETED_MESSAGE'
        })
    }, {
        type: 'Dialog',
        key: 'VISIT_EXPIRED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'VISIT_EXPIRED_HEADER',
            message: 'VISIT_EXPIRED_MESSAGE'
        })
    }, {
        type: 'Dialog',
        key: 'VISIT_EXPIRED_SUBJECT',
        message: MessageHelpers.notifyDialogCreator({
            header: 'VISIT_EXPIRED_HEADER',
            message: 'VISIT_EXPIRED_MESSAGE_SUBJECT'
        })
    }, {
        type: 'Dialog',
        key: 'NO_INTERNET_FIRST_USER',
        message: MessageHelpers.confirmDialogCreator({
            header: 'NO_INTERNET_CONNECTION',
            message: 'TRY_AGAIN_ADD_USER',
            ok: 'RETRY',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'TRANSMIT_FAILED_403_14',
        message: MessageHelpers.notifyDialogCreator({
            header: 'ERROR_TITLE',
            message: 'TRANSMIT_FAILED_403_14',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'TRANSMIT_FAILED_404',
        message: MessageHelpers.notifyDialogCreator({
            header: 'ERROR_TITLE',
            message: 'TRANSMIT_FAILED_404',
            type: 'error'
        })
    });

    const namespaces = ['New_Patient', 'Edit_Patient', 'Skip_Visits', 'Edit_User', 'Deactivate_User', 'Activate_User'];

    _.each(namespaces, (namespace) => {
        const messageId = `BASE_QUESTIONNAIREVIEW_${namespace}`;

        const message = ({ cancelPopupStyle }) => {
            // Will currently take screenshots without anything passed for cancelPopupStyle.
            //   break up possible styles with seperate keys to take screenshots.
            let ns = namespace.toUpperCase();

            const notifyStrings = {
                header: {
                    key: `${ns}_CANCEL_CONFIRM`,
                    namespace
                },
                body: {
                    key: `${ns}_CANCEL_BODY`,
                    namespace
                },
                cancel: {
                    key: 'NO',
                    namespace
                },
                ok: {
                    key: 'YES',
                    namespace
                }
            };

            return LF.getStrings(notifyStrings)
            .then((strings) => {
                let modal = new ConfirmView();

                if (cancelPopupStyle) {
                    strings.styleClass = cancelPopupStyle;
                }

                strings.type = 'error';

                return modal.show(strings);
            });
        };

        MessageRepo.add({
            type: 'Dialog',
            key: messageId,
            message
        });
    });

    const editPatientNamespaces = ['Edit_Patient'];
    _.each(editPatientNamespaces, (namespace) => {
        const messageId = `${namespace}_NO_CHANGES`;

        const message = () => {
            return LF.getStrings({
                header: {
                    key: 'EDIT_PATIENT_NO_CHANGES_CONFIRM',
                    namespace
                },
                body: {
                    key: 'EDIT_PATIENT_NO_CHANGES_BODY',
                    namespace
                },
                cancel: {
                    key: 'NO',
                    namespace
                },
                ok: {
                    key: 'YES',
                    namespace
                }
            })
            .then((res) => {
                res.type = 'warning';

                let modal = new ConfirmView();

                return modal.show(res);
            });
        };

        MessageRepo.add({
            type: 'Dialog',
            key: messageId,
            message
        });
    });
}
