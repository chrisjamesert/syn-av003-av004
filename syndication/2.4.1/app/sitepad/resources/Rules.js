import Data from 'core/Data';
import Subjects from 'core/collections/Subjects';
import Dashboards from 'core/collections/Dashboards';
import Transmissions from 'core/collections/Transmissions';
import LastDiaries from 'core/collections/LastDiaries';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import trial from 'sitepad/trialMerge'; //PDE UPDATE changed to make trial import modality specific
import * as lStorage from 'core/lStorage';
import * as utils from 'core/utilities';
import Transmission from 'core/models/Transmission';
import { setTime } from 'sitepad/actions/lastRefreshTime';
import { MessageRepo } from 'core/Notify';
import notify from 'core/actions/notify';
import State from 'sitepad/classes/State';
import { duplicatPatientTransmissionsResolve } from 'sitepad/actions/duplicatPatientTransmissionsResolve';
import * as Utilities from 'core/utilities';
import COOL from 'core/COOL';
import EULA from 'core/classes/EULA';

let logger = new Logger('Rules');
const design = trial.assets.studyDesign;

trial.assets.coreRules(ELF.rules);

((rules) => {
    /*
     * Display a popup if the battery level is 20% or less
     */
    rules.add({
        id: 'BatteryLevelWarning',
        trigger: 'NAVIGATE:application/home',
        evaluate: (filter, resume) => {
            LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
                if (batteryLevel && batteryLevel <= 5) {
                    filter.key = 'CRITICAL_BATTERY';
                    resume(true);
                } else if (batteryLevel && batteryLevel <= 20) {
                    filter.key = 'LOW_BATTERY';
                    resume(true);
                } else {
                    resume(false);
                }
            });
        },
        resolve: [{
            action: (input) => {
                LF.Actions.notify({
                    key: input.key
                })
                .done();
            }
        }],
        reject: [{ action: 'defaultAction' }]
    });

    /*
     * WidgetValidation
     * If the diary entry contains a widget try to run the widgets validation function
     */
    rules.add({
        id: 'WidgetValidation',
        trigger: 'WIDGET:Validation',
        evaluate (input, done) {
            let validationObj = this.model.get('validation');

            if (this.isUserTextBox && validationObj.params) {
                validationObj.params.answers = this.questionnaire.data.answers;
            }

            done(!!LF.Widget.ValidationFunctions[validationObj.validationFunc]);
        },
        resolve: [{ action: 'widgetValidation' }],
        reject: [{ action: 'defaultAction' }]
    });

    /*
     * QuestionnaireScreenAuthenticationByRole
     * Checks to see if the user logged in has a role able to answer a quesion based on a role configured at the screen level
     * If the user does not have the role required at the screen level. A login window is displayed requiring a user with the level
     * configured at the screen level to login.
     */
    rules.add({
        id: 'QuestionnaireScreenAuthenticationByRole',
        trigger: 'QUESTIONNAIRE:Before',
        evaluate (filter, resume) {
            resume(!lStorage.getItem('isActivation'));
        },
        resolve: [{ action: 'questionnaireScreenAuthenticationByRole' }],
        reject: [{ action: 'defaultAction' }]
    });

    rules.add({
        id: 'UserChangeCredentials',
        trigger: 'USER:ChangeCredentials',
        resolve: [
            { action: 'displayMessage' },
            { action: filter => new Transmission().save(filter) },
            {
                action () {
                    return ELF.trigger('UserChangeCredentials:Transmit', {}, this);
                }
            },
            { action: 'updateCurrentContext' },
            { action: 'removeMessage' }
        ]
    });

    /*
     * AffidavitBackOut
     * If the subject navigates away from the Affidavit, reset its answer record.
     */
    rules.add({
        id: 'AffidavitBackOut',
        trigger: 'QUESTIONNAIRE:Navigate',
        evaluate: (input, done) => {
            done(input.screenId === 'AFFIDAVIT' && input.direction === 'previous');
        },
        resolve: [{
            action: 'resetAnswersSelected',
            data: ['AFFIDAVIT']
        }, {
            action: 'defaultAction'
        }],
        reject: [{ action: 'defaultAction' }]
    });

    /*
     * DuplicateSubjectHandleing
     * Marks duplicate patients created on device.
     */
    rules.add({
        id: 'DuplicateSubjectHandling',
        trigger: 'TRANSMIT:Duplicate/Subject',
        evaluate (filter, resume) {
            Q.all([Subjects.fetchCollection(), Transmissions.fetchCollection(), Dashboards.fetchCollection()])
            .spread((subjects, transmissions, dashboards) => {
                let duplicate = subjects.findWhere({ krpt: filter.krpt }),
                    matchingDashboards = dashboards.where({ krpt: filter.krpt });

                LF.DynamicText.duplicateSubjectId = duplicate.get('subject_id');

                if (matchingDashboards.length) {
                    let matchingIds = _.map(matchingDashboards, (dashboard) => {
                        return dashboard.get('id');
                    });

                    transmissions.forEach((transmission) => {
                        if (transmission) {
                            let parsedParams = JSON.parse(transmission.get('params'));
                            if (_.indexOf(matchingIds, parsedParams.dashboardId) !== -1) {
                                transmission.save({ status: 'failed' });
                                transmissions.remove(transmission);
                            }
                        }
                    });
                }

                return duplicate.save({
                    subject_id: `${duplicate.get('subject_id')}*`,
                    isDuplicate: true
                });
            })
            .then(() => resume(true));
        },
        resolve: [{ action: 'defaultAction' }]
    });

    /*
     * GatewayTransmit
     * Executes all transmission queue items and receives any DCF data.
     */
    rules.add({
        id: 'GatewayTransmit',
        trigger: [
            'HOME:Transmit',
            'VISITGATEWAY:Transmit',
            'FORMGATEWAY:Transmit',
            'LOGIN:Transmit',
            'USERMANAGEMENT:Transmit'
        ],
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'transmitAll' },
            {
                action: () => {
                    // @FIXME: This is a workaround for the delay between transmission of a diary and the diary
                    // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                    return Q.delay(1000);
                }
            },
            { action: 'refreshAllData' },
            { action: 'transmitLogs' },
            { action: 'terminationCheck' },
            { action: 'updateCurrentContext' },
            { action: 'removeMessage' }
        ],
        reject: [{
            action: 'transmitRetry',
            data (input, done) {
                input.message = input.isOnline ? 'TRANSMIT_RETRY_FAILED' : 'TRANSMIT_RETRY_CONNECTION_REQUIRED';

                done(input);
            }
        }, {
            action: 'preventDefault'
        }]
    });

    rules.add({
        id: 'SyncVisitAndFormData',
        trigger: 'HISTORICALDATASYNC:Received/UserReports',
        evaluate: true,
        resolve: [
            { action: 'saveUserVisitsAndReports' },
            { action: ({ res }) => setTime(res.lastRefreshTime) },
            { action: 'defaultAction' }
        ]
    });

    /*
     * This rule syncs user data when it is received
     */
    rules.add({
        id: 'SyncSubjectsData',
        trigger: 'HISTORICALDATASYNC:Received/Subjects',
        evaluate ({ res: subjects, collection }, done) {
            let codeLength = design.participantSettings.participantIDFormat.length,
                subjectIDList = subjects.map((a) => {
                    return a.subject_id;
                }),
                krptList = subjects.map((a) => {
                    return a.krpt;
                }),
                isInKrptList = function (krptList, subject) {
                    return _.indexOf(krptList, subject.get('krpt')) !== -1;
                },
                isNotInSubjectIDList = function (subjectIDList, subject) {
                    if (subject.get('subject_id').slice(-1) === '*') {
                        return _.indexOf(subjectIDList, subject.get('subject_id').substr(0, codeLength)) === -1;
                    }
                    return _.indexOf(subjectIDList, subject.get('subject_id')) === -1;
                },
                duplicates = collection.filter((subject) => {
                    return subject.get('isDuplicate') && (isNotInSubjectIDList(subjectIDList, subject) || subject.get('subject_id').slice(-1) !== '*');
                }),
                removeDeletedSubjects = () => Q.all(collection.filter((subject) => {
                    // A subject should not be deleted from the client db
                    // If it is in the list received from the server
                    // or it is marked as a duplicate subject
                    // or it is a newly created subject which is not yet available on backend
                    return !isInKrptList(krptList, subject) &&
                        !subject.get('isDuplicate') && _.indexOf(duplicates, subject) === -1 &&
                        subject.get('setupCode');
                }).map((subject) => {
                    return collection.destroy(subject);
                }));

            duplicatPatientTransmissionsResolve({ subjects: duplicates, codeLength })
            .then(() => {
                return removeDeletedSubjects()
                .catch((err) => {
                    logger.error(err);
                    done(false);
                })
                .all(_.map(subjects, (subjectData) => {
                    let subjectModel = collection.findWhere({ krpt: subjectData.krpt }) || new collection.model();

                    if (subjectData.deleted) {
                        return subjectModel.destroy();
                    } else if (subjectModel.get('isDuplicate')) {
                        subjectData.subject_id = subjectModel.get('subject_id');
                    }

                    return subjectModel.save(subjectData)
                    .then(() => collection.add(subjectModel));
                }))
                .then(() => {
                    let lastDiaries = new LastDiaries(),
                        deletedKrpt = subjects.reduce((list, subjectData) => {
                            subjectData.deleted && list.push(subjectData.krpt);
                            return list;
                        }, []);

                    return lastDiaries.fetch()
                    .then(() => {
                        let toBeDeleted = lastDiaries.filter(lastDiary => deletedKrpt.indexOf(lastDiary.get('krpt')) > -1);

                        return Q.all(_.map(toBeDeleted, lastDiary => lastDiary.destroy()));
                    });
                })
                .then(() => Users.fetchCollection())
                .then((users) => {
                    return users.updateAllSubjectUsers({
                        userType: 'Subject',
                        password: '2a927b74c9cc27f158c86c574634b3bc',
                        salt: 'temp', // createGUID(),
                        role: 'subject',
                        failedLoginAttempts: 0,
                        permanentPasswordCreated: false
                    });
                });
            })
            .catch((err) => {
                logger.error(err);
                done(false);
            })
            .done(() => {
                done(true);
            });
        },

        resolve: [{ action: 'defaultAction' }]
    });

    /*
     * LoginFailure
     * Use to manage failed login attempts.
     */
    rules.add({
        id: 'LoginFailure',
        trigger: 'LOGIN:Failure',
        evaluate (filter, resume) {
            if (filter.allowed === 0) {
                resume(false);
            } else {
                resume(filter.failures >= filter.allowed);
            }
        },
        resolve: [{ action: 'defaultAction', data: false }]
    });

    /*
     * ChangePhase
     * Checks to change phase when a questionnaire is completed.
    */
    rules.add({
        id: 'ChangePhase',
        trigger: 'QUESTIONNAIRE:Completed',
        evaluate (context, callback) {
            let subject = this.subject,
                triggerPhase = this.model.get('triggerPhase');

            // save off rule info into filter (so available to data
            // (data not executed with a "this" being the questionnaire )
            context.subject = subject;
            context.triggerPhase = triggerPhase;

            if (subject && (subject.get('phaseTriggered') === 'true' || triggerPhase !== undefined)) {
                callback(LF.StudyDesign.studyPhase[triggerPhase] !== subject.get('phase'));
            } else {
                callback(false);
            }
        },
        resolve: [{
            action: 'changePhase',
            data (context, callback) {
                let subject = context.subject,
                    triggerPhase = context.triggerPhase;

                if (subject.get('phaseTriggered') === 'true') {
                    callback({
                        change: true,
                        phase: subject.get('phase'),
                        phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset')
                    });
                } else if (triggerPhase !== undefined) {
                    callback({
                        change: true,
                        phase: LF.StudyDesign.studyPhase[triggerPhase],
                        phaseStartDateTZOffset: LF.Utilities.timeStamp(new Date())
                    });
                }
            }
        }]
    });

    /*
     * Save SPStartDate
     * Save SPStartDate upon questionnaire completion.
     */
    rules.add({
        id: 'SaveSPStartDate',
        trigger: [
            'QUESTIONNAIRE:Completed',
            'ACCOUNT:Configured'
        ],
        evaluate (context, callback) {
            callback(this.subject && !this.subject.get('spStartDate') && (context.questionnaire !== 'New_Patient'));
        },
        salience: 3,
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'saveSPStartDate' },
            { action: 'removeMessage' }
        ]
    });

    /*
     * QuestionnaireTransmit
     * Transmit upon questionnaire completion.
     */
    rules.add({
        id: 'QuestionnaireTransmit',
        trigger: 'QUESTIONNAIRE:Transmit',
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        salience: 2,
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'transmitAll' },
            {
                action: () => {
                    // @TODO: This is a workaroud for the delay between transmission of a diary and the diary
                    // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                    return Q.delay(1000);
                }
            },
            { action: 'refreshAllData' },
            { action: 'transmitLogs' },
            { action: 'terminationCheck' },
            { action: 'updateCurrentContext' },
            { action: 'removeMessage' }
        ],
        reject: [{
            action (input) {
                if (!input.isOnline) {
                    // If the device is offline no need to display dialog or confirmation for retry.
                    // We support offline usage and this is not an unexpected situation.
                    return Q();
                }

                let { Dialog } = MessageRepo;
                return notify({
                    dialog: Dialog && Dialog.TRANSMIT_RETRY_FAILED,
                    options: { httpRespCode: input.httpRespCode || '' }
                });
            }
        }]
    });

    /**
     * QuestionnaireCancel
     */
    rules.add({
        id: 'QuestionnaireCancel',
        trigger: 'QUESTIONNAIRE:Canceled',
        evaluate: true,
        salience: 1,
        resolve: [{
            action: () => {
                return Q.Promise((resolve) => {
                    if (Data.skippedVisits) {
                        Data.skippedVisits = undefined;
                    }

                    resolve(true);
                });
            }
        }]
    });

    // FirstSiteUserSave
    rules.add({
        id: 'FirstSiteUserSave',
        trigger: 'QUESTIONNAIRE:Completed/First_Site_User',
        resolve: [
            {
                action: () => {
                    lStorage.removeItem('isActivation');
                }
            },
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'newUserSave' },
            {
                action () {
                    $('#nextItem').attr('disabled', 'disabled');
                    return ELF.trigger('FirstSiteUserSave:Transmit', {}, this);
                }
            },
            { action: 'launchGetRaterTraining' },
            { action: 'setState', data: State.states.activated },
            { action: 'removeMessage' },
            { action: 'navigateTo', data: 'login' },
            { action: 'preventAll' }
        ]
    });

    rules.add({
        id: 'FirstSiteUserTransmit',
        trigger: [
            'FirstSiteUserSave:Transmit',
            'FirstSiteUserQuestionnaireTimeout:Transmit'
        ],
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'transmitAll' },
            {
                action: () => {
                    // @TODO: This is a workaroud for the delay between transmission of a diary and the diary
                    // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                    return Q.delay(1000);
                }
            },
            { action: 'refreshAllData' },
            { action: 'transmitLogs' },
            { action: 'terminationCheck' },
            { action: 'updateCurrentContext' }
        ],
        reject: [{
            action (input) {
                if (!input.isOnline) {
                    // If the device is offline no need to display dialog or confirmation for retry.
                    // We support offline usage and this is not an unexpected situation.
                    return Q();
                }

                let { Dialog } = MessageRepo;
                return notify({
                    dialog: Dialog && Dialog.TRANSMIT_RETRY_FAILED,
                    options: { httpRespCode: input.httpRespCode || '' }
                });
            }
        }]
    });

    // FirstSiteUserBackout
    rules.add({
        id: 'FirstSiteUserBackout',
        trigger: 'QUESTIONNAIRE:BackOut/First_Site_User',
        salience: 2,
        evaluate: {
            expression: 'confirm',
            input: { key: 'BACK_OUT_CONFIRM' }
        },
        resolve: [
            {
                action: (input, done) => {
                    if (EULA.isAccepted()) {
                        State.set(State.states.setTimeZone);
                        LF.router.navigate('setTimeZone', true);
                    } else {
                        State.set(State.states.endUserLicenseAgreements);
                        LF.router.navigate('endUserLicenseAgreements', true);
                    }
                    done();
                }
            },
            { action: 'preventAll' }
        ],
        reject: [{ action: 'preventAll' }]
    });

    /*
     * FirstSiteUserQuestionnaireTimeout
     * If the questionnaire times out during First Site User creation and the affidavit has been answered,
     * save the questionnaire and go to login; Otherwise, navigate to language selection.
     */
    rules.add({
        id: 'FirstSiteUserQuestionnaireTimeout',
        trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/First_Site_User',
        salience: 4,
        evaluate: 'isAffidavitSigned',
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            {
                action: () => {
                    Data.Questionnaire.questionnaireSessionTimeout = true;
                }
            },
            { action: 'newUserSave' },
            {
                action () {
                    return ELF.trigger('FirstSiteUserQuestionnaireTimeout:Transmit', {}, this);
                }
            },
            { action: 'removeMessage' },
            { action: 'setState', data: State.states.activated },
            { action: 'navigateTo', data: 'login' },
            { action: 'preventAll' }
        ],
        reject: [
            { action: 'notify', data: { key: 'DIARY_TIMEOUT' } },
            { action: 'setState', data: State.states.languageSelection },
            { action: 'navigateTo', data: 'languageSelection' },
            { action: 'preventAll' }
        ]
    });

    /**
     * NewUserSave
     */
    rules.add({
        id: 'NewUserSave',

        // DE16755 - After completing the New User form, the home screen was being displayed.
        // A short circuit on the QuestionnaireCompletionView was removed, triggering a navigate to 'dashboard'.
        // I changed the trigger to one prior to the handoff to the QuestionnaireCompletionView
        // to avoid additional workflow and resolve the issue.
        trigger: 'QUESTIONNAIRE:Navigate/New_User_SitePad/AFFIDAVIT',
        evaluate: 'isDirectionForward',
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'newUserSave' },
            {
                action () {
                    $('#nextItem').attr('disabled', 'disabled');
                    $('#prevItem').attr('disabled', 'disabled');
                    return ELF.trigger('NewUserSave:Transmit', {}, this);
                }
            },
            { action: 'updateCurrentContext' },
            { action: 'launchGetRaterTraining' },
            { action: 'removeMessage' },
            { action: 'setState', data: State.states.activated },
            { action: 'navigateTo', data: 'site-users' },
            { action: 'preventAll' }
        ]
    });

    rules.add({
        id: 'GenericTransmitWithoutRetry',
        trigger: [
            'NewUserSave:Transmit',
            'UserChangeCredentials:Transmit',
            'ActivateUserCompleted:Transmit',
            'DeactivateUserCompleted:Transmit',
            'EditPatientDiarySync:Transmit',
            'EditUserCompleted:Transmit'
        ],
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [
            { action: 'displayMessage', data: 'PLEASE_WAIT' },
            { action: 'transmitAll' },
            {
                action: () => {
                    // @TODO: This is a workaroud for the delay between transmission of a diary and the diary
                    // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                    return Q.delay(1000);
                }
            },
            { action: 'refreshAllData' },
            { action: 'transmitLogs' },
            { action: 'terminationCheck' }
        ],
        reject: [{
            action (input) {
                if (!input.isOnline) {
                    // If the device is offline no need to display dialog or confirmation for retry.
                    // We support offline usage and this is not an unexpected situation.
                    return Q();
                }

                let { Dialog } = MessageRepo;
                return notify({
                    dialog: Dialog && Dialog.TRANSMIT_RETRY_FAILED,
                    options: { httpRespCode: input.httpRespCode || '' }
                });
            }
        }]
    });

    // NewUserQuestionnaireTimeout
    rules.add({
        id: 'NewUserQuestionnaireTimeout',
        trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/New_User_SitePad',
        evaluate: 'isAffidavitSigned',
        salience: 3,
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'newUserSave'
        }, {
            action: 'launchGetRaterTraining'
        }, {
            action: 'removeMessage'
        }, {
            action: 'setState',
            data: State.states.activated
        }, {
            action: 'navigateTo',
            data: 'site-users'
        }, {
            action: 'preventAll'
        }],

        // Prevent the default functionality from executing.
        // This prevents a dashboard record from being written to the database.
        reject: [{
            action: 'navigateTo',
            data: 'site-users'
        }, {
            action: 'notify',
            data: { key: 'DIARY_TIMEOUT' }
        }, {
            action: 'preventAll'
        }]
    });

    /**
     * NewUserTimeout
     */
    rules.add({
        id: 'NewUserSessionTimeout',
        trigger: 'QUESTIONNAIRE:SessionTimeout/New_User_SitePad',
        evaluate: 'isAffidavitSigned',
        salience: 3,
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'newUserSave'
        }, {
            action: 'setState',
            data: State.states.activated
        }, {
            action: 'removeMessage'
        }]
    });

    /*
     * NewUserBackout
     */
    rules.add({
        id: 'NewUserBackout',
        salience: 2,
        trigger: 'QUESTIONNAIRE:BackOut/New_User_SitePad',
        evaluate: {
            expression: 'confirm',
            input: { key: 'BACK_OUT_CONFIRM' }
        },
        resolve: [
            { action: 'navigateTo', data: 'site-users' },
            { action: 'preventAll' }
        ],

        // DE16874 - Cancel has to be clicked twice.
        // After cancel was pressed once, the ELF rule chain would continue,
        // triggering another confirmation.  I've added preventAll to stop
        // the second confirmation from displaying.
        reject: [{ action: 'preventAll' }]
    });

    rules.add({
        id: 'NewUserOpen',
        trigger: 'USERMANAGEMENT:NewUser',
        evaluate: () => {
            return Q.Promise((resolve) => {
                let roles = utils.getNested(LF, 'StudyDesign.roles'),
                    role = roles.get(CurrentContext().role),
                    permissions = role.get('addPermissionsList');

                // If the logged in user has permission to create assign all roles, resolve true to render the diary.
                if (_.contains(permissions, 'ALL')) {
                    resolve(true);
                } else {
                    // Otherwise, determine if the user can assign any roles at all.
                    let canAssign = roles.filter((role) => {
                        return _.contains(permissions, role.get('id'));
                    });

                    resolve(!!canAssign.length);
                }
            });
        },
        reject: [{
            action: 'notify',
            data: { key: 'INVALID_PERMISSION' }
        }, {
            action: 'preventDefault'
        }]
    });

    // Handle the received LastDiaryTableSync Data
    rules.add({
        id: 'SyncLastDiary',
        trigger: 'HISTORICALDATASYNC:Received/LastDiaries',
        evaluate: true,
        resolve: [{ action: 'handleLastDiariesResult' }]
    });

    // Handle the received Users Data
    rules.add({
        id: 'handleUserSyncData',
        trigger: 'HISTORICALDATASYNC:Received/Users',
        evaluate: true,
        resolve: [
            { action: 'handleUserSyncData' },
            { action: 'defaultAction' }
        ]
    });

    // Skip or Display First SiteUser
    rules.add({
        id: 'skipOrDisplayFirstSiteUser',
        trigger: 'REGISTRATION:SkipOrDisplayFirstSiteUser',
        evaluate: true,
        resolve: [{ action: 'skipOrDisplayFirstSiteUser' }]
    });

    // Handle the case when the created user is a duplicate of one in the server
    rules.add({
        id: 'handleDuplicateUserTransmission',
        trigger: 'TRANSMIT:Duplicate/User',
        evaluate: true,
        resolve: [
            // Clear any banners.
            { action: 'clearBanner' },

            // Notify the user that the user is a duplicate, and then display the spinner.
            { action: 'notify', data: { key: 'USER_ALREADY_EXISTS_ERROR' } },
            { action: 'displayMessage', data: 'PLEASE_WAIT' },

            // Handle the duplicated user record.
            { action: 'handleDuplicateUserTransmission' }
        ]
    });

    // Handle the case when a updated user has been deleted on the server.
    rules.add({
        id: 'handleNonexistentUser',
        trigger: 'TRANSMIT:Nonexistent/User',
        resolve: [
            { action: 'clearBanner' },
            { action: 'removeMessage' },

            { action: 'notify', data: { key: 'NONEXISTENT_USER' } },
            { action: 'displayMessage' },

            { action: 'handleNonexistentUser' }
        ]
    });

    /*
     * AffidavitSessionTimeout
     * If the session times out during a questionnaire and the affidavit has been answered, save the questionnaire and logout; Otherwise, logout.
     */
    rules.add({
        id: 'AffidavitSessionTimeout',
        trigger: 'QUESTIONNAIRE:SessionTimeout',
        evaluate: 'isAffidavitSigned',
        resolve: [{
            action (filter, done) {
                Data.Questionnaire.sessionTimeout = true;
                done();
            }
        }, {
            action: 'navigateTo',
            data: 'questionnaire-completion'
        }],
        reject: [{ action: 'logout' }]
    });

    /*
     * AffidavitQestionnaireTimeout
     * If the questionnaire times out during a questionnaire and the affidavit has been answered, save the questionnaire and render dashboard; Otherwise, render dashboard.
     */
    rules.add({
        id: 'AffidavitQuestionnaireTimeout',
        trigger: 'QUESTIONNAIRE:QuestionnaireTimeout',
        evaluate: 'isAffidavitSigned',
        resolve: [{
            action (filter, done) {
                Data.Questionnaire.questionnaireSessionTimeout = true;
                done();
            }
        }, {
            action: 'navigateTo',
            data: 'questionnaire-completion'
        }],
        reject: [{
            action: 'notify',
            data: { key: 'DIARY_TIMEOUT' }
        }, {
            action (input, done) {
                LF.router.flash(this.defaultFlashParamsOnExit).navigate(this.defaultRouteOnExit, true);
                done();
            }
        }]
    });

    /**
     * SkipVisitQuestionnaireTimeout
     * Handles a timeout during the skip visit questionnaire.
     * If the timeout is fired, the user should be redirected to the visit gateway, not the dashboard.
     * This rule will prevent the base QuestionnaireTimeout rule from firing.
     */
    rules.add({
        id: 'SkipVisitQuestionnaireTimeout',
        trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Skip_Visits',
        salience: 3,
        evaluate: 'isAffidavitSigned',
        resolve: [{
            action (filter, done) {
                Data.Questionnaire.questionnaireSessionTimeout = true;
                done();
            }
        }, {
            action: 'navigateTo',
            data: 'questionnaire-completion'
        }],
        reject: [{
            action: 'notify',
            data: { key: 'DIARY_TIMEOUT' }
        }, {
            action () {
                LF.router.flash({
                    subject: this.subject
                }).navigate(`visits/${this.subject.get('id')}`, true);

                return Q();
            }
        }, {
            action: 'preventAll'
        }]
    });

    rules.add({
        id: 'CompletionViewSavedDiarySessionTimeout',
        trigger: 'COMPLETIONVIEW:SavedDiarySessionTimeout',
        evaluate: true,
        resolve: [{ action: 'removeMessage' }]
    });

    // QuestionnaireBackOut
    // Displays a confirmation prompt when a user tries to back out of a questionnaire.
    rules.add({
        id: 'QuestionnaireBackOut',
        trigger: 'QUESTIONNAIRE:BackOut',
        evaluate: {
            expression: 'confirm',
            input: { key: 'BACK_OUT_CONFIRM' }
        },
        resolve: [{
            action (input, done) {
                LF.security.stopQuestionnaireTimeOut();
                localStorage.setItem('questionnaireToDashboard', true);

                LF.router.flash({
                    subject: this.subject,
                    visit: this.visit
                }).navigate('dashboard', {
                    trigger: true,
                    replace: true
                });

                done();
            }
        }],
        reject: [{
            action (input, done) {
                localStorage.removeItem('questionnaireToDashboard');
                done({ preventDefault: true });
            }
        }]
    });

    /*
     * Termination
     * Handles end participation of subject
     */
    rules.add({
        id: 'SubjectTermination',
        trigger: 'SubjectTermination',
        resolve: [{
            action: (input) => {
                let subject = input.subject;

                return Transmissions.fetchCollection()
                .then((transmissions) => {
                    return Q.all(transmissions.models.map((item) => {
                        if (JSON.parse(item.get('params')).krpt === subject.get('krpt') && item.get('method') === 'sitepadPasswordSecretQuestion') {
                            return transmissions.destroy(item.get('id'));
                        }
                        return Q();
                    }));
                });
            }
        }]
    });

    /*
     * If checkForTimeSlip is set to false, prevent TimeConfirmation rule from firing.
     */
    rules.add({
        id: 'SkipTimeConfirmation',
        trigger: [
            'APPLICATION:Loaded',
            'TIMESLIP:Retry'
        ],
        salience: 2,
        evaluate: () => {
            const timeTravel = Utilities.isTimeTravelConfigured();

            if (LF.environment.checkForTimeSlip === false) {
                logger.operational('TimeSlipCheck is skipped due to studyDesign flag "checkForTimeSlip".');
                return true;
            }

            if (timeTravel) {
                logger.operational('TimeSlipCheck is skipped because timeTravel is configured".');
                return true;
            }

            if (!State.isRegistered()) {
                logger.operational('TimeSlipCheck is skipped because device is not registered.');
                return true;
            }
            return false;
        },
        resolve: [{
            action: () => ({ stopRules: true })
        }, {
            action: 'navigateTo',
            data: ''
        }]
    });

    /**
     * Time Confirmation
     */
    rules.add({
        id: 'TimeConfirmation',
        trigger: [
            'APPLICATION:Loaded',
            'TIMESLIP:Retry'
        ],
        salience: 1,
        evaluate (input, resume) {
            COOL.getClass('Utilities').isOnline()
            .then((isOnline) => {
                let registered = State.isRegistered(),
                    isOfflineAndRegistered;

                isOfflineAndRegistered = !isOnline && registered;

                // Show Blind confirmation screen if the device is in offline mode and is already activated
                resume(!isOfflineAndRegistered);
            })
            .done();
        },
        resolve: [{
            action: 'navigateTo',
            data: 'blank'
        }, {
            action: 'setLanguageToSite'
        }, {
            action: 'timeSlipCheck'
        }, {
            action: 'navigateTo',
            data: ''
        }],

        // US7106
        reject: [{
            action: () => logger.operational('TimeConfirmation: Device is offline or no server info is available.')
        }, {
            action: 'setLanguageToSite'
        }, {
            action: 'navigateTo',
            data: 'time-confirmation'
        }, {
            action: 'preventDefault'
        }]
    });

    rules.add({
        id: 'NewPatientPostSave',
        trigger: 'QUESTIONNAIRE:Saved/New_Patient',
        evaluate: true,
        resolve: [{ action: 'launchGetRaterTraining' }]
    });

    rules.add({
        id: 'New_User_RoleChange',
        trigger: [
            'QUESTIONNAIRE:Answered/New_User_SitePad/ADD_USER_ROLE',
            'QUESTIONNAIRE:Displayed/New_User_SitePad/ADD_USER_S_1'
        ],
        resolve: [{
            action () {
                return this.questionViews.filter((question) => {
                    return question.widgetModel.get('type') === 'ConfirmationTextBox' ||
                        question.widgetModel.get('type') === 'TempPasswordTextBox';
                }).forEach((widget) => {
                    return widget.render().then(() => {
                        let roleAnswer = this.answers.findWhere({ question_id: 'ADD_USER_ROLE' });

                        if (roleAnswer && JSON.parse(roleAnswer.get('response')).role !== '') {
                            this.$(`#${widget.id} input`).removeAttr('disabled');
                        }
                    });
                });
            }
        }]
    });

    // FirstSiteUser setting role
    rules.add({
        id: 'FirstSiteUserSaveNewUserRole',
        trigger: 'QUESTIONNAIRE:Open/First_Site_User',
        resolve: [{
            action () {
                return this.data.newUserRole = 'admin';
            }
        }]
    });

    rules.add({
        id: 'HandleTransmissionError',
        trigger: 'EXECUTEALL:Failed',
        resolve: [{
            action ({ errorStack }) {
                let krptNotValidErrors = _.filter(errorStack, (item) => {
                    return item.err.httpCode === 403 && item.err.errorCode === '14';
                });

                if (!krptNotValidErrors.length) {
                    return Q();
                }

                let { Dialog } = MessageRepo;
                return notify({
                    dialog: Dialog && Dialog.TRANSMIT_FAILED_403_14
                });
            }
        }, {
            action ({ errorStack }) {
                let connectivityErrors = _.filter(errorStack, (item) => {
                    return item.err.httpCode === 404 || item.err.httpCode === 0;
                });

                if (!connectivityErrors.length) {
                    return Q();
                }

                let { Dialog } = MessageRepo;
                return notify({ dialog: Dialog && Dialog.TRANSMIT_FAILED_404 })
                .then(() => ({ preventDefault: true }));
            }
        }]
    });
})(ELF.rules);
