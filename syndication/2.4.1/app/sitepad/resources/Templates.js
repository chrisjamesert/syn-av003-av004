/**
 * @fileOverview Core set of templates.
 * @author <a href="mailto:chopkins@phtcorp.com">Corey Hopkins</a>
 * @version 1.4
 */

import Templates from 'core/resources/Templates';

Templates.set([
    {
        name: 'IndicatorFull',
        namespace: 'DEFAULT',
        template: '<div>' +
        '<img src="{{ image }}" alt="{{ label }}"/>' +
        '<p>{{ label }}</p>' +
        '</div>'
    }, {
        name: 'IndicatorImage',
        namespace: 'DEFAULT',
        template: '<div><img src="{{ image }}"/></div>'
    }, {
        name: 'IndicatorLabel',
        namespace: 'DEFAULT',
        template: '<div><p>{{ label }}</p></div>'
    }, {
        name: 'Questionnaire',
        namespace: 'DEFAULT',
        template: '<a class="navigate-right"><strong>{{ title }}</strong></a>'
    }, {
        name: 'Question',
        namespace: 'DEFAULT',
        template: '<p class="{{ label }}">{{ text }}</p>'
    }, {
        name: 'QuestionHeader',
        namespace: 'DEFAULT',
        template: '<div class="question-header row">' +
        '<div class="col-xs-12">{{ left }}</div>' +
        '</div>'
    }, {
        name: 'Help',
        namespace: 'DEFAULT',
        template: '<p class="help"><strong>{{ help }}</strong></p><hr />'
    },

    // Default select
    {
        name: 'Select',
        namespace: 'DEFAULT',
        template: '<select id="{{ id }}" name="{{ id }}" class="{{ className }}"></select>'
    },
    {
        name: 'Input',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}"><br />{{ label }}</label><input type="{{ type }}" id="{{ id }}" name="{{ name }}" placeholder="{{ placeholder }}" size="{{ size }}" />'
    }, {
        name: 'DatePicker',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}" class="date-input" min="{{ min }}" max="{{ max }}" data-role="datebox" data-options=\'{{ configuration }}\' />'
    }, {
        name: 'DatePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}">{{ dateLabel }}</label>'
    }, {
        name: 'TimePicker',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}_time_input" class="time-input" min="{{ min }}" max="{{ max }}" data-role="datebox" data-options=\'{{ configuration }}\' />'
    }, {
        name: 'TimePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}_time_input">{{ timeLabel }}</label>'
    }, {
        name: 'AlarmPicker',
        namespace: 'DEFAULT',
        template: '<div class="hidden"><input id="{{ id }}_alarm_input" class="alarm-input" min="{{ min }}" max="{{ max }}" data-role="datebox" data-options=\'{{ configuration }}\' /></div>' +
        '<a><span>{{ diaryName }}</span>' +
        '<span class="right ui-li-count ">{{ alarmTime }}</span></a>'
    }, {
        name: 'VasTextLabels',
        namespace: 'DEFAULT',
        template: '<div class="vas-label row">' +
        '<div class="col-xs-6 text-left"><p>{{ min }}</p></div>' +
        '<div class="col-xs-6 text-right"><p>{{ max }}</p></div>' +
        '</div>'
    }, {
        name: 'EQ5D',
        namespace: 'DEFAULT',
        template: `<div id="parentContainer" class="eq5d-sp-outer-container">
            <div class="eq5d-container">
                <div id="instructions">
                    <div>{{ instructionText }}</div>
                    <div>{{ footerText }}</div>
                </div>
                <div id="left"  class="eq5d-left-col">
                    <div id="spacer"/>
                    <div id="answerDIV">
                    </div>
                    <div id="yhtDIV">
                        <p>{{ answerLabelText }}</p></p>
                    </div>
                </div>
                <div id="right" class='eq5d-right-col'>
                    <p id="topper">{{ topText }}</p>
                    <div id="middle">
                    <canvas id="mainVasCanvas" height="0" width="0" />
                    </div>
                    <p id="bottom">{{ botText }}</p>
                </div>
            </div>`
    }, {
        name: 'Markers',
        namespace: 'DEFAULT',
        template: '<div class="markers clearfix"><marker class="left">{{ left }}</marker><marker class="right">{{ right }}</marker></div>'
    }, {
        name: 'ListView',
        namespace: 'DEFAULT',
        template: '<ul id="{{ id }}" data-role="listview"></ul>'
    }, {
        name: 'Screen',
        namespace: 'DEFAULT',
        template: '<section class="screen {{ className }}"></div>'
    }, {
        name: 'SubjectNumber',
        namespace: 'DEFAULT',
        template: '<div><strong>{{ subjectLabel }} {{ subjectNumber }}</strong></div>'
    }, {
        name: 'SiteCode',
        namespace: 'DEFAULT',
        template: '<div><strong>{{ siteLabel }} {{ siteCode }}</strong></div>'
    }, {
        name: 'PasswordRules',
        namespace: 'DEFAULT',
        template: '<div class="password-rules">{{ text }}</div><hr />'
    }, {
        name: 'BarcodeScanner',
        namespace: 'DEFAULT',
        template: '<form class="scannerInputForm" data-ajax="false" autocomplete="off" autocorrect="off">' +
        '<input type="text" class="scannerData" />' +
        '</form>' +
        '<div class="scanBtn" data-role="button"></div>'
    }, {
        name: 'ReviewScreen',
        namespace: 'DEFAULT',
        template: '<div id="{{ id }}" class="reviewScreen"><ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview"></ul>' +
        '<hr />' +
        '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul></div>'
    }, {
        name: 'ReviewScreenItems',
        namespace: 'DEFAULT',
        template: '<li><div>{{ text0 }}</div></li>'
    }, {
        name: 'ReviewScreenButtons',
        namespace: 'DEFAULT',
        template: '<li><a data-role="button">{{ text }}</a></li>'
    }, {
        name: 'NumberSpinner',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}" class="{{ className }}" />'
    }, {
        name: 'SignatureBox',
        namespace: 'DEFAULT',
        template: '<div class="signature-frame">' +
        '<div class="signature-box"></div>' +
        '<button type="submit" class="btn btn-lg" id="signature-clear" data-role="button" data-inline="true" data-theme="a">' +
        '{{ clear }}' +
        '</button>' +
        '</div>'
    }, {
        name: 'SiteUser',
        namespace: 'DEFAULT',
        template: '<div id="site-user-name"><strong>{{ siteUserLabel }} {{ siteUser }}</strong></div>'
    }, {
        name: 'SkipReason',
        namespace: 'DEFAULT',
        template: '<div id="skip-reason-content" class="container-fluid">' +
        '<div class="col-xs-9">' +
        '<div class="skip-reason-header">{{ reason }}</div>' +
        '<ul class="skip-reason-answers"></ul>' +
        '</div>' +
        '<div class="col-xs-3">' +
        '<dl>' +
        '<dt>{{ patientIdLabel }}</dt>' +
        '<dd>{{ patientId }}</dd>' +
        '<dt>{{ patientInitialsLabel }}</dt>' +
        '<dd>{{ patientInitials }}</dd>' +
        '<dt>{{ skippedVisitsLabel }}</dt>' +
        '<dd>{{ skippedVisits }}</dd>' +
        '</dl>' +
        '</div>' +
        '</div>'
    }, {
        name: 'EditReason',
        namespace: 'DEFAULT',
        template: '<div id="edit-reason-content" class="container-fluid">' +
        '<div class="row row-01">' +
        '<div class="col-xs-9">' +
        '<div class="edit-reason-header">{{ reason }}</div>' +
        '<ul class="edit-reason-answers"></ul>' +
        '</div>' +
        '<div class="col-xs-3">' +
        '<dl>' +
        '<dt>{{ patientIdLabel }}</dt>' +
        '<dd>{{ patientId }}</dd>' +
        '</dl>' +
        '</div>' +
        '</div>' +
        '</div>'
    }, {
        name: 'ListItem',
        namespace: 'DEFAULT',
        template: '<li id="{{id}}" value="{{value}}">{{ reasonText }}</li>'
    }, {
        name: 'ReasonListItem',
        namespace: 'DEFAULT',
        template: '<li id="{{id}}" data-reason="{{value}}">{{ reasonText }}</li>'
    }, {
        name: 'LoginInfo',
        namespace: 'DEFAULT',
        template: '<div class="{{textClass}}"><b>{{ loggedInAs }}:</b> {{userName}}&nbsp;&nbsp;&nbsp;<b>{{site}}:</b> {{siteNumber}}</div>'
    },
    {
        name: 'DateTimeSpinnerModal',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{modalTitle}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-date-container date-time-time-container date-time-container"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'DateTimeSpinnerModal_12Hour',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{modalTitle}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-date-container date-time-time-container date-time-container" data-format="LL hh:mm A"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'DateTimeSpinnerModal_24Hour',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{modalTitle}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-date-container date-time-time-container date-time-container" data-format="LL HH:mm"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    }
], {
    add: true,
    merge: true,
    remove: false
});

// A shortcut to make displaying templates easier.
LF.Resources.Templates = Templates;
LF.templates = LF.Resources.Templates;

export default Templates;
