import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';
import DailyUnlockCode from 'core/classes/DailyUnlockCode';
import Subjects from 'core/collections/Subjects';
import { MessageRepo } from 'core/Notify';
import * as lStorage from 'core/lStorage';
import getSetupCode from 'sitepad/transmit/getSetupCode';
import Spinner from 'core/Spinner';
import notifyAction from 'core/actions/notify';

const logger = new Logger('Transmit.transmitResetCredentials');

/**
 * Handles the error returned by the webservice method.
 * This is exported so that it can be reused by other credential transmit methods
 * @param {Object} err - the error object that is being handled
 * @param {Object} transmissionItem - The item in the transmission that got the error.
 * @returns {Q.Promise}
 */
export function onError (err, transmissionItem) {
    let { errorCode, httpCode, isSubjectActive } = err;
    const { Dialog } = MessageRepo;
    const reject = () => Q.reject({ errorCode, httpCode, isSubjectActive });

    // If the error is not a transmission error then just log it and remove transmission
    if (!(errorCode || httpCode || isSubjectActive != null)) {
        logger.error(`Error in ${transmissionItem.get('method')}`, err);

        this.remove(transmissionItem);
        return reject();
    }

    if (!isSubjectActive) {
        return Spinner.hide()
        .then(() => notifyAction({ dialog: Dialog && Dialog.PASSWORD_CHANGE_FAILED }))
        .then(() => Spinner.show())
        .then(() => this.destroy(transmissionItem.get('id')))
        .then(() => reject());
    }

    if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
        errorCode = errorCode || '';

        return Spinner.hide()
        .then(() => notifyAction({
            dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
            options: { httpCode, errorCode }
        }))
        .then(() => Spinner.show())
        .then(() => {
            if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && (errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND || errorCode === LF.ServiceErr.SUBJECT_NOT_ACTIVATED)) {
                // If the subject is deleted on backend then destroy the transmission item
                return this.destroy(transmissionItem.get('id'))
                .then(() => reject());
            }

            this.remove(transmissionItem);
            return reject();
        });
    }

    this.remove(transmissionItem);
    return reject();
}

/**
 * Handles the transmitResetCredentials transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise<Object>}
 */
export default function transmitResetCredentials (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params'));

    return Subjects.getSubjectBy({ krpt: params.krpt })
    .tap(getSetupCode)
    .catch((err) => {
        logger.error('Error in transmitResetCredentials', err);
        this.remove(transmissionItem);
    })
    .then((subject) => {
        let service = COOL.new('WebService', WebService),
            todaysUnlockCode = new DailyUnlockCode().getCode(new Date()),
            payload = {
                setupCode: subject.get('setupCode'),
                clientPassword: params.password,
                regDeviceId: lStorage.getItem('deviceId'),
                clientId: lStorage.getItem('clientId'),
                challengeQuestions: [{
                    question: params.secretQuestion,
                    answer: params.secretAnswer
                }]
            };

        let onSuccess = ({ res, isSubjectActive }) => {
            return subject.save({
                subject_password: params.password,
                secret_question: params.secretQuestion,
                secret_answer: params.secretAnswer,
                subject_active: isSubjectActive,
                service_password: res.password
            })
            .catch((err) => {
                logger.error('Error saving subject in transmitResetCredentials', err);
                this.remove(transmissionItem);
            })
            .then(() => {
                CurrentSubject.clearSubject();
                return this.destroy(transmissionItem.get('id'));
            });
        };

        return service.resetSubjectCredentials(payload, params.krpt, todaysUnlockCode)
        .catch((err) => {
            return onError.call(this, err, transmissionItem);
        })
        .then(onSuccess);
    });
}
