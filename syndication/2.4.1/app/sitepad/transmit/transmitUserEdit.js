import COOL from 'core/COOL';
import User from 'core/models/User';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';
import UserSync from 'core/classes/UserSync';
import { MessageRepo } from 'core/Notify';
import ELF from 'core/ELF';

const logger = new Logger('Transmit.transmitUserEdit');

/**
 * Transmits an updated user via the WebService.
 * @param {Transmission} transmissionItem - The model in the transmission queue to send.
 * @returns {Q.Promise<boolean>}
 */
export default function transmitUserEdit (transmissionItem) {
    if (transmissionItem.get('status') === 'failed') {
        return Q(false);
    }

    let { id } = JSON.parse(transmissionItem.get('params'));
    let user = new User({ id });

    // Destroy the transmission record and resolve the promise.
    // @param {boolean} returnValue - The value to return.
    // @returns {Q.Promise<boolean>}
    let destroyAndResolve = (returnValue, error) => {
        return this.destroy(transmissionItem.get('id'))
        .then(() => {
            // If the return value is false, we're going to return
            // a rejected promise to break out of the promise chain.
            if (!returnValue) {
                return Q.reject(error);
            }

            return Q(returnValue);
        });
    };

    // Get the syncLevel of the target user.
    // @returns {(Q.Promise<boolean>|string)}
    // @example
    // return getSyncLevel();
    let getSyncLevel = () => {
        let syncLevel = LF.StudyDesign.roles.findWhere({ id: user.get('role') }).get('syncLevel');

        if (!syncLevel) {
            // If no syncLevel, the updates are not transmitted.
            let message = `User: ${user.get('username')} with role: ${user.get('role')} does not have a syncLevel.  Edits to the user were not transmitted to SW.`;

            logger.operational(message);

            return destroyAndResolve(false, message);
        }

        return syncLevel;
    };

    // Get the target user's syncValue.
    // @param {string} syncLevel - The target user's syncLevel.
    // @returns {Q.Promise<Object>}
    // @example
    // return getSyncValue('site');
    let getSyncValue = (syncLevel) => {
        return UserSync.getValue(syncLevel)
        .then((syncValue) => {
            if (!syncValue && syncValue !== '') {
                // If no syncValue, the updates are not transmitted.
                let message = `User: ${user.get('username')} with role: ${user.get('role')} does not have a syncValue.  Edits to the user were not transmitted to SW.`;

                logger.operational(message);

                return destroyAndResolve(false, message);
            }

            return { syncLevel, syncValue };
        });
    };

    // Update the target user via WebService.
    // @param {Object} sync - The syncLevel and syncValue of the user.
    // @param {string} sync.syncLevel - The syncLevel of the user.
    // @param {string} sync.syncValue - The syncValue of the user.
    // @returns {Q.Promise<string>} - E|N|D|S
    // @example
    // return updateUser({ syncLevel: 'site', syncValue: 'DOM.1234123' });
    let updateUser = (sync) => {
        // Pick all the user properties we want to transmit to SW.
        // We're using userId, instead of the local id for SW to identify the user.
        // password, salt, securityQuestion and securityAnswer are omitted, as they are updated via another transmission.
        let userJSON = user.pick('userId', 'username', 'userType', 'language', 'role', 'active');

        // Extend the syncValue and syncLevel onto the user JSON.
        userJSON = _.extend(userJSON, sync);

        let service = COOL.new('WebService', WebService);

        return service.updateUser(userJSON);
    };

    // Handles the case of a duplicate user.
    // returns {Q.Promise<void>}
    let handleDuplicate = () => {
        logger.info(`Transmit update user failed. User ${user.get('username')} with role ${user.get('role')} already exists.`);

        return ELF.trigger('TRANSMIT:Duplicate/User', { user, transmissionItem }, this)
        .then((e) => {
            if (e.preventDefault) {
                return Q(false);
            }

            const { Dialog } = MessageRepo;

            return MessageRepo.display(Dialog && Dialog.USER_ALREADY_EXISTS_ERROR)
            .then(() => {
                return destroyAndResolve(false);
            });
        });
    };

    // Handles the case of a nonexistent user.
    // returns {Q.Promise<void>}
    let handleNonexistent = () => {
        let error = `${user.get('username')} does not exist.`;
        logger.error(error);

        return ELF.trigger('TRANSMIT:Nonexistent/User', { user }, this)

        // Succeed, or fail, we want the transmission record gone.
        .finally(() => destroyAndResolve(false, error));
    };

    let onSuccess = ({ res }) => {
        const { Banner } = MessageRepo;

        switch (res) {
            case 'S':
                logger.operational(`${user.get('username')} was updated.`);

                if (Banner && Banner.USER_EDITED) {
                    MessageRepo.display(Banner.USER_EDITED);
                }

                return destroyAndResolve(true);

            // The user was a duplicate, so we need to handle it.
            case 'D':
                return handleDuplicate();

            // Attempted to update a user that doesn't exist.
            case 'N':
                return handleNonexistent();

            // An error has occurred when updating the user.
            case 'E':
            default: {
                let err = `There was an error updating the user ${user.get('username')} with role ${user.get('role')}.`;
                logger.error(err);

                this.remove(transmissionItem);

                return Q.reject(err);
            }
        }
    };

    let onError = (err) => {
        if (err) {
            logger.error(`There was an error updating the user ${user.get('username')} with role ${user.get('role')}.`, err);
        }

        this.remove(transmissionItem);

        return Q.reject(err);
    };

    return user.fetch()
    .then(getSyncLevel)
    .then(getSyncValue)
    .then(updateUser)
    .catch(onError)
    .then(onSuccess)
    .finally(() => this.destroy(transmissionItem.get('id')));
}
