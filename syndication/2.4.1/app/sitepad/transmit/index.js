import COOL from 'core/COOL';
import CoreTransmit from 'core/transmit';

import getDeviceId from './getDeviceId';
import getSetupCode from './getSetupCode';
import { getCountryCode } from './getCountryCode';
import sitepadPasswordSecretQuestion from './sitepadPasswordSecretQuestion';
import transmitUserEdit from './transmitUserEdit';
import transmitSPStartDate from './transmitSPStartDate';
import transmitResetCredentials from './transmitResetCredentials';
import transmitResetPassword from './transmitResetPassword';

let Transmit = _.extend({ }, CoreTransmit, {
    getDeviceId,
    getSetupCode,
    getCountryCode,
    sitepadPasswordSecretQuestion,
    transmitUserEdit,
    transmitSPStartDate,
    transmitResetCredentials,
    transmitResetPassword
});

// Prevent sitepad transmit from overriding and therefore erasing any updates from core.
LF.Transmit = _.extend({}, LF.Transmit, Transmit);
COOL.service('Transmit', LF.Transmit);

export default Transmit;
