import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';

let logger = new Logger('Transmit.getSetupCode');

/**
 * Gets the SetupCode of a subject.
 * Runs only for sitepad because LogPad always has the Setup Code during activation.
 * @param {Subject} subject - The subject model to get the SetupCode for.
 * @returns {Q.Promise<void>}
 */
export default function getSetupCode (subject) {
    if (subject.get('setupCode')) {
        return Q();
    }

    let service = COOL.new('WebService', WebService);

    return service.getSetupCode(subject.get('krpt'))
    .tap((setupCode) => {
        if (!setupCode) {
            logger.error('Error, SetupCode is not available yet');
            throw new Error('Error, SetupCode is not available yet');
        }
    })
    .then(setupCode => subject.save({ setupCode }));
}
