import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';

const logger = new Logger('Transmit.getDeviceId');

/**
 * Gets the device_id of a subject of which password has been set before on another device
 * Runs only for sitepad because LogPad always gets the deviceId during activation
 * @param {Subject} subject - The subject model to get the deviceId for
 * @returns {Q.Promise<void>}
 */
export default function getDeviceId (subject) {
    // If deviceId already exists in client resolve
    if (subject.get('device_id')) {
        return Q();
    }

    // If subject does not exist on backend resolve
    if (!subject.get('service_password')) {
        logger.operational(`Subject with krpt: ${subject.get('krpt')} does not exist on backend. No need to call API for getDeviceID`);
        return Q();
    }

    let service = COOL.new('WebService', WebService),
        payload = {
            U: subject.get('setupCode'),
            W: subject.get('subject_password'),
            O: 'SitePad'
        };

    return service.getDeviceID(payload)
    .then(({ res }) => subject.save({ device_id: res.D }))
    .catch((err) => {
        logger.error('Error getting device id', err);
        return Q.reject(err);
    });
}
