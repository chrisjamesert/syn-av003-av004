import COOL from 'core/COOL';
import ELF from 'core/ELF';
import WebService from 'core/classes/WebService';
import Sites from 'core/collections/Sites';
import Logger from 'core/Logger';

let logger = new Logger('Transmit.getCountryCode');

/**
 * Gets the country code of the Site assigned to the sitepad.
 * @returns {Q.Promise<void>}
 */
export function getCountryCode () {
    let service = COOL.new('WebService', WebService);

    return Sites.fetchCollection()
        .then((sites) => {
            // the Sites collection should only contain one record; that of the current site.
            let site = sites.at(0);
            return service.getCountryCode(site.get('site_id'))
                 .then((code) => {
                     if (!code) {
                         logger.operational('No countryCode was returned from NetPro.');
                     } else {
                         logger.info(`WebService call to fetch countryCode has returned: ${code}`);
                         site.set({ countryCode: code });
                         sites.save();
                     }
                 }).catch((err) => {
                     logger.error(`'Error, countryCode is not available '${err}`);
                     throw new Error('Error, countryCode is not available');
                 });
        });
}
ELF.action('getCountryCode', getCountryCode);


