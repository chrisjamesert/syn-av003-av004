import Data from 'core/Data';
import ListBase from './ListBase';

export default class SkipReason extends ListBase {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            reason: 'REASON',
            patientIdLabel: 'PATIENT_ID',
            patientInitialsLabel: 'PATIENT_INITIALS',
            skippedVisitsLabel: 'VISITS_SKIPPED',
            loggedInAs: 'LOGGED_IN_AS',
            site: 'SITE',
            skippedVisits: 'SKIPPED_VISITS'
        };

        /**
         * @property {Object} options - Options passed into the constructor.
         */
        this.options = options;
    }

    /**
     * @property {string} id - The ID of the widget
     * @readonly
     * @default 'skip-reason'
     */
    get id () {
        return 'skip-reason';
    }

    /**
     * @property {string} template - The default template to render.
     * @default 'DEFAULT:SkipReason';
     */
    get template () {
        return 'DEFAULT:SkipReason';
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let modelId = this.model.get('id'),
            answer = this.answer && this.answer.get('response') ? this.answer.get('response').toString() : false;
        $('#prevItem').hide();

        return this.buildHTML('li')
        .then(() => Q.Promise((resolve) => {
            this.parent.$el.prepend(this.loginInfo).append(this.$el)
                .ready(() => {
                    resolve();
                });
        }))
        .then(() => {
            let deferred = Q.defer();
            if (answer) {
                _(this.model.get('answers')).each((item, index) => {
                    if (answer === item.value) {
                        this.$(`#${modelId}_li_${index}`)
                            .addClass('selected')
                            .ready(() => {
                                deferred.resolve();
                            });
                    }
                });
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        })
        .then(() => {
            this.delegateEvents();
        });
    }

    /**
     * Build the html structure of widget
     * @param {string} type - The type of the widget
     * @example this.buildHTML('li');
     * @returns {Q.Promise<void>} resolved when HTML is built and ready
     */
    buildHTML (type) {
        let modelId = this.model.get('id'),
            template = this.model.get('templates') || this.template,
            loginInfoTemplate = this.loginInfoTemplate,
            input = this.li,
            deferred = Q.defer(),
            dynamicStrings = {
                patientId: this.questionnaire.subject.get('subject_id'),
                patientInitials: this.questionnaire.subject.get('initials'),
                userName: LF.security.activeUser.get('username'),
                siteNumber: Data.site.get('siteCode'),
                textClass: 'text-right'
            };

        LF.getStrings(this.templateStrings, (translated) => {
            this.$el.html(LF.templates.display(template, _.extend(translated, dynamicStrings)));
            this.loginInfo = LF.templates.display(loginInfoTemplate, _.extend(translated, dynamicStrings));
            _(this.model.get('answers')).each((answer, i) => {
                LF.getStrings(answer.text, (string) => {
                    let appendOption = ($target) => {
                        return $target.find('.skip-reason-answers')
                            .first()
                            .append(LF.templates.display(input, {
                                id: `${modelId}_${type}_${i}`,
                                reasonText: string,
                                value: answer.value
                            }));
                    };
                    appendOption(this.$el).ready(deferred.resolve());
                }, { namespace: this.parent.parent.id });
            });
        }, { namespace: this.parent.parent.id });
        return deferred.promise;
    }
}

window.LF.Widget.SkipReason = SkipReason;
