import SitepadController from './SitepadController';

import LoginView from '../views/LoginView';
import HomeView from '../views/HomeView';
import DeactivatePatientView from '../views/DeactivatePatientView';
import SiteUsersView from '../views/SiteUsersView';
import FormGatewayView from '../views/FormGatewayView';
import VisitGatewayView from '../views/VisitGatewayView';
import SitePadQuestionnaireView from '../views/SitePadQuestionnaireView';
import SkipVisitQuestionnaireView from '../views/SkipVisitQuestionnaireView';
import EditPatientQuestionnaireView from '../views/EditPatientQuestionnaireView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import SetPasswordView from '../views/SetPasswordView';
import SecretQuestionView from '../views/SecretQuestionView';
import AccountConfiguredView from '../views/AccountConfiguredView';
import UserStatusQuestionnaireView from 'sitepad/views/UserStatusQuestionnaireView';
import NewPatientQuestionnaireView from 'sitepad/views/NewPatientQuestionnaireView';
import AddUserQuestionnaireView from 'sitepad/views/AddUserQuestionnaireView';

import * as utils from 'core/utilities';
import Subject from 'core/models/Subject';
import UniversalLogin from 'core/classes/UniversalLogin';
import BlankView from 'core/views/BlankView';
import Data from 'core/Data';
import Logger from 'core/Logger';

let logger = new Logger('ApplicationController');

export default class ApplicationController extends SitepadController {
    /**
     * Return the appropriate form gateway view for the tablet modality.
     * @returns {View}
     */
    get formGatewayView () {
        return FormGatewayView;
    }

    /**
     * Display the New_User_Sitepad questionnaire.
     */
    addSiteUser () {
        this.authenticateThenGo('AddUserQuestionnaireView', AddUserQuestionnaireView, {
            id: 'New_User_SitePad'
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Displays the Edit_User questionnaire via the UserStatusQuestionnaireView.
     * @param {string} userId - The ID of the user to edit.
     */
    editUser (userId) {
        this.authenticateThenGo('UserStatusQuestionnaireView', UserStatusQuestionnaireView, {
            id: 'Edit_User',
            showCancel: true,

            // The user ID is parsed as an INT so the view can resolve the user data.
            userId: parseInt(userId, 10)
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Displays the Activate_User questionnaire via the UserStatusQuestionnaireView.
     * @param {string} userId - The ID of the user to activate.
     */
    activateUser (userId) {
        this.authenticateThenGo('UserStatusQuestionnaireView', UserStatusQuestionnaireView, {
            id: 'Activate_User',
            showCancel: true,

            // The user ID is parsed as an INT so the view can resolve the user data.
            userId: parseInt(userId, 10)
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Displays the Deactivate_User questionnaire via the UserStatusQuestionnaireView.
     * @param {string} userId - The ID of the user to deactivate.
     */
    deactivateUser (userId) {
        this.authenticateThenGo('UserStatusQuestionnaireView', UserStatusQuestionnaireView, {
            id: 'Deactivate_User',
            showCancel: true,

            // The user ID is parsed as an INT so the view can resolve the user data.
            userId: parseInt(userId, 10)
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the dashboard (FormGatewayView) with no permission check.
     * @param {Object} parameters - Parameters provided by Router.flash().
     * @param {Subject} parameters.subject - The target subject of the dashboard.
     * @param {Visit} parameters.visit - The target visit used to display questionnaires.
     */
    dashboard (parameters) {
        this.dashboardWithPermission(null, parameters);
    }

    /**
     * Display the dashboard (FormGatewayView) pending a permission check.
     * @param {string} permission - The permission object to use. e.g. 'diaryBackoutRoles'
     * @param {Object} parameters - Parameters provided by Router.flash().
     * @param {Subject} parameters.subject - The target subject of the dashboard.
     * @param {Visit} parameters.visit - The target visit used to display questionnaires.
     * @param {Object} [universalLoginOptions={}] options for the UniversalLogin constructor.  Will extend the defaults defined in this method.
     * @param {Object} [newUserLoginOptions={}] options for the new newUserLogin() call.  Will extend the defaults defined in this method.
     * @returns {undefined}
     */
    // eslint-disable-next-line max-params
    dashboardWithPermission (permission, parameters, universalLoginOptions = {}, newUserLoginOptions = {}) {
        let permissionData = utils.getNested(LF, `StudyDesign.sitePad.${permission}`),
            subjectRole = utils.getNested(LF, 'StudyDesign.sitePad.subjectRole'),
            goToView = () => {
                LF.security.checkLogin()
                .then((authenticated) => {
                    if (authenticated) {
                        this.go('FormGatewayView', this.formGatewayView, parameters)
                        .catch(e => logger.error(e))
                        .done();
                    } else {
                        this.navigate('login');
                    }
                })
                .catch(e => logger.error(e))
                .done();
            };

        if (!parameters.subject) {
            return this.navigate('home');
        } else if (!parameters.visit) {
            return this.navigate(`visits/${parameters.subject.get('id')}`);
        }

        if (permission) {
            let nextPermission = permission === subjectRole ? 'diaryBackoutRoles' : subjectRole,
                roleList = ((value) => {
                    return _.isArray(value) ? value : [value != null ? value : subjectRole];
                })(permissionData),
                successfulLogin = () => {
                    goToView();
                },
                filterUsers = (user) => {
                    // Meh, I should think about this... probably a way to do this without clone...
                    let newRoleList = _.clone(roleList),
                        idx = roleList.indexOf(subjectRole);

                    if (idx > -1) {
                        newRoleList.splice(idx, 1);

                        if (subjectRole === user.get('role')) {
                            if (parameters.subject && parameters.subject.get('user') === user.get('id')) {
                                return Q(user);
                            }
                        }
                    }

                    return Q.Promise((resolve) => {
                        if (newRoleList.indexOf(user.get('role')) > -1) {
                            resolve(user);
                        } else {
                            resolve(false);
                        }
                    });
                },
                sortUsers = (users) => {
                    return Q.Promise((resolve) => {
                        let sorted = (_.sortBy(users, (user) => {
                            // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                            return user.get('lastLogin') ? user.get('lastLogin') : new Date(1986, 1, 21).toString();
                        })).reverse();

                        resolve(sorted);
                    });
                },
                backArrow = () => {
                    this.navigate(`dashboard/${nextPermission}`, true, {
                        subject: parameters.subject,
                        visit: parameters.visit
                    });
                };

            let universalLogin = new UniversalLogin(_.extend({
                loginView: LoginView,
                changeTempPasswordView: SetPasswordView,
                resetSecretQuestionView: SecretQuestionView,
                resetPasswordView: SetPasswordView,
                subject: parameters.subject,
                visit: parameters.visit
            }, universalLoginOptions));

            universalLogin.newUserLogin(_.extend({
                successfulLogin,
                filterUsers,
                sortUsers,
                backArrow
            }, newUserLoginOptions));
        } else {
            goToView();
        }

        // for consistent return
        return undefined;
    }


    /**
     * Display the LoginView.
     * @param {Object} [universalLoginOptions={}] options for the UniversalLogin constructor.  Will extend the defaults defined in this method.
     * @param {Object} [newUserLoginOptions={}] options for the new newUserLogin() call.  Will extend the defaults defined in this method.
     */
    login (universalLoginOptions = {}, newUserLoginOptions = {}) {
        let successfulLogin = () => {
                this.navigate('home', true);
            },
            filterUsers = (user) => {
                return Q.Promise((resolve) => {
                    if (LF.StudyDesign.sitePad.loginRoles.indexOf(user.get('role')) > -1) {
                        resolve(user);
                    } else {
                        resolve(false);
                    }
                });
            },
            sortUsers = (users) => {
                return Q.Promise((resolve) => {
                    resolve((_.sortBy(users, (user) => {
                        // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                        return user.get('lastLogin') || new Date(1986, 1, 21).toString();
                    })).reverse());
                });
            };

        let universalLogin = new UniversalLogin(_.extend({
            loginView: LoginView,
            changeTempPasswordView: SetPasswordView,
            resetSecretQuestionView: SecretQuestionView,
            resetPasswordView: SetPasswordView,
            appController: this
        }, universalLoginOptions));

        universalLogin.newUserLogin(_.extend({
            successfulLogin,
            filterUsers,
            sortUsers
        }, newUserLoginOptions));
    }

    /**
     * Display the SitePadQuestionnaireView.
     * @param {string} id - The ID of the questionnaire to display.
     * @param {Object} parameters - Parameters provided by Router.flash().
     * @param {Subject} parameters.subject - The target subject of the questionnaire.
     * @param {Visit} parameters.visit - The visit the questionnaire is being completed under.
     * @returns {Q.Promise<void>}
     */
    questionnaire (id, parameters) {
        Data.Questionnaire = {};

        let viewClassKey = LF.StudyDesign.questionnaires.find({ id }).get('syndicationClass') || 'SitePadQuestionnaireView';

        return this.go(viewClassKey, SitePadQuestionnaireView, {
            id,
            subject: parameters.subject,
            visit: parameters.visit
        })
        .catch(e => logger.error(e));
    }

    /**
     * Display the HomeView.
     */
    home () {
        this.authenticateThenGo('HomeView', HomeView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the VisitGatewayView for a given subject.
     * @param {string} id - The ID of the subject to view visits for.
     * @param {Object} [parameters={}] - Parameters provided by Router.flash().
     * @param {Visit} parameters.visit - The visit to view.
     */
    visitGateway (id, parameters = {}) {
        let subjectId = parseInt(id, 10),
            activeUser = LF.security.activeUser,
            subject = new Subject({ id: subjectId }),
            goToView = () => {
                this.go('VisitGatewayView', VisitGatewayView, { subjectId })
                .catch(e => logger.error(e))
                .done();
            },
            successfulLogin = () => {
                goToView();
            },
            filterUsers = (user) => {
                return Q.Promise((resolve) => {
                    if (LF.StudyDesign.sitePad.visitRoles.indexOf(user.get('role')) > -1) {
                        resolve(user);
                    } else {
                        resolve(false);
                    }
                });
            },
            sortUsers = (users) => {
                return Q.Promise((resolve) => {
                    resolve((_.sortBy(users, (user) => {
                        // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                        return user.get('lastLogin') || new Date(1986, 1, 21).toString();
                    })).reverse());
                });
            },
            backArrow = () => {
                let successfulLogin = () => {
                        this.navigate('dashboard', true, {
                            subject,
                            visit: parameters.visit
                        });
                    },
                    filterUsers = (user) => {
                        return Q.Promise((resolve) => {
                            if (user.get('id') === activeUser.get('id')) {
                                resolve(user);
                            } else {
                                resolve(false);
                            }
                        });
                    },
                    backArrow = () => {
                    // We want to come back to this function and start the logic all over...
                        this.visitGateway(id, parameters);
                    };

                let universalLogin = new UniversalLogin({
                    loginView: LoginView,
                    changeTempPasswordView: SetPasswordView,
                    resetSecretQuestionView: SecretQuestionView,
                    resetPasswordView: SetPasswordView,
                    subject,
                    visit: parameters.visit
                });

                universalLogin.newUserLogin({
                    successfulLogin,
                    filterUsers,
                    backArrow
                });
            };

        subject.fetch()
        .then(() => {
            // Not sure how we want to handle permissions... Im just kind of making something up for now...
            if (LF.StudyDesign.sitePad.visitRoles.indexOf(activeUser.get('role')) > -1) {
                goToView();
            } else {
                let universalLogin = new UniversalLogin({
                    loginView: LoginView,
                    changeTempPasswordView: SetPasswordView,
                    resetSecretQuestionView: SecretQuestionView,
                    resetPasswordView: SetPasswordView,
                    subject,
                    visit: parameters.visit
                });

                universalLogin.newUserLogin({
                    successfulLogin,
                    filterUsers,
                    sortUsers,
                    backArrow
                });
            }
        })
        .done();
    }

    /**
     * Display the EditPatientQuestionnaireView.
     * @param {string} subjectId - The ID of the subject to edit.
     */
    editPatient (subjectId) {
        this.authenticateThenGo('EditPatientQuestionnaireView', EditPatientQuestionnaireView, {
            id: 'Edit_Patient',
            showCancel: true,
            showLoginInfo: true,
            subjectId
        })
        .catch(e => logger.error(e))
        .done();
    }

    // @todo - Proof of concept.
    deactivatePatient (subject) {
        this.go('DeactivatePatientView', DeactivatePatientView, { subject })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the SiteUsersView.
     */
    siteUsers () {
        this.authenticateThenGo('SiteUsersView', SiteUsersView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the NewPatientQuestionnaireView.
     */
    addNewPatient () {
        this.authenticateThenGo('NewPatientQuestionnaireView', NewPatientQuestionnaireView, {
            id: 'New_Patient',
            showCancel: true
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the SkipVisitQuestionnaireView.
     * @param {Object} parameters - Parameters provided by Router.flash().
     * @param {Subject} parameters.subject - The subject to skip visits for.
     * @param {Visit} parameters.visit - The visit to skip.
     */
    skipVisits (parameters) {
        this.authenticateThenGo('SkipVisitQuestionnaireView', SkipVisitQuestionnaireView, {
            id: 'Skip_Visits',
            subject: parameters.subject,
            visit: parameters.visit,
            showCancel: true,
            cancelPopupStyle: 'skipVisit'
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the QuestionnaireCompletionView.
     */
    questionnaireCompletion () {
        this.go('QuestionnaireCompletionView', QuestionnaireCompletionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the SetPasswordView.
     */
    setPassword () {
        this.go('SetPasswordView', SetPasswordView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the SecretQuestionView.
     */
    setSecretQuestion () {
        this.go('SecretQuestionView', SecretQuestionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the AccountConfiguredView.
     */
    accountConfigured () {
        this.go('AccountConfiguredView', AccountConfiguredView)
        .catch(e => logger.error(e))
        .done();
    }


    /**
     * Display the Time_Confirmation questionnaire.
     */
    timeConfirmation () {
        this.go('BaseQuestionnaireView', BaseQuestionnaireView, {
            id: 'Time_Confirmation'
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the Blank page.
     */
    blank () {
        this.go('BlankView', BlankView)
        .catch(e => logger.error(e))
        .done();
    }
}
