import RowView from './RowView';
import UserVisits from '../collections/UserVisits.js';
import * as dateTimeUtil from 'core/DateTimeUtil';

/**
 * A row in the Subject table.
 * @class SubjectRowView
 * @extends RowView
 */
export default class SubjectRowView extends RowView {
    constructor (options) {
        super(options);

        /**
         * @property {Function} template - The template to render.
         */
        this.template = _.template('<td>{{ subject_id }}</td><td>{{ initials }}</td><td>{{ lastVisit }}</td><td>{{ phase }}</td>');

        /**
         * @property {Object<string,string>} strings - Translated strings used to display Phase.
         */
        this.strings = options.strings;
    }

    /*
     * Render the view
     * @return {Q.Promise<SubjectRowView>}
     */
    render () {
        return this.getLastCompletedVisit(this.model.get('krpt'))
        .then((lastVisit) => {
            this.$el.html(this.template({
                subject_id: this.model.get('subject_id'),
                initials: this.model.get('initials') || '---',
                phase: this.strings[this.model.get('phase')],
                lastVisit
            }));

            return super.render();
        });
    }

    /*
     * Get the name of the visit most recently completed by the user
     * @param {string} subjectKrpt - The krpt of the subject
     */
    getLastCompletedVisit (subjectKrpt) {
        let lastVisit,
            userVisits = new UserVisits();

        return userVisits.fetch({
            search: {
                where: {
                    subjectKrpt,
                    state: 'Completed'
                }
            }
        })
        .then(() => {
            lastVisit = userVisits.models.length ? userVisits.max(o => dateTimeUtil.parseDateTimeIsoNoOffset(o.get('dateModified')).getTime()) : false;
            return lastVisit ? this.strings[lastVisit.get('visitId')] : '---';
        });
    }
}
