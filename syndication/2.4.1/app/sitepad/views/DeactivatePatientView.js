import PageView from 'core/views/PageView';

// TODO: This is dead code.

export default class DeactivatePatientView extends PageView {
    constructor (options) {
        super(options);

        this.subject = options.subect;
        this.template = '#deactivate-patient-template';
        this.events = {
            'click #settings': 'settings',
            'click #cancel': 'goBack',
            'click #next': 'next',
            'click #back': 'goBack',
            'click tr': 'select'
        };
    }

    get id () {
        return 'deactivate-patient';
    }

    render () {
        let template = this.getTemplate();

        this.$el.html(template({
            title: 'Deactivate Patient',
            fName: 'Emily',
            lName: 'Kauffman',
            role: 'Site Coordinator',
            siteNumber: '001321 (Biogen)',
            cancel: 'Cancel',
            next: 'Next',
            date: '24 MAR 2015',
            status: 'Active',
            id: '001',
            initials: 'ESK'
        }));

        this.page();

        return this;
    }

    // Goes to settings
    settings () {
        this.navigate('settings', true);
    }

    // Goes back a screen
    goBack () {
        this.navigate('home', true);
    }

    // Goes forward to affidavit
    next () {
        this.navigate('sign-affidavit', true);
    }

    // Enables selection of table rows
    select (evt) {
        $(evt.currentTarget).toggleClass('selected').siblings().removeClass('selected');
    }
}
