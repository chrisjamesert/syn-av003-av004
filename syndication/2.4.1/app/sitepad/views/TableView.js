export default class TableView extends Backbone.View {
    constructor (options = {}) {
        super(options);

        /** @property {string} order - The order of which rows should be ordered. */
        this.order = 'ASC';

        /** @property {Object} options - The options passed to the view. */
        this.options = options;
    }

    /**
     * @property {string} tagName - The root element of the view.
     * @default 'div'
     */
    get tagName () {
        return 'div';
    }

    /**
     * @property {string} className - The class of the view.
     */
    get className () {
        return 'table-view';
    }

    /**
     * Clear out the table body and display a spinner.
     */
    displaySpinner () {
        let template = `<tr>
            <td colspan="${this.options.rows.length}" class="text-center">
                <h1 class="fa fa-refresh fa-spin"></h1>
            </td>
        </tr>`;

        this.$('tbody').html(template);
    }

    /**
     * Fetch and render the table rows.
     * @returns {Q.Promise<void>}
     */
    fetch () {
        this.displaySpinner();

        this.collection.reset();

        return this.collection.fetch()
        .then(() => {
            if (this.options.filter != null) {
                this.collection.reset(this.options.filter(this.collection));
            }
        })
        .then(() => this.renderRows());
    }

    /**
     * Render the table's header.
     * @returns {Q.Promise<void>}
     */
    renderHeader () {
        let $row = $(document.createElement('tr'));

        // Determine the width of the row.
        // @todo We might want to support custom widths via configuration
        let rowWidth = 100 / this.options.rows.length;

        // Start a underscore chain, with a promise.
        return _(this.options.rows).chain()
            .reduce((chain, row) => {
                return chain.then(() => {
                    let $th = $(document.createElement('th'));

                    $th.width(`${rowWidth}%`);

                    // Add an ID to the header cell. ex. th-PHASE
                    $th.attr('id', `th-${row.header}`);

                    return this.i18n(row.header)
                    .then((translation) => {
                        // Remove any colons from translated string.
                        translation = translation && translation.replace(/:/g, '');

                        $th.append(translation);

                        // If the row is sortable, add a click event for sorting.
                        if (row.sortable) {
                            if (row.property !== this.collection.sortProperty) {
                                $th.append('<span class="fa fa-sort"></span>');
                            } else {
                                $th.append(`<span class="fa ${this.order === 'ASC' ? 'fa-caret-up' : 'fa-caret-down'}"></span>`);
                            }

                            $th.click((evt) => {
                                this.sort(evt, row);
                            });
                        }

                        $row.append($th);

                        $th = null;
                    });
                });
            }, Q()).value()
            .then(() => {
                this.$('thead').append($row);
            });
    }

    /**
     * Sort the table by model property.
     * @param {(MouseEvent|TouchEvent)} evt - A jQuery provided click event.
     * @param {Object} row - The model property to sort by.
     * @returns {Q.Promise<void>}
     */
    sort (evt, row) {
        return Q.Promise((resolve) => {
            let $target = this.$(evt.target);

            // DE16789 - Clicking the icon removes it from the header.
            // If the target is a <span>, we need to grab the parent <th> element instead...
            // $(selector).prop('tagName') === evt.target.nodeName
            if ($target.prop('tagName') === 'SPAN') {
                $target = $target.parent('th');
            }

            // Trigger a deselect event and disable sorting.
            this.deselect();
            this.disableSorting();

            // If the comparator hasn't changed...
            if (row.property === this.collection.sortProperty) {
                // Just change the sort order.
                this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
            } else {
                // Otherwise, reassign the comparator and sort order.
                this.collection.comparator = row.comparator || row.property;
                this.collection.sortProperty = row.property;
                this.order = 'ASC';
            }

            // Remove any carets from other header cells.
            this.$('th span.fa').remove();

            this.options.rows.forEach((row) => {
                if (row.sortable && row.property !== this.collection.sortProperty) {
                    this.$(`#th-${row.header}`).append('<span class="fa fa-sort"></span>');
                }
            });

            $target.find('span').remove();

            // Remove the fa-sort span from the target, and add the appropriate caret icon.
            $target.append(`<span class="fa ${this.order === 'ASC' ? 'fa-caret-up' : 'fa-caret-down'}"></span>`);

            this.displaySpinner();
            $target = null;

            this.renderRows()
            .then(() => this.enableSorting())

            // We don't really care if this fails or succeeds here, so we just resolve.
            .done(resolve);
        });
    }

    /**
     * Disable sorting on the table.
     */
    disableSorting () {
        this.options.rows.forEach((row) => {
            this.$(`#th-${row.header}`).off();
        });
    }

    /**
     * Enable sorting on the table.
     */
    enableSorting () {
        this.options.rows.forEach((row) => {
            if (row.sortable) {
                this.$(`#th-${row.header}`).click((evt) => {
                    this.sort(evt, row);
                });
            }
        });
    }

    /**
     * Render the tables rows.
     * @returns {Q.Promise<void>}
     */
    renderRows () {
        return Q.Promise((resolve) => {
            let rows = [],
                rowWidth = 100 / this.options.rows.length;

            // Start by sorting the collection.
            this.collection.sort();

            let chain = this.collection.chain();

            // Reverse the order of the collection if needed.
            if (this.order === 'DESC') {
                chain.reverse();
            }

            chain.reduce((chain, model) => {
                return chain.then(() => {
                    // Render the row, then push it into the array of rows.
                    return this.renderRow(model)
                    .then((row) => {
                        if (row.childElementCount) {
                            let $row = $(row);

                            // For each cell in a row, adjust the width.
                            $row.children().each((i, v) => {
                                $(v).width(`${rowWidth}%`);
                            });

                            rows.push(row);
                        }
                    });
                });
            }, Q()).value()
            .then(() => {
                // Clear out the body of the table, then append each row.
                this.$('tbody').html('');
                rows.forEach((row) => {
                    this.$('tbody').append(row);
                });
            })
            .done(resolve);
        });
    }

    /**
     * A placeholder for child classes to override.
     * @returns {Q.Promise<void>}
     */
    renderRow () {
        return Q();
    }

    /**
     * Render the table
     * @returns {Q.Promise<void>}
     */
    render () {
        // Clear out the table's HTML.
        this.$el.html(
            `<table>
                <thead></thead>
            </table>
            <table class="table scrollable">
                <tbody></tbody>
            </table>`
        );

        return this.renderHeader();
    }

    /**
     * Trigger a select event on the table.
     * @param {Object} model - The model of the row selected.
     */
    select (model) {
        this.selected = model;

        this.trigger('select', model);
    }

    /**
     * Trigger a deselect event on the table.
     * @param {Object} model - The model of the row deselected.
     */
    deselect (model) {
        this.selected = null;

        this.trigger('deselect', model);
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {Object|string} strings - The translations keys to translate.
     * @param {Function} [callback=$.noop] - A callback function invoked when the strings have been translated.
     * @param {Object} options - Options passed into the translation.
     * @returns {Q.Promise}
     */
    i18n (strings, callback = $.noop, options = undefined) {
        return LF.strings.display.call(LF.strings, strings, callback, options);
    }
}
