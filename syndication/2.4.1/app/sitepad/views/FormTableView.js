import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Schedules from 'core/collections/Schedules';
import Roles from 'core/collections/Roles';
import TableView from 'sitepad/views/TableView';
import FormRowView from 'sitepad/views/FormRowView';
import FormRows from 'sitepad/collections/FormRows';
import UserReports from 'sitepad/collections/UserReports';


const logger = new Logger('FormTableView');

/**
 * Displays available forms for a subject's visit.
 * @class FormTableView
 * @extends TableView
 */
export default class FormTableView extends TableView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {Subject} subject - The subject to display forms for.
         */
        this.subject = options.subject;

        /**
         * @property {Visit} visit - The visit to display forms for.
         */
        this.visit = options.visit;

        /**
         * @property {FormRows} collection - The collection to fetch and render.
         */
        this.collection = new FormRows();

        let row = options.rows[0];

        // Set the default sort property and comparator.
        this.collection.sortProperty = row.property;
        this.collection.comparator = row.comparator || row.property;
    }

    /**
     * Fetch all rows to render to the table.
     * @returns {Q.Promise<void>}
     */
    fetch () {
        return Q.Promise((resolve, reject) => {
            let visitQuestionnaires = [],
                visit = this.visit.toJSON(),
                subject = this.subject,
                userIsNotSubject = LF.security.activeUser.get('role') !== 'subject';

            let determineSchedules = () => {
                let formSchedules = new Schedules();

                // Loop through each form...
                visit.forms.forEach((form) => {
                    let targetQuestionnaire = LF.StudyDesign.questionnaires.filter((questionnaire) => {
                        return questionnaire.get('id') === form;
                    });

                    // If the desired form is found in study design...
                    if (targetQuestionnaire) {
                        // Add it to the array of questionnaires available to the visit.
                        visitQuestionnaires.push(targetQuestionnaire);

                        // get only schedules for this form and for current visit
                        // 1. schedule's target form id is this form id
                        // 2. schedule's target visit id is current visit id
                        let targetSchedule = LF.StudyDesign.schedules.filter((schedule) => {
                            return schedule.get('target').id === form;
                        });

                        // If there an associated schedule is found in the study design...
                        if (targetSchedule) {
                            // Add it to the array of form schedules.
                            formSchedules.add(targetSchedule, { merge: true });
                        }
                    }
                });

                return Q.Promise((resolve) => {
                    LF.schedule.determineSchedules(formSchedules, {
                        subject,
                        visit: this.visit
                    }, (available) => {
                        LF.schedule.availableSchedules.set(available);
                        resolve();
                    });
                });
            };

            // Fetch all stored user reports and return the populated collection.
            let fetchUserReports = () => {
                return UserReports.fetchCollection();
            };

            let processQuestionnaires = (userReports) => {
                let processQuestonnaire = (questionnaire, index) => {
                    return Q.Promise((resolve) => {
                        // By default, each questionnaire is not available, unless proven otherwise.
                        let status = LF.QuestionnaireStates.NOT_AVAILABLE;

                        // 1. Ask scheduling if report is available for completion
                        if (LF.schedule.availableSchedules.find(schedule => schedule.get('target').id === questionnaire.get('id'))) {
                            status = LF.QuestionnaireStates.AVAILABLE;

                            // 2. If report is available, we need to check its predecessor
                            // @todo check report predecessor
                        } else {
                            // 3. If report is not available,
                            // If report is in database, for current visit, it is completed
                            let userReport = userReports.findWhere({
                                user_visit_id: this.visit.id.toString(),
                                questionnaire_id: questionnaire.get('id').toString(),
                                subject_krpt: this.subject.get('krpt')
                            });

                            if (userReport != null) {
                                status = userReport.get('state');
                            }
                        }

                        let accessRoles = questionnaire.get('accessRoles') || ['subject'];
                        let roles = new Roles();

                        // Loop through the roles allowed to access the questionnaire...
                        accessRoles.forEach((roleId) => {
                            // Filter out the allowable roles from the study design...
                            let matched = LF.StudyDesign.roles.filter(role => role.get('id') === roleId);

                            // ...and then add them to the collection.
                            roles.add(matched);
                        });

                        // If the user is not a subject or the subject role is allowed to complete the questionnaire...
                        if (userIsNotSubject || accessRoles.indexOf('subject') !== -1) {
                            ELF.trigger(`DASHBOARD:DisplayQuestionnaire/${questionnaire.get('id')}`, {
                                questionnaire,
                                subject
                            }, this)
                            .then((res) => {
                                if (res.preventDefault) {
                                    return;
                                }

                                // for now, do not display unavailable schedules
                                if (status !== LF.QuestionnaireStates.NOT_AVAILABLE) {
                                    let { id, displayName } = questionnaire.toJSON();

                                    this.collection.add({
                                        id,
                                        roles,
                                        displayName,
                                        status,
                                        allowTranscriptionMode: questionnaire.get('allowTranscriptionMode'),
                                        subject: this.subject,
                                        order: index + 1
                                    });
                                }
                            })
                            .done(resolve);
                        } else {
                            resolve();
                        }
                    });
                };

                return _.flatten(visitQuestionnaires, true).reduce((promise, questionnaire, index) => {
                    return promise.then(() => processQuestonnaire(questionnaire, index));
                }, Q());
            };

            // If the visit has forms...
            if (visit.forms) {
                determineSchedules()
                .then(() => this.checkReportCompletion())
                // eslint-disable-next-line consistent-return
                .then((formsAvailable) => {
                    if (formsAvailable) {
                        this.trigger('renderRows');
                        return fetchUserReports()
                        .then(processQuestionnaires)
                        .then(() => this.renderRows());
                    }
                    this.trigger('noFormsAvailable');
                })
                .then(resolve)
                .catch(reject)
                .done();
            } else {
                logger.trace('No forms assigned to target visit.');
                resolve();
            }
        });
    }

    /**
     * Checks if all reports are completed.
     * @returns {boolean} True if forms are available, false if not.
     * @example this.checkReportCompletion();
     */
    checkReportCompletion () {
        // If all reports are completed the code won't ever hit here.
        // So let's only check report completion for subjects
        if (!LF.security.activeUser.isSubjectUser()) {
            return true;
        }

        // This gives us the number of possible subject reports in current phase for current visit
        let lstAvailableSchedulesForSubjectInCurrentPhase = LF.schedule.availableSchedules.filter((formSchedule) => {
            return LF.StudyDesign.questionnaires.find((questionnaire) => {
                let accessRoles = questionnaire.get('accessRoles') || ['subject'];

                // If the questionnaire is scheduled, and is available for the subject.
                return (questionnaire.get('id') === formSchedule.get('target').id) && _.contains(accessRoles, 'subject');
            });
        });

        let noOfPossibleSubjectReports = lstAvailableSchedulesForSubjectInCurrentPhase.length;

        // If the number of completed reports is greater or equal to the number of subject reports...
        if (noOfPossibleSubjectReports !== 0) {
            return true;
        }

        return false;
    }

    renderRow (model) {
        let view = new FormRowView({
            model,
            strings: this.options.translations
        });

        this.listenTo(view, 'select', this.select);
        this.listenTo(view, 'deselect', this.deselect);

        return view.render()
        .then(() => view.el);
    }
}
