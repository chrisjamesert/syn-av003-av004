import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import CoreLogin from 'core/views/LoginView';
import { makeWindowsNumericInputCover } from 'core/utilities/coreUtilities';

const logger = new Logger('ContextSwitchingView');

export default class ContextSwitchingView extends CoreLogin {
    constructor (options) {
        super(options);

        logger.traceEnter('Constructor');

        /**
         * @property {string} viewTriggerName - trigger named used by ELF
         */
        this.viewTriggerName = 'CONTEXTSWITCH';

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = _.defaults({ login: 'LOGIN' }, this.templateStrings);

        logger.traceExit('Constructor');
    }

    /**
     * Get the ID.
     * @property {string} id - The ID of the view.
     * @default 'context-switching-view'
     */
    get id () {
        return 'context-switching-view';
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<ContextSwitchingView>} promise resolved when render is complete
     */
    render () {
        logger.traceEnter('render');
        let inputType = this.getRoleInputType(this.user.get('role'));
        return this.buildHTML({
            inputType
        }, true)
        .then(() => {
            return this.userLoginSetup();
        })
        .finally(() => {
            if (LF.Wrapper.platform === 'windows' && inputType === 'number') {
                makeWindowsNumericInputCover('password', this.$password.parent().parent());
            }
            LF.spinner.hide();

            lStorage.removeItem('isAuthorized');
            LF.security.logout();

            logger.traceExit('render');

            return this;
        });
    }
}

COOL.add('ContextSwitchingView', ContextSwitchingView);
