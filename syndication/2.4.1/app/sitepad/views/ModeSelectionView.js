import PageView from 'core/views/PageView';
import * as lStorage from 'core/lStorage';
import * as utils from 'core/utilities';
import State from 'sitepad/classes/State';
import Environment from 'core/models/Environment';
import Logger from 'core/Logger';
import ELF from 'core/ELF';

let logger = new Logger('ModeSelectionView');
const _evt = { preventDefault: $.noop };

/*
 * @class ModeSelectionView
 * @description
 * When the device is first booted, the user must select which mode to run the device in:
 * - Trainer: This device is used to train end-users.  No external HTTP requests are made.
 * - Depot: The site is no known. The device is shipped and the site must be selected by the end-user.
 * - Production: The device and site are configured by ERT.
 */
export default class ModeSelectionView extends PageView {
    /**
     * @constructor
     * @param {Object} options - Any optional data to be passed into the view.
     */
    constructor (options = {}) {
        super(options);

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': 'nextHandler',
            'change #mode': 'modeHandler',
            'change #environment': 'environmentHandler'
        };

        // This view is only ever viewed by Production Engineers, so we don't need translations.
        // However, we are pulling the app name, in case it changes...

        /**
         * @property {Object} templateStrings - A list of keys to translated to be rendered to the view.
         */
        this.templateStrings = { header: 'APPLICATION_HEADER' };

        lStorage.setItem('isActivation', true);

        utils.setLanguage();
    }

    /**
     * @property {string} id - The ID of the view.
     * @default 'mode-selection-view'
     */
    get id () {
        return 'mode-selection-view';
    }

    /**
     * @property {string} template - The selector of the template to bind to the view.
     * @readonly
     * @default '#mode-selection-tpl'
     */
    get template () {
        return '#mode-selection-tpl';
    }

    /**
     * @property {Object} selectors - A list of selector aliases to populate.
     */
    get selectors () {
        return {
            mode: '#mode',
            environment: '#environment',
            custom: '#custom',
            customStudyDbName: '#customStudyDbName',
            description: '#description',
            navbar: '.navbar',
            next: '#next'
        };
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     * @example
     * this.render()
     * .then(() => { ... });
     */
    render () {
        return this.buildHTML({
            screenshot: 'Screenshot',
            tagline: 'Select a mode and environment for this device.',
            trainer: 'Trainer',
            provision: 'Active',
            depot: 'Depot',
            next: 'Next',
            mode: 'Mode',
            environment: 'Environment',
            custom: 'Custom Environment',
            customStudyDbName: 'Custom Study Name'
        }, true)
        .then(() => {
            this.$mode.select2({ minimumResultsForSearch: Infinity });

            this.init();
        });
    }

    /**
     * Initialize mode and environment
     * @example
     * this.init();
     */
    init () {
        let mode = lStorage.getItem('mode');

        // If the mode has previously been selected...
        if (mode) {
            let environment = lStorage.getItem('environment');

            // We undelegate all events here so the .trigger()'s below don't
            // fire events to save the mode and environment.
            this.undelegateEvents();

            // Set the mode and trigger a change event so the value displays in the UI.
            this.$mode.val(mode).trigger('change');
            this.modeChanged(false);

            // Set the environment and trigger a change event.
            this.$environment.val(environment).trigger('change');
            this.environmentChanged(false);

            // If a custom environment has been selected...
            if (environment === 'custom') {
                let custom = lStorage.getItem('serviceBase');
                let customStudyDbName = lStorage.getItem('studyDbName');

                this.$custom.val(custom);
                this.$customStudyDbName.val(customStudyDbName);
            }

            this.delegateEvents();
        } else {
            this.modeChanged();
        }
    }

    /**
     * Renders a list of available environments per mode.
     * @example
     * this.renderEnvironments();
     */
    renderEnvironments () {
        let mode = this.$mode.val(),
            environments = LF.StudyDesign.environments.filter((environment) => {
                return environment.get('modes').indexOf(mode) !== -1;
            }),
            template = _.template('<option value="{{ id }}">{{ label }}</option>');

        // Clear out any existing environments.
        this.$environment.html('<option disabled selected> -- Select an environment -- </option>');

        let disableCustomEnvironments = LF.StudyDesign.disableCustomEnvironments !== undefined ?
            LF.StudyDesign.disableCustomEnvironments :
            LF.CoreSettings.disableCustomEnvironments;

        // Add custom environment option if the custom environment usage is not disabled
        if (!disableCustomEnvironments) {
            // Append the option for a custom environment at the bottom of the list.
            environments.push(new Environment({
                id: 'custom',
                label: 'Custom environment...'
            }));
        }

        _.each(environments, (environment) => {
            let el = template({
                id: environment.get('id'),
                label: environment.get('label')
            });

            this.$environment.append(el);
        });

        this.$environment.select2({ minimumResultsForSearch: Infinity });
    }

    /**
     * Event handler for a change in the selected mode.
     */
    modeHandler () {
        this.modeChanged(true);
    }

    /**
     * Event logic when the mode has changed.
     * @param {boolean} [store=true] - Determines if the change should be persisted to localStorage.
     * @example
     * this.modeChanged();
     */
    modeChanged (store = true) {
        let mode = this.$mode.val(),
            environment = this.$environment.parent(),
            custom = this.$custom.parent(),
            description;

        switch (mode) {
            case 'provision':
                // The site will be selected by a Production Engineer.
                description = 'The device is registered by manufacturing.';
                this.showElement(environment);
                this.renderEnvironments();
                break;

            // DE16957 https://rally1.rallydev.com/#/36503486296ud/detail/defect/59556929940
            // This defect renders this case unreachable. Commenting this out until Depot mode is supported.
            // The site will be selected by a Site Administrator.
            // case 'depot':
            //     description = 'The target site for this device is unknown.  The device will be shipped to the client to complete device registration.';
            //     this.showElement(environment);
            //     this.renderEnvironments();
            //     break;

            case 'screenshot':
                description = 'Take screenshots';

                // DE17491 - Custom fields are still displayed after mode change to screenshot/trainer.
                // The environment needs to be cleared, otherwise logic in the environmentChanged() method
                // reads the environment as 'custom'
                this.$environment.val('');

                this.hideElement(environment);
                this.hideElement(custom);

                this.enableButton(this.$next);
                break;

            // The device will be used for training...
            case 'trainer':
                description = 'The device will be used for training.';
                this.hideElement(environment);

                // DE17491 - Custom fields are still displayed after mode change to screenshot/trainer.
                // The environment needs to be cleared, otherwise logic in the environmentChanged() method
                // reads the environment as 'custom'
                this.$environment.val('');

                // DE14016 - Custom environment is displayed after mode is changed to 'Trainer'.
                this.hideElement(custom);
                this.enableButton(this.$next);
                break;
            default:
                break;
        }

        // Add the mode's description to the UI.
        this.$description.html(description);

        if (store) {
            this.setModeToLocalStorage();
        }

        this.environmentChanged(store);

        // Clean reference to DOM fragment.
        environment = null;
        custom = null;
    }

    /**
     * Event handler for a change in the selected mode.
     */
    environmentHandler () {
        this.environmentChanged(true);
    }

    /**
     * Event logic for when the environment has changed.
     * @param {boolean} [store=true] - Determines if the change should be persisted to localStorage.
     * @example
     * this.environmentChanged();
     */
    environmentChanged (store = true) {
        let environment = this.$environment.val(),
            mode = this.$mode.val(),
            $parent = this.$custom.parent();

        // Clear out any previous input.
        this.$custom.val('');

        // DE14149 - Custom Study Name is remembered in case mode is changed.
        this.$customStudyDbName.val('');

        if (environment == null && mode !== 'trainer' && mode !== 'screenshot') {
            this.hideElement($parent);
            this.disableButton(this.$next);
        } else if (environment === 'custom') {
            this.showElement($parent);

            // DE14021 - Next button was disabled when custom environment was selected.
            this.enableButton(this.$next);
        } else {
            this.hideElement($parent);
            this.enableButton(this.$next);
        }

        if (store) {
            this.setEnvironmentToLocalStorage();
        }

        // Clean reference to DOM fragment.
        $parent = null;
    }

    /**
     * Click event handler for the Next button.
     * @param {(MouseEvent|TouchEvent)} [evt] - A click event object.
     * @example
     * this.nextHandler(evt);
     */
    nextHandler (evt = _evt) {
        evt.preventDefault();

        // We're using a handler to invoke ModeSelectionView.next() so .done() can be called on
        // the returned promise.  This prevents any errors from being swallowed
        // and the error 'Detected 1 unhandled rejections of Q promises' from being thrown.
        this.next()
        .done();
    }

    /**
     * Navigate to the next screen.
     * @returns {Q.Promise<void>}
     * @example
     * this.next()
     * .then(() => { ... });
     */
    next () {
        let mode = this.$mode.val(),
            environment = this.$environment.val();

        const goNext = () => {
            switch (mode) {
                case 'provision':
                case 'trainer':
                    // Continue to setup the device in manufacturing...
                    State.set(State.states.siteSelection);
                    this.navigate('siteSelection');
                    break;

                // No default
            }
        };

        // DE17460 - If the environment is set to custom, setEnvironmentToLocalStorage needs to be manually invoked
        // to ensure the entered studyDbName is stored.
        if (environment === 'custom') {
            this.setEnvironmentToLocalStorage();
        }

        return ELF.trigger('MODESELECT:Submit', { value: mode, product: 'sitepad' }, this)

        // Don't really need a return here, since no value is expected.
        // eslint-disable-next-line consistent-return
        .then((res) => {
            if (!res.preventDefault) {
                return goNext();
            }
        })
        .catch((err) => {
            logger.error('Error changing mode', err);
        });
    }

    /**
     * Sets environment to local storage.
     * @example
     * this.setEnvironmentToLocalStorage();
     */
    setEnvironmentToLocalStorage () {
        let selectedEnvironment = this.$environment.val(),
            environment = _.findWhere(LF.StudyDesign.environments.toJSON(), { id: selectedEnvironment });

        if (selectedEnvironment == null) {
            lStorage.removeItem('environment');
            lStorage.removeItem('studyDbName');
            return;
        }

        if (environment != null) {
            lStorage.setItem('environment', environment.id);
            utils.setServiceBase(environment.url);
            lStorage.setItem('studyDbName', environment.studyDbName);
        } else {
            lStorage.setItem('environment', 'custom');
            utils.setServiceBase(this.$custom.val());
            lStorage.setItem('studyDbName', this.$customStudyDbName.val());
        }
    }

    /**
     * Sets the provided mode to localStorage.
     * @example
     * this.setModeToLocalStorage();
     */
    setModeToLocalStorage () {
        let mode = this.$mode.val();

        lStorage.setItem('mode', mode);
    }
}
