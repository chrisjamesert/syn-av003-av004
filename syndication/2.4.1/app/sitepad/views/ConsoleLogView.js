import PageView from 'core/views/PageView';
import COOL from 'core/COOL';
import Logs from 'core/collections/Logs';
import * as Utilities from 'core/utilities';

export default class ConsoleLogView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} id - The ID of template to render.
         * @readonly
         * @default '#console-log-tpl'
         */
        this.template = '#console-log-tpl';

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back'
        };

        /**
         * @property {Object} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            title: 'CONSOLE_LOG',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'console-log'
     */
    get id () {
        return 'console-log';
    }

    /**
     *
     * @param logs - Array of log objects
     * @param container - Data table target container
     * @returns Data Table with Log Data
     */

    logViewer (logs, container) {
        let $containerFluid = $('#console-log > .container-fluid'),
            dynamicContainerHeight = (containerFluid) => {
                let $navBar = $('#console-log > nav.navbar'),
                    $navBarHeight = $navBar.height(),
                    $navBarMarginBottom = $navBar.css('margin-bottom'),
                    $navBarMarginBottomValue = parseInt($navBarMarginBottom, 10),
                    $consoleLogContainerFluidHeight = $(window).height() - $navBarHeight - $navBarMarginBottomValue;

                containerFluid.height($consoleLogContainerFluidHeight);
            };

        $(window).resize(() => {
            dynamicContainerHeight($containerFluid);
        });

        logs.map((log) => {
            let logDate = new Date(log.attributes.clientTime);
            let formattedDate = moment(logDate).format('DDMMMYYYY, HH:mm:ss');
            let message = log.attributes.message;
            let id = log.attributes.id;
            let level = log.attributes.level;
            let loc = log.attributes.location;
            let row = `<tr>
                            <td>${id}</td>
                            <td>${formattedDate}</td>
                            <td>${level}</td>
                            <td>${loc}</td>
                            <td>${message}</td>
                          </tr>`;
            return container.append(row);
        });

        container.DataTable({
            dom: '<"pull-left"l><"#table-search-buttons.pull-right"f<"pull-right"B>r><"clearfix">t<"pull-left"i>p',
            order: [[1, 'desc']], // Order by date descending
            language: {
                lengthMenu: 'Display _MENU_ log entries',
                zeroRecords: 'No logs were found',
                info: 'Showing _START_ of _END_ log entries',
                infoEmpty: 'No logs available',
                infoFiltered: '(filtered from _MAX_ total log entries)'
            },
            buttons: [
                // The Export Excel is not working on Android/mobile devices and will need to
                // be postponed, until I can create a fix.
                // { extend: 'excelHtml5', text: '<i class="fa fa-lg fa-file-excel-o"></i>' },
                { extend: 'colvis', text: '<i class="fa fa-lg fa-columns"></i>' }
            ],
            fixedHeader: true,
            responsive: true
        });

        /*
         * Handle closing the column visibility modal when navigating
         * outside page, usually on session timeout.
         */
        $(window).one('hashchange', () => {
            let dtBackground = $('.dt-button-background');
            if (dtBackground.length > 0) {
                dtBackground.click();
            }
        });
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        Utilities.setLanguage('en-US', 'ltr');
        return this.buildHTML({}, true)
            .then(() => Logs.fetchCollection())
            .then((logs) => {
                let container = $('#user-console-log');
                this.logViewer(logs, container);
            });
    }

    /**
     * Navigates back to the settings view.
     */
    back () {
        Utilities.setLanguage(`${LF.Preferred.language}-${LF.Preferred.locale}`,
            LF.strings.getLanguageDirection());
        this.navigate('settings');
    }
}
COOL.add('ConsoleLogView', ConsoleLogView);
