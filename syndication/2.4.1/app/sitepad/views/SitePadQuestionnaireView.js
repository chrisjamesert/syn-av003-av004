/** #depends Page.js
 * @file Defines the SitePadQuestionnaireView view.
 * @author <a href="mailto:dimitar.vukman@ert.com">Dimitar Vukman</a>
 * @version 1.0
 */
import QuestionnaireView from 'core/views/QuestionnaireView';
import Logger from 'core/Logger';
import UserReport from '../models/UserReport.js';
import UserVisit from '../models/UserVisit.js';
import UserReports from '../collections/UserReports.js';
import UserVisits from '../collections/UserVisits.js';
import NotifyView from 'core/views/NotifyView';
import Schedules from 'core/collections/Schedules';
import * as utils from 'core/utilities';
import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import Dashboard from 'core/models/Dashboard';
import Answer from 'core/models/Answer';
import Transmission from 'core/models/Transmission';
import { isVisitExpired } from 'sitepad/visits/evaluateVisit';
import { createEndVisitReport, createEventAtEntryVariables } from 'sitepad/visits/visitUtils';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';


let logger = new Logger('SitePadQuestionnaireView');

export default class SitePadQuestionnaireView extends QuestionnaireView {
    constructor (options) {
        this.notify = new NotifyView();
        this.visit = options.visit;
        super(options);
    }

    /**
     * Saves the user report to UserReport table.
     * @returns {Q.Promise}
     */
    saveUserReport () {
        let questionnaireID = this.id.toString(),
            visitID = this.visit.id.toString(),
            subjectKrpt = this.subject.get('krpt'),
            state = LF.QuestionnaireStates.COMPLETED,
            dateTime = new Date(),
            sigId = this.data.dashboard.get('sig_id'),
            userReport;

        userReport = new UserReport({
            questionnaire_id: questionnaireID,
            user_visit_id: visitID,
            subject_krpt: subjectKrpt,
            state,
            date_recorded: DateTimeUtil.timeStamp(dateTime),
            phase: this.previousPhase,
            sig_id: sigId
        });

        return userReport.save()
        .then(() => {
            logger.debug('saved user report.');
        })
        .catch((err) => {
            logger.error('saveUserReport() failed', err);
        });
    }

    /**
     * If all questionnaires for this visit are completed or skipped,
     * visit state will be set to completed, otherwise in progress. A
     * new entry will be made to User Visits table.
     * @returns {Q.Promise}
     */
    setVisitState () {
        let deferred = Q.defer(),
            forms = this.visit.get('forms'),
            userReports = new UserReports(),
            visitId = this.visit.id.toString(),
            studyEventId = this.visit.get('studyEventId'),
            subjectKrpt = this.subject.get('krpt'),
            userVisits = new UserVisits(),
            userVisit,
            visitState,
            formSchedules = new Schedules();

        // 1. Get questionnaire list for active visit
        // 2. Check if every questionnaire was completed or skipped for this subject
        // 3. If all completed, make new entry in UserVisit table, marking visit Completed
        // 4. Else make new visit in UserVisit table, marking visit as In Progress

        userReports.fetch({
            search: {
                where: { user_visit_id: visitId, subject_krpt: subjectKrpt }
            }
        })
        .then(() => {
            if (!forms) {
                deferred.resolve();
                return;
            }

            // Determine how many reports are available for this visit in this phase
            forms.forEach((visit) => {
                formSchedules.add(LF.StudyDesign.schedules.filter(schedule => schedule.get('target').id === visit), { merge: true });
            });

            LF.schedule.determineSchedules(formSchedules, {
                subject: this.subject,
                visit: this.visit
            }, (available) => {
                LF.schedule.availableSchedules.set(available);

                if (LF.schedule.availableSchedules.length === 0) {
                    // All reports completed or skipped, mark report as completed or skipped
                    let completed = 0,
                        skipped = 0,
                        countResult;

                    logger.debug('setVisitState(). Marking report as completed or skipped.');

                    countResult = _.countBy(userReports.models, (userReport) => {
                        return userReport.get('state') === LF.QuestionnaireStates.COMPLETED ?
                            LF.QuestionnaireStates.COMPLETED :
                            LF.QuestionnaireStates.SKIPPED;
                    });

                    skipped = countResult.skipped ? countResult.skipped : 0;
                    completed = countResult.completed ? countResult.completed : 0;
                    logger.debug(`setVisitState(). Count result: ${JSON.stringify(countResult)}`);
                    logger.debug(`setVisitState(). Completed: ${completed}`);
                    logger.debug(`setVisitState(). Skipped: ${skipped}`);

                    if (skipped >= userReports.length) {
                        // Skipped Visit
                        // Should never get here, unless you can skip from inside the report itself!
                        visitState = LF.VisitStates.SKIPPED;
                    } else {
                        // Completed Visit
                        visitState = LF.VisitStates.COMPLETED;
                    }
                } else {
                    // Mark visit as in progress
                    logger.debug('setVisitState(). Marking visit as in progress.');
                    visitState = LF.VisitStates.IN_PROGRESS;
                }

                userVisits.fetch({
                    search: {
                        where: { visitId, subjectKrpt }
                    }
                })
                .then(() => {
                    if (userVisits.size() === 0) {
                        userVisit = new UserVisit();
                        userVisit.set({
                            // TODO: Rename ISOLocalTZStamp() to meet code guidelines.
                            // eslint-disable-next-line new-cap
                            dateStarted: this.data.started.ISOLocalTZStamp()
                        });
                    } else {
                        userVisit = userVisits.at(0);
                    }
                })
                .then(() => {
                    return (visitState === LF.VisitStates.IN_PROGRESS) ?
                        this.visit.get('expiration') ? isVisitExpired(this.visit, userVisit, this.subject) : false :
                        false;
                })
                .then((visitExpired) => {
                    if (visitExpired) {
                        visitState = LF.VisitStates.INCOMPLETE;
                    }
                    let currentDate = new Date();
                    userVisit.set({
                        visitId,
                        studyEventId,
                        subjectKrpt,
                        state: visitState,
                        dateModified: DateTimeUtil.timeStamp(currentDate),
                        expired: visitExpired ? 'true' : 'false'
                    });

                    userVisit.save()
                    .then(() => {
                        logger.debug('setVisitState() saved User Visit state');
                        let result = null;

                        if (visitState === LF.VisitStates.COMPLETED || visitState === LF.VisitStates.SKIPPED || visitState === LF.VisitStates.INCOMPLETE) {
                            logger.debug('setVisitState(): creating visit end report');
                            let visitStateCode = 3;
                            if (visitState === LF.VisitStates.SKIPPED) {
                                // Check if there is a skip form??? if not, then this is all extra
                                visitStateCode = 1;
                            } else if (visitState === LF.VisitStates.INCOMPLETE) {
                                visitStateCode = 2;
                            }
                            result = createEndVisitReport({
                                visitStateCode,
                                visit: this.visit,
                                skippedReason: visitExpired ? '0' : null,
                                visitStartDate: userVisit.get('dateStarted'),
                                subject: this.subject,
                                instanceOrdinal: BaseQuestionnaireView.getInstanceOrdinal(),
                                ink: null,
                                visitExpired
                            })
                            .then(res => userVisit.save({
                                visitEndSigId: res.sig_id,
                                dateEnded: res.eventEndDate
                            }));
                        } else {
                            result = Q();
                        }

                        return result;
                    })
                    .catch((e) => {
                        logger.error('setVisitState() failed saving user visit.', e);
                        deferred.reject(e);
                    })
                    .finally(() => {
                        deferred.resolve();
                    });
                })
                .catch((e) => {
                    logger.error('setVisitState() failed fetching user visit.', e);
                    deferred.reject(e);
                });
            });
        })
        .catch((e) => {
            logger.error('setVisitState() failed', e);
            deferred.reject(e);
        });

        return deferred.promise;
    }

    /**
     * Sets container visit state. If there is only one user report,
     * creates a new user visit entry and initializes the data.
     * @returns {Q.Promise}
     */
    setContainerVisitState () {
        let userReports = new UserReports(),
            visitId = this.visit.id.toString(),
            subjectKrpt = this.subject.get('krpt');

        return userReports.fetch({
            search: {
                where: { user_visit_id: visitId, subject_krpt: subjectKrpt }
            }
        })
        .then(() => {
            if (userReports.length === 1) {
                let userVisit = new UserVisit(),
                    studyEventId = this.visit.get('studyEventId'),
                    currentDate = new Date();

                userVisit.set({
                    visitId,

                    // TODO: Rename ISOLocalTZStamp() to meet code guidelines.
                    // eslint-disable-next-line new-cap
                    dateStarted: this.data.started.ISOLocalTZStamp(),
                    studyEventId,
                    subjectKrpt,
                    state: LF.VisitStates.AVAILABLE,
                    dateModified: DateTimeUtil.timeStamp(currentDate),
                    expired: 'false'
                });

                return userVisit.save();
            }
            return Q();
        });
    }

    /**
     * Hook method override. Called just before the form is saved.
     * @returns {Q.Promise<string>} A signature id.
     */
    onBeforeQuestionnaireCompleted () {
        // 1. Get visit state from the DB
        // 2. If no entries, this is the first report
        let result = null,
            visitId = this.visit.id.toString(),
            subjectKrpt = this.subject.get('krpt'),
            userVisits = new UserVisits();

        if (this.visit.get('visitType') === 'container') {
            result = Q();
        } else {
            result = userVisits.fetch({
                search: {
                    where: { visitId, subjectKrpt }
                }
            })
            // eslint-disable-next-line consistent-return
            .then(() => {
                if (userVisits.size() === 0) {
                    logger.debug('onBeforeQuestionnaireCompleted(): creating visit start report');
                    return this.createStartVisitReport(this.visit.get('studyEventId'), false, null)
                    .then((sig_id) => {
                        let userVisit = new UserVisit();
                        return userVisit.save({
                            state: '',
                            visitId,
                            subjectKrpt,
                            visitStartSigId: sig_id,
                            dateStarted: this.data.started.ISOLocalTZStamp()
                        });
                    });
                }
            });
        }

        return result;
    }

    /**
     * Hook method override. Called after report is successfully completed.
     * @returns {Q.Promise<void>}
     */
    onAfterQuestionnaireCompleted () {
        let deferred = Q.defer();

        this.saveUserReport()
        .then(() => {
            if (this.visit.get('visitType') === 'container') {
                return this.setContainerVisitState();
            }
            return this.setVisitState();
        })
        .then(() => {
            return this.addEventAtEntryVariables();
        })
        .then(() => {
            deferred.resolve();
        })
        .catch((e) => {
            logger.error('onAfterQuestionnaireCompleted() failed.', e);
            deferred.reject();
        })
        .finally(() => {
            logger.debug('onAfterQuestionnaireCompleted() finished.');
        });

        return deferred.promise;
    }

    /**
     * Get the battery level of the device.
     * @returns {Q.Promise<number>}
     */
    getBatteryLevel () {
        return Q.Promise((resolve) => {
            LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
                resolve(batteryLevel);
            });
        });
    }

    /**
     * Creates visit start report for current visit.
     * @param {number} studyEventId - The ID of the study event.
     * @param {boolean} isSkipVisit - true if skipping visit.
     * @param {Date} visitStartDate - The start date of the visit.
     * @returns {Q.Promise<string>} The sig_id of the newly created dashboard record.
     */
    createStartVisitReport (studyEventId, isSkipVisit, visitStartDate) {
        // 1. Create a dashboard for visit start
        // 2. Add report variables
        // 3. Create transmission item

        // 1. Create a dashboard for visit start
        // 2. Add report variables
        let user = LF.security.activeUser,
            currentDate = new Date(),
            started = isSkipVisit ? currentDate.ISOStamp() : this.data.started.ISOStamp(),
            reportDate = isSkipVisit ? DateTimeUtil.convertToDate(currentDate) : DateTimeUtil.convertToDate(this.data.started),
            eventStartDate = isSkipVisit ? utils.buildStudyWorksFormat(DateTimeUtil.dateLocalTZFromISO8601(visitStartDate)) : utils.buildStudyWorksFormat(this.data.started),
            dashboard = new Dashboard({
                user_id: user ? user.get('id') : '',
                subject_id: this.subject.get('subject_id'),
                device_id: lStorage.getItem('deviceId') || lStorage.getItem('IMEI') || this.subject.device_id || this.subject.get('device_id'),
                questionnaire_id: 'P_VisitStart',
                SU: 'VisitStart',
                instance_ordinal: BaseQuestionnaireView.getInstanceOrdinal(),
                started,
                report_date: reportDate,
                phase: this.subject.get('phase'),
                phaseStartDateTZOffset: this.subject.get('phaseStartDateTZOffset'),
                study_version: LF.StudyDesign.studyVersion,
                ink: '',
                sig_id: `${'SA'}.${currentDate.getTime().toString(16)}${lStorage.getItem('IMEI')}`,
                krpt: this.subject.get('krpt'),
                responsibleParty: isSkipVisit ? user.get('username') : lStorage.getItem('studyDbName')
            }),
            varStartDate = new Answer({
                subject_id: this.subject.get('subject_id'),
                question_id: 'EventStartDate',
                questionnaire_id: 'P_VisitStart',
                response: eventStartDate,
                SW_Alias: 'VisitStartDate.0.EventStartDate',
                instance_ordinal: dashboard.get('instance_ordinal')
            }),
            varEventId = new Answer({
                subject_id: this.subject.get('subject_id'),
                question_id: 'EventID',
                questionnaire_id: 'P_VisitStart',
                response: studyEventId,
                SW_Alias: 'VisitStartDate.0.EventID',
                instance_ordinal: dashboard.get('instance_ordinal')
            }),
            varAffidavit = new Answer({
                subject_id: this.subject.get('subject_id'),
                question_id: 'AFFIDAVIT',
                questionnaire_id: 'P_VisitStart',
                response: '1',
                SW_Alias: 'CustomAffidavit',
                instance_ordinal: dashboard.get('instance_ordinal')
            }),
            varProtocol = new Answer({
                subject_id: this.subject.get('subject_id'),
                question_id: 'Protocol',
                questionnaire_id: 'P_VisitStart',
                response: this.subject.get('custom10') != null ? this.subject.get('custom10') : LF.StudyDesign.studyProtocol.defaultProtocol.toString(),
                SW_Alias: 'Protocol.0.Protocol',
                instance_ordinal: dashboard.get('instance_ordinal')
            }),
            transmission = new Transmission();

        return dashboard.save({
            completed: currentDate.ISOStamp(),
            completed_tz_offset: currentDate.getOffset(),
            diary_id: parseInt(currentDate.getTime().toString() +
                currentDate.getMilliseconds().toString(), 10),
            battery_level: this.data.dashboard.get('battery_level')
        })
        .then(() => {
            return varStartDate.save();
        })
        .then(() => {
            return varEventId.save();
        })
        .then(() => {
            return varAffidavit.save();
        })
        .then(() => {
            return varProtocol.save();
        })
        .then(() => {
            return createEventAtEntryVariables({
                eventId: studyEventId,
                startDate: eventStartDate,
                offset: DateTimeUtil.tzOffsetInMillis(isSkipVisit ? currentDate : this.data.started),
                questionnaireId: 'P_VisitStart',
                instanceOrdinal: dashboard.get('instance_ordinal'),
                subject: this.subject
            });
        })
        .then(() => {
            let newTransmission = {
                method: this.transmissionMethod,
                params: JSON.stringify({
                    dashboardId: dashboard.get('id'),
                    sigId: dashboard.get('sig_id')
                }),
                created: currentDate.getTime(),
                hideCount: true
            };
            if (this.subject.get('isDuplicate')) {
                newTransmission.status = 'failed';
            }
            return transmission.save(newTransmission);
        })
        .then(() => dashboard.get('sig_id'));
    }

    addEventAtEntryVariables () {
        let questionnaireID = this.id.toString(),
            visitId = this.visit.id.toString(),
            studyEventId = this.visit.get('studyEventId'),
            subjectKrpt = this.subject.get('krpt'),
            visitStart = null,
            visitStartTimeStamp = null,
            userVisits = new UserVisits();


        return userVisits.fetch({
            search: {
                where: { visitId, subjectKrpt }
            }
        })
        .then(() => {
            visitStart = userVisits.at(0).get('dateStarted');
            visitStartTimeStamp = DateTimeUtil.dateLocalTZFromISO8601(visitStart);

            return createEventAtEntryVariables({
                eventId: studyEventId,
                startDate: utils.buildStudyWorksFormat(visitStartTimeStamp),
                offset: DateTimeUtil.tzOffsetInMillis(visitStartTimeStamp),
                questionnaireId: questionnaireID,
                instanceOrdinal: this.ordinal,
                subject: this.subject
            });
        });
    }
}
