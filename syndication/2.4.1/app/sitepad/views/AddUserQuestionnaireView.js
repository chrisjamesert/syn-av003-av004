import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';

export default class AddUserQuestionnaireView extends BaseQuestionnaireView {
    get defaultRouteOnExit () {
        return 'site-users';
    }

    get defaultFlashParamsOnExit () {
        return {};
    }
}
