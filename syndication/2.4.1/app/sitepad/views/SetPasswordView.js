import ActivationBaseView from 'core/views/ActivationBaseView';
import ConfirmView from 'core/views/ConfirmView';
import CurrentContext from 'core/CurrentContext';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import { MessageRepo } from 'core/Notify';
import * as utilities from 'core/utilities';

const logger = new Logger('SetPasswordView');

export default class SetPasswordView extends ActivationBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - A list of method to bind to the DOM.
         */
        this.events = {
            'input #txtNewPassword': 'validateNewPassword',
            'input #txtConfirmPassword': 'validatePasswords',
            'click #submit': 'next',
            'click #back': 'back'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to be translated prior to render.
         */
        this.templateStrings = {
            title: 'CREATE_PERMANENT_PASSWORD_TITLE',
            legend: 'CREATE_PERMANENT_PASSWORD',
            password: 'PASSWORD',
            confirmPassword: 'CONFIRM_PASSWORD',
            cancel: 'CANCEL',
            back: 'BACK',
            next: 'NEXT',
            patientID: CurrentContext().get('user').isSubjectUser() ? 'PATIENT_ID' : 'USERNAME',
            popup_title: 'CANCEL_PERMANENT_PASSWORD_TITLE',
            popup_text: 'CANCEL_PERMANENT_PASSWORD_TEXT',
            ok: 'OK',
            yes: 'YES',
            no: 'NO',
            settings: 'SETTINGS',
            help: 'HELP',
            header: 'APPLICATION_HEADER'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     */
    get id () {
        return 'set-password';
    }

    /**
     * @property {string} template - The template to render by ID.
     */
    get template () {
        return '#set-password-template';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate in scope.
     */
    get selectors () {
        return {
            txtPassword: 'input#txtNewPassword',
            txtConfirmPassword: 'input#txtConfirmPassword'
        };
    }

    /**
     * Navigates back to the login screen
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    back (e) {
        e.preventDefault();

        this.i18n({
            header: this.templateStrings.popup_title,
            body: this.templateStrings.popup_text,
            cancel: this.templateStrings.no,
            ok: this.templateStrings.yes
        })
        .then((res) => {
            let modal = new ConfirmView();

            // @TODO SCREENSHOT-SITEPAD
            modal.show(res)
            .then(() => {
                localStorage.removeItem('Reset_Password');
                localStorage.removeItem('Reset_Password_Secret_Question');
                LF.Data.NewSecretQuestionParams = null;
                LF.Data.NewPasswordParams = null;
                this.navigate('login', true);
            }, $.noop)
            .done();
        })
        .done();
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        let inputType = this.getRoleInputType(CurrentContext().get('user').get('role'));
        return this.buildHTML({
            languageDirection: LF.strings.getLanguageDirection(),
            user: CurrentContext().get('user').get('username'),
            inputType
        }, true)
        .then(() => {
            this.buildSelectors();
            this.delegateEvents();
            if (LF.Wrapper.platform === 'windows' && inputType === 'number') {
                utilities.makeWindowsNumericInputCover('txtNewPassword', this.$txtPassword.parent().parent());
                utilities.makeWindowsNumericInputCover('txtConfirmPassword', this.$txtConfirmPassword.parent().parent());
            }
        });
    }

    /**
     * Navigate to the next screen.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        let newPassword = this.$txtPassword.val(),
            getPasswordParams = () => {
                if (CurrentContext().get('user').isSubjectUser()) {
                    return this.getSubjectPasswordParams(newPassword);
                }
                return Q(this.getPasswordChangeParams(newPassword));
            },
            newRole = LF.StudyDesign.roles.findWhere({
                id: CurrentContext().role
            });

        e && e.preventDefault();

        COOL.getClass('Utilities').isOnline()
        .then((isOnline) => {
            if (!isOnline && !newRole.canAddOffline()) {
                MessageRepo.display(MessageRepo.Banner.CONNECTION_REQUIRED, { delay: 500 })
                .done();

                return;
            }

            LF.Data.NewSecretQuestionParams = null;

            getPasswordParams()
            .then((passwordParams) => {
                // Forward to account configured screen, pass parameters
                let navigateToScreenId = 'account_configured';

                if (!!LF.StudyDesign.askSecurityQuestion && localStorage.getItem('Reset_Password_Secret_Question')) {
                    navigateToScreenId = 'reset_secret_question';
                }

                LF.Data.NewPasswordParams = passwordParams;
                this.navigate(navigateToScreenId, true);
            })
            .catch((err) => {
                logger.error(err);
            })
            .done();
        })
        .done();
    }

    /**
     * Get the the subject's encrypted password and salt.
     * @param {string} newPassword - The new password to encrypt.
     * @returns {Q.Promise<Object>}
     */
    getSubjectPasswordParams (newPassword) {
        return CurrentContext().get('user').getSubject()
        .then((subject) => {
            let krpt = subject.get('krpt');

            return {
                password: hex_sha512(newPassword + krpt),
                salt: krpt
            };
        });
    }
}

COOL.add('SetPasswordView', SetPasswordView);
