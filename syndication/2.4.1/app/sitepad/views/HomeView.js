import PageView from 'core/views/PageView';
import Sites from 'core/collections/Sites';
import Transmissions from 'core/collections/Transmissions';
import SubjectTableView from 'sitepad/views/SubjectTableView';
import { getCurrentProtocol } from 'core/Helpers';
import QrCodeView from 'sitepad/views/QrCodeView';
import getSetupCode from 'sitepad/transmit/getSetupCode';
import Logger from 'core/Logger';

let logger = new Logger('HomeView');

export default class HomeView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - The template to render to the DOM.
         * @default '#home-tpl';
         */
        this.template = '#home-tpl';

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            tbody: 'tbody',
            transmit: '#transmit',
            deactivate: '#deactivate-btn',
            patientVisit: '#patient-visit-btn',
            editPatient: '#edit-patient-btn',
            siteUsers: '#site-users-btn',
            addPatient: '#add-patient-btn',
            resetPin: '#reset-pin-btn',
            logout: '#logout-btn',
            pendingTransmission: '#pending-transmission-count'
        };

        /**
         * @property {Object<string,string>} events - A list of events to bind to the view.
         */
        this.events = {
            'click #settings-btn': 'settings',
            'click #logout-btn': 'logout',
            'click #patient-visit-btn': 'patientVisit',
            'click #edit-patient-btn': 'editPatient',
            'click #deactivate-btn': 'deactivate',
            'click #site-users-btn': 'siteUsers',
            'click #add-patient-btn': 'addNewPatient',
            'click #transmit': 'transmit'
        };

        /**
         * @property {Object<string,string>} templateStrings - Resource strings to translate and provide to the template.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            home: 'HOME',
            site: 'SITE',
            reports: 'REPORTS_AND_FORMS',
            transmissions: 'PENDING_TRANSMISSIONS',
            patient: 'PATIENT',
            initials: 'INITIALS',
            active: 'ACTIVE',
            patientVisits: 'GO_TO_VISITS',
            editPatient: 'EDIT_PATIENT',
            deactivatePatient: 'DEACTIVATE_PATIENT',
            resetPin: 'RESET_PIN',
            manageUsers: 'MANAGE_USERS',
            addPatient: 'ADD_PATIENT',
            settings: 'SETTINGS',
            logout: 'LOGOUT',
            help: 'HELP',
            siteAssessments: 'SITE_ASSESSMENTS',
            phase: 'PHASE',
            lastVisit: 'LAST_VISIT',
            deactivated: 'DEACTIVATED',
            sponsor: 'SPONSOR',
            studyVersion: 'STUDY_VERSION',
            protocol: 'PROTOCOL'
        };

        // Add Visit name keys to templateStrings
        _.each(LF.StudyDesign.visits.models, (value) => {
            this.templateStrings[value.get('id')] = value.get('displayName');
        });

        // Add phases to templateStrings
        _.each(LF.StudyDesign.studyPhase, (id, name) => {
            this.templateStrings[id] = name;
        });
    }

    /**
     * The ID of the view.
     * @property {string} id
     * @default 'home-view'
     */
    get id () {
        return 'home-view';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q.all([Sites.fetchFirstEntry(), Transmissions.fetchCollection()])
            .spread((site, transmissions) => {
                /**
                 * @property {Site} site - The site at which the device belongs.
                 */
                this.site = site;

                /**
                 * @property {Transmissions} transmissions - A collection of transmission records.
                 */
                this.transmissions = transmissions;
            });
    }

    /*
     * Render the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = LF.security.activeUser,
            role = LF.security.getUserRole();

        this.templateStrings.role = role.get('displayName');

        return user.fetch()
            .then(() => this.buildHTML({
                username: user.get('username'),
                siteCode: this.site.get('siteCode'),
                sponsorVal: LF.StudyDesign.clientName,
                protocolVal: getCurrentProtocol(),
                studyVersionVal: LF.StudyDesign.studyVersion
            }, true))
            .then((translations) => {
                /**
                 * @property {SubjectTableView} table - A table to display subjects available at the site.
                 */
                this.table = new SubjectTableView({
                    rows: [{
                        header: 'PATIENT',

                        // @todo - Disabled until it's "supported" via US.
                        // sortable: true,
                        property: 'subject_id'
                    }, {
                        header: 'INITIALS',

                        // @todo - Disabled until it's "supported" via US.
                        // sortable: true,
                        property: 'initials'
                    }, {
                        header: 'LAST_VISIT'
                    }, {
                        header: 'PHASE'
                    }],
                    translations
                });

                this.listenTo(this.table, 'select', this.select);
                this.listenTo(this.table, 'deselect', this.deselect);

                return this.table.render();
            })
            .then(() => {
                this.$('.table-wrapper').append(this.table.$el);
                return this.table.fetch();
            })
            .then(() => this.getPendingReportCount())
            .then((count) => {
                this.$pendingTransmission.html(count);
            })
            .then(() => this.delegateEvents());
    }

    /*
     * Navigate to the settings screen
     */
    settings () {
        this.navigate('settings');
    }

    /*
     * Log the user out
     */
    logout () {
        LF.security.logout(true);
    }

    /*
     * Navigate to the visits screen
     */
    patientVisit () {
        let id = this.selected.get('id');

        this.navigate(`visits/${id}`);
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        LF.security.pauseSessionTimeOut();

        return ELF.trigger('HOME:Transmit', {}, this)
            .then((evt) => {
                if (evt && evt.preventDefault) {
                    return;
                }

                this.render();
                LF.security.restartSessionTimeOut();
            });
    }

    /*
     * Transmit stored reports.
     * @returns {Q.Promise<void>}
     */
    transmit () {
        return this.triggerTransmitRule();
    }

    /*
     * Navigate to the edit patient screen.
     */
    editPatient () {
        this.navigate(`edit-patient/${this.selected.get('id')}`);
    }

    /*
     * Navigate to the deactivate patient screen.
     */
    deactivate () {
        this.navigate('deactivate-patient', true, this.selected);
    }

    /*
     * Enable buttons when a subject is selected.
     * @param {Subject} subject - The selected subject.
     */
    select (subject) {
        this.selected = subject;

        this.enableButton(this.$patientVisit);
        this.enableButton(this.$editPatient);
        this.enableButton(this.$deactivate);
        this.enableButton(this.$resetPin);

        if (LF.StudyDesign.enableQRForSubject(subject) === true) {
            HomeView.displaySubjectQR(subject);
        }
    }

    /*
     * Build and display a QR code view for the selected subject.
     * @param {Subject} subject - The selected subject to display a QR for.
     */
    static displaySubjectQR (subject) {
        let subjectCode = subject.attributes.setupCode;

        if (subjectCode === undefined) {
            LF.strings.display('QR_CODE_UNAVAILABLE')
                .then((text) => {
                    $('#qr-code').empty();
                    $('#qr-error-message').html(` ${text}`);
                    logger.info(`'Subject SetupCode not defined. Cannot generate QR Code--> fetching setup code for: '${subject.attributes.subject_id}`);

                    // Please note. We do NOT want to wait for this to finish retrieval, as we can't know how long it will take.
                    // The application must be allowed to keep moving so the user can do other things while waiting.
                    getSetupCode(subject);
                }).done();
        } else {
            const qrview = new QrCodeView();
            qrview.setQRCodeString(subject);
            const stringsToFetch = {
                title: 'QR_MODAL_TITLE',
                ok: 'OK'
            };

            LF.getStrings(stringsToFetch)
                .then((strings) => {
                    $('#qr-code').empty();
                    $('#qr-error-message').empty();
                    $('#qr-code').qrcode({ width: 60, height: 60, text: qrview.getQRCodeString() });
                    $('#qr-code canvas').click(() => {
                        return qrview.show({
                            title: `${strings.title} <bdo dir='ltr'>${subject.attributes.subject_id}</bdo>`,
                            ok: strings.ok
                        });
                    });
                }).done();
        }
    }

/*
 * Disable buttons when a subject is deselected.
 */
    deselect () {
        delete this.selected;

        this.disableButton(this.$patientVisit);
        this.disableButton(this.$editPatient);
        this.disableButton(this.$deactivate);
        this.disableButton(this.$resetPin);
        $('#qr-error-message').empty();
        $('#qr-code').empty();
    }

    /*
     * Navigate to the manage site users screen
     */
    siteUsers () {
        this.navigate('site-users');
    }

    /*
     * Navigate to the add new patient screen
     */
    addNewPatient () {
        this.navigate('add-new-patient');
    }
}
