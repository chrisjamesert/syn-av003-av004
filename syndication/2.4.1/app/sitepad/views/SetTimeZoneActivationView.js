import SetTimeZoneActivationViewCore from 'core/views/SetTimeZoneActivationView';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import State from 'sitepad/classes/State';
import * as lStorage from 'core/lStorage';
import TimeZoneUtil from 'core/classes/TimeZoneUtil';
import COOL from 'core/COOL';
import * as helpers from 'core/Helpers';

let logger = new Logger('setTimeZoneActivation');

/*
 * @class SetTimeZoneActivationView
 * @description View for setting the time zone during Sitepad activation
 * @extends SetTimeZoneActivationViewCore
 */
export default class SetTimeZoneActivationView extends SetTimeZoneActivationViewCore {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string, string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            timeZoneMsg: 'SET_TIMEZONE_MESSAGE',
            back: 'BACK',
            next: 'NEXT'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'set-tz-activation-view'
     */
    get id () {
        return 'set-tz-activation-view';
    }

    /**
     * Navigates back to the code entry view.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    back (e) {
        e.preventDefault();

        return helpers.checkTimeZoneSet()
        .then(() => {
            State.set(State.states.locked);
            this.navigate('unlockCode');
        });
    }

    /**
     * Enable the next button based on selection.
     */
    chooseZone () {
        super.chooseZone();

        // Defaults to 'America/New_York'.
        let tzSwId = 21;

        if (LF.Wrapper.platform) {
            let selected = this.$selectTimeZone.select2('data')[0],
                tzConfig = _.findWhere(LF.StudyDesign[LF.Wrapper.platform].timeZoneOptions, { tzId: selected.id });

            if (tzConfig) {
                tzSwId = tzConfig.swId;
            } else {
                // eslint-disable-next-line quotes
                logger.warn(`No configuration found for selected time zone. Defaulting to 'America/New_York'.`);
            }
        }

        lStorage.setItem('tzSwId', tzSwId);
    }

    /**
     * Navigates to the next screen.
     * @returns {Q.Promise<void>}
     */
    navigateNext () {
        State.set(State.states.endUserLicenseAgreements);
        this.navigate('endUserLicenseAgreements');
    }

    /**
     * Sets the Timezone selected.
     * @returns {Q.Promise<void>}
     */
    setTimeZone () {
        // Returning a Promise is entirely for testability.
        // This method is invoked via a click event, and doesn't live within a promise chain.
        return Q.Promise((resolve) => {
            let selected = this.$selectTimeZone.select2('data')[0];

            // Handles case where the end user cancels the timezone change, or lack thereof.
            let cancel = () => {
                logger.operational(`User canceled setting the time zone to: ${selected.displayName}`);

                // DE17472 - If canceled, enable the next button again.
                this.enableButton(this.$next);
                resolve();
            };

            LF.DynamicText.selectedTZName = selected.displayName;

            // DE17472 - Disable the next button so it can't be clicked twice.
            this.disableButton(this.$next);

            // If the device's current timezone is different than the one selected...
            if (this.currentTimeZone.id !== selected.id) {
                this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM })
                // eslint-disable-next-line consistent-return
                .then((res) => {
                    if (!res) {
                        return cancel();
                    }

                    this.changeTimeZone(selected)
                    .then(() => resolve())
                    .done();
                })
                .done();
            } else {
                // The time zone hasn't been changed. Display a message informing the user of no change.
                this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM_NOCHANGE })
                .then((res) => {
                    if (res) {
                        logger.operational(`User used the device's Time zone: ${selected.displayName}`);
                        resolve();
                        this.navigateNext();
                    } else {
                        cancel();
                    }
                })
                .done();
            }
        });
    }

    /**
     * Changes the Timezone.
     * @param {string} timezone TimeZone object.
     * @returns {Q.Promise<void>}
     */
    changeTimeZone (timezone) {
        return Q.Promise((resolve) => {
            let errorHandler = (err) => {
                logger.error(`Error happened when setting the time zone to: ${timezone.displayName}, error: ${err}`);

                // DE17472 - If rejected, we need to enable the next button again.
                this.enableButton(this.$next);
                resolve();
            };

            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    // If the application is run on a device, set the device's active timezone.
                    TimeZoneUtil.setTimeZone(timezone)
                    .then(() => {
                        return TimeZoneUtil.restartApplication()
                        .finally(() => {
                            resolve();
                            this.navigateNext();
                        });
                    }, (err) => {
                        return this.notify({
                            dialog: MessageRepo.Dialog && MessageRepo.Dialog.TIME_ZONE_SET_ERROR
                        })
                        .then(() => errorHandler(err));
                    })
                    .done();
                },
                execWhenNotWrapped: () => {
                    // The application is being run in a web browser, and doesn't have access to the
                    // TimeZoneUtil plugin.  Instead of changing the timezone, just navigate to the handoff view.
                    resolve();
                    this.navigateNext();
                }
            });
        });
    }
}

COOL.add('SetTimeZoneActivationView', SetTimeZoneActivationView);
