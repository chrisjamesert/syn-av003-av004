import RowView from './RowView';

/**
 * A row in the FormTableView.
 * @class FormRowView
 * @extends RowView
 */
export default class FormRowView extends RowView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string, string>} statusTypes - A list of status types available for translation.
         */
        this.statusTypes = {
            available: 'AVAILABLE',
            notavailable: 'NOT_AVAILABLE',
            skipped: 'SKIPPED',
            completed: 'COMPLETED'
        };
    }

    /**
     * Render the form row view to the DOM.
     * example this.render();
     * @returns {Q.Promise<FormRowView>}
     */
    render () {
        let model = this.model.toJSON();

        let toTranslate = {
            displayName: {
                namespace: model.id,
                key: model.displayName
            },
            status: this.statusTypes[model.status]
        };

        // Loop through each role associated with the questionnaire.
        model.roles.forEach((role) => {
            // If the role is not subject...
            if (role.get('id') !== 'subject') {
                // Add a translation item for said role's display name.
                toTranslate[role.get('displayName')] = {
                    namespace: 'STUDY',
                    key: role.get('displayName')
                };
            }
        });

        // Translate the provided translation keys...
        return this.i18n(toTranslate)
        .then((strs) => {
            let { displayName, status } = strs;
            let assignmentString = model.roles.map((role) => {
                if (role.get('id') !== 'subject') {
                    return `${strs[role.get('displayName')]}`;
                }
                let { subject_id, initials } = model.subject.toJSON();

                return `${subject_id} (${initials})`;
            }).join('<br />');

            // Set the translated strings on the model so we can sort by them.
            // This would be a problem if we didn't sort by order first, and these
            // values weren't already populated.
            this.model.set('translatedName', displayName);
            this.model.set('translatedStatus', status);

            let template = `<td>${displayName}</td>
                <td>${assignmentString}</td>
                <td>${status}</td>`;

            this.$el.html(template);

            return super.render();
        });
    }
}
