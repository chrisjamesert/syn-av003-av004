import SetTimeZoneViewCore from 'core/views/SetTimeZoneView';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import * as lStorage from 'core/lStorage';
import TimeZoneUtil from 'core/classes/TimeZoneUtil';

let logger = new Logger('setTimeZone');

/*
 * @class SetTimeZoneView
 * @description View for setting the time zone during Sitepad activation
 * @extends SetTimeZoneViewCore
 */
export default class SetTimeZoneView extends SetTimeZoneViewCore {
    constructor (options) {
        super(options);

        /**
         * The view's events.
         * @readonly
         * @enum {Event}
         */
        this.events = {
            'click #back': (e) => {
                this.back(e)
                .done();
            },
            'click #next': 'nextHandler',
            'change #selectTimeZone': 'changeSelection'
        };

        /**
         * @property {Object<string, string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            timeZoneMsg: 'SET_TIMEZONE_MESSAGE',
            back: 'BACK',
            next: 'NEXT'
        };
    }

    /**
     * The ID of the view.
     * @property {string} id
     * @readonly
     * @default 'set-tz-view'
     */
    get id () {
        return 'set-tz-view';
    }

    /**
     * Id of template to render
     * @type {string}
     * @readonly
     * @default '#set-time-zone-activation-template'
     */
    get template () {
        return '#set-time-zone-activation-template';
    }

    /**
     * A list of selectors to populate upon render.
     * @type {Object}
     */
    get selectors () {
        return {
            selectTimeZone: '#selectTimeZone',
            set: '#next'
        };
    }

    /**
     * Navigates back to the code entry view.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    back (e) {
        e && e.preventDefault();

        this.navigate('settings');
        return Q();
    }

    /**
     * Sets the Timezone selected.
     * @returns {Q.Promise<void>}
     */
    setTimeZone () {
        // Returning a Promise is entirely for testability.
        // This method is invoked via a click event, and doesn't live within a promise chain.
        return Q.Promise((resolve) => {
            let selected = this.$selectTimeZone.select2('data')[0];

            // Handles case where the end user cancels the timezone change, or lack thereof.
            let cancel = () => {
                logger.operational(`User canceled setting the time zone to: ${selected.displayName}`);

                // DE17472 - If canceled, enable the next button again.
                this.enableButton(this.$set);
                resolve();
            };

            LF.DynamicText.selectedTZName = selected.displayName;

            // DE17472 - Disable the next button so it can't be clicked twice.
            this.disableButton(this.$set);

            this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM })
            // eslint-disable-next-line consistent-return
            .then((result) => {
                if (!result) {
                    return cancel();
                }
                let errorHandler = (err) => {
                        logger.error(`Error happened when setting the time zone to: ${selected.displayName}, error: ${err}`);

                        // DE17472 - If rejected, we need to enable the next button again.
                        this.enableButton(this.$set);
                        resolve();
                    },

                    // Defaults to 'America/New_York'.
                    tzSwId = 21;

                LF.Wrapper.exec({
                    execWhenWrapped: () => {
                        let tzConfig = _.findWhere(LF.StudyDesign[LF.Wrapper.platform].timeZoneOptions, { tzId: selected.id });

                        if (tzConfig) {
                            tzSwId = tzConfig.swId;
                        } else {
                            logger.warn("No configuration found for selected time zone. Defaulting to 'America/New_York'.");
                        }
                        lStorage.setItem('tzSwId', tzSwId);

                        // If the application is run on a device, set the device's active timezone.
                        TimeZoneUtil.setTimeZone(selected)
                        .then(() => {
                            return TimeZoneUtil.restartApplication()
                            .finally(() => {
                                resolve();
                                this.back();
                            });
                        }, (err) => {
                            return this.notify({
                                dialog: MessageRepo.Dialog && MessageRepo.Dialog.TIME_ZONE_SET_ERROR
                            })
                            .then(() => errorHandler(err));
                        })
                        .done();
                    },
                    execWhenNotWrapped: () => {
                        // The application is being run in a web browser, and doesn't have access to the
                        // TimeZoneUtil plugin.  Instead of changing the timezone, just navigate back.
                        lStorage.setItem('tzSwId', tzSwId);
                        resolve();
                        this.back();
                    }
                });
            })
            .done();
        });
    }
}
