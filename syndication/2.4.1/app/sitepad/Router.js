import RouterBase from 'core/classes/RouterBase';
import * as lStorage from 'core/lStorage';
import Session from 'sitepad/classes/Session';
import ScheduleManager from 'core/classes/ScheduleManager';
import trial from 'sitepad/trialMerge'; //PDE UPDATE changed to make trial import modality specific
import Spinner from 'core/Spinner';

const studyRoutes = trial.assets.studyRoutes;

// Due to the many incompatibilities of Backbone with ES6 syntax,
// The routes have been defined outside of the class, and are passed directly
// into the Router's super call.
let routes = {
    // RegistrationController
    modeSelection: 'registration#modeSelection',
    siteSelection: 'registration#siteSelection',
    unlockCode: 'registration#unlockCode',
    setTimeZone: 'registration#setTimeZone',
    languageSelection: 'registration#languageSelection',
    endUserLicenseAgreements: 'registration#endUserLicenseAgreements',
    setupUser: 'registration#setupUser',

    // ApplicationController
    // The routes listed below fall under the 'activated' workflow.
    login: 'application#login',
    home: 'application#home',
    'visits/:id': 'application#visitGateway',
    'questionnaire/:id': 'application#questionnaire',
    'edit-patient/:id': 'application#editPatient',
    'deactivate-patient': 'application#deactivatePatient',
    'site-users': 'application#siteUsers',
    'edit-user/:id': 'application#editUser',
    'add-new-patient': 'application#addNewPatient',
    dashboard: 'application#dashboard',
    'dashboard/:id': 'application#dashboardWithPermission',
    'add-site-user': 'application#addSiteUser',
    'activate-user/:id': 'application#activateUser',
    'deactivate-user/:id': 'application#deactivateUser',
    'skip-visits': 'application#skipVisits',
    'questionnaire-completion': 'application#questionnaireCompletion',
    'set-password': 'application#setPassword',
    'set-secret-question': 'application#setSecretQuestion',
    'account-configured': 'application#accountConfigured',
    'time-confirmation': 'application#timeConfirmation',
    blank: 'application#blank',

    // SettingsController
    settings: 'settings#index',
    about: 'settings#about',
    'console-log': 'settings#consoleLog',
    'customer-support': 'settings#customerSupport',
    'set-time-zone': 'settings#timeZoneSettings'
};

export default class Router extends RouterBase {
    constructor (options = {}) {
        super(_.extend(options, {
            routes: _.extend({}, routes, options.routes, studyRoutes.sitepad, {
                // This route needs to be last otherwise anything added after will match this first.
                '*path': 'registration#route'
            })
        }));

        let language = lStorage.getItem('language'),
            locale = lStorage.getItem('locale');

        LF.security = new Session();
        LF.schedule = new ScheduleManager();
        LF.spinner = Spinner;

        // TODO: Move the hardcoded language values to app/core/coreSettings.js
        LF.Preferred.language = language || LF.StudyDesign.defaultLanguage || 'en';
        LF.Preferred.locale = locale || LF.StudyDesign.defaultLocale || 'US';
    }
}
