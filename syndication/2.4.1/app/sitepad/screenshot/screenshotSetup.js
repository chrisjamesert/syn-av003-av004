import ScreenshotSetup from 'core/screenshot/screenshotSetupHelper';
import setupSitepadScreenshotDialogs from './screenshotMessages';
import Subjects from 'core/collections/Subjects';

/**
 * Function to set up ELF triggers to start sreenshot mode in logpad
 */
export default function setup () {
    const setupHelper = new ScreenshotSetup({
        strings: {
            header: 'APPLICATION_HEADER'
        },

        // Used to filter diaries so that diaries with logpad specific widgets don't break.
        product: 'sitepad'
    });

    // The rule to enter screenshot mode
    ELF.rules.add({
        id: 'Screenshot_Tool',
        trigger: 'MODESELECT:Submit',
        evaluate: filter => filter.value === 'screenshot',
        resolve: [{ action: setupSitepadScreenshotDialogs }].concat(setupHelper.createActionsList()),
        reject: [{
            action () {
                if (localStorage.getItem('screenshot') === 'true') {
                    // clear all leftovers from screenshot mode
                    localStorage.removeItem('screenshot');
                    localStorage.removeItem('krpt');
                    return Subjects.clearStorage();
                }
                return Q();
            }
        }]
    });
}
