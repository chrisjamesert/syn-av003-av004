import FormRow from 'sitepad/models/FormRow';

/**
 * A collection of FormRow models.
 * @class {FormRows<T>}
 * @extends {Backbone.Collection<T>}
 */
export default class FormRows extends Backbone.Collection {
    /**
     * @property {FormRow} model - The model class the collection contains.
     */
    get model () {
        return FormRow;
    }
}
