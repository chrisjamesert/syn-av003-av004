/**
 * A model to represent a Visit row on the VisitTableView.
 * @class {VisitRow<T>}
 * @extends {Backbone.Model<T>}
 */
export default class VisitRow extends Backbone.Model { }
