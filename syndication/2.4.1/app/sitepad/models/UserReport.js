import StorageBase from 'core/models/StorageBase';

/**
 * A model representing a User Report.
 * @class {UserReport<T>}
 * @extends {StorageBase<T>}
 */
export default class UserReport extends StorageBase {
    /**
     * @property {string} name - The name of the model.
     */
    get name () {
        return 'UserReport';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            questionnaire_id: {
                type: String,
                required: true
            },
            user_visit_id: {
                type: String,
                required: true
            },
            site_user_id: {
                type: String,
                required: false
            },
            subject_krpt: {
                type: String,
                required: true
            },
            state: {
                type: String,
                required: true
            },
            date_recorded: {
                type: String,
                required: true
            },
            phase: {
                type: Number,
                required: true
            },
            sig_id: {
                type: String
            }
        };
    }
}
