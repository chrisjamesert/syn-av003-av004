import Dashboards from 'core/collections/Dashboards';
import Transmissions from 'core/collections/Transmissions';
import Logger from 'core/Logger';

// Logger is defined, but not yet used.
// eslint-disable-next-line no-unused-vars
let logger = new Logger('duplicatPatientTransmissionsResolve');

/**
 * @memberOf ELF.actions/sitepad
 * @method duplicatePatientTransmissionsResolve
 * @description
 * Resolves a duplicate patient transmission.
 * @param {Object} params - Parameter passed in by the trigger.
 * @returns {Q.Promise<void>}
 */
export function duplicatPatientTransmissionsResolve (params) {
    let deferred = Q.defer(),
        krptList = params.subjects.map((subject) => {
            subject.unset('isDuplicate');
            subject.save({ subject_id: subject.get('subject_id').substr(0, params.codeLength) });
            return subject.get('krpt');
        });

    Q.all([Transmissions.fetchCollection(), Dashboards.fetchCollection()])
    // eslint-disable-next-line consistent-return
    .spread((transmissions, dashboards) => {
        let transmissionsToCheck = transmissions.filter((transmission) => {
                return transmission.get('status') === 'failed';
            }),
            questionnaireTransmissions = [];
        if (transmissionsToCheck && krptList.length) {
            // TODO: Something is fishy here. transmissionsToCheck.map returns an array full of undefined.
            //       It should be returning an aray of Q promises to be passed for Q.all.
            //       After fixing that remove the eslint-disable line
            // eslint-disable-next-line array-callback-return
            return Q.all(transmissionsToCheck.map((transmission) => {
                let transmissionMethod = transmission.get('method'),
                    parsedParams = JSON.parse(transmission.get('params'));

                switch (transmissionMethod) {
                    case 'transmitSubjectAssignment': {
                        if (_.indexOf(krptList, parsedParams.krpt) !== -1) {
                            transmission.unset('status');
                            transmissions.execute(_.indexOf(transmissions.models, transmission));
                        }
                        break;
                    }
                    case 'transmitQuestionnaire': {
                        let matchingDashboard = dashboards.findWhere({ id: parsedParams.dashboardId });
                        if (_.indexOf(krptList, matchingDashboard.get('krpt')) !== -1) {
                            transmission.unset('status');
                            transmission.save();
                            questionnaireTransmissions.push(transmission);
                        }
                        break;
                    }
                    case 'transmitEditPatient': {
                        if (_.indexOf(krptList, parsedParams.krpt) !== -1) {
                            transmission.unset('status');
                            transmission.save();
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }))
            // eslint-disable-next-line consistent-return
            .then(() => {
                let step = (questionnaireTransmissions) => {
                    // eslint-disable-next-line consistent-return
                    transmissions.execute(_.indexOf(transmissions.models, questionnaireTransmissions[0]), () => {
                        if (questionnaireTransmissions.length > 1) {
                            questionnaireTransmissions.shift();
                            step(questionnaireTransmissions);
                        } else {
                            return Q(true);
                        }
                    });
                };

                if (questionnaireTransmissions.length) {
                    // TODO: SOmething is fishy here. Promise chain is not ended nor returned.
                    Q.delay(10000)
                    .then(() => step(questionnaireTransmissions));
                } else {
                    return Q(true);
                }
            });
        }
    })
    .then(deferred.resolve())
    .catch(deferred.reject())
    .done();

    return deferred.promise;
}

ELF.action('duplicatPatientTransmissionsResolve', duplicatPatientTransmissionsResolve);
