import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import * as DateTimeUtil from 'core/DateTimeUtil';
import * as lStorage from 'core/lStorage';

let logger = new Logger('saveSPStartDate');

/**
 * @memberOf ELF.actions/sitepad
 * @method saveSPStartDate
 * @description
 * If SPStartDate timestamp does not exist for this patient, create a new timestamp and update it on the
 * server via new transmission.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'saveSPStartDate' }]
 */
export function saveSPStartDate () {
    let subject = this.questionnaire ? this.questionnaire.subject : this.subject,
        now = new Date(),
        createTransmission = () => {
            let model = new Transmission(),
                user = LF.security.activeUser,
                subjectEnrollmentDate = new Date(subject.get('enrollmentDate')),
                params = JSON.stringify({
                    krpt: subject.get('krpt'),
                    ResponsibleParty: user.get('username'),
                    dateStarted: subjectEnrollmentDate.ISOStamp(),
                    dateCompleted: subjectEnrollmentDate.ISOStamp(),
                    reportDate: DateTimeUtil.convertToDate(subjectEnrollmentDate),
                    phase: subject.get('phase'),
                    phaseStartDate: subject.get('phaseStartDateTZOffset'),
                    sigID: `SA.${now.getTime().toString(16)}${lStorage.getItem('IMEI')}`,
                    SPStartDate: subject.get('spStartDate')
                }),
                newTransmission = {
                    method: 'transmitSPStartDate',
                    params,
                    created: now.getTime()
                };

            return model.save(newTransmission);
        };

    if (subject && !subject.get('spStartDate')) {
        subject.set('spStartDate', LF.Utilities.timeStamp(now));
        return subject.save()
        .then(createTransmission)
        .catch((e) => {
            logger.error(`SaveSPStartDate failed:  ${e}`);
        });
    }

    return Q();
}

ELF.action('saveSPStartDate', saveSPStartDate);
