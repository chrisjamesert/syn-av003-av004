import ELF from 'core/ELF';
import * as syncHistoricalData from 'core/actions/syncHistoricalData';

/**
 * @memberOf ELF.actions/sitepad
 * @method getAllSubjects
 * @description
 * Triggers the syncHistoricalData action for syncing All Subjects.
 * @param {boolean} [rejectOnError=false] If true rejects the promise returned
 * @return {Q.Promise<void>}
 */
export function getAllSubjects (rejectOnError = false) {
    return syncHistoricalData.syncHistoricalData({
        collection: 'Subjects',
        webServiceFunction: 'getAllSubjects',
        activation: false
    }, rejectOnError);
}

ELF.action('getAllSubjects', getAllSubjects);
