import * as CoreTransmitLogs from 'core/actions/transmitLogs';
import * as lStorage from 'core/lStorage';

/**
 * @memberOf ELF.actions/sitepad
 * @method transmitLogs
 * @description
 * Get the correct device id and send it to core transmitlogs
 * @param {Object} params - Parameters
 * @param {string} [params.level] - The level of logs to grab else uses system defined level
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'transmitLogs' }]
 */
export default function transmitLogs (params) {
    params.device = lStorage.getItem('deviceId') || null;
    return CoreTransmitLogs.transmitLogs(params);
}

ELF.action('transmitLogs', transmitLogs);

// @todo remove
LF.Actions.transmitLogs = transmitLogs;
