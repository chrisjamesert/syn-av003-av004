import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions/sitepad
 * @method handleNonexistentUser
 * @description
 * Deletes a nonexistent user on the device.
 * @param {Object} params - Parameters provided by the ELF rule.
 * @param {User} params.user - The nonexistent user on the device.
 * @returns {Q.Promise<Object>}
 * @example
 * resolve: [{ action: 'handleNonexistentUser' }]
 */
export function handleNonexistentUser (params) {
    return params.user.destroy();
}

ELF.action('handleNonexistentUser', handleNonexistentUser);
