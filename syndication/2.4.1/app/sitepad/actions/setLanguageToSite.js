import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';


/**
 * @memberOf ELF.actions/sitepad
 * @method setLanguageToSite
 * @description
 * Sets the context language to that of the site.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'setLanguageToSite' }]
 */
export default function setLanguageToSite () {
    let language = LF.StudyDesign.defaultLanguage || 'en';
    let locale = LF.StudyDesign.defaultLocale || 'US';

    return CurrentContext().setContextLanguage(`${language}-${locale}`);
}

ELF.action('setLanguageToSite', setLanguageToSite);
