import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Subjects from 'core/collections/Subjects';
import * as utilities from 'core/utilities';

let logger = new Logger('terminationCheck');

/**
 * @memberOf ELF.actions/sitepad
 * @method terminationCheck
 * @description
 * Check all subjects for termination
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'terminationCheck' }]
 */
export function terminationCheck () {
    return Subjects.fetchCollection()
    // eslint-disable-next-line consistent-return
    .then(subjects => utilities.promiseChain(subjects.models, (previousResult, subject) => {
        if (subject.get('phase') === LF.StudyDesign.terminationPhase) {
            logger.operational(`Subject participation has ended for krpt: ${subject.get('krpt')}`);

            return ELF.trigger('SubjectTermination', {
                subject,
                endParticipation: true,
                subjectActive: true
            }, this);
        }
    }));
}

ELF.action('terminationCheck', terminationCheck);
