/**
 * Function for evaluation of visit availability.
 * @param {Object} visit - Visit
 * @param {Object} userVisit - User visit
 * @param {Object} subject - Subject
 * @returns {Q.Promise<string>}
 */
export function checkVisitAvailability (visit, userVisit, subject) {
    let visitAvailability = visit.get('availability'),
        functionList = [];

    if (!visitAvailability) {
        return Q(true);
    }

    for (let i = 0; i < visitAvailability.length; i++) {
        let availabilityFunction = LF.Visits.availabilityFunctions[visitAvailability[i].function];
        if (availabilityFunction) {
            functionList.push(availabilityFunction({ avaliability: visitAvailability[i], visit, userVisit, subject }));
        }
    }

    return Q.all(functionList)
    .then((results) => {
        for (let i = 0; i < results.length; i++) {
            if (results[i] === false) {
                // stop evaluating and return action
                return visitAvailability[i].actionNotAvailable;
            }
        }

        return LF.StudyDesign.visitAction.show;
    });
}

/**
 * Is visit expired function.
 * @param {Object} visit - A visit
 * @param {Object} userVisit - User Visit
 * @param {Object} subject - Subject in question.
 * @returns {Q.Promise<boolean>}
 */
export function isVisitExpired (visit, userVisit, subject) {
    let visitExpiration = visit.get('expiration'),
        availabilityFunction;

    if (!visitExpiration) {
        return Q(false);
    }

    availabilityFunction = LF.Visits.availabilityFunctions[visitExpiration.function];

    if (!availabilityFunction) {
        return Q(false);
    }

    return availabilityFunction({ expiration: visitExpiration, visit, userVisit, subject });
}

