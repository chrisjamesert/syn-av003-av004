import CoreSession from 'core/classes/Session';
import { Banner } from 'core/Notify';
import State from 'sitepad/classes/State';

export default class Session extends CoreSession {
    /**
     * Default handler for the session timeout
     * @param {Backbone.View} currentView - The view that is being displayed when timeout happens
     */
    defaultSessionTimeoutHandler (currentView) {
        let removeModeSelectionLStorageValues = () => {
            this.remove('environment');
            this.remove('mode');
            this.remove('studyDbName');
            this.remove('serviceBase');
        };

        switch (State.get()) {
            case State.states.new:
                removeModeSelectionLStorageValues();
                Backbone.history.loadUrl(undefined);
                break;
            case State.states.siteSelection:
                Banner.closeAll();

                removeModeSelectionLStorageValues();

                State.set(State.states.new);
                LF.router.navigate('modeSelection', true);

                ELF.trigger('SITESELECTION:Back', { product: 'sitepad' }, this)
                .done();
                break;
            case State.states.locked:
            case State.states.setupUser:
            case State.states.languageSelection:
            case State.states.setTimeZone:
            case State.states.endUserLicenseAgreements:
                State.set(State.states.languageSelection);
                LF.router.navigate('languageSelection', true);
                break;
            case State.states.activated: {
                // DE19403: These initializations are done to disable the date/time picker
                // when session timeout occurs
                let timeTravelDateBox = $('#timeTravelDateBox');
                let timeTravelTimeBox = $('#timeTravelTimeBox');
                timeTravelDateBox.length && timeTravelDateBox.datebox().datebox('close');
                timeTravelTimeBox.length && timeTravelTimeBox.datebox().datebox('close');
                $('.modal-backdrop').remove();
                timeTravelDateBox = null;
                timeTravelTimeBox = null;
                super.defaultSessionTimeoutHandler(currentView);
                break;
            }
            default:
                super.defaultSessionTimeoutHandler(currentView);
        }
    }
}
