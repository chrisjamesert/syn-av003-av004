import CoreState from 'core/classes/State';
import Logger from 'core/Logger';

let logger = new Logger('Sitepad State');

/*
 * @class State
 * The class for state management
 */
export default class State extends CoreState {
    /**
     * The states for the sitepad modality
     * @property {Object} states
     * @property {string} states.new - The default state for a new device
     * @property {string} states.siteSelection - A site must be selected.
     * @property {string} states.locked - The device has been registered.
     * @property {string} states.setTimeZone - Time Zone needs to be selected.
     * @property {string} states.endUserLicenseAgreements - EULA needs to be accepted by the first Site User
     * @property {string} states.languageSelection - The preferred site language needs to be selected.
     * @property {string} states.setupUser - The inital super user must be created.
     * @property {string} states.activated - The device is fully registered/activated and ready for use.
     * @readonly
     * @static
     */
    static get states () {
        return {
            new: 'NEW',
            siteSelection: 'SITE_SELECTION',
            locked: 'LOCKED',
            setTimeZone: 'SET_TIME_ZONE',
            endUserLicenseAgreements: 'END_USER_LICENSE_AGREEMENTS',
            languageSelection: 'LANGUAGE_SELECTION',
            setupUser: 'SETUP_USER',
            activated: 'ACTIVATED'
        };
    }

    /**
     * Tries to convert the invalid state value becasue it could be an upgrade scenario from 2.3.2
     * @param {Object} state - The unexpected state value to be handled. Could be any type.
     * @returns {string|null} the converted state value or null if the conversion fails
     */
    static handleInvalidStateValue (state) {
        let convertedState;

        switch (state) {
            case '0':
                convertedState = this.states.new;
                break;
            case '1':
                convertedState = this.states.siteSelection;
                break;
            case '2':
                convertedState = this.states.locked;
                break;
            case '3':
                convertedState = this.states.setTimeZone;
                break;
            case '4':
                convertedState = this.states.languageSelection;
                break;
            case '5':
                convertedState = this.states.setupUser;
                break;
            case '6':
                convertedState = this.states.activated;
                break;
            default:
                convertedState = null;
                logger.fatal(`The State value in the localstorage is not valid and can not be converted: ${state}`);
        }

        if (convertedState !== null) {
            logger.operational(`The State value in the localstorage was invalid. It is converted from ${state} to ${convertedState}`);

            this.set(convertedState);
        }

        return convertedState;
    }

    /**
     * The initial state for the sitepad modality
     * @property {string} initialState
     * @readonly
     * @static
     */
    static get initialState () {
        return this.states.new;
    }

    /**
     * Returns whether the current state is considered registered or not
     * @returns {boolean}
     * @static
     */
    static isRegistered () {
        let state = this.get();

        return state !== State.states.new && state !== State.states.siteSelection;
    }
}
