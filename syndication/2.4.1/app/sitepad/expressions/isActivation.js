import ELF from 'core/ELF';
import * as lStorage from 'core/lStorage';

/**
 * Determines if the running application is in the activation state.
 * @memberOf ELF.expressions/sitepad
 * @method isActivation
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'isActivation'
 */
export function isActivation () {
    return Q(lStorage.getItem('isActivation'));
}

ELF.expression('isActivation', isActivation);
