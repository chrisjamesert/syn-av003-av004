/*
* this file has moved the core/index merging of core and trial to be modality specific.
* any reference of "import trial from 'core/index';" in the sitepad workflow will be replaced with
* a path to 'app/sitepad/trialMerge';
*/

import core from 'core/index';
import trial from 'core/../../trial/tablet'; //PDE_TODO figure out why it doesn't like trial/tablet
import {mergeObjects} from 'core/utilities/languageExtensions';

// split rules out into separate properties, so they can be executed individually along with the platform-specific rules
core.assets.coreRules = core.assets.studyRules;
core.assets.trialRules = trial.assets && trial.assets.studyRules ? trial.assets.studyRules : () => {};

delete core.assets.studyRules;
if (trial.assets) {
    delete trial.assets.studyRules;
}

export default mergeObjects(core, trial);
