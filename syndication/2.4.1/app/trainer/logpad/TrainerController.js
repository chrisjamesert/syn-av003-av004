import BaseController from 'core/controllers/BaseController';
import COOL from 'core/COOL';
import TrainerLanguageSelectionView from 'trainer/logpad/TrainerLanguageSelectionView';
import TrainerSetupView from 'trainer/logpad/TrainerSetupView';
import TrainerData from 'trainer/logpad/TrainerData';
/**
 * Navigation controller for the Trainer mode.
 * @class TrainerController
 * @extends {BaseController}
 */
export default class TrainerController extends COOL.getClass('BaseController', BaseController) {
    trainerLanguageSelection () {
        this.go('TrainerLanguageSelectionView', TrainerLanguageSelectionView);
    }

    trainerSetup () {
        this.go('LogPadTrainerSetupView', TrainerSetupView, TrainerData);
    }
}

COOL.add('TrainerController', TrainerController);
