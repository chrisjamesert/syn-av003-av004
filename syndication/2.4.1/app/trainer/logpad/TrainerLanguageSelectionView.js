import Logger from 'core/Logger';
import ActivationBaseView from 'core/views/ActivationBaseView';
import COOL from 'core/COOL';
import ELF from 'core/ELF';
import * as Utilities from 'core/utilities';

const logger = new Logger('TrainerLanguageSelectionView');

export default class TrainerLanguageSelectionView extends COOL.getClass('ActivationBaseView', ActivationBaseView) {
    constructor (trainerData) {
        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': 'next',
            'click #back': 'back'
        };

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            language: '#language'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            language: 'LANGUAGE',
            tagline: 'SELECT_LANGUAGE',
            next: 'NEXT',
            back: 'BACK'
        };

        super();
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'trainer-setup-page'
     */
    get id () {
        return 'trainer-setup-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#trainer-language-selection-template'
     */
    get template () {
        return '#trainer-language-selection-template';
    }

    /**
     * @property {string} id of the template for the language select options
     * @readonly
     * @default '#language-login-option'
     */
    get languageSelectionTemplate () {
        return '#trainer-language-option';
    }

    /**
     * Back button click handler
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    back (e) {
        e.preventDefault();

        LF.Utilities.restartApplication();
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise.<void>}
     */
    render () {
        return this.buildHTML(null, true)
        .then(() => {
            this.delegateEvents();
            this.$language.select2({
                escapeMarkup: markup => markup,
                data: this.createLanguageSelectionData(),
                templateSelection: item => this.getTemplate(this.languageSelectionTemplate)({ item }),
                templateResult: item => this.getTemplate(this.languageSelectionTemplate)({ item }),
                minimumResultsForSearch: Infinity,

                // Defer width of select2 in favor of the CSS preferences for this element.
                width: ''
            });
        });
    }

    /**
     * Creates and returns language data
     * @return {Array} data to populate the language dropdown list
     */
    createLanguageSelectionData () {
        let localized = LF.strings.match({ namespace: 'CORE', localized: {} }),
            langs = [];

        // get the localized names from the resource string files in order of 'language' and 'locale' keys
        localized.sort((firstComparatorObject, secondComparatorObject) => {
            let lang1 = firstComparatorObject.get('language'),
                lang2 = secondComparatorObject.get('language'),
                locale1 = firstComparatorObject.get('locale'),
                locale2 = secondComparatorObject.get('locale');
            if (lang1 > lang2) {
                return 1;
            } else if (lang1 < lang2) {
                return -1;
            }
            return locale1 > locale2 ? 1 : -1;
        }).forEach((language) => {
            let langCode = `${language.get('language')}-${language.get('locale')}`,
                lang = {
                    id: langCode,
                    localized: language.get('localized'),
                    fontFamily: Utilities.getFontFamily(langCode),
                    dir: language.get('direction'),
                    cssClass: language.get('direction') === 'rtl' ? 'right-direction text-right-absolute' : 'left-direction text-left-absolute',
                    selected: language.get('language') === LF.Preferred.language && language.get('locale') === LF.Preferred.locale
                };

            langs.push(lang);
        });

        return langs;
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        localStorage.setItem('preferredLanguageLocale', this.$language.val());

        return ELF.trigger('LANGUAGESELECTION:Next', {}, this)
        .then((evt) => {
            if (!evt.preventDefault) {
                this.navigate('set_time_zone_activation', true);
            }
        });
    }
}

COOL.add('TrainerLanguageSelectionView', TrainerLanguageSelectionView);
