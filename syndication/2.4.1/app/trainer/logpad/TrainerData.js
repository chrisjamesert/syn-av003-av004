let TrainerData = {};

export default TrainerData;

let now = new Date(),
    timestamp = LF.Utilities.timeStamp(now);

let Configs = {
    /**
     * The flag to determine whether or not to expose the Trainer Data Form view.
     * @type Boolean
     * @readonly
     */
    dataForm: true,

    /**
     * The default values to be used in Acitvating the user.
     * These can be overridden by PDE's here.
     * @type Object
     * @readonly
     */
    defaults: {
        activationDate: LF.Utilities.timeStamp(new Date()),
        enrollmentDate: LF.Utilities.timeStamp(new Date()),
        lang: 'en-US',
        log_level: 'ERROR',
        phaseTriggered: 'false',
        phaseStartDate: LF.Utilities.timeStamp(new Date()),
        subject_active: 1,
        subject_id: 12345678,
        initials: 'AAA',
        krpt: hex_sha512(new Date().getTime()),
        subject_number: '001',
        site_code: '300',
        device_id: '77892234567',
        service_password: '12345',
        site_password: '54321',
        phase: 10,
        krdom: 'DOM.28041.245'
    }
};

/**
 * Array of questions for the TrainerDataFormView form. Each item is an object
 * that defines the id, label, type, and storage type. 'Select' types
 * also have a an array of options that contain objects with the value and text
 * @type Array
 * @readonly
 */
Configs.localVariables = [
/**
 * A Data Form question to select the Phase at runtime
 * This configuration creates a select element with a pre-selected value and differing texts and values
 * Comment out this Object to remove it from the questionnaire
 */
    {
        id: 'phase',
        label: 'PHASE',
        type: 'select',
        options: [
            { value: 1, text: 'Phase 1' },
            { value: 2, text: 'Phase 2', selected: true },
            { value: 3, text: 'Phase 3' },
            { value: 4, text: 'Phase 4' }
        ],
        storage: 'LF.Data'
    },

/**
 * A Data Form question to select the Logging Level at runtime
 * This configuration creates a select element with a pre-selected value and the same texts and values
 * Comment out this Object to remove it from the questionnaire
 */
    {
        id: 'logging',
        label: 'LOG_LEVEL',
        type: 'select',
        options: [
            { text: 'TRACE' },
            { text: 'ERROR', selected: true },
            { text: 'WARN' },
            { text: 'INFO' },
            { text: 'DEBUG' }
        ],
        storage: 'LF.Data'
    },

/**
 * A Data Form question to select the Site Code at runtime
 * This configuration creates a blank text input
 * Comment out this Object to remove it from the questionnaire
 */
    {
        id: 'site',
        label: 'SITE_CODE',
        type: 'input',
        storage: 'LF.Data',
        validation: {
            regex: /^\d+$/,
            errorKey: 'ERROR_TITLE'
        }
    },

/**
 * A Data Form question to select the Subject Number at runtime
 * This configuration creates a blank text input
 * Comment out this Object to remove it from the questionnaire
 */
    {
        id: 'subject',
        label: 'SUBJECT_NUMBER',
        type: 'input',
        storage: 'LF.Data',
        validation: {
            regex: /^\d+$/,
            errorKey: 'ERROR_TITLE'
        }
    },

/**
 * A Data Form question to select the krpt at runtime
 * This configuration creates a text input with a default value
 * Comment out this Object to remove it from the questionnaire
 */
    {
        id: 'krpt',
        label: 'KRPT',
        default: Configs.defaults.krpt,
        type: 'input',
        storage: 'localStorage'
    }
];

TrainerData.Configs = Configs;

TrainerData.getAdminPassword = () => ({
    role: 'Admin',
    hash: `0x${hex_sha512(`apple${Configs.defaults.site_password || '54321'}`)}`,
    key: 'apple'
});

TrainerData.getkrDom = () => ({
    sitecode: LF.Data.site || Configs.defaults.site_code || '301',
    krdom: Configs.defaults.krdom
});

TrainerData.getSubjectActive = {
    S: false,
    K: Configs.defaults.krpt
};

TrainerData.registerDevice = {};
TrainerData.getSites = {};
TrainerData.getSubjects = {};
TrainerData.getSubjectQuestion = {};
TrainerData.querySubjectData = {};
TrainerData.updateSubjectData = {};
TrainerData.sendDiary = {};
TrainerData.sendLogs = {};
TrainerData.syncLastDiary = 'null';
TrainerData.addUser = 'E';
TrainerData.updateUserCredentials = 'E';
TrainerData.getSiteAccessCode = Configs.defaults.site_password || '54321';

// Defining the value as a property makes sure that it returns the current date.
Object.defineProperty(TrainerData, 'getServerTime', {
    get: () => ({
        utc: new Date()
    })
});

TrainerData.getDeviceID = () => ({
    D: Configs.defaults.device_id || '12345678',
    W: Configs.defaults.service_password || '12345',
    Q: 1,
    A: 'fish'
});

TrainerData.doSubjectSync = {
    A: Configs.defaults.activationDate || timestamp,
    C: LF.Data.site || Configs.defaults.site_code || '301',
    E: Configs.defaults.enrollmentDate || timestamp,
    I: Configs.defaults.initials || '',
    K: localStorage.getItem('krpt') || Configs.defaults.krpt || hex_sha512(now.getTime()),
    L: LF.Data.logging || Configs.defaults.log_level || 'ERROR',

    // LF.Data.subject contains the subject number entered in the trainer setup view.
    N: LF.Data.subject || Configs.defaults.subject_number || '001',
    T: Configs.defaults.phaseStartDate || timestamp,
    P: (!isNaN(parseInt(LF.Data.phase, 10)) && parseInt(LF.Data.phase, 10)) || Configs.defaults.phase || 10
};

TrainerData.getSingleSubject = {
    initials: Configs.defaults.initials,
    language: Configs.defaults.lang,
    krdom: Configs.defaults.krdom,
    deleted: false,
    dob: '',
    gender: '',
    custom1: '',
    custom2: '',
    custom3: '',
    custom4: '',
    custom5: '',
    custom6: '',
    custom7: '',
    custom8: '',
    custom9: '',
    custom10: '3',
    siteCode: LF.Data.site || Configs.defaults.site_code || '301',
    krpt: localStorage.getItem('krpt') || Configs.defaults.krpt || hex_sha512(now.getTime()),
    logLevel: LF.Data.logging || Configs.defaults.log_level || 'ERROR',
    setupCode: localStorage.getItem('activationCode'),

    // LF.Data.subject contains the subject number entered in the trainer setup view.
    subjectId: LF.Data.subject || Configs.defaults.subject_number || '001',
    phaseStartDate: Configs.defaults.phaseStartDate || timestamp,
    phase: (!isNaN(parseInt(LF.Data.phase, 10)) && parseInt(LF.Data.phase, 10)) || Configs.defaults.phase || 10,
    isActive: true
};

TrainerData.syncUsers = {};
