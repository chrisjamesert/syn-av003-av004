import ELF from 'core/ELF';

/**
 * Determines if the study is online. For trainer always returns true
 * @param {Object} input - ELF context.
 * @param {function} done - Invoked upon completion of the expression.
 */
function studyOnline (input, done) {
    done(true);
}


/**
 * overrides the isStudyOnline expression to avoid API calls.
 * @function overrideIsStudyOnline
 */
export function overrideIsStudyOnline () {
    ELF.expression('isStudyOnline', studyOnline);
}
