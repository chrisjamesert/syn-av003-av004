import COOL from 'core/COOL';
import SiteSelectionView from 'sitepad/views/SiteSelectionView';

/**
 * @function extendSiteSelectionView
 * Extends SiteSelectionView to modify the unlock code validation behavior
 */
export function extendSiteSelectionView () {
    /**
     * @class TrainerSiteSelectionView
     * A class that extends the sitepad SiteSelectionView view.
     */
    class TrainerSiteSelectionView extends COOL.getClass('SiteSelectionView', SiteSelectionView) {
        /**
         * @method validateUnlockCode
         * Compares the entered unlockCode to be a string of a 9 digit number
         * @param {string} unlockCode The unlock code user entered.
         * @returns {boolean}
         */
        validateUnlockCode (unlockCode) {
            let validator = /^\d{9}$/;

            return validator.test(unlockCode);
        }
    }

    COOL.add('SiteSelectionView', TrainerSiteSelectionView);
}