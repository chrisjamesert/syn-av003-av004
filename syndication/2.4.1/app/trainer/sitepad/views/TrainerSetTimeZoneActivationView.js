import COOL from 'core/COOL';
import SetTimeZoneActivationView from 'sitepad/views/SetTimeZoneActivationView';

/**
 * Extends SetTimeZoneActivationView to fake setting time zone on trainer
 * @function extendSetTimeZoneActivationView
 */
export function extendSetTimeZoneActivationView () {
    /**
     * A class that extends the sitepad SetTimeZoneActivationView view.
     * @class TrainerSetTimeZoneActivationView
     */
    class TrainerSetTimeZoneActivationView extends COOL.getClass('SetTimeZoneActivationView', SetTimeZoneActivationView) {
        /**
         * Resolve any dependencies required for the view to render.
         * @returns {Q.Promise<void>}
         */
        resolve () {
            this.timeZonesDisplay = [{ id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' }];
            this.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };

            return Q();
        }

        /**
         * Changes the Timezone. In trainer, just go to the next screen.
         * @param {string} timezone TimeZone object.
         * @returns {Q.Promise<void>}
         */
        changeTimeZone (timezone) {
            this.navigateNext();

            return Q();
        }
    }

    COOL.add('SetTimeZoneActivationView', TrainerSetTimeZoneActivationView);
}
