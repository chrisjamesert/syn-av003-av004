import COOL from 'core/COOL';
import UnlockCodeView from 'sitepad/views/UnlockCodeView';

/**
 * @function extendUnlockCodeView
 * Extends UnlockCodeView to modify the unlock code validation behavior
 */
export function extendUnlockCodeView () {
    /**
     * @class TrainerUnlockCodeView
     * A class that extends the sitepad UnlockCodeView view.
     */
    class TrainerUnlockCodeView extends COOL.getClass('UnlockCodeView', UnlockCodeView) {
        /**
         * @method validateUnlockCode
         * Compares the entered unlockCode to be a string of a 9 digit number
         * @param {string} unlockCode The unlock code user entered.
         * @returns {boolean}
         */
        validateUnlockCode (unlockCode) {
            let validator = /^\d{9}$/;

            return validator.test(unlockCode);
        }
    }

    COOL.add('UnlockCodeView', TrainerUnlockCodeView);
}