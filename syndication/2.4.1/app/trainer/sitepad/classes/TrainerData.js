import COOL from 'core/COOL';

/**
 * A class to provide fake data for the trainer mode
 * @class TrainerData
 */
export default class TrainerData {
    /**
     * Return the fake data for a specific webservice function
     * @method getData
     * @param {string} webserviceFunction The name of the webservice function to return fake data for
     * @returns {Array|Object} The fake trainer data.
     */
    static getData (webserviceFunction) {
        let syncStatus = {
            syncID: null,
            isSubjectActive: 1,
            isDuplicate: false
        };

        switch (webserviceFunction) {
            case 'getSites':
                return _.extend(syncStatus, {
                    res: [
                        { krdom: 'DOM.466280.195', siteCode: '0001' },
                        { krdom: 'DOM.466591.209', siteCode: '0002' },
                        { krdom: 'DOM.463029.888', siteCode: '0003' },
                        { krdom: 'DOM.466985.231', siteCode: '0004' },
                        { krdom: 'DOM.466797.219', siteCode: '0005' },
                        { krdom: 'DOM.473830.257', siteCode: '0006' },
                        { krdom: 'DOM.469130.239', siteCode: '0007' },
                        { krdom: 'DOM.466923.225', siteCode: '0008' },
                        { krdom: 'DOM.468381.188', siteCode: '0009' },
                        { krdom: 'DOM.472978.249', siteCode: '0010' },
                        { krdom: 'DOM.473794.253', siteCode: '0011' },
                        { krdom: 'DOM.478020.319', siteCode: '0012' }
                    ]
                });
            case 'getCountryCode':
                return 'US';
            case 'registerDevice':
                return _.extend(syncStatus, {
                    res: { apiToken: '78525C0D907EB7EF96F1CBA4A4D54C91', regDeviceId: 422 }
                });
            case 'addUser':
                return _.extend(syncStatus, {
                    res: 195
                });
            case 'getAllSubjects':
                return { res: [] };
            case 'syncUserVisitsAndReports':
                return _.extend(syncStatus, {
                    res: { reports: [], lastRefreshTime: new Date().ISOLocalTZStamp() }
                });
            case 'syncUsers':
                return { res: [] };
            case 'updateUserCredentials':
                return _.extend(syncStatus, {
                    res: 'S'
                });
            case 'sendLogs':
                return _.extend(syncStatus, {
                    res: null
                });
            case 'sendSubjectAssignment':
                return _.extend(syncStatus, {
                    res: { krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813' }
                });
            case 'setSubjectData':
                return _.extend(syncStatus, {
                    res: { D: '186b6970-5b57-4b51-8234-1ba70aeb8fc7', W: 'e025bf2329734303c524ab6e92f26235' }
                });
            case 'sendDiary':
                return _.extend(syncStatus, {
                    res: null
                });
            case 'updateSubjectData':
                return _.extend(syncStatus, {
                    res: { W: 'e025bf2329734303c524ab6e92f26235' }
                });
            case 'sendEditPatient':
                return _.extend(syncStatus, {
                    res: { krpt: 'SA.97c321226b23cfdd2fd6f289340761ef' }
                });
            case 'getSetupCode':
                return _.extend(syncStatus, {
                    res: '84317334'
                });
            case 'getDeviceID':
                return _.extend(syncStatus, {
                    res: {
                        D: '818ad42a-c0f5-4bff-b8b7-0f3f08f46aee',
                        W: 'e6c91a51191b0b80a63fcc065bfa6c3b',
                        Q: 0,
                        A: 'f7c0e071db137f5ae65382041c7cef4b'
                    }
                });
            case 'getServerTime':
                return {
                    res: { utc: new Date() }
                };
            case 'updateUser':
                return _.extend(syncStatus, {
                    res: 'S'
                });
            default:
                return undefined;
        }
    }
}

COOL.add('SitepadTrainerData', TrainerData);
