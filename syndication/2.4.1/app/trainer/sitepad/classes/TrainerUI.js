import COOL from 'core/COOL';

/**
 * @class TrainerUI
 * A class that modifies the UI behavior and style for Trainer mode.
 */
export default class TrainerUI {
    /**
     * @method registerCSS
     * Registers CSS styles to distinguish trainer mode UI
     */
    static registerCSS () {
        $('#application').addClass('trainer-mode');
    }

    /**
     * @method overrideCoreStrings
     * Add TRAINER_HEADER to appropriate strings for all locales
     */
    static overrideCoreStrings () {
        let appStrings = LF.strings.match({
            namespace: 'CORE',
            resources: {}
        });

        appStrings.forEach(item => {
            let resources = item.get('resources');

            // DE17366 - If APPLICATION_HEADER or APP_NAME is undefined, do nothing. Let the i18n
            // service fallback to a language where it is defined.
            resources.APPLICATION_HEADER && (resources.APPLICATION_HEADER += ' {{ TRAINER_HEADER }}');
            resources.APP_NAME && (resources.APP_NAME += ' {{ TRAINER_HEADER }}');
        });
    }
}

COOL.add('SitepadTrainerUI', TrainerUI);
