import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';
import TrainerData from './TrainerData';

let logger = new Logger('Sitepad TrainerWebservice');

/**
 * Extends some of the functions of the core Webservice module and adds the module to the COOL registry
 * @function extendCoreWebservice
 */
export function extendCoreWebservice () {
    /**
     * A class that extends the core Webservice Module.
     * The functions in this class override the core functionality to prevent backend API calls
     * @class SitepadTrainerWebservice
     */
    class SitepadTrainerWebservice extends COOL.getClass('WebService', WebService) {
        /**
         * Get a list of available sites that belong to a given study.
         * @method getSites
         * @param {string} studyName The name of the study to get sites for.
         * @returns {Object} A deferred promise.
         */
        getSites (studyName) {
            logger.trace(`getSites for ${studyName}`);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('getSites'));
        }

        /**
         * Gets the site countryCode from the server. Ex. "US"
         * @param {string} studyName The name of the study to get sites for.
         * @returns {Q.Promise<Object>} Q Promise
         */
        getCountryCode (studyName) {
            logger.trace(`getCountryCode for ${studyName}`);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('getCountryCode'));
        }

        /**
         * Gets the time in utc from the server.
         * @method getServerTime
         * @returns {Q.Promise<Object>} Q Promise
         */
        getServerTime () {
            logger.trace('getServerTime');

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('getServerTime'));
        }

        /**
         * Register a SitePad device.
         * @method registerDevice
         * @param {Object} payload - Payload passed to the webservice method.
         * @returns {Q.Promise}
         */
        registerDevice (payload) {
            logger.trace('registerDevice', payload);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('registerDevice'));
        }

        /**
         * Send new user data.
         * @param {Object} params - Parameters passed to the webservice method.
         * @param {string} params.user_type type of the user ['admin' || null]
         * @param {string} params.user_name user's name
         * @param {string} params.language user's language
         * @param {string} params.password user's password
         * @param {string} params.key salt for the user's password hash
         * @param {string} params.role role of the new user
         * @param {string} params.syncLevel level of synchronization
         * @param {string} params.syncValue value of the sync level
         * @param {string} params.auth authorization token for the webservice.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        addUser (params = {}) {
            logger.trace('addUser', params);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('addUser'));
        }

        /**
         * Get all subject data for a site from Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
         * @param {Object} params - Parameters passed to the webservice method
         * @param {string} params.auth AJAX authorization token.
         * @returns {Q.Promise}
         */
        getAllSubjects (params = {}) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('getAllSubjects');

            logger.trace('getAllSubjects', params);

            return Q(data);
        }

        /**
         * Send krpt, krDom, lastUpdated, deviceId and get all visits and forms status for that site.
         * @param  {Object} params -  Parameters passed to the webservice method
         * @param  {string} params.auth - auth token
         * @param  {string} params.krDom - site krDom
         * @param  {string} params.lastUpdated - last update timestamp
         * @param  {string} params.krpt - subject SW ID
         * @param  {string} params.deviceId - device SW ID
         * @returns {Q.Promise}
         */
        syncUserVisitsAndReports (params = {}) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('syncUserVisitsAndReports');

            logger.trace('syncUserVisitsAndReports', params);

            return Q(data);
        }

        /**
         * Update Get all users that have been added/updated since PHT_lastUpdatedUsers or 0
         * @param {Object} params - Parameters passed to the webservice method
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        syncUsers (params = {}) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('syncUsers');

            logger.trace('syncUsers', params);

            return Q(data);
        }

        /**
         * Update non-subject User's password and/or security question and answer
         * @param {Object} params - Parameters passed to the webservice method
         * @param {string} params.auth authorization token for the webservice.
         * @param {string} params.userId unique id of user to update
         * @param {string} params.password new password we want to send
         * @param {string} params.salt salt of the new password
         * @param {string} params.secretQuestion secretQuestion of user to update
         * @param {string} params.secretAnswer secretAnswer of user to update
         * @returns {Q.Promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
         */
        updateUserCredentials (params = {}) {
            logger.trace('updateUserCredentials', params);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('updateUserCredentials'));
        }

        /**
         * Logs to be sent.
         * @param {Object} logs Logs generated to be sent.
         * @returns {Q.Promise.reject()} Q promise
         */
        sendLogs (logs) {
            logger.trace('sendLogs', logs);
            return Q.reject('Not sending logs on trainer!');
        }

        /**
         * Send the new Patient using Transmission Queue.
         * @method sendSubjectAssignment
         * @param {string} assignment The subject assignment to be sent.
         * @param {string} auth Authorization token.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        sendSubjectAssignment (assignment, auth) {
            logger.trace('sendSubjectAssignment', assignment);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('sendSubjectAssignment'));
        }

        /**
         * Used to set the subject password, secret question and secret answer for the first time.
         * @param {Object} payload - Payload passed to the transmit method.
         * @param {string} payload.U Setup Code
         * @param {string} payload.W Password
         * @param {string} payload.Q Secret Question
         * @param {string} payload.A Secret Answer
         * @param {string} payload.O Client Name
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        setSubjectData (payload) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('setSubjectData');

            logger.trace('setSubjectData', payload);

            return Q(data);
        }

        /**
         * Send the completed Diary using Transmission Queue.
         * @param {Object} diary The completed diary to be sent.
         * @param {Object} auth the authentication token
         * @param {boolean} jsonh - The value indicating whether jsonh compression is being used or not
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        sendDiary (diary, auth, jsonh) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('sendDiary');

            logger.trace('sendDiary', diary);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('sendDiary'));
        }

        /**
         * Updates the Subject's Data.
         * @param {string} deviceID The device ID for the subject.
         * @param {Object} subjectData The data needs to be updated.
         * @param {string} auth AJAX authorization token.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        updateSubjectData (deviceID, subjectData, auth) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('updateSubjectData');
            logger.trace(`updateSubjectData for deviceID: ${deviceID}`, subjectData);

            return Q(data);
        }

        /**
         * Sends Edit Patient Diary data
         * @param {Object} params data to be sent
         * @param {Object} auth the authentication token
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q Promise
         */
        sendEditPatient (params, auth) {
            logger.trace('sendEditPatient', params);

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('sendEditPatient'));
        }

        /**
         * Gets subject setup code
         * @param {Object} krpt krpt of the subject of which setup code is to be returned
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        getSetupCode (krpt) {
            logger.trace('getSetupCode', krpt);

            // the response needs to be the final res value, not the res object or the calling
            // code will fail.
            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('getSetupCode').res);
        }

        /**
         * Get the device ID for the subject.
         * @param {Object} subjectData The Subject's data used to retrieve the device ID.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        getDeviceID (subjectData) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('getDeviceID');

            logger.trace('getDeviceID for subjectData', subjectData);

            return Q(data);
        }

        /**
         * Update existing user.
         * @param {Object} params - Parameters passed into the method.
         * @param {number} params.id - The ID of the user.
         * @param {string} params.userType - type of the user ['admin' || null]
         * @param {string} params.username - user's name
         * @param {string} params.language - user's language
         * @param {string} params.role - role of the new user
         * @param {string} params.syncLevel - level of synchronization
         * @param {string} params.syncValue - value of the sync level
         * @param {string} [params.auth] - authorization token for the webservice.
         * @returns {Q.Promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
         */
        updateUser (params = {}) {
            logger.trace('updateUser');

            return Q(COOL.getClass('SitepadTrainerData', TrainerData).getData('updateUser'));
        }
    }

    COOL.add('WebService', SitepadTrainerWebservice);

    // FIXME: This should be considered a workaround.
    // We should not be using LF.studyWebService thoughout the code. It should be extended using COOL by PDEs in the study level
    LF.studyWebService = COOL.new('WebService');
}
