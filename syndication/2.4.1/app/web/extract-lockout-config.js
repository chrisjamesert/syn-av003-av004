/* eslint-disable */
// This is a hackish solution to retrieving the unlock code lockout config
// from core/assets/study-design/study-design.js.
// This is done here instead of on the server side because
// of issues with ArrayRef and ObjectRef

const fs = require('fs');
const path = require('path');

process.chdir(__dirname);

const retrieveConfig = () => {
    const src = path.join(__dirname, '../core/assets/study-design/study-design.js'),
        dest = path.resolve(__dirname, '../../target/web/node/unlock-code-config.js'),
        studyDesign = fs.readFileSync(src, 'utf8'),
        callback = (err, data) => {
            if (err) {
                console.error('An error occurred writing to file.', err);
                return;
            }

            console.log('Wrote target/web/node/unlock-code-config.js');
        };

    let newFile,
        linesToExtract = studyDesign.split('\n')
            .filter((line) => {
                return line.indexOf('maxUnlockAttempts') !== -1 ||
                    line.indexOf('unlockCodeLockoutTime') !== -1;
            })
            .join('\n')
            .replace(/(^,)|(,$)/g, '')
            .trim();

    newFile = `const config = {\n\t${linesToExtract}\n};\nmodule.exports = config;`;

    fs.writeFile(dest, newFile, callback);
};

retrieveConfig();
