import setupWebDialogs from './DialogBoxes';
import setupWebBanners from './Banners';

/**
 * Registers the web messages
 */
export default function setupWebMessages () {
    setupWebDialogs();
    setupWebBanners();
}
