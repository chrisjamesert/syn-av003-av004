import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

/**
 * Registers Web Modality Dialogs
 */
export default function setupWebDialogs () {
    MessageRepo.add({
        type: 'Dialog',
        key: 'UNLOCK_ATTEMPT_EXCEEDED',
        message: MessageHelpers.notifyDialogCreator({
            message: 'UNLOCK_ATTEMPT_EXCEEDED'
        })
    }, {
        type: 'Dialog',
        key: 'UNSUPPORTED_BROWSER',
        message: MessageHelpers.notifyDialogCreator({
            message: 'UNSUPPORTED_BROWSER'
        })
    }, {
        type: 'Dialog',
        key: 'CONFIRM_ACTIVATE_TAB',
        message: MessageHelpers.confirmDialogCreator({
            message: 'CONFIRM_ACTIVATE_TAB',
            ok: 'YES',
            cancel: 'NO',
            type: 'warning'
        })
    }, {
        type: 'Dialog',
        key: 'SITE_CHANGE_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            message: 'SITE_CHANGE_CONFIRM'
        })
    }, {
        type: 'Dialog',
        key: 'SITE_IS_LOCKED_OUT',
        message: MessageHelpers.notifyDialogCreator({
            message: 'SITE_IS_LOCKED_OUT',
            type: 'warning'
        })
    }, {
        type: 'Dialog',
        key: 'TRANSCRIBE_FORM_CONFIRMATION',
        message: MessageHelpers.confirmDialogCreator({
            header: 'TRANSCRIBE_FORM_HEADER',
            message: 'TRANSCRIBE_FORM_CONFIRMATION',
            ok: 'YES',
            cancel: 'NO'
        })
    });
}
