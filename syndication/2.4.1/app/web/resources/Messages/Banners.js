import { Banner, MessageRepo } from 'core/Notify';

export default function setupWebBanners () {
    MessageRepo.add({
        type: 'Banner',
        key: 'INVALID_USERNAME',
        message: ({ afterShow = $.noop }) => {
            Banner.error('INVALID_USERNAME', { afterShow });
        }
    }, {
        type: 'Banner',
        key: 'BACK_BUTTON_NOTIFICATION',
        message: ({ afterShow = $.noop }) => {
            Banner.error('BACK_BUTTON_NOTIFICATION', { afterShow });
        }
    }, {
        type: 'Banner',
        key: 'INCORRECT_USERNAME_PWD',
        message: ({ afterShow = $.noop }) => {
            Banner.error('INCORRECT_USERNAME_PWD', { afterShow });
        }
    }, {
        type: 'Banner',
        key: 'INCORRECT_UNLOCK_CODE',
        message: ({ afterShow = $.noop }) => {
            Banner.error('INCORRECT_UNLOCK_CODE', { afterShow });
        }
    }, {
        type: 'Banner',
        key: 'NO_RESULTS_FOUND',
        message: ({ afterShow = $.noop }) => {
            Banner.error('NO_RESULTS_FOUND', { afterShow });
        }
    });
}
