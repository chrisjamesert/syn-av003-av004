// Logs the user out on a failed affidavit.
const passwordAffidavitLockout = {
    id: 'passwordAffidavitLockout',
    trigger: 'QUESTIONNAIRE:AffidavitFailure',
    resolve: [
        { action: 'logout' }
    ]
};

export default passwordAffidavitLockout;
