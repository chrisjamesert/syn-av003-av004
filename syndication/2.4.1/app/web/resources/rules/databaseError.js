// Refreshes the browser tab whenever the database has locked up.
const databaseError = {
    id: 'onDatabaseError',
    trigger: 'APPLICATION:DatabaseError',
    resolve: [
        { action: 'refresh' }
    ]
};

export default databaseError;
