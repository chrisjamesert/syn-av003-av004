// Navigates to the InactiveTabView when a tab becomes inactive.
const inactiveTab = {
    id: 'inactiveTab',
    trigger: 'APPLICATION:InactiveTab',
    resolve: [{
        action: 'navigateTo',
        data: 'inactive-tab'
    }, {
        // DE21781 - Edge browser is locking IndexedDB connections when new tabs are opened.
        // Whenever a tab becomes inactive we need to close the databases to free up the active tab's connections
        action: 'closeDatabases'
    }]
};

export default inactiveTab;
