// QuestionnaireBackOut
// Displays a confirmation prompt when a user tries to back out of a questionnaire.
const questionnaireBackOut = {
    id: 'questionnaireBackOut',
    trigger: 'QUESTIONNAIRE:BackOut',
    evaluate: {
        expression: 'confirm',
        input: { key: 'BACK_OUT_CONFIRM' }
    },
    resolve: [{
        action (input, done) {
            // DE22251 this fixes the current state for IE
            // to assure Back Button Banner doesn't show.
            window.history.replaceState(null, null);

            LF.security.stopQuestionnaireTimeOut();
            localStorage.setItem('questionnaireToDashboard', true);

            LF.router.flash({
                subject: this.subject,
                visit: this.visit
            }).navigate('dashboard', {
                trigger: true,
                replace: true
            });

            done();
        }
    }],
    reject: [{
        action (input, done) {
            localStorage.removeItem('questionnaireToDashboard');
            done({ preventDefault: true });
        }
    }]
};

export default questionnaireBackOut;
