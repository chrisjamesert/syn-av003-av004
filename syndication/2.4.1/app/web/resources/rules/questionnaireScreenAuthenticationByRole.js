/*
 * QuestionnaireScreenAuthenticationByRole
 * Checks to see if the user logged in has a role able to answer a quesion based on a role configured at the screen level
 * If the user does not have the role required at the screen level. A login window is displayed requiring a user with the level
 * configured at the screen level to login.
 */
export default {
    id: 'QuestionnaireScreenAuthenticationByRole',
    trigger: 'QUESTIONNAIRE:Before',
    evaluate: ['AND', '!isActivation', '!isTranscriptionMode'],
    resolve: [{
        action: 'questionnaireScreenAuthenticationByRole',
        data: {
            sortUsers: (users) => {
                let subjects = _.find(users, (user) => {
                    return user.get('role') === 'subject';
                });

                return Q.Promise((resolve) => {
                    let sorted = (_.sortBy(users, (user) => {
                        // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                        return subjects ? user.get('role') : user.get('lastLogin') ? user.get('lastLogin') : new Date(1986, 1, 21).toString();
                    })).reverse();

                    resolve(sorted);
                });
            }
        }
    }],
    reject: [{ action: 'defaultAction' }]
};
