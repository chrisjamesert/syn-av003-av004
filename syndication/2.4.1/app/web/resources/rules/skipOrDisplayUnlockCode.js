// Determines if the unlock code view should be displayed.
const skipOrDisplayUnlockCode = {
    id: 'skipOrDisplayUnlockCode',
    trigger: 'SITEASSIGNMENT:Completed',
    resolve: [{ action: 'skipOrDisplayUnlockCode' }]
};

export default skipOrDisplayUnlockCode;
