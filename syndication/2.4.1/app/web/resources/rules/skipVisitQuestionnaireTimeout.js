// Workflow logic for questionnaire timeout while skipping visit on the web app.
// It runs the same actions as it would for unsigned affidavit on SitePad.
const skipVisitQuestionnaireTimeout = {
    id: 'skipVisitQuestionnaireTimeout',
    trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Skip_Visits',
    salience: 3,
    resolve: [
        { action: 'notify', data: { key: 'DIARY_TIMEOUT' } },
        {
            action () {
                LF.router.flash({
                    subject: this.subject
                }).navigate(`visits/${this.subject.get('id')}`, true);

                return Q();
            }
        },
        { action: 'preventAll' }
    ]
};

export default skipVisitQuestionnaireTimeout;
