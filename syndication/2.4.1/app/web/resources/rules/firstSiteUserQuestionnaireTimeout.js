// Workflow logic for questionnaire timeout while first site user creation on the web app.
const firstSiteUserQuestionnaireTimeout = {
    id: 'firstSiteUserQuestionnaireTimeout',
    trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/First_Site_User',
    salience: 4,
    resolve: [
        { action: 'setState', data: 'locked' },
        { action: 'navigateTo', data: 'unlock-code' },
        { action: 'notify', data: { key: 'DIARY_TIMEOUT' } },
        { action: 'preventAll' }
    ]
};

export default firstSiteUserQuestionnaireTimeout;
