import trial from 'sitepad/trialMerge'; //PDE UPDATE changed to make trial import modality specific
import ELF from 'core/ELF';

// US7721 disabled until 2.5 release
// import fetchLockedOutSites from 'web/resources/rules/fetchLockedOutSites';
import firstSiteUserBackout from 'web/resources/rules/firstSiteUserBackout';
import inactiveTab from 'web/resources/rules/inactiveTab';
import passwordAffidavitLockout from 'web/resources/rules/passwordAffidavitLockout';
import preventOffline from 'web/resources/rules/preventOffline';
import questionnaireScreenAuthenticationByRole from 'web/resources/rules/questionnaireScreenAuthenticationByRole';
import siteChangeConfirmation from 'web/resources/rules/siteChangeConfirmation';
import skipOrDisplaySiteTimeZoneSelection from 'web/resources/rules/skipOrDisplaySiteTimeZoneSelection';
import skipOrDisplayUnlockCode from 'web/resources/rules/skipOrDisplayUnlockCode';
import transmitSiteTimeZone from 'web/resources/rules/transmitSiteTimeZone';
import uninstall from 'web/resources/rules/uninstall';
import questionnaireBackOut from 'web/resources/rules/questionnaireBackOut';
import webQuestionnaireCancel from 'web/resources/rules/webQuestionnaireCancel';
import firstSiteUserQuestionnaireTimeout from 'web/resources/rules/firstSiteUserQuestionnaireTimeout';
import siteUserQuestionnaireTimeout from 'web/resources/rules/siteUserQuestionnaireTimeout';
import skipTimeConfirmation from 'web/resources/rules/skipTimeConfirmation';
import affidavitQuestionnaireTimeout from 'web/resources/rules/affidavitQuestionnaireTimeout';
import skipVisitQuestionnaireTimeout from 'web/resources/rules/skipVisitQuestionnaireTimeout';
import affidavitSessionTimeout from 'web/resources/rules/affidavitSessionTimeout';
import newPatientQuestionnaireTimeout from 'web/resources/rules/newPatientQuestionnaireTimeout';
import databaseError from 'web/resources/rules/databaseError';

trial.assets.coreRules(ELF.rules);

// The following two rules are created in sitepad, and need to be removed.
ELF.rules.remove('FirstSiteUserBackout')
    .remove('QuestionnaireScreenAuthenticationByRole')
    .remove('QuestionnaireBackOut')
    .remove('QuestionnaireCancel')
    .remove('FirstSiteUserQuestionnaireTimeout')
    .remove('NewUserQuestionnaireTimeout')
    .remove('AffidavitQuestionnaireTimeout')
    .remove('SkipVisitQuestionnaireTimeout')
    .remove('ActivateUserSessionTimeout')
    .remove('ActivateUserCompletedTimeout')
    .remove('DeactivateUserSessionTimeout')
    .remove('DeactivateUserQuestionnaireTimeout')
    .remove('EditUserSessionTimeout')
    .remove('EditUserQuestionnaireTimeout')
    .remove('NewPatientSessionTimeout')
    .remove('NewPatientQuestionnaireTimeout')
    .remove('AffidavitSessionTimeout')
    .remove('NewUserSessionTimeout')
    .remove('SkipTimeConfirmation')
    .add([
    // US7721 disabled until 2.5 release
    // fetchLockedOutSites,
        firstSiteUserBackout,
        inactiveTab,
        passwordAffidavitLockout,
        preventOffline,
        questionnaireScreenAuthenticationByRole,
        siteChangeConfirmation,
        skipOrDisplaySiteTimeZoneSelection,
        skipOrDisplayUnlockCode,
        transmitSiteTimeZone,
        uninstall,
        questionnaireBackOut,
        webQuestionnaireCancel,
        firstSiteUserQuestionnaireTimeout,
        siteUserQuestionnaireTimeout,
        affidavitQuestionnaireTimeout,
        skipVisitQuestionnaireTimeout,
        skipTimeConfirmation,
        affidavitSessionTimeout,
        newPatientQuestionnaireTimeout,
        databaseError
    ]);
