// Prevent rules and default actions to be run in offline mode
const preventOffline = {
    id: 'preventOffline',
    trigger: [
        'WEB:PreventOffline',
        'QUESTIONNAIRE:Navigate'
    ],
    salience: Number.MAX_SAFE_INTEGER,
    evaluate: 'isOnline',
    resolve: [],
    reject: [
        { action: 'displayBanner', data: 'CONNECTION_REQUIRED' },
        { action: 'preventAll' }
    ]
};

export default preventOffline;
