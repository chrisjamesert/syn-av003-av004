import Data from 'core/Data';

/**
     * QuestionnaireCancel
     */
export default({
    id: 'webQuestionnaireCancel',
    trigger: 'QUESTIONNAIRE:Canceled',
    evaluate: 'isOnline',
    salience: 1,
    resolve: [{
        action: () => {
            return Q.Promise((resolve) => {
                if (Data.skippedVisits) {
                    Data.skippedVisits = undefined;
                }

                resolve(true);
            });
        }
    }],
    reject: [
        { action: 'displayBanner', data: 'CONNECTION_REQUIRED' },
        { action: 'preventAll' }
    ]
});
