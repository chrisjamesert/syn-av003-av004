// Transmits the site time zone data.
const transmitSiteTimeZone = {
    id: 'transmitSiteTimeZone',
    trigger: 'SETSITETIMEZONE:Transmit',
    resolve: [{ action: 'transmitSiteTimeZone' }]
};

export default transmitSiteTimeZone;
