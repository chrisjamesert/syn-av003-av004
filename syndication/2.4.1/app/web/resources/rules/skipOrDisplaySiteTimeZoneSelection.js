// Determines if the site timezone selection view should be dislayed.
const skipOrDisplaySiteTimeZoneSelection = {
    id: 'skipOrDisplaySiteTimeZoneSelection',
    trigger: 'LOGIN:Completed',
    resolve: [
        { action: 'displayMessage', data: 'PLEASE_WAIT' },
        { action: 'skipOrDisplaySiteTimeZoneSelection' },
        { action: 'removeMessage' }
    ]
};

export default skipOrDisplaySiteTimeZoneSelection;
