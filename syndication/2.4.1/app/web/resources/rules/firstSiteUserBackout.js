// Workflow logic for backing out of first site user creation on the web app.
const firstSiteUserBackout = {
    id: 'firstSiteUserBackout',
    trigger: 'QUESTIONNAIRE:BackOut/First_Site_User',
    salience: 2,
    evaluate: {
        expression: 'confirm',
        input: { key: 'BACK_OUT_CONFIRM' }
    },
    resolve: [
        { action: 'setState', data: 'locked' },
        { action: 'navigateTo', data: 'unlock-code' },
        { action: 'preventAll' }
    ],
    reject: [{ action: 'preventAll' }]
};

export default firstSiteUserBackout;
