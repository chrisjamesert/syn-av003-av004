// Workflow logic for questionnaire timeout in the web app.
// It runs the same actions as it would for unsigned affidavit on SitePad.
const affidavitQuestionnaireTimeout = {
    id: 'affidavitQuestionnaireTimeout',
    trigger: 'QUESTIONNAIRE:QuestionnaireTimeout',
    resolve: [
        { action: 'notify', data: { key: 'DIARY_TIMEOUT' } },
        {
            action () {
                LF.router.flash(this.defaultFlashParamsOnExit).navigate(this.defaultRouteOnExit, true);
                return Q();
            }
        }
    ]
};

export default affidavitQuestionnaireTimeout;
