// Workflow logic for questionnaire timeout in site user related questionnaires on the web app.
// It runs the same actions as it would for unsigned affidavit on SitePad.
const siteUserQuestionnaireTimeout = {
    id: 'siteUserQuestionnaireTimeout',
    trigger: [
        'QUESTIONNAIRE:QuestionnaireTimeout/New_User_SitePad',
        'QUESTIONNAIRE:QuestionnaireTimeout/Activate_User',
        'QUESTIONNAIRE:QuestionnaireTimeout/Deactivate_User',
        'QUESTIONNAIRE:QuestionnaireTimeout/Edit_User'
    ],
    salience: 3,
    resolve: [
        { action: 'navigateTo', data: 'site-users' },
        { action: 'notify', data: { key: 'DIARY_TIMEOUT' } },
        { action: 'preventAll' }
    ]
};

export default siteUserQuestionnaireTimeout;
