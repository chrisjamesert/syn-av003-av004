const skipTimeConfirmation = {
    id: 'SkipTimeConfirmation',
    trigger: [
        'APPLICATION:Loaded',
        'TIMESLIP:Retry'
    ],
    salience: 2,
    evaluate: 'shouldSkipTimeSlip',
    resolve: [{
        action: () => ({ stopRules: true })
    }, {
        action: 'navigateTo',
        data: ''
    }]
};

export default skipTimeConfirmation;
