// Displays a confirmation dialog when attempting to switch sites.
// If the confirmation is accepted, the application data is uninstall and site selection is displayed.
const siteChangeConfirmation = {
    id: 'siteChangeConfirmation',
    trigger: 'SITEASSIGNMENT:ConfirmChange',
    evaluate: {
        expression: 'confirm',
        input: { key: 'SITE_CHANGE_CONFIRM' }
    },
    resolve: [
        // The tab state needs to be cleared prior to the uninstall.
        { action: 'clearTabState' },
        { action: 'uninstall' },
        { action: 'refresh' }
    ],
    reject: [
        { action: 'preventAll' }
    ]
};

export default siteChangeConfirmation;
