// Gets all locked out sites from server
const fetchLockedOutSites = {
    id: 'fetchLockedOutSites',
    trigger: 'APPLICATION:Loaded',
    salience: 3,
    resolve: [
        { action: 'displayMessage', data: 'PLEASE_WAIT' },
        { action: 'fetchLockedOutSites' },
        { action: 'removeMessage' }
    ]
};

export default fetchLockedOutSites;
