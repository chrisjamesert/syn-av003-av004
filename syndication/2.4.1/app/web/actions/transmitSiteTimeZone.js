import COOL from 'core/COOL';
import Spinner from 'core/Spinner';
import Sites from 'core/collections/Sites';
import Transmit from 'web/transmit';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('transmitSiteTimeZone action');

/**
 * After a TimeZone Selection is completed this action will be called
 * to send the time zone to the server
 * @memberOf ELF.actions/web
 * @method transmitSiteTimeZone
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'transmitSiteTimeZone' }]
 */
export default function transmitSiteTimeZone () {
    return Spinner.show()
    .then(() => Sites.fetchFirstEntry())
    .then((site) => {
        let params = { siteCode: site.get('siteCode'), tz: lStorage.getItem('tzSwId') };

        return COOL.getService('Transmit', Transmit).transmitSiteTimeZone(params)
        .then(tz => [site, tz]);
    })
    .then(([site, tz]) => {
        logger.operational(`Timezone for this site: ${tz}`);
        return site.save({ timeZone: tz.res });
    })
    .catch(() => {
        return Q.reject({ preventActions: true });
    })
    .finally(() => Spinner.hide());
}

ELF.action('transmitSiteTimeZone', transmitSiteTimeZone);
