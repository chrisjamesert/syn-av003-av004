import ELF from 'core/ELF';
import COOL from 'core/COOL';
import TabManager from 'web/classes/TabManager';

/**
 * Clear the state of the tab via TabManager.clear.
 * @memberOf ELF.actions/web
 * @method clearTabState
 * @returns {Q.Promise<void>}
 */
export function clearTabState () {
    let tab = COOL.new('TabManager', TabManager);

    tab.clear();

    return Q();
}

ELF.action('clearTabState', clearTabState);
