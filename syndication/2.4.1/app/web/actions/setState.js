import ELF from 'core/ELF';
import State from 'web/classes/State';

/**
 * Set the current state of the application.
 * @memberOf ELF.actions/web
 * @method setState
 * @param {function} state - The desired state to set.
 * @returns {Q.Promise<void>}
 */
export function setState (state) {
    // DE21308 - If the state is not found, pass in the provided value instead.
    State.set(State.states[state] || state);

    return Q();
}

ELF.action('setState', setState);
