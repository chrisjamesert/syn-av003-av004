import Users from 'core/collections/Users';
import COOL from 'core/COOL';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';
import EULA from 'core/classes/EULA';
import State from 'web/classes/State';
import * as sync from 'core/actions/syncUsers';
import ELF from 'core/ELF';

/**
 * Evaluates if the user is an admin user
 * @private
 * @param {User} user - The user to check.
 * @returns {boolean}
 */
export function isAdminUser (user) {
    return user.get('role') === 'admin' && user.get('active') === 1;
}

/**
 * After Site selection is completed this action will be called to determine
 * if Unlock Code Screen should be displayed or skipped
 * @memberOf ELF.actions/web
 * @method skipOrDisplayUnlockCode
 * @returns {Q.Promise}
 * @example
 * resolve: [{ action: 'skipOrDisplayUnlockCode' }]
 */
export default function skipOrDisplayUnlockCode () {
    // Displays a confirmation modal with retry logic.
    let showDialog = (key) => {
        return Spinner.hide()
        .then(() => MessageRepo.display(MessageRepo.Dialog[key]))
        .then(() => Q.delay(1000))
        .then(() => skipOrDisplayUnlockCode.call(this));
    };

    return Spinner.show()
    .then(() => COOL.getClass('Utilities').isOnline())
    .then((onlineStatus) => {
        if (!onlineStatus) {
            return showDialog('CONNECTION_REQUIRED_ERROR');
        }

        // Passing true to the syncUsers action forces a reject on error.
        return sync.syncUsers(true)
        .then(() => Users.fetchCollection())
        .then((users) => {
            let filterFunction = LF.StudyDesign.sitePad.adminUserFilter || isAdminUser,
                adminUsers = users.filter(filterFunction);

            if (adminUsers.length) {
                State.set(State.states.activated);
                this.navigate('login');
            } else {
                if (EULA.isAccepted()) {
                    State.set(State.states.locked);
                    this.navigate('unlock-code');
                } else {
                    State.set(State.states.endUserLicenseAgreements);
                    this.navigate('eula');
                }
            }
        })
        .catch(() => {
            return showDialog('PLEASE_TRY_OR_CONTACT_SUPPORT');
        })
        .finally(() => Spinner.hide());
    });
}

ELF.action('skipOrDisplayUnlockCode', skipOrDisplayUnlockCode);
