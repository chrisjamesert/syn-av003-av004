import COOL from 'core/COOL';
import Logger from 'core/Logger';
import ELF from 'core/ELF';
import Data from 'core/Data';
import SitepadApplicationController from 'sitepad/controllers/ApplicationController';
import UniversalLogin from 'core/classes/UniversalLogin';
import LoginView from 'web/views/LoginView';
import SetPasswordView from 'sitepad/views/SetPasswordView';
import SecretQuestionView from 'sitepad/views/SecretQuestionView';
import SetSiteTimeZoneView from 'web/views/SetSiteTimeZoneView';
import WebFormGatewayView from 'web/views/WebFormGatewayView';
import UnsupportedBrowserView from 'web/views/UnsupportedBrowserView';
import InactiveTabView from 'web/views/InactiveTabView';
import TranscriptionQuestionnaireView from 'web/views/TranscriptionQuestionnaireView';
import WebQuestionnaireCompletionView from 'web/views/WebQuestionnaireCompletionView';
import WebQuestionnaireView from 'web/views/WebQuestionnaireView';

const logger = new Logger('Web:ApplicationController');

export default class ApplicationController extends SitepadApplicationController {
    /**
     * Return the appropriate form gateway view for the web modality.
     * @returns {View}
     */
    get formGatewayView () {
        return WebFormGatewayView;
    }

    /**
     * Display the TranscriptionQuestionnaireView.
     * @param {string} id - The ID of the questionnaire to display.
     * @param {Object} params - Parameters provided by Router.flash().
     * @param {Subject} params.subject - The target subject of the questionnaire.
     * @param {Visit} params.visit - The visit the questionnaire is being completed under.
     */
    transcribe (id, params) {
        let options = _.extend(params, { id });

        Data.Questionnaire = {};

        this.authenticateThenGo('TranscriptionQuestionnaireView', TranscriptionQuestionnaireView, options)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the dashboard (FormGatewayView) pending a permission check.
     * @param {string} permission - The permission object to use. e.g. 'diaryBackoutRoles'
     * @param {Object} parameters - Parameters provided by Router.flash().
     * @param {Subject} parameters.subject - The target subject of the dashboard.
     * @param {Visit} parameters.visit - The target visit used to display questionnaires.
     * @param {Object} [universalLoginOptions={}] options for the UniversalLogin constructor.  Will extend the defaults defined in this method.
     * @param {Object} [newUserLoginOptions={}] options for the new newUserLogin() call.  Will extend the defaults defined in this method.
     * @returns {undefined}
     */
    // eslint-disable-next-line max-params
    dashboardWithPermission (permission, parameters, universalLoginOptions = {}, newUserLoginOptions = {}) {
        let ulOptions = _.extend({
            // DE20581 - Switched universal login configuration to use the ContextSwitchingView, instead of LoginView.
            loginView: COOL.getClass('ContextSwitchingView', null)
        }, universalLoginOptions);

        return super.dashboardWithPermission(permission, parameters, ulOptions, newUserLoginOptions);
    }

    /**
     * Display the LoginView.
     * @param {Object} [universalLoginOptions={}] options for the UniversalLogin constructor.  Will extend the defaults defined in this method.
     * @param {Object} [newUserLoginOptions={}] options for the new newUserLogin() call.  Will extend the defaults defined in this method.
     */
    login (universalLoginOptions = {}, newUserLoginOptions = {}) {
        let successfulLogin = () => {
                return ELF.trigger('LOGIN:Completed', {}, this);
            },
            filterUsers = (user) => {
                return Q.Promise((resolve) => {
                    if (LF.StudyDesign.sitePad.loginRoles.indexOf(user.get('role')) > -1) {
                        resolve(user);
                    } else {
                        resolve(false);
                    }
                });
            },
            sortUsers = (users) => {
                return Q.Promise((resolve) => {
                    resolve((_.sortBy(users, (user) => {
                        // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                        return user.get('lastLogin') || new Date(1986, 1, 21).toString();
                    })).reverse());
                });
            };

        let universalLogin = new UniversalLogin(_.extend({
            loginView: LoginView,
            siteSelectionView: COOL.getClass('SiteSelectionView'),
            changeTempPasswordView: SetPasswordView,
            resetSecretQuestionView: SecretQuestionView,
            resetPasswordView: SetPasswordView,
            appController: this
        }, universalLoginOptions));

        universalLogin.newUserLogin(_.extend({ successfulLogin, filterUsers, sortUsers }, newUserLoginOptions));
    }

    /**
     * Display the SetTimeZoneActivationView.
     */
    setSiteTimeZone () {
        // DE21314 - Ensure a valid user is logged in prior to navigating to the set time zone view.
        this.authenticateThenGo('SetSiteTimeZoneView', SetSiteTimeZoneView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Displays the UnsupportedBrowserView.
     */
    unsupportedBrowser () {
        this.go('UnsupportedBrowserView', UnsupportedBrowserView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Displays the InactiveTabView.
     */
    inactiveTab () {
        this.go('InactiveTabView', InactiveTabView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the WebQuestionnaireView.
     * @param {string} id - The ID of the questionnaire to display.
     * @param {Object} parameters - Parameters provided by Router.flash().
     * @param {Subject} parameters.subject - The target subject of the questionnaire.
     * @param {Visit} parameters.visit - The visit the questionnaire is being completed under.
     * @returns {Q.Promise<void>}
     */
    questionnaire (id, parameters) {
        Data.Questionnaire = {};

        return this.go('WebQuestionnaireView', WebQuestionnaireView, {
            id,
            subject: parameters.subject,
            visit: parameters.visit
        })
        .catch(e => logger.error(e));
    }

    /**
     * Display the QuestionnaireCompletionView.
     */
    questionnaireCompletion () {
        this.go('WebQuestionnaireCompletionView', WebQuestionnaireCompletionView)
            .catch(e => logger.error(e))
            .done();
    }
}
