import SitepadSettingsController from 'sitepad/controllers/SettingsController';

import SettingsWebView from 'web/views/SettingsWebView';
import WebAboutView from 'web/views/WebAboutView';
import Logger from 'core/Logger';

let logger = new Logger('SettingsWebController');

export default class SettingsController extends SitepadSettingsController {
    /**
     * Navigate to the settings view.
     */
    index () {
        this.authenticateThenGo('SettingsWebView', SettingsWebView)
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * Navigate to the about view.
     */
    about () {
        this.authenticateThenGo('WebAboutView', WebAboutView)
            .catch(e => logger.error(e))
            .done();
    }
}
