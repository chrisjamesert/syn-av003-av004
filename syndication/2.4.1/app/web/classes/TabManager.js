import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import * as utils from 'core/utilities';
import ELF from 'core/ELF';
import COOL from 'core/COOL';

const logger = new Logger('TabManager');

// Key used in SessionStorage to maintain the tab's unique identifier.
const sessionKey = 'TAB_ID';

// Key used in LocalStorage to maintain the application's running tab ID.
const activeKey = 'ACTIVE_TAB_ID';

// Interval between reevaluating the active tab.
const checkInterval = 3 * 1000;

// Selector of a hidden form field to persist duplicate flag.
const selector = '#active-tab';

// Id of the interval being set to check the current active status of the tab.
let intervalId;

/**
 * Manage the running browser tabs.
 * @class TabManager
 */
export default class TabManager {
    // TODO: This class should better be static. Each new initialization creates a new interval which causes issues and memory leak.
    //       The properties should be module level variables
    constructor () {
        this.sessionTab = this.sessionTab || utils.createGUID();
        this.activeTab = this.activeTab || this.sessionTab;

        // When a browser unload event is triggered, we need to handle it by removing the active key.
        window.addEventListener('beforeunload', () => this.handleUnload());

        this.startMonitoring();
    }

    /**
     * Get the active tab.
     * @returns {string}
     */
    get activeTab () {
        return lStorage.getItem(activeKey);
    }

    /**
     * Set the active tab.
     * @param {string} id - The ID of the current tab.
     * @returns {string}
     */
    set activeTab (id) {
        logger.trace(`Current active tab: ${id}`);

        lStorage.setItem(activeKey, id);

        return id;
    }

    /**
     * Get the current tab's ID.
     * @returns {string}
     */
    get sessionTab () {
        return sessionStorage.getItem(sessionKey);
    }

    /**
     * Set the current tab's ID.
     * @param {string} id - The ID to set.
     * @returns {string}
     */
    set sessionTab (id) {
        logger.trace(`Current session tab: ${id}`);

        sessionStorage.setItem(sessionKey, id);

        return id;
    }

    /**
     * Determines if the tab is a duplicate.
     * @returns {boolean}
     */
    get isDuplicate () {
        return $(selector).val() === '1';
    }

    /**
     * Set the tab as a duplicate.
     * @param {boolean} value - True if duplicate, false if not.
     */
    set isDuplicate (value) {
        let output = value ? '1' : '0';

        $(selector).val(output);
    }


    /**
     * Starts monitoring the tabs to detect if there are multiple tabs being active at the same time
     */
    startMonitoring () {
        TabManager.stopMonitoring();

        // Set an interval to check the current active status of the tab.
        intervalId = window.setInterval(() => this.checkStatus(), checkInterval);
    }

    /**
     * Stops monitoring the tabs to detect if there are multiple tabs being active at the same time
     * @static
     */
    static stopMonitoring () {
        if (intervalId) {
            window.clearInterval(intervalId);
            intervalId = undefined;
        }
    }

    /**
     * Determines if the current tab is the active one.
     * @returns {boolean} True if active, false if not.
     */
    isActiveTab () {
        return this.sessionTab === this.activeTab;
    }

    /**
     * Activate the current tab.
     */
    activateTab () {
        this.activeTab = this.sessionTab;
    }

    /**
     * Checks if the tab is duplicate.
     */
    checkDuplicates () {
        if (this.isDuplicate) {
            // This tab is a duplicate and we need to assign a new Session ID.
            // When the status of the tab is checked, the Inactive Tab view will be displayed.
            this.sessionTab = utils.createGUID();
        } else {
            // Once the application has been loaded, we need to set the tab to active.
            // When the tab is duplicated the hidden form field will persist onto the duplicate tab.
            this.isDuplicate = true;
        }
    }

    /**
     * Handle a browser unload event.
     */
    handleUnload () {
        if (this.isActiveTab()) {
            this.isDuplicate = false;

            lStorage.removeItem(activeKey);
        }
    }

    /**
     * Check the status of the tab.
     */
    checkStatus () {
        if (!this.isActiveTab()) {
            ELF.trigger('APPLICATION:InactiveTab', {}, this)
            .done();
        }
    }

    /**
     * Clear the tab's state out of memory and localStorage.
     */
    clear () {
        // We need to clear the interval status checks prior to removing data from localStorage.
        // If we do not, and the interval happens to trigger, we could end up with a bad state.
        TabManager.stopMonitoring();

        this.isDuplicate = false;
        lStorage.removeItem(activeKey);
    }
}

COOL.add('TabManager', TabManager);
