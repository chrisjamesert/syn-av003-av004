import RouterBase from 'core/classes/RouterBase';
import * as lStorage from 'core/lStorage';
import Session from 'web/classes/Session';
import ScheduleManager from 'core/classes/ScheduleManager';
import trial from 'sitepad/trialMerge'; //PDE UPDATE changed to make trial import modality specific
import Spinner from 'core/Spinner';
import ELF from 'core/ELF';
import UserAgentParser from 'web/classes/UserAgentParser';

const studyRoutes = trial.assets.studyRoutes;

// Due to the many incompatibilities of Backbone with ES6 syntax,
// The routes have been defined outside of the class, and are passed directly
// into the Router's super call.
let routes = {
    // RegistrationController
    'site-assignment': 'registration#siteAssignment',
    'unlock-code': 'registration#unlockCode',
    'setup-user': 'registration#setupUser',
    eula: 'registration#eula',

    // ApplicationController
    // The routes listed below fall under the 'activated' workflow.
    login: 'application#login',
    home: 'application#home',
    'visits/:id': 'application#visitGateway',
    'questionnaire/:id': 'application#questionnaire',
    'transcribe/:id': 'application#transcribe',
    'edit-patient/:id': 'application#editPatient',
    'deactivate-patient': 'application#deactivatePatient',
    'site-users': 'application#siteUsers',
    'edit-user/:id': 'application#editUser',
    'add-new-patient': 'application#addNewPatient',
    dashboard: 'application#dashboard',
    'dashboard/:id': 'application#dashboardWithPermission',
    'add-site-user': 'application#addSiteUser',
    'activate-user/:id': 'application#activateUser',
    'deactivate-user/:id': 'application#deactivateUser',
    'skip-visits': 'application#skipVisits',
    'questionnaire-completion': 'application#questionnaireCompletion',
    'set-password': 'application#setPassword',
    'set-secret-question': 'application#setSecretQuestion',
    'account-configured': 'application#accountConfigured',
    'time-confirmation': 'application#timeConfirmation',
    blank: 'application#blank',
    'set-site-time-zone': 'application#setSiteTimeZone',
    'unsupported-browser': 'application#unsupportedBrowser',
    'inactive-tab': 'application#inactiveTab',

    // SettingsController
    settings: 'settings#index',
    about: 'settings#about',
    'console-log': 'settings#consoleLog',
    'customer-support': 'settings#customerSupport',
    'set-time-zone': 'settings#timeZoneSettings'
};

// It's not a good idea to import and extend the SitePad's Router here.
// It has a decent amount of routes defined in a closure that have nothing to bind to.
// We either need to extract the SPA's routes so they can be modified, or manually add the routes we want
// to the routes variable in this file.
export default class Router extends RouterBase {
    constructor (options = {}) {
        super(_.extend(options, {
            routes: _.extend({}, routes, options.routes, studyRoutes.web, {
                // This route needs to be last otherwise anything added after will match this first.
                '*path': UserAgentParser.isBrowserSupported(navigator.userAgent) ? 'registration#route' : 'application#unsupportedBrowser'
            })
        }));

        let language = lStorage.getItem('language'),
            locale = lStorage.getItem('locale');

        LF.security = new Session();
        LF.schedule = new ScheduleManager();
        LF.spinner = Spinner;

        LF.Preferred.language = language || LF.StudyDesign.defaultLanguage || LF.CoreSettings.defaultLanguage;
        LF.Preferred.locale = locale || LF.StudyDesign.defaultLocale || LF.CoreSettings.defaultLocale;
    }

    /**
     * Navigate to another view after checking online status
     * @param {string} fragment - URL fragment to navigate to.
     * @param {(Object|boolean)} options - to pass to Backbone.history.navigate().
     */
    navigate (fragment, options) {
        let currentView = this.view();

        $('#noty_top_layout_container').remove();

        if (currentView && (currentView.id === 'questionnaireCompletion-page' || fragment === currentView.defaultRouteOnExit || currentView.id === 'Skip_Visits' && fragment.substring(0, 6) === 'visits')) {
            super.navigate(fragment, options);
            return;
        }

        if (fragment === 'login' || fragment === 'time-confirmation') {
            super.navigate(fragment, options);
            return;
        }

        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }

            super.navigate(fragment, options);
        })
        .done();
    }
}
