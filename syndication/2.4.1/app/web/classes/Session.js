import CoreSession from 'core/classes/Session';
import State from 'web/classes/State';
import * as lStorage from 'core/lStorage';
import { refresh } from 'core/actions/refresh';
import COOL from 'core/COOL';
import WebService from 'web/classes/WebService';

export default class Session extends CoreSession {
    /**
     * Login into the application.
     * @param {number} id - The ID of the user to login as.
     * @param {string} password - The user entered password.
     * @returns {Q.Promise<User>}
     */
    login (id, password) {
        return super.login(id, password).tap((user) => {
            if (user != null) {
                // DE20581 - User_Login is required for maintaining a logged in state.
                // core/classes/Session doesn't set this value for some reason...
                lStorage.setItem('User_Login', user.get('id'));
            }
        });
    }

    /**
     * Log out of the current session.
     * @param {boolean} [navigate] - Determines if application should display the login view.
     * @returns {Q.Promise<void>}
     */
    logout (navigate) {
        return super.logout()
        .then(() => {
            lStorage.removeItem('isAuthorized');
            lStorage.removeItem('User_Login');

            // DE20581 - Added conditional navigate to the method.
            if (navigate) {
                LF.router.navigate('login', true);
            }
        });
    }

    /*
     * Checks the isLocked value of the user
     * @param {User} user The current user
     * @return (boolean) true - max login attempts is exceeded, false - max login attempts is not exceeded
     */
    /* checkLock (user) {
        return user.get('isLocked');
    }*/

    /*
     * Checks if max login attempts is exceeded and sets the isLocked value of the user
     * @param {User} user The current user
     * @return (boolean) true - max login attempts is exceeded, false - max login attempts is not exceeded
     */
    checkUserLockout (user) {
        let maxAttemptsExceeded = LF.StudyDesign.maxLoginAttempts && (LF.security.getLoginFailure(user) + 1 >= LF.StudyDesign.maxLoginAttempts),
            username = user.get('username'),
            siteCode = lStorage.getItem('siteCode'),
            webService = COOL.new('WebService', WebService),
            userRole = user.get('role'),
            isLocked = user.get('isLocked');


        return webService.checkUserLockout({ username, siteCode, userRole })
        .then((result) => {
            if (result.res.user === 0 && maxAttemptsExceeded || !isLocked && maxAttemptsExceeded) {
                return webService.setUserLockout({ username, siteCode, role: userRole, lockoutTime: LF.StudyDesign.lockoutTime })
                .then(() => user.set('isLocked', true).save())
                .then(() => true);
            } else if (result.res.locked) {
                return user.set('isLocked', true).save()
                .then(() => true);
            } else if (!result.res.locked && maxAttemptsExceeded && isLocked) {
                this.resetFailureCount(user);
                return user.set('isLocked', false).save()
                .then(() => false);
            }
            return user.set('isLocked', false).save()
            .then(() => false);
        });
    }

    /**
     * Default handler for the session timeout
     * @param {Backbone.View} currentView - The view that is being displayed when timeout happens
     */
    defaultSessionTimeoutHandler (currentView) {
        switch (State.get()) {
            case State.states.new:
                Backbone.history.loadUrl(undefined);
                break;
            case State.states.registered:
            case State.states.endUserLicenseAgreements:
                ELF.trigger('APPLICATION:Uninstall', {}, this)
                .done();
                break;
            case State.states.locked:
                if (LF.router.view().id === 'unlock-code-view') {
                    refresh();
                } else {
                    LF.router.navigate('unlock-code', true);
                }
                break;
            case State.states.activated: {
                // DE19403: These initializations are done to disable the date/time picker
                // when session timeout occurs
                let timeTravelDateBox = $('#timeTravelDateBox');
                let timeTravelTimeBox = $('#timeTravelTimeBox');
                timeTravelDateBox.length && timeTravelDateBox.datebox().datebox('close');
                timeTravelTimeBox.length && timeTravelTimeBox.datebox().datebox('close');
                $('.modal-backdrop').remove();
                timeTravelDateBox = null;
                timeTravelTimeBox = null;
                super.defaultSessionTimeoutHandler(currentView);
                break;
            }
            default:
                super.defaultSessionTimeoutHandler(currentView);
        }
    }
}
