/**
 * @typedef {Object} UserAgentParser~Parsed
 * @property {Object} browser - Encapsulates browser information
 * @property {string} browser.name - The name of the browser, for example 'Chrome'
 * @property {string} browser.version - The browser's version number
 * @property {Object} browser.cpu - Encapsulates the CPU information for the user agent
 * @property {string} cpu.architecture - The architecture information for the user agent
 * @property {Object} device - Encapsulates the device information for the user agent
 * @property {string} device.model - The device model
 * @property {string} device.vendor - The device vendor information
 * @property {string} device.type - The device type
 * @property {string} device.console - The device console
 * @property {string} device.mobile
 * @property {string} device.smarttv
 * @property {string} device.tablet
 * @property {string} device.wearable
 * @property {string} device.embedded
 * @property {Object} engine - Encapsulates the rendering engine details
 * @property {string} engine.name - The name of the rendering engine
 * @property {string} engine.version - The version information for the rendering engine
 * @property {Object} os - Encapsulates the OS information for the user agent
 * @property {string} os.name - The OS name
 * @property {string} os.version - The OS version
 */

import uaParser from 'ua-parser-js';

export default class UserAgentParser {
    /**
     * Parse the provided user agent string
     * @param {string} userAgent - The user agent string to be parsed.
     * @returns {UserAgentParser~Parsed}
     */
    static parse (userAgent) {
        return uaParser(userAgent);
    }

    /**
     * Parse the provided user agent string and determine if the browser meets the study-defined browser support
     * @param {string} userAgent - The user agent string to check
     * @returns {boolean}
     */
    static isBrowserSupported (userAgent) {
        const parsedUA = UserAgentParser.parse(userAgent),
            supportedBrowsers = LF.StudyDesign.supportedBrowsers,
            filteredBrowsers = _.where(supportedBrowsers, { name: parsedUA.browser.name }),
            checkVersion = (list) => {
                return _.findWhere(list, { version: '*' }) !== undefined ||
                    _.findWhere(list, { version: parsedUA.browser.version }) !== undefined;
            };

        switch (filteredBrowsers.length) {
            case 0:
                return false;
            case 1:
                return true;
            default:
                return checkVersion(filteredBrowsers);
        }
    }
}
