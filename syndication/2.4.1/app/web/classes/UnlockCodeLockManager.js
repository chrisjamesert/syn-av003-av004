import Logger from 'core/Logger';
import LockOutStates from 'web/collections/LockOutStates';
import LockOutState from 'web/models/LockOutState';

const logger = new Logger('UnlockCodeLockManager');

export default class UnlockCodeLockManager {
    constructor () {
        // TODO - Promises should never exist within a contstructor.
        LockOutStates.fetchCollection()
        .then((lockOutStates) => {
            this.lockOutState = lockOutStates.at(0) || new LockOutState();
        })
        .done();
    }

    /*
     * Checks if max unlock attempts is exceeded
     * @return (boolean) Returns a boolean value to indicate if max unlock attempts is exceeded
     */
    isLockedOut () {
        let maxUnlockAttempts = LF.StudyDesign.maxUnlockAttempts || LF.CoreSettings.maxUnlockAttempts,
            maxAttemptsExceeded = maxUnlockAttempts && (this.getUnlockFailure() >= maxUnlockAttempts),
            nowUtcTimestamp = new Date().getTime(),
            lockoutExpirationTimestamp = this.lockOutState.get('lockoutExpirationUtc'),
            lockoutExpired = lockoutExpirationTimestamp && (nowUtcTimestamp >= lockoutExpirationTimestamp);

        if (lockoutExpired) {
            logger.operational('Site unlock code lockout expired');

            this.resetUnlockFailure();
        }

        return maxAttemptsExceeded ? !lockoutExpired : false;
    }

    /*
     * Sets the site's failedUnlockAttempts to the current number of consecutive unsuccessful unlock attempts.
     * @param {Object} site - The current user
     * @param {number} attempts - current number of consecutive unsuccessful logon attempts to be saved
     * return {Q.Promise}
     */
    setUnlockFailure (attempts) {
        return this.lockOutState.set('failedUnlockAttempts', attempts).save();
    }

    /**
     * Return the current number of consecutive unsuccessful unlock attempts, if not return 0
     * @return {number}
     */
    getUnlockFailure () {
        let attempts = this.lockOutState.get('failedUnlockAttempts');

        return attempts != null ? parseInt(attempts, 10) : 0;
    }

    /**
     * Increments the failed unlock code attempts by 1
     * return {Q.Promise}
     */
    incrementUnlockFailure () {
        return this.setUnlockFailure(this.getUnlockFailure() + 1)
        .then(() => {
            if (this.isLockedOut()) {
                this.lockOut();
            }
        });
    }

    /**
     * Resets the failed unlock code attempts
     * return {Q.Promise}
     */
    resetUnlockFailure () {
        this.lockOutState.set('lockoutExpirationUtc', 0);
        this.lockOutState.set('failedUnlockAttempts', 0);
        return this.lockOutState.save();
    }

    /*
     * Locks out the unlock code entry for a site. Also sets the expiration time for the lockout.
     * @param {Site} site - The site to be locked out for unlockcode entry
     * return {Q.Promise}
     */
    lockOut () {
        let nowUtcTimestamp = new Date().getTime(),
            unlockCodeLockoutTime = LF.StudyDesign.unlockCodeLockoutTime || LF.CoreSettings.unlockCodeLockoutTime,
            unlockCodeLockoutTimeInMs = unlockCodeLockoutTime * 60 * 1000,
            lockoutExpirationUtcTimeStamp = nowUtcTimestamp + unlockCodeLockoutTimeInMs;

        return this.lockOutState.set('lockoutExpirationUtc', lockoutExpirationUtcTimeStamp).save();
    }
}
