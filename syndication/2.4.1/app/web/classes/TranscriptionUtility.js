import * as utils from 'core/utilities';
import { MessageRepo } from 'core/Notify';
import Data from 'core/Data';

/**
 * Contains any transcription functionality that needs to be shared across multiple modules.
 * @class TranscriptionUtility
 */
export default class TranscriptionUtility {
    /**
     * Wraps the provided navigation method with additional functionality concerning transcription of forms.
     * This is a temp solution, and should be revised at a later date as transcription functionality is expanded.
     * @param {function} navigate - The navigate method to wrap.
     * @returns {function} The wrapped navigation method.
     */
    static navigate (navigate) {
        return function (fragment, options = true, flashParams) {
            let form = Data.Questionnaire;

            // We only really care about cases where the 'dashboard' is to be displayed.
            // Instead of navigating to the dashboard, the application is configured to display the Questionnaire view.
            // In addition, we only want to execute the logic if the Questionnaire has been set into the Data object.
            // Note: There is an odd case where the navigate method is being executed twice, with no context, thus this != null
            if (this != null && fragment === 'dashboard' && form != null && form.model) {
                let { allowTranscriptionMode } = form.model.toJSON(),
                    isBackout = this.constructor.name === 'ContextSwitchingView' && this.backArrow == null;

                // If the target questionnaire is transcribable, and we're on the first screen of the questionnaire, and the mode hasn't already been selected.
                if (allowTranscriptionMode && form.screen === 0 && !form.modeSelected && !isBackout) {
                    let masterForm = LF.StudyDesign.questionnaires.findWhere({ id: form.model.get('id') }),
                        userRole = LF.security.activeUser.get('role'),
                        isTranscriberRole = (utils.getNested('StudyDesign.sitePad.transcriptionRoles', LF) || []).indexOf(userRole) !== -1,
                        isDualRole = false,

                        // We need to get the original attributes of the master form data to determine if the user's current role existed there.
                        previousAttrs = masterForm.previousAttributes(),
                        prompt = () => {
                            return Q.Promise((resolve) => {
                                MessageRepo.display(MessageRepo.Dialog.TRANSCRIBE_FORM_CONFIRMATION)
                                .then(() => resolve(true), () => resolve(false))
                                .done();
                            });
                        },
                        request;

                    if (previousAttrs.accessRoles != null && isTranscriberRole) {
                        isDualRole = previousAttrs.accessRoles.indexOf(userRole) !== -1;
                    }

                    // If the user is logging in as a 'dual role', meaning they have default access to the questionnaire and they are a transcriber,
                    // we need to prompt them if they want to transcribe the form.  By default confirmation is assumed.
                    request = isTranscriberRole ? prompt() : Q(true);

                    // eslint-disable-next-line consistent-return
                    request.then((confirmation) => {
                        let transcriptionMode = isTranscriberRole && confirmation,
                            screen = form.data.screens[form.screen],
                            screenAccessRoles = screen.get('accessRoles') || [];

                        // If the confirmation was denied, and the user isn't a dual role, we need to navigate back to the FormGateway
                        // DE21445 - If the user doesn't have access to the first screen, and clicks 'No' on the popup, we want to navigate back to the form gateway.
                        if (!confirmation && !isDualRole && screenAccessRoles.indexOf(userRole) === -1) {
                            let { subject, visit } = Data.Questionnaire;

                            // We need to use the global namespace to escape the wrapped method provided.
                            return LF.router.flash({ subject, visit })
                                .navigate('dashboard', true);
                        }

                        // If not transcription mode, we need to adjust the questionnaire-level access roles on the local copy of the questionnaire configuration.
                        // This prevents the user from switching to transcription mode once the standard workflow has been initiated.
                        if (!transcriptionMode) {
                            let accessRoles = masterForm.previousAttributes().accessRoles;

                            form.model.set('accessRoles', accessRoles);
                        } else {
                            // Since this is transcription mode, we need to prevent the subject or any non-transcriber roles from logging in on the first screen.
                            // This prevent's the user from switching to standard mode once the transcription workflow has been initiated.
                            let accessRoles = _.filter(form.model.get('accessRoles') || [], (role) => {
                                return LF.StudyDesign.sitePad.transcriptionRoles.indexOf(role) !== -1;
                            });

                            form.model.set('accessRoles', accessRoles);
                        }

                        // Loop through each screen and modify the accessRoles depending on if the form is in transcription mode.
                        form.data.screens.forEach((screen) => {
                            let accessRoles = screen.get('accessRoles');

                            if (accessRoles) {
                                if (transcriptionMode) {
                                    // Union of two arrays with unique values only.
                                    accessRoles = _.union(accessRoles, LF.StudyDesign.sitePad.transcriptionRoles);

                                    screen.set('accessRoles', accessRoles);
                                } else {
                                    // Fetch the master copy of the screen configuration.
                                    let masterCopy = LF.StudyDesign.screens.findWhere({ id: screen.get('id') });

                                    screen.set('accessRoles', masterCopy.get('accessRoles'));
                                }
                            }
                        });

                        // The mode of the form has been determined (standard vs. transcription).
                        form.modeSelected = true;

                        navigate(fragment, options, flashParams);
                    })
                    .done();
                } else {
                    navigate(fragment, options, flashParams);
                }
            } else {
                navigate(fragment, options, flashParams);
            }
        };
    }
}
