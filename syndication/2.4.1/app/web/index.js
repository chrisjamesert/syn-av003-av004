import COOL from 'core/COOL';

// Import all Core and SitePad actions.
import 'core/actions';
import 'sitepad/actions';
import 'web/actions';

// Import all Core and SitePad expressions.
import 'core/expressions';
import 'sitepad/expressions';
import 'web/expressions';

// @todo most of these are imported due to requirments of the study design.
import 'core/collections/Languages';
import 'core/collections/Templates';
import 'core/collections/StoredSchedules';
import 'core/collections/Logs';
import 'core/collections/SubjectAlarms';
import 'core/branching/branchingHelpers';
import 'sitepad/resources/Templates';
import 'sitepad/resources/Rules';
import 'web/resources/rules';
import 'web/views/ContextSwitchingView';
import 'web/views/SiteAssignmentView';

// Import SitePad Widgets
import 'sitepad/widgets/ListBase';
import 'sitepad/widgets/SkipReason';
import 'sitepad/widgets/EditReason';

// Import SitePad Visits
import 'sitepad/visits';

// Import Web Widgets
import 'web/widgets/PasswordWidget';

import 'core/widgets/LanguageSelectWidget';
import 'core/widgets/RoleSelectWidget';
import 'core/widgets/PasswordMessageWidget';

// Import all other study assets...
import 'trial/tablet';

import 'web/classes/WebService';
import Startup from 'web/classes/Startup';

$('select').click(() => {
    this.blur();
    this.focus();
});

COOL.new('Startup', Startup).startup();
