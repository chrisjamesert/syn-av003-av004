import COOL from 'core/COOL';
import PageView from 'core/views/PageView';
import TabManager from 'web/classes/TabManager';
import { MessageRepo } from 'core/Notify';

/*
 * Display an inactive tab message.
 * @class InactiveTabView
 * @extends PageView
 */
export default class InactiveTabView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string, string>} events - A list of events to bind to the DOM.
         */
        this.events = {
            'click #activate': 'activateHandler'
        };

        /**
         * @property {Object<string, string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            inactiveTabHeader: 'INACTIVE_TAB_HEADER',
            inactiveTab: 'INACTIVE_TAB',
            activateTab: 'ACTIVATE_TAB'
        };
    }

    /**
     * Returns the ID of the view.
     * @returns {string} The ID, defaults to inactive-browser-view
     */
    get id () {
        return 'inactive-tab-view';
    }

    /**
     * The ID of the EJS template associated with the view
     * @returns {string}
     */
    get template () {
        return '#inactive-tab-template';
    }

    get selectors () {
        return { activate: '#activate' };
    }

    /**
     * Event handler to ensure no errors are swallowed promise chains.
     */
    activateHandler () {
        this.activate().done();
    }

    /**
     * Make the current tab the active one.
     * @returns {Q.Promise<void>}
     */
    activate () {
        const { Dialog } = MessageRepo;

        this.disableButton(this.$activate);

        return MessageRepo.display(Dialog && Dialog.CONFIRM_ACTIVATE_TAB)
        .then(() => {
            let tab = COOL.new('TabManager', TabManager);

            tab.activateTab();

            // DE21784 - IndexedDB locked down once the app data was installed.
            // Implemented a complete refresh of the window to resolve.
            this.refresh();
        }, () => {
            this.enableButton(this.$activate);
        });
    }

    /**
     * Refresh the browser.
     */
    refresh () {
        window.location.reload();
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<Object>} Promise resolves with any translated strings.
     */
    render () {
        return this.buildHTML({}, true);
    }
}

COOL.add('InactiveTabView', InactiveTabView);
