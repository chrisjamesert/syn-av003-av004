import SitePadSecretQuestionView from 'sitepad/views/SecretQuestionView';
import COOL from 'core/COOL';

/**
 * Extended SecretQuestionView for web modality.
 * @class WebSecretQuestionView
 * @extends SecretQuestionView
 */
export default class WebSecretQuestionView extends SitePadSecretQuestionView {
    /**
     * Go to the next screen.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }
            super.next(e);
        })
        .done();
    }
}

COOL.add('SecretQuestionView', WebSecretQuestionView);
