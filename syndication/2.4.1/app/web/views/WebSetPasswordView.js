import SitePadSetPasswordView from 'sitepad/views/SetPasswordView';
import COOL from 'core/COOL';

/**
 * Extended SetPasswordView for web modality.
 * @class WebSetPasswordView
 * @extends SetPasswordView
 */
export default class WebSetPasswordView extends SitePadSetPasswordView {
    /**
     * Go to the next screen.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }
            super.next(e);
        })
        .done();
    }
}

COOL.add('SetPasswordView', WebSetPasswordView);
