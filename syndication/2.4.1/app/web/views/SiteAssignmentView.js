import PageView from 'core/views/PageView';
import Sites from 'core/collections/Sites';
import * as Utilities from 'core/utilities';
import CurrentContext from 'core/CurrentContext';
import Data from 'core/Data';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import WebService from 'web/classes/WebService';
import COOL from 'core/COOL';
import { MessageRepo } from 'core/Notify';
import State from 'web/classes/State';
import Transmit from 'web/transmit';
import ELF from 'core/ELF';

const logger = new Logger('SiteAssignment');

export default class SiteAssignmentView extends PageView {
    constructor (props) {
        super(props);

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and pass to the view's template.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            next: 'NEXT',
            site: 'SITE',
            siteNumber: 'SELECT_SITE_NUMBER',
            noResults: 'NO_RESULTS_FOUND',
            language: 'LANGUAGE'
        };

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': _.debounce(e => this.nextHandler(e), 300, true),
            'change #site': 'siteChanged',
            'change #languages': 'updateLanguage'
        };

        /**
         * @property {Sites} sites - The list of sites to render.
         */
        this.sites = new Sites();

        /**
         * @property {string} studyDbName - The defined study database name from a prior screen.
         */
        this.studyDbName = lStorage.getItem('studyDbName');
    }

    /**
     * Gets the View ID property
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'site-assignment-view'
     */
    get id () {
        return 'site-assignment-view';
    }

    /**
     * Gets the template for the View
     * @property {string} template - A selector that points to the template for the view.
     * @readonly
     * @default '#site-assignment-tpl'
     */
    get template () {
        return '#site-assignment-tpl';
    }

    /**
     * Gets the template for the Language Select Element
     * @property {string} id of the template for the language select options
     * @readonly
     * @default '#language-login-option'
     */
    get languageSelectionTemplate () {
        return '#language-login-option';
    }

    /**
     * Gets the selectors for the View
     * @property {Object.<string, string>} selectors - A list of selectors to populate.
     */
    get selectors () {
        return {
            site: '#site',
            next: 'button#next',
            form: '#site-assignment-form',
            languages: '#languages'
        };
    }

    /**
     * Render the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let preferredLang = lStorage.getItem('preferredLanguageLocale');
        if (preferredLang) {
            this.translateToSelectedLanguage(preferredLang);
        }
        return this.buildHTML({}, true)
        .then((strings) => {
            let siteCode = lStorage.getItem('siteCode');

            let service = COOL.new('WebService', WebService);

            service.getSites(this.studyDbName)
            .then(({ res }) => {
                return this.sites.add(res.map((site) => {
                    return {
                        id: site.krdom,
                        site_code: site.site_code,
                        siteCode: site.siteCode,
                        study_name: site.study_name,
                        hash: site.hash,
                        site_id: site.site_id
                    };
                }));
            })
            .then(() => {
                this.renderSites();
            })
            .then(() => {
                if (siteCode) {
                    this.$site.val(siteCode);
                    this.$site.trigger('change');
                }
            })
            .catch((err) => {
                MessageRepo.display(MessageRepo.Banner.NO_RESULTS_FOUND);
                logger.error('Sites not found error', err);
            });


            this.$site.select2({
                placeholder: strings.siteNumber,
                language: { noResults: () => strings.noResults },
                width: ''
            });


            this.$languages.data('select2') && this.$languages.select2('destroy').html('');

            this.$languages.select2({
                escapeMarkup: markup => markup,
                data: this.createLanguageSelectionData(),
                templateSelection: item => this.getTemplate(this.languageSelectionTemplate)({ item }),
                templateResult: item => this.getTemplate(this.languageSelectionTemplate)({ item }),
                minimumResultsForSearch: Infinity,

                // Defer width of select2 in favor of the CSS preferences for this element.
                width: ''
            });
            if (!preferredLang) {
                this.translateToSelectedLanguage(this.$languages.val());
            }
        });
    }

    /**
     * Render the current list of sites to the DOM.
     */
    renderSites () {
        this.sites.forEach((site) => {
            let el = `<option value="${site.get('siteCode')}">${site.get('siteCode')}</option>`;

            this.$site.append(el);
        });
    }

    /**
     * Click handler for the next button.
     * @param {(MouseEvent|TouchEvent)} evt - A click event object.
     */
    nextHandler (evt) {
        evt.preventDefault();

        this.disableButton(this.$next);

        this.next()

        // DE22450 - Only enable the button on error. next() will navigate away from the view upon success.
        .catch(() => this.enableButton(this.$next))
        .done();
    }

    /**
     * Next button workflow.
     * @returns {Q.Promise<void>}
     */
    next () {
        let siteCode = this.$site.val(),
            site = this.sites.findWhere({ siteCode });

        // Registers the device with StudyWorks/Expert.
        let register = () => {
            let service = COOL.new('WebService', WebService);

            // TODO: Only .isOnline() is actually registered with COOL.
            // Once this is resolved, Utilities should be fetched via COOL.
            let deviceUuid = Utilities.createGUID();

            let imei = Utilities.createGUID()
                .replace(/[\-\/]/g, '')
                .substr(0, 15);

            lStorage.setItem('deviceUUID', deviceUuid);

            return service.register({
                studyName: this.studyDbName,
                krdom: site.get('site_id'),
                siteCode,
                deviceUuid,
                imei
            })
            .then(({ res }) => {
                State.set(State.states.registered);
                lStorage.setItem('ucode', res.apiToken);
                lStorage.setItem('apiToken', res.apiToken);
                lStorage.setItem('deviceId', res.regDeviceId);
                lStorage.setItem('clientId', res.clientId);
                lStorage.setItem('IMEI', imei);
            });
        };

        let createSite = () => {
            if (!site.get('backupSiteId')) {
                site.set('backupSiteId', site.id);
            }

            site.set('site_id', site.get('backupSiteId'));
            site.unset('id', { silent: true });
            Data.site = site;
        };

        let getCountryCode = () => {
            return COOL.getService('Transmit', Transmit).getCountryCode();
        };

        return Q()
        .then(() => createSite())
        .then(() => register())
        .then(() => site.save())
        .then(() => getCountryCode())
        .then(() => ELF.trigger('SITEASSIGNMENT:Completed', {}, this));
    }

    /**
     * The site has changed.
     * @returns {Q.Promise<void>}
     */
    siteChanged () {
        const siteCode = this.$site.val();
        const resetEntry = () => {
            // we don't wan't to trigger 'change', could cause a stack overflow
            this.$site.select2().val('').trigger('change.select2');
            this.disableButton(this.$next);
        };
        const siteIsLockedOut = () => {
            let lockHasExpired,
                lockedSites = lStorage.getItem('LockedOutSites'),
                lockedSite = lockedSites && lockedSites.length && (JSON.parse(lockedSites))
                    .filter((site) => {
                        return site.siteCode === siteCode;
                    })[0],
                siteNotFound = !lockedSite;

            if (siteNotFound) {
                return false;
            }

            lockHasExpired = new Date().getTime() > lockedSite.lockExpires;

            return !lockHasExpired;
        };
        const notifyUser = () => {
            return Q()
            .then(() => MessageRepo.display(MessageRepo.Dialog.SITE_IS_LOCKED_OUT));
        };

        if (siteIsLockedOut()) {
            return notifyUser()
            .then(() => resetEntry())
            .done();
        }

        this.enableButton(this.$next);
        lStorage.setItem('siteCode', siteCode);

        return Q();
    }

    /**
     * Creates and returns language data
     * @returns {Array} data to populate the language dropdown list
     */
    createLanguageSelectionData () {
        let localized = LF.strings.match({ namespace: 'CORE', localized: {} }),
            langs = [];

        logger.traceEnter('createLanguageSelectionData');

        // get the localized names from the resource string files in order of 'language' and 'locale' keys
        localized.sort((firstComparatorObject, secondComparatorObject) => {
            let lang1 = firstComparatorObject.get('language'),
                lang2 = secondComparatorObject.get('language'),
                locale1 = firstComparatorObject.get('locale'),
                locale2 = secondComparatorObject.get('locale');
            if (lang1 > lang2) {
                return 1;
            } else if (lang1 < lang2) {
                return -1;
            }
            return locale1 > locale2 ? 1 : -1;
        }).forEach((language) => {
            let langCode = `${language.get('language')}-${language.get('locale')}`,
                lang = {
                    id: langCode,
                    localized: language.get('localized'),
                    fontFamily: Utilities.getFontFamily(langCode),
                    dir: language.get('direction'),
                    cssClass: language.get('direction') === 'rtl' ? 'right-direction text-right-absolute' : 'left-direction text-left-absolute',
                    selected: langCode === `${LF.Preferred.language}-${LF.Preferred.locale}`
                };

            langs.push(lang);
        });

        logger.traceExit('createLanguageSelectionData');

        return langs;
    }

    /**
     * Updates the view with the selected language-locale strings
     * when the user selects a new language
     */
    updateLanguage () {
        let lang = this.$languages.val();

        logger.operational('AdminUserLanguage ={{ language }} has been saved.', { language: lang });
        this.undelegateEvents();

        this.translateToSelectedLanguage(lang)
        .then(() => this.render())
        .done();
    }

    /**
     * Translate the UI to the selected user's preferred language.
     * @param {Object} lang - The selected language
     * @returns {Q.Promise<boolean>}
     * @example this.translateToUserLanguage();
     */
    translateToSelectedLanguage (lang) {
        CurrentContext().setContextLanguage(lang);
        lStorage.setItem('preferredLanguageLocale', lang);
        return Q(true);
    }
}

COOL.add('SiteSelectionView', SiteAssignmentView);
