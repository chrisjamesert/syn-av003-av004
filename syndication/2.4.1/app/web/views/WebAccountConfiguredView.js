import AccountConfiguredView from 'sitepad/views/AccountConfiguredView';
import COOL from 'core/COOL';
import TranscriptionUtility from 'web/classes/TranscriptionUtility';

/**
 * Extended AccountConfiguredView for web modality.
 * @class WebAccountConfiguredView
 * @extends AccountConfiguredView
 */
export default class WebAccountConfiguredView extends AccountConfiguredView {
    /**
     * Mixes in transcription workflow to the navigate method of the AccountConfiguredView.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        this.navigate = TranscriptionUtility.navigate(this.navigate);

        // Check online status before moving forward.
        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }
            super.next(evt);
        })
        .done();
    }
}

COOL.add('AccountConfiguredView', WebAccountConfiguredView);
