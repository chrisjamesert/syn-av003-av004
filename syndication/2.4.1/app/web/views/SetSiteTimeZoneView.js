import SetTimeZoneActivationViewCore from 'core/views/SetTimeZoneActivationView';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import State from 'sitepad/classes/State';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import Sites from 'core/collections/Sites';
import COOL from 'core/COOL';

let logger = new Logger('setSiteTimeZone');

/*
 * @class SetTimeZoneActivationView
 * @description View for setting the time zone during Sitepad activation
 * @extends SetTimeZoneActivationViewCore
 */
export default class SetSiteTimeZoneView extends SetTimeZoneActivationViewCore {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string, string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            timezoneMessage: 'SET_TIMEZONE_MESSAGE',
            back: 'BACK',
            next: 'NEXT'
        };
        this.sites = new Sites();
    }

    /**
     * Gets the View ID property
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'set-tz-activation-view'
     */
    get id () {
        return 'set-site-tz-view';
    }

    /**
     * Gets the template for the View
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#set-time-zone-activation-template'
     */
    get template () {
        return '#set-site-time-zone-template';
    }

    /**
     * Navigates back to the code entry view.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    back (e) {
        e.preventDefault();
        State.set(State.states.activated);
        lStorage.removeItem('tzSwId');
        this.navigate('login');
        return Q();
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return super.resolve()
        .then(() => this.sites.fetch());
    }

    /**
     * Enable the next button based on selection.
     */
    chooseZone () {
        super.chooseZone();

        // Defaults to 'America/New_York'.
        let tzSwId = this.sites.at(0).get('timeZone') || 21,
            selected = this.$selectTimeZone.select2('data')[0],
            tzConfig = _.findWhere(LF.StudyDesign.web.timeZoneOptions, { tzId: selected.id });

        if (tzConfig) {
            tzSwId = tzConfig.swId;
        } else {
            logger.warn('No configuration found for selected time zone. Defaulting to "America/New_York".');
        }

        lStorage.setItem('tzSwId', tzSwId);
    }

    /**
     * Navigates to the next screen.
     * @returns {Q.Promise<void>}
     */
    navigateNext () {
        State.set(State.states.activated);
        return this.navigate('home');
    }

    /**
     * Sets the Timezone selected.
     * @returns {Q.Promise<void>}
     */
    setTimeZone () {
        // Returning a Promise is entirely for testability.
        // This method is invoked via a click event, and doesn't live within a promise chain.
        return Q.Promise((resolve) => {
            let selected = this.$selectTimeZone.select2('data')[0];

            // Handles case where the end user cancels the timezone change, or lack thereof.
            let cancel = () => {
                logger.operational(`User canceled setting the time zone to: ${selected.displayName}`);

                // DE17472 - If canceled, enable the next button again.
                this.enableButton(this.$next);
                resolve();
            };

            LF.DynamicText.selectedTZName = selected.displayName;

            // DE17472 - Disable the next button so it can't be clicked twice.
            this.disableButton(this.$next);

            // If the device's current timezone is different than the one selected...
            if (this.currentTimeZone.id !== selected.id) {
                this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM })
                .then((res) => {
                    if (!res) {
                        cancel();
                        return;
                    }

                    this.changeTimeZone()
                    .then(() => resolve())
                    .done();
                })
                .done();
            } else {
                // The time zone hasn't been changed. Display a message informing the user of no change.
                this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM_NOCHANGE })
                .then((res) => {
                    if (res) {
                        ELF.trigger('SETSITETIMEZONE:Transmit', {}, this)
                        .then(() => {
                            logger.operational(`User used the device's Time zone: ${selected.displayName}`);
                            resolve();
                            this.navigateNext();
                        })
                        .done();
                    } else {
                        cancel();
                    }
                })
                .done();
            }
        });
    }

    /**
     * Changes the Timezone.
     * @param {string} timezone TimeZone object.
     * @returns {Q.Promise<void>}
     */
    changeTimeZone () {
        return ELF.trigger('SETSITETIMEZONE:Transmit', {}, this)
        .then(() => this.navigateNext());
    }
}

COOL.add('SetSiteTimeZoneView', SetSiteTimeZoneView);
