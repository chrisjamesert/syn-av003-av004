import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import SettingsView from 'sitepad/views/SettingsView';

export default class SettingsWebView extends SettingsView {
    /**
     * Render the view to the DOM for the Web app ('Set Time Zone' option is hidden)
     * @returns {Q.Promise<void>}
     */
    render () {
        return super.render()
        .then(() => {
            let mode = lStorage.getItem('mode');
            if (mode === 'web') {
                this.hideElement(this.$('#set-timezone'));
            }
        });
    }
}

COOL.add('SettingsWebView', SettingsWebView);
