import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import * as utils from 'core/utilities';
import SitePadQuestionnaireView from 'sitepad/views/SitePadQuestionnaireView';

export default class WebQuestionnaireView extends COOL.getClass('SitePadQuestionnaireView', SitePadQuestionnaireView) {
    /**
     * Create a dashboard record.
     * @param {string} id - The id of the current questionnaire.
     * @returns {Q.Promise<void>}
     */
    prepDashboardData (id) {
        return super.prepDashboardData(id)
        .tap(() => {
            let { dashboard } = this.data,
                prefix = utils.getNested('StudyDesign.web.sigIdPrefix', LF) || 'SA';

            if (dashboard) {
                dashboard.set('sig_id', `${prefix}.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`);
            }
        });
    }
}
