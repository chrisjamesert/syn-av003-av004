import COOL from 'core/COOL';
import PageView from 'core/views/PageView';

/*
 * Unsupported browser view
 * @class UnsupportedBrowserView
 * @extends PageView
 */
export default class UnsupportedBrowserView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string, string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            unsupportedBrowser: 'UNSUPPORTED_BROWSER'
        };
    }

    /**
     * @property The ID of the view.
     * @returns {string} The ID, defaults to unsupported-browser-view
     */
    get id () {
        return 'unsupported-browser-view';
    }

    /**
     * The ID of the EJS template associated with the view
     * @property
     * @returns {string}
     */
    get template () {
        return '#unsupported-browser-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML({}, true);
    }
}

COOL.add('UnsupportedBrowserView', UnsupportedBrowserView);
