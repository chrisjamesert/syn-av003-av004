import FormGatewayView from 'sitepad/views/FormGatewayView';
import Logger from 'core/Logger';
import Data from 'core/Data';
import ELF from 'core/ELF';
import COOL from 'core/COOL';

// eslint-disable-next-line no-unused-vars
const logger = new Logger('WebFormGatewayView');

export default class WebFormGatewayView extends FormGatewayView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,function>} events - A list of DOM events.
         * @readonly
         */
        this.events = _.defaults({
            'click #start-form-transcription': _.debounce(() => {
                this.startTranscription();
            }, 2000, true)
        }, this.events);

        /**
         * @property {Object<string,string>} templateStrings - A collection of resource strings for the template.
         * @readonly
         */
        this.templateStrings = _.defaults({
            startTranscription: 'START_TRANSCRIPTION_MODE'
        }, this.templateStrings);
    }

    /**
     * Handle the selection of a row.
     * @param {FormRow} formRow - The selected form row.
     */
    select (formRow) {
        super.select(formRow);

        let { allowTranscriptionMode, status } = formRow.toJSON();

        if (status === 'available' && allowTranscriptionMode) {
            this.enableButton('#start-form-transcription');
        } else {
            this.disableButton('#start-form-transcription');
        }
    }

    /**
     * Handle the de-selection of a row.
     */
    deselect () {
        super.deselect();

        this.disableButton('#start-form-transcription');
    }

    /**
     * Open a form.
     * @param {string} questionnaireId - ID of the questionnaire.
     * @returns {Q.Promise<void>}
     */
    openQuestionnaire (questionnaireId) {
        let id = questionnaireId || this.selected.get('id'),
            questionnaire = LF.StudyDesign.questionnaires.findWhere({ id }),
            subjectIsNotLoggedIn = LF.security.activeUser.get('role') !== 'subject';

        /**
         * DE22408 - If user is not online then don't remove activeUser.
         * Otherwise, it will cause errors and not display the
         * CONNECTION_REQUIRED banner.
         */
        return COOL.getClass('Utilities').isOnline()
        .then((isOnline) => {
            // requiredLogin is dynamically added to the questionnaire model by web/classes/WebStudyDesign
            // The property indicates that the target questionnaire was originally subject-only, and a new login
            // prompt is required to allow for selecting which user should complete the questionnaire.
            if (questionnaire.get('requireLogin') && subjectIsNotLoggedIn && isOnline) {
                // If this value is not set, sitepad/actions/questionnaireScreenAuthenticationByRole will trigger a Context Switch.
                delete LF.security.activeUser;
            }

            return super.openQuestionnaire(questionnaireId);
        });
    }

    /**
     * Handles a click of the "start transcription mode" button
     */
    startTranscription () {
        let id = this.selected.get('id');

        // Trigger an OpenQuestionnaire event via ELF.
        return ELF.trigger(`DASHBOARD:TranscribeQuestionnaire/${id}`, { questionnaire: id }, this)
        .then((res) => {
            if (res.preventDefault) {
                return Q();
            }

            // DE12483: Delay opening questionnaire to prevent the issue of questionnaire screen display
            return Q.delay(150)
            .then(() => {
                Data.Questionnaire = {};

                this.navigate(`transcribe/${id}`, true, {
                    subject: this.subject,
                    visit: this.visit
                });
            });
        });
    }
}
