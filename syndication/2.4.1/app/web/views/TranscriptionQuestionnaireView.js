import WebQuestionnaireView from 'web/views/WebQuestionnaireView';
import * as DateTimeUtil from 'core/DateTimeUtil';

/**
 * Enables transcription mode on a questionnaire.
 * @class TranscriptionQuestionnaireView
 * @extends WebQuestionnaireView
 */
export default class TranscriptionQuestionnaireView extends WebQuestionnaireView {
    constructor (options) {
        super(options);

        /**
         * @property {boolean} transcriptionMode - Flag used to determine if the questionnaire is being completed in transcription mode.
         */
        this.transcriptionMode = true;
    }

    /**
     * Set the report date. Invoked by ELF rule upon completion of the injected report date screen.
     * @returns {Q.Promise<void>}
     */
    setReportDate () {
        let screens = this.data.screens,
            currentScreen = screens[this.screen].get('id');

        // Only change the report date when navigating away from the transcription screen.
        if (currentScreen === 'TRANSCRIPTION_REPORT_DATE_S_1') {
            let answer = this.answers.findWhere({ SW_Alias: 'TRANSCRIPTION_REPORT_DATE.0.DATE_TIME' }),
                dt = new Date(answer.get('response'));

            // StudyWorks - Report Start Date and Time.
            // Transmitted as 'S'
            this.data.dashboard.set('started', dt.ISOStamp());

            // StudyWorks - Report Date
            // Transmitted as 'R'
            this.data.dashboard.set('report_date', DateTimeUtil.convertToDate(dt));
        }

        return Q();
    }

    prepScreens () {
        super.prepScreens();

        this.data.screens.unshift(LF.StudyDesign.screens.get('TRANSCRIPTION_REPORT_DATE_S_1'));

        return this.data.screens;
    }

    /**
     * Inject the transcription report date screen into the questionnaire.
     */
    injectScreen () {
        let screens = this.model.get('screens');

        screens.unshift('TRANSCRIPTION_REPORT_DATE_S_1');

        this.model.set('screens', screens);
    }
}
