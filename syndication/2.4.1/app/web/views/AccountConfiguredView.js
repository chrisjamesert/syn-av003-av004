import SitePadAccountConfiguredView from 'sitepad/views/AccountConfiguredView';
import COOL from 'core/COOL';

export default class AccountConfiguredView extends SitePadAccountConfiguredView {
    /**
     * Go to the next screen.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }
            super.next(e);
        })
        .done();
    }
}

COOL.add('AccountConfiguredView', AccountConfiguredView);
