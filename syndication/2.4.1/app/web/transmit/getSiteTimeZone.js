import COOL from 'core/COOL';
import WebService from 'web/classes/WebService';
import Sites from 'core/collections/Sites';

/**
 * Gets the time zone for the sitefrom the server.
 * @returns {Q.Promise<void>} Q Promise
 */
export default function getSiteTimeZone () {
    let service = COOL.new('WebService', WebService);

    return Sites.fetchFirstEntry()
    .then((site) => {
        let params = {
            siteCode: site.get('siteCode')
        };
        return service.getSiteTimeZone(params);
    });
}
