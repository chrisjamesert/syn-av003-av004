import COOL from 'core/COOL';
import WebService from 'web/classes/WebService';

/**
 * Handles the site time zone transmission to the web-service.
 * @param {Object} params - The Request Body to transmit.
 * @param {string} params.siteCode - site code that the tz is being saved for.
 * @param {string} params.tz - the study works id of the timezone.
 * @returns {Q.Promise<Object>}
 */
export default function transmitSiteTimeZone (params) {
    let service = COOL.new('WebService', WebService);

    return service.transmitSiteTimeZone(params);
}
