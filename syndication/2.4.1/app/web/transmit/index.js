import COOL from 'core/COOL';
import SPTransmit from 'sitepad/transmit';

import transmitSiteTimeZone from './transmitSiteTimeZone';
import getSiteTimeZone from './getSiteTimeZone';

let Transmit = _.extend({}, SPTransmit, {
    transmitSiteTimeZone,
    getSiteTimeZone
});

COOL.service('Transmit', Transmit);

LF.Transmit = Transmit;

export default Transmit;
