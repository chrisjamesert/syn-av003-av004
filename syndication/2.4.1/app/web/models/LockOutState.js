import StorageBase from 'core/models/StorageBase';

/**
 * A model to store a LockOutState record.
 * @extends StorageBase
 */
export default class LockOutState extends StorageBase {
    constructor (options) {
        super(options);

        /**
         * The model's name
         * @readonly
         * @type String
         * @default 'LockOutState'
         */
        this.name = 'LockOutState';
    }

    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type {Object}
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            failedUnlockAttempts: {
                type: Number,
                encrypt: true
            },
            lockoutExpirationUtc: {
                type: Number,
                encrypt: true
            }
        };
    }
}
