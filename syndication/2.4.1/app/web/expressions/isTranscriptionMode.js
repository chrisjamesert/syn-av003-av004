import ELF from 'core/ELF';

/**
 * Determines if the current view is running in transcription mode.
 * @memberOf ELF.expressions/web
 * @method isTranscriptionMode
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'isTranscriptionMode'
 */
export function isTranscriptionMode () {
    return Q(!!this.transcriptionMode);
}

ELF.expression('isTranscriptionMode', isTranscriptionMode);
