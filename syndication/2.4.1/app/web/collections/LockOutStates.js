import LockOutState from 'web/models/LockOutState';
import StorageBase from 'core/collections/StorageBase';

/**
 * A collection of LockOutState models.
 * @class {LockOutStates<T>}
 * @extends {Backbone.Collection<T>}
 */
export default class LockOutStates extends StorageBase {
    /**
     * @property {LockOutState} model - The model class the collection contains.
     */
    get model () {
        return LockOutState;
    }
}
