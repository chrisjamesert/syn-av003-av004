IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_AddUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LPA_AddUser]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_UpdateUserCredentials]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LPA_UpdateUserCredentials]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_SyncUsers_XML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LPA_SyncUsers_XML]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[synd_UpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[synd_UpdateUser]
GO

IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE id = object_id(N'[dbo].[LogPadAppUser]'))
Begin
	CREATE TABLE [dbo].[LogPadAppUser](
		userId int identity (1,1) primary key,
		user_type VARCHAR(36),
		userName NVARCHAR(36),
		lang VARCHAR(36),
		pwd NVARCHAR(128),
		keys VARCHAR(36),
		userRole VARCHAR(36),
		secret_question VARCHAR(36),
		secret_answer NVARCHAR(128),
		sync_level VARCHAR(36),
		sync_value VARCHAR(36),
		active tinyint,
		updated DATETIME
	);
End

GO

CREATE PROCEDURE LPA_AddUser
	( @user_type varchar(36),
	  @userName nvarchar(36),
	  @lang varchar(36),
	  @pwd nvarchar(128),
	  @keys varchar(36),
	  @secret_question varchar(36),
      @secret_answer nvarchar(128),
	  @userRole varchar(36),
	  @sync_level varchar(36),
	  @sync_value varchar(36),
	  @active tinyint = 1
)
AS
/*****************************************************************************************************************
LPA_AddUser - Add User
Version	Date		Change
1.00	07Oct2015	[bcalderwood]	Initial Release
1.01	12Feb2016	[dyildiz]		@active parameter is added
1.02	22Apr2016	[dyildiz]		Fixed returning null instead of duplicate error code
1.03	28Aug2016	[againessmith]	Updated for the 128 length SHA2 value
*****************************************************************************************************************/

set nocount on
DECLARE @retval varchar(5) = 'S'

BEGIN TRY
	--- check to see if the user exists
	IF EXISTS (SELECT 1 
				 FROM dbo.LogPadAppUser WITH(NOLOCK)
				WHERE [userName] = @userName
				  AND [userRole] = @userRole
				  AND sync_level = @sync_level
				  AND sync_value = @sync_value
			  )
	BEGIN
			SET @retval = 'D';
	END ELSE BEGIN
		-- if the user does not exist then add it.
		INSERT 
		  INTO dbo.LogPadAppUser (
					user_type,
					[userName],
					[lang],
					[pwd],
					[keys],
					[secret_question],
                    [secret_answer],
					[userRole],
					[sync_level],
					[sync_value],
					[active],
					[updated]
				)
		SELECT 
			@user_type,
			@userName,
			@lang,
			@pwd,
			@keys,
			@secret_question,
            @secret_answer,
			@userRole,
			@sync_level,
			@sync_value,
			@active,
			GETUTCDATE()

		SET @retval = SCOPE_IDENTITY();
	END
END TRY

BEGIN CATCH
	SET @retval = 'E';
END CATCH

SELECT @retval;

GO

CREATE PROCEDURE LPA_UpdateUserCredentials
	( @userId int,
	  @pwd nvarchar(128),
	  @keys varchar(36),
	  @secret_question varchar(36),
	  @secret_answer nvarchar(128)
) 
AS
/*****************************************************************************************************************
LPA_UPdateUserCredentials - Update User Credentials
Version	Date		Change
1.00	07Oct2015	[bcalderwood] Initial Release
1.01	12Feb2016	[dyildiz]     Changes due to renamed columns in LogPadAppUser
1.02	28Aug2016	[againessmith] Updated for the 128 length SHA2 value
*****************************************************************************************************************/

set nocount on
DECLARE @retval varchar(5) = 'S'

BEGIN TRY
	--- check to see if the user exists
	IF EXISTS (SELECT 1 
				 FROM dbo.LogPadAppUser WITH(NOLOCK)
				WHERE [userId] = @userId
			  )
		UPDATE dbo.LogPadAppUser 
			SET [pwd] = @pwd,
				[keys] = @keys,
				[secret_question] = @secret_question,
				[secret_answer] = @secret_answer,
				[updated] = GETUTCDATE()
		  WHERE [userId] = @userId;

	ELSE
		SET @retval = 'E';
END TRY

BEGIN CATCH
	SET @retval = 'E';
END CATCH

SELECT @retval;

GO

SET QUOTED_IDENTIFIER ON -- 20Jul19 Reset to default QUOTED_IDENTIFIER ON to ensure XML / XQuery commands run correctly.
GO

CREATE PROCEDURE dbo.LPA_SyncUsers_XML
    ( @utcDateTime DATETIME,
      @sync_level varchar(36),
      @sync_value varchar(36)
) 
AS
/*****************************************************************************************************************
LPA_SyncUsers_XML - Sync Users
Version Date        Change
1.01	16Dec2015	[dyildiz] Changes to return data in XML format
*****************************************************************************************************************/

SET nocount ON

DECLARE @results XML
DECLARE @lastUpdated DATETIME

SELECT @results = (
	SELECT  user_type userType,
			userName username,
			lang [language],
			pwd [password],
			KEYS salt,
			sync_value syncValue,
			CAST(userId AS Varchar(36)) userId,
			secret_question secretQuestion,
			secret_answer secretAnswer,
			CAST(active AS Varchar(36)) active,
			userRole [role]
	FROM	dbo.LogPadAppUser (NOLOCK)
	WHERE	sync_level = @sync_level
			AND sync_value = @sync_value
			AND (@utcDateTime IS NULL OR updated > @utcDateTime)
	FOR XML RAW ('user'), ROOT('Results')
)

SET @results = ISNULL(@results, '<Results></Results>')
SET @lastUpdated = GETUTCDATE()
SET @results.modify('insert attribute lastUpdated {sql:variable("@lastUpdated")} into (/Results)[1]')

SELECT @results
GO

CREATE PROCEDURE synd_UpdateUser(
    @userId int,
    @user_type varchar(36),
    @userName nvarchar(36),
    @lang varchar(36),
    @userRole varchar(36),
    @sync_level varchar(36),
    @sync_value varchar(36),
    @active tinyint
)
AS
/*****************************************************************************************************************
synd_UpdateUser - Update User
Version Date        Change
1.00    02Dec2016   [dyildiz] Initial Release
*****************************************************************************************************************/

SET NOCOUNT ON

DECLARE @retval varchar(5)

BEGIN TRY
    -- Check to see if the update will create a duplicate
    IF EXISTS(  SELECT  1
                FROM    dbo.LogPadAppUser (NOLOCK)
                WHERE   userName = @userName
                        AND userRole = @userRole
                        AND sync_level = @sync_level
                        AND sync_value = @sync_value
                        AND userId <> @userId)
    BEGIN
        -- Error code for Duplicate User
        SET @retval = 'D';
    END ELSE IF NOT EXISTS( SELECT  1
                            FROM    dbo.LogPadAppUser (NOLOCK)
                            WHERE   userId = @userId)
    BEGIN
        -- Error code for No user exists with the passed userId
        SET @retval = 'N';
    END ELSE BEGIN
        UPDATE  dbo.LogPadAppUser
        SET     user_type = @user_type,
                userName = @userName,
                lang = @lang,
                userRole = @userRole,
                sync_level = @sync_level,
                sync_value = @sync_value,
                active = @active,
                updated = GETUTCDATE()
        WHERE   userId = @userId

        SET @retval = 'S';
    END
END TRY

BEGIN CATCH
    -- Error code for unexpected error
    SET @retval = 'E';
END CATCH

SELECT @retval;
GO

/*LPA_AddUser Version / permissions */
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_AddUser' AND TYPE = 'P')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'LPA_AddUser', @version = 1.03 -- UPDATE VERSION NUMBER HERE
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GRANT EXECUTE ON LPA_AddUser TO pht_server_read_only

GO

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'LPA_AddUser') BEGIN
INSERT [allowed_clin_procs] ([proc_name]) VALUES ('LPA_AddUser')
END

/*LPA_UpdateUserCredentials Version / permissions */
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_UpdateUserCredentials' AND TYPE = 'P')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'LPA_UpdateUserCredentials', @version = 1.02 -- UPDATE VERSION NUMBER HERE
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GRANT EXECUTE ON LPA_UpdateUserCredentials TO pht_server_read_only

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'LPA_UpdateUserCredentials') BEGIN
INSERT [allowed_clin_procs] ([proc_name]) VALUES ('LPA_UpdateUserCredentials')
END

GO

/*LPA_SyncUsers_XML Version / permissions */

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_SyncUsers_XML' AND TYPE = 'P')
BEGIN
    declare @name as varchar(50)
    declare @version as numeric(18,2)

    select @name = 'LPA_SyncUsers_XML', @version = 1.01 -- UPDATE VERSION NUMBER HERE
    exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GRANT EXECUTE ON LPA_SyncUsers_XML TO pht_server_read_only

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'LPA_SyncUsers_XML') BEGIN
INSERT [allowed_clin_procs] ([proc_name]) VALUES ('LPA_SyncUsers_XML')
END

GO

/*synd_UpdateUser Version / permissions */
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_UpdateUser' AND TYPE = 'P')
BEGIN
    declare @name as varchar(50)
    declare @version as numeric(18,2)

    select @name = 'synd_UpdateUser', @version = 1.00 -- UPDATE VERSION NUMBER HERE
    exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GRANT EXECUTE ON synd_UpdateUser TO pht_server_read_only

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'synd_UpdateUser') BEGIN
INSERT [allowed_clin_procs] ([proc_name]) VALUES ('synd_UpdateUser')
END

GO
