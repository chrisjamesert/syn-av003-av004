IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_SyncVisitsAndForms' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[synd_SyncVisitsAndForms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[synd_SyncVisitsAndForms]

@KrDOM VARCHAR(36),
@DeviceID VARCHAR(36),
@Krpt VARCHAR(36) = NULL,
@lastRefreshTime DATETIME = NULL

AS
BEGIN
    /*************************************************************************************************************************************
    Stored procedure: synd_SyncVisitsAndForms
    Description: Get latest visits and forms for all subjects (@Krpt = NULL) or specific subject (valid @Krpt passed) on specific site.
    Project: Syndication
    Version Date    Change
    1.00            21Jun2016   Initial creation
    1.01            23Feb2017   [dyildiz]   Changes to reduce data size. Now stored proc returns a nested data format
    1.02            21Mar2017   [tblagaic]  added phase.EventAtEntry IS NOT NULL condition to the query
    *************************************************************************************************************************************/

	SET NOCOUNT ON;

	DECLARE @Xml XML;
	DECLARE @lastUpdated DATETIME;

    WITH Results AS (
        SELECT pt.krpt as krpt,
        reportTable.krsu as krsu,
        reportTable.sigorig as sigOrig,
        reportTable.signingts as completedTS,
        phase.EventAtEntry as eventId,
        reportTable.phaseatentry as phaseId,
        CAST(vs.EventStartDate AS DATETIME) as startDate,
        CAST(ve.EventEndDate AS DATETIME) as endDate,
        ve.VisitStatus as visitStatus,
        reportTable.deleted
        FROM lookup_su reportTable WITH (NOLOCK)
        INNER JOIN ig_Phase phase WITH (NOLOCK) ON reportTable.sigorig = phase.SIGORIG
        INNER JOIN lookup_pt pt WITH (NOLOCK) ON reportTable.krpt = pt.krpt
        LEFT JOIN ig_VisitStartDate vs WITH (NOLOCK) ON reportTable.sigorig = vs.SIGORIG
        LEFT JOIN ig_VisitEndDate ve WITH (NOLOCK) ON reportTable.sigorig = ve.SIGORIG
        WHERE (reportTable.krsu = 'VisitStart' OR reportTable.krsu = 'VisitEnd' OR reportTable.source <> 'SitePad' OR phase.EventAtEntry NOT IN ( --completed visits have VisitEnd form created -> we dont want to return forms for completed visits.
            SELECT ph.EventAtEntry
            FROM ig_Phase ph WITH (NOLOCK)
            INNER JOIN  lookup_su su WITH (NOLOCK) ON su.sigorig = ph.SIGORIG
            WHERE su.krsu = 'VisitEnd' AND su.deleted = 0 AND su.krpt = pt.krpt AND ph.EventAtEntry IS NOT NULL))
        AND (phase.EventAtEntry IS NOT NULL)
        AND (pt.deleted = 0 AND pt.krdom = @KrDOM)
        AND (@Krpt IS NULL OR pt.krpt = @Krpt)
        AND (@lastRefreshTime IS NULL OR reportTable.receiptts > @lastRefreshTime)
        AND (reportTable.deleted <> 0 OR reportTable.LogPadDeviceId <> @DeviceID OR reportTable.LogPadDeviceId IS NULL OR reportTable.source <> 'SitePad')
    )
    SELECT @Xml = (
        SELECT  r1.krpt '@krpt',
                (
                SELECT  r2.eventId '@eventId',
                        (
                        SELECT  r3.krsu '@krsu',
                                r3.sigOrig '@sigOrig',
                                r3.completedTS '@completedTS',
                                r3.phaseId '@phaseId',
                                r3.deleted '@deleted',
                                r3.startDate '@startDate',
                                r3.endDate '@endDate',
                                r3.visitStatus '@status'
                        FROM    Results r3
                        WHERE   r3.krpt = r1.krpt AND r3.eventId = r2.eventId
                        GROUP BY r3.krsu, r3.sigOrig, r3.completedTS, r3.phaseId, r3.deleted, r3.startDate, r3.endDate, r3.visitStatus
                        ORDER BY r3.completedTS
                        FOR XML PATH('report'), type
                        )
                FROM    Results r2
                WHERE   r2.krpt = r1.krpt
                GROUP BY r2.eventId
                ORDER BY r2.eventId
                FOR XML PATH('visit'), type
                )
        FROM    Results r1
        GROUP BY r1.krpt
        ORDER BY r1.krpt
        DESC
        FOR XML PATH('subject'), ROOT('reports')
    );

	SET @Xml = ISNULL(@Xml, '<reports/>')
	SET @lastUpdated = GETUTCDATE()
	SET @Xml.modify('insert attribute lastUpdated {sql:variable("@lastUpdated")} into (/reports)[1]')
	SELECT @Xml;

	DECLARE @String NVARCHAR(MAX);
	SET @String = CONVERT(NVARCHAR(MAX),@Xml);
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_SyncVisitsAndForms' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'synd_SyncVisitsAndForms', @version = 1.02
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GO


GRANT EXECUTE ON [dbo].[synd_SyncVisitsAndForms] TO pht_server_read_only
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='synd_SyncVisitsAndForms')
	INSERT INTO allowed_clin_procs (proc_name) values ('synd_SyncVisitsAndForms');
GO
