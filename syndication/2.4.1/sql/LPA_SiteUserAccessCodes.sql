
--Script Updated 20 Jul 2016 by TAM to include all objects from depreacted LPA_getRoleAccessCodes_XML.sql
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DD_StudyConfigVariables]') AND type in (N'U'))
Begin 
	Create Table DBO.DD_StudyConfigVariables(
		StdyCfgVarID Int Identity(1,1),
		CfgKey Int,
		CfgVarID VarChar(10),
		CfgDesc VarChar(100),
		Value  VarChar(100))

	Insert Into DBO.DD_StudyConfigVariables (CfgKey, CfgVarID, CfgDesc, Value)
	Values(1, 'CfgSTACLVL', 'Site Access Code Level', 2),
	(2, 'CfgSTACLEN', 'Site Access Code Length', 5)
END

IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_RoleConfiguration]') AND type in (N'U'))
Begin 
	Create Table DBO.LPA_RoleConfiguration(
		CfgRole VarChar(100),
		CfgLevel VarChar(100),
		CfgLength  VarChar(100))
-- CfgLevel is for defining password specifity. 'Site' is for creating site specific passwords.
	Insert Into DBO.LPA_RoleConfiguration (CfgRole, CfgLevel, CfgLength)
	Values('Admin', 'Site', 5)
	
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DD_HashToAccessCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[DD_HashToAccessCode]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_getRoleAccessCodes_XML]') AND type in (N'P'))
DROP Procedure [dbo].LPA_getRoleAccessCodes_XML
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DD_v_LPA_SiteUserAccessCodes]') AND type in (N'V'))
DROP VIEW [dbo].DD_v_LPA_SiteUserAccessCodes
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PDE_LPA_SiteUserAccessCodes]') AND type in (N'P'))
DROP Procedure [dbo].PDE_LPA_SiteUserAccessCodes
GO

CREATE Function [dbo].[DD_HashToAccessCode] (@RawString VarChar(MAX), @CodeLength Int) Returns VarChar(100) as
Begin
	Return (Select RIGHT(Replicate('0', @CodeLength) +
					left(
						cast(
							ABS(
								cast(
									cast(HASHBYTES('MD5', @RawString) as VarBinary(2000)) 
									as BigInt)
								) 
							as VarChar(64)), @CodeLength), 
						@CodeLength))
End
GO

Create View DBO.DD_v_LPA_SiteUserAccessCodes as 
Select	Distinct 
		Upper(DB_NAME()) As StudyName,
		LD.sitecode,
		LD.krdom ,
		LD.parentkrdom,
		LUC.kruser,
		LUC.krrole,
		LU.firstname,
		LU.middlename,
		LU.lastname,
		X.CfgSTACLVL,
		X.CfgSTACLEN,
		DBO.DD_HashToAccessCode(Upper(DB_NAME() +'|26NOV|'+ 
				Case When X.CfgSTACLVL & 2 = 2 Then LD.sitecode +'|' Else '' End +
				Case When X.CfgSTACLVL & 4 = 4 Then LUC.krrole +'|' Else '' End +
				Case When X.CfgSTACLVL & 8 = 8 Then LUC.kruser +'|' Else '' End), CfgSTACLEN) As AccessCode,
		Y.CfgRole,
		Y.CfgLevel,
		DBO.DD_HashToAccessCode(Upper(DB_NAME() +'|26NOV|'+ LD.krdom +'|'+ Y.CfgRole), Y.CfgLength) As LPAAccessCode
From	(
			Select	Max(Cast(Case When CfgKey = 1 Then Value Else NULL End As Int)) As CfgSTACLVL,
					Max(Cast(Case When CfgKey = 2 Then Value Else NULL End As int)) As CfgSTACLEN
			From	DD_StudyConfigVariables
		) X
		Join Lookup_Domain LD With (nolock) on 1=1
		Left Join lookup_usercontext LUC With (nolock) on LUC.krdom = ld.krdom and luc.deleted = 0
		Left Join PHT_ADMIN..v_ca_login_repl LU with (nolock) on lu.kruser = LUC.kruser 
		Left Join LPA_RoleConfiguration Y With (nolock) on Y.CfgLevel = 'Site'
Where LD.deleted = 0

Go
Create Proc DBO.PDE_LPA_SiteUserAccessCodes(@SiteCode VarChar(254)) as 

Select (
Select MAX(StudyName) as '@Stdy',
	MAX(CfgSTACLVL) as '@Lvl', 
	Max(CfgSTACLEN) as '@Len',
	(Select sitecode as '@SiteCode',  
			krdom	as	'@krDom', 
			kruser	as	'@krUser', 
			krrole	as	 '@krRole', 
			firstname as '@FirstN', 
			middlename as '@MidN', 
			lastname	as '@LastN', 
			master.dbo.fn_varbintohexstr(HASHBYTES('MD5', CONVERT(VarChar(64), StudyName) + AccessCode)) as '@Hash'	
		from DD_v_LPA_SiteUserAccessCodes with (nolock)
		Where sitecode = @SiteCode 
		for XML Path('UAC'), Type)
from DD_v_LPA_SiteUserAccessCodes with (nolock)
for XML Path('UACs'), type) as AccessCodes

GO

CREATE Proc [dbo].[LPA_getRoleAccessCodes_XML](
	@krdom VarChar(254),
	@useAcccessCodesFor1dot10 Bit = 0
) AS
/*****************************************************************************************************************
LPA_getRoleAccessCodes_XML - Role Access Codes
Version	Date		Change
1.01	01Dec2015	[dyildiz]	Changes to return data in XML format and to utilize 
								the merged version of DD_v_LPA_SiteUserAccessCodes
1.02    10Mar2017   [dyildiz]   Increased the version number although there are no code changes. This is done to
                                fix the incorrect version number and avoid possible downgrade error prompts in
                                the future
*****************************************************************************************************************/

SET NOCOUNT ON

SELECT 	CfgRole [role],
		master.dbo.fn_varbintohexstr(HASHBYTES('MD5', CONVERT(VarChar(64), StudyName) + 
			CASE @useAcccessCodesFor1dot10
				WHEN 0 THEN AccessCode
				WHEN 1 THEN LPAAccessCode
			End )) hash,
		StudyName [key]
FROM 	dbo.DD_v_LPA_SiteUserAccessCodes (NOLOCK)
WHERE 	krdom = @krdom
FOR XML RAW ('accessCode'), ROOT('Results');
GO

IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='LPA_getRoleAccessCodes_XML')
	insert into allowed_clin_procs (proc_name) values ('LPA_getRoleAccessCodes_XML');
GO

IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='PDE_LPA_SiteUserAccessCodes')
	insert into allowed_clin_procs (proc_name) values ('PDE_LPA_SiteUserAccessCodes');
GO

grant select on dbo.DD_v_LPA_SiteUserAccessCodes to pht_server_read_only
grant execute on dbo.DD_HashToAccessCode to pht_server_read_only
grant execute on dbo.PDE_LPA_SiteUserAccessCodes to pht_server_read_only 
grant execute on dbo.LPA_getRoleAccessCodes_XML to pht_server_read_only
IF DATABASE_PRINCIPAL_ID('DDE') IS NOT NULL
BEGIN
 grant execute on dbo.DD_HashToAccessCode to DDE
END
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DD_HashToAccessCode' AND TYPE = 'P')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'DD_HashToAccessCode', @version = 1.00
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END
go
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DD_v_LPA_SiteUserAccessCodes' AND TYPE = 'V')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'DD_v_LPA_SiteUserAccessCodes', @version = 1.02
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END
go
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_LPA_SiteUserAccessCodes' AND TYPE = 'P')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'PDE_LPA_SiteUserAccessCodes', @version = 1.02
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END

GO
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_getRoleAccessCodes_XML' AND TYPE = 'P')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'LPA_getRoleAccessCodes_XML', @version = 1.02 -- UPDATE VERSION NUMBER HERE
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GO

