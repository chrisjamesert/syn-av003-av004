IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getLanguageLocale_XML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getLanguageLocale_XML]
GO

SET ANSI_NULLS ON

GO
SET QUOTED_IDENTIFIER ON

GO

/*****************************************************************************************************************
getLanguageLocale_XML 
Version Date        Change
1.00    29DEC2015   [dyildiz] Initial release
1.01    10MAR2017   [dyildiz]   Increased the version number although there are no code changes. Since the 
                                Syndication 2.2 scripts have the same script with version number 1.01, this is
                                done to avoid possible downgrade error prompts in the future. 
*****************************************************************************************************************/
CREATE PROCEDURE [dbo].[getLanguageLocale_XML](@krPT VARCHAR(256))
AS

SET NOCOUNT ON

SELECT 	Language lang 
FROM 	dbo.ig_Assignment 
WHERE 	KRPT = @krPT
FOR XML RAW ('lang'), TYPE, ROOT('Results');

GO

GRANT EXECUTE ON getLanguageLocale_XML TO pht_server_read_only
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'getLanguageLocale_XML' AND TYPE = 'P')
BEGIN
    declare @name as varchar(50)
    declare @version as numeric(18,2)

    select @name = 'getLanguageLocale_XML', @version = 1.01  -- UPDATE VERSION NUMBER HERE
    exec [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END
GO

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'getLanguageLocale_XML') BEGIN
    INSERT [allowed_clin_procs] ([proc_name]) VALUES ('getLanguageLocale_XML')
END