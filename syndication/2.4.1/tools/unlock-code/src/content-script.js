
chrome.runtime.sendMessage({
    from    :    'content',
    subject : 'showPageAction'
});

chrome.runtime.onMessage.addListener((msg, sender, response) => {

    let studyName = localStorage.getItem('PHT_studyDbName');

    if ((msg.from === 'popup') && (msg.subject === 'StudyInfo')) {
        response({ studyName:  studyName });
    }

});
