import UnlockCode from 'core/classes/UnlockCode';

$(() => {

    let $siteCode = $('#siteCode'),
        $studyName = $('#studyName'),
        $unlockCode = $('#unlockCode'),
        $submit = $('#submit'),
        $clear = $('#clear');

    chrome.tabs.query({
        active        : true,
        currentWindow : true
    }, (tabs) => {

        chrome.tabs.sendMessage(tabs[0].id, {
            from    : 'popup',
            subject : 'StudyInfo'
        }, (res) => $studyName.val(res.studyName));

    });

    $submit.click(() => {

        let code = UnlockCode.createStartupUnlockCode($siteCode.val(), $studyName.val());

        $('#unlockCode').html(code);

    });

    $clear.click(() => {

        $siteCode.val('');
        $studyName.val('');
        $unlockCode.html('');

    });

});
