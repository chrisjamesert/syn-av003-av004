#!/bin/bash

find . \
  -name lib -prune \
  -o -name node_modules -prune \
  -o -name target -prune \
  -o -type f -name '*.js*' \
        -exec bash -c 'UNTAB="$0._"; expand -t 4 "$0" > "$UNTAB" && cmp -s "$UNTAB" "$0" || mv "$UNTAB" "$0" && echo "$0"; /bin/rm -f "$UNTAB" 2> /dev/null' {} \;
