How to Use:
- Browse to the decryption_tool folder and open the index.html file.
- Open the debugger for LogPad using chrome.
- In the console, paste the following code and hit enter.

(function (table) { 
	var col = new LF.Collection[table](); 

	col.fetch({ 
		onSuccess: function (result){ 
			result.push(table);
			result.push(_.keys(result[0].schema));
			console.log(JSON.stringify(result)); 
		} 
	}); 
})('Users')

- This will fetch the Users table and print the result in the console. If you would like to fetch any other table, edit the last line of the function and change 'Users' to any table name you would like to fetch. Make sure you write the table name within the single quotes.
- Copy the entire result from the console, make sure nothing is missed out, and paste it in the text area of the decryption tool and hit submit.
- Unencrypted table will be populated.

Note: 
- The database tool will only work in WebKit-based browsers.