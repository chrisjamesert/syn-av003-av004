﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("invivodata.Interop.WAMPC")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

[assembly: AssemblyCompany("ERT")]
[assembly: AssemblyCopyright("Copyright © ERT 2016")]
[assembly: AssemblyProduct("SITEpro Tablet")]
[assembly: AssemblyInformationalVersion("5.7.53.0")]

[assembly: Guid("a30428b9-7bb4-4eb1-a89b-bd0e771d762b")]

#if DEBUG
[assembly: AssemblyVersion("1.1.0.*")]
#else
[assembly: AssemblyVersion("1.1.0.0")]
#endif