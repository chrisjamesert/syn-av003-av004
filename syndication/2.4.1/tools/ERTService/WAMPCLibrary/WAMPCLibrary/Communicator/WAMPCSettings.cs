﻿using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace invivodata.Interop.WAMPC
{
    /// <summary>
    /// This class reads WAMPC settings from app.config.  They should appear under:
    ///     &lt;SystemSettings&gt;
    ///     &lt;/SystemSettings&gt;
    /// The format of these settings were predetermined by ELiX API uses them.
    /// </summary>
    public class WAMPCSettings
    {
        #region ctor
        /// <summary>
        /// ctor -- initilizes internal variables if required.
        /// </summary>
        public WAMPCSettings()
        {
        }
        #endregion

        #region Public Consts
        /// <summary>
        /// Consts used to refer to EXPERT field types.
        /// </summary>
		public const string FIELD_INITIALS ="INITIALS";
        public const string FIELD_DATE_OF_BIRTH = "DATE_OF_BIRTH";
        public const string FIELD_SUBJECT_ID1 = "SUBJECT_ID1";
        public const string FIELD_SUBJECT_ID2 = "SUBJECT_ID2";
        public const string FIELD_ETHNICITY = "ETHNICITY";
        public const string FIELD_GENDER = "GENDER";
        public const string FIELD_COLLECTION_DATE = "COLLECTION_DATE";
        public const string FIELD_COLLECTION_TIME = "COLLECTION_TIME";
        public const string FIELD_SITE_ID = "SITE_ID";
        public const string FIELD_BP = "BP";
        public const string FIELD_WEIGHT = "WEIGHT";
        public const string FIELD_TFIELD1 = "TFIELD1";
        public const string FIELD_TFIELD2 = "TFIELD2";
        public const string FIELD_TFIELD3 = "TFIELD3";
        public const string FIELD_TFIELD4 = "TFIELD4";
        public const string FIELD_TFIELD5 = "TFIELD5";
        public const string FIELD_TFIELD6 = "TFIELD6";
        public const string FIELD_TFIELD7 = "TFIELD7";
        public const string FIELD_TFIELD8 = "TFIELD8";
        public const string FIELD_DFIELD1 = "DFIELD1";
        public const string FIELD_DFIELD2 = "DFIELD2";
        public const string FIELD_DFIELD3 = "DFIELD3";
        public const string FIELD_DFIELD4 = "DFIELD4";

        /// <summary>
        /// Consts used to refer to EXPERT field data types.
        /// </summary>
        public const string DATA_INTEGER = "INTEGER";
        public const string DATA_STRING = "STRING";
        #endregion

        #region Public Properties
        /// <summary>
        /// Returns subject and visit demo field type mappings.
        /// </summary>
        public Dictionary<string, int> FieldTypeMap
        {
            get
            {
                Dictionary<string, int> map = new Dictionary<string, int>();
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/FieldTypeMapping") as NameValueCollection;
                if (settings != null)
                {
                    foreach (string key in settings.AllKeys)
                    {
                        int value = -1;
                        if (int.TryParse(settings[key], out value))
                            map.Add(key, value);
                    }
                }
                else
                {
                    // return default mappings.
                    map.Add(FIELD_INITIALS, 1);
                    map.Add(FIELD_DATE_OF_BIRTH, 2);
                    map.Add(FIELD_SUBJECT_ID1, 3);
                    map.Add(FIELD_SUBJECT_ID2, 4);
                    map.Add(FIELD_ETHNICITY, 5);
                    map.Add(FIELD_GENDER, 6);
                    map.Add(FIELD_COLLECTION_DATE, 7);
                    map.Add(FIELD_COLLECTION_TIME, 8);
                    map.Add(FIELD_SITE_ID, 9);
                    map.Add(FIELD_BP, 10);
                    map.Add(FIELD_WEIGHT, 11);
                    map.Add(FIELD_TFIELD1, 51);
                    map.Add(FIELD_TFIELD2, 52);
                    map.Add(FIELD_TFIELD3, 53);
                    map.Add(FIELD_TFIELD4, 54);
                    map.Add(FIELD_TFIELD5, 55);
                    map.Add(FIELD_TFIELD6, 56);
                    map.Add(FIELD_TFIELD7, 57);
                    map.Add(FIELD_TFIELD8, 58);
                    map.Add(FIELD_DFIELD1, 61);
                    map.Add(FIELD_DFIELD2, 62);
                    map.Add(FIELD_DFIELD3, 63);
                    map.Add(FIELD_DFIELD4, 64);
                }

                return map;
            }
        }

        /// <summary>
        /// Returns required demographic fields.
        /// </summary>
        public Dictionary<int, string> MostImportantDemoFields
        {
            get
            {
                Dictionary<int, string> map = new Dictionary<int, string>();
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/MostImportantDemoFields") as NameValueCollection;
                if (settings != null)
                {
                    foreach (string key in settings.AllKeys)
                    {
                        int keyValue = -1;
                        if (int.TryParse(key, out keyValue))
                            map.Add(keyValue, settings[key].ToUpper());
                    }
                }
                else
                {
                    map.Add(1, FIELD_INITIALS);
                    map.Add(2, FIELD_SUBJECT_ID1);
                    map.Add(3, FIELD_SUBJECT_ID2);
                    map.Add(4, FIELD_DATE_OF_BIRTH);
                    map.Add(5, FIELD_GENDER);
                    map.Add(6, FIELD_ETHNICITY);
                }

                return map;
            }
        }

        /// <summary>
        /// Returns DateFormat to use.
        /// </summary>
        public string DateFormat
        {
            get
            {
                string value = "yyyy-MM-dd";
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/Formatting") as NameValueCollection;
                if (settings != null && settings["StoredDATEFormat"] != null)
                {
                    value = settings["StoredDATEFormat"];
                }

                return value;
            }
        }

        /// <summary>
        /// Returns TimeFormat to use.
        /// </summary>
        public string TimeFormat
        {
            get
            {
                string value = "HH:mm:ss";
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/Formatting") as NameValueCollection;
                if (settings != null && settings["StoredTIMEFormat"] != null)
                {
                    value = settings["StoredTIMEFormat"];
                }

                return value;
            }
        }

        /// <summary>
        /// Acquisition duration in seconds: Min 10, max 120, step 10
        /// </summary>
        public int AcquisitionDuration
        {
            get
            {
                int value = 60;
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/Wamm") as NameValueCollection;
                if (settings != null && settings["AcquisitionDuration_s"] != null)
                {
                    int.TryParse(settings["AcquisitionDuration_s"], out value);
                }

                return value;
            }
        }

        /// <summary>
        /// Minimum valid duration in seconds: Min 10, max 45, step 5, must be &lt= AcquisitionDuration_s
        /// </summary>
        public int MinValidDuration
        {
            get
            {
                int value = 15;
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/Wamm") as NameValueCollection;
                if (settings != null && settings["MinValidDuration_s"] != null)
                {
                    int.TryParse(settings["MinValidDuration_s"], out value);
                }

                return value;
            }
        }

        /// <summary>
        /// Duration to keep the test button active before shutting off: Min 3, max 15, step 1
        /// </summary>
        public int TestButtonLength
        {
            get
            {
                int value = 3;
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/Wamm") as NameValueCollection;
                if (settings != null && settings["TestButtonLength_s"] != null)
                {
                    int.TryParse(settings["TestButtonLength_s"], out value);
                }

                return value;
            }
        }

        /// <summary>
        /// Duration to keep the power on before shutting it off in minutes: Min 3, max 15, step 1
        /// </summary>
        public int AutoPwrOffLength
        {
            get
            {
                int value = 5;
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/Wamm") as NameValueCollection;
                if (settings != null && settings["AutoPwrOffLength_m"] != null)
                {
                    int.TryParse(settings["AutoPwrOffLength_m"], out value);
                }

                return value;
            }
        }

        /// <summary>
        /// Returns the default layout of PDFViewer.
        /// </summary>
        public string Layout
        {
            get
            {
                string value = "3x4_1R,Summary,12x1";
                NameValueCollection settings = ConfigurationManager.GetSection("SystemSettings/PDFViewer") as NameValueCollection;
                if (settings != null && settings["Layout"] != null)
                {
                    value = settings["Layout"];
                }

                return value;
            }
        }
        #endregion

    }
}
