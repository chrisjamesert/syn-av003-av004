﻿namespace invivodata.Interop.WAMPC.Communicator
{
    public enum WamBatteryLevel
    {
        NA = -1,
        OK = 0,
        Low = 1,
        Critical = 2,
    }
}
