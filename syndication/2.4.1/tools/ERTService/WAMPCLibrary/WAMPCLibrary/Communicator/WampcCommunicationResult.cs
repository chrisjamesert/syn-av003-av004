﻿using System;
using System.Xml.Serialization;

namespace invivodata.Interop.WAMPC
{
    public class WampcCommunicationResult<TSomeType>
    {
        /// <summary>
        /// Exception thrown during communication with WAM PC
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Communication result returned due to communication between host and the WAM PC
        /// </summary>
        public TSomeType Result { get; set; }

        /// <summary>
        /// Creates an instance of a new WampcCommunicationResult with a successful communication
        /// </summary>
        /// <param name="result"></param>
        public WampcCommunicationResult(TSomeType result)
        {
            Result = result;
        }

        public WampcCommunicationResult(Exception ex)
        {
            Result = default(TSomeType);
            Exception = ex;
        }
    }

    public class ECGDownloadData
    {
        public string Serialized_ELIX_Record;
        public byte[] ECGData;
    }
}
