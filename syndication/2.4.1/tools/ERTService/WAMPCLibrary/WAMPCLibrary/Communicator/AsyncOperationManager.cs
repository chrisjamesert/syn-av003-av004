﻿using System;
using System.Diagnostics;
using System.Threading;
using Mortara.ELIxCtrl.API;

namespace invivodata.Interop.WAMPC
{
	/// <summary>
	/// Class which helps manage asynchronous operations.
	/// </summary>
	/// <remarks>
	/// This class is intended to allow timeouts for Wampc operations and the user
	/// must run the operation while using an EventWaitHandle to manage threads.
	/// </remarks>
	internal class AsyncOperationManager
	{
		private Stopwatch _operationTimer = new Stopwatch();

		/// <summary>
		/// Initialize a new instance of the <see cref="AsyncOperationManager"/> class.
		/// </summary>
		/// <param name="timeout">Timeout for the operation in milliseconds.</param>
		public AsyncOperationManager(int timeout)
		{
			Timeout = timeout;
		}

		/// <summary>
		/// Gets the timeout for the operation in milliseconds.
		/// </summary>
		public long Timeout
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets a value which indicates if the operation is finished.
		/// This is only accurate as of the last <see cref="EvaluateStop"/> call.
		/// </summary>
		public bool IsFinished
		{
			get;
			private set;
		}

		/// <summary>
		/// Starts managing the operation.
		/// </summary>
		public void Start()
		{
			if (!IsFinished && !_operationTimer.IsRunning)
			{
				_operationTimer.Start();
			}
		}

		/// <summary>
		/// Stops the specified operation and resumes the caller thread.
		/// </summary>
		/// <param name="asyncOperation">The asynchronous operation to stop.</param>
		public void Stop(IAsyncOperationProgress asyncOperation)
		{
			if (null == asyncOperation)
			{
				throw new ArgumentNullException("asyncOperation");
			}

			if (IsFinished)
			{
				return;
			}

			EventWaitHandle operationProgress = (EventWaitHandle)asyncOperation.CallerStateObject;

			SafelyResume(operationProgress);

			asyncOperation.AbortOperation();

			IsFinished = true;
		}

		/// <summary>
		/// Stops the specified operation if it has exceeded the timeout.
		/// Resumes the caller thread if appropriate.
		/// </summary>
		/// <param name="asyncOperation">The asynchronous operation to evaluate.</param>
		public void EvaluateStop(IAsyncOperationProgress asyncOperation)
		{
			if (null == asyncOperation)
			{
				throw new ArgumentNullException("asyncOperation");
			}

			if (IsFinished)
			{
				return;
			}

			EventWaitHandle operationProgress = (EventWaitHandle)asyncOperation.CallerStateObject;

			if (operationProgress.SafeWaitHandle.IsClosed)
			{
				IsFinished = true;
			}
			else if (asyncOperation.AsyncOperationResult != OperationResult.InProgress)
			{
				// Result is always InProgress until the operation finishes
				// So, the operation has finished and we need to resume the calling thread
				SafelyResume(operationProgress);
				IsFinished = true;
			}
			else if (_operationTimer.ElapsedMilliseconds >= Timeout)
			{
				// Operation has timed out so abort the operation and resume the calling thread
				_operationTimer.Stop();

				SafelyResume(operationProgress);

				asyncOperation.AbortOperation();

				IsFinished = true;
			}
		}

		private void SafelyResume(EventWaitHandle handle)
		{
			Debug.Assert(null != handle);
			// Avoid exceptions by ensuring the wait handle is not already closed
			if (!handle.SafeWaitHandle.IsClosed)
			{
				handle.Set();
			}
		}
	}
}
