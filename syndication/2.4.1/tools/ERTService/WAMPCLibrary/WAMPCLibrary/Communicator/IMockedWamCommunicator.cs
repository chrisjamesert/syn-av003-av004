﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace invivodata.Interop.WAMPC.Communicator
{
    public interface IMockedWamCommunicator
    {
        #region Hardware mock'ing methods

        MockedWamPowerState PowerState { get; }
        WamBatteryLevel CurrentMockBatteryLevel { get; }
        WamDeviceState CurrentMockDeviceState { get; }

        void MockTogglePower();
        void MockSetState(WamDeviceState newState);
        void MockSetBatteryLevel(WamBatteryLevel newBatteryLevel);
        void MockTakeECGPressed();

        #endregion

    }

    public enum MockedWamPowerState
    {
        WamOn,
        WamOff
    }
}
