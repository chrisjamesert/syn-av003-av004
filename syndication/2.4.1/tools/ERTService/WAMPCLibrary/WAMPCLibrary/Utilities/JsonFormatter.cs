﻿using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;

namespace invivodata.Interop.WAMPC.Utilities
{
	/// <summary>
	/// This class is used to serialize/de-serialize object into/out of JSON data format.
	/// </summary>
	internal class JsonFormatter
	{
		/// <summary>
		/// This function serializes given object in JSON format.
		/// </summary>
		/// <typeparam name="T">Type of object to serialize</typeparam>
		/// <param name="t">object to serialize</param>
		/// <returns>object serialized in JSON format</returns>
		public static string JsonSerializer<T>(T t)
		{
			string jsonString = string.Empty;
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			using (MemoryStream ms = new MemoryStream())
			{
				ser.WriteObject(ms, t);
				jsonString = Encoding.UTF8.GetString(ms.ToArray());
			}

			return jsonString;
		}

		/// <summary>
		/// This funtion de-serializes JSON string into requested type.
		/// </summary>
		/// <typeparam name="T">requested type</typeparam>
		/// <param name="jsonString">serized JSON string</param>
		/// <returns>de-serized object in requested type</returns>
		public static T JsonDeserialize<T>(string jsonString)
		{
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			T obj = default(T);
			using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
			{
				obj = (T)ser.ReadObject(ms);
			}
			return obj;
		}
	}
}
