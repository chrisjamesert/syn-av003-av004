﻿using System;

public class SettingsModule : Nancy.NancyModule
{
	public SettingsModule()
	{
        Get["/"] = _ => "Received GET request";

        Post["/"] = _ => "Received POST request";

        Get["/"] = _ => "Let me have the request!";

        Post["/setTimeZone"] = _ =>
        {
            var id = this.Request.Body;
            var length = this.Request.Body.Length;
            var data = new byte[length];
            id.Read(data, 0, (int)length);
            var body = System.Text.Encoding.Default.GetString(data);

            var request = JsonConvert.DeserializeObject<SimpleRequest>(body);
            Console.WriteLine(request);
            return 200;
        };
    }
}
