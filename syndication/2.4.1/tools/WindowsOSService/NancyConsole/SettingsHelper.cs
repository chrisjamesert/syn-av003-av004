﻿using NancyConsole.log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole
{
    public class SettingsHelper
    {

        public static bool setLoopbackPermissions() {
            bool result = true;

            List<string> appIds = RegistryHelper.getSitePadAppIdsEx();
        
            foreach (string appId in appIds) {
                int exitCode;
                string output;
                
                ConsoleHelper.CaptureConsoleAppOutput("CheckNetIsolation.exe", "LoopbackExempt -a -p=" + "\"" + appId + "\"", 5000, out exitCode, out output);                
                result = !result ? result : (exitCode != -1); //will fail if at least one fails

                LogHelper.Log("CheckNetIsolation for app id: " + appId + ", result: " + result);
            }

            return result;
        }

    }
}
