﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole
{

    public class ERTTimeZoneInfo {
        public String displayName;
        public String id;
        public double offset;
    }

    public class TimeUtil
    {
        public static void SetSystemTimeZone(string timeZoneId)
        {
            var process = System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
            {
                FileName = "tzutil.exe",
                Arguments = "/s \"" + timeZoneId + "\"",
                UseShellExecute = false,
                CreateNoWindow = true
            });

            if (process != null)
            {
                process.WaitForExit();
                System.TimeZoneInfo.ClearCachedData();
            }
        }


        public static string GetTimeZoneList(long time)
        {
            string result = null;
            int code = 0;
            StringBuilder jsonResult = new StringBuilder();

            long unixDate = time;
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dateTimeUtc = start.AddMilliseconds(unixDate);

            ConsoleHelper.CaptureConsoleAppOutput("tzutil.exe", "/l", 5000, out code, out result);
            if (code != -1) {
                List<ERTTimeZoneInfo> tzList = new List<ERTTimeZoneInfo>();
                System.IO.StringReader reader = new System.IO.StringReader(result.Trim());
                string line = null;

                string name = null;
                string id = null;

                bool loop = true;
                while (loop)
                {
                    line = reader.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    else if (line == Environment.NewLine || line.Trim().Length == 0)
                    {
                        continue;
                    }

                    name = line;
                    id = reader.ReadLine();
                    ERTTimeZoneInfo zone = new ERTTimeZoneInfo();
                    zone.displayName = name;
                    zone.id = id;

                    TimeZoneInfo info = TimeZoneInfo.FindSystemTimeZoneById(id);
                    TimeSpan offset = info.GetUtcOffset(dateTimeUtc);
                    zone.offset = offset.TotalMilliseconds;

                    tzList.Add(zone);
                }


                Nancy.Json.JavaScriptSerializer serializer = new Nancy.Json.JavaScriptSerializer();
                result =  serializer.Serialize(tzList);
            }

            return result;
        }

        public static string getCurrentTimeZone() {
            string result = null;
            string strCurrentTzID = null;

            //TimeZone currentTZ = TimeZone.CurrentTimeZone;
            //result = currentTZ.StandardName; does not work well when combined with tzutil
            int code = 0;
            StringBuilder jsonResult = new StringBuilder();

            ConsoleHelper.CaptureConsoleAppOutput("tzutil.exe", "/g", 5000, out code, out result);
            if (code != -1)
            {
                List<ERTTimeZoneInfo> tzList = new List<ERTTimeZoneInfo>();
                System.IO.StringReader reader = new System.IO.StringReader(result.Trim());
                strCurrentTzID = reader.ReadLine();
            }

            ConsoleHelper.CaptureConsoleAppOutput("tzutil.exe", "/l", 5000, out code, out result);
            if ( (code != -1) && (strCurrentTzID != null))
            {
                ERTTimeZoneInfo zone = new ERTTimeZoneInfo();
                System.IO.StringReader reader = new System.IO.StringReader(result.Trim());
                string line = null;

                string name = null;
                string id = null;

                bool loop = true;
                while (loop)
                {
                    line = reader.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    else if (line == Environment.NewLine || line.Trim().Length == 0)
                    {
                        continue;
                    }

                    name = line;
                    id = reader.ReadLine();

                    if (id.Equals(strCurrentTzID))
                    {
                        zone.displayName = name;
                        zone.id = id;
                        break;
                    }
                }

                Nancy.Json.JavaScriptSerializer serializer = new Nancy.Json.JavaScriptSerializer();
                result = zone != null ? serializer.Serialize(zone) : null;
            }

            return result;
        }

    }
}
