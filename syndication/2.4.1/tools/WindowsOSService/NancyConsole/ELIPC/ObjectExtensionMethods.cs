﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole.ELIPC
{
    public static class ObjectExtensionMethods
    {
        public const int ToIntFailedConversionConstant = -9999;

        /// <summary>
        /// Tries to convert an object to an int
        /// </summary>
        /// <param name="value">object to convert</param>
        /// <returns>converted int or -9999 if fails</returns>
        public static int ToInt(this object value)
        {
            try
            {
                //ELIPCModule.writeLog("Attempting to convert: " + value.ToString() + "    of type: " + value.GetType().ToString());

                // convert the object parameter into an int
                var convertible = value as IConvertible;
                if (convertible != null)
                {
                    return convertible.ToInt32(null);
                }

            }
            catch (Exception)
            {
                ELIPCModule.writeLog("An exception was thrown in the ToInt extension method");
                return ToIntFailedConversionConstant;
            }

            return ToIntFailedConversionConstant;
        }
    }
}
