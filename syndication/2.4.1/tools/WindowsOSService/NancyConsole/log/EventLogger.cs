﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole.log
{
    public class EventLogger : BaseLogger
    {
        public override void Log(string message)
        {
            lock (lockObj)

            {
                System.Diagnostics.EventLog eventlog;
                eventlog = new System.Diagnostics.EventLog();

                if (!System.Diagnostics.EventLog.SourceExists(Constants.TagSource))
                {
                    System.Diagnostics.EventLog.CreateEventSource(
                        Constants.TagSource, Constants.TagLogs);
                }

                eventlog.Source = Constants.TagSource;
                eventlog.Log = Constants.TagLogs;

                eventlog.WriteEntry(message);
            }
        }
    }

}