let express = require('express'),
    path = require('path'),
    commandLineArgs = require('command-line-args'),
    http = require('http'),
    ip = require('ip'),
    compression = require('compression'),
    Q = require('q');

let Logger = require('@ert/logger'),
    hutils = require('@ert/hutils'),
    api = require('./routes/api'),
    Web = require('./business/web');

let logger = new Logger('syndicationhost:app');

let app = express();

const serviceConfigParams = {
    user: { prompt: 'Enter the Mongo Username', required: false },
    pwd: { prompt: 'Enter the Mongo Password', required: false },
    dbHosts: { prompt: 'Enter the Mongo hosts list (host:port)', required: true },
    replicaSet: { prompt: 'Enter the Mongo replica set name', required: false }
};

let showSyntaxAndExit = (message) => {
    if (message) {
        logger.error(message);
    }

    logger.info(`Usage: node ${process.argv[1]} [options]`);
    logger.info('-g, --configure    Prompt for configuration parameters prior to startup');
    logger.info('-p, --port         the port number the service should listen on');
    logger.info('-h, --help         logs this syntax message');
};

hutils.showSyntaxAndExit = showSyntaxAndExit;

let optionsDefinitions = [{
    name: 'serviceName',
    alias: 'n',
    type: String,
    defaultValue: 'syndicationhost'
}, {
    name: 'port',
    alias: 'p',
    defaultValue: 3010
}, {
    name: 'configure',
    alias: 'g'
}, {
    name: 'help',
    alias: 'h'
}];

let options;

try {
    // commandLineArgs - See https://www.npmjs.com/package/command-line-args
    // Parses command-line options.
    options = commandLineArgs(optionsDefinitions);

    // If the help argument is set, display the help message.
    if (options.help !== undefined) {
        showSyntaxAndExit();
    }
} catch (e) {
    showSyntaxAndExit(`Invalid argument. ${e.message}`);
}

// This will generate a file path to store local configuration options.
const dataPath = hutils.ensureDataPath(options.serviceName);

// Don't care about consistent return here.  Process will exit on failed cases.
// eslint-disable-next-line consistent-return
Q().then(() => {
    let { serviceName } = options;

    if (options.configure !== undefined) {
        // Initialize the configuration prompts
        return hutils.promptForConfig(
            serviceName,
            dataPath,
            serviceConfigParams
        ).then((service) => {
            // No DBHost provide.
            if (!service.serviceConfiguration.dbHosts) {
                showSyntaxAndExit('Invalid configuration. Missing dbHosts. Please reconfigure.');
            }

            // Username set w/o password.
            if (service.serviceConfiguration.user && !service.serviceConfiguration.pwd) {
                showSyntaxAndExit('Invalid configuration. Mongo password not specified.');
            }

            // Replica set configured with no user.
            if (service.serviceConfiguration.replicaSet && !service.serviceConfiguration.user) {
                showSyntaxAndExit('Invalid configuration. UN/PWD must be provided for replica sets.');
            }

            // Writes the configuration to file.
            // Once we adopt horizon, we need to register the service to compass.
            hutils.writeCompassKey(JSON.stringify(service), '', dataPath);

            return service;
        });
    }

    try {
        // Read the local config file.
        // Horizon: We should first prompt compass for the configuration,
        // and fallback to the local file.
        return JSON.parse(hutils.readCompassKey('', dataPath));
    } catch (e) {
        // Attempted to run the service w/o first configuring.
        showSyntaxAndExit('Service is not configured.');
    }
}).then((service) => {
    // This needs to be handled prior to the bodyParser middleware.
    // Otherwise, we'd have this under routes/api
    app.use(Web.proxyMiddleware(/\/proxy\/.*/, path.join(__dirname, 'public')));
    app.use(Web.registerMiddleware(/\/register/, path.join(__dirname, 'public')));

    // Enable gzip compression.
    app.use(compression());

    hutils.serviceStart(app);

    service.publicPath = path.join(__dirname, 'public');
    service.protocol = process.env.registeredProtocol === 'https' ? 'https' : 'http';

    // Warn the user if HTTP protocol is being used in production.
    if (app.get('env') === 'production' && service.protocol !== 'https') {
        logger.warn('Service in production should use HTTPS!');
    }

    return api.start(service)
        .then(() => {
            http.createServer(app).listen(options.port);

            // Register api/v1.
            app.use('/api', api);

            // Serve static files (HTML, JavaScript, etc...) from the public directory.
            app.use(express.static(service.publicPath));

            hutils.serviceFinalRoutes(app);

            logger.operational(`${options.serviceName} service started:
            - protocol:       ${service.protocol}
            - ip:             ${ip.address()}
            - port:           ${options.port}
            - mode:           ${app.get('env')}
            - public:         ${service.publicPath}
        `);
        });
}).catch((err) => {
    logger.error('Error at startup...', err);

    process.exit();
});
