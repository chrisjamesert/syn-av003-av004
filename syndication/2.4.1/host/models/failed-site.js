const mongoose = require('mongoose'),
    failedSiteUnlockSchema = require('../schemas/failed-sites-schema');

// mongoose.set('debug', true);

module.exports = mongoose.model('FailedSiteUnlock', failedSiteUnlockSchema, 'FailedSites');
