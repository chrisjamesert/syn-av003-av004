const md5Hex = require('md5-hex');
const unlockCodeLength = 8;

/**
 * Returns MD5 hash of the provided string.
 * @param {string} input - Input to hash.
 * @returns {string} The hashed value.
 */
const hashString = (input) => {
    return md5Hex(input).substring(0, unlockCodeLength);
};

/**
 * Pads the provided string.
 * @param {string} source - The string to pad.
 * @param {number} length - The length the string should be.
 * @param {string} padChar - The character to pad with.
 * @param {boolean} padRight - Determines if the code should be padded on the right.
 * @returns {string} The padded string.
 */
const padString = function (source, length, padChar, padRight) {
    let split = source.split(''),
        diff = length - split.length;

    if (diff > 0) {
        for (let i = 0; i < diff; i += 1) {
            split[padRight ? 'push' : 'unshift'](padChar);
        }
    }

    return split.join('');
};

/**
 * Converts Hexidecimal input to decimal.
 * @param {string} input - The input to converted.
 * @returns {string} The converted value.
 */
const convertHextoDec = function (input) {
    let unlockCode = (parseInt(input, 16)).toString();

    if (unlockCode.length < 10) {
        unlockCode = padString(unlockCode, 10, '0', false);
    }

    return unlockCode;
};

/**
 * Generates the unlock code (startup code) for the SitePad and Web modality.
 * @class UnlockCode
 */
class UnlockCode {
    /**
     * Generate the unlock code.
     * @param {string} siteCode - The code of the site.
     * @param {string} databaseName - The name of the study's database.
     * @returns {string} The unlock code.
     */
    static createStartupUnlockCode (siteCode, databaseName) {
        let output;

        if (siteCode == null) {
            throw new Error('Missing arugment (siteCode) to createStartupUnlockCode.');
        }

        if (databaseName == null) {
            throw new Error('Missing argument (databaseName) to createStartupUnlockCode.');
        }

        output = hashString(databaseName + siteCode);
        output = convertHextoDec(output);

        return output;
    }
}

module.exports = UnlockCode;
