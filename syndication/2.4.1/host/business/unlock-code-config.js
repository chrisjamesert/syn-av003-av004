/**
 * Stores the unlock code lockout config from study-design
 * @class UnlockCodeConfig
 */
class UnlockCodeConfig {
    static set maxAttempts (val) {
        this.maxUnlockAttempts = val;
    }

    static get maxAttempts () {
        return this.maxUnlockAttempts;
    }

    static set lockoutDuration (val) {
        this.unlockCodeLockoutTime = val;
    }

    static get lockoutDuration () {
        return this.unlockCodeLockoutTime;
    }
}

module.exports = UnlockCodeConfig;
