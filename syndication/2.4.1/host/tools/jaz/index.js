#!/usr/bin/env node

// NOTE: This tool was pulled in from the horizon project and doesn't match
// our current style guidelines. Updating the code to match would be a waste of time,
// as it may be available from the @ert registry at some point.

/*
*   --suppress=pass,fail,skip,console  (console represents output captured from browser console)
*   --reporters=a,b,c  use reporters a, b, and c (e.g. dots,coverage)
*   --quiet Bare minimum output, basically just test failures
*              ~= --reporters=spec --suppress=pass,skip,console
*              also sets logLevel:'WARN'
*   --no-color turn off ANSI escapes
*              used for Jenkins; note: attempted auto-detect did't work. ???
*/

let path = require('path'),
    Command = require('jasmine/lib/command.js'),
    Jasmine = require('jasmine/lib/jasmine.js'),
    SpecReporter = require('jasmine-spec-reporter'),
    optimist = require('optimist'),
    _ = require('lodash');

let option  = (name) => optimist.argv[name];
let suppress = option('suppress');
let consoleLogLevel = 'trace';

// The point of the following crazy stuff is:
// convert environment variable '--name=value' to
// equivalent command line argument.
// Why? because WebStorm does not support passing in args
// but does support passing in env vars.
function env2opts () {
    let envArgs = _(process.env).keys() // Get all the env var names
        .filter(function (name) {return  /^--/.test(name); }) // extract out env names that start with --
        .map(function (name) {return name + '=' + process.env[name]; }); // build array of --name=value, argv style
    let envOpts = optimist(envArgs).argv; // use optimist to parse out that fake argv
    _.extend(envOpts); // and combine result with previously parsed CLI opts
}
env2opts();

// handle quiet first; specifc other options then take precedence
if (option('quiet')) {
    //reporters = _.difference(reporters, ['coverage']);
    suppress = 'pass,skip,console';
    consoleLogLevel = 'error';
}

if (/console/.test(suppress)) {
    consoleLogLevel = 'error';
}

process.env['HZ_CONSOLE_LOG_LEVEL'] = consoleLogLevel;

let jasmine = new Jasmine({projectBaseDir: path.resolve()});
function noop () {}

jasmine.configureDefaultReporter({print: noop});    // remove default reporter logs

let specReporter = new SpecReporter({
    displayStacktrace: 'none',    // display stacktrace for each failed assertion, values: (all|specs|summary|none)
    displayFailuresSummary: true, // display summary of all failures after execution
    displayPendingSummary: true,  // display summary of all pending specs after execution
    displaySuccessfulSpec: !/pass/.test(suppress),  // display each successful spec
    displayFailedSpec: !/fail/.test(suppress),      // display each failed spec
    displayPendingSpec: !/skip/.test(suppress) || option('grep'),    // display each pending spec
    displaySpecDuration: true,   // display each spec duration
    displaySuiteNumber: false,    // display each suite number (hierarchical)
    colors: {
        success: 'green',
        failure: 'red',
        pending: 'yellow'
    },
    prefixes: {
        success: '✓ ',
        failure: '✗ ',
        pending: '* '
    },
    customProcessors: []
});
jasmine.jasmine.getEnv().addReporter(specReporter);   // add jasmine-spec-reporter

let command = new Command(path.resolve(), null, console.log);

command.run(jasmine, process.argv.slice(2));
