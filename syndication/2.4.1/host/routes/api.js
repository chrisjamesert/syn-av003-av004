const express = require('express'),
    Web = require('../business/web'),
    Logger = require('@ert/logger');

// This is a express convention, not much we can do here but disable the rule for the line.
// eslint-disable-next-line new-cap
let router = express.Router(),
    logger = new Logger('api');

router.start = Web.start;

// Any service APIs should be defined here, but business logic in business/*.js
// The Proxy API is an exception due to requirements of when and where it is defined/executed
// in the middleware stack.

router.post('/v1/unlock-code', (req, res) => {
    logger.trace(`GET api/v1/unlock-code - ${JSON.stringify(req.body)}`);

    Web.verifyUnlockCode(req, res);
});

router.get('/v1/lockout/user', (req, res) => {
    logger.trace(`GET api/v1/lockout/user- ${JSON.stringify(req.query)}`);

    Web.checkLockout(req, res);
});

router.post('/v1/lockout/user', (req, res) => {
    logger.trace(`POST api/v1/lockout/user- ${JSON.stringify(req.body)}`);

    Web.setLockout(req, res);
});

router.post('/v1/locked-out-sites', (req, res) => {
    logger.trace(`POST api/v1/locked-out-sites - ${JSON.stringify(req.body)}`);

    Web.lockedSites(req, res);
});

module.exports = router;
