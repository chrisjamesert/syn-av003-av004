const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    userSchema = new Schema({
        username: String,
        siteCode: String,
        role: String,
        timeStamp: Number,
        lockoutTime: Number
    });


module.exports = userSchema;
