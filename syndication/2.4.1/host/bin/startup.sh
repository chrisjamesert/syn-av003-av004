#!/bin/bash -e
docker rm --force mongodb || true
docker pull mongo:3.4
# Build and start a MongoDB instance.
# You may enounter an issue where mongodb won't run.  This is likely caused by not enough memory available on the VM.
# Easiest solution is to clear any docker volumns.  Execute the following:
# docker volume rm $(docker volume ls -qf dangling=true)
docker run --name mongodb -d mongo:3.4

docker rm --force web || true
# Rebuild and deploy the development host container.
docker build -t web .
docker run --name host --link mongodb:mongo -p 3010:3010 -v $PWD:/usr/src/app -d -t web