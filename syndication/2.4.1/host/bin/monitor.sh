#!/bin/bash -e

# Monitor the running host container.  Ensure you've executed startup.sh prior to this.
docker exec -i -t host pm2 monit