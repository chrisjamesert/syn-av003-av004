#!/bin/bash -e
docker rm --force mongodb || true
docker pull mongo:3.4
# Build and start a MongoDB instance.
# You may enounter an issue where mongodb won't run.  This is likely caused by not enough memory available on the VM.
# Easiest solution is to clear any docker volumns.  Execute the following:
# docker volume rm $(docker volume ls -qf dangling=true)
docker run --name mongodb -d mongo:3.4

docker rm --force host-prod || true
# Rebuild and deploy the production host container.
docker build --file ./Dockerfile-prod -t host-prod .
docker run --name host-prod --link mongodb:mongo -p 3010:3010 -v $PWD/public:/usr/src/app/public -d -t host-prod 