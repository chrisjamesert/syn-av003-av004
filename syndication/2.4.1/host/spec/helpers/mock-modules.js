/* eslint-disable no-empty-function */
let mockery = require('mockery');

// @ert/logger reaches out to a RabbitMQ server, which generates a number
// of connections errors for tests.  Since we only care about the logger being
// called, we're replacing require('@ert/logger') with a mock module.
beforeAll(() => {
    mockery.enable({
        // This will hide a lot of useless logs.
        warnOnReplace: false,
        warnOnUnregistered: false
    });

    class Logger {
        info () {}
        operational () {}
        fatal () {}
        error () {}
        warn () {}
        announce () {}
        debug () {}
        trace () {}
    }

    mockery.registerMock('@ert/logger', Logger);
});

afterAll(() => {
    mockery.deregisterMock('@ert/logger');

    mockery.disable();
});
