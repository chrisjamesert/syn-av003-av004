require('@ert/async-spec-helpers');
require('jasmine2-custom-message');
require('@ert/trace-matrix');

const hMatchers = require('@ert/h-matchers');

// don't bother trying this in beforeAll, no worky
beforeEach(() => {
    jasmine.addMatchers(hMatchers);

    // This will stop the running node instance unless we stub it out.
    spyOn(process, 'exit').and.stub();
});
