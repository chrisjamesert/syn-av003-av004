import COOL from 'core/COOL';
import ToolboxView from 'logpad/views/ToolboxView';
import { MessageRepo } from 'core/Notify';

// This function is imported for a study rule, so it doesn't require an index import.

/**
 * @function extendToolboxViewPDE
 * Extends ToolboxView to add Toggle Automation button, to show/hide automation controls.
 */
export default function extendToolboxViewPDE () {
    /**
     * @class PDEToolboxView
     * A class that extends the logpad ToolboxView
     */
    class PDEToolboxView extends COOL.getClass('ToolboxView', ToolboxView) {

        constructor (options) {
            super(options);
            this.events['click #toggleAutomationControls'] = 'toggleAutomationControls';
        }

        /**
         * @method render
         * Calls super render then appends a 'Toggle Automation' button.
         * @returns {Q.Promise<any> | Promise.<TResult>}
         */
        render () {

            return super.render()
            .then(() => {
                if (PDE.Automation.allowPDEAutomation) {
                    return LF.getStrings('PDE_AUTOMATION_TOGGLE_AUTOMATION')
                    .then((string) => {
                        let $toggleAutomationControls = this.$('#toggleAutomationControls');
                        if (!$toggleAutomationControls.length) {
                            // Add the item
                            let itemHTML = LF.templates.display('PDE:toolboxMenuToggleAutomationButton',
                                { toggleAutomation: string });
                            this.$('#toolbox-list').append(itemHTML);
                            this.delegateEvents();
                        }
                        return Q();
                    });
                } else {
                    return Q();
                }
            });
        }

        /**
         * @method toggleAutomationControls
         * Toggles the automation controls between visible/hidden.
         */
        toggleAutomationControls () {
            PDE.Automation.toggleAutomationControls();
        }

    }

    COOL.add('ToolboxView', PDEToolboxView);
}
