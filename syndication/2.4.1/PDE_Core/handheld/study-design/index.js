import SD from './handheld_study-design';
import toolBox from './toolbox-config';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(SD, toolBox);
