export default {
    // The current handheld study version
    studyVersion: '00.01',

    // The current handheld study database version
    studyDbVersion: 0,

    // flag to enable partial sync for the handheld modality
    enablePartialSync: false,

    // flag to enable full sync for the handheld modality
    enableFullSync: true,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 30,

    // The amount of time in minutes that a user is allowed to complete a diary within.
    // Should not be > sessionTimeout, since sessionTimeout will happen first and exit the diary anyway.
    // If 0, questionnaire itself doesn't time out, but sessionTimeout will still occur
    questionnaireTimeout: 0,

    // Default volume settings for alarms
    alarmVolumeConfig: {
        // Volume Percentage at which to sound the alarm (0-100)
        alarmVolume: 100,

        // Enable or Disable vibration for notifications
        vibrate: true,

        // Delay after alarm sounds to revert back to the users configured phone settings
        resetDelay: 2
    },

    // Configuration for the eSense feature
    eSenseConfig: {

        // Whether or not Bluetooth should be disabled automatically after an operation is complete
        disableBluetooth: true,

        // Timeout value in milliseconds for eSense all API calls (except findDevices and get records).
        apiCallTimeout: 10000,

        // Timeout value in milliseconds for eSense findDevices API calls (in milliseconds).
        findDevicesTimeout: 60000,

        // Timeout value in milliseconds for eSense get records API calls (in milliseconds).
        getRecordsTimeout: 90000,

        // AM1+ Device specific configurations
        AM1Plus: {

            // Timeout Window configuration for AM1+
            timeWindow: {
                windowNumber: 1,
                startHour: 0,
                startMinute: 0,
                endHour: 23,
                endMinute: 59,
                maxNumber: 15
            }
        }
    },

    // Configuration for rater training
    raterTrainingConfig: {
        logpad: {
            useRaterTraining: false,
            awsKey: '',
            awsSecretKey: '',
            awsRegion: '',
            awsBucket: '',
            rootDirectory: 'rater',
            fallBackBaseURL: '',
            pages: {
                default: 'index.html'
            },
            zipFile: true
        }
    },

    // A collection of indicator configurations
    indicators: []
};
