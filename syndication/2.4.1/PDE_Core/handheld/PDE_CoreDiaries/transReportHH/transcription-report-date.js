// This is a core questionnaire required for the Transcription workflow on the eCoa App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at the study (namespace:STUDY) level.
const year = new Date().getFullYear();

export default {
    screens: [{
        id: 'TRANSCRIPTION_REPORT_DATE_S_1',
        className: 'TRANSCRIPTION_REPORT_DATE_S_1',
        questions: [
            { id: 'TRANSCRIPTION_REPORT_DATE_Q_1', mandatory: true }
        ]
    }],

    questions: [{
        id: 'TRANSCRIPTION_REPORT_DATE_Q_1',
        IG: 'TRANSCRIPTION_REPORT_DATE',
        IT: 'DATE_TIME',
        text: ['TRANSCRIPTION_REPORT_DATE_TEXT'],
        className: 'TRANSCRIPTION_REPORT_DATE_Q_1',
        localOnly: true,
        widget: {
            id: 'TRANSCRIPTION_REPORT_DATE_W_1',
            type: 'DateTimePicker',
            className: 'TRANSCRIPTION_REPORT_DATE_W_1',
            showLabels: true,
            configuration: {
                defaultValue: `${year}-01-01`,
                timeConfiguration: { }
            }
        }
    }]
};
