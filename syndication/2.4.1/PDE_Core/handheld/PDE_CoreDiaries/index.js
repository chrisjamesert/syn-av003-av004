import addUserHH from './addUserHH';
import raterTrainingHH from './raterTrainingHH';
import timeConfHH from './timeConfHH';
import transReportHH from './transReportHH';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(addUserHH, raterTrainingHH, timeConfHH, transReportHH);
