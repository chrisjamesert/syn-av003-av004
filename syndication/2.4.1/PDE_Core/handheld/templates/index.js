import automationButton from './toolboxToggleAutomationButton';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, automationButton);
