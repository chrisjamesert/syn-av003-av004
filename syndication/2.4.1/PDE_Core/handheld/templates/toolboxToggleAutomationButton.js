export default {
    templates: [
        {
            name: 'toolboxMenuToggleAutomationButton',
            namespace: 'PDE',
            template: `<a id="toggleAutomationControls" class="list-group-item">
                           <span class="navigate-right">
                               <span class="title-block"><p class="title">{{ toggleAutomation }}</p></span>
                               <span class="glyphicon glyphicon-menu-right"></span>
                           </span>
                       </a>`
        }
    ]
};
