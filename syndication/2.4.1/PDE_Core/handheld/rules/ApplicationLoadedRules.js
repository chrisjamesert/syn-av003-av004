import extendToolboxViewPDE from 'PDE_Core/handheld/views/PDEToolboxView';

export default {
    rules: [
        {
            id: 'pdeHHApplicationLoaded',
            trigger: 'APPLICATION:Loaded',
            salience: 100,
            evaluate: true,
            resolve: [
                {
                    action: extendToolboxViewPDE
                }
            ]
        }
    ]
}
