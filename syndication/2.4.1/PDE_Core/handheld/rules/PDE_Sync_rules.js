export default {
    rules: [
        {
            id: 'PostActivationFullSync',
            trigger: [
                'INSTALL:ModelsInstalled'
            ],
            evaluate: [
                'AND',
                '!isTrainer',
                'doFullSync'
            ],
            resolve: [
                {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getFullSyncData',
                        activation: true
                    }
                }
            ]
        },

        {
            id: 'ManualFullSync',
            trigger: [
                'TOOLBOX:Transmit'
            ],
            evaluate: [
                'AND',
                '!isTrainer',
                'doFullSync',
                'isOnline',
                'isStudyOnline',
                { expression: 'setNeedToSyncIfPending', input: 'getFullSyncData' }
            ],
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getFullSyncData',
                        activation: false
                    }
                }
            ]
        },

        {
            id: 'ManualPartialSync',
            trigger: [
                'LOGIN:Transmit',
                'DASHBOARD:Transmit'
            ],
            evaluate: [
                'AND',
                '!isTrainer',
                'isOnline',
                'isStudyOnline',
                'doPartialSync'
            ],
            salience: 50,
            resolve: [
                {
                    action: 'displayMessage',
                    data: 'PLEASE_WAIT'
                }, {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getPartialSyncData'
                    }
                }, {
                    action: 'removeMessage'
                }
            ]
        },

        {
            id: 'LoginPartialSync',
            trigger: [
                'LOGIN:Sync'
            ],
            evaluate: [
                'AND',
                'doPartialSync',
                '!isTrainer',
                'isOnline',
                'isStudyOnline'
            ],
            salience: 50,
            resolve: [
                {
                    action: 'displayMessage',
                    data: 'PLEASE_WAIT'
                }, {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getPartialSyncData'
                    }
                }, {
                    action: 'removeMessage'
                }
            ]
        },

        {
            id: 'ForcedFullSync',
            trigger: [
                'DASHBOARD:Open'
            ],
            evaluate: [
                'AND',
                'doFullSync',
                { expression: 'checkNeedToSync', input: 'getFullSyncData' }
            ],
            resolve: [
                {
                    action: 'displayMessage',
                    data: 'PLEASE_WAIT'
                }, {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getFullSyncData'
                    }
                }, {
                    action: 'removeMessage'
                }
            ]
        },

        {
            id: 'ForcedPartialSync',
            trigger: [
                'DASHBOARD:Open'
            ],
            evaluate: [
                'AND',
                'doPartialSync',
                { expression: 'checkNeedToSync', input: 'getPartialSyncData' }
            ],
            resolve: [
                {
                    action: 'displayMessage',
                    data: 'PLEASE_WAIT'
                }, {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getPartialSyncData'
                    }
                }, {
                    data: 'removeMessage'
                }
            ]
        }
    ]
};
