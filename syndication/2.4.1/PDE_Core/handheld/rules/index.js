import { mergeObjects } from 'core/utilities/languageExtensions';

import aLoadRules from './ApplicationLoadedRules';
import syncRules from './PDE_Sync_rules';

export default mergeObjects({}, aLoadRules, syncRules);
