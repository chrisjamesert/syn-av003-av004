/**
 * This file will read the content of all json files and perform transformations on it.
 * The transformation should clean up unnecessary html tags or replace them with shorter html tags.
 * @author - Udit Adhikari
 * @email - udit.adhikari@ert.com
 */

var fs = require('fs');
var path = require('path');

/**
 * This function calls other helper functions to replace unnecessary html tags or shorten them
 * @param fileContent - file content in string
 * @returns {*} - transformed cleaned up json file content
 */
function replaceFileContent(fileContent){
    var replacedFileContent = fileContent;
    replacedFileContent = replaceDivHr(replacedFileContent);
    replacedFileContent = replaceBrNbspBr(replacedFileContent);
    replacedFileContent = replaceBrNbsp(replacedFileContent);
    replacedFileContent = replaceDivNbsp(replacedFileContent);
    replacedFileContent = replaceNbsp(replacedFileContent);
    replacedFileContent = replaceBoldPattern(replacedFileContent);
    replacedFileContent = replaceUnderlinePattern(replacedFileContent);
    replacedFileContent = replaceColorPattern1(replacedFileContent);
    replacedFileContent = replaceColorPattern2(replacedFileContent);
    replacedFileContent = replaceWeirdColorBold(replacedFileContent);
    replacedFileContent = replacedFileContent.trim();
    return replacedFileContent;
}

/**
 * This function replaces <br>&nbsp;<br> with <br><br>
 * @param data - file line content to clean.
 * @returns {string} - return string without &nbsp; between two <br>
 */
function replaceBrNbspBr(data) {
    var strToReplace = /\<br\>\&nbsp\;\<br\>/;
    var replacement = "\<br\>\<br\>";
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}

/**
 * This function replaces <br>&nbsp; with <br>
 * @param data - file content to clean.
 * @returns {string} - return string without &nbsp; after <br>&nbsp;
 */
function replaceBrNbsp(data) {
    var strToReplace = /\<br\>\&nbsp\;/;
    var replacement = "\<br\>";
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}

/**
 * This function will replace with the redundant centering and &nbsp;
 * @param data - file content to clean.
 * @returns {string} - return string without '<div style="text-align: center;">&nbsp;<\/div>'
 */
function replaceDivNbsp(data) {
    var strToReplace = /<div style=\\"text-align: center;\\">&nbsp;<\/div>/;
    var replacement = "";
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}

/**
 * This function will replace redundant centering on <hr>
 * @param data - file content to clean.
 * @returns {string} - return string without '<div style="text-align: center;"><hr><\/div>'
 */
function replaceDivHr(data) {
    var strToReplace = /<div style=\\"text-align: center;\\"><hr><\/div>/;
    var replacement = "<hr>";
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}

/**
 * This function will replace unnecessary '&nbsp;' with space.
 * @param data - file content to clean.
 * @returns {string} - return string that has replaced '&nbsp;' with space.
 */
function replaceNbsp(data) {
    var strToReplace = /\&nbsp\;/;
    var replacement = " ";
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}

/**
 * This function will replace the long '<span style="font-weight: bold;">{{{text}}}</span>'
 * with shorter <b>{{{text}}}</b>
 * @param data - file content to clean.
 * @returns {string} - return string that has replaced long <span> bolding with shorter <b> tags.
 */
function replaceBoldPattern(data) {
    var openBoldTagSearchString = '<span style=\\"font-weight: bold;\\">',
        openTagSearchRegularExp = /<span style=\\"font-weight: bold;\\">/g,
        closeBoldTagSearchString = '<\/span>',
        replacedOpenString = "<b>",
        replacedClosingString = "</b>",
        output = "";

    output = replaceGenericHTMLPattern(openTagSearchRegularExp, openBoldTagSearchString, closeBoldTagSearchString , replacedOpenString, replacedClosingString, data);
    return output;
}

/**
 * This function will replace the long '<span style="text-decoration: underline;">{{{text}}}</span>'
 * with shorter <u>{{{text}}}</u>
 * @param data - file content to clean.
 * @returns {string} - return string that has replaced long <span> underlining with shorter <u> tags.
 */
function replaceUnderlinePattern(data) {
    var openUnderlineTagSearchString = '<span style=\\"text-decoration: underline;\\">',
        openTagSearchRegularExp = /<span style=\\"font-weight: underline;\\">/g,
        closeUnderlineTagSearchString = '<\/span>',
        replacedOpenString = "<u>",
        replacedClosingString = "</u>",
        output = "";

    output = replaceGenericHTMLPattern(openTagSearchRegularExp, openUnderlineTagSearchString, closeUnderlineTagSearchString , replacedOpenString, replacedClosingString, data);
    return output;
}

/**
 * This function will remove the incorrect '<span style="color: black;">{{{text}}}</span>'
 * @param data - file content to clean.
 * @returns {string} - return string that has no text colored as black.
 */
function replaceColorPattern1(data) {
    var openColorTagSearchString = '<span style=\\"color: black;\\">',
        openTagSearchRegularExp = /<span style=\\"color: black;\\">/g,
        closeColorTagSearchString = '<\/span>',
        replacedOpenString = "",
        replacedClosingString = "",
        output = "";

    output = replaceGenericHTMLPattern(openTagSearchRegularExp, openColorTagSearchString, closeColorTagSearchString , replacedOpenString, replacedClosingString, data);
    return output;
}

/**
 * This function will remove the redundant '<span style="color: windowtext;">{{{text}}}</span>'
 * @param data - file content to clean.
 * @returns {string} - return string that has no text colored as windowtext.
 */
function replaceColorPattern2(data) {
    var openColorTagSearchString = '<span style=\\"color: windowtext;\\">',
        openTagSearchRegularExp = /<span style=\\"color: windowtext;\\">/g,
        closeColorTagSearchString = '<\/span>',
        replacedOpenString = "",
        replacedClosingString = "",
        output = "";

    output = replaceGenericHTMLPattern(openTagSearchRegularExp, openColorTagSearchString, closeColorTagSearchString , replacedOpenString, replacedClosingString, data);
    return output;
}

/**
 * This function will remove the weird <span style=\\"color: black; font-weight: bold;\\">{{{text}}}</span>
 * with <b>{{{text}}}</b>
 * @param data - file content to clean.
 * @returns {string} - return string that has repalced '<span style=\\"color: black; font-weight: bold;\\">'
 * with <b> tags.
 */
function replaceWeirdColorBold(data) {
    var openColorTagSearchString = '<span style=\\"color: black; font-weight: bold;\\">',
        openTagSearchRegularExp = /<span style=\\"color: black; font-weight: bold;\\">/g,
        closeColorTagSearchString = '<\/span>',
        replacedOpenString = "<b>",
        replacedClosingString = "</b>",
        output = "";

    output = replaceGenericHTMLPattern(openTagSearchRegularExp, openColorTagSearchString, closeColorTagSearchString , replacedOpenString, replacedClosingString, data);
    return output;
}

/**
 * ****This function is currently not used****
 * This function will remove redundant <div> tags.
 * @param data - file content to clean.
 * @returns {string} - return string with no <div> tags
 */
function replaceDivPattern(data) {
    var openDivTagSearchString = '<div>',
        openTagSearchRegularExp = /<div>/g,
        closeDivTagSearchString = '<\/div>',
        replacedOpenString = "",
        replacedClosingString = "",
        output = "";

    output = replaceGenericHTMLPattern(openTagSearchRegularExp, openDivTagSearchString, closeDivTagSearchString , replacedOpenString, replacedClosingString, data);
    return output;
}

/**
 * This is a generic function will replace open and close html tags with supplied open and close strings
 * @param openTagSearchString - html open tag to be replaced.
 * @param closeTagSearchString - html close tag to be replaced.
 * @param replacedOpenString - string that should replace the supplied 'openTagSearchString'
 * @param replacedClosingString - string that should replace the supplied 'closeTagSearchString'
 * @param data - file content to clean.
 * @returns {string} - return string with replacements performed on open and close html tags
 */
function replaceGenericHTMLPattern(openTagSearchRegularExp, openTagSearchString, closeTagSearchString, replacedOpenString, replacedClosingString, data) {

    var line,
        lines = data.split("\n"),
        searchVal,
        startIndexOpenBold = 0,
        startIndexCloseBold = 0,
        endIndexOpenBold = 0,
        endIndexCloseBold = 0,
        detectedNestedObj,
        detectedNested,
        outputLine = "";

    for(var index=0; index<lines.length; index++) {
        line = lines[index];
        searchVal = searchForIndexWithString(openTagSearchString, line);
        while (searchVal !== -1) {
            startIndexOpenBold = parseInt(searchVal);

            //splice the closing bold span tag and replace with supplied closing tag
            //!!!!!detect nesting after index
            detectedNestedObj = detectNestedTags(line, closeTagSearchString);
            detectedNested = detectedNestedObj.nestedTagsDetected;

            if(detectedNested) {
                line = tranformNestedTags(line, openTagSearchRegularExp, openTagSearchString, closeTagSearchString, replacedOpenString, replacedClosingString);
                searchVal = -1;
            } else {
                startIndexCloseBold = findTheEarliestCloseTagAfterIndex(startIndexOpenBold, line, closeTagSearchString);
                endIndexCloseBold = startIndexCloseBold + closeTagSearchString.length;
                line = spliceAndInsert(startIndexCloseBold, endIndexCloseBold, line.length -1, replacedClosingString, line);

                //splice the opening bold span tag and insert with supplied opening tag
                endIndexOpenBold = startIndexOpenBold + openTagSearchString.length;
                line = spliceAndInsert(startIndexOpenBold, endIndexOpenBold, line.length -1, replacedOpenString, line);

                searchVal = searchForIndexWithString(openTagSearchString, line);
            }

        }
        outputLine += line + "\n";
    }

    return outputLine;
}

/**
 * This function takes a line to parse and the close tag. Based on the close tag it writes a regex pattern for open and close tag.
 * The two common ones we see and only care to parse are <div(.*)>(.*)</div> and <span(.*)>(.*)</span>
 * The function then builds an array of indices of where open tags are present in the line and where the close tags are present in the line.
 * It finally returns an object that contains three properties - nestedTagsDetected, openTagArr, and closeTagArr.
 * @param line - string to parse which represents a line from the file.
 * @param closeTag - the close tag currently being parsed.
 * @returns {{nestedTagsDetected: boolean, openTagArr: Array, closeTagArr: Array}} - an object with following properties
 * 'nestedTagsDetected' is a boolean value that indicates whether a nested tag was detected or not. True if it was otherwise false.
 * 'openTagArr' stores an array of indices of open tags in the line.
 * 'closeTagArr' stores an array of indices of close tags in the line.
 *
 */
function detectNestedTags(line, closeTag) {
    var openTagRegexPattern,
        closeTagRegexPattern,
        numOpenSpanTagFound = 0,
        match,
        indexArrOpenTags = [],
        indexArrCloseTags = [],
        lastIndex = 0,
        extractedOpenTagContent = line,
        extractedCloseTagContent = line,
        nestedTagsDetected = false;

    if(closeTag == "<\/div>") {

        openTagRegexPattern = /<div[^>]*>(.*)/;
        closeTagRegexPattern = /<\/div[^>]*>(.*)/;

    } else if(closeTag == "<\/span>") {

        openTagRegexPattern = /<span[^>]*>(.*)/;
        closeTagRegexPattern = /<\/span[^>]*>(.*)/;

    }

    //find all open tags and build an array
    while(match = openTagRegexPattern.exec(extractedOpenTagContent)) {
        lastIndex = lastIndex + match.index;
        indexArrOpenTags.push(lastIndex);
        extractedOpenTagContent = match[1];
        lastIndex = line.indexOf(extractedOpenTagContent); //find the index of where the sub string starts in original line
    }

    lastIndex = 0;           //reset index

    //find all close tags and build an array
    while(match = closeTagRegexPattern.exec(extractedCloseTagContent)) {
        lastIndex = lastIndex + match.index;
        indexArrCloseTags.push(lastIndex);
        extractedCloseTagContent = match[1];
        lastIndex = line.indexOf(extractedCloseTagContent); //find the index of where the sub string starts in original line
    }

    if(indexArrOpenTags.length == indexArrCloseTags.length) {
        for(var index=0 ; index < indexArrCloseTags.length ; index++) {

            if(index <= indexArrCloseTags.length - 2) {
                if(indexArrCloseTags[index] > indexArrOpenTags[index+1]) { //if any of the close tags start after two opening span tags
                    nestedTagsDetected = true;
                    break;
                }
            }
        }
    }
    var returnObj = {"nestedTagsDetected" : nestedTagsDetected, openTagArr : indexArrOpenTags, closeTagArr : indexArrCloseTags};
    return returnObj;
}


//
/**This function will build a pairing array. It will keep looping till counter index of both open and close reach end of their arrays.
 * It's purpose is to find those open tags that match the regular expression. Whenever the open array index encounters it is smaller than close array index it exists the loop.
 * @param str - string to transform
 * @param openTagSearchRegularExp - open tag search string regular expression
 * @param openTagSearchString - open tag search string
 * @param closeTagSearchString - close tag search string
 * @param openTagReplacement - replacement of the open tag
 * @param closeTagReplacement - replacement of the close tag
 * @returns {string} - transformed string with replacement of tags done at appropriate locations
 */
function tranformNestedTags(str, openTagSearchRegularExp, openTagSearchString, closeTagSearchString, openTagReplacement, closeTagReplacement) {
    var indexOfCorrectClosingTag;
    var returnObj = detectNestedTags(str, closeTagSearchString);
    var openTagArr = returnObj.openTagArr;
    var closeTagArr = returnObj.closeTagArr;
    var lenOfOpenTagArr = openTagArr.length;
    var lenOfCloseTagArr = closeTagArr.length;
    var indexOfOpenTag = 0;
    var indexOfCloseTag = 0;
    var tempIndexOpenTag = 0;
    var tempIndexCloseTag = 0;
    var countOpenTags = 0;
    var countCloseTags = 0;
    var lastOpenTagIndex = 0;

    var pairedArr = [];

    //build the array
    for(var index=0; index<openTagArr.length; index++) {
        pairedArr.push({"openTagIndex" : openTagArr[index], "closeTagIndex" : -1});
    }

    while(indexOfCloseTag < lenOfCloseTagArr ) {

        //find the open tag that is greater than the current close tag
        if(indexOfOpenTag < lenOfOpenTagArr) {
            while (closeTagArr[tempIndexCloseTag] > openTagArr[tempIndexOpenTag]) {
                tempIndexOpenTag++;
            }
            countOpenTags = tempIndexOpenTag - 1;
            indexOfOpenTag = tempIndexOpenTag;

            while (countOpenTags >= 0 ) {

                if((tempIndexOpenTag == lenOfOpenTagArr) || (closeTagArr[tempIndexCloseTag] < openTagArr[tempIndexOpenTag])) {
                    if (pairedArr[countOpenTags].closeTagIndex == -1) {
                        pairedArr[countOpenTags].closeTagIndex = closeTagArr[tempIndexCloseTag];
                        tempIndexCloseTag++;
                    }
                } else {
                    break;
                }
                countOpenTags--;
            }
        }
        else {
            pairedArr[countOpenTags].closeTagIndex = closeTagArr[tempIndexCloseTag];
            tempIndexCloseTag++;
        }
        indexOfCloseTag = tempIndexCloseTag;

    }

    var indexArrOfOpeningTags = [];
    var match;
    var transformedString = "";

    while (match = openTagSearchRegularExp.exec(str)) {
        indexArrOfOpeningTags.push(match.index);
    }

    // stores an array of object. Each object is contains attribute
    // openTagIndex - starting index of opening tag
    // closeTagIndex - starting index of closing tag
    var arrIndexToReplace = [];

    for(var index=0; index<indexArrOfOpeningTags.length; index++) {
        for(var index2=0; index2 < pairedArr.length; index2++ ) {
            if(pairedArr[index2].openTagIndex == indexArrOfOpeningTags[index]) {
                arrIndexToReplace.push(pairedArr[index2]);
            }
        }
    }

    transformedString = replaceOpenAndCloseTags(str, arrIndexToReplace, openTagSearchString, closeTagSearchString, openTagReplacement, closeTagReplacement);

    return transformedString;
}

/**
 * This function iterates over all elements in 'replaceIndexArr'. An element of 'replaceIndexArr'
 * contains 'openTagIndex' and 'closeTagIndex'. This contain index to
 * @param line - line to perform replacement on
 * @param replaceIndexArr - arr of object that contains open tag index and close tag index
 * @param openTagReplacement - replacement string of open tag
 * @param closeTagReplacement - replacement string of close tag
 */
function replaceOpenAndCloseTags(line, replaceIndexArr, openTagSearchString, closeTagSearchString, openTagReplacement, closeTagReplacement) {

    var openTagStartIndex = 0,
        openTagEndIndex = 0,
        closeTagStartIndex = 0,
        closeTagEndIndex = 0,
        lastLineLen = 0,
        lineLenDif = 0;         //when replacements happen we need to update the indices of where to replace

    lastLineLen = line.length;

    for (var index=0; index<replaceIndexArr.length; index++) {


        if(replaceIndexArr[index].openTagIndex >= 0 && replaceIndexArr[index].closeTagIndex >= 0) {
            //replace open tag
            openTagStartIndex = replaceIndexArr[index].openTagIndex;
            openTagEndIndex = openTagStartIndex + openTagSearchString.length;
            openTagStartIndex = openTagStartIndex - lineLenDif;
            openTagEndIndex = openTagEndIndex - lineLenDif;
            line = spliceAndInsert(openTagStartIndex, openTagEndIndex, line.length - 1, openTagReplacement, line);
            lineLenDif += lastLineLen - line.length; //adding lineLefDif because our index arr maintains indices in the very orginal text
            lastLineLen = line.length;

            //replace close tag
            openTagStartIndex = replaceIndexArr[index].closeTagIndex;
            openTagEndIndex = openTagStartIndex + closeTagSearchString.length;
            openTagStartIndex = openTagStartIndex - lineLenDif;
            openTagEndIndex = openTagEndIndex - lineLenDif;
            line = spliceAndInsert(openTagStartIndex, openTagEndIndex, line.length - 1, closeTagReplacement, line);
            lineLenDif += lastLineLen - line.length; //adding lineLefDif because our index arr maintains indices in the very orginal text
            lastLineLen = line.length;
        }

    }

    return line;
}

/**
 * This function will return the index value for a string to be searched inside a string
 * @param searchString - string to search
 * @param data - string on which search is performed
 * @returns {number|Number} - returns index value of the searched string
 */
function searchForIndexWithString(searchString, data) {
    return data.indexOf(searchString);
}

/**
 * This function will insert a supplied string from the start to end indices supplied on a string being operated
 * @param start - start index of the sub-string to be replaced
 * @param end - end index of the sub-string to be replaced
 * @param rem - the last index of the string - strToOperateOn
 * @param strToInsert - string to insert on the replaced string
 * @param strToOperateOn - It is the string on which replacement operation will be performed.
 * It will usually be the file line to clean
 * @returns {string} - return cleaned up string with replacement done with 'strToInsert' to 'strToOperateOn'
 */
function spliceAndInsert(start, end, rem, strToInsert, strToOperateOn) {
    var startStr = strToOperateOn.substr(0, parseInt(start));
    var endStr = strToOperateOn.substr(end, rem);
    var finalStr = startStr + strToInsert + endStr;
    return finalStr;
}

/**
 * This function finds the index of earliest close tag supplied to it after a given index
 * @param index - string index to search after
 * @param string - string to perform search on
 * @param closeTagSearchString - close tag to search for
 * @returns {*} - returns the index of earliest close tag
 */
function findTheEarliestCloseTagAfterIndex(index, string, closeTagSearchString) {
    var substr = string.substr(index, string.length);
    var subStrIndexOfClosingSpan = substr.indexOf(closeTagSearchString);
    var indexOfClosingSpan = index + subStrIndexOfClosingSpan;
    return indexOfClosingSpan;
}

/**
 * This function returns the colors found in a file. This will be used to generate a log file
 * which can be used by PDE to cross check with SDA whether these colors were delibrate.
 * @param fileName - name of the file
 * @param data - file content
 * @returns {Array} - array of color codes found in the file.
 */
function findColors(fileName, data) {
    var line,
        lines = data.split("\n"),
        searchString = /<span style=\\"color: (.*);\\">/,
        lineColorsFound = [],
        allColorsFound = [];

    allColorsFound[0] = fileName + " : "; //first index has file name

    for(var index=0; index<lines.length; index++) {
        line = lines[index];
        lineColorsFound = line.match(searchString);
        if(lineColorsFound && lineColorsFound.length > 0) {
            allColorsFound.push(lineColorsFound[1]);
        }
    }
    allColorsFound.push("\n");
    return allColorsFound;
}

//define all export functions
module.exports.replaceFileContent = replaceFileContent;
module.exports.replaceBrNbspBr = replaceBrNbspBr;
module.exports.replaceDivHr = replaceDivHr;
module.exports.replaceBrNbspBr = replaceBrNbspBr;
module.exports.replaceBrNbsp = replaceBrNbsp;
module.exports.replaceDivNbsp = replaceDivNbsp;
module.exports.replaceNbsp = replaceNbsp;
module.exports.replaceBoldPattern = replaceBoldPattern;
module.exports.replaceUnderlinePattern = replaceUnderlinePattern;
module.exports.replaceColorPattern1 = replaceColorPattern1;
module.exports.replaceColorPattern2 = replaceColorPattern2;
module.exports.replaceWeirdColorBold = replaceWeirdColorBold;
module.exports.replaceDivPattern = replaceDivPattern;
module.exports.findColors = findColors;
