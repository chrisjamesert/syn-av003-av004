/**
 * This file's original purpose is to make small alterations to unit test xml output to prevent errors during the
 * process of generating html. It is based on Udit Adhikari's readFiles for json cleanup
 * @author Mason Blaschak
 * @email mason.blaschak@ert.com
 */

var fs = require('fs');
var path = require('path');
var fileUtility = require('./XMLfileUtility');


/**
 * This function iterates over all XML files and calls helper functions
 * to perform clean up transformation on them.
 *
 * @param directoryPath - parent directory to all language json files
 */
function cleanFile(filePath) {

    var fileOutputPath;
    var colorsFoundArr = [];
    var colorsFoundStr;

    //repalce file content
    var fileContent = readFileContent(filePath);
    var replacedFileContent = fileUtility.replaceFileContent(fileContent);

    //write the file
    fileOutputPath = filePath;
    writeFileContent(fileOutputPath, replacedFileContent);

    return replacedFileContent;
}

/**
 * This function will get all filename in a directory
 * @param path - path to the parent directory of all json files
 * @returns {*} - array of file names in the directory
 */
function getAllFilesInPath(path) {
    var fileNames = fs.readdirSync(path);
    return fileNames;
}

/**
 * This function will get the content of a file
 * @param filePath - path to the file
 * @returns {*} - returns the content of the file as a string.
 */
function readFileContent(filePath){
    var fileContent = fs.readFileSync(filePath, 'utf8');
    return fileContent;
}

/**
 * This function will write to a file
 * @param filePath - path of the file to be written
 * @param fileContent - data to be writte to file
 */
function writeFileContent(filePath, fileContent) {
    fs.writeFileSync(filePath, fileContent);
}

module.exports.cleanFile = cleanFile;
