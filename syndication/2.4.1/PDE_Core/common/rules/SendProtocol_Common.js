export default {
    rules: [
        {
            id: 'SendProtocol',
            trigger: [
                'QUESTIONNAIRE:Completed'
            ],
            evaluate: 'formHasSubjectContext',
            resolve: [
                {
                    action: 'addProtocolToForm'
                }
            ]

        }
    ]
};
