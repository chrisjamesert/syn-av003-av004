import AutomationController from 'PDE_Core/common/automation/controllers/AutomationController';
import { allowPDEAutomationUtil } from 'PDE_Core/common/automation/utilities';

export default {
    rules: [
        {
            id: 'pdeAutomationSetup',
            trigger: [
                'LOGIN:Rendered'
            ],
            evaluate: function (filter, resume) {
                resume(allowPDEAutomationUtil() && !localStorage.getItem('screenshot'));
            },
            resolve: [
                {
                    action: (input, done) => {
                        if (!PDE.Automation.isInitialised) {
                            PDE.Automation = new AutomationController();
                            PDE.Automation.init()
                            .then(() => {
                                done();
                            });
                        } else {
                            done();
                        }
                    }
                }
            ]
        }
    ]
};
