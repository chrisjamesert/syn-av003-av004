import SOrules from './screenOrientationRules';
import syncRules from './PDE_SyncRules_Common';
import loadProtStrings from './loadProtocolStrings';
import SendProtocol from './SendProtocol_Common';
import automationRules from './automationRules_Common';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, SOrules, syncRules, loadProtStrings, SendProtocol, automationRules);
