import Logger from 'core/Logger';

export default {
    rules: [
        {
            id: 'SyncDataReceived',
            trigger: [
                'CUSTOMDATASYNC:Received/getFullSyncData',
                'CUSTOMDATASYNC:Received/getPartialSyncData'
            ],
            evaluate: true,
            resolve: [
                {
                    action (input) {
                        // Clear sync flag
                        localStorage.removeItem(`NeedToSync_${input.webServiceFunction}`);
                        localStorage.removeItem('PDE_ActivationSync');
                        let logger = new Logger('StudyRules:SyncDataReceived'),

                            // resolves with a model prepped to save, or rejects with an error
                            processModel = (modelXML) => {
                                return Q()
                                    .then(() => {
                                        if (modelXML) {
                                            logger.info(`Processing XML for single model: ${modelXML.nodeName}`);
                                            let model = new LF.Model[modelXML.nodeName]();
                                            $.each(modelXML.attributes, (i, attr) => {
                                                logger.info(`adding attribute "${attr.name}" with value "${attr.value}" to model ${modelXML.nodeName}`);
                                                if (model.set(attr.name, attr.value) === false) {
                                                    // error is logged appropriately later by promise chain
                                                    throw new Error(`attribute "${attr.name}" with value "${attr.value}" failed validation, check schema for ${modelXML.nodeName}`);
                                                }
                                            });
                                            return model;
                                        }

                                        // error is logged appropriately later by promise chain
                                        throw new Error('No XML to process for this model.');
                                    });
                            },

                            // resolves with an object containing two keys:
                            // {
                            //      collectionName: string name of collection this applies to,
                            //      preppedModels: array of models successfully prepped to save
                            //                      (could be an empty array if collection should be empty)
                            // }
                            // or rejects with an error
                            processCollection = (collectionXML) => {
                                if (collectionXML) {
                                    if ($(collectionXML).children().length > 0) {
                                        logger.info(`Processing XML for collection: ${collectionXML.nodeName}. ${$(collectionXML).children().length} model(s) available for processing`);
                                        let promiseModelFactory = _($(collectionXML).children()).map(modXML => processModel(modXML));
                                        return Q.allSettled(promiseModelFactory)
                                            .then((settledPromiseArray) => {
                                                let successfulSnapshots = _(settledPromiseArray).filter({ state: 'fulfilled' }),
                                                    rejectedSnapshots = _(settledPromiseArray).filter({ state: 'rejected' });
                                                logger.info(`Collection ${collectionXML.nodeName}: successfully prepped ${successfulSnapshots.length}/${settledPromiseArray.length} model(s) for saving`);
                                                if (rejectedSnapshots.length > 0) {
                                                    logger.traceEnter(`${rejectedSnapshots.length} models(s) were unable to be prepped, listing rejection reasons:`);
                                                    _(rejectedSnapshots).forEach((rejected) => {
                                                        logger.warn(`Prepping model for collection ${collectionXML.nodeName} failed:`, rejected.reason);
                                                    });
                                                    logger.traceExit();
                                                }

                                                // return object containing the collection name, and all prepped models
                                                return {
                                                    collectionName: collectionXML.nodeName,
                                                    preppedModels: _(successfulSnapshots).map(snapshot => snapshot.value)
                                                };
                                            });
                                    }
                                    logger.info(`Collection ${collectionXML.nodeName} has no models to process.`);

                                    // return object containing the collection name, and no prepped models,
                                    // so we will simply clear the collection
                                    return Q({ collectionName: collectionXML.nodeName, preppedModels: [] });
                                }
                                throw new Error('No XML to process for this collection');
                            };
                        return Q(logger.traceEnter('Begin Parsing PDE CustomDataSync'))
                            .then(() => {
                                return $($.parseXML(input)).children();
                            })
                            .then((customSyncXML) => {
                                logger.info(`${customSyncXML.children().length} collection(s) available for processing from sync.`);
                                let promiseCollectionFactory = _(customSyncXML.children()).map(colXML => processCollection(colXML));
                                return Q.allSettled(promiseCollectionFactory)
                                    .then((settledPromiseArray) => {
                                        let successfulSnapshots = _(settledPromiseArray).filter({ state: 'fulfilled' }),
                                            rejectedSnapshots = _(settledPromiseArray).filter({ state: 'rejected' });
                                        logger.info(`Successfully prepped ${successfulSnapshots.length}/${settledPromiseArray.length} collection(s) for saving.`);
                                        if (rejectedSnapshots.length > 0) {
                                            logger.traceEnter(`${rejectedSnapshots.length} collection(s) were unable to be processed, listing rejection reasons:`);
                                            _(rejectedSnapshots).forEach((rejected) => {
                                                logger.warn('A collection failed to process:', rejected.reason);
                                            });
                                            logger.traceExit();
                                        }
                                        return _(successfulSnapshots).map(snapshot => snapshot.value);
                                    })
                                    .then((preppedCollections) => {
                                        let promiseColClearFactory = _(preppedCollections).map((preppedObject) => {
                                            return Q()
                                                .then(() => new LF.Collection[preppedObject.collectionName]())
                                                .then(collection => collection.clear())
                                                .then(() => {
                                                    logger.info(`Successfully cleared collection ${preppedObject.collectionName}`);
                                                    let promiseModSaveFactory = _(preppedObject.preppedModels).map(mod => mod.save());
                                                    return Q.allSettled(promiseModSaveFactory)
                                                        .then((settledPromiseArray) => {
                                                            let successfulSnapshots = _(settledPromiseArray).filter({ state: 'fulfilled' }),
                                                                rejectedSnapshots = _(settledPromiseArray).filter({ state: 'rejected' });
                                                            logger.info(`Successfully saved ${successfulSnapshots.length}/${settledPromiseArray.length} model(s) into collection ${preppedObject.collectionName}`);
                                                            if (rejectedSnapshots.length > 0) {
                                                                logger.traceEnter(`Collection ${preppedObject.collectionName} was cleared, but ${rejectedSnapshots.length} model(s) failed to save, listing rejection reasons:`);
                                                                _(rejectedSnapshots).forEach((rejected) => {
                                                                    logger.error('Failed to save model after clearing collection! We are now potentially missing data.', rejected.reason);
                                                                });
                                                                logger.traceExit();
                                                            }
                                                        });
                                                });
                                        });
                                        return Q.allSettled(promiseColClearFactory)
                                            .then((settledPromiseArray) => {
                                                let successfulSnapshots = _(settledPromiseArray).filter({ state: 'fulfilled' }),
                                                    rejectedSnapshots = _(settledPromiseArray).filter({ state: 'rejected' });
                                                logger.info(`Successfully cleared ${successfulSnapshots.length}/${settledPromiseArray.length} collections, and processed saving of new models.`);
                                                if (rejectedSnapshots.length > 0) {
                                                    logger.traceEnter(`${rejectedSnapshots.length} collections(s) failed to clear or encountered some other unexpected error, listing rejection reasons:`);
                                                    _(rejectedSnapshots).forEach((rejected) => {
                                                        let rejectedColName = preppedCollections[_(settledPromiseArray).indexOf(rejected)].collectionName;
                                                        logger.error(`Collection ${rejectedColName} failed to clear or encountered unexpected error:`, rejected.reason);
                                                    });
                                                    logger.traceExit();
                                                }
                                            });
                                    });
                            }).catch((err) => {
                                logger.error('Unexpected error processing custom sync:', err);
                            }).finally(() => {
                                logger.traceExit();
                            });
                    }
                }
            ]
        }
    ]
};
