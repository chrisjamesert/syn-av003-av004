export default {
    rules: [
        {
            id: 'LoadProtocolStrings',
            trigger: 'APPLICATION:Loaded',
            evaluate: true,
            salience: 10, // needs high salience because APPLICATION:Loaded has a rule with 'preventAll' action
            resolve: [
                {
                    action: 'buildProtocolStrings'
                }
            ]
        }
    ]
};
