/*
This file contains wrappers for functionality that existed in previous templates, but has since been modified.
It allows the previous functionality, but logs a warning indicating the depreciation, what should be used instead,
and an end trigger for when the depreciated functionality will be fully disabled.
 */
import Logger from 'core/Logger';
import { navigateToScreen } from 'PDE_Core/common/branching/branchingHelpers';
import * as CollectionModelUtils from 'PDE_Core/common/utilities/CollectionModelUtils';
import * as DateTimeUtils from 'PDE_Core/common/utilities/DateTimeUtils';
import * as DeviceStatusUtils from 'PDE_Core/common/utilities/deviceStatusUtils';

const logger = new Logger('DEPRECIATED_FUNCTIONALITY'),
    depIn_2_5 = 'This reference will be removed in Syndication 2.5';

// PDE_Core_TODO: Remove in Syndication 2.5
LF.Utilities.navigateToScreen = (screenID) => {
    logger.warn(`Expected Invocations: "LF.Branching.Helpers.navigateToScreen(screenID)"\nor "import BranchHelpers from 'core/branching/branchingHeleprs'; ... BranchHelpers.navigateToScreen(screenID)"\nor "import {navigateToScreen} from  'PDE_Core/common/branching/branchingHelpers'; ... navigateToScreen(screenID)".\n${depIn_2_5}`);
    return navigateToScreen(screenID);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.Utilities.getCollection = (collectionName) => {
    logger.warn(`Expected Invocations: "PDE.CollectionModelUtils.getCollection(collectionName);"\nor "import {getCollection} from 'PDE_Core/common/utilities/CollectionModelUtils'; ... getCollection(collectionName);".\n${depIn_2_5}`);
    return CollectionModelUtils.getCollection(collectionName);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.Utilities.getCollections = (collectionNames) => {
    logger.warn(`Expected Invocations: "PDE.CollectionModelUtils.getCollections(collectionNames);"\nor "import {getCollections} from 'PDE_Core/common/utilities/CollectionModelUtils'; ... getCollections(collectionNames);".\n${depIn_2_5}`);
    return CollectionModelUtils.getCollections(collectionNames);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.Utilities.dateDiffInDays = (date1, date2, useAbsolute) => {
    logger.warn(`Expected Invocations: "PDE.DateTimeUtils.dateDiffInDays(date1, date2, useAbsolute);"\nor "import {dateDiffInDays} from 'PDE_Core/common/utilities/DateTimeUtils'; ... dateDiffInDays(date1, date2, useAbsolute);".\n${depIn_2_5}`);
    return DateTimeUtils.dateDiffInDays(date1, date2, useAbsolute);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.DynamicText.format = (date, format) => {
    logger.warn(`Expected Invocations: "PDE.DateTimeUtils.format(date, format);"\nor "import {format} from 'PDE_Core/common/utilities/DateTimeUtils'; ... format(date, format);".\n${depIn_2_5}`);
    return DateTimeUtils.format(date, format);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.DynamicText.formatDate = (date) => {
    logger.warn(`Expected Invocations: "PDE.DateTimeUtils.formatDate(date);"\nor "import {formatDate} from 'PDE_Core/common/utilities/DateTimeUtils'; ... formatDate(date);".\n${depIn_2_5}`);
    return DateTimeUtils.formatDate(date);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.DynamicText.formatTime = (time) => {
    logger.warn(`Expected Invocations: "PDE.DateTimeUtils.formatDate(date);"\nor "import {formatDate} from 'PDE_Core/common/utilities/DateTimeUtils'; ... formatDate(date);".\n${depIn_2_5}`);
    return DateTimeUtils.formatTime(time);
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.Utilities.isTrainer = () => {
    logger.warn(`Expected Invocations: "PDE.DeviceStatusUtils.isTrainer();"\nor "import {isTrainer} from 'PDE_Core/common/utilities/deviceStatusUtils'; ... isTrainer();".\n${depIn_2_5}`);
    return DeviceStatusUtils.isTrainer();
};

// PDE_Core_TODO: Remove in Syndication 2.5
LF.Utilities.isScreenshotMode = () => {
    logger.warn(`Expected Invocations: "PDE.DeviceStatusUtils.isScreenshotMode();"\nor "import {isScreenshotMode} from 'PDE_Core/common/utilities/deviceStatusUtils'; ... isScreenshotMode();".\n${depIn_2_5}`);
    return DeviceStatusUtils.isTrainer();
};
