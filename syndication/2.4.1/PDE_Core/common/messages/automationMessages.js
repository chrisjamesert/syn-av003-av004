/**
 * File: PDE_Automation/messages.js
 * Author: Chris James, chris.james@ert.com
 * Date: 19 Jul 2018
 */
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

export default {
    messages: [
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_SCRIPT_EXISTS',
            message: MessageHelpers.notifyDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_SCRIPT_EXISTS',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_FILE_NOT_FOUND',
            message: MessageHelpers.notifyDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_FILE_NOT_FOUND',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_NOT_ON_LOGIN',
            message: MessageHelpers.notifyDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_NOT_ON_LOGIN',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_CONFIRM_DELETE',
            message: MessageHelpers.confirmDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_CONFIRM_DELETE',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_CONFIRM_DISCARD',
            message: MessageHelpers.confirmDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_CONFIRM_DISCARD',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_CANNOT_TOGGLE',
            message: MessageHelpers.notifyDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_CANNOT_TOGGLE',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_STUDY_VERSION_DIFF',
            message: MessageHelpers.confirmDialogCreator({
                ok: 'PDE_AUTOMATION_PLAY',
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_STUDY_VERSION_DIFF'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_BROWSER_NAME_DIFF',
            message: MessageHelpers.confirmDialogCreator({
                ok: 'PDE_AUTOMATION_PLAY',
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_BROWSER_NAME_DIFF'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_BROWSER_VERSION_DIFF',
            message: MessageHelpers.confirmDialogCreator({
                ok: 'PDE_AUTOMATION_PLAY',
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_BROWSER_VERSION_DIFF'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_MODALITY_DIFF',
            message: MessageHelpers.notifyDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_MODALITY_DIFF',
                type: 'warning'
            })
        },
        {
            type: 'Dialog',
            key: 'PDE_AUTOMATION_ROLE_DIFF',
            message: MessageHelpers.notifyDialogCreator({
                header: 'PDE_AUTOMATION',
                message: 'PDE_AUTOMATION_ROLE_DIFF',
                type: 'warning'
            })
        }
    ]
};
