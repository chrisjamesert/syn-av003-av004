import automationMessages from './automationMessages';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, automationMessages);
