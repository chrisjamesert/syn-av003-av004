export default {
    templates: [
        {
            name: 'automationShareTemplate',
            namespace: 'PDE',
            template: `<div id='script-share-dialog-msg'>{{ message }}</div>
                <select id='script-share-dialog-select' size='10'></select>
                <div id='script-share-dialog-buttons'>
                    <button id='script-share-dialog-share' class='btn fa fa-share-alt' type='button'></button>
                    <button id='script-share-dialog-cancel' class='btn' type='button'>{{ cancel }}</button>
                </div>`
        }
    ]
};
