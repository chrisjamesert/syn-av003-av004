export default {
    templates: [
        {
            name: 'automationControlsTemplate',
            namespace: 'PDE',
            template: `<div id='script-title'>{{ titleText }}</div>
                <div id='script-buttons'>
                    <button id='script-play' class='btn' type='button'></button>
                    <button id='script-record' class='btn' type='button'></button>
                    <button id='script-import' class='btn fa fa-arrow-down' type='button'></button>
                    <button id='script-share' class='btn fa fa-share-alt' type='button'></button>
                    <div id='script-state-indicator' class=''></div>
                </div>
                <div id='script-info-text'></div>`
        }
    ]
};
