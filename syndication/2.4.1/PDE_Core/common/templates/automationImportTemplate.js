export default {
    templates: [
        {
            name: 'automationImportTemplate',
            namespace: 'PDE',
            template: `<div id='script-import-dialog-msg'>{{ message }}</div>
                <div id='script-import-dialog-buttons'>
                    <input type='file' id='script-import-input-file' name='file' />
                    <button id='script-import-file-load' class='btn' type='button'>{{ importText }}</button>
                    <button id='script-import-cancel' class='btn' type='button'>{{ cancel }}</button>
                </div>`
        }
    ]
};
