export default {
    templates: [
        {
            name: 'automationSaveTemplate',
            namespace: 'PDE',
            template: `<div id='script-save-dialog-msg'>{{ message }}</div>
                <input id='script-save-dialog-input' type='text'/>
                <div id='script-save-dialog-buttons'>
                    <button id='script-save' class='btn fa fa-save green' type='button'></button>
                    <button id='script-discard' class='btn fa fa-trash red' type='button'></button>
                </div>`
        }
    ]
};
