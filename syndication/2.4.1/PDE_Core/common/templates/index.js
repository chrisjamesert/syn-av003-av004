import controlsTemplate from './automationControlsTemplate';
import saveTemplate from './automationSaveTemplate';
import playTemplate from './automationPlayTemplate';
import importTemplate from './automationImportTemplate';
import shareTemplate from './automationShareTemplate';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, controlsTemplate, saveTemplate, playTemplate, importTemplate, shareTemplate);
