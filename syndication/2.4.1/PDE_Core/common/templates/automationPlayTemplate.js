export default {
    templates: [
        {
            name: 'automationPlayTemplate',
            namespace: 'PDE',
            template: `<div id='script-play-dialog-msg'>{{ message }}</div>
                <select id='script-play-dialog-select' size='10'></select>
                <div id='script-play-dialog-buttons'>
                    <button id='script-play-dialog-play' class='btn fa fa-play green' type='button'></button>
                    <button id='script-play-dialog-delete' class='btn fa fa-trash red' type='button'></button>
                    <button id='script-play-dialog-cancel' class='btn' type='button'>{{ cancel }}</button>
                </div>`
        }
    ]
};
