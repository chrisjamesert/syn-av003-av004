import AutomationController from 'PDE_Core/common/automation/controllers/AutomationController';

export default class PDEAutomationView extends Backbone.View {

    constructor (controller) {
        super();

        if (controller.__proto__.constructor.name !== 'AutomationController') {
            throw new Error(`Expected controller to be an AutomationController but received ${controller.__proto__.constructor.name}`)
        }
        this.controller = controller;
    }

    /**
     * Find and return and underscore template.
     * @returns {Function <_.template>}
     */
    getTemplate () {
        return LF.templates.getTemplateFromKey(this.template);
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {Object|string} strings The translations keys to translate.
     * @param {Function} callback A callback function invoked when the strings have been translated.
     * @param {Object} options Options passed into the translation.
     * @returns {Q.Promise<Object>}
     */
    i18n (strings, callback, options) {
        return LF.strings.display.call(LF.strings, strings, callback, options);
    }

    /**
     * A simple render method that can be chained by subclasses if required.
     * @param {Object} dynamicStrings Optional dynamicStrings object.
     * @returns {Q.Promise<any>}
     */
    render (dynamicStrings = {}) {
        let template = this.getTemplate();
        return this.i18n(this.templateStrings)
        .then((translated) => {
            let strings = _.extend(translated, dynamicStrings);
            this.$el.html(template(strings));
            this.hide(true);
            $('body').append(this.$el);
            this.delegateEvents();
        })
    }

    /**
     * Show the automation view.
     */
    show () {
        this.$el.show();
    }

    /**
     * Determines if the automation view is displayed.
     * @returns {boolean} True if displayed, false if not.
     */
    isShown () {
        return this.$el.is(':visible');
    }

    /**
     * Hides the automation view.
     */
    hide () {
        this.$el.hide();
    }
}
