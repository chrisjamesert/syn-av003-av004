import PDEAutomationView from './PDEAutomationView';

export default class PDEAutomationSaveView extends PDEAutomationView {

    constructor (controller) {
        super(controller);

        this.events = {
            'click button#script-save': 'save',
            'click button#script-discard': 'discard'
        };

        this.template = 'PDE:automationSaveTemplate';

        this.templateStrings = {
            message: 'PDE_AUTOMATION_SAVE_MSG'
        }
    }

    get id () {
        return 'script-save-dialog'
    }

    get input () {
        let $input = this.$el.find('#script-save-dialog-input');
        if (!$input.length) {
            throw new Error('Unable to locate "#script-save-dialog input" element');
        }
        return $input.val();
    }

    set input (text) {
        let $input = this.$el.find('#script-save-dialog-input');
        if (!$input.length) {
            throw new Error('Unable to locate "#script-save-dialog input" element');
        }
        $input.val(text);
    }

    save () {
        this.controller.save();
    }

    discard () {
        this.controller.discard();
    }
}
