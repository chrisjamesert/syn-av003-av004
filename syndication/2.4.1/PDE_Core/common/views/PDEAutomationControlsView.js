import PDEAutomationView from './PDEAutomationView';

export default class PDEAutomationControlsView extends PDEAutomationView {

    constructor (controller) {
        super(controller);

        this.events = {
            'click button#script-record': 'startRecording',
            'click button#script-play': 'firstPlayButtonClick',
            'click button#script-import': 'importButtonClick',
            'click button#script-share': 'shareButtonClick'
        };

        this.template = 'PDE:automationControlsTemplate';

        this.templateStrings = {
            titleText: 'PDE_AUTOMATION'
        };
    }

    get id () {
        return 'script-controls';
    }

    render () {
        return super.render()
        .then(() => {
            this.refreshControls();
        });
    }

    refreshControls () {
        const PLAY_CLASSES = 'fa fa-play green',
            STOP_CLASSES = 'fa fa-square blue',
            REC_CLASSES = 'fa fa-circle red';

        let $playButton = this.$el.find('button#script-play'),
            $recordButton = this.$el.find('button#script-record'),
            $importButton = this.$el.find('button#script-import'),
            $shareButton = this.$el.find('button#script-share');

        $playButton.removeClass(PLAY_CLASSES);
        $playButton.removeClass(STOP_CLASSES);
        $recordButton.removeClass(REC_CLASSES);
        $recordButton.removeClass(STOP_CLASSES);

        $playButton.addClass(this.controller.playingScript ? STOP_CLASSES : PLAY_CLASSES);
        $recordButton.addClass(this.controller.recordingScript ? STOP_CLASSES : REC_CLASSES);

        // .attr does not handle falsy values well, so we turn them into booleans with !!
        $playButton.attr('disabled', !!(this.controller.recordingScript || this.controller.getScriptCount() === 0));
        $recordButton.attr('disabled', !!this.controller.playingScript);
        $importButton.attr('disabled', !!(this.controller.recordingScript || this.controller.playingScript));
        $shareButton.attr('disabled', !!(this.controller.recordingScript || this.controller.playingScript || this.controller.getScriptCount() === 0));

        if (!LF.Wrapper.isWrapped) {
            $shareButton.hide();
        }
        this.setInfoText('');
    }

    show () {
        super.show();
        localStorage.setItem('PDE_Automation_is_shown', 'true');
    }

    hide (isRender) {
        super.hide();
        if (!isRender) {
            localStorage.removeItem('PDE_Automation_is_shown');
        }
    }

    toggleVisibility() {
        if (this.isShown()) {
            this.hide();
        } else {
            this.show();
        }
    }

    startRecording () {
        this.controller.startRecording();
    }

    firstPlayButtonClick () {
        this.controller.firstPlayButtonClick();
    }

    importButtonClick () {
        this.controller.showImportView();
    }

    shareButtonClick () {
        this.controller.showShareView();
    }

    flashIndicator () {
        let $indicator = $('#script-state-indicator');
        if (!$indicator.length) {
            throw new Error('Cannot locate #script-state-indicator');
        }
        PDE.Automation.indicatorOn = !PDE.Automation.indicatorOn;
        if (PDE.Automation.playingScript) {
            if (PDE.Automation.indicatorOn) {
                $indicator.removeClass('fa fa-play green');
                $indicator.addClass('fa fa-play clear');
            } else {
                $indicator.removeClass('fa fa-play clear');
                $indicator.addClass('fa fa-play green');
            }
            setTimeout(PDE.Automation.controlsView.flashIndicator, 500);
        } else if (PDE.Automation.recordingScript) {
            if (PDE.Automation.indicatorOn) {
                $indicator.removeClass('fa fa-circle red');
                $indicator.addClass('fa fa-circle clear');
            } else {
                $indicator.removeClass('fa fa-circle clear');
                $indicator.addClass('fa fa-circle red');
            }
            setTimeout(PDE.Automation.controlsView.flashIndicator, 500);
        } else {
            $indicator.removeClass('fa fa-circle fa-play clear green red');
            PDE.Automation.indicatorOn = false;
        }
    }

    setInfoText (text) {
        let $infoText = this.$el.find('div#script-info-text');
        $infoText.text(text);
    }
}
