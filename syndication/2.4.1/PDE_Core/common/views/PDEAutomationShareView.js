import PDEAutomationView from './PDEAutomationView';

export default class PDEAutomationShareView extends PDEAutomationView {

    constructor (controller) {
        super(controller);

        this.events = {
            'click button#script-share-dialog-share': 'share',
            'click button#script-share-dialog-cancel': 'hide'
        };

        this.template = 'PDE:automationShareTemplate';

        this.templateStrings = {
            message: 'PDE_AUTOMATION_SHARE_MSG',
            cancel: 'PDE_AUTOMATION_SHARE_CANCEL'
        }
    }

    get id () {
        return 'script-share-dialog';
    }

    get selected () {
        let $select = this.$el.find('#script-share-dialog-select');
        if (!$select.length) {
            throw new Error('Could not locate #script-share-dialog-select');
        }
        return $select.val();
    }

    share () {
        this.controller.secondShareButtonClick();
    }

    show (scripts) {
        let $shareButton = this.$el.find('#script-share-dialog-share');

        $shareButton.attr('disabled', true);

        let $select = this.$el.find('select');
        $select.empty();
        scripts.forEach((name) => {
            $select.append(`<option>${name}</option>`);
        });
        $select.off('change');
        $select.on('change', () => {
            if ($select.val()) {
                $shareButton.attr('disabled', false);
            }
        });
        super.show();
    }
}
