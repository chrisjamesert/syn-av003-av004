import PDEAutomationView from './PDEAutomationView';

export default class PDEAutomationImportView extends PDEAutomationView {

    constructor (controller) {
        super(controller);

        this.events = {
            'click input#script-import-input-file': 'chooseClicked',
            'click button#script-import-file-load': 'importFile',
            'click button#script-import-cancel': 'hide'
        };

        this.template = 'PDE:automationImportTemplate';

        this.templateStrings = {
            message: 'PDE_AUTOMATION_IMPORT_MSG',
            importText: 'PDE_AUTOMATION_IMPORT',
            cancel: 'PDE_AUTOMATION_IMPORT_CANCEL'
        };
    }

    get id () {
        return 'script-import-dialog';
    }

    clear () {
        $('#script-import-input-file').val('');
    }

    chooseClicked () {
        this.clear();
    }

    importFile () {
        this.controller.importFile();
    }
}
