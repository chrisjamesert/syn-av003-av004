import PDEAutomationView from './PDEAutomationView';

export default class PDEAutomationPlayView extends PDEAutomationView {

    constructor (controller) {
        super(controller);

        this.events = {
            'click button#script-play-dialog-play': 'play',
            'click button#script-play-dialog-delete': 'deleteButtonClick',
            'click button#script-play-dialog-cancel': 'hide'
        };

        this.template = 'PDE:automationPlayTemplate';

        this.templateStrings = {
            message: 'PDE_AUTOMATION_PLAY_MSG',
            cancel: 'PDE_AUTOMATION_PLAY_CANCEL'
        }
    }

    get id () {
        return 'script-play-dialog';
    }

    get selected () {
        let $select = this.$el.find('#script-play-dialog-select');
        if (!$select.length) {
            throw new Error('Could not locate #script-play-dialog-select');
        }
        return $select.val();
    }

    play () {
        this.controller.secondPlayButtonClick();
    }

    deleteButtonClick () {
        this.controller.deleteButtonClick();
    }

    show (scripts) {
        let $playButton = this.$el.find('#script-play-dialog-play'),
            $deleteButton = this.$el.find('#script-play-dialog-delete');

        $playButton.attr('disabled', true);
        $deleteButton.attr('disabled', true);

        let $select = this.$el.find('select');
        $select.empty();
        scripts.forEach((name) => {
            $select.append(`<option>${name}</option>`);
        });
        $select.off('change');
        $select.on('change', () => {
            if ($select.val()) {
                $playButton.attr('disabled', false);
                $deleteButton.attr('disabled', false);
            }
        });
        super.show();
    }
}
