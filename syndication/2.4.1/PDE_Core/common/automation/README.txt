PDE automation tools
chris.james@ert.com

CURRENT FEATURES:

* Macro recording
* Macro playback
* Macro sharing
* Macro importing

USAGE:

The PDE Automation tool facilitates recording, playback and sharing of macros in Syndication Tablet and Handheld. Web
is not currently fully supported (sharing will not be possible on Web modality).

Via the settings view, toggle the automation controls on or off by tapping 'Toggle Automation'.

IMPORTANT: Recording and playback must start from the Login view.

Record a macro by pressing the record button (red circle). A flashing red circle on the controls indicates that recording
is active. Your interractions will be recorded as you use the app. When you are finished, press the stop button (blue
square). You will be prompted to name your macro. Enter a unique name and hit save, or hit cancel if you want to discard
the recording that you just made. The state that the app was in at the start of recording, and the steps recorded, are
saved in localStorage, and a JSON file is written to the app data folder. The JSON file can be shared, either via the
Share feature (see below), or by manually going to the storage location and grabbing the file yourself.

Playback a macro by pressing the play button (green arrow). A flashing green arrow on the controls indicates that
playback is active. Be careful not to manually interract with the app during playback as it is not locked to standard
input. If the playback sequence fails to locate its next target element then playback will hang, and can be stopped by
pressing the stop button (blue square). If playback is delayed due to, e.g. a 'Please wait...' spinner, playback should
resume when the next playback target is available.

Playback speed is slightly compressed by limiting the maximum interval between actions to 1 second. A future version
may add an option to disable and/or adjust playback compression.

Share - TODO

Import - TODO

Delete - TODO


// TODO: Password confirm screens also have the .row issue and they also have a single entry HR output.

TODO: Day numbering display and/or percentage complete playback

TODO: AutomationController is getting very large. Consider hiving off some of the functionality into other files.

TODO: Add X's to the upper right of the play/record butons when not on starting screen.

TODO: Add study/protocol checks.

TODO: Make the 'wait' on playback more robust - look for 'Please wait...' dialog for starters.

TODO: Pre-installed library scripts?

TODO: Broken playback alert - but ignore Please wait...

TODO: Very strange edge case where isValidDate(new Date('ABC 1')) === true

TODO: Analytics...








