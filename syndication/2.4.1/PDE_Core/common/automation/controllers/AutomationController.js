/* eslint-disable lines-around-comment */

// core imports
import TimeTravel from 'core/TimeTravel';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import CurrentContext from 'core/CurrentContext';
import { timeStamp } from 'core/DateTimeUtil';
import { MessageRepo } from 'core/Notify';
import { isSitePad, isLogPad } from 'core/utilities/coreUtilities';
import Logger from 'core/Logger';

// PDE_Core imports
import { allowPDEAutomationUtil } from 'PDE_Core/common/automation/utilities';
import PDEAutomationControlsView from 'PDE_Core/common/views/PDEAutomationControlsView';
import PDEAutomationSaveView from 'PDE_Core/common/views/PDEAutomationSaveView';
import PDEAutomationPlayView from 'PDE_Core/common/views/PDEAutomationPlayView';
import PDEAutomationImportView from 'PDE_Core/common/views/PDEAutomationImportView';
import PDEAutomationShareView from 'PDE_Core/common/views/PDEAutomationShareView';
import { dateDiffInDays, format, isValidDate } from 'PDE_Core/common/utilities/DateTimeUtils';

let logger = new Logger('AutomationController');

/**
 * @class AutomationController
 * @description Main Controller class for PDE Automation. An instance of AutomationController
 * forms the base of the PDE.Automation object.
 */
export default class AutomationController {
    constructor () {
        this.version = 16; // Version of the scriptEngine. Incremental integer.

        this.storagePrefix = '__script__';
        this.playbackStartDateKey = 'playback_start_date';
        this.delegateEventSplitter = /^(\S+)\s*(.*)$/;

        this.controlsView = new PDEAutomationControlsView(this);
        this.saveView = new PDEAutomationSaveView(this);
        this.playView = new PDEAutomationPlayView(this);
        this.importView = new PDEAutomationImportView(this);
        this.shareView = new PDEAutomationShareView(this);

        this.isInitialised = false;
    }

    /**
     * Query used when restricting the Automation tool to the
     * Dev, Staging and Trainer environments.
     * @returns {boolean}
     */
    get allowPDEAutomation () {
        return allowPDEAutomationUtil();
    }

    init () {
        this.isInitialised = true;
        return this.controlsView.render()
        .then(() => {
            return this.saveView.render();
        }).then(() => {
            return this.playView.render();
        }).then(() => {
            return this.importView.render();
        }).then(() => {
            if (LF.Wrapper.isWrapped) {
                return this.shareView.render();
            }
            return Q();
        }).then(() => {
            let shouldBeVisible = localStorage.getItem('PDE_Automation_is_shown');
            if (!!shouldBeVisible !== this.controlsView.isShown()) {
                this.toggleAutomationControls();
            }
        }).then(() => {
            this.hookIntoBackboneDelegateEvents();
        }).then(() => {
            this.createDoloresTargets();
        });
    }

    /**
     * Creates a set of target elements that can be used by DOLORES or other
     * methods of DOM-level interaction. These elements are standard, non-Backbone
     * elements. Most of the targets are triggered via a 'click' event on them
     * to invoke the associated automation functionality.
     * The exception to this is the 'save' target, which needs the .val('SaveName').change() sequence.
     *
     * Usage:
     * First, it is recommended to show the controls (although recording should work with them hidden):
     *     $('#REPROCORDER_SHOW_CONTROLS').click()
     * To start recording:
     *     $('#REPROCORDER_START_RECORDING').click()
     * To stop recording:
     *     $('#REPROCORDER_STOP_RECORDING').click()
     * To discard your recording (e.g. no defect found):
     *     $('#REPROCORDER_DISCARD_RECORDING').click()
     * To save your recording:
     *     $('#REPROCORDER_SAVE_RECORDING').val('MyRecordingName').change()
     * Finally, hide the controls:
     *     $('#REPROCORDER_HIDE_CONTROLS').click()
     *
     * In order to save or discard a recording you must first stop the recording.
     * If you try to discard or save whilst recording is in progress an error will be shown in the console
     * and nothing more will happen.
     */
    createDoloresTargets () {
        if ($('#REPROCORDER_SHOW_CONTROLS').length) {
            logger.error('DOLORES targets already exist.');
            return;
        }

        let $showControls = $('<div id="REPROCORDER_SHOW_CONTROLS"></div>'),
            $hideControls = $('<div id="REPROCORDER_HIDE_CONTROLS"></div>'),
            $start = $('<div id="REPROCORDER_START_RECORDING"></div>'),
            $stop = $('<div id="REPROCORDER_STOP_RECORDING"></div>'),
            $discard = $('<div id="REPROCORDER_DISCARD_RECORDING"></div>'),
            $save = $('<input id="REPROCORDER_SAVE_RECORDING"></input>'),
            $body = $('body');

        $showControls.hide();
        $hideControls.hide();
        $start.hide();
        $stop.hide();
        $discard.hide();
        $save.hide();

        $body.append($showControls);
        $body.append($hideControls);
        $body.append($start);
        $body.append($stop);
        $body.append($discard);
        $body.append($save);

        // Show controls
        $showControls.click(() => {
            if (!this.controlsView.isShown()) {
                this.toggleAutomationControls();
            }
        });
        // Hide controls
        $hideControls.click(() => {
            if (this.controlsView.isShown()) {
                this.toggleAutomationControls();
            }
        });
        // Start recording
        $start.click(() => {
            if (this.recordingScript) {
                logger.error('Script recording is already started.');
                return;
            }
            this.startRecording();
        });
        // Stop recording
        $stop.click(() => {
            if (!this.recordingScript) {
                logger.error('There is no recording in progress.');
                return;
            }
            this.stopRecording(true);
        });
        // Discard recording
        $discard.click(() => {
            if (this.recordingScript) {
                logger.error('Script recording still in progress. Must stop recording before discarding.');
                return;
            }
            this.clearRecording();
        });
        // Save recording
        $save.change(() => {
            if (this.recordingScript) {
                logger.error('Script recording still in progress. Must stop recording before saving.');
                return;
            }
            if (this.script.length > 1) {
                this.save($save.val());
            }
        });
    }

    /**
     * Hooks into Backbone.View.prototype.delegateEvents() which is
     * a function that takes Backbone View events definitions and delegates them.
     * Hooking into this function allows us to observe all events that are relevant to Syndication UI,
     * assuming that the developer has implemented the Backbone View design pattern.
     */
    hookIntoBackboneDelegateEvents () {
        if (!Backbone.View.prototype.hasOwnProperty('_pdeAutomation_delegateEvents')) {
            Backbone.View.prototype._pdeAutomation_delegateEvents = Backbone.View.prototype.delegateEvents;
            Backbone.View.prototype.delegateEvents = function (events) {
                if (PDE.Automation.allowPDEAutomation) {
                    PDE.Automation.monitorEvents(this, events);
                }
                return this._pdeAutomation_delegateEvents(events);
            };
        }
    }

    /**
     * This function is a copycat parasite on the Backbone.delegateEvents hook.
     * @param context
     * @param events
     * @returns {*}
     */
    monitorEvents (context, events) {
        events || (events = _.result(context, 'events'));
        if (!events) return context;
        context._selector = PDE.Automation.getContextSelector(context);
        PDE.Automation.stopMonitoringEvents(context);
        for (let key in events) {
            let method = events[key];
            if (!_.isFunction(method)) method = context[method];
            if (!method) continue;
            let match = key.match(PDE.Automation.delegateEventSplitter);
            PDE.Automation.monitor(context, match[1], match[2], method.name);
        }
        return context;
    }

    /**
     * Applies formatting for context selector.
     * @param context
     * @returns {string}
     */
    getContextSelector (context) {
        let localName = context.el.localName ? context.el.localName : '',
            id = context.el.id ? '#' + context.el.id : '';
        return localName + id;
    }

    /**
     * Builds the command objects that are added to the script array.
     * When called, it captures the delay since the previous action and assigns this to the 'wait' key.
     * @param event Usually the same as the incoming event type, but sometimes is given a custom value.
     * @param selector CSS selector for playback to target.
     * @param data Data for playback to apply to the selector target.
     * @returns {{wait: *, event: *, selector: *, data: *}}
     */
    cmdBuilder (event, selector, data = null) {
        return {
            wait: PDE.Automation.recordingGetWait(),
            event,
            selector,
            data
        };
    }

    /**
     * Calculates and returns the time elapsed since the last action during Recording.
     * @returns {number} Time elapsed, in ms.
     */
    recordingGetWait () {
        let now = Date.now(),
            wait = now - this.lastTime;
        this.lastTime = now;
        return wait;
    }

    /**
     * This is the main workhorse of the Record feature.
     * Filters Backbone events and determines how they are recorded.
     * @param context
     * @param eventName
     * @param selector
     * @param listener
     * @returns {*}
     */
    monitor (context, eventName, selector, listener) {
        context.$el.on(eventName + '.monitorEvents' + context.cid, selector, function (e) {

            if (!PDE.Automation.recordingScript) {
                return;
            }

            let target = e.target.id ? `#${e.target.id}` : selector,
                that = PDE.Automation,
                cmd, // Script output
                sentence, // Human-readable output
                textWasEntered = false,
                keypressDetected = false,
                screen;

            target = target.replace(':', '\\:'); // Support colons in ids

            screen = that.getCurrentDiaryScreen(target);

            switch (e.type) {
                case 'click':
                case 'focus':
                case 'focusin':
                    if (['input[data-role="datebox"]', 'input[type=tel], input[type=text]']
                            .includes(selector)) {
                        // Nothing to do here - data is handled by the change or input events
                        // If in the future we need to support dialog interaction, this is where it will get recorded.
                    } else if (selector === 'svg path') {
                        cmd = that.cmdBuilder(e.type, `path${target}`);
                        sentence = that.getSelectSentenceFromEvent(e, screen);
                    } else if (selector === '#sync-button') {
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`);
                        sentence = 'Press the \'Sync\' button.';
                    } else if (selector === '#transmit') {
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`);
                        sentence = 'Press the \'Transmit\' button.';
                    } else if (selector === '#application-toolbox') {
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`);
                        sentence = 'Open the settings menu.';
                    } else if (e.currentTarget.localName === 'tr' && isSitePad()) {
                        const viewClassName = LF.router.view().constructor.name;
                        if (viewClassName === 'VisitGatewayView') {
                            // Tablet Visit Gateway visit row click. These little blighters don't have ids...!
                            // Give it the target #visit-gateway to enable the wait feature
                            target = '#visit-gateway';
                        } else if (viewClassName === 'FormGatewayView') {
                            // Tablet Form Gateway form row click. Similar to visits, no ids...
                            target = '#begin-visit';
                        } else {
                            logger.error(`Unimplemented tr select for view ${viewClassName}`);
                            target = '#application'; // Hope the playback delay is sufficient until we fix any overlooked views.
                        }
                        cmd = that.cmdBuilder('gatewayRowSelect', target, e.currentTarget.rowIndex);
                        sentence = that.getGatewaySelectSentenceFromEvent(e, target);
                    } else if (isSitePad() && LF.router.view().constructor.name === 'VisitGatewayView' &&
                        e.type === 'click' && selector === '#back') {
                        // Show a warning. It is a bad idea for anyone to record the process of going back
                        // to the Home View on Tablet because it implies that they want to select a patient row
                        // which cannot be guaranteed to exist on the playback device etc.
                        // Use a standard alert so as not to interfere with the recording.
                        const warning = '!!! WARNING - RECORDING PATIENT SELECTION IS LIKELY TO BREAK PLAYBACK !!! Only continue if you can ensure that your list has the same count and status of patients. Patient numbers may be different.';
                        alert(warning);
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`);
                        sentence = `${that.getSelectSentenceFromEvent(e, screen)} - ${warning}`;
                    } else {
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`);
                        sentence = that.getSelectSentenceFromEvent(e, screen);
                    }
                    break;
                case 'mousedown':
                case 'mousemove':
                case 'mouseup':
                    cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, {
                        clientX: e.originalEvent.clientX,
                        clientY: e.originalEvent.clientY
                    });
                    sentence = that.getSelectSentenceFromEvent(e, screen);
                    break;
                case 'touchstart':
                case 'touchmove':
                case 'touchend':
                    let touch = e.originalEvent.changedTouches[0];
                    cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, {
                        clientX: touch.clientX,
                        clientY: touch.clientY
                    });
                    if (target === '#unitCursorCanvas') {
                        // Horrible hack to deal with the delay in updating the UI in VAS scales.
                        setTimeout(() => {
                            let localVal = $('#unitCursorCanvas').siblings('[data-selectedvalue]').attr('data-selectedvalue') || '[ADD VALUE HERE]',
                                localSentence = that.getSelectSentenceFromValue(localVal, screen);
                            logger.info(localSentence);
                            that.hrOutput.push(localSentence);
                        }, 100);
                    } else if (target === '#mainVasCanvas') {
                        // Horrible hack to deal with the delay in updating the UI in VAS scales.
                        setTimeout(() => {
                            let localVal = $('#answerDIV').text().trim() || '[ADD VALUE HERE]',
                                localSentence = that.getSelectSentenceFromValue(localVal, screen);
                            logger.info(localSentence);
                            that.hrOutput.push(localSentence);
                        }, 100);
                    } else if (e.currentTarget.className.split(' ').includes('vvas')) {
                        // Horrible hack to deal with the delay in updating the UI in VAS scales.
                        setTimeout(() => {
                            let localVal = $('.vvas').attr('data-selectedvalue') || '[ADD VALUE HERE]',
                                localSentence = that.getSelectSentenceFromValue(localVal, screen);
                            logger.info(localSentence);
                            that.hrOutput.push(localSentence);
                        }, 100);
                    } else {
                        sentence = that.getSelectSentenceFromEvent(e, screen);
                    }
                    break;
                case 'change':
                case 'input':
                    if (['input[type=radio]',
                            'input[type=checkbox]'].includes(selector)) {
                        // To simulate this event, we have to click the div with class btn-id
                        let tgt = `div.btn-${e.target.id}`;
                        cmd = that.cmdBuilder('click', tgt);
                        screen = that.getCurrentDiaryScreen(tgt);
                        if (LF.router.view() instanceof BaseQuestionnaireView &&  $(`#${e.target.id}`).closest('.row').length > 0) {
                            // This is a matrix question
                            sentence = that.getMatrixSelectSentenceFromValue($(tgt).text(), e.target.id, screen);
                        } else {
                            sentence = that.getSelectSentenceFromValue($(tgt).text(), screen);
                        }
                    } else if (selector === '.signature-box') {
                        // To simulate the signatures:
                        // $('.signature-box').jSignature('setData', data, 'native');
                        let data = $('.signature-box').jSignature('getData', 'native');
                        if (data && data.length) {
                            cmd = that.cmdBuilder(e.type, '.signature-box', data);
                            const signSentence = 'Sign the Affidavit';
                            if (_.last(that.hrOutput) !== signSentence) {
                                sentence = signSentence;
                            }
                        }
                    } else if (selector === '#user' && e.currentTarget.localName === 'select') {
                        // Special case for user selects
                        // The .val() of the select is the user id. We will use this to confirm the role of the user.
                        let userId = $(target).val(),
                            user = LF.router.view().users.findWhere({ id: parseInt(userId, 10) }),
                            role = user.get('role');
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, role);
                        // Must ensure the playback will see this
                        if (cmd.selector !== 'div#login-view #user') {
                            logger.error(`Expected cmd.selector of 'div#login-view #user' but found ${cmd.selector}. Rectify code to prevent error in playback.`);
                        }
                        let arrUsers = e.currentTarget.nextElementSibling.nextElementSibling.innerText.split('\n'),
                            userName = e.currentTarget.nextElementSibling.innerText.trim(),
                            userRole = arrUsers[arrUsers.indexOf(userName) + 1];
                        sentence = `Select user '${userName}' (${userRole})`;
                    } else if (e.currentTarget.localName === 'select') {
                        let selectVal = $(target).val();
                        if (selectVal) {
                            cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, selectVal);
                            let option = _.find(e.currentTarget.options, (opt) => {
                                return opt.value === selectVal;
                            });
                            sentence = that.getSelectSentenceFromValue(option.text.trim(), screen);
                        }
                    } else if (selector === 'input[data-role="datebox"]' ||
                        (isNaN($(target).val()) && isValidDate(new Date($(target).val())) &&
                            dateDiffInDays(new Date(), new Date($(target).val()), false) < 1000)) {
                            // The dateDiffInDays(...) < 1000 part is a very annoying and flawed
                            // hack we have to use to prevent things like isValidDate(new Date('Took 1')) === true.
                            // A better solution would be to get ALL Core date inputs to have an identifiable selector... ho hum.
                            // It is flawed because things like 'Blah 1 2 25' will slip through because 25 is the year part.
                            // But that will restrict it to a far more edge case scenario.
                        // DateSpinner does not have any helpful tags, unlike DatePicker, so we need to || with isNaN && isValidDate
                        if (e.target.classList.contains('date-input') || isValidDate(new Date($(target).val()))) {
                            // It is a date input
                            let dateVal = $(target).val();
                            cmd = that.cmdBuilder(`${e.type}.date-input`, `${context._selector} ${target}`, dateVal);
                            sentence = that.getRelativeDateSentence(dateVal, screen)
                        } else if (e.target.classList.contains('time-input')) {
                            // It is a time input
                            let timeVal = $(target).val();
                            cmd = that.cmdBuilder(`${e.type}.time-input`, `${context._selector} ${target}`, timeVal);
                            sentence = that.getTimeSentence(timeVal, screen);
                        } else {
                            let msg = `Automation found an unrecognized datebox class that it does not yet know how to format: ${e.target.className}`;
                            alert(msg);
                            logger.error(msg);
                            let someVal = $(target).val();
                            cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, someVal);
                            sentence = that.getEnterSentenceFromValue(someVal, screen);
                        }
                    } else {
                        // To simulate a change: $('...').val(...).change()
                        // To simulate an input: Set value with $('...').val(...) then call $('...').trigger('input')
                        let aVal = $(target).val();
                        cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, aVal);
                        if (aVal) {
                            textWasEntered = true;
                            if (LF.router.view() instanceof BaseQuestionnaireView && $(`#${e.target.id}`).closest('.row').length > 0) {
                                that.lastTextEnteredHROutput = that.getMatrixEnterSentenceFromValue(aVal, e.target.id, screen);
                            } else {
                                that.lastTextEnteredHROutput = that.getEnterSentenceFromValue(aVal, screen);
                            }
                        }
                    }
                    break;
                case 'keypress':
                    cmd = that.cmdBuilder(e.type, `${context._selector} ${target}`, e.key);
                    keypressDetected = true;
                    if (!that.lastKeypressSequence) {
                        that.lastKeypressSequence = '';
                    }
                    that.lastKeypressSequence += e.key;
                    break;
                case 'hidden':
                case 'remove':
                    // Ignore these
                    break;
                default:
                    logger.error(`>>> UNIMPLEMENTED EVENT: ${context._selector} selector: ${selector} target: ${target} type: ${e.type}`);
            }
            if (cmd) {
                that.script.push(cmd);
                //logger.info(cmd); // TODO: UNCOMMENT IF YOU WANT TO SEE THE COMMAND AS IT IS RECORDED.
            }

            if (!textWasEntered && !keypressDetected && that.lastTextEnteredHROutput) {
                that.addHRTextInput(that);
            } else if (!textWasEntered && !keypressDetected && that.lastKeypressSequence) {
                that.addHRKeyPressSequence(that, screen);
            }

            if (sentence) {
                that.hrOutput.push(sentence);
                logger.info(sentence);
            }
        });
        return context;
    }

    /**
     * Tries to return the name of the screen if we are in a diary.
     * @param target CSS selector
     * @returns {string|null}
     */
    getCurrentDiaryScreen (target) {
        let screenSection = $(target).closest('section.screen');
        if (screenSection.length && screenSection[0].className.split(' ').length > 1) {
            return screenSection[0].className.split(' ')[1];
        }
        return null;
    }

    /**
     * Primary HR text output handler.
     * Dominant to addHRKeyPressSequence which is always called second.
     * Some views only listen for keypress events and not change events.
     * @param controller
     */
    addHRTextInput(controller) {
        // Text entry is complete. Capture the final sentence:
        controller.hrOutput.push(controller.lastTextEnteredHROutput);
        logger.info(controller.lastTextEnteredHROutput);
        controller.lastTextEnteredHROutput = null;
        controller.lastKeypressSequence = null;
    }

    /**
     * Secondary HR text output handler.
     * Subservient to addHrTextInput which is always called first.
     * Some views only listen for keypress events and not change events.
     * @param controller
     * @param screen
     */
    addHRKeyPressSequence(controller, screen) {
        // Keypress sequence is complete. Capture the final input:
        let sequenceSentence = controller.getEnterSentenceFromValue(controller.lastKeypressSequence, screen);
        controller.hrOutput.push(sequenceSentence);
        logger.info(sequenceSentence);
        controller.lastKeypressSequence = null;
    }

    /**
     * For human-readable output where it is appropriate to say "Select 'something'"
     * if giving a command for someone to reproduce the event.
     * @param e
     * @param screen
     * @returns {string}
     */
    getSelectSentenceFromEvent(e, screen) {
        let prefix = `Select`;
        if (screen) {
            prefix = `On screen ${screen}, select`;
        }
        return `${prefix} '${e.currentTarget.innerText ? e.currentTarget.innerText.trim() : e.currentTarget.id}'`;
    }

    /**
     * If you know the value selected and just want to say 'select XYZ'
     * and maybe 'on screen ABC' then use this.
     * @param value
     * @param screen
     * @returns {string}
     */
    getSelectSentenceFromValue(value, screen) {
        if (value === null || value === undefined) {
            value = '[ADD VALUE HERE]';
        }
        if (screen) {
            return `On screen ${screen}, select '${value}'`;
        }
        return `Select '${value}'`;
    }

    /**
     * Get the human-readable output when we are dealing with a matrix screen.
     * @param value
     * @param targetId
     * @param screen
     */
    getMatrixSelectSentenceFromValue(value, targetId, screen) {
        if (value === null || value === undefined) {
            value = '[ADD VALUE HERE]';
        }
        screen = screen || '???';
        let column = this.getMatrixColumn(targetId),
            columnString = column !== -1 ? ` column ${column}` : '';
        return `On screen ${screen}, row ${this.getMatrixRowNumber(targetId)}${columnString}, select '${value}'`;
    }

    /**
     * Get the row number of the targetId on a Matrix widget.
     * Ignores header rows. Returns -1 if it cannot find any rows.
     * Otherwise returns a 1-based row number.
     * @param targetId
     * @returns {number}
     */
    getMatrixRowNumber(targetId) {
        let $rows = $('.row'),
            hasRowHeader = $('.row.header').length,
            $myRow = $(`#${targetId}`).closest('.row'),
            myRowIndex = $rows.index($myRow),
            myRowNumber = hasRowHeader ? myRowIndex : myRowIndex + 1;
        if ($rows.length === 0 || $myRow.length === 0 || myRowIndex === -1) {
            return -1;
        } else {
            return myRowNumber;
        }
    }

    /**
     * Get the column number for Matrix widgets.
     * @param targetId
     * @returns number -1 if no column found, otherwise the column number.
     */
    getMatrixColumn(targetId) {
        let $target = $(`#${targetId}`),
            $row = $target.closest('.row'),
            $cols = $row.find('[class^=col-]'),
            myColIndex = $cols.index($target.closest('[class^=col-]'));
        if (myColIndex !== -1) {
            return myColIndex + 1;
        } else {
            return -1;
        }
    }

    /**
     * Use this for human-readable output when you want to say
     * something like "On screen ABC, enter '2'".
     * @param value
     * @param screen
     * @returns {string}
     */
    getEnterSentenceFromValue(value, screen) {
        if (value === null || value === undefined) {
            value = '[ADD VALUE HERE]';
        }
        if (screen) {
            return `On screen ${screen}, enter '${value}'`;
        }
        return `Enter '${value}'`;
    }

    /**
     * Use this when you have identified a Matrix widget and want some human-readable output
     * of the form "On screen ABC, row 1 column 2, enter 'I can jump'".
     * @param value
     * @param targetId
     * @param screen
     * @returns {string}
     */
    getMatrixEnterSentenceFromValue(value, targetId, screen) {
        if (value === null || value === undefined) {
            value = '[ADD VALUE HERE]';
        }
        screen = screen || '???';
        let column = this.getMatrixColumn(targetId),
            columnString = column !== -1 ? ` column ${column}` : '';
        return `On screen ${screen}, row ${this.getMatrixRowNumber(targetId)}${columnString}, enter '${value}'`;
    }

    /**
     * When on the Visit Gatweay, returns the column index for the Visit Name.
     * @returns {number}
     */
    getVisitColumnIndex () {
        let $visitHeaderItem = $('#th-VISIT_NAME');
        if ($visitHeaderItem.length) {
            return $visitHeaderItem.index();
        }
        return -1;
    }

    /**
     * When on the Form Gateway, returns the column index for the Form Name.
     * @returns {number}
     */
    getFormColumnIndex () {
        let $formHeaderItem = $('#th-FORM_NAME');
        if ($formHeaderItem.length) {
            return $formHeaderItem.index();
        }
        return -1;
    }

    /**
     * For use with Tablet Gateway views such as VisitGatewayView
     * and FormGatewayView. Provides human-readable output that
     * makes sense for these views.
     * @param e
     * @param target
     */
    getGatewaySelectSentenceFromEvent(e, target) {
        // Get the text from the first cell:
        let targetColumnIndex = -1,
            targetText,
            action = e.currentTarget.className === 'bg-info' ? 'deselect' : 'select',
            targetDescription;
        switch (target) {
            case '#visit-gateway':
                targetDescription = `Visit Gateway view, ${action} Visit`;
                targetColumnIndex = this.getVisitColumnIndex();
                break;
            case '#begin-visit':
                targetDescription = `Form Gateway view, ${action} Form`;
                targetColumnIndex = this.getFormColumnIndex();
                break;
            default:
                targetDescription = `table view, ${action} item`;
                break;
        }
        if (targetColumnIndex !== -1) {
            let $container = $('<div></div>');
            $container.append(e.currentTarget.innerHTML);
            targetText = $container.children()[targetColumnIndex].innerText;
        } else {
            targetText = e.currentTarget.innerText; // Just spit out the whole row if we don't know what to do.
        }
        return `On the ${targetDescription} '${targetText}'`;
    }


    /**
     * For human-readable output where it is appropriate to say "Select Activation Date + n days"
     * if giving a command for someone to reproduce the date selection event.
     * @param dateVal
     * @param screen
     * @returns {string}
     */
    getRelativeDateSentence(dateVal, screen) {
        let startDate = new Date(this.script[0].startDate),
            newDate = new Date(dateVal),
            diff = dateDiffInDays(newDate, startDate, false),
            direction = diff >= 0 ? 'plus' : 'minus',
            prefix = 'Enter';
        diff = Math.abs(diff);
        if (screen) {
            prefix = `On screen ${screen}, enter`;
        }
        return `${prefix} the date as Activation ${direction} ${diff} ${diff === 1 ? 'day' : 'days'}`;
    }

    /**
     * For human-readable output of Time Travel events.
     * e.g. "Time Travel to Activation plus 1 day at 14:00."
     * @param dateVal
     * @returns {string}
     */
    getRelativeTimeTravelSentence(dateVal) {
        let startDate = new Date(this.script[0].startDate),
            newDate = new Date(dateVal),
            diff = dateDiffInDays(newDate, startDate, false),
            direction = diff >= 0 ? 'plus' : 'minus',
            time = format(newDate, 'timeOutput');
        diff = Math.abs(diff);
        return `Time Travel to Activation ${direction} ${diff} ${diff === 1 ? 'day' : 'days'} at ${time}.`;
    }

    /**
     * For human-readable output of Time inputs.
     * @param value
     * @param screen
     * @returns {string}
     */
    getTimeSentence(value, screen) {
        if (value === null || value === undefined) {
            value = '[ADD VALUE HERE]';
        }
        if (screen) {
            return `On screen ${screen}, enter the time as '${value}'`;
        }
        return `Enter the time as '${value}'`;
    }

    /**
     * When a new Backbone View is loaded, this method is called
     * to turn off any ongoing monitoring of old Views' events.
     * @param context
     * @returns {*}
     */
    stopMonitoringEvents (context) {
        if (context.$el) context.$el.off('.monitorEvents' + context.cid);
        return context;
    };

    /**
     * Toggles the visibility of the Automation controls.
     */
    toggleAutomationControls () {
        if (this.recordingScript || this.playingScript) {
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_CANNOT_TOGGLE);
            return;
        }
        this.controlsView.toggleVisibility();
    }

    /**
     * Called when the Play button on the Automation Controls View is pressed.
     * It also handles the 'Stop Playback' functionality, because the Play button
     * doubles as the Stop Playback button.
     */
    firstPlayButtonClick () {
        if (!this.playingScript) {
            if (!this.isOnStartingView()) {
                return;
            }
            this.showPlayDialog();
        } else {
            this.stopPlayback();
        }
    }

    /**
     * Called when the Play Dialog's Play Button is pressed.
     */
    secondPlayButtonClick () {
        let scriptToPlay = this.playView.selected;
        if (!scriptToPlay) {
            return;
        }
        this.hidePlayDialog();
        localStorage.setItem(this.playbackStartDateKey, timeStamp(new Date()));
        this.startPlayback(scriptToPlay);
    };

    /**
     * Shows the Play Dialog, passing in the scripts to populate the list.
     */
    showPlayDialog () {
        this.playView.show(this.getScripts());
    }

    /**
     * Hides the Play Dialog.
     * Calls refreshControls because the state may have changed
     * (Playback may have commenced, or a script may have been deleted.)
     */
    hidePlayDialog () {
        this.playView.hide();
        this.controlsView.refreshControls();
    }

    /**
     * Called when the Play Dialog's Delete button is pressed.
     */
    deleteButtonClick () {
        let $select = $('#script-play-dialog-select');
        if (!$select.length) {
            throw new Error('Could not locate #script-play-dialog-select');
        }
        let scriptToDelete = $select.val();
        if (!scriptToDelete) {
            return;
        }

        MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_CONFIRM_DELETE)
        .then(() => {
            localStorage.removeItem(`${this.storagePrefix}${scriptToDelete}`);

            let deleteFileCallback = (success) => {
                if (success) {
                    this.hidePlayDialog();
                } else {
                    logger.error('Failed to delete file.');
                }
            };
            if (LF.Wrapper.isWrapped) {
                this.deleteScriptFileFromAppStorage(scriptToDelete, deleteFileCallback);
            } else {
                this.hidePlayDialog();
            }
        }).catch(() => {
            // DO NOTHING
        }).done();
    }

    /**
     * Stops Playback and refreshes the Automation Controls.
     */
    stopPlayback () {
        this.playingScript = false;
        this.controlsView.refreshControls();
    }

    /**
     * Groups the generation of meta-data into one helper method.
     * Meta-data is added as the first item in this.script at the
     * start of Recording. Meta-data is used at Playback to check
     * if there are going to be any potential problems, e.g. different
     * app versions.
     */
    generateMetaData () {
        // Get the native date for debugging purposes
        let phtTimeTraveled = localStorage.getItem('PHT_Time_Traveled'),
            nativeDate = new Date().getTime();
        if (phtTimeTraveled) {
            nativeDate -= parseInt(phtTimeTraveled, 10);
        }

        // Add metadata to the first index
        this.script.push({
            nativeDate,
            startDate: new Date().getTime(),
            studyVersion: LF.StudyDesign.studyVersion,
            modality: LF.appName,
            autoVersion: this.version,
            browser: this.getBrowser(),
            roleStart: CurrentContext().role
        });
        this.validateMetaData();
    }

    /**
     * Checks that the meta-data object contains the expected keys.
     * References the autoVersion - PDE.Automation version number,
     * for backwards compatability.
     * @returns {boolean}
     */
    validateMetaData () {
        const metaMissingError = 'Expected first index to contain metadata. Missing';
        if (!this.script[0].autoVersion) {
            alert('Error: Missing autoVersion');
            logger.error(`${metaMissingError} autoVersion`);
            return false;
        }
        let recVersion = this.script[0].autoVersion;
        if (!this.script[0].nativeDate && recVersion >= 8) {
            alert('Error: Missing nativeDate');
            logger.error(`${metaMissingError} nativeDate`);
        } else if (!this.script[0].startDate && recVersion >= 8) {
            alert('Error: Missing startDate');
            logger.error(`${metaMissingError} startDate`);
        } else if (!this.script[0].studyVersion && recVersion >= 8) {
            alert('Error: Missing studyVersion');
            logger.error(`${metaMissingError} studyVersion`);
        } else if (!this.script[0].modality && recVersion >= 8) {
            alert('Error: Missing modality');
            logger.error(`${metaMissingError} modality`);
        } else if (!this.script[0].browser && recVersion >= 9) {
            alert('Error: Missing browser');
            logger.error(`${metaMissingError} browser`);
        } else if (!this.script[0].roleStart && recVersion >= 14) {
            alert('Error: Missing roleStart');
            logger.error(`${metaMissingError} roleStart`);
        } else {
            return true;
        }
        return false;
    }

    /**
     * Get the browser being used.
     * @returns {{ name: string, version: string }}
     */
    getBrowser() {
        let ua = navigator.userAgent,
            tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return {name: 'IE', version: (tem[1] || '')};
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR|Edge\/(\d+)/);
            if (tem != null) {
                return {name: 'Opera', version: tem[1]};
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1,1,tem[1]);
        }
        return {
            name: M[0],
            version: M[1]
        };
    }

    /**
     * Starts Playback of the script with the given name.
     * @param scriptName
     */
    startPlayback (scriptName) {
        let scriptKey = this.storagePrefix + scriptName,
            script = localStorage.getItem(scriptKey);

        logger.info(`Script '${scriptName}' playback started`);
        if (!script) {
            throw new Error(`Could not find script ${scriptName}`);
        }
        this.script = JSON.parse(script);
        if (!this.script.length ||
            (this.script.length === 1 && this.script[0].nativeDate)) {
            // If we find nativeDate it means we are looking at the metadata
            logger.error('Trying to play an empty script');
            return;
        }

        if (!this.validateMetaData()) {
            return;
        }

        let commence = (that) => {
            that.scriptIndex = 1; // Skip metadata
            that.playingScript = true;
            that.controlsView.refreshControls();
            that.playback();
            that.controlsView.flashIndicator();
        };

        if (this.script[0].studyVersion !== LF.StudyDesign.studyVersion) {
            // Soft stop
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_STUDY_VERSION_DIFF)
            .then(() => { commence(this) }).catch(() => { /* do nothing */ }).done();
        } else if (this.script[0].autoVersion >= 9 && this.script[0].browser.name !== this.getBrowser().name) {
            // Soft stop
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_BROWSER_NAME_DIFF)
            .then(() => { commence(this) }).catch(() => { /* do nothing */ }).done();
        } else if (this.script[0].autoVersion >= 9 && this.script[0].browser.version !== this.getBrowser().version) {
            // Soft stop
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_BROWSER_VERSION_DIFF)
            .then(() => { commence(this) }).catch(() => { /* do nothing */ }).done();
        } else if (this.script[0].modality !== LF.appName) {
            // Hard stop
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_MODALITY_DIFF);
        } else if (this.script[0].autoVersion >= 14 && this.script[0].roleStart !== CurrentContext().role) {
            // Hard stop
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_ROLE_DIFF);
        } else {
            // No issues found
            commence(this);
        }
    }

    /**
     * Timer-triggered method, the workhorse of the Playback feature.
     */
    playback () {
        let that = PDE.Automation,
            cmd = that.script[that.scriptIndex],
            $target = $(cmd.selector);

        if (that.spinnerIsShown) {
            setTimeout(that.playback, 1000);
            that.controlsView.setInfoText('Waiting for spinner...');
            logger.info('Waiting for Spinner process to finish...');
            return;
        } else if (cmd.event !== 'TimeTravel' && !$target.length && that.playingScript) {
            // Wait for screen to load or processing to finish, etc.
            setTimeout(that.playback, 1000);
            that.controlsView.setInfoText(`Waiting for '${cmd.selector}' ...`);
            logger.info(`Waiting for target '${cmd.selector}' to load...`);
            return;
        } else if (cmd.selector === 'div#account-configured #submit' &&
            $target.attr('disabled')) {
            // Tablet account configured screen has a delay on the Next button
            // becoming enabled after activation, approx 500 ms.
            setTimeout(that.playback, 1000);
            that.controlsView.setInfoText('Waiting for Next ...');
            logger.info('Waiting for Next button to become enabled...');
            return;
        }
        that.controlsView.setInfoText('');
        that.scriptIndex++;

        switch (cmd.event) {
            case 'click':
            case 'focus':
            case 'focusin':
                $target[cmd.event]();
                break;
            case 'mousedown':
            case 'mousemove':
            case 'mouseup':
                that.sendMouseEvent(cmd.data.clientX, cmd.data.clientY, $target[0], cmd.event);
                break;
            case 'touchstart':
            case 'touchmove':
            case 'touchend':
                that.sendTouchEvent(cmd.data.clientX, cmd.data.clientY, $target[0], cmd.event);
                break;
            case 'change':
                if (cmd.selector === '.signature-box') {
                    // This will be a change event, but we don't need to call change for signatures.
                    $target.jSignature('setData', cmd.data, 'native');
                } else if (cmd.selector === 'div#login-view #user' && that.script[0].autoVersion < 15) {
                    // Prior to V.15 the login cmd had data = userId, instead of role.
                    // So we have to try to achieve what was desired.
                    $target.val(cmd.data).change();
                } else if (cmd.selector === 'div#login-view #user' && that.script[0].autoVersion >= 15) {
                    // Find the first available user that matches the role.
                    // Then select it by using its id.
                    let user = LF.router.view().users.findWhere({ active: 1, role: cmd.data });
                    if (!user) {
                        // If we don't find a user, we must show an error dialog and stop playback.
                        alert(`Could not find a user with role '${cmd.data}'. Please register a ${cmd.data} user and start a fresh playback session.`)
                        that.stopPlayback();
                    }
                    let userId = user.get('id');
                    $target.val(userId).change();
                } else {
                    $target.val(cmd.data).change();
                }
                break;
            case 'input':
                $target.val(cmd.data).trigger('input');
                break;
            case 'change.date-input':
            case 'input.date-input':
                // Convert dates to relative
                let validationDate = new Date(cmd.data),
                    dateOutput = cmd.data;
                if (isValidDate(validationDate)) {
                    // Make relative and match formatting in THIS build (not format from recorder) so that we
                    // can use automation to validate defects in date/time formatting.
                    let date = that.getRelativeDate(cmd.data, that);
                    dateOutput = format(date, 'dateFormat');
                } else {
                    // Something went wrong - expected valid date. Fail by entering raw data
                    logger.error(`Invalid date was found: ${cmd.data}`);
                }
                cmd.event.indexOf('change') === 0 ? $target.val(dateOutput).change() : $target.val(dateOutput).trigger('input');
                break;
            case 'change.time-input':
            case 'input.time-input':
                // Assumes we only ever want to display HH:mm or hh:mm AM/PM type times, ignoring seconds and ms.
                let timeDate = new Date(),
                    hhmm = cmd.data.split(':'),
                    is24H = hhmm[1].length === 2,
                    meridiem = is24H ? null : hhmm[1].substr(2).trim(),
                    hours = hhmm[0],
                    minutes = hhmm[1].substr(0, 2);

                timeDate.setHours(Number(hours), Number(minutes), 0, 0);
                if (meridiem && meridiem.toLowerCase() === 'pm') {
                    timeDate.setHours(Number(hours) + 12);
                }

                let timeOutput = format(timeDate, 'timeOutput');
                cmd.event.indexOf('change') === 0 ? $target.val(timeOutput).change() : $target.val(timeOutput).trigger('input');
                break;
            case 'keypress':
                $target.keypress(cmd.data);
                break;
            case 'TimeTravel':
                if (!cmd.data.dateString) {
                    throw new Error('TimeTravel cmd.data.dateString not found');
                }
                let date = that.getRelativeDate(cmd.data.dateString, that);
                TimeTravel.setTimeTravelDate(date.getTime());
                break;
            case 'gatewayRowSelect':
                let $tableRows = $('table.table.scrollable').find('tr');
                if (!$tableRows.length || !$tableRows[cmd.data]) {
                    // Not ready yet...
                    // This is a special case where we need to delay playback
                    // but have a custom target ('gatewayRowSelect') that slipped through
                    // the delay sequence earlier in this function.
                    that.controlsView.setInfoText('Waiting for Gatweay to load ...');
                    that.scriptIndex--;
                    setTimeout(that.playback, 1000);
                    return;
                }
                $tableRows[cmd.data].click();
                break;
            default:
                logger.error(`>>> UNIMPLEMENTED EVENT: ${JSON.stringify(cmd)}`);
        }

        if (that.scriptIndex < that.script.length && that.playingScript) {
            that.scheduleNext(that.script[that.scriptIndex].wait);
        } else {
            logger.info('Script playback finished');
            that.stopPlayback();
        }
    }

    /**
     * Calculates and returns relative date.
     * TimeTravel objects contain the dateString literal date that was travelled to,
     * and the offset phtTimeTravelled in ms from nativeDate.
     * this.script[0].nativeDate contains the recorded native date getTime.
     * this.script[0].startDate contains the recorded start date (Date().getTime(), may be under time travel)
     * We need to travel to the same DAY relative to start date,
     * but to the same TIME as in the dateString literal.
     * @param dateString
     * @param automationController
     * @returns {Date}
     */
    getRelativeDate(dateString, automationController) {
        let playbackStartDate = localStorage.getItem(automationController.playbackStartDateKey);
        if (!playbackStartDate) {
            throw new Error(`Could not find playbackStartDate in ${automationController.playbackStartDateKey}`);
        }
        let date = new Date(dateString),
            recordedStartDate = new Date(automationController.script[0].startDate),
            recordedDiffInDays = dateDiffInDays(date, recordedStartDate, false),
            playbackDiffInDays = dateDiffInDays(date, new Date(playbackStartDate));

        if (recordedDiffInDays !== playbackDiffInDays) {
            // Need to adjust the date by the difference
            let recVsPlaybackDiff = recordedDiffInDays - playbackDiffInDays;
            date.setDate(date.getDate() + recVsPlaybackDiff);
        }
        return date;
    }

    /**
     * Triggers a mouse event in Playback.
     * @param x
     * @param y
     * @param element
     * @param eventType
     */
    sendMouseEvent (x, y, element, eventType) {
        const mouseEvent = new MouseEvent(eventType, {
            cancelable: true,
            bubbles: true,
            clientX: x,
            clientY: y
        });
        element.dispatchEvent(mouseEvent);
    }

    /**
     * Triggers a touch event in Playback.
     * @param x
     * @param y
     * @param element
     * @param eventType
     */
    sendTouchEvent (x, y, element, eventType) {
        const touchObj = new Touch({
            identifier: Date.now(),
            target: element,
            clientX: x,
            clientY: y,
            radiusX: 2.5,
            radiusY: 2.5,
            rotationAngle: 10,
            force: 0.5
        });

        const touchEvent = new TouchEvent(eventType, {
            cancelable: true,
            bubbles: true,
            touches: [touchObj],
            targetTouches: [],
            changedTouches: [touchObj],
            shiftKey: false
        });
        element.dispatchEvent(touchEvent);
    }

    /**
     * Timer method that re-triggers playback with reference to
     * the wait. This is where the playback time 'compression' is
     * applied, limiting wait to max 1s.
     * @param wait Time to wait, in ms, before next playback call.
     */
    scheduleNext (wait) {
        if (wait < 10) {
            wait = 10;
        } else if (wait > 1000) {
            wait = 1000;
        }
        setTimeout(PDE.Automation.playback, wait);
    }

    /**
     * Saves the recording to localStorage and, if on a device, to app data storage.
     * If triggered from the UI, domName will be null.
     * If triggered from the DOM targets, domName should be provided.
     * Finalizes the human-readable output.
     * @param domName
     */
    save (domName = null) {
        if (!this.script) {
            throw new Error('script is not defined!');
        } else if (!this.script.length) {
            throw new Error('script length is zero!');
        } else if (this.script.length === 1) {
            // Looks like just the metadata is trying to be saved.
            throw new Error('script length must be greater than 1!');
        }

        let scriptName = domName || this.saveView.input;

        if (!scriptName.length) {
            logger.error('Script save name was zero length.');
            return;
        }
        let scriptStorageItem = `${this.storagePrefix}${scriptName}`;

        // Saves from the DOM will always overwrite
        if (!domName && localStorage.getItem(scriptStorageItem)) {
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_SCRIPT_EXISTS);
            return;
        }

        localStorage.setItem(scriptStorageItem, JSON.stringify(this.script));

        let steps = '',
            counter = 1;

        if (LF.Wrapper.isWrapped) {
            this.hrOutput.forEach((step) => {
                steps += `${counter}) ${step}\\`; // The \\ is for Jira text input to parse a new line. JSON cannot contain new lines.
                counter++;
            });
            this.writeToFile(scriptName + '.json', {
                name: scriptName,
                script: this.script,
                steps
            });
        } else {
            this.hrOutput.forEach((step) => {
                steps += `${counter}) ${step}\n`;
                counter++;
            });
            logger.info(`Steps: \n\n${steps}`);
        }

        logger.info(`Script '${scriptName}' saved`);
        this.clearRecording();
    }

    /**
     * Discard a recording after recording has stopped.
     */
    discard () {
        MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_CONFIRM_DISCARD)
        .then(() => {
            this.clearRecording();
        }).catch(() => {
            // DO NOTHING
        }).done();
    }

    /**
     * Resets the recording, clearing all values.
     */
    clearRecording () {
        this.script = null;
        this.hrOutput = null;
        this.saveView.hide();
        this.controlsView.refreshControls();
    }

    /**
     * Checks that we are on the correct view for the start of recording/playback.
     * On Handheld this is the login view.
     * On Tablet this is the subject visits list view.
     * @returns {boolean}
     */
    isOnStartingView () {
        let result = false;
        if (isSitePad()) {
            // All TB recording/playback must start from the visit gateway.
            result = !!$('#visit-gateway').length;
        } else if (isLogPad()) {
            // All HH recording/playback must start from the login view.
            result = !!$('#login-view').length;
        } else {
            logger.error(`Modality undetermined: ${LF.appName}`);
            return false;
        }
        if (!result) {
            MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_NOT_ON_LOGIN);
        }
        return result;
    }

    /**
     * Starts Recording if we are on the starting view.
     */
    startRecording () {
        if (this.recordingScript) {
            this.stopRecording();
            return;
        }

        if (!this.isOnStartingView()) {
            return;
        }

        this.recordingScript = true;
        this.script = [];
        this.hrOutput = [];

        this.generateMetaData();

        this.lastTime = Date.now();
        this.controlsView.refreshControls();
        this.controlsView.flashIndicator();
        logger.info('Script recording started.');
    }

    /**
     * Stops the recording process.
     * When triggered from the UI, leave the default value for isDOMTriggered = false.
     * When triggered from the DOM (e.g. DOLORES) pass in true for isDOMTriggered to
     * prevent unnecessary UI-related behaviour.
     * If recording was stopped before anything had been recorded, clears the recording memory.
     * Finalizes the human-readable output for any text input that was ongoing at time of stopping.
     * @param isDOMTriggered
     */
    stopRecording (isDOMTriggered = false) {
        if (!this.recordingScript) {
            throw new Error('Call to stopRecording() when recordingScript was false!');
        }
        this.recordingScript = false;
        this.lastTime = null;

        // Finalizes the human-readable output for text input that was ongoing at time of stopping.
        if (this.lastTextEnteredHROutput) {
            this.addHRTextInput(this);
        } else if (this.lastKeypressSequence) {
            this.addHRKeyPressSequence(this);
        }

        this.controlsView.refreshControls();
        logger.info('Script recording stopped.');

        // Scripts start with 1 entry - the metadata, so check length > 1
        if (this.script.length > 1 && !isDOMTriggered) {
            this.showSaveDialog();
        } else if (this.script.length <= 1) {
            this.clearRecording();
        }
    }

    /**
     * Shows the Save Dialog.
     */
    showSaveDialog () {
        if (!this.script) {
            throw new Error('script is not defined');
        } else if (!this.script.length) {
            throw new Error('script length is zero!');
        }
        this.saveView.input = '';
        this.saveView.show();
    }

    /**
     * Shows the Import Dialog.
     */
    showImportView () {
        this.importView.clear();
        this.importView.show();
    }

    /**
     * Shows the Share Dialog.
     */
    showShareView () {
        this.shareView.show(this.getScripts());
    }

    /**
     * Called when the Share Dialog's Share button is pressed.
     */
    secondShareButtonClick () {

        let scriptName = this.shareView.selected;

        if (!scriptName) {
            return;
        }

        // Check for the existence of the file
        let filePath = `${cordova.file.externalDataDirectory}${scriptName}.json`,
            onShareSuccess = () => {
                this.shareView.hide();
                this.controlsView.refreshControls();
            },
            onShareError = (err) => {
                let msg = `Share error: ${err}`;
                alert(msg);
                logger.error(msg);
            },
            fileExists = () => {
                LF.getStrings(['PDE_AUTOMATION_SHARE_MESSAGE',
                'PDE_AUTOMATION_SHARE_SUBJECT'])
                .then((strings) => {
                    window.plugins.socialsharing.shareWithOptions({
                        message: strings[0],
                        subject: `${scriptName}.json`, // subject is the placeholder file name for Google Drive share
                        files: [filePath]
                    }, onShareSuccess, onShareError);
                });
            },
            fileNotFound = (err) => {
                let msg = `File error: ${err}`;
                alert(msg);
                logger.error(msg);
            };

        this.checkIfFileExists(filePath, fileExists, fileNotFound);
    }

    /**
     * checkIfFileExists
     * Check if a file exists.
     * @param filePath
     * @param onSuccess
     * @param onFail
     */
    checkIfFileExists (filePath, onSuccess, onFail) {
        window.resolveLocalFileSystemURL(filePath, onSuccess, onFail);
    }

    /**
     * Returns the number of scripts in localStorage.
     * @returns {number}
     */
    getScriptCount () {
        return this.getScripts().length;
    }

    /**
     * Gets a list of the script names from localStorage.
     * @returns {Array<string>}
     */
    getScripts () {
        let scripts = [];
        for (let key in localStorage) {
            if (key.indexOf(this.storagePrefix) === 0) {
                scripts.push(key.substring(this.storagePrefix.length));
            }
        }
        return scripts;
    }

    /**
     * Called when the Import Dialog's Import button is pressed.
     * Initiatese the Import sequence.
     */
    importFile() {
        if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
            alert('The File APIs are not fully supported in this browser.');
            return;
        }

        let input = document.getElementById('script-import-input-file');
        if (!input) {
            alert('Unable to locate the file input element.');
        }
        else if (!input.files) {
            alert('This browser does not seem to support the "files" property of file inputs.');
        }
        else if (!input.files[0]) {
            alert('Please select a file before clicking "Import"');
        }
        else {
            let file = input.files[0],
                fr = new FileReader();
            fr.onload = () => {
                let data,
                    that = PDE.Automation;
                try {
                    data = JSON.parse(fr.result);
                } catch (e) {
                    logger.error(e);
                    alert(e);
                    return;
                }
                if (!data) {
                    return;
                }
                let scriptName = that.validateFile(data);
                if (scriptName) {
                    let scriptStorageItem = `${that.storagePrefix}${scriptName}`;
                    if (localStorage.getItem(scriptStorageItem)) {
                        MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_SCRIPT_EXISTS);
                        return;
                    }
                    localStorage.setItem(scriptStorageItem, JSON.stringify(data.script));
                    if (LF.Wrapper.isWrapped) {
                        that.writeToFile(scriptName + '.json', {
                            name: scriptName,
                            script: data.script,
                            steps: data.steps
                        })
                    }
                    logger.info(`Script '${scriptName}' imported`);
                    that.importView.hide();
                    that.controlsView.refreshControls();
                }
            };
            fr.readAsText(file);
        }
    }

    /**
     * Validates data before import.
     * @param data
     * @returns {boolean|String} false if invalid, or the value of data.name if valid.
     */
    validateFile (data) {
        if (!data.hasOwnProperty('name')) {
            const err = 'File is missing a name key';
            logger.error(err);
            alert(err);
            return false;
        }
        if (!data.hasOwnProperty('script')) {
            const err = 'File is missing a script key';
            logger.error(err);
            alert(err);
            return false;
        }
        if (!data.hasOwnProperty('steps')) {
            const err = 'File is missing a steps key';
            logger.error(err);
            alert(err);
            return false;
        }
        if (!data.name.length) {
            const err = 'The name in this file is not defined';
            logger.error(err);
            alert(err);
            return false;
        }
        if (!data.script.length) {
            const err = 'The script in this file is empty';
            logger.error(err);
            alert(err);
            return false;
        }
        if (!data.steps.length) {
            const err = 'The steps in this file are empty';
            logger.error(err);
            alert(err);
            return false;
        }
        return data.name;
    }

    /**
     * On the device, deletes the script file from app data storage.
     * @param scriptName
     * @param callback
     */
    deleteScriptFileFromAppStorage(scriptName, callback) {
        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dir) {
            dir.getFile(scriptName + '.json', {create: false}, function (fileEntry) {
                fileEntry.remove(function (file) {
                    callback(true);
                }, function (e) {
                    alert(`Error: ${e}`);
                    logger.error(e);
                    callback(false);
                }, function (data) {
                    alert(data);
                    logger.error(data);
                    callback(false);
                });
            });
        });
    }

    /**
     * Writes data to fileName in app data storage.
     * @param fileName
     * @param data
     */
    writeToFile (fileName, data) {
        if (!LF.Wrapper.isWrapped) {
            return;
        }
        data = JSON.stringify(data, null, '\t');
        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (directoryEntry) {
            directoryEntry.getFile(fileName, { create: true }, function (fileEntry) {
                fileEntry.createWriter(function (fileWriter) {
                    fileWriter.onwriteend = function (e) {
                        // for real-world usage, you might consider passing a success callback
                        logger.info(`Write of file '${fileName}' completed.`);
                    };

                    fileWriter.onerror = function (e) {
                        // you could hook this up with our global error handler, or pass in an error callback
                        logger.info(`Write failed: ${e.toString()}`);
                    };

                    let blob = new Blob([data], { type: 'text/plain' });
                    fileWriter.write(blob);
                }, PDE.Automation.fsErrorHandler.bind(null, fileName));
            }, PDE.Automation.fsErrorHandler.bind(null, fileName));
        }, PDE.Automation.fsErrorHandler.bind(null, fileName));
    }

    fsErrorHandler (fileName, e) {
        let msg = '';
        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'Storage quota exceeded';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'File not found';
                MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.PDE_AUTOMATION_FILE_NOT_FOUND);
                break;
            case FileError.SECURITY_ERR:
                msg = 'Security error';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'Invalid modification';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'Invalid state';
                break;
            default:
                msg = 'Unknown error';
                break;
        }
        logger.error(`Error (${fileName}): ${msg}`);
    }
}
