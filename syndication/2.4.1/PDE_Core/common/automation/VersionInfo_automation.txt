Version info for PDE_Automation
========================================================================

Known issues:

- DateSpinner playback format is not correct.

TODO: Add support for when Subect Creation is incomplete? This can be dealt with by manual intervention but it would
      be better to integrate an automated solution as the user will expect playback to be uninteractive.
      There are two situations to address:
      1) When during recording subject creation was incomplete in SW,
         playback must anticipate that subject creation may have completed
         so decide to skip the interaction with the warning message dialog.
      2) When during recording subject creation was already complete in SW,
         playback must anticipate that subject creation may not have been completed.
      The simplest way to achieve this will be to add a focused rule that caters
      for interaction with the warning message and make it optional to remove/add in
      such an interaction in an attempt to overcome a waiting situation.


========================================================================

LABEL   DATE            CHANGES
======  ===========     ============

16       28 Oct 2018    [cjames]
                        * Adds an information text field to the controls to display e.g. waiting information.
                        * Patch for V.15, permits legacy usage of user switching prior to V.15.

15       27 Oct 2018    [cjames]
                        * Fixes an issue with the user select on login view. Previously the script was recording the
                          user id only. The improved version now records the user role and will select the first user
                          with matching role on playback. If such a role is not available a message is shown and
                          playback is stopped.

14       27 Oct 2018    [cjames]
                        * Adds a roleStart field to the metadata to enforce playback start with same role selected
                          as was selected at recording start, together with a hard stop if roles do not match.

13       26 Oct 2018    [cjames]
                        * Adds a utility function for environment-dependant PDE Automation switch.

12       25 Oct 2018    [cjames]
                        * Fixes issue on tablet with subject activation 'Next' button delay.

11       24 Oct 2018    [cjames]
                        * Fixes issue with the Gateway views delay.

9        06 Oct 2018    [cjames]
                        * Changes the version numbering system to integer.
                        * Adds a version numbering check to the validation.
                        * Adds support for Tablet Gateway Views.
                        * Adds support for Tablet Matrix quesion HR output (e.g. CSSRS).
                        * Further improvements made to the HR output.
                        * Adds browser name and version check.

0.08     04 Oct 2018    [cjames]
                        * Implements human-readable output
                        * Tested on all standard library diaries for HH
                        * Adds DOLORES targets on the DOM.
                        * Re-write of the Import feature. Now supports direct
                          import from Google Drive via an input type="file" element.
                        * Import now supported on the desktop browser.

0.07     01 Oct 2018    [cjames]
                        * Implements studyVersion comparison and message.

0.06     30 Sep 2018    [cjames]
                        * Implements relative datebox dates.
                        * Re-formats dates and times in local build formatting to support formatting defects.

0.05     29 Sep 2018    [cjames]
                        * Removes the StateManager and removes state saving from the tool.

0.04     12 Sep 2018    [cjames]
                        * Adds Share feature
                        * Improves the Import feature, adding a Browse option
                        * Adds mouse event monitoring

0.03     06 Sep 2018    [cjames]
                        * Implementation for PDE_Core.
                        * Makes various pattern adjustments to conform to the Syndication framework.

0.02     16 Aug 2018    [cjames]
                        * Adds button icons and colors.
                        * Adds flashing indicator during playback/record.
                        * Prevents initiation of playback/record unless on Login View + dialog.
                        * Toggle feature on Toolbox View.
                        * Adds confirmation dialogs to Delete and Discard features.

0.01     19 Jul 2018    [cjames]
                        * Initial release
                        * Supported products: PDE Template 241.1.0 - Handheld (Tablet not yet tested)
