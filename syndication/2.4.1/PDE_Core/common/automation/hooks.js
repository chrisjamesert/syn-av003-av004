import TimeTravel from 'core/TimeTravel';
import SpinnerView from 'core/views/SpinnerView';
import { timeStamp } from 'core/DateTimeUtil';
import Logger from 'core/Logger';
let logger = new Logger('PDE Automation');

/**
 * Automation Time Travel hook
 * To record Time Travel we add a hook to localStorage.setItem and watch
 * for set of PHT_Time_Traveled. When we see this, we create a TimeTravel event.
 * Time Travel in Syndication Core is relative. PDE Automation adds a further degree of relativity
 * by ensuring that the playback of Time Travel is done relative to the start datetime, NOT the literal date
 * that was entered during recording.
 */
let originalSetItem = localStorage.setItem;
localStorage.setItem = function () {
    if (PDE.Automation.allowPDEAutomation && PDE.Automation.recordingScript && arguments.length && arguments[0] === 'PHT_Time_Traveled') {
        setTimeout(() => {
            if (PDE.Automation.recordingScript) {
                let dateString = timeStamp(new Date()), // Literal time to which we are travelling
                    cmd = PDE.Automation.cmdBuilder('TimeTravel', null, {
                        dateString,
                        phtTimeTraveled: arguments[1] // Offset relative to native date
                    }),
                    sentence = PDE.Automation.getRelativeTimeTravelSentence(dateString);
                logger.info(cmd);
                PDE.Automation.script.push(cmd);
                logger.info(sentence);
                PDE.Automation.hrOutput.push(sentence);
            }
        }, 1000);
    }
    originalSetItem.apply(this, arguments);
};

LF.Class.Logger.prototype.tryStore = function (level, message, nameValuePairs, type) {

    if (!Logger.getPath(LF, 'dataAccess')) {
        if (logToConsole !== false) {
            logger.warn(`Logger ${this.name} attempted to log to db, but dataAccess not ready.`);
            this.print(level, message, nameValuePairs, type);
        }
        return null;
    }

    try {
        return this.store(level, message, nameValuePairs, type);
    } catch (e) {
        if (localStorage.getItem('PDE_Automation_is_shown')) {
            // Ignore date mismatch errors caused by automation time travel / state restore
            return null;
        }
        logger.error(`Logger ${this.name} attempt to store a Log failed`, e);
        this.print(level, message, nameValuePairs, type);
        return null;
    }
};

// Instead of querying $('.fa-spinner').length on every single
// playback call, which would be quite intensive, we can
// use a flag for when the spinner is shown with the following hooks.
let originalSpinnerView_show = SpinnerView.prototype.show;
SpinnerView.prototype.show = function (translationKeys = { text: 'PLEASE_WAIT' }) {
    if (PDE.Automation.allowPDEAutomation) {
        PDE.Automation.spinnerIsShown = true;
    }
    return originalSpinnerView_show.apply(this, translationKeys);
};

let originalSpinnerView_hide = SpinnerView.prototype.hide;
SpinnerView.prototype.hide = function () {
    if (PDE.Automation.allowPDEAutomation) {
        PDE.Automation.spinnerIsShown = false;
    }
    return originalSpinnerView_hide.apply(this, arguments);
};

