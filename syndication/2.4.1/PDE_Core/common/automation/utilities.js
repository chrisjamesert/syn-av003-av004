/**
 * Use this function to determine whether or not PDE Automation is permitted
 * based on the current environment.
 * @returns {boolean}
 */
export function allowPDEAutomationUtil () {
    return ['dev', 'staging', 'anonymous'].includes(LF.environment.id.toLowerCase());
}
