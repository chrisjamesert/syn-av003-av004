/**
 * Syncs custom historical data with the Web Service using transmission queue.
 * @param {Object} params Includes web service method names.
 */
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

let logger = new Logger('customSyncDataRequest');

/**
 * Syncs historical data with the Web Service using transmission queue.
 * @param {Object} input Includes web service method names.
 * @return {Promise}
 */
export function customSyncDataRequest (input) {
    let method = 'customSyncData',
        params = {},
        model = new Transmission();

    input.webServiceFunction ? params.webServiceFunction = input.webServiceFunction : null;
    typeof input.activation !== 'undefined' ? params.activation = input.activation : null;

    return Q.Promise((resolve, reject) => {
        if (params.webServiceFunction) {
            resolve(model.save({
                method,
                params: JSON.stringify(params),
                created: new Date().getTime()
            }));
            return;
        }
        logger.error('webServiceFunction is undefined, rejecting customSyncDataRequest');
        reject();
    })
        .then(() => Transmissions.fetchCollection())
        .then((transmissions) => {
            let syncTransmission = transmissions.findWhere({ method });
            return transmissions.execute(transmissions.indexOf(transmissions.get(syncTransmission)));
        })
        .catch((err) => {
            err && logger.error('Error executing customSyncDataRequest', err);
        });
}

ELF.action('customSyncDataRequest', customSyncDataRequest);
