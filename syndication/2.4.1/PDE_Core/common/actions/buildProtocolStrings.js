import ELF from 'core/ELF';

/**
 * Adds STUDY namespace string resources for protocol names
 * Used in New-Patient protocol select dropdown
 * Avoids having to define separate protocol strings in a json namespace,
 * instead using the direct string values from protocolList
 * @returns {Q.Promise<void>} returns empty promise to continue chain
 */
export function buildProtocolStrings () {
    let protocolList = LF.StudyDesign.studyProtocol.protocolList,
        resources = {};
    for (let protVal in protocolList) {
        resources[protocolList[protVal]] = protocolList[protVal];
    }
    return Q(LF.strings.addLanguageResources({
        language: LF.StudyDesign.defaultLanguage || 'en',
        locale: LF.StudyDesign.defaultLocale || 'US',
        namespace: 'STUDY',
        resources
    }));
}

ELF.action('buildProtocolStrings', buildProtocolStrings);
