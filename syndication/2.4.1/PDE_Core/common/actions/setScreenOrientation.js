import ELF from 'core/ELF';
import { isSitePad } from 'core/utilities/coreUtilities';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

const logger = new Logger('actions/setScreenOrientation.js');

/**
 * Sets screen orientation specified in questionnaire config. Supported values are
 * 'portrait' and 'landscape'.
 * Orientation can be defined for a questionnaire or screen by screen.
 * Hierarchy for from most important to least is screen > questionnaire > deviceDefault
 * Screen Example:
 *      {
            id         : 'PL_PL010',
            className  : 'PL_PL010',
       ---> orientation: 'landscape', <---
            questions  : [
                {
                    id       : 'PL_PL010_Q0',
                    mandatory: true
                }
            ]
        }
 *Questionnaire Example:
 *      {
            id            : 'PL',
            SU'           : 'PL',
            displayName   : 'DISPLAY_NAME',
            className'    : 'questionnaire',
            previousScreen: false,
       ---> orientation   : 'landscape', <---
            affidavit     : 'DEFAULT',
            screens       : [
                'PL_PL010'
            ],
            branches      : [],
            initialScreen : [],
        }
 *
 * @param {object} [param] - optional object of parameters
 * @param {boolean} [param.setDefault] - optional, if true, returns to defaultOrientation of modality
 * @param {object} [param.popup] - optional, contains the config for a popup should the orientation change
 * @param {string} param.popup.toLandscape - required if popup object is supplied, string key of the MessageRepo Dialog to display when the orientation transitions to Landscape
 * @param {string} param.popup.toPortrait - required if popup object is supplied, string key of the MessageRepo Dialog to display when the orientation transitions to Portrait
 * @return {Q.Promise<void>}
 */
export function setScreenOrientation (param) {
    let defaultOrientation = isSitePad() ? 'landscape' : 'portrait',
        newOrientation,
        screen,
        questionnaireOrientation,
        nativeScreen = window.screen,
        dialogKey,
        orientationChanged,
        showPopup = () => {
            if (param.popup && orientationChanged) {
                /**
                 * Screen Orientation Plugin Update:
                 *
                 * Requires update of plugin to 3.0.1 and installation of dependency plugin 'es6-promise-plugin' 4.1.0.
                 * C James
                 *
                 * Code Fix 1:
                 *
                 * Description: The 'indexOf(...)' method is no longer available on the orientation object.
                 * Must reference orientation value instead.
                 *
                 * Original code for plugin 1.4.2:
                 * dialogKey = (newOrientation.indexOf('landscape') > -1) ?
                 *     MessageRepo.Dialog.SCREEN_ORIENTATION_LANDSCAPE_SET_CONFIRM : MessageRepo.Dialog.SCREEN_ORIENTATION_PORTRAIT_SET_CONFIRM;
                 *
                 * New code follows (1 line):
                 */
                dialogKey = newOrientation === 'landscape' ?
                    MessageRepo.Dialog[param.popup.toLandscape] : MessageRepo.Dialog[param.popup.toPortrait];
                return MessageRepo.display(dialogKey);
            }
            return Q();
        };

    if (param.setDefault) {
        newOrientation = defaultOrientation;
    } else {
        screen = this.data.screens[this.screen];
        questionnaireOrientation = this.model.get('orientation');
        newOrientation = screen.get('orientation') || questionnaireOrientation || defaultOrientation;
    }

    /**
     * Screen Orientation Plugin Update:
     *
     * Requires update of plugin to 3.0.1 and installation of dependency plugin 'es6-promise-plugin' 4.1.0.
     * C James
     *
     * Code Fix 2:
     *
     * Description: The 'indexOf(...)' method is no longer available on the orientation object.
     * Must reference orientation value instead.
     *
     * Original code for plugin 1.4.2:
     * orientationChanged = nativeScreen.orientation && nativeScreen.orientation.indexOf(newOrientation) === -1;
     *
     * New code follows (1 line):
     */
    orientationChanged = nativeScreen.orientation && nativeScreen.orientation !== newOrientation;

    return showPopup()
        .then(() => {
            if (orientationChanged) {
                /**
                 * Screen Orientation Plugin Update
                 *
                 * Requires update of plugin to 3.0.1 and installation of dependency plugin 'es6-promise-plugin' 4.1.0.
                 * C James
                 *
                 * Code Fix 3
                 *
                 * Description: The 'lockOrientation' method is no longer supported.
                 * Must use orientation.lock instead.
                 *
                 * Original code:
                 * nativeScreen.lockOrientation(newOrientation);
                 *
                 * New code follows (1 line):
                 */
                nativeScreen.orientation.lock(newOrientation);
            }
        })
        .catch((err) => {
            logger.error('error while setting screen orientation:', err);
        });
}

ELF.action('setScreenOrientation', setScreenOrientation);
