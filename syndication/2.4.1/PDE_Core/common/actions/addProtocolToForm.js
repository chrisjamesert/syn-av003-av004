import ELF from 'core/ELF';
import Logger from 'core/Logger';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';

/**
 * @memberOf ELF.actions
 * @method addProtocolToForm
 * @description
 * Uses the subject context of the current questionnaire view to add a form submission item Protocol.0.Protocol.
 * Should only be used in 'QUESTIONNAIRE:' triggers, where 'this' is the current view
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{
 *    action: 'addProtocolToForm'
 * }]
 */
export default function addProtocolToForm () {
    let view = this,
        subject = view.subject,
        logger = new Logger('PDE.action:addProtocolToForm');
    return Q.Promise((resolve, reject) => {
        if (!(view instanceof BaseQuestionnaireView)) {
            logger.error('current view is not an instance of BaseQuestionnaireView', { view });
            reject(false);
        } else if (!subject) {
            logger.error('unable to find subject context for this form');
            reject(false);
        } else if (!subject.get('custom10')) {
            logger.error('unable to find protocol information (custom10) for this Subject', { subject });
            reject(false);
        } else {
            resolve(view.addIT({
                question_id: 'PROTOCOL',
                questionnaire_id: view.id,
                response: subject.get('custom10'),
                IG: 'Protocol',
                IGR: 0,
                IT: 'Protocol'
            }));
        }
    }).catch((err) => {
        if (err) {
            logger.error('Unexpected error occured', err);
        }
        return true;
    });
}

ELF.action('addProtocolToForm', addProtocolToForm);
