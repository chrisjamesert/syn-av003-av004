import ELF from 'core/ELF';
import PDE from 'PDE_Core/common/PDE';

/**
 * @memberOf ELF.expressions
 * @method  isTrainer
 * @description
 * Determines if the device is currently in trainer mode
 * @returns {Q.Promise<boolean>} True if in trainer mode, false otherwise
 * @example
 * evaluate : 'isTrainer'
 */
export default function isTrainer () {
    return Q(PDE.DeviceStatusUtils.isTrainer());
}

ELF.expression('isTrainer', isTrainer);
