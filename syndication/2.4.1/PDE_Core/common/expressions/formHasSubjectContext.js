import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method  formHasSubjectContext
 * @description
 * Determines if the questionnaire being completed has a subject context
 * @returns {Q.Promise<boolean>} True if the current questionnaire view has a reference to a subject
 * @example
 * evaluate : 'formHasSubjectContext'
 */
export default function formHasSubjectContext () {
    return Q(Boolean(this.subject));
}

ELF.expression('formHasSubjectContext', formHasSubjectContext);
