import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method  doFullSync
 * @description
 * Determines if fullSync is enabled for the device
 * @returns {Q.Promise<boolean>} True if full sync is enabled in the study design
 * @example
 * evaluate : 'doFullSync'
 */
export default function doFullSync () {
    return Q(Boolean(LF.StudyDesign.enableFullSync));
}

ELF.expression('doFullSync', doFullSync);
