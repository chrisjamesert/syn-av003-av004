import CurrentContext from 'core/CurrentContext';
import { isSitePad, isLogPad } from 'core/utilities/coreUtilities';
import Logger from 'core/Logger';
let logger = new Logger('automationDynamicText');

export default {
    dynamicText: [
        {
            id: 'PDE_Automation_recordedStudyVersion',
            evaluate: () => {
                return PDE.Automation.script[0].studyVersion;
            },
            screenshots: {
                values: ['00.01']
            }
        },
        {
            id: 'PDE_Automation_studyVersion',
            evaluate: () => {
                return LF.StudyDesign.studyVersion;
            },
            screenshots: {
                values: ['00.02']
            }
        },
        {
            id: 'PDE_Automation_starting_view',
            evaluate: () => {
                return LF.getStrings(['PDE_AUTOMATION_STARTING_VIEW_HH',
                    'PDE_AUTOMATION_STARTING_VIEW_TB'])
                .then((strings) => {
                    if (isLogPad()) {
                        return strings[0];
                    } else if (isSitePad()) {
                        return strings[1];
                    } else {
                        logger.error(`Unknown modality: ${LF.appName}`);
                        return 'ERROR';
                    }
                });
            },
            screenshots: {
                values: ['Login View']
            }
        },
        {
            id: 'PDE_Automation_recordedModality',
            evaluate: () => {
                return PDE.Automation.script[0].modality;
            },
            screenshots: {
                values: ['SitePad App']
            }
        },
        {
            id: 'PDE_Automation_modality',
            evaluate: () => {
                return LF.appName;
            },
            screenshots: {
                values: ['LogPad App']
            }
        },
        {
            id: 'PDE_Automation_recordedBrowser',
            evaluate: () => {
                let name = PDE.Automation.script[0].browser.name,
                    version = PDE.Automation.script[0].browser.version;
                return `${name} v.${version}`;
            },
            screenshots: {
                values: ['Chrome 67']
            }
        },
        {
            id: 'PDE_Automation_playbackBrowser',
            evaluate: () => {
                let browser = PDE.Automation.getBrowser();
                return `${browser.name} v.${browser.version}`;
            },
            screenshots: {
                values: ['Chrome 69']
            }
        },
        {
            id: 'PDE_Automation_recordedStartingRole',
            evaluate: () => {
                return PDE.Automation.script[0].roleStart;
            },
            screenshots: {
                values: ['site']
            }
        },
        {
            id: 'PDE_Automation_playbackStartingRole',
            evaluate: () => {
                return CurrentContext().role;
            },
            screenshots: {
                values: ['admin']
            }
        }
    ]
};
