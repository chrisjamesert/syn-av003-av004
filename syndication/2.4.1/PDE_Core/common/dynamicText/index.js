import automationDynamicText from './automationDynamicText';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, automationDynamicText);
