import Logger from 'core/Logger';
import ELF from 'core/ELF';
import PDEWebService from 'PDE_Core/common/webService/commonWebService';
import COOL from 'core/COOL';
import { isLogPad, isSitePad } from 'core/utilities/coreUtilities';

const logger = new Logger('customSyncData');

/**
 * Handles custom historical data sync transmission to the web service
 * @param {Object} transmissionItem The item in the transmission queue to send.
 * @returns {Promise}
 */
export function customSyncData (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params')),
        historicalDataSyncSuccess = ({ res }) => {
            // Don't update data unless we get a response
            if (_.isEmpty(res)) {
                return this.destroy(transmissionItem.get('id'));
            }
            return ELF.trigger(`CUSTOMDATASYNC:Received/${params.webServiceFunction}`, res)
                .then(() => this.destroy(transmissionItem.get('id')))
                .catch((err) => {
                    err && logger.error('Error destroying transmission', err);
                    return Q.reject(err);
                });
        },
        historicalDataSyncError = (err) => {
            // Remove from queue
            return this.destroy(transmissionItem.get('id'))
                .then(() => Q.reject(err));
        };

    return COOL.new('WebService', PDEWebService)[params.webServiceFunction](params)
        .catch(historicalDataSyncError)
        .then(historicalDataSyncSuccess);
}

LF.Transmit = _.extend({}, LF.Transmit, { customSyncData });
COOL.service('Transmit', LF.Transmit);
