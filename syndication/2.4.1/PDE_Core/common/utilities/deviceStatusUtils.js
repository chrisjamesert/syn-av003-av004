import PDE from 'PDE_Core/common/PDE';

/**
 * isTrainer checks local storage for a trainer flag
 * @returns {boolean} returns true if in trainer mode
 */
export function isTrainer () {
    return Boolean(localStorage.getItem('trainer'));
}

PDE.DeviceStatusUtils.isTrainer = isTrainer;


/**
 * isScreenshotMode checks local storage for a screenshot flag
 * @returns {boolean} returns true if in screenshot mode
 */
export function isScreenshotMode () {
    return Boolean(localStorage.getItem('screenshot'));
}

PDE.DeviceStatusUtils.isScreenshotMode = isScreenshotMode;
