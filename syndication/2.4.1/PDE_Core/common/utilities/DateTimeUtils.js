/* eslint-disable no-fallthrough */
import PDE from 'PDE_Core/common/PDE';
import Logger from 'core/Logger';

const logger = new Logger('PDE.DateTimeUtils');

/**
 * dateDiffInDays returns the number of days between date 1 and date 2, rounded down to the nearest day, taking into account time zone offsets.
 * If useAbsolute evaluates as false, then the return value is date1 - date2.
 * If useAbsolute evaluates as true, the return value is |date1 - date2| (the absolute value of the difference)
 * @param {Date} date1 first date to compare
 * @param {Date} date2 second date to compare
 * @param {boolean} [useAbsolute] if true, then returns the absolute value of calculation
 * @returns {number} dateDiff
 */
export function dateDiffInDays (date1, date2, useAbsolute) {
    let date1Copy = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), 0, 0, 0, 0),
        date2Copy = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), 0, 0, 0, 0),
        date1Offset = date1Copy.getTimezoneOffset() * 60 * 1000,
        date2Offset = date2Copy.getTimezoneOffset() * 60 * 1000,
        msDiff;

    if (!useAbsolute) {
        msDiff = date1Copy - date2Copy + (date2Offset - date1Offset);
    } else {
        msDiff = Math.abs(date1Copy - date2Copy + (date2Offset - date1Offset));
    }
    return Math.floor(msDiff / 1000 / 60 / 60 / 24);
}

PDE.DateTimeUtils.dateDiffInDays = dateDiffInDays;

/**
 * isValidDate courtesy of stackoverflow
 * https://stackoverflow.com/questions/1353684/detecting-an-invalid-date-date-instance-in-javascript
 * @param {Object} date object to validate as a Javascript date object containing a valid date
 * @returns {boolean} true if object date is a valid date.
 */
export function isValidDate (date) {
    return Object.prototype.toString.call(date) === '[object Date]' ? !isNaN(date.getTime()) : false;
}

PDE.DateTimeUtils.isValidDate = isValidDate;

/**
 *
 * format converts a date into one specifically formatted according to the current locality's dateConfig option.
 * Uses date passed in, or defaults to current date if a falsey value is passed into the first parameter.
 * @memberof! DateTimeUtils
 * @param {Date|string} [date] - OPTIONAL the date to format
 * @param {string} format - name of dateConfig item that contains the format
 * @returns {string} the formatted string
 */
export function format (date, format) {
    const dateOptions = _.extend({}, LF.strings.dates({ dates: {} }), LF.strings.dates({ dateConfigs: {} })),
        dateBox = $('<input data-role=\"datebox\" />'),
        dateLang = {
            lang: {
                default: dateOptions,
                isRTL: LF.strings.getLanguageDirection() === 'rtl'
            },
            mode: 'datebox'
        };

    dateBox.datebox(dateLang);

    if (date) {
        if (typeof date === 'string') {
            date = new Date(date);
        }

        if (isValidDate(date)) {
            return dateBox.datebox('callFormat', dateOptions[format], date);
        }
        let errMsg = 'Provided date or time is not valid.';
        logger.error(errMsg);
        throw new Error(errMsg);
    }
    return dateBox.datebox('callFormat', dateOptions[format], dateBox.datebox('getTheDate'));
}

PDE.DateTimeUtils.format = format;

/**
 * validates a string to ensure it fits a valid time pattern for javascript date object generation.
 * valid time patterns consist of 24 hour time in the format of 'hour:minute:second:millisecond'
 * where hour and minute are required,
 * second and millisecond with their preceding colons are optional,
 * and leading zeros are optional.
 * Ranges:
 * hours 0-23
 * minutes 0-59
 * seconds 0-59
 * milliseconds 0-999
 * @param {string} time string to check if valid
 * @returns {boolean} true if valid time string, false otherwise
 * @examples
 * '12:24:36:123'
 * '01:01:1:1'
 * '1:2:3:004'
 * '12:12'
 * '3:16:50'
 * '23:59:59:03'
 */
export function isValidTimeString (time) {
    let timeArr = (typeof time === 'string') ? time.split(':') : [],
        temp;

    // noinspection FallThroughInSwitchStatementJS
    switch (timeArr.length) {
        case 4:
            temp = Number(timeArr[3]);
            if (isNaN(temp) || temp < 0 || temp > 999) {
                return false;
            }
        case 3:
            temp = Number(timeArr[2]);
            if (isNaN(temp) || temp < 0 || temp > 59) {
                return false;
            }
        case 2:
            temp = Number(timeArr[1]);
            if (!(isNaN(temp) || temp < 0 || temp > 59)) {
                temp = Number(timeArr[0]);
                if (!(isNaN(temp) || temp < 0 || temp > 23)) {
                    return true;
                }
            }
        default:
            return false;
    }
}

PDE.DateTimeUtils.isValidTimeString = isValidTimeString;

/**
 * formatTime converts date object or valid string representation of time (validated by PDE.DateTimeUtils.isValidTimeString)
 * to the format defined in dateConfig's timeOutput resource
 * Defaults to current time if first parameter is falsey
 * @param {Date|string} [time] - OPTIONAL date object or time string to format
 * @returns {string} - the formatted string
 */
export function formatTime (time) {
    if (typeof time === 'string' && isValidTimeString(time)) {
        return format(`01 Jan 1900 ${time}`, 'timeOutput');
    } else if (time instanceof Date || !time) {
        // format uses current time if first parameter is falsey
        return format(time, 'timeOutput');
    }
    let errMsg = 'Parameter is neither a valid time string nor date object';
    logger.error(errMsg);
    throw new Error(errMsg);
}

PDE.DateTimeUtils.formatTime = formatTime;

/**
 * formatDate converts date object to the format
 * defined in dateConfig's dateFormat resource
 * Defaults to current date if first parameter is falsey
 * @param {Date|string} date - OPTIONAL date object or date string to format
 * @returns {string} - the formatted string
 */
export function formatDate (date) {
    return format(date, 'dateFormat');
}

PDE.DateTimeUtils.formatDate = formatDate;

/**
 * formatDateTime converts date object to the format
 * defined in dateConfig's dateTimeFormat resource if it exists,
 * otherwise it concats the resuld from dateFormat and timeOutput
 * Defaults to current date and time if first parameter is falsey
 * @param {Date|string} date - OPTIONAL date object or dateTime string to format
 * @returns {string} - the formatted string
 */
export function formatDateTime (date) {
    let currentLangDateConfigs = LF.strings.dates({ dateConfigs: {} });
    if (currentLangDateConfigs.dateTimeFormat) {
        return format(date, 'dateTimeFormat');
    }
    return `${formatDate(date)} ${formatTime(date)}`;
}

PDE.DateTimeUtils.formatDateTime = formatDateTime;
