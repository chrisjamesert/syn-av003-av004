import PDE from '../PDE';
import Logger from 'core/Logger';

let CollectionModelUtils = PDE.CollectionModelUtils;

/**
 * getCollection returns a promise that resolves with the requested collection with data fetched
 * @param {string} collectionName the name of the collection to fetch
 * @returns {Promise.<collection>} promise that resolves as collection populated with data
 */
export function getCollection (collectionName) {
    let logger = new Logger('PDE_Utilities.getCollection');
    return Q()
        .then(() => {
            if (typeof LF.Collection[collectionName] !== 'undefined') {
                return LF.Collection[collectionName].fetchCollection();
            }
            throw new Error(`Failed: ${collectionName} is not a valid collection.`);
        }).then((fetchedCollection) => {
            logger.info(`Successfully fetched collection ${collectionName}`);
            return fetchedCollection;
        }, (err) => {
            logger.error(`Failed: unable to fetch data for ${collectionName}.`, err);
            throw err;
        });
}

CollectionModelUtils.getCollection = getCollection;

/**
 * getCollections returns a promise that resolves with an object,
 * where the keys are names of collections that contains values of the fetched collections.
 * @param {string[]} collectionNames array of collection names to fetch
 * @returns {Promise.<Object<collections>>} object containing keys names for each fetched collection, with values of the collections
 */
export function getCollections (collectionNames) {
    let promiseFetches = [];

    if (!_.isArray(collectionNames)) {
        throw new Error('Fail to get collections. CollectionNames is not an array.');
    }

    _.forEach(collectionNames, (colName) => {
        promiseFetches.push(getCollection(colName));
    });
    return Q.all(promiseFetches)
        .then((fetchedColArray) => {
            let result = {};

            _.each(fetchedColArray, (collection, index) => {
                result[collectionNames[index]] = collection;
            });

            return result;
        });
}

CollectionModelUtils.getCollections = getCollections;

/**
 * Clear collections in a given collection names
 *
 * @param {Array} collectionNames of the collections to be clear
 @returns {promise} resolves or rejects with error messages
 */
export function clearCollections (collectionNames) {
    let logger = new Logger('CollectionModelUtils.clearCollections');

    return Q.all(_.map(collectionNames, (collectionName) => {
        // If collection is unknown
        if (!LF.Collection[collectionName]) {
            Q.reject(new Error(`Unable to clear collection. Collection "${collectionName}" does not exist.`));
        }

        let collection = new LF.Collection[collectionName]();
        return collection.clear()
            .then(() => {
                logger.info(`Successfully clear "${collectionName}" Collection.`);
            });
    }));
}

CollectionModelUtils.clearCollections = clearCollections;

/**
 * Convert XML Element to a Model
 * @param {Object} element value to be converted into a model
 * @returns {Object} model
 */
export function convertXmlElementToModel (element) {
    let logger = new Logger('CollectionModelUtils.clearCollections'),
        numValues = 0,
        obj = {};

    $.each(element.attributes, (i, attr) => {
        let name = attr.name;

        obj[name] = attr.value;
        numValues++;
    });

    // Only return a model if there is values for it
    if (numValues > 0) {
        if (LF.Model[element.nodeName]) {
            return new LF.Model[element.nodeName](obj);
        }
        let err = `Failed to convert XML to Model. "${element.nodeName}" model does not exist.`;
        logger.error(err);
        throw new Error(err);
    } else {
        let err = `Failed to convert XML to Model. No values could be processed in the supplied element : ${element}`;
        logger.error(err);
        throw new Error(err);
    }
}

CollectionModelUtils.convertXmlElementToModel = convertXmlElementToModel;
