import PDE from 'PDE_Core/common/PDE';
import Logger from 'core/Logger';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Answer from 'core/models/Answer';

/**
 * addSysvar attempts to use the given params to add a sysvar to a questionnaire view
 * @param {Object} params={} object containing the parameters for this sysvar
 * @param {PageView} [params.view=LF.router.view()] view to add sysvar to, if missing defaults to current view found via LF.router.view()
 * @param {string} [params.questionnaire_id] questionnaire id for sysvar Answer, if missing attempts to pull id from params.view
 * @param {string} params.IG the IG to send this sysvar to (typically PT, for patient data)
 * @param {string} params.sysvar the sysvar being submitted
 * @param {!(string|number|boolean)} params.response value to be submitted for this sysvar
 * @returns {boolean} true if addSysvar executed without issue
 * @throws {Error} throws error if params fails validation. Logger error includes array of messages describing failed params.
 */
export function addSysvar (params = {}) {
    // noinspection FunctionWithMoreThanThreeNegationsJS
    let model,
        logger = new Logger('PDE.QuestionnaireUitls.addSysvar'),
        validateParams = (p) => {
            let errors = [];
            if (!p.view) {
                logger.info('View not supplied, assuming current view.');
                p.view = LF.router.view();
            }
            if (p.view instanceof QuestionnaireCompletionView) {
                p.view = p.view.questionnaire;
            }
            if (!(p.view instanceof BaseQuestionnaireView)) {
                errors.push('View is not a questionnaire, unable to add sysvar');
            }
            if (!(p.questionnaire_id || p.view.id)) {
                errors.push('Unable to determine questionnaire id');
            }
            if (!p.IG) {
                errors.push('No IG supplied');
            }
            if (!p.sysvar) {
                errors.push('No sysvar supplied');
            }
            if (p.response === null || p.response === undefined) {
                errors.push('No response supplied');
            }
            return errors.length ? errors : true;
        },
        checkParams = validateParams(params);
    if (checkParams === true) {
        model = new Answer({
            response: params.response,
            SW_Alias: `${params.IG}.${params.sysvar}`,
            questionnaire_id: params.questionnaire_id || params.view.id,
            question_id: 'SYSVAR'
        });
        if (params.view.data.dashboard) {
            model.set('instance_ordinal', params.view.data.dashboard.get('instance_ordinal'));
        }
        params.view.data.answers.add(model);
        return true;
    }
    logger.error('Attempt failed to insert a sysvar.', { errorMessages: checkParams });
    throw new Error('Attempt failed to insert a sysvar.');
}

PDE.QuestionnaireUtils.addSysvar = addSysvar;
