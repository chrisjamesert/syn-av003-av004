/**
 * Created by bob.yennaco on 3/20/2017.
 */

import UserReports from 'sitepad/collections/UserReports';

LF.Schedule.schedulingFunctions.PDEnTimesPerVisit = function (schedule, completedQuestionnaires, value, context, callback) {
    let scheduleParams = schedule.get('scheduleParams') || {},
        questionnaireID = schedule.get('target').id,
        currentSubject = context.subject,
        currentVisit = context.visit,
        maxReports,
        visitID;

    if (!currentVisit || _.isEmpty(scheduleParams)) {
        callback(false);
        return;
    }

    // TODO: Rewrite this to cater for two Protocols
    let currentVisitParams = _.select(scheduleParams.visits, (visitParams) => {
        let result = false;

        if (visitParams.visitID === currentVisit.id) {
            if (!!visitParams.phase) {
                let phase = _.select(visitParams.phase, (phaseID) => {
                    return phaseID == currentSubject.get('phase');
                });

                result = phase.length > 0;
            } else {
                result = true;
            }
        }

        return result;
    });

    if (currentVisitParams.length > 0) {
        let userReports = new UserReports();

        // Should be only one
        currentVisitParams = currentVisitParams[0];
        maxReports = parseInt(currentVisitParams.maxReports, 10);
        visitID = currentVisitParams.visitID;

        if (maxReports === 0) {
            // 0 means always available
            callback(true);
        } else {
            userReports.fetch().then((reports) => {
                let result = reports.length === 0,
                    filteredReports;

                // PDE: Add support for a single predecessor.  Ultimately should support multiple predecessors.
                let predecessorID = currentVisitParams.predecessorID;
                if (predecessorID != null) {
                    filteredReports = _.filter(reports, (report) => {
                        return report.get('user_visit_id') === visitID &&
                            report.get('questionnaire_id') === predecessorID &&
                            report.get('subject_krpt') === currentSubject.get('krpt');
                    });
                    if (!!filteredReports && (filteredReports.length == 0)) {
                        // Completed predecessor not found.  Check to see if it's completion is in progress.
                        // If it is, then this diary is available.
                        let reportSaveInProgress = localStorage.getItem("ReportSaveInProgress");
                        if ((reportSaveInProgress != null) && (reportSaveInProgress === predecessorID)) {
                            callback(true);
                        } else {
                            callback(false);
                        }
                        return;
                    }
                }

                if (!result) {
                    filteredReports = _.filter(reports, (report) => {
                        return report.get('user_visit_id') === visitID &&
                            report.get('questionnaire_id') === questionnaireID &&
                            report.get('subject_krpt') === currentSubject.get('krpt');
                    });
                    result = !!filteredReports ? (filteredReports.length < maxReports) : false;
                }

                callback(result);
            });
        }
    } else {
        callback(false);
    }
};
