/**
 * @fileOverview This is an example visits study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.1
 */
export default {
    visits: [],

    // Server value map. value maps to LF.VisitStates (see application.js)
    visitStatus: {
        1: 'SKIPPED',
        2: 'INCOMPLETE',
        3: 'COMPLETED'
    },

    visitAction: {
        show: 'show',
        hide: 'hide',
        notAvailable: 'notAvailable'
    }
};
