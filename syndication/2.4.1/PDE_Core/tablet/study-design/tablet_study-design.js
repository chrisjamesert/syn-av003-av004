import ArrayRef from 'core/classes/ArrayRef';

export default {
    // The current tablet study version
    studyVersion: '00.01',

    // The current tablet study database version
    studyDbVersion: 0,

    // flag to enable partial sync for the tablet modality
    enablePartialSync: false,

    // flag to enable full sync for the tablet modality
    enableFullSync: true,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 30,

    // The amount of time in minutes that a user is allowed to complete a diary within.
    // Should not be > sessionTimeout, since sessionTimeout will happen first and exit the diary anyway.
    // If 0, questionnaire itself doesn't time out, but sessionTimeout will still occur
    questionnaireTimeout: 0,

    // Configuration for rater training
    raterTrainingConfig: {
        sitepad: {
            useRaterTraining: false,
            awsKey: '',
            awsSecretKey: '',
            awsRegion: '',
            awsBucket: '',
            rootDirectory: 'rater',
            fallBackBaseURL: '',
            pages: {
                default: 'index.html'
            },
            zipFile: true
        }
    },

    // Configuration for participant/patient
    participantSettings: {
        participantID: {
            seed: 100,
            steps: 1,
            max: 600
        },
        participantIDFormat: '0000-0000',
        participantNumberPortion: new ArrayRef([5, 9]),
        siteNumberPortion: new ArrayRef([0, 4])

    },

    // The phase from studyPhase to be used as the phase after Subject Assignment
    subjectAssignmentPhase: 10
};
