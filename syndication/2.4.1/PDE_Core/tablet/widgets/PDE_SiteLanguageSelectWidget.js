import PDE from 'PDE_Core/common/PDE';
import LanguageSelectWidget from 'core/widgets/LanguageSelectWidget';

import * as utils from 'core/utilities';
import Sites from 'core/collections/Sites';

export default class PDE_SiteLanguageSelectWidget extends LanguageSelectWidget {

    /**
     * Filters an array of language objects.  Compares to the study-design siteUserLanguageList.
     * If no languages are specified in the study-defined list, all languages are returned as default.
     * @param  {Object} languages -  String array of language objects. ex.: LF.strings.getLanguages().
     * @returns {Q.Promise<Language[]>}
     */
    filterLanguagesBySpecification (languages) {
        if (utils.isSitePad() || utils.isWeb()) {
            return Sites.fetchFirstEntry().then((site) => {
                // Currently no support for per-site language filtering
                //let siteCode = site && site.get('countryCode');
                let studyLanguages = languages;
                if (LF.StudyDesign.siteUserLanguageList){
                    let filteredLanguageArray = [];
                        studyLanguages = LF.StudyDesign.siteUserLanguageList.default;
                        debugger
                    if (studyLanguages.length > 0) {
                        filteredLanguageArray = super.filterLanguagesByLocale(studyLanguages);

                        if (filteredLanguageArray.length != 0){
                            studyLanguages = filteredLanguageArray;
                        }
                    }
                }
                return studyLanguages;
            });
        }

        return Q(languages);
    }

    filterLanguagesByLocale (languages) {

        // This is a workaround for an issue this functionality created on the LogPad App.
        // This widget needs to be extended in sitepad/widgets.
        if (utils.isSitePad() || utils.isWeb()) {
            // This is a PDE workaround to display all site languages for testing purposes during trainer
            // as well ass if the DevConfig object is set up
            if (PDE.DeviceStatusUtils.isTrainer() && PDE.DevConfig && PDE.DevConfig.disableLanguageFiltering) {
                return Q.Promise((resolve) => {
                    // Filtering is disabled.  Return all languages in the default list.
                    let filteredLanguageArray = [];
                    let defaultList = LF.StudyDesign.sitelanguagelist.default;
                    filteredLanguageArray = languages.filter((languageObj) => {
                        return defaultList.indexOf(`${languageObj.language}-${languageObj.locale}`) !== -1;
                    });
                    return resolve(filteredLanguageArray);
                });
            }else {
                return Sites.fetchFirstEntry().then((site) => {
                    let siteCode = site && site.get('countryCode');
                    let studyLanguages = languages;
                    if (LF.StudyDesign.siteUserLanguageList && (Object.keys(LF.StudyDesign.siteUserLanguageList).length > 0)) {
                        let filteredLanguageArray = [];

                        // get a set/array of all the locales int the site language list
                        // get default list if there is one.
                        let locales = Object.keys(LF.StudyDesign.siteUserLanguageList);
                        let defaultList = LF.StudyDesign.siteUserLanguageList.default;

                        // If countryCode matches a locales key filter the languages for inclusion. If NOT found in the study sitelanguage list,
                        // do NOT filter and return all languages. Study language set is ultimate source of truth.
                        if (siteCode && locales.indexOf(siteCode) !== -1) {
                            let sitelanguageset = LF.StudyDesign.siteUserLanguageList[siteCode];
                            filteredLanguageArray = studyLanguages.filter((languageObj) => {
                                return sitelanguageset.indexOf(`${languageObj.language}-${languageObj.locale}`) !== -1;
                            });
                            studyLanguages = filteredLanguageArray;
                        } else if (defaultList) {
                            filteredLanguageArray = studyLanguages.filter((languageObj) => {
                                return defaultList.indexOf(`${languageObj.language}-${languageObj.locale}`) !== -1;
                            });
                            studyLanguages = filteredLanguageArray;
                        }
                    }
                    return studyLanguages;
                });
            }
        }

        return Q(languages);

        //return this.filterLanguagesBySpecification(languages);
    }
}

window.LF.Widget.PDE_SiteLanguageSelectWidget = PDE_SiteLanguageSelectWidget;

