import PDE from 'PDE_Core/common/PDE';
import LanguageSelectWidget from 'core/widgets/LanguageSelectWidget';

export default class PDE_SubjectLanguageSelectWidget extends LanguageSelectWidget {

    filterLanguagesByLocale (languages) {
        if (PDE.DeviceStatusUtils.isTrainer() && PDE.DevConfig && PDE.DevConfig.disableLanguageFiltering) {
            return Q.Promise((resolve) => {
                // Filtering is disabled.  Return all languages in the default list.
                let filteredLanguageArray = [];
                let defaultList = LF.StudyDesign.sitelanguagelist.default;
                filteredLanguageArray = languages.filter((languageObj) => {
                    return defaultList.indexOf(`${languageObj.language}-${languageObj.locale}`) !== -1;
                });
                return resolve(filteredLanguageArray);
            });
        } else {
            return super.filterLanguagesByLocale(languages);
        }
    }
}

window.LF.Widget.PDE_SubjectLanguageSelectWidget = PDE_SubjectLanguageSelectWidget;
