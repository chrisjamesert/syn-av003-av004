import Logger from 'core/Logger';
import PDE from 'PDE_Core/common/PDE';
import ELF from 'core/ELF';

export function newPatientAddProtocol () {
    let view = this,
        logger = new Logger('PDE.action:newPatientAddProtocol');
    if (view.id !== 'New_Patient') {
        let err = `Expected current view to be New_Patient questionnaire, got ${view.id} instead`;
        logger.error(err);
        return Q.reject(err);
    }
    return Q(_.first(view.queryAnswersByIT('ADD_PATIENT_PROTOCOL')))
        .then((answer) => {
            if (!answer) {
                logger.error('Expected to find answer with IT: ADD_PATIENT_PROTOCOL');
                return Q.reject();
            }
            return answer.get('response');
        }).then((protocol) => {
            if (!_(Object.keys(LF.StudyDesign.studyProtocol.protocolList)).contains(protocol)) {
                logger.error('Expected protocol code to exist in protocolList', {
                    protocol,
                    protocolList: LF.StudyDesign.studyProtocol.protocolList
                });
                return Q.reject();
            }
            let questionnaire_id = view.id,
                IG = 'PT';

            // Add protocol to PT.Custom9 and PT.Custom10
            PDE.QuestionnaireUtils.addSysvar({
                view,
                questionnaire_id,
                IG,
                sysvar: 'Custom9',
                response: protocol
            });
            PDE.QuestionnaireUtils.addSysvar({
                view,
                questionnaire_id,
                IG,
                sysvar: 'Custom10',
                response: protocol
            });

            // send protocol to Protocol.0.Protocol
            view.addIT({
                question_id: 'SYSVAR',
                questionnaire_id,
                response: protocol,
                IG: 'Protocol',
                IGR: 0,
                IT: 'Protocol'
            });
            return true;
        }).catch((err) => {
            if (err) {
                logger.error('Unexpected error occurred', err);
            }
            return false;
        });
}

ELF.action('newPatientAddProtocol', newPatientAddProtocol);
