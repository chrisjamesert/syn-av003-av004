import ELF from 'core/ELF';

/**
 * Triggers a PDEaddSysvar hook, intended for new-patient and edit-patient questionnaires
 * @memberOf ELF.actions/sitepad
 * @method triggerPDEaddSysvar
 * @param {Object} params - Not used in this action.
 * @param {function} done - A callback function invoked upon completion of the action.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'triggerPDEaddSysvar' }]
 */
export function triggerPDEaddSysvar (params, done) {
    ELF.trigger(`QUESTIONNAIRE:PDEaddSysvar/${this.id}`, {}, this)
        .done(() => {
            done();
        });
}

ELF.action('triggerPDEaddSysvar', triggerPDEaddSysvar);
