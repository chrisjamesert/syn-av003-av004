import ELF from 'core/ELF';
import Answer from 'core/models/Answer';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import * as lStorage from 'core/lStorage';

// import the core-defined editPatientSave to be 100% certain this file will override the ELF.action() registration
import 'sitepad/actions/editPatientSave';

let logger = new Logger('PDE_editPatientSave');

/**
 * Saves a new Edit Patient Diary
 * @memberOf ELF.actions/sitepad
 * @method editPatientSave
 * @param {Object} params - Not used in this action.
 * @param {function} done - A callback function invoked upon completion of the action.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'editPatientSave' }]
 */
export function editPatientSave (params, done) {
    let deferred = Q.defer(),
        diaryAnswers = this.data.answers,
        subject = this.subject,
        user = this.user,
        dashboard = this.data.dashboard,
        reason = diaryAnswers.match({ question_id: 'EDIT_PATIENT_REASON' })[0].get('response'),
        answersToRemove = [],
        transmissionsToCheck,
        addStatus = false,
        view = this,
        checkAndAddAnswers = () => {
            let answer,
                subjectIdAnswer,
                subjectInitialsAnswer,
                languageAnswer;

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' });
            subjectIdAnswer = answer[0].get('response');

            if (subjectIdAnswer === subject.get('subject_id')) {
                answersToRemove.push(answer[0]);
                if (subject.get('isDuplicate')) {
                    addStatus = true;
                }
            } else {
                let comment = new Answer({
                        subject_id: subject.get('subject_id'),
                        response: reason,
                        SW_Alias: 'PT.Patientid.Comment',
                        questionnaire_id: this.id,
                        question_id: 'EDIT_PATIENT_ID_COMMENT'
                    }),
                    subjectNumber = subjectIdAnswer.substring(LF.StudyDesign.participantSettings.participantNumberPortion[0],
                        LF.StudyDesign.participantSettings.participantNumberPortion[1]);

                diaryAnswers.add(comment);

                // setting the subject_id to subject model
                subject.set('subject_id', subjectIdAnswer);

                // setting subject number to subject model
                subject.set('subject_number', subjectNumber);

                // setting the subject_id to username of the user model (DE16517)
                user.set('username', subjectIdAnswer);

                // hacking SWAlias
                answer[0].set('SW_Alias', answer[0].get('SW_Alias').replace('.0.', '.'));
            }

            /*answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_INITIALS' });
            subjectInitialsAnswer = answer[0].get('response');

            if (subjectInitialsAnswer === subject.get('initials')) {
                answersToRemove.push(answer[0]);
            } else {
                let comment = new Answer({
                    subject_id: subject.get('subject_id'),
                    response: reason,
                    SW_Alias: 'PT.Initials.Comment',
                    questionnaire_id: this.id,
                    question_id: 'EDIT_PATIENT_INITIALS_COMMENT'
                });

                diaryAnswers.add(comment);

                // setting the initials to subject model
                subject.set('initials', subjectInitialsAnswer);

                // hacking SWAlias
                answer[0].set('SW_Alias', answer[0].get('SW_Alias').replace('.0.', '.'));
            }*/

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_LANGUAGE' });
            languageAnswer = JSON.parse(answer[0].get('response')).language;


            if (languageAnswer === user.get('language')) {
                answersToRemove.push(answer[0]);
            } else {
                let comment = new Answer({
                    subject_id: subject.get('subject_id'),
                    response: reason,
                    SW_Alias: 'Assignment.0.Language.Comment',
                    questionnaire_id: this.id,
                    question_id: 'EDIT_PATIENT_LANGUAGE_COMMENT'
                });
                diaryAnswers.add(comment);

                // setting the language to user model
                user.set('language', languageAnswer);
                answer[0].set('response', languageAnswer.replace('-', '_'));
            }
        };

    Q.all([Transmissions.fetchCollection()])
        .spread((transmissions) => {
            transmissionsToCheck = transmissions.filter((transmission) => {
                return transmission.get('status') === 'failed';
            });

            if (transmissionsToCheck) {
                transmissionsToCheck.forEach((transmission) => {
                    let transmissionMethod = transmission.get('method'),
                        parsedParams = JSON.parse(transmission.get('params'));

                    switch (transmissionMethod) {
                        case 'transmitSubjectAssignment': {
                            if (parsedParams.krpt === subject.get('krpt')) {
                                let answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' })[0].get('response'),
                                    codeLength = LF.StudyDesign.participantSettings.participantIDFormat.length;

                                if (parsedParams.patientId !== answer) {
                                    parsedParams.patientId = answer;
                                    parsedParams = JSON.stringify(parsedParams);
                                    transmission.unset('status');
                                    transmission.save({ params: parsedParams });
                                    subject.save({ subject_id: subject.get('subject_id').substr(0, codeLength) });
                                }
                            }
                            break;
                        }

                        case 'transmitEditPatient': {
                            if (parsedParams.krpt === subject.get('krpt') && !addStatus) {
                                transmission.destroy();
                            }
                            break;
                        }

                        // No default
                    }
                });
            }

            checkAndAddAnswers();
        })
        .then(() => {
            let removeAnswers = () => {
                    // answers to delete should be deleted here
                    let reasonAnswer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_REASON' });

                    if (reasonAnswer) {
                        answersToRemove.push(reasonAnswer[0]);
                    }

                    diaryAnswers.remove(answersToRemove);
                },
                customSysVars,
                subjectSaveSysVars,
                createTransmission = () => {
                    let model = new Transmission(),
                        user = LF.security.activeUser,
                        now = new Date(),
                        diaryData = _.where(diaryAnswers.toJSON(), { questionnaire_id: this.id }),
                        params = JSON.stringify({
                            krpt: subject.get('krpt'),
                            ResponsibleParty: user.get('username'),
                            dateStarted: dashboard.get('started'),
                            dateCompleted: now.ISOStamp(),
                            reportDate: dashboard.get('report_date'),
                            phase: subject.get('phase'),
                            phaseStartDate: subject.get('phaseStartDateTZOffset'),
                            sigID: `SA.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`,
                            answers: diaryData,
                            ink: this.data.ink,
                            batteryLevel: dashboard.get('battery_level')
                        }),
                        newTransmission = {
                            method: 'transmitEditPatient',
                            params,
                            created: now.getTime()
                        };

                    if (addStatus) {
                        newTransmission.status = 'failed';
                    }

                    return model.save(newTransmission);
                };

            Q().then(removeAnswers)
                .then(() => {
                    // PDE UPDATES:
                    // grab SysVars for processing
                    customSysVars = diaryAnswers.match({ question_id: 'SYSVAR' });

                    /* // apply edit comment for each sysvar update
                    // (assumes you've only added the sysvar if there was a change)
                    // TODO: adding edit comment in this manner is rejected by studyworks, find out how/if the product wants these comments added for study level additions to edit-patient
                    _(customSysVars).forEach((sysvar) => {
                        let swalias = sysvar.get('SW_Alias');

                        // only add comments for PT sysvars
                        if (/^PT\./.test(swalias)) {
                            let comment = new Answer({
                                subject_id: subject.get('subject_id'),
                                response: reason,
                                SW_Alias: `${swalias}.Comment`,
                                questionnaire_id: view.id,
                                question_id: `EDIT_PATIENT_${swalias.split('.')[1].toUpperCase()}_COMMENT`
                            });
                            diaryAnswers.add(comment);
                        }
                    });*/

                    // collect sysvar updates to apply to the subject model directly on save
                    subjectSaveSysVars = _(customSysVars).reduce((subjectJSON, answer) => {
                        let swAlias = answer ? answer.get('SW_Alias') : '';
                        if (/^PT\./.test(swAlias)) {
                            subjectJSON[swAlias.split('.')[1].toLowerCase()] = answer.get('response');
                        }
                        return subjectJSON;
                    }, {});
                })
                .then(createTransmission)
                .then(() => {
                    return subject.save(subjectSaveSysVars);
                })
                .then(() => {
                    return user.save();
                })
                .then(() => {
                    deferred.resolve(true);
                })
                .catch((e) => {
                    logger.error(`EditPatientSave removeAnswers,createTransmission failed:  ${e}`);
                    LF.spinner.hide();
                    deferred.reject(false);
                })
                .done(() => {
                    done();
                });
        })
        .catch((e) => {
            logger.error(`EditPatientSave checkAndAddAnswers failed: ${e}`);
            LF.spinner.hide();
            deferred.reject(false);
        })
        .done();

    return deferred.promise;
}

ELF.action('editPatientSave', editPatientSave);
