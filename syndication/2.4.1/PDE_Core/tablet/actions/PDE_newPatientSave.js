import Data from 'core/Data';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';
import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import CurrentContext from 'core/CurrentContext';
import { mergeObjects } from 'core/utilities/languageExtensions';

// import the core-defined newPatientSave to be 100% certain this file will override the ELF.action() registration
import 'sitepad/actions/newPatientSave';

let logger = new Logger('PDE_newPatientSave');

/**
 * @memberOf ELF.actions/sitepad
 * @method newPatientSave
 * @description
 * Saves a new Patient.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'newPatientSave' }]
 */
export function newPatientSave () {
    return Q.Promise((resolve) => {
        let site = Data.site,
            siteCode = site.get('siteCode'),
            answers = this.data.answers,
            now = new Date(),
            phase = LF.StudyDesign.subjectAssignmentPhase ||
                LF.StudyDesign.studyPhase[Object.keys(LF.StudyDesign.studyPhase)[0]],
            answer,
            subjectNumber,
            subjectId,
            subjectInitials,
            password,
            salt,
            role,
            languageId,
            createSubject,
            createUser,
            createTransmission,
            affidavit,
            krptGen,
            krpt,
            customSysVars,
            subjectModelSysVars;

        this.phase = phase;

        answer = answers.match({ question_id: 'ADD_PATIENT_ID' });
        subjectId = answer[0].get('response');
        subjectNumber = subjectId.substring(LF.StudyDesign.participantSettings.participantNumberPortion[0],
            LF.StudyDesign.participantSettings.participantNumberPortion[1]);

        answer = answers.match({ question_id: 'ADD_PATIENT_INITIALS' });
        subjectInitials = answer[0] ? answer[0].get('response') : '---';

        answer = answers.match({ question_id: 'ADD_PATIENT_PASSWORD' });
        password = JSON.parse(answer[0].get('response')).password;

        answer = answers.match({ question_id: 'ADD_PATIENT_SALT' });
        salt = JSON.parse(answer[0].get('response')).salt;

        answer = answers.match({ question_id: 'ADD_PATIENT_ROLE' });
        role = JSON.parse(answer[0].get('response')).role;

        answer = answers.match({ question_id: 'ADD_PATIENT_LANGUAGE' });
        languageId = JSON.parse(answer[0].get('response')).language;

        affidavit = answers.match({ question_id: 'AFFIDAVIT' })[0].get('SW_Alias');

        customSysVars = answers.match({ question_id: 'SYSVAR' });

        // PDE UPDATE: collect PT sysvars for the subject into json so we can add them to the new Subject model
        subjectModelSysVars = _(customSysVars).reduce((subjectJSON, answer) => {
            let swAlias = answer ? answer.get('SW_Alias') : '';
            if (/^PT\./.test(swAlias)) {
                subjectJSON[swAlias.split('.')[1].toLowerCase()] = answer.get('response');
            }
            return subjectJSON;
        }, {});

        krptGen = `${now.getFullYear()},${now.getMonth()},${now.getDate()},${now.getHours()}` +
            `,${now.getMinutes()}${now.getSeconds()},${now.getMilliseconds()}` +
            `,${now.getTimezoneOffset()},${siteCode},${subjectNumber}`;

        krpt = `SA.${(hex_sha512(krptGen)).substring(0, 33)}`;

        createSubject = () => {
            // PDE UPDATE: add PT sysvars straight to the subject model via a mergeObjects call
            let subject = new Subject(mergeObjects({
                subject_active: 1,
                subject_id: subjectId,
                initials: subjectInitials,
                subject_number: subjectNumber,
                site_code: siteCode,
                phase: this.phase,
                phaseStartDateTZOffset: DateTimeUtil.timeStamp(this.data.started),
                krpt,
                language: languageId,

                // @todo; not sure of anything below this line (dtp)
                subject_password: '',
                service_password: '',
                log_level: '',
                enrollmentDate: DateTimeUtil.timeStamp(now),
                activationDate: '',
                secret_question: null,
                secret_answer: null,
                spStartDate: DateTimeUtil.timeStamp(now)
            }, subjectModelSysVars));

            return subject.save()
                .then(() => {
                    this.subject = subject;
                    return subject;
                });
        };

        // Method for creating a user for this subject. Needed for login.
        createUser = (subject) => {
            // TODO: The user collection of the current context is cached.  It doesn't use the most up-to-date data.
            return CurrentContext().get('users').updateSubjectUser(subject, {
                userType: 'Subject',
                username: subjectNumber,
                password: hex_sha512(password + salt),
                salt,
                role,
                language: languageId,
                failedLoginAttempts: 0,
                permanentPasswordCreated: false
            });
        };

        createTransmission = () => {
            let model = new Transmission(),
                user = LF.security.activeUser,
                params = JSON.stringify({
                    krpt,
                    responsibleParty: user.get('username'),
                    dateStarted: this.data.started.ISOStamp(),
                    dateCompleted: now.ISOStamp(),
                    reportDate: DateTimeUtil.convertToDate(this.data.started),
                    subjectAssignmentPhase: this.phase,
                    phaseStartDate: DateTimeUtil.timeStamp(this.data.started),
                    sigID: `SA.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`,
                    initials: subjectInitials,
                    patientId: subjectId,
                    enrollDate: DateTimeUtil.timeStamp(this.data.started),
                    TZValue: lStorage.getItem('tzSwId'),
                    language: languageId.replace('-', '_'),
                    affidavit,
                    ink: this.data.ink,
                    batteryLevel: this.data.batteryLevel,
                    SPStartDate: DateTimeUtil.timeStamp(now),
                    customSysVars
                });

            return model.save({
                method: 'transmitSubjectAssignment',
                params,
                created: now.getTime(),
                token: ''
            });
        };

        ELF.trigger('QUESTIONNAIRE:New_Patient/DETERMINE_PHASE', {}, this)
            .then(createSubject)
            .then(createUser)
            .then(createTransmission)
            .then(() => {
                return MessageRepo.display(MessageRepo.Banner.PATIENT_ADDED);
            })
            .then(resolve)
            .catch((err) => {
                logger.error(`Action:newPatientSave failed. Error: ${err}`);

                Spinner.hide();
                LF.Actions.notify({ dialog: MessageRepo.Dialog.ERROR_PATIENT_SAVE });

                resolve({ stopActions: true });
            })
            .done();
    });
}

ELF.action('newPatientSave', newPatientSave);
