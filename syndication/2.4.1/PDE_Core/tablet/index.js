// import the PDE_Core common assets
import PDE_Core_CommonAssets from 'PDE_Core/common';

/*
 * PDE_TODO:
 * import files that don't contribute to assets object, but need to ensure the content executes
 * for example, a file that assigns a function into the global LF namespace
 */
// import './exampleBranchingFunctions';
// import './exampleUtilities';
// import './exampleScheduleFunctions';

import './expressions';
import './schedules';
import './utilities';
import './actions';
import './views';
import './widgets';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};

/*
 * PDE_TODO:
 * Collect and merge studyDesign objects
 */
// import diary from './exampleDiary';
// import messages from './exampleMessages';
// import dynamicText from './exampleDynamicText';

import SD from './study-design';
import rules from './rules';
import templates from './templates';

studyDesign = mergeObjects(studyDesign, SD, rules, templates /* , diary, dynamicText*/);

// create the PDE_Core tablet assets object
let PDE_Core_TabletAssets = { studyDesign };

// import the PDE_CoreDiaries for tablet
import PDE_CoreDiaries from './PDE_CoreDiaries';

// combine and export the assets for PDE_Core
export default mergeObjects(PDE_Core_CommonAssets, PDE_Core_TabletAssets, PDE_CoreDiaries);
