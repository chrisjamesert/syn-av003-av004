export default {
    templates: [
        {
            name: 'settingsMenuToggleAutomationButton',
            namespace: 'PDE',
            template: '<button id="toggleAutomationControls" class="btn btn-default btn-block">{{ toggleAutomation }}</button>'
        }
    ]
};
