import updateButton from './settingsUpdatebutton';
import automationButton from './settingsToggleAutomationButton';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, updateButton, automationButton);
