export default {
    rules: [
        // EditPatientQuestionnaire
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // When the Edit_Patient questionnaire is rendered, the configured fields will be populated
        // based on the selected patient.  The EditPatientQuestionnaireView resolves the selected patient (this.subject)
        // and it's associated user (this.user) model.,
        // See the Edit_Patient questionnaire configuration for which fields to populate.
        {
            id: 'EditPatientQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Edit_Patient',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'PT.0.Patientid',
                    question_id: 'EDIT_PATIENT_ID',
                    questionnaire_id: 'Edit_Patient',

                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',

                    // Use the subject model's subject_id property to populate the response.
                    // e.g. this.subject.get('subject_id');
                    property: 'subject_id'
                }, {
                    SW_Alias: 'PT.0.Initials',
                    question_id: 'EDIT_PATIENT_INITIALS',
                    questionnaire_id: 'Edit_Patient',

                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',

                    // Use the subject model's initials property to populate the response.
                    // e.g. this.subject.get('initials');
                    property: 'initials'
                }, {
                    SW_Alias: 'Assignment.0.Language',
                    question_id: 'EDIT_PATIENT_LANGUAGE',
                    questionnaire_id: 'Edit_Patient',

                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { language: 'en-US' }
                    // Indicate that the response is JSON.
                    isJSON: true,

                    // Determine the field name of the JSON response. e.g. { language: ... }
                    field: 'language',

                    // Use the user model scoped to the EditPatientQuestionnaireView (this.user).
                    model: 'user',

                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },

        // EditPatientDiaryComplete
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when attempting to navigate on the affidavit screen.
        // If the 'next' button was pressed, display a message, and save the patient edits.
        {
            id: 'EditPatientSave',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT',
            evaluate: 'isDirectionForward',

            // High importance so this rule runs before any others.
            salience: 3,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'editPatientSave' }
            ]
        },

        // EditPatientQuestionnaireTimeout
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when a questionnaire timeout occurs in the diary.
        // If the 'next' button was pressed, display a message, and save the patient edits.
        {
            id: 'EditPatientQuestionnaireTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Edit_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: 'editPatientSave'
            }, {
                action: 'updateCurrentContext'
            }, {
                action: 'removeMessage'
            }, {
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'preventAll'
            }],
            reject: [{
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // EditPatientSessionTimeout
        // NOTE: This rule is required for SPA's Edit Patient workflow.
        {
            id: 'EditPatientSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/Edit_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: 'editPatientSave'
            }, {
                action: 'logout'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }]
        },

        // EditPatientDiarySync
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when attempting to navigate on the affidavit screen.
        // If the 'next' button was pressed and the device is online,
        // transmit all queued transmissions and sync subjects.
        {
            id: 'EditPatientDiarySync',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT',
            evaluate: ['AND', 'isOnline', 'isDirectionForward'],

            // Importance below that of EditPatientDiaryComplete, so this is triggered following it.
            salience: 2,
            resolve: [{
                action () {
                    return ELF.trigger('EditPatientDiarySync:Transmit', {}, this);
                }
            }]
        },

        // EditPatientDiaryNavigate
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when attempting to navigate on the affidavit screen.
        // If the 'next' button was pressed, update the current context,
        // and then navigate to the Subject list (HomeView) view.
        {
            id: 'EditPatientDiaryNavigate',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT',
            evaluate: 'isDirectionForward',
            salience: 1,
            resolve: [
                { action: 'updateCurrentContext' },
                { action: 'removeMessage' },
                { action: 'navigateTo', data: 'home' },
                { action: 'preventDefault' }
            ]
        },

        // EditPatientCancel
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // Navigates to the Subject list (HomeView) view after the questionnaire has been cancelled.
        {
            id: 'EditPatientCancel',
            trigger: 'QUESTIONNAIRE:Canceled/Edit_Patient',
            resolve: [
                { action: 'navigateTo', data: 'home' },
                { action: 'preventDefault' }
            ]
        },

        // PDE UPDATE
        // EditPatientCustomSysvarHooks
        // NOTE: These rules should have higher salience than all rules with the 'editPatientSave' action.
        // They allow the PDE to add their sysvars before the subject model is updated and the form is transmitted
        // via an ELF hook
        {
            id: 'EditPatientCustomSysvarHook_Signed',
            trigger: [
                'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT'
            ],
            evaluate: 'isDirectionForward',
            salience: 10,
            resolve: [
                {
                    action: 'triggerPDEaddSysvar'
                }
            ]
        },
        {
            id: 'EditPatientCustomSysvarHook_Timeout',
            trigger: [
                'QUESTIONNAIRE:QuestionnaireTimeout/Edit_Patient',
                'QUESTIONNAIRE:SessionTimeout/Edit_Patient'
            ],
            evaluate: 'isAffidavitSigned',
            salience: 10,
            resolve: [
                {
                    action: 'triggerPDEaddSysvar'
                }
            ]
        }
    ]
};
