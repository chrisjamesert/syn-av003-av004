import getRoleInputType from 'core/widgets/param-functions/getRoleInputType';

export default {
    questionnaires: [{
        id: 'First_Site_User',
        SU: 'First_Site_User',
        displayName: 'FIRST_SITE_USER',
        className: 'FIRST_SITE_USER',
        affidavit: 'FirstUserSignatureAffidavit',
        product: ['sitepad', 'web'],
        screens: [
            'FIRST_SITE_USER_S_1'
        ],
        accessRoles: undefined,
        branches: []
    }],

    screens: [{
        id: 'FIRST_SITE_USER_S_1',
        className: 'FIRST_SITE_USER_S_1',
        questions: [
            { id: 'FIRST_USER_USERNAME', mandatory: true },
            { id: 'FIRST_USER_LANG', mandatory: true },
            { id: 'FIRST_USER_SECRET_QUESTION', mandatory: true },
            { id: 'FIRST_USER_SECRET_ANSWER', mandatory: true },
            { id: 'FIRST_USER_PASSWORD', mandatory: true },
            { id: 'FIRST_USER_PASSWORD_CONFIRM', mandatory: true },
            { id: 'FIRST_USER_ROLE', mandatory: true }
        ]
    }],

    questions: [{
        id: 'FIRST_USER_USERNAME',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_USERNAME',
        skipIT: '',
        title: '',
        text: [],
        className: 'FIRST_SITE_USER',
        widget: {
            id: 'ADD_USER_W_1',
            type: 'AddUserTextBox',
            label: 'FULL_NAME',
            templates: {},
            answers: [],
            field: 'username',
            maxLength: 25,
            allowedKeyRegex: /[\w\W ]/,
            validateRegex: /[\w\W ]{3,}/,
            validation: {
                validationFunc: 'checkUserNameField',
                params: {
                    maxLength: 25,
                    minLength: 3
                }
            }
        }
    }, {
        id: 'FIRST_USER_LANG',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_LANG',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'FIRST_USER_W_2',
            type: 'PDE_SiteLanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: []
        }
    }, {
        id: 'FIRST_USER_SECRET_QUESTION',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_SECRET_QUESTION',
        skipIT: '',
        title: '',
        text: [],
        className: 'FIRST_USER',
        widget: {
            id: 'ADD_USER_W_3',
            type: 'SecretQuestionList',
            label: 'SECRET_QUESTION',
            field: 'secretQuestion',
            clearOnChange: ['ADD_USER_W_4'],
            templates: {},
            answers: []
        }
    }, {
        id: 'FIRST_USER_SECRET_ANSWER',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_SECRET_ANSWER',
        skipIT: '',
        title: '',
        text: [],
        className: 'FIRST_SITE_USER',
        widget: {
            id: 'ADD_USER_W_4',
            type: 'SecretAnswerTextBox',
            label: 'SECRET_ANSWER',
            templates: {},
            field: 'secretAnswer'
        }
    }, {
        id: 'FIRST_USER_PASSWORD',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_PASSWORD',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'FIRST_USER_W_5',
            type: 'TempPasswordTextBox',
            label: 'PASSWORD',
            templates: {
                input: 'PasswordTextBox'
            },
            answers: [],
            field: 'password',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'checkPasswordFieldWidget',
                params: {
                }
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: true
            }
        }
    }, {
        id: 'FIRST_USER_PASSWORD_CONFIRM',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_PASSWORD_CONFIRM',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'FIRST_USER_W_6',
            type: 'ConfirmationTextBox',
            label: 'CONFIRM_PASSWORD',
            templates: {
                input: 'PasswordTextBox'
            },
            answers: [],
            field: 'confirm',
            fieldToConfirm: 'FIRST_USER_W_5',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'confirmFieldValue',
                params: {
                    errorString: 'PASSWORD_MISMATCH'
                }
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: true
            }
        }
    }, {
        id: 'FIRST_USER_ROLE',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_ROLE',
        className: 'FIRST_SITE_USER',
        title: '',
        text: [],
        widget: {
            id: 'ADD_USER_W_7',
            type: 'HiddenField',
            value: 'admin',
            field: 'role'
        }
    }]
};
