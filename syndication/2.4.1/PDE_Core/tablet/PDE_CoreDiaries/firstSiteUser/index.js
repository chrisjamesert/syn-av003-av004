/*
* PDE_TODO:
* import files that don't contribute to assets object, but need to ensure the content executes
* for example, a file that assigns a function into the global LF namespace
*/
// import './exampleBranchingFunctions';
// import './exampleUtilities';
// import './exampleScheduleFunctions';

import { mergeObjects } from 'core/utilities/languageExtensions';
let studyDesign = {};

/*
* PDE_TODO:
* Collect and merge studyDesign objects
*/
// import rules from './exampleRules';
// import messages from './exampleMessages';
// import templates from './exampleTemplates';
// import dynamicText from './exampleDynamicText';

import diary from './first-site-user';

studyDesign = mergeObjects(studyDesign, diary /* , rules, templates, dynamicText*/);


/* export the assets object*/
export default { studyDesign };
