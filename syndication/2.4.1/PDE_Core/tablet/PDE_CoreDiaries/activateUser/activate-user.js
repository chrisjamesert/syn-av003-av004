// This is a core questionnaire required for the Aactivate User workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Aeactivate_User)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [{
        id: 'Activate_User',
        SU: 'Activate_User',
        displayName: 'ACTIVATE_USER',
        className: 'ACTIVATE_USER',

        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'ActivateUserAffidavit',
        screens: ['ACTIVATE_USER_S_1'],
        branches: [],

        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],

        // This questionnaire can only be accessed by a Site Administrator.
        accessRoles: ['admin']
    }],

    screens: [{
        id: 'ACTIVATE_USER_S_1',
        className: 'ACTIVATE_USER_S_1',

        // Disable the back button on this screen to prevent a questionnaire back out.
        disableBack: true,
        questions: [
            { id: 'ACTIVATE_USER_USERNAME', mandatory: true },
            { id: 'ACTIVATE_USER_ROLE', mandatory: true },
            { id: 'ACTIVATE_USER_LANG', mandatory: false }
        ]
    }],

    questions: [{
        id: 'ACTIVATE_USER_USERNAME',
        IG: 'Activate_User',
        IT: 'ACTIVATE_USER_1',
        skipIT: '',
        title: '',
        text: ['ACTIVATE_USER_CONFIRM_INFORMATION'],
        className: 'ACTIVATE_USER',
        widget: {
            id: 'ACTIVATE_USER_W_1',
            type: 'StaticField',
            label: 'FULL_NAME',
            field: 'username'
        }
    }, {
        id: 'ACTIVATE_USER_ROLE',
        IG: 'Activate_User',
        IT: 'ACTIVATE_USER_2',
        skipIT: '',
        title: '',
        text: [],
        className: 'ROLE',
        widget: {
            id: 'ACTIVATE_USER_W_2',
            type: 'StaticRoleField',
            label: 'ROLES',
            field: 'role'
        }
    }, {
        id: 'ACTIVATE_USER_LANG',
        IG: 'Activate_User',
        IT: 'ACTIVATE_USER_3',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'ACTIVATE_USER_W_3',
            type: 'StaticLanguageField',
            label: 'LANGUAGE',
            field: 'language'
        }
    }],

    affidavits: [{
        id: 'ActivateUserAffidavit',
        text: ['ACTIVATE_USER_AFF', 'USER_TO_ACTIVATE'],
        krSig: 'SubmitForm',
        widget: {
            id: 'SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox'
        }
    }],

    rules: [

        // ActivateUserOpen
        // This rule is required for SPA's Activate User workflow.
        // Prior to the Activate_User questionnaire openning, this rule is evaluated.
        // If the user cannot access the questionnaire, a notification is displayed.
        {
            id: 'ActivateUserOpen',
            trigger: 'USERMANAGEMENT:ActivateUser',
            evaluate: { expression: 'canAccessQuestionnaire', input: 'Activate_User' },
            reject: [{
                action: 'notify',
                data: { key: 'INVALID_PERMISSION' }
            }, {
                action: 'preventDefault'
            }]
        },

        // ActivateUserQuestionnaire
        // This rule is required for SPA's Activate User workflow.
        // When the Activate_User questionnaire is rendered, configured fields will be populated
        // based on the selected user.  The ActivateUserQuestionnaire view resolves the selected user,
        // assigning it to this.user.  See the Activate_User questionnaire configuration for which fields to populate.
        {
            id: 'ActivateUserQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Activate_User',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'Activate_User.0.ACTIVATE_USER_1',
                    question_id: 'ACTIVATE_USER_USERNAME',
                    questionnaire_id: 'Activate_User',

                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { username: 'jsmith' }
                    // Indicate that the response is JSON.
                    isJSON: true,

                    // Determine the field name of the JSON response. e.g. { username: ... }
                    field: 'username',

                    // Use the user model scoped to the ActivateUserQuestionnaireView (this.user).
                    model: 'user',

                    // Use the user model's username property to populate the response.
                    // e.g. this.user.get('username');
                    property: 'username'
                }, {
                    SW_Alias: 'Activate_User.0.ACTIVATE_USER_2',
                    question_id: 'ACTIVATE_USER_ROLE',
                    questionnaire_id: 'Activate_User',

                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { role: 'site' }
                    // Indicate that the response is JSON.
                    isJSON: true,

                    // Determine the field name of the JSON response. e.g. { role: ... }
                    field: 'role',

                    // Use the user model scoped to the ActivateUserQuestionnaireView (this.user).
                    model: 'user',

                    // Use the user model's role property to populate the response.
                    // e.g. this.user.get('role');
                    property: 'role'
                }, {
                    SW_Alias: 'Activate_User.0.ACTIVATE_USER_3',
                    question_id: 'ACTIVATE_USER_LANG',
                    questionnaire_id: 'Activate_User',

                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { language: 'en-US' }
                    // Indicate that the response is JSON.
                    isJSON: true,

                    // Determine the field name of the JSON response. e.g. { language: ... }
                    field: 'language',

                    // Use the user model scoped to the ActivateUserQuestionnaireView (this.user).
                    model: 'user',

                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },

        // ActivateUserOfflineSyncCheck
        // This rule is required for the SPA's activate user workflow.
        // After clicking the next button on the first screen, if the device is online,
        // and the user record being activated is set to not allow offline sync,
        // display a connection required banner.
        {
            id: 'ActivateUserOfflineSyncCheck',
            trigger: 'QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1',

            // canSyncOffline is an expression that exists on the context of the questionnaire.
            // see UserStatusQuestionnaireView.js.
            evaluate: ['AND', 'isDirectionForward', '!isOnline', '!canSyncOffline'],
            resolve: [{
                action: 'displayBanner',
                data: 'CONNECTION_REQUIRED'
            }, {
                action: 'preventDefault'
            }]
        },

        // ActivateUserCompleted
        // This rule is required for the SPA's activate user workflow.
        // Upon signing the affidavit, instead of navigating to the QuestionnaireCompletionView,
        // execute the activateUser action, navigate to the Manage Site Users view (SiteUserView), and prevent other rules from executing.
        {
            id: 'ActivateUserCompleted',
            trigger: 'QUESTIONNAIRE:Navigate/Activate_User/AFFIDAVIT',
            evaluate: 'isDirectionForward',

            // Set this rule to a high importance, preventing other rules from executing before it.
            salience: 2,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Activates the user, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserStatus',
                data: { active: 1 }
            }, {
                action () {
                    return ELF.trigger('ActivateUserCompleted:Transmit', {}, this);
                }
            }, {
                action: 'updateCurrentContext'
            }, {
                // Remove the loading message.
                action: 'removeMessage'
            }, {
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }],

            // Prevent the default functionality from executing.
            // This prevents a dashboard record from being written to the database.
            reject: [{ action: 'preventDefault' }]
        },

        // ActivateUserCompletedTimeout
        // This rule is required for the SPA's activate user workflow.
        // Upon signing the affidavit, instead of navigating to the QuestionnaireCompletionView,
        // execute the activateUser action, navigate to the Manage Site Users view (SiteUserView), and prevent other rules from executing.
        {
            id: 'ActivateUserCompletedTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Activate_User',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Activates the user, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserStatus',
                data: { active: 1 }
            }, {
                // Remove the loading message.
                action: 'removeMessage'
            }, {
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }],
            reject: [{
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // ActivateUserSessionTimeout
        // NOTE: This rule is required for SPA's Activate User workflow.
        {
            id: 'ActivateUserSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/Activate_User',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Activates the user, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserStatus',
                data: { active: 1 }
            }, {
                action: 'logout'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }]
        },

        // ActivateUserAffidavitBackout
        // This rule is required for the Activate User workflow.
        // Due to the ActivateUserCompleted rule, we need this rule to
        // reinstate the back button behavior on the affidavit screen of the questionnaire.
        {
            id: 'ActivateUserAffidavitBackout',

            // Trigger only on a navigation event on the affidavit screen of the Activate User questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/Activate_User/AFFIDAVIT',

            // If the back button was pressed, evaluate as true.
            evaluate: 'isDirectionBackward',

            // Set the importance of this rule below that of ActivateUserCompleted.
            salience: 1,

            // Trigger the navigation handler of the BaseQuestionnaireView to navigate back to the previous screen.
            resolve: [{ action: 'navigationHandler', data: 'back' }]
        },

        // ActivateUserCancel
        // This rule is required for the SPA's activate user workflow.
        // When attempting to cancel the Activate_User workflow, a confirmation dialog will appear.
        // Clicking 'Cancel' will cancel out the form and navigate to the Manage Site Users (SiteUsersView) view.
        // Clicking 'No' will close the dialog.
        {
            id: 'ActivateUserCancel',
            trigger: 'QUESTIONNAIRE:Canceled/Activate_User',
            resolve: [
                { action: 'navigateTo', data: 'site-users' },

                // Prevent the default action, which is to navigate to the dashboard view.
                { action: 'preventDefault' }
            ]
        }
    ]
};
