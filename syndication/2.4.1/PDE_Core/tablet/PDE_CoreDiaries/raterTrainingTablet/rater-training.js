import getRaterTrainingURL from 'core/widgets/param-functions/getRaterTrainingURL';

// This is a core questionnaire required for the Skip Visit workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Skip_Visits)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [
        {
            id: 'Rater_Training',
            SU: 'Rater_Training',
            displayName: 'RATER_TRAINING',
            className: 'RaterTraining',
            affidavit: undefined,
            screens: ['RATER_TRAINING_S_1']
        }
    ],

    screens: [
        {
            id: 'RATER_TRAINING_S_1',
            className: 'RATER_TRAINING_S_1',
            questions: [
                { id: 'RATER_TRAINING_Q_1', mandatory: true }
            ]
        }
    ],

    questions: [
        {
            id: 'RATER_TRAINING_Q_1',
            IG: 'RaterTraining',
            IT: 'RATER_TRAINING_Q1',
            text: 'RATER_TRAINING_QUESTION_1',
            className: 'RATER_TRAINING_Q_1',
            widget: {
                id: 'RATER_TRAINING_W_1',
                type: 'Browser',
                className: 'SKIP_VISIT',
                linkText: 'RATER_TRAINING_LINK',
                url: () => {
                    return getRaterTrainingURL('default');
                },
                browserCloseURLPattern: /goodbye.html$/
            }
        }
    ],

    /*
                id: 'ActivateUserOfflineSyncCheck',
            trigger: 'QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1',
            // canSyncOffline is an expression that exists on the context of the questionnaire.
            // see UserStatusQuestionnaireView.js.
            evaluate: ['AND', 'isDirectionForward', '!isOnline', '!canSyncOffline'],
    */
    rules: [
        {
            id: 'RaterTrainingDiaryStart',
            trigger: 'QUESTIONNAIRE:Open/Rater_Training',
            evaluate: true,
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: 'getRaterTraining'
            }, {
                action: 'removeMessage'
            }]
        }]
};
