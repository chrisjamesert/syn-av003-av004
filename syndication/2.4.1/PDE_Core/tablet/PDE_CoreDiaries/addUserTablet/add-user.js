import getRoleInputType from 'core/widgets/param-functions/getRoleInputType';

export default {
    questionnaires: [{
        id: 'New_User_SitePad',
        SU: 'New_User',
        displayName: 'NEW_USER',
        className: 'ADD_USER',
        affidavit: 'NewUserAffidavit',
        product: ['sitepad'],
        screens: [
            'ADD_USER_S_1',
            'ADD_USER_S_2'
        ],
        branches: [],
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'ADD_USER_S_1',
        className: 'ADD_USER_S_1',
        questions: [
            { id: 'ADD_USERNAME', mandatory: true },
            { id: 'ADD_USER_ROLE', mandatory: true },
            { id: 'ADD_USER_LANG', mandatory: true },
            { id: 'ADD_USER_PASSWORD', mandatory: true },
            { id: 'ADD_USER_PASSWORD_CONFIRM', mandatory: true },
            { id: 'ADD_USER_SALT', mandatory: true }
        ]
    }, {
        id: 'ADD_USER_S_2',
        className: 'ADD_USER_S_2',
        questions: [
            { id: 'ADD_USER_PASSWORD_MESSAGE', mandatory: false }
        ]
    }],

    questions: [{
        id: 'ADD_USERNAME',
        IG: 'Add_User',
        IT: 'ADD_USER_1',
        skipIT: '',
        title: '',
        text: [],
        className: 'ADD_USER',
        widget: {
            id: 'ADD_USER_W_1',
            type: 'AddUserTextBox',
            label: 'FULL_NAME',
            templates: {},
            answers: [],
            field: 'username',
            maxLength: 20,
            allowedKeyRegex: /[\w\W ]/,
            validateRegex: /[\w\W ]{3,}/,
            validation: {
                validationFunc: 'checkUserNameField',
                params: {
                    maxLength: 20,
                    minLength: 3
                }
            }
        }
    }, {
        id: 'ADD_USER_ROLE',
        IG: 'Add_User',
        IT: 'ADD_USER_2',
        skipIT: '',
        title: '',
        text: [],
        className: 'ROLE',
        widget: {
            id: 'ADD_USER_W_2',
            type: 'RoleSelectWidget',
            label: 'ROLES',
            field: 'role',
            templates: {},
            answers: [],
            params: {
                language: 'LANGUAGE',
                password: 'PASSWORD'
            },
            validation: {
                validationFunc: 'checkRoleFieldWidget',
                params: {
                    errorString: 'CONNECTION_REQUIRED'
                }
            }
        }
    }, {
        id: 'ADD_USER_LANG',
        IG: 'Add_User',
        IT: 'ADD_USER_3',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'ADD_USER_W_3',
            type: 'PDE_SiteLanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: [],
            validation: {
                validationFunc: 'checkLanguageFieldWidget',
                params: {}
            }

        }
    }, {
        id: 'ADD_USER_PASSWORD',
        IG: 'Add_User',
        IT: 'ADD_USER_4',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_USER_W_4',
            type: 'TempPasswordTextBox',
            label: 'TEMP_PASSWORD',
            templates: {},
            answers: [],
            disabled: true,
            field: 'password',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'checkPasswordFieldWidget',
                params: {}
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: false
            }
        }
    }, {
        id: 'ADD_USER_PASSWORD_CONFIRM',
        IG: 'Add_User',
        IT: 'ADD_USER_5',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_USER_W_5',
            type: 'ConfirmationTextBox',
            label: 'CONFIRM_TEMP_PASS',
            templates: {},
            answers: [],
            disabled: true,
            field: 'confirm',
            fieldToConfirm: 'ADD_USER_W_4',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'confirmFieldValue',
                params: {
                    errorString: 'PASSWORD_MISMATCH'
                }
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: false
            }
        }
    }, {
        id: 'ADD_USER_PASSWORD_MESSAGE',
        IG: 'Add_User',
        IT: 'ADD_USER_6',
        skipIT: '',
        title: '',
        text: [],
        className: 'ADD_USER',
        widget: {
            id: 'ADD_USER_W_6',
            type: 'PasswordMessageWidget',
            templates: {},
            answers: [],
            text: {
                tempPasswordMessage: 'TEMPORARY_PASSWORD_MESSAGE',
                tempPassword: 'TEMP_PASSWORD',
                role: 'ROLES',
                language: 'LANGUAGE',
                userName: 'USER_NAME'
            }
        }
    }, {
        id: 'ADD_USER_SALT',
        IG: 'Add_User',
        IT: 'ADD_USER_SALT',
        className: 'ADD_USER_SALT',
        title: '',
        text: [],
        widget: {
            id: 'ADD_USER_W_7',
            type: 'HiddenField',
            value: 'temp',
            field: 'salt'
        }
    }]
};
