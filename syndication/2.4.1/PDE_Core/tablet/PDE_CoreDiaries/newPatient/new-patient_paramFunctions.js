/**
 * getProtocolListDropdown populates the protocol select dropdown based on the configurables from
 * StudyDesign.studyProtocol.protocolList
 * Text displays correctly thanks to the studyrule id: 'LoadProtocolStrings'
 * @returns {Object[]} array of dropdownlist item configs built from protocolList
 */
export function getProtocolListDropdown () {
    let protocolList = LF.StudyDesign.studyProtocol.protocolList,
        itemsArray = [];
    for (let protVal in protocolList) {
        let item = {};
        item.value = `${protVal}`;
        item.text = `${protocolList[protVal]}`;
        itemsArray.push(item);
    }
    itemsArray = _(itemsArray).sortBy(item => Number(item.value));
    return itemsArray;
}

export function getProtocolDefaultValue () {
    let showDefault = LF.StudyDesign.prepopulateDefaultProtocol;
    if (showDefault) {
        let defaultProt = LF.StudyDesign.studyProtocol.defaultProtocol.toString();
        return Q(defaultProt);
    }
    return Q(-1);
}

export function getAllProtocolValues () {
    let showDefault = LF.StudyDesign.prepopulateDefaultProtocol;
    if (showDefault) {
        return Q(Object.keys(LF.StudyDesign.studyProtocol.protocolList));
    }
    return Q(-1);
}
