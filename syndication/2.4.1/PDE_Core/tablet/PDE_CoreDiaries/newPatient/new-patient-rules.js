export default {
    rules: [
        // NewPatientCancel
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // This rule ensures that when the questionnaire is cancelled, the correct view is displayed.
        {
            id: 'NewPatientCancel',
            trigger: 'QUESTIONNAIRE:Canceled/New_Patient',
            resolve: [
                // Instead of navigating to 'dashboard', we want to navigate to 'home'.
                { action: 'navigateTo', data: 'home' },

                // Prevent the default navigate call.
                { action: 'preventDefault' }
            ]
        },

        // QuestionnaireTransmitNewPatient
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // This rule ensures that the newly created patient is transmitted after the questionnaire is completed.
        //
        {
            id: 'QuestionnaireTransmitNewPatient',
            trigger: 'QUESTIONNAIRE:Transmit/New_Patient',
            salience: 1,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'navigateTo', data: 'home' },
                { action: 'removeMessage' },
                { action: 'preventDefault' }
            ]
        },

        // NewPatientAffidavitBackout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // Due to the NewPatientSave we need this rule to
        // reinstate the back button behavior on the affidavit screen of the questionnaire.
        {
            id: 'NewPatientAffidavitBackout',

            // Trigger only on a navigation event on the affidavit screen of the New Patient questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT',

            // If the back button was pressed, evaluate as true.
            evaluate: 'isDirectionBackward',

            // Set the importance of this rule below that of NewPatientSave.
            salience: 2,

            // Trigger the navigation handler of the BaseQuestionnaireView to navigate back to the previous screen.
            resolve: [
                { action: 'navigationHandler', data: 'back' },
                { action: 'preventDefault' }
            ]
        },

        // NewPatientSave
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // After the user clicks 'Next' on the affidavit screen, save the new patient
        // and queue a transmission record to transmit the patient.
        {
            id: 'NewPatientSave',
            trigger: 'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT',

            // Only resolved on 'next' click.
            evaluate: 'isDirectionForward',
            salience: 1,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' }
            ]
        },

        // NewPatientSaveOnTimeout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        {
            id: 'NewPatientQuestionnaireTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/New_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' },
                { action: 'removeMessage' },
                { action: 'navigateTo', data: 'home' },
                { action: 'preventAll' }
            ],
            reject: [{
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // NewPatientSaveOnTimeout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        {
            id: 'NewPatientSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/New_Patient',
            salience: 3,
            evaluate: 'isAffidavitSigned',
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' },
                { action: 'removeMessage' }
            ]
        },

        // NewPatientPreventNavigation
        // NOTE: This rule is required for the New Patient workflow
        {
            id: 'NewPatientDefect',
            trigger: 'NAVIGATE:application/addNewPatient',
            evaluate: 'isQuestionnaireCompleted',
            resolve: [{ action: 'preventDefault' }]
        },

        // PDE UPDATE
        // NewPatientCustomSysVarHooks
        // NOTE: These rules should have higher salience than all rules with the 'editPatientSave' action.
        // They allow the PDE to add their sysvars before the subject model is updated and the form is transmitted
        // via an ELF hook
        {
            id: 'NewPatientCustomSysvarHook_Signed',
            trigger: [
                'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT'
            ],
            evaluate: 'isDirectionForward',
            salience: 10,
            resolve: [
                {
                    action: 'triggerPDEaddSysvar'
                }
            ]
        },
        {
            id: 'NewPatientCustomSysvarHook_Timeout',
            trigger: [
                'QUESTIONNAIRE:QuestionnaireTimeout/New_Patient',
                'QUESTIONNAIRE:SessionTimeout/New_Patient'
            ],
            evaluate: 'isAffidavitSigned',
            salience: 10,
            resolve: [
                {
                    action: 'triggerPDEaddSysvar'
                }
            ]
        },
        {
            id: 'NewPatient_addProtocol',
            trigger: 'QUESTIONNAIRE:PDEaddSysvar/New_Patient',
            salience: 10,
            resolve: [
                {
                    action: 'newPatientAddProtocol'
                }
            ]
        }
    ]
};
