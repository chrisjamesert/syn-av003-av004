import activateUser from './activateUser';
import addUserTablet from './addUserTablet';
import deactivateuser from './deactivateUser';
import editPatient from './editPatient';
import editUser from './editUser';
import firstSiteUser from './firstSiteUser';
import newPatient from './newPatient';
import raterTrainingTablet from './raterTrainingTablet';
import skipVisit from './skipVisit';
import timeConfTablet from './timeConfTablet';
import transReportTablet from './transReportTablet';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(activateUser, addUserTablet, deactivateuser, editPatient, editUser, firstSiteUser, newPatient, raterTrainingTablet, skipVisit, timeConfTablet, transReportTablet);
