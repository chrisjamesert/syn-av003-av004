import ELF from 'core/ELF';

export default function execActivationSync () {
    return Q(Boolean(localStorage.getItem('PDE_ActivationSync')));
}

ELF.expression('execActivationSync', execActivationSync);
