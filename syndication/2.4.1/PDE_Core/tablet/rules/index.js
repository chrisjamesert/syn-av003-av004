import { mergeObjects} from 'core/utilities/languageExtensions';

import aLoadRules from './ApplicationLoadedRules';
import syncRules from './PDE_SyncRules_Tablet';

export default mergeObjects({}, aLoadRules, syncRules);
