export default {
    rules: [
        {
            id: 'PrepActivationFullSync',
            trigger: ['MODESELECT:Submit'],
            evaluate: 'doFullSync',
            resolve: [
                {
                    action (input) {
                        return Q().then(() => {
                            if (input.product === 'sitepad' && input.value === 'provision') {
                                // a provisioned device will need to perform a full sync in case of replacement
                                localStorage.setItem('PDE_ActivationSync', 'true');
                                return;
                            }

                            // make sure if provision is not the chosen mode, we remove the flag for full sync
                            localStorage.removeItem('PDE_ActivationSync');
                        });
                    }
                }
            ]
        },
        {
            id: 'TriggerLoginFullSync',
            trigger: [
                'NAVIGATE:application/login'
            ],
            evaluate: [
                'AND',
                'isOnline',
                'isStudyOnline',
                ['OR', 'execActivationSync', { expression: 'checkNeedToSync', input: 'getFullSyncData' }],
                '!isTrainer',
                'doFullSync'
            ],
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'transmitAll' },
                {
                    action: () => {
                        // @FIXME: This is a workaround for the delay between transmission of a diary and the diary
                        // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                        return Q.delay(1000);
                    }
                },
                { action: 'refreshAllData' },
                { action: 'transmitLogs' },
                { action: 'updateCurrentContext' },

                // PDE added rule, must run this late to ensure we have populated subjects
                {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getFullSyncData'
                    }
                },
                { action: 'terminationCheck' },
                { action: 'removeMessage' }
            ]
        },
        {
            id: 'TabletManualUpdateWithFullSyncTrigger',
            trigger: [
                'PDE:UpdateTablet',
                'RETRY_TRANSMISSION'
            ],
            evaluate: [
                'AND',
                'isOnline',
                'isStudyOnline',
                '!isTrainer',
                'doFullSync'
            ],
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'transmitAll' },
                {
                    action: () => {
                        // @FIXME: This is a workaround for the delay between transmission of a diary and the diary
                        // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                        return Q.delay(1000);
                    }
                },
                { action: 'refreshAllData' },
                { action: 'transmitLogs' },
                { action: 'updateCurrentContext' },

                // PDE added rule, must run this late to ensure we have populated subjects
                {
                    action () {
                        return ELF.trigger('PDE:RunManualFullSync', {}, this);
                    }
                },
                { action: 'terminationCheck' },
                { action: 'removeMessage' }
            ],
            reject: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                {
                    action () {
                        return Q.delay(1000);
                    }
                },
                { action: 'removeMessage' }
               /*{
                    action: 'retryTransmission',
                    data (input, done) {
                        input.message = input.isOnline ? 'TRANSMIT_RETRY_FAILED' : 'TRANSMIT_RETRY_CONNECTION_REQUIRED';

                        done(input);
                    }
                }*/
            ]
        },
        {
            id: 'TabletFullSyncManualUpdate',
            trigger: [
                'PDE:RunManualFullSync'
            ],
            evaluate: { expression: 'setNeedToSyncIfPending', input: 'getFullSyncData' },
            resolve: [
                {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getFullSyncData'
                    }
                }
            ]
        },
        {
            id: 'ManualPartialSync',
            trigger: [
                'HOME:Transmit',
                'VISITGATEWAY:Transmit',
                'FORMGATEWAY:Transmit',
                'LOGIN:Transmit',
                'USERMANAGEMENT:Transmit'
            ],
            evaluate: ['AND', 'isOnline', 'isStudyOnline', '!isTrainer', 'doPartialSync'],
            salience: 50,
            resolve: [
                {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getPartialSyncData'
                    }
                }
            ]
        },
        {
            id: 'AutomaticPartialSync',
            trigger: [
                'NAVIGATE:application/visitGateway',
                'NAVIGATE:application/dashboard'
            ],
            evaluate: ['AND', 'isOnline', 'isStudyOnline', '!isTrainer', 'doPartialSync'],
            salience: 50,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                {
                    action: 'customSyncDataRequest',
                    data: {
                        webServiceFunction: 'getPartialSyncData'
                    }
                },
                { action: 'removeMessage' }
            ]
        }
    ]
};
