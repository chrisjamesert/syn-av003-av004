import COOL from 'core/COOL';
import SettingsView from 'sitepad/views/SettingsView';

// This function is imported for a study rule, so it doesn't require an index import.

/**
 * @function extendSettingsViewPDE
 * Extends SettingsView for the following purposes:
 * 1) Adds Update button, to execute transmissions and fullsync.
 * 2) Adds Toggle Automation button, to show/hide automation controls.
 */
export default function extendSettingsViewPDE () {
    /**
     * @class PDESettingsView
     * A class that extends the sitepad SettingsView view.
     */
    class PDESettingsView extends COOL.getClass('SettingsView', SettingsView) {

        constructor (options) {
            super(options);
        }

        /**
         * @method render
         * Overides the settings template and render the view to the DOM
         * @returns {Q.Promise<void>}
         */
        render () {

            let settingsTemplate = $('<div>').append($('#settings-template').html()),
                buttonContainer = settingsTemplate.find('#button-container');

            return LF.getStrings('CHECK_FOR_UPDATES')
            .then((string) => {
                let buttonHTML = LF.templates.display('PDE:settingsMenuUpdateButton', { update: string });

                if (!buttonContainer.find('#update-button').length) {
                    buttonContainer.append(buttonHTML).html();
                    $('#settings-template').html(settingsTemplate.html());
                }

                this.events['click #update-button'] = 'updateTablet';
            })
            .then(() => {
                return LF.getStrings('PDE_AUTOMATION_TOGGLE_AUTOMATION');
            })
            .then((toggleAutomation) => {
                // Prevent usage on any environment other than Dev, staging or trainer
                if (!PDE.Automation.allowPDEAutomation) {
                    return;
                }
                let buttonHTML = LF.templates.display('PDE:settingsMenuToggleAutomationButton', { toggleAutomation });

                if (!buttonContainer.find('#toggleAutomationControls').length) {
                    buttonContainer.append(buttonHTML).html();
                    $('#settings-template').html(settingsTemplate.html());
                }

                this.events['click #toggleAutomationControls'] = 'toggleAutomationControls';
            })
            .then(() => {
                return super.render();
            });
        }

        /**
         * @method exitTrainerMode
         * Handles the update button click event
         */
        updateTablet () {
            ELF.trigger('PDE:UpdateTablet', {}, this)
                .done();
        }

        toggleAutomationControls () {
            PDE.Automation.toggleAutomationControls();
        }

    }

    COOL.add('SettingsView', PDESettingsView);
}
