import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import { evaluateBranching } from 'core/branching/evaluateBranching';
import SitePadQuestionnaireView from 'sitepad/views/SitePadQuestionnaireView';

let logger = new Logger('PDE_SitePadQuestionnaireView');

/**
 * updates the render function of SitePadQuestionnaireView to include an initialScreen branching check
 */
export default class PDE_SitePadQuestionnaireView extends COOL.getClass('SitePadQuestionnaireView', SitePadQuestionnaireView) {
    render (id) {
        this.templateStrings = {
            header: {
                namespace: this.model.get('id'),
                key: this.model.get('displayName')
            },
            back: 'BACK',
            next: 'NEXT'
        };

        if (this.showCancel) {
            this.templateStrings.cancel = 'CANCEL';
        } else {
            this.templateStrings.cancel = '';
        }

        return this.buildHTML({
            className: this.model.get('className')
        })
            .then(() => {
                this.page();

                this.disableButton(this.$('#nextItem'));
                this.disableButton(this.$('#prevItem'));

                // Hide the cancel button, if appropriate:
                if (!this.showCancel) {
                    this.hideCancelButton();
                }
            })
            .then(() => ELF.trigger(`QUESTIONNAIRE:Rendered/${this.id}`, { questionnaire: this.id }, this))
            .then(() => { // PDE UPDATE: add initial branching evaluation
                let firstDefinedScreen = this.data.screens[0].get('id'),
                    initScreenObjs = _(this.model.get('initialScreen') || []).forEach(obj => obj.branchFrom = firstDefinedScreen);
                return evaluateBranching(initScreenObjs, firstDefinedScreen, this.answers, this)
                    .then((result) => {
                        if (result && result.branchTo) {
                            return result.branchTo;
                        }
                        return firstDefinedScreen;
                    });
            })
            .then((initScreen) => {
                return this.displayScreen(id || initScreen).then(() => initScreen);
            })
            .then((initScreen) => {
                if (!id) {
                    this.screenStack.push(initScreen);
                }
                this.delegateEvents();
            })
            .catch((e) => {
                if (e.doNotLog) {
                    return Q();
                }

                logger.error('From render.QUESTIONNAIRE:Rendered: ', e);
                return Q.reject(e);
            });
    }
}

COOL.add('SitePadQuestionnaireView', PDE_SitePadQuestionnaireView);

