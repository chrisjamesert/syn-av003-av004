import { getNested } from 'core/utilities/coreUtilities';
import trialAssets from 'trial/handheld/index';

import altConfigs from 'trial/test/conf/integratedTestConfigs/HH_Configs';

import { validateITs } from 'PDE_Core/test/specs/integration/common/db/validateITs.shared.partialspec';

let studyDesign = getNested('assets.studyDesign', trialAssets);

let excludeITs = [];
if (altConfigs.excludeITs.length) {
    excludeITs = _.union(excludeITs, altConfigs.excludeITs);
}
let includeITs = [];
if (altConfigs.includeITs.length) {
    includeITs = _.union(includeITs, altConfigs.includeITs);
}
let excludeIGs = [];
if (altConfigs.excludeIGs.length) {
    excludeIGs = _.union(excludeIGs, altConfigs.excludeIGs);
}

let configs = {
    user: altConfigs.user || 'pdereadonly',
    password: altConfigs.password || 'pdereadonly2018',
    environment: 'staging',
    excludeITs,
    includeITs,
    excludeIGs
};

describe('validate_ITs.staging', () => {
    validateITs(studyDesign, configs);
});
