import { getNested } from 'core/utilities/coreUtilities';
import trialAssets from 'trial/handheld/index';

import altConfigs from 'trial/test/conf/integratedTestConfigs/HH_Configs';

import { validateSUs } from 'PDE_Core/test/specs/integration/common/db/validateSUs.shared.partialspec';

let studyDesign = getNested('assets.studyDesign', trialAssets);

let excludeSUs = [];
if (altConfigs.excludeSUs.length) {
    excludeSUs = _.union(excludeSUs, altConfigs.excludeSUs);
}
let includeSUs = [];
if (altConfigs.includeSUs.length) {
    includeSUs = _.union(includeSUs, altConfigs.includeSUs);
}

let configs = {
    user: altConfigs.user || 'pdereadonly',
    password: altConfigs.password || 'pdereadonly2018',
    environment: 'staging',
    excludeSUs,
    includeSUs
};

describe('validate_SUs.staging', () => {
    validateSUs(studyDesign, configs);
});
