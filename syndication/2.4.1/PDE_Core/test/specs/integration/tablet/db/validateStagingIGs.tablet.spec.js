import { getNested } from 'core/utilities/coreUtilities';
import trialAssets from 'trial/tablet/index';

import altConfigs from 'trial/test/conf/integratedTestConfigs/Tablet_Configs';

import { validateIGs } from 'PDE_Core/test/specs/integration/common/db/validateIGs.shared.partialspec';

let studyDesign = getNested('assets.studyDesign', trialAssets);

let excludeIGs = [];
if (altConfigs.excludeIGs.length) {
    excludeIGs = _.union(excludeIGs, altConfigs.excludeIGs);
}
let includeIGs = [];
if (altConfigs.includeIGs.length) {
    includeIGs = _.union(includeIGs, altConfigs.includeIGs);
}

let configs = {
    user: altConfigs.user || 'pdereadonly',
    password: altConfigs.password || 'pdereadonly2018',
    environment: 'staging',
    excludeIGs,
    includeIGs
};

describe('validate_IGs.staging', () => {
    validateIGs(studyDesign, configs);
});
