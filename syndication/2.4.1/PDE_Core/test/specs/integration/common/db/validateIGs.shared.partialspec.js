/* eslint-disable no-console */
import { getNested } from 'core/utilities/coreUtilities';

const devServer = '10.101.41.52',
    stagingServer = '10.15.145.21';
let config,
    erroredConfig = {};


function checkIssue (error) {
    if (error.code === 'ELOGIN') {
        return `
        Failed to connect to the database, please verify these connection params and update if required:
        ${JSON.stringify(erroredConfig)}`;
    }
    return undefined;
}

export function validateIGs (studyDesign, configs) {
    let initialIGList = [];
    _(getNested('questions', studyDesign)).forEach((question) => {
        let IG = question.IG;
        if (IG && !_.find(initialIGList, elem => elem === IG)) {
            initialIGList.push(IG);
        }
    });
    if (configs.excludeIGs && Array.isArray(configs.excludeIGs)) {
        initialIGList = _(initialIGList).without(...configs.excludeIGs);
    }
    if (configs.includeIGs && Array.isArray(configs.includeIGs)) {
        initialIGList = _.union(initialIGList, configs.includeIGs);
    }

    let generateIGXML = (IGList) => {
            const makeIGelement = (IG) => {
                return `<IG name="${IG}" />`;
            };
            let tempList = _(IGList).map(IG => makeIGelement(IG));
            return `<ROOT>${tempList.join('')}</ROOT>`;
        },
        parseIGXMLResonse = (responseData, resultsArray, doneCallback) => {
            let xmlDoc = $.parseXML(responseData);
            let root = $(xmlDoc).children();
            if (root.length) {
                let igList = $(root).children();
                if (igList.length) {
                    _(igList).forEach((ig) => {
                        resultsArray.push({
                            ig: ig.attributes[0].value,
                            pass: ig.attributes[1].value
                        });
                    });
                    doneCallback();
                } else {
                    console.log('no IG elements!');
                    doneCallback();
                }
            } else {
                console.log('no ROOT element!');
                doneCallback();
            }
        };

    let IGResults = [],
        originalInterval,
        error = false;
    beforeAll(() => {
        originalInterval = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
    });
    beforeAll((done) => {
        let database = _(getNested('environments', studyDesign)).find({ id: configs.environment }).studyDbName;
        if (!database) {
            console.log('Can\'t find database name from \'staging\' environment config');
            done();
        } else {
            let inputXML = generateIGXML(initialIGList);
            let server;
            switch (configs.environment) {
                case 'dev':
                    server = devServer;
                    break;
                case 'staging':
                default:
                    server = stagingServer;
            }
            config = {
                user: configs.user,
                password: configs.password,
                server,
                database
            };

            erroredConfig = {
                user: config.user,
                password: config.password,
                server: config.server,
                db: config.database
            };

            Q($.ajax({
                url: 'http://localhost:1337/IG',
                type: 'POST',
                data: JSON.stringify({
                    config,
                    inputXML
                })
            })).catch((jqXHR) => {
                console.error('ERROR', jqXHR);
                error = 'validateIGs: Communication Failed';
                done();
            }).then((data) => {
                console.log('validateIGs: gotResponse!', data);
                let regex = /^failure:/;
                if (regex.test(data) === false) {
                    parseIGXMLResonse(data, IGResults, done);
                } else {
                    console.log('validateIGs: query failed!');
                    console.error(data);
                    error = 'validateIGs: Query Failed';
                    done();
                }
            });
        }
    });

    afterAll(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalInterval;
    });

    describe(`IG results from ${configs.environment}`, () => {
        it('should find a non-zero number of IGs to investigate', () => {
            expect(initialIGList.length).toBeGreaterThan(0);
        });
        it('should not receive an error response from the query service', () => {
            expect(error).toBeFalsy(checkIssue(error));
        });

        function testResult (IGname, index) {
            it(`should find a table for IG ${IGname}`, () => {
                let IGresObj = _(IGResults).find(res => res.ig === IGname);
                expect(IGresObj.pass).toMatch('1');
            });
        }

        for (let i = 0; i < initialIGList.length; i++) {
            testResult(initialIGList[i], i);
        }
    });
}
