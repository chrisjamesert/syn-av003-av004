/* eslint-disable no-console */
import { getNested } from 'core/utilities/coreUtilities';

const devServer = '10.101.41.52',
    stagingServer = '10.15.145.21';
let config,
    erroredConfig;


function checkIssue (error) {
    if (error.code === 'ELOGIN') {
        return `
        Failed to connect to the database, please verify these connection params and update if required:
        ${JSON.stringify(erroredConfig)}`;
    }
    return undefined;
}

export function validateSUs (studyDesign, configs) {
    let initialSUList = [];
    _(getNested('questionnaires', studyDesign)).forEach((questionnaire) => {
        let SU = questionnaire.SU;
        if (SU && !_.find(initialSUList, elem => elem === SU)) {
            initialSUList.push(SU);
        }
    });
    if (configs.excludeSUs && Array.isArray(configs.excludeSUs)) {
        initialSUList = _(initialSUList).without(...configs.excludeSUs);
    }
    if (configs.includeSUs && Array.isArray(configs.includeSUs)) {
        initialSUList = _.union(initialSUList, configs.includeSUs);
    }

    let generateSUXML = (SUList) => {
            const makeSUelement = (SU) => {
                return `<SU name="${SU}" />`;
            };
            let tempList = _(SUList).map(SU => makeSUelement(SU));
            return `<ROOT>${tempList.join('')}</ROOT>`;
        },
        parseSUXMLResonse = (responseData, resultsArray, doneCallback) => {
            let xmlDoc = $.parseXML(responseData);
            let root = $(xmlDoc).children();
            if (root.length) {
                let suList = $(root).children();
                if (suList.length) {
                    _(suList).forEach((su) => {
                        resultsArray.push({
                            su: su.attributes[0].value,
                            pass: su.attributes[1].value
                        });
                    });
                    doneCallback();
                } else {
                    console.log('no SU elements!');
                    doneCallback();
                }
            } else {
                console.log('no ROOT element!');
                doneCallback();
            }
        };

    let SUResults = [],
        originalInterval,
        error = false;
    beforeAll(() => {
        originalInterval = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
    });
    beforeAll((done) => {
        let database = _(getNested('environments', studyDesign)).find({ id: configs.environment }).studyDbName;
        if (!database) {
            console.log(`Can\'t find database name from \'${configs.environment}\' environment config`);
            done();
        } else {
            let inputXML = generateSUXML(initialSUList);
            let server;
            switch (configs.environment) {
                case 'dev':
                    server = devServer;
                    break;
                case 'staging':
                default:
                    server = stagingServer;
            }
            config = {
                user: configs.user,
                password: configs.password,
                server,
                database
            };

            erroredConfig = {
                user: config.user,
                password: config.password,
                server: config.server,
                db: config.database
            };

            Q($.ajax({
                url: 'http://localhost:1337/SU',
                type: 'POST',
                data: JSON.stringify({
                    config,
                    inputXML
                })
            })).catch((jqXHR) => {
                console.error('ERROR', jqXHR);
                error = 'validateSUs: Communication Failed';
                done();
            }).then((data) => {
                console.log('validateSUs: gotResponse!', data);
                let regex = /^failure:/;
                if (regex.test(data) === false) {
                    parseSUXMLResonse(data, SUResults, done);
                } else {
                    console.log('validateSUs: query failed!');
                    console.error(data);
                    error = 'validateSUs: Query Failed';
                    done();
                }
            });
        }
    });

    afterAll(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalInterval;
    });

    describe(`SU results from ${configs.environment}`, () => {
        it('should find a non-zero number of SUs to investigate', () => {
            expect(initialSUList.length).toBeGreaterThan(0);
        });
        it('should not receive an error response from the query service', () => {
            expect(error).toBeFalsy(checkIssue(error));
        });

        function testResult (SUname) {
            it(`should find a form with SU ${SUname}`, () => {
                let suResObj = _(SUResults).find(res => res.su === SUname);
                expect(suResObj.pass).toMatch('1');
            });
        }

        for (let i = 0; i < initialSUList.length; i++) {
            testResult(initialSUList[i]);
        }
    });
}

