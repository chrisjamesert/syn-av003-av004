This location should house all jasmine spec.js files for PDE_Core/tablet items.
This will ensure that the tests are ran for only the tablet modality specific unit test suite and the PDE_Core test suite.

You may make any subdirectories to improve organization, or leave all specs at the top level of this directory.

The specs in this directory and any subdirectories will be executed as part of the 'PDE_Core.tablet' package.

To comply with the html report generation, the first parameter in describe should be a dot path to the component
    being tested, starting in the tablet directory.

For example, a unit test spec for a file 'PDE_Core/tablet/branching/MyTabletBranchFunction.js' should have the
    following describe statement:

    describe('branching.MyTabletBranchFunction', () => { ... };

The reporting feature will prepend the package name,
    to a full descriptor of 'PDE_Core.tablet.branching.MyTabletBranchFunction'

Then, when the html report is generated, the tests will be organized by these dot paths, improving readability.
