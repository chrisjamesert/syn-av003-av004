export default {
    /**
     * custom jasmine matcher to check if item is a Function.
     * @returns {{function}} comparison
     */
    toBeFunction () {
        return {
            compare (actual) {
                let result = {};

                result.pass = typeof actual === 'function';
                if (result.pass) {
                    // message for failure of .not.toBeNumber
                    result.message = `Expected ${actual} not to be a function`;
                } else {
                    result.message = `Expected ${actual} to be a function`;
                }
                return result;
            }
        };
    }
};
