export default {
    /**
     * custom jasmine matcher to run an instance comparison for non-native javascript classes.
     * @returns {{function}} comparison
     */
    toBeInstanceOf () {
        return {
            compare (actual, expected) {
                let result = {};

                result.pass = actual instanceof expected;
                if (result.pass) {
                    // message for failure of .not.toBeInstanceOf
                    result.message = `Expected ${actual} not to be an instance of ${expected.name}`;
                } else {
                    result.message = `Expected ${actual} to be an instance of ${expected.name}`;
                }
                return result;
            }
        };
    }
};
