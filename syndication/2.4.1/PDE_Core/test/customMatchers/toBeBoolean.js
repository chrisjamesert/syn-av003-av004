export default {
    /**
     * custom jasmine matcher to check if item is a Boolean.
     * @returns {Object} containing comparison
     */
    toBeBoolean () {
        return {
            compare (actual) {
                let result = {};

                result.pass = typeof actual === 'boolean';
                if (result.pass) {
                    result.message = `Expected ${actual} not to be a Boolean`;
                } else {
                    result.message = `Expected ${actual} to be a Boolean`;
                }
                return result;
            }
        };
    }
};
