import tba from './toBeAnArray';
import tbf from './toBeFunction';
import tbi from './toBeInstanceOf';
import tbn from './toBeNumber';
import tbs from './toBeString';
import tbe from './toBeEmpty';
import tbNull from './toBeNull';
import tbBool from './toBeBoolean';

/* import tbEoA from './toBeElementOfArray'; // unnecessary inversion of .toContain */

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(tba, tbf, tbi, tbn, tbs, tbe, tbNull, tbBool/* , tbEoA*/);
