export default {
     /**
     * custom jasmine matcher to check if item is a String.
     * @returns {{function}} comparison
     */
    toBeString () {
        return {
            compare (actual) {
                let result = {};

                result.pass = typeof actual === 'string';
                if (result.pass) {
                    // message for failure of .not.toBeNumber
                    result.message = `Expected ${actual} not to be a string`;
                } else {
                    result.message = `Expected ${actual} to be a string`;
                }
                return result;
            }
        };
    }
};
