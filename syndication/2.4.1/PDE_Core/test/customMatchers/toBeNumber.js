
export default {
    /**
     * custom jasmine matcher to check if item is a Number.
     * @returns {{function}} comparison
     */
    toBeNumber () {
        return {
            compare (actual) {
                let result = {};

                result.pass = !isNaN(actual);
                if (result.pass) {
                    // message for failure of .not.toBeNumber
                    result.message = `Expected ${actual} not to be a Number`;
                } else {
                    result.message = `Expected ${actual} to be a Number`;
                }
                return result;
            }
        };
    }
};
