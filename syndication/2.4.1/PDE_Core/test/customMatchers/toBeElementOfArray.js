export default {
    /**
     * custom jasmine matcher to check if item is an element within the supplied array.
     * @returns {Object} containing comparison
     */
    toBeElementOfArray (utils, customEqualityTesters) {
        return {
            compare (actual, expected) {
                let result = {};

                if (Array.isArray(expected)) {
                    result.pass = _(expected).includes(actual);
                    result.message = `Expected ${actual} to be an element of Array ${expected}`;
                } else {
                    result.pass = false;
                    result.message = `Expected ${expected} to be an array, in order to check if ${actual} is an element`
                }
                return result;
            },
            negativeCompare (actual, expected) {
                let result = {};

                if (Array.isArray(expected)) {
                    result.pass = !_(expected).includes(actual);
                    result.message = `Expected ${actual} not to be an element of Array ${expected}`;
                } else {
                    result.pass = false;
                    result.message = `Expected ${expected} to be an array, in order to check if ${actual} is an element`
                }
                return result;
            }
        };
    }
};
