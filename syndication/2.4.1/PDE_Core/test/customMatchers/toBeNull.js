export default {
    /**
     * custom jasmine matcher to check if item is null.
     * @returns {Object} containing comparison
     */
    toBeNull () {
        return {
            compare (actual) {
                let result = {};

                result.pass = actual === null;
                if (result.pass) {
                    // message for failure of .not.toBeArray
                    result.message = `Expected ${actual} not to be null`;
                } else {
                    result.message = `Expected ${actual} to be null`;
                }
                return result;
            }
        };
    }
};
