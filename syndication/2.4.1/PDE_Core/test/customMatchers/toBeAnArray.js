import ArrayRef from 'core/classes/ArrayRef';
export default {
    /**
     * custom jasmine matcher to check if item is a Function.
     * @returns {Object} containing comparison
     */
    toBeAnArray () {
        return {
            compare (actual) {
                let result = {},
                    buildString;
                if (actual instanceof ArrayRef) {
                    result.pass = true;
                    buildString = (array) => {
                        let retStr = '[';
                        for (let i = 0; i < array.length; i++) {
                            retStr = retStr + array[i];
                            if (i < array.length - 1) {
                                retStr = `${retStr}, `;
                            }
                        }
                        retStr = `${retStr}]`;
                        return retStr;
                    };
                } else {
                    result.pass = actual instanceof Array;
                    buildString = (ob) => {
                        return ob.toString();
                    };
                }
                if (result.pass) {
                    // message for failure of .not.toBeArray
                  //  console.info(`3 _ ${actual}`);
                    result.message = `Expected ${buildString(actual)} not to be an Array`;

                   // console.info(`4 _ ${actual}`);
                } else {
                   // console.info(`5 _ ${actual}`);
                    result.message = `Expected ${buildString(actual)} to be an Array`;

                   // console.info(`6 _ ${actual}`);
                }
                return result;
            }
        };
    }
};
