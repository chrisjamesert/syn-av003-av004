module.exports = function (grunt) {

    // Automagically load grunt tasks.
    require('load-grunt-config')(grunt, {
        config: {
            pkg: grunt.file.readJSON('package.json'),
            webConfig: grunt.file.readJSON('web-config.json'),
            jitgrun: true,
            core: 'app/core',
            logpad: 'app/logpad',
            sitepad: 'app/sitepad',
            web: 'app/web',
            target: 'target',
            host_staging: 'host/public/ecoa-study/staging'
        }
    });

    grunt.loadTasks('tasks');

    // Time how long tasks take.
    require('time-grunt')(grunt);
};
