/*var express = require('express'),
    _ = require('underscore'),
    fs = require('fs'),
    data = require('../data/subjects'),
    activations = require('../data/activations'),
    setupCodes = require('../data/setupCodes'),
    router = express.Router();


router.get('/', function(req, res, next) {

    res.send({
        content : data
    });

});

router.get('/id/:id', function (req, res, next) {

    var match = _.findWhere(data, { id : parseInt(req.params.id, 10) });

    if (match) {
        res.send(match);
    } else {

        // @todo send proper HTTP response error.
        throw 'User not found.';
    }

});

router.get('/id/:id/activations/current', function (req, res, next) {

    // Only works if IDs are sequential
    res.send(activations[parseInt(req.params.id, 10) - 1]);

});

router.get('/id/:id/setupCode', function (req, res, next) {

    // Only works if IDs are sequential
    res.send(setupCodes[parseInt(req.params.id, 10) - 1]);

});*/

var express = require('express'),
    _ = require('underscore'),
    fs = require('fs'),
    data = require('../data/subjects'),
    router = express.Router();

router.get('/:pin', function(req, res, next) {

    var authorized = req.get('Authorization'),
        pin = req.params.pin,
        subject = _.findWhere(data, { setupCode : pin });

        if (subject) {

            if (authorized && authorized.length != 0) {

                res.set('X-Sync-ID', '12345678');
                res.send({
                    I : subject.subjectInitials,
                    K : subject.krpt,
                    N : subject.setupCode,
                    C : subject.subjectNumber,
                    P : subject.phase,
                    T : subject.phaseStartDateTZOffset,
                    L : 'ERROR',
                    E : new Date(),
                    A : new Date()
                });

            } else {

                res.send({ S : subject.active });

            }

        } else {

            // There is no subject matching that number.
            res.send({ });

        }


});

module.exports = router;
