/**
 * grunt task for cleaning unit test xml outputs to avoid errors when generating html
 * based on Udit Adhikari's jsonCleanUpTasks.js
 * @author Mason Blaschak
 * @email mason.blaschak@ert.com
 */
'use strict';

module.exports = function (grunt) {

    var fs = require('fs'),
        path = require('path'),
        readReportFiles = require('../PDE_Core/tools/TestSupport/reportCleanup/reportReadFiles');

    grunt.registerMultiTask('reportCleanup', 'extra cleanup of the generated junit report htmls', function () {

        var options = this.options({ cwd: '' });

        // Force task into async mode and grab a handle to the "done" function.
        var done = this.async();

        let src;

        this.files.forEach(function (file) {
            src = file.src.filter(function (filepath) {
                filepath = file.cwd ? file.cwd + filepath : filepath;
                if (!grunt.file.exists(filepath)) {
                    grunt.log.warn('Source File "' + filepath + '" not found.');
                    return false;
                }
                return true;
            });

        });

        let returnVal,
            cleanedData,
            params = { newTitle: options.title };
        src.forEach(function (filePath) {
            returnVal = readReportFiles.cleanFile(filePath, params);
            cleanedData = returnVal;

            fs.writeFileSync(filePath, cleanedData);
        });

        done();

    });
};
