'use strict';

module.exports = function (grunt) {
    var path = require('path');

    grunt.registerMultiTask('concat', 'Concatenate files.', function () {
        var options = this.options({
            separator: grunt.util.linefeed
        });

        var src = this.files.forEach(function(file) {
            var fileSrc = file.src.filter(function (fpath) {
                return grunt.file.exists(fpath) && !grunt.file.isDir(fpath);
            }).map(function (fpath, index) {
                return grunt.file.read(fpath);
            }).join(options.separator);

            grunt.file.write(file.dest, fileSrc);
            grunt.log.writeln('File written to: ' + file.dest.green);
        });
    });
};
