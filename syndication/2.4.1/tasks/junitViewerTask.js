/**
 * grunt task for executing junit-viewer
 * @author Mason Blaschak
 * @email mason.blaschak@ert.com
 */
'use strict';

module.exports = function (grunt) {

    var fs = require('fs'),
        jv = require('junit-viewer');

    grunt.registerMultiTask('junit-viewer', 'parses junit xml into html', function () {

        var options = this.options({ cwd: '' });

        // Force task into async mode and grab a handle to the "done" function.
        var done = this.async();

        let targetDirectory = options.destDir ? options.destDir : '',
            targetFile = options.destFile ? options.destFile : 'report.html',
            contracted = Boolean(options.contracted);
        if (!fs.existsSync(targetDirectory)) {
            fs.mkdirSync(targetDirectory);
        }

        var src = options.srcDirectory || 'junit';
        var parsedAndRenderedData = jv.junit_viewer(src, contracted);

        if(parsedAndRenderedData) {
            fs.writeFileSync(targetDirectory + '/' + targetFile, parsedAndRenderedData);
        } else {
            grunt.log.warn('No data was parsed, unable to produce file');
        }
        done();

    });
};
