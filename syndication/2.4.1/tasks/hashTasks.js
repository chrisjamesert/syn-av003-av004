/**
 * Created by uadhikari on 1/3/2018.
 */
'use strict';

module.exports = function (grunt) {

    var fs = require('fs'),
        crypto = require('crypto'),
        path = require('path');

    grunt.registerMultiTask('hash', 'Generates HTML script tags.', function () {

        var options = this.options({ cwd: '' });
        var modality = options.modality;

        // Force task into async mode and grab a handle to the "done" function.
        var done = this.async();

        let allDirectories = [];

        this.files.forEach(function (file) {
            var hash,
                newFile,
                fileTarget = file.cwd ? (file.cwd + file.target) : file.target,
                fileDest = file.cwd ? (file.cwd + file.dest) : file.dest,
                src = file.src.filter(function (filepath) {
                    filepath = file.cwd ? file.cwd + filepath : filepath;
                    if (!grunt.file.exists(filepath)) {
                        grunt.log.warn('Source File "' + filepath + '" not found.');
                        return false;
                    }
                    return true;
                });
            allDirectories = src;
        });

        let hashList = [];

        allDirectories.forEach(function(directory) {

            let allFiles = [];

            // Get all files in a directory in Node.js recursively in a synchronous fashion
            let walkSync = function(dir, filelist) {

                let files = fs.readdirSync(dir);

                filelist = filelist || [];

                files.forEach(function(file) {

                    if (fs.statSync(path.join(dir, file)).isDirectory()) {
                        filelist = walkSync(path.join(dir, file), filelist);
                    }
                    else {
                        filelist.push(path.join(dir, file));
                    }
                });
                return filelist;
            };

            allFiles = walkSync(directory, allFiles);

            let hash = crypto.createHash('md5');

            //for all files hash and update the hash

            allFiles.forEach(function(filePath) {
                let data;

                if(grunt.file.exists(filePath)) {
                    data = grunt.file.read(filePath);
                    hash.update(data, 'utf8');
                }
            });

            hash = hash.digest('hex');
            hashList.push(hash);

        });


        //Build string for the library folder and its hash
        if(allDirectories.length !== hashList.length) {

            grunt.log.warn('Error in hashing library folders.');

        } else {

            let date = new Date(),
                dataToWrite = "Hashing performed at : " + date.toDateString() + " " + date.toTimeString() + " for modality : " + modality + "\n\n";

            allDirectories.forEach(function(directory, index) {

                let directorySplit = directory.split("/"),
                    directoryName;

                if(directorySplit && directorySplit.length > 0) {
                    directoryName = directorySplit[directorySplit.length - 1];
                    directoryName = directoryName.split("Lib_").join("");
                }

                dataToWrite += "Library Diary : "  + directoryName + " ---> " + "hash : " + hashList[index] + "\n\n";

            });

            //write the hashing info for each library folder to trial/hash.txt

            let pathToWrite = "trial" + path.sep + modality + path.sep + "hash.txt";

            fs.writeFile(pathToWrite, dataToWrite, { flag: 'w' }, function(err) {

                if(err) {
                    grunt.log.warn('Error in writing Hashing File to trial/hash.txt');
                    grunt.log.warn(err);
                } else {
                    grunt.log.writeln("The hashing file was successfully written.");
                }
                done();

            });

        }

    });
};
