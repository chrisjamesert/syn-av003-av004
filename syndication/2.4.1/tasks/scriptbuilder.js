'use strict';

module.exports = function (grunt) {

    var util = require('util');

    grunt.registerMultiTask('scriptbuilder', 'Generates HTML script tags.', function () {

        var options = this.options({
                start: '<!-- START SCRIPT -->',
                end: '<!-- END SCRIPT -->',
                template: '<script type="text/javascript" src="%s"></script>',
                cwd: ''
            }),
            prepend = options.useCDN ? options.cdnUrl : '.';

        this.files.forEach(function (file) {
            var start,
                end,
                newFile,
                fileTarget = file.cwd ? (file.cwd + file.target) : file.target,
                fileDest = file.cwd ? (file.cwd + file.dest) : file.dest,
                src = '\n' + file.src.filter(function (filepath) {
                    filepath = file.cwd ? file.cwd + filepath : filepath;
                    if (!grunt.file.exists(filepath)) {
                        grunt.log.warn('Source File "' + filepath + '" not found.');
                        return false;
                    }
                    return true;
                })
                .map(function (filepath) {
                    filepath = prepend + '/' + filepath;
                    return util.format(options.template, filepath);
                })
                .join('\n');

            // Add a new line to the end of the src string.
            src += '\n';

            if (!grunt.file.exists(fileTarget)) {
                grunt.log.warn('Target file "' + fileTarget + '" not found.');
            } else {
                // Read in the target file.
                newFile = grunt.file.read(fileTarget);

                // Get the index of the start tag.
                start = newFile.indexOf(options.start);

                // Get the index of the end tag.
                end = newFile.indexOf(options.end);

                if (start === -1 || end === -1) {
                    // Either the start or end tag wasn't found.
                    grunt.log.warn('Start and/or end tag is not found.');
                } else {
                    newFile = newFile.substr(0, start + options.start.length) + src + newFile.substr(end);
                    grunt.file.write(fileDest, newFile);
                    grunt.log.writeln('File "' + fileDest + '" updated.');
                }
            }
        });
    });
};
