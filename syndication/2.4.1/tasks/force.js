/**
 * grunt task to override the force flag during the execution of a series of tasks
 * sourced from Kyle Simpson on stackOverflow
 * https://stackoverflow.com/questions/16612495/continue-certain-tasks-in-grunt-even-if-one-fails/16972894
 * @example in alias.yaml
 * series-of-tasks:
 *   - task1
 *   - force:on
 *   - task2
 *   - force:restore
 *   - task3
 */

module.exports = function(grunt) {
    var previous_force_state = grunt.option("force");

    grunt.registerTask("force",function(set){
        if (set === "on") {
            grunt.option("force",true);
        }
        else if (set === "off") {
            grunt.option("force",false);
        }
        else if (set === "restore") {
            grunt.option("force",previous_force_state);
        }
    });
};
