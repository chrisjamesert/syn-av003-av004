Version info for Syneos AV003-AV004 Tablet, Syndication app core release 2.4.1, PDE Template 241.5.0 + ReproCorder
New versions should be added to the top, pushing previous versions down.

CORE CHANGES BY PDE:
app/sitepad/visits/visitUtils.js @212-225: Adds visit border ELF trigger

===================

Known issues:

* None

===================

LABEL   DATE            CHANGES
======  ===========     ============

01.04   31OCT2018       [cjames/hkarthikeyan]
                        * ONEECOA-118400: [TB] Tablet Training Module: The questionnaire is saved and sent to SW when completed in Training Visit
                        * ONEECOA-118402: [TB] Select Visit: Visit 2 - Week 0, Premature Discontinuation can be closed by other visits
                        * ONEECOA-118404: [TB] Unscheduled Visit 1 - 10 are skippable
                        * ONEECOA-118115: [TB][End of Study] End of study Visit can be closed
                        * ONEECOA-118397: [TB] Order of assessment is sorted by alphabet instead of activity order

01.03   31OCT2018       [cjames]
                        * ONEECOA-118396: [TB][UI] Time format mismatch with PDS Sec.3.2.1.1.1
                        * Updates the language and date resources for all study langauges.

01.02   29OCT2018       [cjames]
                        * ONEECOA-117402: [UI][Add First Site User/Site User] Full keyboard is shown instead of number
                          pad entry for Password and Confirm Password fields
                        * ONEECOA-117879: [TB][Home Screen] Phase displays on Home Screen different from Spec
                        * Updates the ReproCorder automation tool.

01.01   26OCT2018       [cjames]
                        * ONEECOA-117585: [TB] Cannot Edit Patient
                        * ONEECOA-117361: [TB][UI][Add/Edit Site User] Language list doesn't match with PDS
                        * ONEECOA-117633: [TB][Add Site User] Site Administrator Role doesn't match PDS
                        * ONEECOA-117589: [TB][Childhood ACT] Wrong form name displayed on device
                        * Updates the ReproCorder automation tool.

01.00   25OCT2018       [cjames]
                        * Updates the ReproCorder automation tool.
                        * Updates form availability for unscheduled visits per PDS update.
                        * Fixes the Patient ID formatting error.
                        * Implements the Edit Subject screen.
                        * ONEECOA-117363: [TB][UI][Deactivation] Missing two line-break on screen TD010
                        * ONEECOA-117371: [TB][UI][Registration screen] Unlock Code view screen displays "ERT Unlock Codes" instead of "Tablet Unlock Codes"
                        * ONEECOA-117385: [TB][UI][Add new patient] Wrong text on screen ADD_PATIENT_S_1_POPUP
                        * ONEECOA-117386: [UI] [Childhood ACT Screens] Missing line break after the first and second paragraph on screen CAC010
                        * ONEECOA-116959: Tablet Training Module_Missing line break after the first and second paragraph
                        * ONEECOA-117087: Add Patient_Text should not bolded and redundant colon (mismatch PDS 13.1.1.1)

00.02   23OCT2018       [cjames]
                        * UI Testing build.

00.01   15OCT2018       [cjames]
                        * First build, for screenshots.
