import RadioButton from 'core/widgets/RadioButton';

/**
 * CustomImageRadioButton lets you assign an image to each item.
 */
class CustomImageRadioButton extends RadioButton {
    /**
     * Constructs a CustomImageRadioButton Class.
     * @param {Object} options The options used to create the RadioButton
     * @param {Object} options.model The model containing the specific configuration for the widget.  This is represented by the "widget" property in the question definition in the study design.
     * @param {string} options.model.id {dev-only} The ID associated with the model.
     * @param {string} [options.model.className=CustomRadioButton] {dev-only} The CSS classname applied to the widget.
     * @param {Array} options.model.answers The list of answer options used to render the widget.
     *       Custom Image Code here: Each element of the array should an object of the form <code>{ text: 'ONE', value: '1', image: 'path/to/image' }</code>
     * @param {Object} options.model.templates {dev-only} Contains any display templates to override the widget's defaults.
     * @param {string} [options.model.templates.wrapper=DEFAULT:RadioButtonWrapper] {dev-only} The template used to render the wrapper element for the widget.
     * @param {string} [options.model.templates.input=DEFAULT:RadioButton] {dev-only} The template used to render the input elements for the widget.
     * @param {string} [options.model.templates.label=DEFAULT:RadioButtonLabel] {dev-only} The template used to render the labels attached to the inputs of the widget.
     */
    // eslint-disable-next-line no-useless-constructor
    constructor (options) {
        super(options);
    }

    // noinspection JSMethodCanBeStatic
    get image () {
        return 'DEFAULT:CustomImageRadioButtonImage';
    }

    // noinspection JSMethodCanBeStatic
    /**
     * The class of the root element.
     * @returns {String}
     */
    get className () {
        return 'CustomRadioButton';
    }

    /**
     * Build the html structure of widget
     * @param {String} type The type of the widget
     * @example this.buildHTML('checkbox');
     * @example this.buildHTML('radio')'
     * @returns {Q.Promise} resolved when HTML is built and ready
     */
    buildHTML (type) {
        let modelId = this.model.get('id'),
            templates = this.model.get('templates') || {},
            container = templates.container || this.container,
            input = templates.input || this.input,
            label = templates.label || this.label,
            wrapper = templates.wrapper || this.wrapper,
            image = templates.image || this.image,
            curPromise = Q();

        this.$el.html(LF.templates.display(container));
        const AddOptionFactory = (answer, i) => {
            return LF.getStrings(answer.text, $.noop, { namespace: this.question.getQuestionnaire().id })
                .then((string) => {
                    return Q.Promise((resolve) => {
                        let $ele,

                            // Append options within a container
                            appendOption = function ($target) {
                                return $target.find('[data-container]').addBack('[data-container]')
                                    .first()
                                    .append(LF.templates.display(input, {
                                        id: `${modelId}_${type}_${i}`,
                                        name: modelId,
                                        value: answer.value
                                    }), LF.templates.display(label, {
                                        link: `${modelId}_${type}_${i}`,
                                        className: `${type}_${i}`,
                                        text: string
                                    }), LF.templates.display(image, {
                                        src: answer.image,
                                        className: `${type}_${i}`
                                    }));
                            };

                        if (wrapper != null) {
                            $ele = $(LF.templates.display(wrapper, {
                                id: `${modelId}_${type}_${i}`
                            }));
                            appendOption($ele);
                            this.$('[data-container]')
                                .first()
                                .append($ele)
                                .ready(resolve);
                        } else {
                            appendOption(this.$el).ready(resolve);
                        }
                    });
                });
        };

        _(this.model.get('answers')).each((answer, i) => {
            curPromise = curPromise.then(() => {
                // eslint-disable-next-line new-cap
                return AddOptionFactory(answer, i);
            });
        });
        return curPromise;
    }
}

window.LF.Widget.CustomImageRadioButton = CustomImageRadioButton;
export default CustomImageRadioButton;
