
import ChildACT from './ChildACT';
import templates from './childActTemplates';
import './widgets';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, ChildACT, templates);

export default { studyDesign };

