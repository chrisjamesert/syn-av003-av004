export default {
    templates: [
        {
            name: 'CustomImageRadioButtonImage',
            namespace: 'DEFAULT',
            template: '<img class="{{ className }}" src="{{ src }}"/>'
        }
    ]
};
