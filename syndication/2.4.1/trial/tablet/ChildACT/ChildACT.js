
export default {
    questionnaires: [
        {
            id: 'ChildACT',
            SU: 'ChildACT',
            deviceType: 'sitepad',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'CAC010',
                'CAC020',
                'CAC030',
                'CAC040',
                'CAC050',
                'CAC060',
                'CAC070',
                'CAC080',
                'CAC090'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: [
                'subject'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'CAC010',
            className: 'CAC010',
            questions: [
                {
                    id: 'CAC010_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC020',
            className: 'CAC020',
            questions: [
                {
                    id: 'CAC020_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC030',
            className: 'CAC030',
            questions: [
                {
                    id: 'CAC030_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC040',
            className: 'CAC040',
            questions: [
                {
                    id: 'CAC040_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC050',
            className: 'CAC050',
            questions: [
                {
                    id: 'CAC050_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC060',
            className: 'CAC060',
            questions: [
                {
                    id: 'CAC060_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC070',
            className: 'CAC070',
            questions: [
                {
                    id: 'CAC070_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC080',
            className: 'CAC080',
            questions: [
                {
                    id: 'CAC080_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'CAC090',
            className: 'CAC090',
            questions: [
                {
                    id: 'CAC090_Q1',
                    mandatory: true
                },
                {
                    id: 'CAC090_Q2',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }
    ],
    questions: [
        {
            id: 'CAC010_Q1',
            text: 'CAC010_Q1_TEXT',
            title: '',
            className: 'CAC010_Q1'
        }, {
            id: 'CAC020_Q1',
            IG: 'ChildACT',
            IT: 'ASTTOD1L',
            text: 'CAC020_Q1_TEXT',
            title: '',
            className: 'CAC020_Q1',
            widget: {
                id: 'CAC020_Q1_W0',
                type: 'CustomImageRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'CAC020_Q1_W0_A0',
                        image: 'trial/tablet/ChildACT/images/face_0.jpg'
                    },
                    {
                        value: '1',
                        text: 'CAC020_Q1_W0_A1',
                        image: 'trial/tablet/ChildACT/images/face_1.jpg'
                    },
                    {
                        value: '2',
                        text: 'CAC020_Q1_W0_A2',
                        image: 'trial/tablet/ChildACT/images/face_2.jpg'
                    },
                    {
                        value: '3',
                        text: 'CAC020_Q1_W0_A3',
                        image: 'trial/tablet/ChildACT/images/face_3.jpg'
                    }
                ]
            }
        }, {
            id: 'CAC030_Q1',
            IG: 'ChildACT',
            IT: 'ASTEXC1L',
            text: 'CAC030_Q1_TEXT',
            title: '',
            className: 'CAC030_Q1',
            widget: {
                id: 'CAC030_Q1_W0',
                type: 'CustomImageRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'CAC030_Q1_W0_A0',
                        image: 'trial/tablet/ChildACT/images/face_0.jpg'
                    },
                    {
                        value: '1',
                        text: 'CAC030_Q1_W0_A1',
                        image: 'trial/tablet/ChildACT/images/face_1.jpg'
                    },
                    {
                        value: '2',
                        text: 'CAC030_Q1_W0_A2',
                        image: 'trial/tablet/ChildACT/images/face_2.jpg'
                    },
                    {
                        value: '3',
                        text: 'CAC030_Q1_W0_A3',
                        image: 'trial/tablet/ChildACT/images/face_3.jpg'
                    }
                ]
            }
        }, {
            id: 'CAC040_Q1',
            IG: 'ChildACT',
            IT: 'COGHAS1L',
            text: 'CAC040_Q1_TEXT',
            title: '',
            className: 'CAC040_Q1',
            widget: {
                id: 'CAC040_Q1_W0',
                type: 'CustomImageRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'CAC040_Q1_W0_A0',
                        image: 'trial/tablet/ChildACT/images/face_0.jpg'
                    },
                    {
                        value: '1',
                        text: 'CAC040_Q1_W0_A1',
                        image: 'trial/tablet/ChildACT/images/face_1.jpg'
                    },
                    {
                        value: '2',
                        text: 'CAC040_Q1_W0_A2',
                        image: 'trial/tablet/ChildACT/images/face_2.jpg'
                    },
                    {
                        value: '3',
                        text: 'CAC040_Q1_W0_A3',
                        image: 'trial/tablet/ChildACT/images/face_3.jpg'
                    }
                ]
            }
        }, {
            id: 'CAC050_Q1',
            IG: 'ChildACT',
            IT: 'WAKAST1L',
            text: 'CAC050_Q1_TEXT',
            title: '',
            className: 'CAC050_Q1',
            widget: {
                id: 'CAC050_Q1_W0',
                type: 'CustomImageRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'CAC050_Q1_W0_A0',
                        image: 'trial/tablet/ChildACT/images/face_0.jpg'
                    },
                    {
                        value: '1',
                        text: 'CAC050_Q1_W0_A1',
                        image: 'trial/tablet/ChildACT/images/face_1.jpg'
                    },
                    {
                        value: '2',
                        text: 'CAC050_Q1_W0_A2',
                        image: 'trial/tablet/ChildACT/images/face_2.jpg'
                    },
                    {
                        value: '3',
                        text: 'CAC050_Q1_W0_A3',
                        image: 'trial/tablet/ChildACT/images/face_3.jpg'
                    }
                ]
            }
        }, {
            id: 'CAC060_Q1',
            text: 'CAC060_Q1_TEXT',
            title: '',
            className: 'CAC060_Q1'
        }, {
            id: 'CAC070_Q1',
            IG: 'ChildACT',
            IT: 'DAYAST1L',
            text: 'CAC070_Q1_TEXT',
            title: '',
            className: 'CAC070_Q1',
            widget: {
                id: 'CAC070_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '5',
                        text: 'CAC_NOT_AT_ALL'
                    },
                    {
                        value: '4',
                        text: 'CAC_1_TO_3_DAYS'
                    },
                    {
                        value: '3',
                        text: 'CAC_4_TO_10_DAYS'
                    },
                    {
                        value: '2',
                        text: 'CAC_11_TO_18_DAYS'
                    },
                    {
                        value: '1',
                        text: 'CAC_19_TO_24_DAYS'
                    },
                    {
                        value: '0',
                        text: 'CAC_EVERYDAY'
                    }
                ]
            }
        }, {
            id: 'CAC080_Q1',
            IG: 'ChildACT',
            IT: 'WHZAST1L',
            text: 'CAC080_Q1_TEXT',
            title: '',
            className: 'CAC080_Q1',
            widget: {
                id: 'CAC080_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '5',
                        text: 'CAC_NOT_AT_ALL'
                    },
                    {
                        value: '4',
                        text: 'CAC_1_TO_3_DAYS'
                    },
                    {
                        value: '3',
                        text: 'CAC_4_TO_10_DAYS'
                    },
                    {
                        value: '2',
                        text: 'CAC_11_TO_18_DAYS'
                    },
                    {
                        value: '1',
                        text: 'CAC_19_TO_24_DAYS'
                    },
                    {
                        value: '0',
                        text: 'CAC_EVERYDAY'
                    }
                ]
            }
        }, {
            id: 'CAC090_Q1',
            IG: 'ChildACT',
            IT: 'ASTWAK1L',
            text: 'CAC090_Q1_TEXT',
            title: '',
            className: 'CAC090_Q1',
            widget: {
                id: 'CAC090_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '5',
                        text: 'CAC_NOT_AT_ALL'
                    },
                    {
                        value: '4',
                        text: 'CAC_1_TO_3_DAYS'
                    },
                    {
                        value: '3',
                        text: 'CAC_4_TO_10_DAYS'
                    },
                    {
                        value: '2',
                        text: 'CAC_11_TO_18_DAYS'
                    },
                    {
                        value: '1',
                        text: 'CAC_19_TO_24_DAYS'
                    },
                    {
                        value: '0',
                        text: 'CAC_EVERYDAY'
                    }
                ]
            }
        }, {
            id: 'CAC090_Q2',
            text: 'CAC090_Q2_TEXT',
            title: '',
            className: 'CAC090_Q2'
        }
    ]
};
