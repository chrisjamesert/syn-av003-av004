import Answer from 'core/models/Answer';
import Logger from 'core/Logger';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import UserReports from 'sitepad/collections/UserReports';
import Schedules from 'core/collections/Schedules';
import UserVisits from 'sitepad/collections/UserVisits';


/**
 * commonUtils is written such that it can be imported into another file,
 * or the traditional global namespace LF.Utilities
 */

/**
 * addSysVar returns a promise that resolves true if a Sysvar is successfully added, or rejects if it fails.
 * @param {Object} params object containing parameters for sysvar to be added
 * @return {Promise<T>} resolves true if successful, else rejects
 */
export function addSysVar (params) {
    let model,
        logger = new Logger('PDE_Utilities.addSysvar');
    return Q().then(() => {
        if (params && params.view && params.questionnaire_id && params.response && params.IG && params.sysvar) {
            model = new Answer({
                response        : params.response,
                SW_Alias        : `${params.IG}.${params.sysvar}`,
                questionnaire_id: params.questionnaire_id,
                question_id     : 'SYSVAR'
            });
            if (params.view.data.dashboard) {
                model.set('instance_ordinal', params.view.data.dashboard.get('instance_ordinal'));
            }
            params.view.data.answers.add(model);
            return true;
        } else {
            logger.error('Attempt to insert a sysVar failed: params are incomplete.');
            throw new Error('Attempt to insert a sysVar failed: params are incomplete.');
        }
    });
}

LF.Utilities.addSysvar = addSysVar;


LF.Utilities.determineAutoLaunch = function(schedulesToCheck) {
    let deferred = Q.defer();
    LF.Utilities.getCollection('LastDiaries').then((lastDiaries) => {

        let view = LF.router.view();
        if (!(view instanceof BaseQuestionnaireView)) {
            view = view.questionnaire;
        }

        let length = schedulesToCheck.length,
            completedQ = lastDiaries,
            tempSchedule,
            currentReportID = view.data.dashboard.get('questionnaire_id'),
            currentVisit = view.visit.get('id'),
            logger = new Logger('PDE_Utilities.determineAutoLaunch'),
            phase = view.data.dashboard.get('phase'),
            tempSchedulePhases,
            isActiveSchedulePhase = false,
            step = function(count) {

                tempSchedule = _.find(LF.StudyDesign.schedules.models, function(sched) {
                    return sched.get('id') == schedulesToCheck[count];
                });

                /*tempSchedulePhases = tempSchedule.get('phase');

                _.each(tempSchedulePhases, function(tempSchedulePhase) {
                    if (LF.StudyDesign.studyPhase[tempSchedulePhase] == phase) {
                        isActiveSchedulePhase = true;
                        //break out of loop
                        return false;
                    }
                });*/

                //if (isActiveSchedulePhase) {

                    LF.Schedule.schedulingFunctions[tempSchedule.get('scheduleFunction')](tempSchedule, completedQ, null, view, function(diaryAvail) {

                            logger.info("Parsing Schedule");
                            let inVisitSchedule = _.where(tempSchedule.get('scheduleParams').visits, {visitID: currentVisit}),
                                predID,
                                currentView = LF.router.view(),
                                subjectGenderModel = subjectsGender.where({'krpt' : currentView.questionnaire.subject.get('krpt')});

                            if (tempSchedule.get('id') == 'ICIQUI_Schedule'){
                                if (subjectGenderModel && subjectGenderModel.length !== 0){
                                    if (subjectGenderModel[0].get('gender') == '0'){
                                        deferred.resolve();
                                        return;
                                    }
                                }
                            }



                                if (inVisitSchedule.length){
                                    predID = inVisitSchedule[0].predecessorID;
                                }
                            //if (diaryAvail && (currentReportID !== tempSchedule.get('target').id) && (currentReportID === tempSchedule.get('target').predecessorID)) {
                            if (predID && (currentReportID === predID)) {

                                localStorage.setItem('AUTOLAUNCH', tempSchedule.get('target').id);
                                deferred.resolve(tempSchedule.get('target').id);
                                return;
                            }
                            else if (count + 1 < length) {
                                step(count + 1);
                            }
                            else {
                                deferred.resolve();
                                return;
                            }

                    });
                /*}
                else {
                    if (count + 1 < length) {
                        step(count + 1);
                    }
                    else {
                        return;
                    }
                }*/
            };

        step(0);
    });
    return deferred.promise;
};

LF.Utilities.isPDEVisit = function(visit) {
    if (visit.get('studyEventId') === '-9999') {
        return true;
    } else {
        return false;
    }
};


/**
 * getSite returns a promise that resolves with the site
 * @return {Promise.<T>}
 */
export function getSite () {
    return LF.Utilities.getCollection('Sites')
        .then((sitesCollection) => {
            return sitesCollection.at(0);
        });
}

LF.Utilities.getSite = getSite;

/**
 * Return whether the specified visit has been touched.
 * A touched visit is one that has a UserVisit object associated with it.
 * That would include one that was skipped, expired, complete, or in-progress.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has been touched, otherwise false
 */
export function isVisitTouched (userVisit) {
    return (userVisit !== null) && (userVisit !== undefined);
}

LF.Utilities.isVisitTouched = isVisitTouched;

/**
 * Return whether the specified visit is completed.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit is completed, otherwise false
 */
export function isVisitCompleted (userVisit) {
    return isVisitTouched(userVisit) &&
        (   userVisit.get('state') === LF.VisitStates.COMPLETED ||
            userVisit.get('state') === LF.VisitStates.INCOMPLETE);
}

LF.Utilities.isVisitCompleted = isVisitCompleted;

/**
 * Converts the study phases from study design into an object whereby the name can be reference by key
 * @returns {Array}
 */
const convertStudyPhaseToPhases = () => {
    let phaseArray = [],
        studyPhases = LF.StudyDesign.studyPhase;
    for (let key in studyPhases) {
        let phaseObject = { name: key, number: studyPhases[key] };
        phaseArray.push(phaseObject);
    }
    return phaseArray;
};

/**
 * Given a particular visit, it checks the phases that the visit is available within
 * and also checks the subjects current visit returns whether they match
 * @param visit
 * @param currentPhase
 * @returns {boolean}
 */
export const visitPhasesIncludeSubjectCurrentPhase = (visit, currentPhase) => {
    let visitPhases = visit.phases,
        studyPhases = convertStudyPhaseToPhases();
    for (let i = 0; i < visitPhases.length; i++) {

        let visitPhase = visitPhases[i],
            isNumber = !isNaN(visitPhase),
            matches = studyPhases.filter(studyPhase => {
                if (isNumber) {
                    return studyPhase.number === visitPhase && studyPhase.number === currentPhase;
                }
                else {
                    return studyPhase.name === visitPhase && studyPhase.number === currentPhase;
                }
            })
        if (matches.length > 0) return true;
    }
    return false;
};


/**
 * Return whether the specified visit is in progress.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit is in progress, otherwise false
 */
export function isVisitInProgress (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.IN_PROGRESS);
}

LF.Utilities.isVisitInProgress = isVisitInProgress;

/**
 * Return whether the specified visit is finished.
 * A visit is considered finished if a visit has dateEnded for the UserVisit object. This is
 * equivalent to a visit with incomplete, aborted, expired or completed visit status.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has ended, otherwise false
 */
export function isVisitFinished (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('dateEnded') !== null);
}

LF.Utilities.isVisitFinished = isVisitFinished;

/**
 * Return whether the specified visit has been skipped.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has been skipped, otherwise false
 */
export function isVisitSkipped (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.SKIPPED);
}

LF.Utilities.isVisitSkipped = isVisitSkipped;

/**
 * Return whether the specified visit is available.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit is available, otherwise false
 */
export function isVisitAvailable (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.AVAILABLE);
}

LF.Utilities.isVisitAvailable = isVisitAvailable;

/**
 * Return whether the specified visit has expired.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has expired, otherwise false
 */
export function isVisitExpired (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.INCOMPLETE)
        && (userVisit.get('expired') === true);
}

LF.Utilities.isVisitExpired = isVisitExpired;

/**
 * Return whether the specified visit was aborted.
 * A visit is considered aborted if it was started and then prematurely closed
 * but has not expired.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit was aborted, otherwise false
 */
export function isVisitAborted (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.INCOMPLETE) && !isVisitExpired(userVisit);
}

LF.Utilities.isVisitAborted = isVisitAborted;

/**
 * Return the list of available report schedules remaining
 * @param {Subject} subject the subject
 * @param {UserVisit} visit the visit
 * @returns {Schedule[]}array of schedules
 */
export function getAvailableSchedules (subject, visit) {

    let logger = new Logger('getAvailableSchedules');

    // Because LF.schedule.determineSchedules has not yet been changed to use promises, the "available" callback function
    // is called multiple times.  The workaround is to wrap it to return a promise inside our own function, then call our own function, below.
    let determineSchedules = (schedules, options) => {
        return Q.Promise(resolve => {
            LF.schedule.determineSchedules(schedules, options, resolve);
        });
    };

    let forms = visit.get('forms'),
        userReports = new UserReports(),
        visitId = visit.id,
        subjectKrpt = subject.get('krpt'),
        formSchedules = new Schedules(),
        myAvailableSchedules = new Schedules();

    if (!forms) {
        // We didn't get to userReports.fetch, so only now do we need a new promise. Q() basically creates a promise that resolves immediately.
        // "return Q.resolve()" is the same thing if that is more clear to you.
        return Q.resolve(myAvailableSchedules);
    }

    // userReports.fetch() already returns a new promise so no need to create our own.
    return userReports.fetch({
        search: {
            where: {user_visit_id: visitId, subject_krpt: subjectKrpt}
        }
    })
        .then((reports) => {

            //Determine how many reports are available for this visit in this phase
            forms.forEach((visit) => {
                formSchedules.add(LF.StudyDesign.schedules.filter((schedule) => schedule.get('target').id === visit), {merge: true});
            });

            // Call our new determineSchedules function
            return determineSchedules(formSchedules, {
                subject, // Same as subject: subject
                visit // same as visit: visit
            });
        })
        .then(available => {
            myAvailableSchedules.set(available);
            return myAvailableSchedules;
        })
        .catch(err => {
            // log the error.
            logger.error('Cannot get available schedules', err);
        });
}
LF.Utilities.getAvailableSchedules = getAvailableSchedules;

/**
 * Decrypts an encrypted string.
 * @param {string} encrypted
 * @param {string} krpt Subject krpt is usually the key for encryption.
 * @returns {string} The decrypted data.
 */
LF.Utilities.PDEDecrypt = function (encrypted, krpt) {
    let decrypted;
    GibberishAES.size(128);
    try {
        decrypted = GibberishAES.dec(encrypted, krpt);
    } catch (e) {
        throw new Error(e.toString()); // GibberishAES gives no stack trace; create one
    }
    return decrypted;
};
