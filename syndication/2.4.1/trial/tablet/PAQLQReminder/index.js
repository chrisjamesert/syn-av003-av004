
import PAQLQReminder from './PAQLQReminder';

import {mergeObjects} from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, PAQLQReminder);

export default { studyDesign };

