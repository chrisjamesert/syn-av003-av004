
export default {
    questionnaires: [
        {
            id: 'PAQLQReminder',
            SU: 'PAQLQ',
            deviceType: 'sitepad',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            screens: [
                'PAQ010'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: [
                'site',
                'admin'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'PAQ010',
            className: 'PAQ010',
            questions: [
                {
                    id: 'PAQ010_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }
    ],
    questions: [
        {
            id: 'PAQ010_Q1',
            text: 'PAQ010_Q1_TEXT',
            title: '',
            className: 'PAQ010_Q1'
        }
    ]
};
