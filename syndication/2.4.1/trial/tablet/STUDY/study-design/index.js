import SD from './tablet_study-design';
import visits from './visits';
import './visitsAvailabilityFunctions';
import './LocalTools';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(SD, visits);
