import Logger from 'core/Logger';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';

const logger = new Logger('LocalTools');

LF.Utilities.getCachedCollection = function (view, collectionName) {
    return PDE.PDECollections[collectionName];
};

LF.Utilities.isPDEVisit = function (visit) {
    if (visit.get('studyEventId') === '-9999') {
        return true;
    }
    return false;
};

LF.Widget.DatePicker.prototype.buildStudyWorksStringWithTime = function (date) {
    if (!(date instanceof Date)) {
        throw new Error(`Expected a Date, got ${date}`);
    }
    let swDate = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate(),
        swMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        swMonth = swMonths[date.getMonth()],
        swYear = date.getFullYear(),
        swHour = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours(),
        swMinutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes(),
        swSeconds = date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds();

    return `${swDate} ${swMonth} ${swYear} ${swHour}:${swMinutes}:${swSeconds}`;
};



// Always empty by default, except for methods which should always do nothing or return
// no data (null) or false.  Essentially it must have NO impact on an official build.
// Developer can override object in debugger with whatever easter-eggs are needed.
LF.Utilities.DevConfig = {
    isPDEVisitEnabled: false,
    allowMultiVisitDays: false,
    confOnly: false,

    isConfOnly (visit, reportID) {
        return false;
    }
};

LF.Utilities.getLastVisitID = function () {
    return Q.Promise((resolve) => {
        LF.Utilities.getCollection('UserVisits')
            .then((UserVisit) => {
                let visitID = null,
                    krpt = LF.Data.Questionnaire.subject.get('krpt'),
                    visit = _.last(UserVisit.where({ state: 'Completed', subjectKrpt: krpt }));

                if (visit != null) {
                    visitID = visit.get('studyEventId');
                }

                resolve(visitID);
            });
    });
};

/**
 * saveModel returns a promise which executes the save function of a supplied model, with optional data object
 * @param {LF.Model.<T>} model the model to be saved
 * @param {Object} [data] data to be saved into the model
 * @return {Promise.<T>} resolves upon successful save, rejects if model DNE or cannot be saved
 */
LF.Utilities.saveModel = function (model, data = {}) {
    let logger = new Logger('PDE_Utilities.saveModel');
    return Q.Promise((resolve) => {
        model.save(data, {
            onSuccess: () => {
                logger.info(`Successfully saved model ${model.name}`);
                resolve();
            },
            onError: (err) => {
                logger.error(`Failed to save model ${model.name}`, err);
                resolve(err);
            }
        });
    }).then((err) => {
        // this catches the onError error and transforms it into a reject without accidentally logging both
        // the Failed to save model and Attempted to save non-model object messages
        if (err) {
            throw err;
        } else {
            return true;
        }
    }, (err) => {
        logger.error('Attempted to save non-model object', err);
        throw err;
    });
};
