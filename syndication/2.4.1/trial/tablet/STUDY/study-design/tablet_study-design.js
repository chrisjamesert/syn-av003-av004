import ArrayRef from 'core/classes/ArrayRef';
import ObjectRef from 'core/classes/ObjectRef';

export default {
    // The current tablet study version
    studyVersion: '01.03',

    // The current tablet study database version
    studyDbVersion: 0,

    // flag to enable partial sync for the tablet modality
    enablePartialSync: false,

    // flag to enable full sync for the tablet modality
    enableFullSync: true,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 35,

    // The amount of time in minutes that a user is allowed to complete a diary within.
    // Should not be > sessionTimeout, since sessionTimeout will happen first and exit the diary anyway.
    // If 0, questionnaire itself doesn't time out, but sessionTimeout will still occur
    questionnaireTimeout: 30,

    // Configuration for rater training
    raterTrainingConfig: {
        sitepad: {
            useRaterTraining: false,
            awsKey: '',
            awsSecretKey: '',
            awsRegion: '',
            awsBucket: '',
            rootDirectory: 'rater',
            fallBackBaseURL: '',
            pages: {
                default: 'index.html'
            },
            zipFile: true
        }
    },

    // Configuration for participant/patient
    participantSettings: {
        participantID: {
            seed: 1,
            steps: 1,
            max: 999
        },
        participantIDFormat: 'E0000000',
        participantNumberPortion: new ArrayRef([5, 8]),
        siteNumberPortion: new ArrayRef([1, 5]),

        // PDE Custom for multi-protocol ID range support.
        // The keys must correspond with the protocol list keys
        // in common_study-design.js
        protocolIdRanges: new ObjectRef({
            3: {
                min: 1,
                max: 499
            },
            4: {
                min: 500,
                max: 999
            }
        })
    },

    // The phase from studyPhase to be used as the phase after Subject Assignment
    subjectAssignmentPhase: 20,

    // When true, the Protocol select dropdown in New Patient will be prepopulated with
    // the defaultProtocol defined in StudyDesign.studyProtocol
    prepopulateDefaultProtocol: true
};
