import UserVisits from 'sitepad/collections/UserVisits';
import * as dateTimeUtil from 'core/DateTimeUtil';

export function visitExpiresAfterMidnight (params) {
    return new Q.Promise((resolve) => {
        UserVisits.fetchCollection()
        .then((uVisits) => {
            let expired = false,
                visit = uVisits.find((uVisit) => {
                    return uVisit.get('subjectKrpt') === params.subject.get('krpt') &&
                        uVisit.get('visitId') === params.visit.id;
                });

            if (visit != null) {
                let visitDate = dateTimeUtil.parseDateTimeIsoNoOffset(visit.get('dateStarted')),
                    now = new Date(),
                    diff = PDE.DateTimeUtils.dateDiffInDays(now, visitDate);
                expired = diff >= 1;
            }

            resolve(expired);
        });
    });
}

LF.Visits.availabilityFunctions.visitExpiresAfterMidnight = visitExpiresAfterMidnight;
