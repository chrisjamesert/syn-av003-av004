export default {
    visits: [{
        id: 'visit_2_week_0_av003', // DONE
        studyEventId: '10',
        displayName: 'VISIT_2_WEEK_0_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 1,
        forms: [
            'TabletTrainingModule',
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'

        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: false,
        isBaseline: true
    }, {
        id: 'visit_3_week_4_av003', // DONE
        studyEventId: '20',
        displayName: 'VISIT_3_WEEK_4_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 2,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_4_week_8_av003', // DONE
        studyEventId: '30',
        displayName: 'VISIT_4_WEEK_8_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 3,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_5_week_16_av003', // DONE
        studyEventId: '40',
        displayName: 'VISIT_5_WEEK_16_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 4,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_6_week_24_av003', // DONE
        studyEventId: '50',
        displayName: 'VISIT_6_WEEK_24_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 5,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_7_week_36_av003', // DONE
        studyEventId: '60',
        displayName: 'VISIT_7_WEEK_36_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 6,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_8_week_48_av003', // DONE
        studyEventId: '70',
        displayName: 'VISIT_8_WEEK_48_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 7,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_9_week_60_av003', // DONE
        studyEventId: '80',
        displayName: 'VISIT_9_WEEK_60_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 8,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_10_week_72_av003', // DONE
        studyEventId: '90',
        displayName: 'VISIT_10_WEEK_72_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 9,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_11_week_84_av003', // DONE
        studyEventId: '100',
        displayName: 'VISIT_11_WEEK_84_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 10,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_12_week_96_av003', // DONE
        studyEventId: '110',
        displayName: 'VISIT_12_WEEK_96_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 11,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_13_week_108_av003', // DONE
        studyEventId: '120',
        displayName: 'VISIT_13_WEEK_108_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 12,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_14_week_120_av003', // DONE
        studyEventId: '130',
        displayName: 'VISIT_14_WEEK_120_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 13,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_15_week_132_av003', // DONE
        studyEventId: '140',
        displayName: 'VISIT_15_WEEK_132_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 14,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_16_week_144_av003', // DONE
        studyEventId: '150',
        displayName: 'VISIT_16_WEEK_144_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 15,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_17_week_156_av003', // DONE
        studyEventId: '160',
        displayName: 'VISIT_17_WEEK_156_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 16,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_18_week_168_av003', // DONE
        studyEventId: '170',
        displayName: 'VISIT_18_WEEK_168_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 17,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_19_week_180_av003', // DONE
        studyEventId: '180',
        displayName: 'VISIT_19_WEEK_180_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 18,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_20_week_192_av003', // DONE
        studyEventId: '190',
        displayName: 'VISIT_20_WEEK_192_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 19,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_21_week_204_av003', // DONE
        studyEventId: '195',
        displayName: 'VISIT_21_WEEK_204_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 20,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'end_of_study_av003', // DONE
        studyEventId: '800',
        displayName: 'END_OF_STUDY_AV003_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 21,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: false,
        isFinalScheduled: true
    }, {
        id: 'premature_discontinuation_av003', // DONE
        studyEventId: '810',
        displayName: 'PREMATURE_DISCONTINAUTION_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 22,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: false,
        isDiscontinuation: true
    }, {
        id: 'unscheduled_visit_1_av003', // DONE
        studyEventId: '300',
        displayName: 'UNSCHEDULED_VISIT_1_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 23,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_2_av003', // DONE
        studyEventId: '310',
        displayName: 'UNSCHEDULED_VISIT_2_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 24,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_3_av003', // DONE
        studyEventId: '320',
        displayName: 'UNSCHEDULED_VISIT_3_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 25,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_4_av003', // DONE
        studyEventId: '330',
        displayName: 'UNSCHEDULED_VISIT_4_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 26,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_5_av003', // DONE
        studyEventId: '340',
        displayName: 'UNSCHEDULED_VISIT_5_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 27,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_6_av003', // DONE
        studyEventId: '350',
        displayName: 'UNSCHEDULED_VISIT_6_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 28,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_7_av003', // DONE
        studyEventId: '360',
        displayName: 'UNSCHEDULED_VISIT_7_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 29,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_8_av003', // DONE
        studyEventId: '370',
        displayName: 'UNSCHEDULED_VISIT_8_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 30,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_9_av003', // DONE
        studyEventId: '380',
        displayName: 'UNSCHEDULED_VISIT_9_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 31,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_10_av003', // DONE
        studyEventId: '390',
        displayName: 'UNSCHEDULED_VISIT_10_AV003_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 32,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'training_visit', // DONE
        studyEventId: '998',
        displayName: 'TRAINING_VISIT_DISPLAYNAME',
        visitType: 'container',
        visitOrder: null,
        forms: [
            'TabletTrainingModule'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003',
            'AV004'
        ],
        skippable: false
    }, {
        id: 'deactivation', // DONE
        studyEventId: '999',
        displayName: 'DEACTIVATION_DISPLAYNAME',
        visitType: 'container',
        visitOrder: null,
        forms: [
            'TabletDeactivation'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV003',
            'AV004'
        ],
        skippable: false
    }, {
        id: 'visit_2_week_0_av004', // DONE
        studyEventId: '200',
        displayName: 'VISIT_2_WEEK_0_AV004_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 1,
        forms: [
            'TabletTrainingModule',
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: false,
        isBaseline: true
    }, {
        id: 'visit_4_week_4_av004', // DONE
        studyEventId: '210',
        displayName: 'VISIT_4_WEEK_4_AV004_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 2,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_5_week_8_av004', // DONE
        studyEventId: '220',
        displayName: 'VISIT_5_WEEK_8_AV004_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 3,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'visit_6_week_12_av004', // DONE
        studyEventId: '230',
        displayName: 'VISIT_6_WEEK_12_AV004_DISPLAYNAME',
        visitType: 'ordered',
        visitOrder: 4,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: false,
        isFinalScheduled: true
    }, {
        id: 'premature_discontinuation_av004', // DONE
        studyEventId: '240',
        displayName: 'PREMATURE_DISCONTINAUTION_AV004_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 5,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: false,
        isDiscontinuation: true
    }, {
        id: 'unscheduled_visit_1_av004', // DONE
        studyEventId: '250',
        displayName: 'UNSCHEDULED_VISIT_1_AV004_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 6,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_2_av004', // DONE
        studyEventId: '260',
        displayName: 'UNSCHEDULED_VISIT_2_AV004_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 7,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_3_av004', // DONE
        studyEventId: '270',
        displayName: 'UNSCHEDULED_VISIT_3_AV004_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 8,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_4_av004', // DONE
        studyEventId: '280',
        displayName: 'UNSCHEDULED_VISIT_4_AV004_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 9,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'unscheduled_visit_5_av004', // DONE
        studyEventId: '290',
        displayName: 'UNSCHEDULED_VISIT_5_AV004_DISPLAYNAME',
        visitType: 'unscheduled',
        visitOrder: 10,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder'
        ],
        delay: '0',
        phase: [
            'TREATMENT'
        ],
        protocol: [
            'AV004'
        ],
        skippable: true,
        expiration: {
            function: 'visitExpiresAfterMidnight'
        }
    }, {
        id: 'pdeVisit',
        studyEventId: '-9999',
        displayName: 'PDE Visit',
        visitType: 'container',
        visitOrder: 999,
        forms: [
            'ACT',
            'AQLQS12T',
            'ChildACT',
            'PAQLQReminder',
            'TabletTrainingModule'
        ],
        delay: '0',
        phase: ['TREATMENT'],
        protocol: [
            'AV003',
            'AV004'
        ],
        skippable: false
    }

        // Training Visit and Deactivation for AV004 are duplicated with AV003 using the protocol array.
    ],

    // Server value map. value maps to LF.VisitStates (see application.js)
    visitStatus: {
        1: 'SKIPPED',
        2: 'INCOMPLETE',
        3: 'COMPLETED'
    },

    visitAction: {
        show: 'show',
        hide: 'hide',
        notAvailable: 'notAvailable'
    }
};
