
/**
 * @fileOverview This is an example study configuration of schedules used for core development.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.0
 */
// A collection of schedule configurations
export default {
    schedules: [
        {
            id: 'AddNewUser_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'AddNewUser_Diary'
            },
            scheduleRoles: ['admin'],
            scheduleFunction: 'checkAlwaysAvailability'
        },
        {
            id: 'AutoDial',
            target: {
                objectType: 'autodial'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 1,
                time: {
                    startTimeRange: '02:00',
                    endTimeRange: '03:00'
                },
                repeat: 'daily'
            }
        }, {
            id: 'ACT_schedule',
            target: {
                objectType: 'questionnaire',
                id: 'ACT'
            },
            scheduleFunction: 'PDEnTimesPerVisit',
            scheduleParams: {
                ageGroup: {
                    min: 12,
                    max: 999
                },
                AV003: {
                    visits: [
                        { visitID: 'visit_2_week_0_av003', maxReports: '1', predecessorID: 'TabletTrainingModule' },
                        { visitID: 'visit_3_week_4_av003', maxReports: '1' },
                        { visitID: 'visit_4_week_8_av003', maxReports: '1' },
                        { visitID: 'visit_5_week_16_av003', maxReports: '1' },
                        { visitID: 'visit_6_week_24_av003', maxReports: '1' },
                        { visitID: 'visit_7_week_36_av003', maxReports: '1' },
                        { visitID: 'visit_8_week_48_av003', maxReports: '1' },
                        { visitID: 'visit_9_week_60_av003', maxReports: '1' },
                        { visitID: 'visit_10_week_72_av003', maxReports: '1' },
                        { visitID: 'visit_11_week_84_av003', maxReports: '1' },
                        { visitID: 'visit_12_week_96_av003', maxReports: '1' },
                        { visitID: 'visit_13_week_108_av003', maxReports: '1' },
                        { visitID: 'visit_14_week_120_av003', maxReports: '1' },
                        { visitID: 'visit_15_week_132_av003', maxReports: '1' },
                        { visitID: 'visit_16_week_144_av003', maxReports: '1' },
                        { visitID: 'visit_17_week_156_av003', maxReports: '1' },
                        { visitID: 'visit_18_week_168_av003', maxReports: '1' },
                        { visitID: 'visit_19_week_180_av003', maxReports: '1' },
                        { visitID: 'visit_20_week_192_av003', maxReports: '1' },
                        { visitID: 'visit_21_week_204_av003', maxReports: '1' },
                        { visitID: 'end_of_study_av003', maxReports: '1' },
                        { visitID: 'premature_discontinuation_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_1_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_6_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_7_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_8_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_9_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_10_av003', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                },
                AV004: {
                    visits: [
                        { visitID: 'visit_2_week_0_av004', maxReports: '1', predecessorID: 'TabletTrainingModule' },
                        { visitID: 'visit_4_week_4_av004', maxReports: '1' },
                        { visitID: 'visit_5_week_8_av004', maxReports: '1' },
                        { visitID: 'visit_6_week_12_av004', maxReports: '1' },
                        { visitID: 'premature_discontinuation_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_1_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av004', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                }
            },
            scheduleRoles: ['subject', 'site', 'admin']
        }, {
            id: 'AQLQS12T_schedule',
            target: {
                objectType: 'questionnaire',
                id: 'AQLQS12T'
            },
            scheduleFunction: 'PDEnTimesPerVisit',
            scheduleParams: {
                ageGroup: {
                    min: 12,
                    max: 999
                },
                AV003: {
                    visits: [
                        { visitID: 'visit_2_week_0_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_3_week_4_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_4_week_8_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_5_week_16_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_6_week_24_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_7_week_36_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_8_week_48_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_9_week_60_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_10_week_72_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_11_week_84_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_12_week_96_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_13_week_108_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_14_week_120_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_15_week_132_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_16_week_144_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_17_week_156_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_18_week_168_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_19_week_180_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_20_week_192_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_21_week_204_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'end_of_study_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'premature_discontinuation_av003', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'unscheduled_visit_1_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_6_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_7_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_8_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_9_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_10_av003', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                },
                AV004: {
                    visits: [
                        { visitID: 'visit_2_week_0_av004', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_4_week_4_av004', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_5_week_8_av004', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'visit_6_week_12_av004', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'premature_discontinuation_av004', maxReports: '1', predecessorID: 'ACT' },
                        { visitID: 'unscheduled_visit_1_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av004', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                }
            },
            scheduleRoles: ['subject', 'site', 'admin']
        }, {
            id: 'TabletTrainingModule_schedule',
            target: {
                objectType: 'questionnaire',
                id: 'TabletTrainingModule'
            },
            scheduleFunction: 'PDEnTimesPerVisit',
            scheduleParams: {
                AV003: {
                    visits: [
                        { visitID: 'visit_2_week_0_av003', maxReports: '1' },
                        { visitID: 'training_visit', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                },
                AV004: {
                    visits: [
                        { visitID: 'visit_2_week_0_av004', maxReports: '1' },
                        { visitID: 'training_visit', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                }
            },
            scheduleRoles: ['subject', 'site', 'admin']
        }, {
            id: 'ChildACT_schedule',
            target: {
                objectType: 'questionnaire',
                id: 'ChildACT'
            },
            scheduleFunction: 'PDEnTimesPerVisit',
            scheduleParams: {
                ageGroup: {
                    min: 4,
                    max: 11
                },
                AV003: {
                    visits: [
                        { visitID: 'visit_2_week_0_av003', maxReports: '1', predecessorID: 'TabletTrainingModule' },
                        { visitID: 'visit_3_week_4_av003', maxReports: '1' },
                        { visitID: 'visit_4_week_8_av003', maxReports: '1' },
                        { visitID: 'visit_5_week_16_av003', maxReports: '1' },
                        { visitID: 'visit_6_week_24_av003', maxReports: '1' },
                        { visitID: 'visit_7_week_36_av003', maxReports: '1' },
                        { visitID: 'visit_8_week_48_av003', maxReports: '1' },
                        { visitID: 'visit_9_week_60_av003', maxReports: '1' },
                        { visitID: 'visit_10_week_72_av003', maxReports: '1' },
                        { visitID: 'visit_11_week_84_av003', maxReports: '1' },
                        { visitID: 'visit_12_week_96_av003', maxReports: '1' },
                        { visitID: 'visit_13_week_108_av003', maxReports: '1' },
                        { visitID: 'visit_14_week_120_av003', maxReports: '1' },
                        { visitID: 'visit_15_week_132_av003', maxReports: '1' },
                        { visitID: 'visit_16_week_144_av003', maxReports: '1' },
                        { visitID: 'visit_17_week_156_av003', maxReports: '1' },
                        { visitID: 'visit_18_week_168_av003', maxReports: '1' },
                        { visitID: 'visit_19_week_180_av003', maxReports: '1' },
                        { visitID: 'visit_20_week_192_av003', maxReports: '1' },
                        { visitID: 'visit_21_week_204_av003', maxReports: '1' },
                        { visitID: 'end_of_study_av003', maxReports: '1' },
                        { visitID: 'premature_discontinuation_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_1_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_6_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_7_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_8_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_9_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_10_av003', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                },
                AV004: {
                    visits: [
                        { visitID: 'visit_2_week_0_av004', maxReports: '1', predecessorID: 'TabletTrainingModule' },
                        { visitID: 'visit_4_week_4_av004', maxReports: '1' },
                        { visitID: 'visit_5_week_8_av004', maxReports: '1' },
                        { visitID: 'visit_6_week_12_av004', maxReports: '1' },
                        { visitID: 'premature_discontinuation_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_1_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av004', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                }
            },
            scheduleRoles: ['subject', 'site', 'admin']
        }, {
            id: 'PAQLQReminder_schedule',
            target: {
                objectType: 'questionnaire',
                id: 'PAQLQReminder'
            },
            scheduleFunction: 'PDEnTimesPerVisit',
            scheduleParams: {
                ageGroup: {
                    min: 4,
                    max: 11
                },
                AV003: {
                    visits: [
                        { visitID: 'visit_2_week_0_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_3_week_4_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_4_week_8_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_5_week_16_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_6_week_24_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_7_week_36_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_8_week_48_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_9_week_60_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_10_week_72_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_11_week_84_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_12_week_96_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_13_week_108_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_14_week_120_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_15_week_132_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_16_week_144_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_17_week_156_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_18_week_168_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_19_week_180_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_20_week_192_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_21_week_204_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'end_of_study_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'premature_discontinuation_av003', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'unscheduled_visit_1_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_6_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_7_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_8_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_9_av003', maxReports: '1' },
                        { visitID: 'unscheduled_visit_10_av003', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                },
                AV004: {
                    visits: [
                        { visitID: 'visit_2_week_0_av004', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_4_week_4_av004', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_5_week_8_av004', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'visit_6_week_12_av004', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'premature_discontinuation_av004', maxReports: '1', predecessorID: 'ChildACT' },
                        { visitID: 'unscheduled_visit_1_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_2_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_3_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_4_av004', maxReports: '1' },
                        { visitID: 'unscheduled_visit_5_av004', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                }
            },
            scheduleRoles: ['subject', 'site', 'admin']
        }, {
            id: 'TabletDeactivation_schedule',
            target: {
                objectType: 'questionnaire',
                id: 'TabletDeactivation'
            },
            scheduleFunction: 'PDEnTimesPerVisit',
            scheduleParams: {
                AV003: {
                    visits: [
                        { visitID: 'deactivation', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                },
                AV004: {
                    visits: [
                        { visitID: 'deactivation', maxReports: '1' },
                        { visitID: 'pdeVisit', maxReports: '20000' }
                    ]
                }
            },
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
