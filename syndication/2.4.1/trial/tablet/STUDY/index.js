/*
 * PDE_TODO:
 * import files that don't contribute to assets object, but need to ensure the content executes
 * for example, a file that assigns a function into the global LF namespace
 */
// import './exampleBranchingFunctions';
// import './exampleUtilities';
// import './exampleScheduleFunctions';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};

/*
 * PDE_TODO:
 * Collect and merge studyDesign objects
 */
// import rules from './exampleRules';
// import messages from './exampleMessages';
// import templates from './exampleTemplates';
// import dynamicText from './exampleDynamicText';

import SD from './study-design';
import schedules from './tablet_study-schedules';
import rules from './rules';
import './schedules';
import './actions';

studyDesign = mergeObjects(studyDesign, SD, schedules, rules);


/* export the assets object*/
export default { studyDesign };
