import Logger from 'core/Logger';
import UserVisits from 'sitepad/collections/UserVisits';
import { isVisitInProgress } from 'PDE_Core/tablet/utilities/tablet_VisitUtils';

export default {
    rules: [
        {
            id: 'DetermineVisitStatus',
            trigger: 'VISITGATEWAY:BeforeRenderVisits',
            salience: 10,
            resolve: [
                {
                    action: (params, done) => {
                        let collection = params.collection;
                        let subject = params.subject;

                        // Map a given user visit to the study configuration visit.
                        let getStudyVisit = function (userVisit) {
                            return LF.StudyDesign.visits.find(visit => visit.get('id') === userVisit.get('visitId'));
                        };
                        let userVisits = new UserVisits();
                        userVisits.fetch().then((uVisits) => {
                            // Get the last started visit for this subject.  The visit is not
                            // necessarily the last completed visit - it could be in progress.
                            // Skipped visits and container-type visits are not considered.
                            let subjectVisits = _.filter(uVisits, (uVisit) => {
                                return (uVisit.get('subjectKrpt') === subject.get('krpt')) &&
                                    (uVisit.get('state') !== LF.VisitStates.SKIPPED) &&
                                    (getStudyVisit(uVisit).get('visitType') !== 'container');
                            });
                            let lastStartedVisit = _.chain(subjectVisits)
                                .sortBy((v) => {
                                    return LF.Utilities.parseDateTimeIsoNoOffset(v.get('dateStarted'));
                                })
                                .reverse()
                                .first()
                                .value();

                            let isCurrentPhase = (visit) => {
                                // the visit object can have a phases property with an array value
                                // if it does check that the subjects current phase matches one of these
                                let subjectPhase = subject.get('phase'),
                                    visitPhases = visit.get('phases');

                                if (visitPhases && visitPhases.length) { // !visitPhases.includes(subjectPhase)
                                    let phaseMatched = false;
                                    visitPhases.forEach((phase) => {
                                        if (LF.StudyDesign.studyPhase[phase] === subjectPhase) {
                                            phaseMatched = true;
                                        }
                                    });
                                    return phaseMatched;
                                }
                                return true;
                            };
                            let isVisitAvailableNow = function (subject, visit) {
                                // Easter-egg!  Allow developer to override minimum number of days between visits.
                                if (LF.Utilities.DevConfig.allowMultiVisitDays === true) {
                                    return true;
                                }
                                if (lastStartedVisit !== undefined) {
                                    // check to see if subject phase is same as the phases defined on the visit
                                    if (!isCurrentPhase(visit)) return false;

                                    let lastVisitDate = LF.Utilities.parseDateTimeIsoNoOffset(lastStartedVisit.get('dateStarted'));
                                    let daysSinceLastVisit = Math.abs(PDE.DateTimeUtils.dateDiffInDays(lastVisitDate, new Date()));
                                    let minDaysFromLastVisit = visit.get('delay') !== undefined ? parseInt(visit.get('delay')) : 0;
                                    if (daysSinceLastVisit < minDaysFromLastVisit) {
                                        return false;
                                    }
                                }
                                return true;
                            };
                            collection.each((model) => {
                                let { visit, userVisit, action } = model.toJSON();
                                if (visit.get('visitType') === 'container') {
                                    model.set('action', LF.StudyDesign.visitAction.Available);
                                } else {
                                    if ((userVisit === undefined) || (userVisit.get('state') === LF.VisitStates.AVAILABLE)) {
                                        if (!isVisitAvailableNow(subject, visit)) {
                                            model.set('action', LF.StudyDesign.visitAction.notAvailable);
                                        }
                                    }
                                }
                            });
                            done();
                        });
                    }
                }
            ]
        },

        /**
         * Rule used to filter Visits collection before gateway render.
         */
        {
            id: 'VisitFilter',
            trigger: 'VISITGATEWAY:BeforeRenderVisits',
            salience: 0,
            resolve: [
                {
                    action: (params, callback) => {
                        /**  Boilerplate code usable by any study **/
                        if (PDE.isDev === true) {
                            callback(true);
                            return;
                        }
                        let collection = params.collection;
                        let subject = params.subject;
                        let phaseID = subject.get('phase');
                        let protocol = subject.get('custom10') == 3 ? 'AV003' : 'AV004';
                        let studyPhases = LF.StudyDesign.studyPhase;
                        let visitActions = LF.StudyDesign.visitAction;
                        let logger = new Logger('PDEVisitFilter');

                        /**
                         * Return the VisitRow model object associated with the specific visit ID
                         * in the visits collection used to render the visits gateway.
                         * @param {String} visitID the visit ID
                         * @returns {Object} the visit model
                         */
                        let getModel = (visitID) => {
                            let model = collection.models.find((uModel) => {
                                let visit = uModel.get('visit');
                                return (visit.id === visitID);
                            });
                            return (model);
                        };
                        let hideVisit = (visitID, untouchedOnly) => {
                            return Q().then(() => {
                                let model = getModel(visitID);
                                if (untouchedOnly) {
                                    if (!LF.Utilities.isVisitTouched(model.get('userVisit'))) {
                                        model.set('action', visitActions.hide);
                                    }
                                } else {
                                    model.set('action', visitActions.hide);
                                }
                            });
                        };

                        /**
                         * Hide a range of visits.
                         * @param {Array} visitIDs and array of visit IDs
                         * @param {boolean} [untouchedOnly] if true, only hide a visit if it has not been touched
                         *                                if false, unconditionally hide it
                         */
                        let hideVisits = (visitIDs, untouchedOnly) => {
                            let promiseArray = visitIDs.map((visitID) => {
                                return hideVisit(visitID, untouchedOnly);
                            });
                            return Q.all(promiseArray).catch((err) => {
                                logger.error(`Failed to hide a visit from the list ${visitIDs}`, err);
                                return undefined;
                            });
                        };
                        let unhideVisit = (visitID) => {
                            return Q().then(() => {
                                let model = getModel(visitID);
                                model.set('action', visitActions.show);
                            });
                        };

                        /*
                         * Unhide a range of visits.
                         * @param {Array} visitIDs and array of visit IDs
                         */
                        let unhideVisits = (visitIDs) => {
                            let promiseArray = visitIDs.map((visitID) => {
                                return unhideVisit(visitID);
                            });
                            return Q.all(promiseArray).catch((err) => {
                                logger.error(`Failed to show a visit from the list ${visitIDs}`, err);
                            });
                        };

                        /*
                         * Constrain a list of visits to show only a specific number
                         * available at a time.
                         * @param {Number} n the number to be displayed at a time
                         * @param {string[]} visitIDs the list of visits IDs.  Must be in order as they
                         * appear on the visit gateway.
                         */
                        let showAvailableAtOneTime = (n, visitIDs) => {
                            for (let i = 0; i < visitIDs.length; i++) {
                                let model = getModel(visitIDs[i]);
                                let userVisit = model.get('userVisit');
                                if (!(LF.Utilities.isVisitCompleted(userVisit) ||
                                        LF.Utilities.isVisitAborted(userVisit) ||
                                        LF.Utilities.isVisitExpired(userVisit))) {
                                    // Visit is available, so hide all following visits at the specified rate.
                                    for (let k = i + n; k < visitIDs.length; k++) {
                                        model = getModel(visitIDs[k]);
                                        if (model !== undefined) {
                                            model.set('action', visitActions.hide);
                                        }
                                    }
                                    break;
                                }
                            }
                        };

                        /**  End Boilerplate code usable by any study **/
                        /**  Everything below here is study custom **/

                        let discontinuationVisit = [];
                        collection.models.forEach((visitRow) => {
                            let visit = visitRow.get('visit');
                            if (visit.get('isDiscontinuation') &&
                                    visit.get('protocol').includes(protocol)) {
                                discontinuationVisit.push(visit.get('id'));
                            }
                        });

                        if (discontinuationVisit.length === 0) {
                            logger.error(`Could not find a discontinuation visit for protocol ${protocol} (isDiscontinuation: true)`);
                        } else if (discontinuationVisit.length > 1) {
                            logger.error(`Found ${discontinuationVisit.length} discontinuation visits for protocol ${protocol}. Protocols can have only 1 discontinuation visit (isDiscontinuation: true)`);
                        }

                        let finalScheduledVisit = [];
                        collection.models.forEach((visitRow) => {
                            let visit = visitRow.get('visit');
                            if (visit.get('isFinalScheduled') &&
                                visit.get('protocol').includes(protocol)) {
                                finalScheduledVisit.push(visit.get('id'));
                            }
                        });

                        if (finalScheduledVisit.length === 0) {
                            logger.error(`Could not find a final scheduled visit for protocol ${protocol} (isFinalScheduled: true)`);
                        } else if (finalScheduledVisit.length > 1) {
                            logger.error(`Found ${finalScheduledVisit.length} final scheduled visits for protocol ${protocol}. Protocols can have only 1 final scheduled visit (isFinalScheduled: true)`);
                        }

                        let unscheduledVisits = [];
                        collection.models.forEach((visitRow) => {
                            let visit = visitRow.get('visit');
                            if (visit.get('visitType') === 'unscheduled' &&
                                visit.get('protocol').includes(protocol) &&
                                !visit.get('isDiscontinuation')) {
                                unscheduledVisits.push(visit.get('id'));
                            }
                        });

                        showAvailableAtOneTime(1, unscheduledVisits);
                        hideVisit('pdeVisit');
                        if (LF.Utilities.DevConfig.isPDEVisitEnabled) {
                            unhideVisit('pdeVisit');
                        }
                        let baselineVisit = [],
                            baselineVisitRows = collection.models.filter((visitRow) => {
                                let visit = visitRow.get('visit');
                                return visit.get('isBaseline') &&
                                    visit.get('protocol').includes(protocol);
                            });

                        if (baselineVisitRows.length === 0) {
                            logger.error(`No baseline visit found for protocol ${protocol}. Set isBaseline: true for the baseline visit.`);
                        } else if (baselineVisitRows.length < 1) {
                            logger.error(`Fournd ${baselineVisitRows.length} baseline visits for protocol ${protocol}. There can only be 1 baseline visit (isBaseline: true)`);
                        }
                        baselineVisit.push(baselineVisitRows[0].get('visit').get('id'));
                        let remainingScheduledVisits = [];
                        collection.models.forEach((visitRow) => {
                            let visit = visitRow.get('visit');
                            if (visit.get('visitType') === 'ordered' &&
                                visit.get('protocol').includes(protocol) &&
                                !visit.get('isBaseline') &&
                                !visit.get('isFinalScheduled')) {
                                remainingScheduledVisits.push(visit.get('id'));
                            }
                        });

                        let nonProtocolVisits = [];
                        collection.models.forEach((visitRow) => {
                            let visit = visitRow.get('visit');
                            if (!visit.get('protocol').includes(protocol)) {
                                nonProtocolVisits.push(visit.get('id'));
                            }
                        });

                        let trainingVisit = ['training_visit'],
                            deactivationVisit = ['deactivation'],
                            baselineVisitModel = getModel(baselineVisit[0]).get('userVisit'),
                            discontinuationVisitModel = getModel(discontinuationVisit[0]).get('userVisit'),
                            finalScheduledVisitModel = getModel(finalScheduledVisit[0]).get('userVisit'),
                            hideVisitList = [];
                        if (isVisitInProgress(baselineVisitModel)) {
                            hideVisitList = hideVisitList.concat(
                                remainingScheduledVisits,
                                finalScheduledVisit,
                                discontinuationVisit,
                                unscheduledVisits,
                                nonProtocolVisits
                            );
                        } else if (phaseID == LF.StudyDesign.studyPhase.DEACTIVATION) {
                            // Hide everything
                            hideVisitList = hideVisitList.concat(
                                baselineVisit,
                                remainingScheduledVisits,
                                finalScheduledVisit,
                                discontinuationVisit,
                                unscheduledVisits,
                                trainingVisit,
                                nonProtocolVisits,
                                deactivationVisit
                            );
                        } else if (phaseID == LF.StudyDesign.studyPhase.PENDING_DEACTIVATION) {
                            hideVisitList = hideVisitList.concat(
                                baselineVisit,
                                remainingScheduledVisits,
                                finalScheduledVisit,
                                discontinuationVisit,
                                unscheduledVisits,
                                trainingVisit,
                                nonProtocolVisits
                            );
                        } else if (!LF.Utilities.isVisitCompleted(baselineVisitModel)) {
                            hideVisitList = hideVisitList.concat(
                                remainingScheduledVisits,
                                finalScheduledVisit,
                                nonProtocolVisits);
                        } else {
                            hideVisitList = nonProtocolVisits;

                            // Spec. ID 5.1.6
                            // If Premature Discontinuation Visit is mutually exclusive with the last scheduled visit,
                            // if one is started another becomes unavailable.
                            if (isVisitInProgress(discontinuationVisitModel)) {
                                hideVisitList = hideVisitList.concat(
                                    finalScheduledVisit,
                                    unscheduledVisits
                                );
                            } else if (isVisitInProgress(finalScheduledVisitModel)) {
                                hideVisitList = hideVisitList.concat(
                                    discontinuationVisit,
                                    unscheduledVisits
                                );
                            }
                        }

                        let promiseChainStart = Q(hideVisitList);

                        // Execute the chain, do not return the chain because this action has a callback parameter,
                        // but consider rewriting rule to use the promise action structure instead.
                        promiseChainStart.then((visitsToHide) => {
                            return hideVisits(visitsToHide,
                                phaseID != LF.StudyDesign.studyPhase.DEACTIVATION);
                        }).finally(() => {
                            callback();
                        });
                    }
                }
            ]
        },
        {
            id: 'changePhase_PendingDeactivation',
            trigger: [
                'PDE_VISITBORDER:End/end_of_study_av003',
                'PDE_VISITBORDER:End/premature_discontinuation_av003',
                'PDE_VISITBORDER:End/visit_6_week_12_av004',
                'PDE_VISITBORDER:End/premature_discontinuation_av004'
            ],
            evaluate: function (filter, resume) {
                resume(parseInt(filter.subject.get('phase'), 10) < LF.StudyDesign.studyPhase.PENDING_DEACTIVATION);
            },
            salience: 10,
            resolve: [
                {
                    action: 'changePhaseOnVisit',
                    data: (input, callback) => {
                        callback(_(input).extend({
                            change: true,
                            phase: LF.StudyDesign.studyPhase.PENDING_DEACTIVATION,
                            phaseStartDateTZOffset: LF.Utilities.timeStamp(new Date())
                        }));
                    }
                }
            ]
        },
        {
            id: 'SaveVST1L',
            trigger: [
                'QUESTIONNAIRE:Completed/ChildACT',
                'QUESTIONNAIRE:Completed/PAQLQReminder',
                'QUESTIONNAIRE:Completed/AQLQS12T',
                'QUESTIONNAIRE:Completed/ACT'
            ],
            evaluate (filter, resume) {
                if (PDE.DeviceStatusUtils.isScreenshotMode()) {
                    resume(false);
                    return;
                }
                resume(true);
            },
            salience: 10,
            resolve: [
                {
                    action (input, done) {
                        // Send VST1L when needed
                        let ig = input.questionnaire,
                            currentVisitID = this.visit.get('studyEventId');
                        if (parseInt(currentVisitID, 10) === -9999) {
                            done();
                            return;
                        }
                        if (ig === 'PAQLQReminder') {
                            ig = 'PAQLQ';
                        }
                        this.addIT({
                            question_id: 'VST1L',
                            questionnaire_id: input.questionnaire,
                            response: currentVisitID,
                            IG: ig,
                            IGR: 0,
                            IT: 'VST1L'
                        });
                        done();
                    }
                }
            ]
        }
    ]
};
