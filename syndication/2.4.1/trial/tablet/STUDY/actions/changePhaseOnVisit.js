import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import ObjectRef from 'core/classes/ObjectRef';
import ELF from 'core/ELF';

/**
 * @description
 * ELF action for use only on PDE_VISITBORDER triggers, which includes
 * PDE_VISITBORDER:Start/{visitID}
 * PDE_VISITBORDER:End/{visitID}
 * @param {Object} input - the parameters passed into the function
 * @param {Subject} input.subject - current Subject model, handled by trigger
 * @param {Visit} input.visit - current Visit model, handled by trigger
 * @param {Object} input.dashboard - the dashboard for the visit start/end form, handled by trigger
 * @param {boolean} [input.change] - Optional true if phase should be changed, handled by data of rule
 * @param {string|Number} input.phase - Required if input.change === true, value phase should be changed to, handled by data of rule
 * @param {string} input.phaseStartDateTZOffset - Required if input.change === true, the timestamp of the phase change, handled by data of rule
 * @return {*|Promise<T>}
 *
 * @example
 * {
 *  id: 'changePhaseVisit3End',
 *  trigger: 'PDE_VISITBORDER:End/visit3',
 *  evaluate: true,
 *  salience: 10,
 *  resolve: [{
 *              action : 'changePhaseOnVisit',
 *              data : (input, callback) => {
 *                          callback(_(input).extend({
 *                                      change : true,
 *                                      phase : LF.StudyDesign.studyPhase.TREATMENT,
 *                                      phaseStartDateTZOffset : LF.Utilities.timeStamp(new Date())
 *                                   }));
 *                     }
 *           }]
 * }
 *
 */
export function changePhaseOnVisit (input) {
    let logger = new Logger('PDE_Action:changePhaseOnVisit');
    return Q()
        .then(() => {
            // check for required input items from trigger
            if (!(input && input.subject && input.visit && input.dashboard)) {
                logger.error('changePhaseOnVisit is not called on a PDE_VISITBORDER trigger, no change made.');
                throw new Error();
            }

            // check for required input items from data, if applicable
            else if (input && input.change && !(input.phase && input.phaseStartDateTZOffset)) {
                logger.error('change === true, expecting phase and phaseStartDateTZOffset');
                throw new Error();
            }

            // if we're changing, return true for the next .then resolve
            else if (input && input.change) {
                logger.info('change === true, attempting to update phase');
                return true;
            }

            // otherwise, throw error skip the rest of the logic, but catch the error and continue the ELF chain
            else {
                logger.info('change !== true, not attempting to update phase');
                throw new Error();
            }
        })
        .then(() => {
            // check that the phase exists in the StudyDesign
            let phase = false;
            switch (typeof input.phase) {
                case 'string':
                    if (isNaN(input.phase)) {
                        phase = LF.StudyDesign.studyPhase[input.phase];
                    } else {
                        let phaseNames = ObjectRef.keys(LF.StudyDesign.studyPhase);

                        // noinspection FunctionWithInconsistentReturnsJS
                        _(phaseNames).forEach((name) => {
                            if (LF.StudyDesign.studyPhase[name] === Number(input.phase)) {
                                phase = Number(input.phase);

                                // return false to break lodash forEach
                                return false;
                            }
                        });
                    }
                    break;
                case 'number':
                    let phaseNames = ObjectRef.keys(LF.StudyDesign.studyPhase);

                    // noinspection FunctionWithInconsistentReturnsJS
                    _(phaseNames).forEach((name) => {
                        if (LF.StudyDesign.studyPhase[name] === input.phase) {
                            phase = input.phase;

                            // return false to break lodash forEach
                            return false;
                        }
                    });
            }
            if (phase) {
                logger.info('requested phase exists, performing phase change');

                input.subject.save({
                    phase,
                    phaseStartDateTZOffset: input.phaseStartDateTZOffset,
                    phaseTriggered: 'false'
                });

                input.dashboard.save({
                    change_phase: input.change ? 'true' : 'false',
                    phase,
                    phaseStartDateTZOffset: input.phaseStartDateTZOffset
                });

                lStorage.setItem('outOfSync', true);
            } else {
                logger.error(`No phase with code ${phase} exists, phase update will not be performed.`);
            }
        }).catch(() => {
            return true;
        });
}

ELF.action('changePhaseOnVisit', changePhaseOnVisit);
