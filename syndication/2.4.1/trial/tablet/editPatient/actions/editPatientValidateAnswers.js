import ELF from 'core/ELF';
import Spinner from 'core/Spinner';
import { MessageRepo } from 'core/Notify';
import preventAll from 'core/actions/preventAll';

/**
 * Validates the answers of the Edit Patient Diary
 * @memberOf ELF.actions/sitepad
 * @method editPatientValidateAnswers
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'editPatientValidateAnswers' }]
 */
export function editPatientValidateAnswers () {
    let getNumberOfChangedAnswers = () => {
        let diaryAnswers = this.data.answers,
            changedAnswersCounter = 0,
            answer;

        answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' });
        let subjectIdAnswer = answer[0].get('response');
        if (subjectIdAnswer !== this.subject.get('subject_id')) {
            changedAnswersCounter++;
        }

        answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_LANGUAGE' });
        let languageAnswer = JSON.parse(answer[0].get('response')).language;
        if (languageAnswer !== this.user.get('language')) {
            changedAnswersCounter++;
        }

        return changedAnswersCounter;
    };

    if (getNumberOfChangedAnswers() === 0) {
        return Spinner.hide()

        // Displays a modal informing the user that no changes have been made.
            .then(() => MessageRepo.display(MessageRepo.Dialog[`${this.id}_NO_CHANGES`]))
            .then(() => {
                // When the OK button is clicked, it will cancel the edit patient diary by navigating out.
                this.navigate(this.defaultRouteOnExit, true, this.defaultFlashParamsOnExit);
                return preventAll();
            })
            .catch(() => preventAll());
    }

    return Q();
}

ELF.action('editPatientValidateAnswers', editPatientValidateAnswers);
