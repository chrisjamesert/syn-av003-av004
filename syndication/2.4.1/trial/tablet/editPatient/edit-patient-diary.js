// This is a core questionnaire required for the Edit Patient workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Edit_Patient)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [{
        id: 'Edit_Patient',
        SU: 'Edit_Patient',
        displayName: 'EDIT_PATIENT',
        className: 'EDIT_PATIENT',

        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'EditPatientSignatureAffidavit',

        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        screens: [
            'EDIT_PATIENT_S_1',
            'EDIT_PATIENT_S_2',
            'EDIT_PATIENT_S_3'
        ],
        branches: [],

        // Only Site Users and System Adminstrators may access this questionnaire.
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'EDIT_PATIENT_S_1',
        className: 'EDIT_PATIENT_S_1',

        // DE16954 - Disable the back button on this screen only.
        disableBack: true,
        questions: [
            { id: 'EDIT_PATIENT_S1_INFO' },
            { id: 'EDIT_PATIENT_ID', mandatory: true }
        ]
    }, {
        id: 'EDIT_PATIENT_S_2',
        className: 'EDIT_PATIENT_S_2',
        questions: [
            { id: 'EDIT_PATIENT_LANGUAGE', mandatory: true }
        ]
    }, {
        id: 'EDIT_PATIENT_S_3',
        className: 'EDIT_PATIENT_S_3',
        questions: [
            { id: 'EDIT_PATIENT_REASON_INFO' },
            { id: 'EDIT_PATIENT_REASON', mandatory: true }
        ]
    }],

    questions: [{
        id: 'EDIT_PATIENT_S1_INFO',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_S1_INFO',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_1_MAIN_TITLE'],
        className: 'EDIT_PATIENT_S1_INFO'
    }, {
        id: 'EDIT_PATIENT_ID',
        IG: 'PT',
        IT: 'Patientid',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_1'],
        className: 'EDIT_PATIENT_ID',
        widget: {
            id: 'EDIT_PATIENT_W_1',
            type: 'PatientIDTextBox',
            templates: {},
            answers: [],

            // numeric from 1 to 4 characters
            maxLength: 4,
            allowedKeyRegex: /[\d]/,
            validateRegex: /[\d]+/,
            _isUnique: true,
            _isInRange: true,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_1_EMPTY',
                header: 'EDIT_PATIENT_QUESTION_1_EMPTY_HEADER',
                ok: 'OK'
            }, {
                property: 'isInRange',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_1_RANGE',
                header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                ok: 'OK'
            }],
            validation: {
                validationFunc: 'checkEditPatientID',
                params: {
                    errorStrings: {
                        errString: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                        header: 'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                        ok: 'OK',
                        isInRange: {
                            errString: 'EDIT_PATIENT_QUESTION_1_RANGE',
                            header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                            ok: 'OK'
                        },

                        isUnique: {
                            errString: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                            header: 'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                            ok: 'OK'
                        }
                    }
                }
            }
        }
    }, /*{
        id: 'EDIT_PATIENT_PROTOCOL',
        IG: 'Protocol',
        IT: 'Protocol',
        localOnly: true,
        skipIT: '',
        title: '',
        text: [],
        className: '',
        widget: {
            id: 'EDIT_PATIENT_W_PROTOCOL',
            type: 'HiddenField',
            label: 'HiddenProtocolField'
        }
    },*/ {
        id: 'EDIT_PATIENT_LANGUAGE',
        IG: 'Assignment',
        IT: 'Language',
        skipIT: '',
        title: 'EDIT_PATIENT_QUESTION_3_TITLE',
        text: ['EDIT_PATIENT_QUESTION_3'],
        className: '',
        widget: {
            id: 'EDIT_PATIENT_W_3',
            type: 'PDE_SubjectLanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: [],
            validation: {
                validationFunc: 'checkLanguageFieldWidget',
                params: {}
            }
        }
    }, {
        id: 'EDIT_PATIENT_REASON_INFO',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_REASON_INFO',
        title: '',
        text: 'EDIT_PATIENT_QUESTION_4_TITLE',
        className: 'EDIT_PATIENT_REASON_INFO'
    }, {
        id: 'EDIT_PATIENT_REASON',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_REASON',
        title: '',
        text: 'EDIT_PATIENT_QUESTION_4',
        className: 'EDIT_PATIENT_REASON',
        widget: {
            id: 'EDIT_PATIENT_REASON',
            type: 'EditReason',
            className: 'EDIT_PATIENT_REASON',
            answers: [
                { text: 'EDIT_PATIENT_REASON_1', value: '1' },
                { text: 'EDIT_PATIENT_REASON_2', value: '2' },
                { text: 'EDIT_PATIENT_REASON_3', value: '3' }
            ]
        }
    }],

    affidavits: [{
        id: 'EditPatientSignatureAffidavit',
        text: [
            'EDIT_PATIENT_AFFIDAVIT_HEADER',
            'EDIT_PATIENT_AFF'
        ],
        templates: { question: 'AffidavitText' },
        krSig: 'AddSubject',
        widget: {
            id: 'EDIT_PATIENT_SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox',
            configuration: {
                sizeRatio: 3
            }
        }
    }]
};
