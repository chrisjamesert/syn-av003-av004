import getRoleInputType from 'core/widgets/param-functions/getRoleInputType';

export function getYOB_min () {
    let min_year,
        today = new Date();

    min_year = today.getFullYear() - 100;

    return min_year;
}

export function getYOB_max () {
    let max_year,
        today = new Date();

    max_year = today.getFullYear() - 4;

    return max_year;
}

export default {
    questionnaires: [{
        id: 'New_Patient',
        SU: 'New_Patient',
        displayName: 'ADD_PATIENT',
        className: 'ADD_PATIENT',

        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'NewPatientAffidavit',

        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        screens: [
            'ADD_PATIENT_S_1',
            'ADD_PATIENT_S_2'
        ],
        branches: [], // TODO: Subject Language definition from PDS. See e.g. 13.1.3.3, 13.1.3.4

        // Only Site Users and System Adminstrators may access this questionnaire.
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'ADD_PATIENT_S_1',
        className: 'ADD_PATIENT_S_1',

        // Prevent the back button from displaying on the first screen.
        disableBack: true,
        questions: [
            { id: 'ADD_PATIENT_INFO' },
            { id: 'ADD_PATIENT_ID', mandatory: true },
            { id: 'ADD_PATIENT_YOB', mandatory: true },
            { id: 'ADD_PATIENT_PROTOCOL', mandatory: true }, // Hidden
            { id: 'ADD_PATIENT_PASSWORD', mandatory: true },
            { id: 'ADD_PATIENT_PASSWORD_CONFIRM', mandatory: true },
            { id: 'ADD_PATIENT_ROLE', mandatory: true },
            { id: 'ADD_PATIENT_SALT', mandatory: true }
        ]
    }, {
        id: 'ADD_PATIENT_S_2',
        className: 'ADD_PATIENT_S_2',
        questions: [
            { id: 'ADD_PATIENT_LANGUAGE_INFO' },
            { id: 'ADD_PATIENT_LANGUAGE', mandatory: true }
        ]
    }],

    questions: [{
        id: 'ADD_PATIENT_INFO',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_INFO',
        skipIT: '',
        title: '',
        text: ['NEW_PATIENT_QUESTION_1_MAIN_TITLE'],
        className: 'ADD_PATIENT_INFO'
    }, {
        id: 'ADD_PATIENT_ID',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_ID',
        skipIT: '',
        title: '',
        text: ['NEW_PATIENT_QUESTION_1'],
        className: 'ADD_PATIENT_ID',
        widget: {
            id: 'ADD_PATIENT_W_1',
            type: 'PatientIDTextBox',
            templates: {},
            answers: [],
            label: '',

            // numeric from 1 to 4 characters
            maxLength: 4,
            allowedKeyRegex: /[\d]/,
            validateRegex: /[\d]+/,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'NEW_PATIENT_QUESTION_1_EMPTY',
                header: 'NEW_PATIENT_QUESTION_1_EMPTY_HEADER',
                ok: 'OK'
            }],
            validation: {
                validationFunc: 'checkPatientID',
                params: {
                    errorStrings: {
                        isInRange: {
                            errString: 'NEW_PATIENT_QUESTION_1_RANGE',
                            header: 'NEW_PATIENT_QUESTION_1_RANGE_HEADER',
                            ok: 'OK'
                        },
                        isUnique: {
                            errString: 'NEW_PATIENT_QUESTION_1_UNIQUE',
                            header: 'NEW_PATIENT_QUESTION_1_UNIQUE_HEADER',
                            ok: 'OK'
                        }
                    }
                }
            }
        }
    }, {
        id: 'ADD_PATIENT_PROTOCOL',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_PROTOCOL',
        localOnly: true,
        skipIT: '',
        title: '',
        text: [],
        className: '',
        widget: {
            id: 'ADD_PATIENT_W_PROTOCOL',
            type: 'HiddenField',
            label: 'HiddenProtocolField'
        }
    }, {
        id: 'ADD_PATIENT_YOB',
        IG: 'Add_Patient',
        IT: 'dob',
        className: 'YOB',
        text: ['NEW_PATIENT_QUESTION_4'],
        widget: {
            id: 'ADD_PATIENT_YOB_W1',
            type: 'NumericTextBox',
            min: getYOB_min(),
            max: getYOB_max(),
            precision: '0',
            step: 1
        }
    }, {
        id: 'ADD_PATIENT_PASSWORD',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_PASSWORD',
        skipIT: '',
        title: '',
        text: ['TEMP_PASSWORD'],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_PATIENT_W_3',
            type: 'TempPasswordTextBox',
            label: '', //'TEMP_PASSWORD',
            templates: {},
            answers: [],
            field: 'password',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'checkPasswordFieldWidget',
                params: {}
            },
            getRoleInputType: () => {
                return getRoleInputType('subject');
            }
        }
    }, {
        id: 'ADD_PATIENT_PASSWORD_CONFIRM',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_PASSWORD_CONFIRM',
        skipIT: '',
        title: '',
        text: ['CONFIRM_TEMP_PASS'],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_PATIENT_W_4',
            type: 'ConfirmationTextBox',
            label: '', //'CONFIRM_TEMP_PASS',
            templates: {},
            answers: [],
            field: 'confirm',
            fieldToConfirm: 'ADD_PATIENT_W_3',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'confirmFieldValue',
                params: {
                    errorString: 'PASSWORD_MISMATCH'
                }
            },
            getRoleInputType: () => {
                return getRoleInputType('subject');
            }
        }
    }, {
        id: 'ADD_PATIENT_SALT',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_SALT',
        className: 'ADD_PATIENT_SALT',
        title: '',
        text: [],
        widget: {
            id: 'ADD_PATIENT_W_5',
            type: 'HiddenField',
            value: 'temp',
            field: 'salt'
        }
    }, {
        id: 'ADD_PATIENT_ROLE',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_ROLE',
        className: 'ADD_PATIENT_ROLE',
        title: '',
        text: [],
        widget: {
            id: 'ADD_PATIENT_W_8',
            type: 'HiddenField',
            value: 'subject',
            field: 'role'
        }
    }, {
        id: 'ADD_PATIENT_LANGUAGE_INFO',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_LANGUAGE_INFO',
        skipIT: '',
        title: '',

        text: ['NEW_PATIENT_QUESTION_3_TITLE'],
        className: 'ADD_PATIENT_LANGUAGE_INFO'
    }, {
        id: 'ADD_PATIENT_LANGUAGE',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_LANGUAGE',
        skipIT: '',
        title: '',
        text: ['NEW_PATIENT_QUESTION_3'],
        className: '',
        widget: {
            id: 'ADD_PATIENT_W_LANG',
            type: 'PDE_SubjectLanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: [],
            validation: {
                validationFunc: 'checkLanguageFieldWidget',
                params: {}
            }
        }
    }],

    affidavits: [{
        id: 'NewPatientAffidavit',
        text: [
            'NEW_PATIENT_AFFIDAVIT_HEADER',
            'SITE_REPORT_AFF'
        ],
        templates: { question: 'AffidavitText' },
        krSig: 'SubmitForm',
        widget: {
            id: 'SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox'
        }
    }]
};
