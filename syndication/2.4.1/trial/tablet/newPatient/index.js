import { mergeObjects } from 'core/utilities/languageExtensions';
let studyDesign = {};

import diary from './new-patient';
import rules from './new-patient-rules';
import dynamicText from './new-patient-dynamicText';
import messages from './new-patient-messages';

studyDesign = mergeObjects(studyDesign, diary, rules, dynamicText, messages);

export default { studyDesign };
