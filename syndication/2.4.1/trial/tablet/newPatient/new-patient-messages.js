import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

export default {
    messages: [
        {
            type: 'Dialog',
            key: 'NEW_PATIENT_CONFIRM',
            message: MessageHelpers.confirmDialogCreator({
                header: 'PLEASE_CONFIRM_CAPITALIZED',
                message: 'NEW_PATIENT_CONFIRM',
                ok: 'YES',
                cancel: 'NO'
            })
        }
    ]
};
