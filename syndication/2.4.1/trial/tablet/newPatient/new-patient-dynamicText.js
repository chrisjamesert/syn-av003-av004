import { zeroPad } from 'core/utilities/coreUtilities';

export default {
    dynamicText: [
        {
            id: 'patientIDRangeByProtocol',
            evaluate: () => {
                let text = '',
                    idLen = LF.StudyDesign.participantSettings.participantID.max.toString().length;
                _.each(LF.StudyDesign.participantSettings.protocolIdRanges, (val, key) => {
                    // Get the protocol name
                    let name = LF.StudyDesign.studyProtocol.protocolList[key];
                    text += `${name}: ${zeroPad(val.min, idLen)} - ${zeroPad(val.max, idLen)}<br />`;
                });
                text = text.substr(0, text.length - 6); // remove final <br />
                return text;
            },
            screenshots: {
                values: ['AV003: 001 - 499<br />AV004: 500 - 999']
            }
        },
        {
            id: 'newPatientSubjectId',
            evaluate: () => {
                return LF.Data.Questionnaire.queryAnswersByID('ADD_PATIENT_ID')[0].get('response');
            },
            screenshots: {
                values: ['001']
            }
        },
        {
            id: 'newPatientProtocol',
            evaluate: () => {
                let subjectId = LF.Data.Questionnaire.queryAnswersByID('ADD_PATIENT_ID')[0].get('response'),
                    subjectNumber = subjectId.substring(subjectId.length - 3);
                return parseInt(subjectNumber, 10) < 500 ? 'AV003' : 'AV004';
            },
            screenshots: {
                values: ['AV003']
            }
        },
        {
            id: 'newPatientYearOfBirth',
            evaluate: () => {
                return LF.Data.Questionnaire.queryAnswersByIT('dob')[0].get('response');
            },
            screenshots: {
                values: ['1978']
            }
        }
    ]
};
