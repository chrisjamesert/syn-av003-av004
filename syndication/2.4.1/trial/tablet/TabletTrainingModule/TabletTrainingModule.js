
export default {
    questionnaires: [
        {
            id: 'TabletTrainingModule',
            SU: 'TTM',
            deviceType: 'sitepad',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'TTM010',
                'TTM020',
                'TTM030_EMPH_MSG',
                'TTM040_INSTR_MSG',
                'TTM050_MC_BAR',
                'TTM060_MC_RADIO',
                'TTM260_END_MSG',
                'TTM270'
            ],
            branches: [
                {
                    spec_id: 'B219',
                    branchTo: 'TTM010',
                    branchFrom: 'TTM270',
                    branchFunction: 'equalBranchBack',
                    branchParams: {
                        questionId: 'TTM270_Q1',
                        value: '0'
                    }
                }
            ],
            initialScreen: [],
            accessRoles: [
                'subject'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'TTM010',
            className: 'TTM010',
            questions: [
                {
                    id: 'TTM010_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM020',
            className: 'TTM020',
            questions: [
                {
                    id: 'TTM020_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM030_EMPH_MSG',
            className: 'TTM030_EMPH_MSG',
            questions: [
                {
                    id: 'TTM030_EMPH_MSG_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM040_INSTR_MSG',
            className: 'TTM040_INSTR_MSG',
            questions: [
                {
                    id: 'TTM040_INSTR_MSG_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM050_MC_BAR',
            className: 'TTM050_MC_BAR',
            questions: [
                {
                    id: 'TTM050_MC_BAR_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM060_MC_RADIO',
            className: 'TTM060_MC_RADIO',
            questions: [
                {
                    id: 'TTM060_MC_RADIO_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM260_END_MSG',
            className: 'TTM260_END_MSG',
            questions: [
                {
                    id: 'TTM260_END_MSG_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TTM270',
            className: 'TTM270',
            questions: [
                {
                    id: 'TTM270_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }
    ],
    questions: [
        {
            id: 'TTM010_Q1',
            text: 'TTM010_Q1_TEXT',
            title: '',
            className: 'TTM010_Q1'
        }, {
            id: 'TTM020_Q1',
            text: 'TTM020_Q1_TEXT',
            title: '',
            className: 'TTM020_Q1'
        }, {
            id: 'TTM030_EMPH_MSG_Q1',
            text: 'TTM030_EMPH_MSG_Q1_TEXT',
            title: '',
            className: 'TTM030_EMPH_MSG_Q1'
        }, {
            id: 'TTM040_INSTR_MSG_Q1',
            text: 'TTM040_INSTR_MSG_Q1_TEXT',
            title: '',
            className: 'TTM040_INSTR_MSG_Q1'
        }, {
            id: 'TTM050_MC_BAR_Q1',
            IG: 'TTM',
            IT: 'TTM050_MC_BAR_Q1',
            localOnly: true,
            text: 'TTM050_MC_BAR_Q1_TEXT',
            title: '',
            className: 'TTM050_MC_BAR_Q1',
            widget: {
                id: 'TTM050_MC_BAR_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'TTM_HAPPY'
                    },
                    {
                        value: '2',
                        text: 'TTM_SAD'
                    },
                    {
                        value: '3',
                        text: 'TTM_TIRED'
                    },
                    {
                        value: '4',
                        text: 'TTM_SURPRISED'
                    }
                ]
            }
        }, {
            id: 'TTM060_MC_RADIO_Q1',
            IG: 'TTM',
            IT: 'TTM060_MC_RADIO_Q1',
            localOnly: true,
            text: 'TTM060_MC_RADIO_Q1_TEXT',
            title: '',
            className: 'TTM060_MC_RADIO_Q1',
            widget: {
                id: 'TTM060_MC_RADIO_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'TTM_HAPPY'
                    },
                    {
                        value: '2',
                        text: 'TTM_SAD'
                    },
                    {
                        value: '3',
                        text: 'TTM_TIRED'
                    },
                    {
                        value: '4',
                        text: 'TTM_SURPRISED'
                    }
                ]
            }
        }, {
            id: 'TTM260_END_MSG_Q1',
            text: 'TTM260_END_MSG_Q1_TEXT',
            title: '',
            className: 'TTM260_END_MSG_Q1'
        }, {
            id: 'TTM270_Q1',
            IG: 'TTM',
            IT: 'TRNCNF1L',
            text: 'TTM270_Q1_TEXT',
            title: '',
            className: 'TTM270_Q1',
            widget: {
                id: 'TTM270_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'TTM270_Q1_W0_A0'
                    },
                    {
                        value: '0',
                        text: 'TTM270_Q1_W0_A1'
                    }
                ]
            }
        }
    ]
};
