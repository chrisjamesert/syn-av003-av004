
import diary from './TabletTrainingModule';
import rules from './rules';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, diary, rules);

export default { studyDesign };

