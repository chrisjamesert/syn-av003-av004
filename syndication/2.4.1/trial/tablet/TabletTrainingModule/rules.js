
export default {
    rules: [
        {
            id: 'NoSendTTMAfterFirstCompletion',
            trigger: ['QUESTIONNAIRE:Completed/TabletTrainingModule'],
            resolve: [
                {
                    action (input, done) {
                        if (this.visit.get('id') === 'training_visit') {
                            // Spec. ID 5.2.1.5
                            // When Training Module is completed in Training Visit, questionnaire will not be saved nor
                            // sent to StudyWorks.
                            done({
                                preventDefault: true
                            });
                        } else {
                            done();
                        }
                    }
                }
            ]
        }
    ]
};
