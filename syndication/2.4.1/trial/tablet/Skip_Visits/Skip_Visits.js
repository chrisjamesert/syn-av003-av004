
export default {
    questionnaires: [
        {
            id: 'Skip_Visits',
            deviceType: 'sitepad',
            SU: 'Skip_Visits',
            displayName: 'SKIP_VISITS',
            className: 'SKIP_VISIT',
            screens: [
                'SKIP_VISIT_S_1'
            ],
            product: [
                'sitepad'
            ],
            accessRoles: [
                'site',
                'admin'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'SKIP_VISIT_S_1',
            className: 'SKIP_VISIT_S_1',
            disableBack: true,
            questions: [
                {
                    id: 'SKIP_VISIT_Q_1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'SKIP_VISIT_Q_1',
            IG: 'SkipVisit',
            IT: 'SKIP_VISIT',
            text: 'SKIP_VISITS_QUESTION_1',
            className: 'SKIP_VISIT_Q_1',
            widget: {
                id: 'SKIP_VISIT_Q1',
                type: 'SkipReason',
                className: 'SKIP_VISIT',
                answers: [
                    {
                        text: 'SKIP_VISITS_REASON_1',
                        value: '1'
                    },
                    {
                        text: 'SKIP_VISITS_REASON_2',
                        value: '2'
                    },
                    {
                        text: 'SKIP_VISITS_REASON_3',
                        value: '3'
                    },
                    {
                        text: 'SKIP_VISITS_REASON_4',
                        value: '4'
                    }
                ]
            }
        }
    ]
};
