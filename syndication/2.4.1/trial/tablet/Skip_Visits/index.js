
import Skip_Visits from './Skip_Visits';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, Skip_Visits);

export default { studyDesign };

