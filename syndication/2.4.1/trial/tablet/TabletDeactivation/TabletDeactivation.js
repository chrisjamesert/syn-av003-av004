
export default {
    questionnaires: [
        {
            id: 'TabletDeactivation',
            SU: 'DeactivateSubject',
            deviceType: 'sitepad',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            screens: [
                'TD010',
                'TD020'
            ],
            branches: [
                {
                    spec_id: 'B114',
                    branchTo: 'TD010',
                    branchFrom: 'TD010',
                    branchFunction: 'exitDiary',
                    branchParams: {
                        questionId: 'TD010_Q1',
                        value: '0'
                    }
                }
            ],
            initialScreen: [],
            accessRoles: [
                'site',
                'admin'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'TD010',
            className: 'TD010',
            questions: [
                {
                    id: 'TD010_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'TD020',
            className: 'TD020',
            questions: [
                {
                    id: 'TD020_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }
    ],
    questions: [
        {
            id: 'TD010_Q1',
            IG: 'DeactivateSubject',
            IT: 'TD010_Q1',
            localOnly: true,
            text: 'TD010_Q1_TEXT',
            title: '',
            className: 'TD010_Q1',
            widget: {
                id: 'TD010_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'TD010_Q1_W0_A0'
                    },
                    {
                        value: '0',
                        text: 'TD010_Q1_W0_A1'
                    }
                ]
            }
        }, {
            id: 'TD020_Q1',
            IG: 'DeactivateSubject',
            IT: 'ReasonfordeactivatingSubject',
            text: 'TD020_Q1_TEXT',
            title: '',
            className: 'TD020_Q1',
            widget: {
                id: 'TD020_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'TD020_Q1_W0_A0'
                    },
                    {
                        value: '20',
                        text: 'TD020_Q1_W0_A1'
                    }
                ]
            }
        }
    ]
};
