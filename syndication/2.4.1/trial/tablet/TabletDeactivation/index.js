
import TabletDeactivation from './TabletDeactivation';
import rules from './rules';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, TabletDeactivation, rules);

export default { studyDesign };

