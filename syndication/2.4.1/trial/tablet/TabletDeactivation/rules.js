import Logger from 'core/Logger';

export default {
    rules: [
        {
            id: 'OnDeactivationTabletCompleted',
            trigger: [
                'QUESTIONNAIRE:Completed/TabletDeactivation'
            ],
            salience: 1,
            evaluate: function (filter, resume) {
                let logger = new Logger('StudyRules:TabletDeactivation');

                if (PDE.DeviceStatusUtils.isScreenshotMode()) {
                    resume(false);
                    return;
                }

                if (LF.Utilities.isPDEVisit(this.visit)) {
                    resume(false);
                    return;
                }

                resume(true);
            },
            resolve: [
                {
                    action: 'changePhase',
                    data: function (input, done) {
                        let logger = new Logger('StudyRules:TabletDeactivation');

                        // Send current date/time to PT.SPEndDate
                        let dateStr = LF.Widget.DatePicker.prototype.buildStudyWorksStringWithTime(new Date());
                        LF.Utilities.addSysvar({
                            view: this,
                            questionnaire_id: this.model.id,
                            response: dateStr,
                            IG: 'PT',
                            sysvar: 'SPEndDate'
                        });

                        // Send current date/time to PT.EndLogPadUse
                        LF.Utilities.addSysvar({
                            view: this,
                            questionnaire_id: this.model.id,
                            response: dateStr,
                            IG: 'PT',
                            sysvar: 'EndLogPadUse'
                        });

                        done({
                            change: true,
                            phase: LF.StudyDesign.studyPhase.DEACTIVATION,
                            phaseStartDateTZOffset: LF.Utilities.timeStamp(new Date())
                        });
                    }
                }
            ]
        }
    ]
};
