
export default {
    questionnaires: [
        {
            id: 'AQLQS12T',
            SU: 'AQLQS12T',
            deviceType: 'sitepad',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'AQT002',
                'AQT010',
                'AQT020',
                'AQT030',
                'AQT040',
                'AQT050',
                'AQT060',
                'AQT070',
                'AQT080',
                'AQT090',
                'AQT100',
                'AQT110',
                'AQT120',
                'AQT130',
                'AQT140',
                'AQT150',
                'AQT160',
                'AQT170',
                'AQT180',
                'AQT190',
                'AQT200',
                'AQT210',
                'AQT220',
                'AQT230',
                'AQT240',
                'AQT250',
                'AQT260',
                'AQT270',
                'AQT280',
                'AQT290',
                'AQT300',
                'AQT310',
                'AQT320'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: [
                'subject'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'AQT002',
            className: 'AQT002',
            questions: [
                {
                    id: 'AQT002_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT010',
            className: 'AQT010',
            questions: [
                {
                    id: 'AQT010_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT020',
            className: 'AQT020',
            questions: [
                {
                    id: 'AQT020_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT030',
            className: 'AQT030',
            questions: [
                {
                    id: 'AQT030_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT040',
            className: 'AQT040',
            questions: [
                {
                    id: 'AQT040_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT050',
            className: 'AQT050',
            questions: [
                {
                    id: 'AQT050_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT060',
            className: 'AQT060',
            questions: [
                {
                    id: 'AQT060_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT070',
            className: 'AQT070',
            questions: [
                {
                    id: 'AQT070_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT080',
            className: 'AQT080',
            questions: [
                {
                    id: 'AQT080_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT090',
            className: 'AQT090',
            questions: [
                {
                    id: 'AQT090_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT100',
            className: 'AQT100',
            questions: [
                {
                    id: 'AQT100_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT110',
            className: 'AQT110',
            questions: [
                {
                    id: 'AQT110_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT120',
            className: 'AQT120',
            questions: [
                {
                    id: 'AQT120_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT130',
            className: 'AQT130',
            questions: [
                {
                    id: 'AQT130_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT140',
            className: 'AQT140',
            questions: [
                {
                    id: 'AQT140_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT150',
            className: 'AQT150',
            questions: [
                {
                    id: 'AQT150_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT160',
            className: 'AQT160',
            questions: [
                {
                    id: 'AQT160_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT170',
            className: 'AQT170',
            questions: [
                {
                    id: 'AQT170_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT180',
            className: 'AQT180',
            questions: [
                {
                    id: 'AQT180_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT190',
            className: 'AQT190',
            questions: [
                {
                    id: 'AQT190_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT200',
            className: 'AQT200',
            questions: [
                {
                    id: 'AQT200_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT210',
            className: 'AQT210',
            questions: [
                {
                    id: 'AQT210_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT220',
            className: 'AQT220',
            questions: [
                {
                    id: 'AQT220_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT230',
            className: 'AQT230',
            questions: [
                {
                    id: 'AQT230_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT240',
            className: 'AQT240',
            questions: [
                {
                    id: 'AQT240_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT250',
            className: 'AQT250',
            questions: [
                {
                    id: 'AQT250_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT260',
            className: 'AQT260',
            questions: [
                {
                    id: 'AQT260_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT270',
            className: 'AQT270',
            questions: [
                {
                    id: 'AQT270_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT280',
            className: 'AQT280',
            questions: [
                {
                    id: 'AQT280_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT290',
            className: 'AQT290',
            questions: [
                {
                    id: 'AQT290_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT300',
            className: 'AQT300',
            questions: [
                {
                    id: 'AQT300_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT310',
            className: 'AQT310',
            questions: [
                {
                    id: 'AQT310_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AQT320',
            className: 'AQT320',
            questions: [
                {
                    id: 'AQT320_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }
    ],
    questions: [
        {
            id: 'AQT002_Q1',
            text: 'AQT002_Q1_TEXT',
            title: '',
            className: 'AQT002_Q1'
        }, {
            id: 'AQT010_Q1',
            IG: 'AQLQS12T',
            IT: 'ACTSTR1L',
            text: 'AQT010_Q1_TEXT',
            title: '',
            className: 'AQT010_Q1',
            widget: {
                id: 'AQT010_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_TOTALLY_LIMITED'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_EXTREMELY_LIMITED'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_VERY_LIMITED'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_LIMITATION'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME_LIMITATION'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_A_LITTLE_LIMITATION'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NOT_AT_ALL_LIMITED'
                    }
                ]
            }
        }, {
            id: 'AQT020_Q1',
            IG: 'AQLQS12T',
            IT: 'ACTMOD1L',
            text: 'AQT020_Q1_TEXT',
            title: '',
            className: 'AQT020_Q1',
            widget: {
                id: 'AQT020_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_TOTALLY_LIMITED'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_EXTREMELY_LIMITED'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_VERY_LIMITED'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_LIMITATION'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME_LIMITATION'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_A_LITTLE_LIMITATION'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NOT_AT_ALL_LIMITED'
                    }
                ]
            }
        }, {
            id: 'AQT030_Q1',
            IG: 'AQLQS12T',
            IT: 'ACTSOC1L',
            text: 'AQT030_Q1_TEXT',
            title: '',
            className: 'AQT030_Q1',
            widget: {
                id: 'AQT030_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_TOTALLY_LIMITED'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_EXTREMELY_LIMITED'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_VERY_LIMITED'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_LIMITATION'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME_LIMITATION'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_A_LITTLE_LIMITATION'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NOT_AT_ALL_LIMITED'
                    }
                ]
            }
        }, {
            id: 'AQT040_Q1',
            IG: 'AQLQS12T',
            IT: 'ACTWRK1L',
            text: 'AQT040_Q1_TEXT',
            title: '',
            className: 'AQT040_Q1',
            widget: {
                id: 'AQT040_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_TOTALLY_LIMITED'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_EXTREMELY_LIMITED'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_VERY_LIMITED'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_LIMITATION'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME_LIMITATION'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_A_LITTLE_LIMITATION'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NOT_AT_ALL_LIMITED'
                    }
                ]
            }
        }, {
            id: 'AQT050_Q1',
            IG: 'AQLQS12T',
            IT: 'LIMSLP1L',
            text: 'AQT050_Q1_TEXT',
            title: '',
            className: 'AQT050_Q1',
            widget: {
                id: 'AQT050_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_TOTALLY_LIMITED'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_EXTREMELY_LIMITED'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_VERY_LIMITED'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_LIMITATION'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME_LIMITATION'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_A_LITTLE_LIMITATION'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NOT_AT_ALL_LIMITED'
                    }
                ]
            }
        }, {
            id: 'AQT060_Q1',
            IG: 'AQLQS12T',
            IT: 'CHETGH1L',
            text: 'AQT060_Q1_TEXT',
            title: '',
            className: 'AQT060_Q1',
            widget: {
                id: 'AQT060_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_A_VERY_GREAT_DEAL'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_A_GREAT_DEAL'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_DEAL'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_AMOUNT'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_VERY_LITTLE'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE'
                    }
                ]
            }
        }, {
            id: 'AQT070_Q1',
            IG: 'AQLQS12T',
            IT: 'ASTCON1L',
            text: 'AQT070_Q1_TEXT',
            title: '',
            className: 'AQT070_Q1',
            widget: {
                id: 'AQT070_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT080_Q1',
            IG: 'AQLQS12T',
            IT: 'SHRBTH1L',
            text: 'AQT080_Q1_TEXT',
            title: '',
            className: 'AQT080_Q1',
            widget: {
                id: 'AQT080_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT090_Q1',
            IG: 'AQLQS12T',
            IT: 'SMKEXP1L',
            text: 'AQT090_Q1_TEXT',
            title: '',
            className: 'AQT090_Q1',
            widget: {
                id: 'AQT090_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT100_Q1',
            IG: 'AQLQS12T',
            IT: 'CHEWZE1L',
            text: 'AQT100_Q1_TEXT',
            title: '',
            className: 'AQT100_Q1',
            widget: {
                id: 'AQT100_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT110_Q1',
            IG: 'AQLQS12T',
            IT: 'AVDSMK1L',
            text: 'AQT110_Q1_TEXT',
            title: '',
            className: 'AQT110_Q1',
            widget: {
                id: 'AQT110_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT120_Q1',
            IG: 'AQLQS12T',
            IT: 'CGHDIS1L',
            text: 'AQT120_Q1_TEXT',
            title: '',
            className: 'AQT120_Q1',
            widget: {
                id: 'AQT120_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_A_VERY_GREAT_DEAL'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_A_GREAT_DEAL'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_DEAL'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_AMOUNT'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_VERY_LITTLE'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE'
                    }
                ]
            }
        }, {
            id: 'AQT130_Q1',
            IG: 'AQLQS12T',
            IT: 'ASTFRS1L',
            text: 'AQT130_Q1_TEXT',
            title: '',
            className: 'AQT130_Q1',
            widget: {
                id: 'AQT130_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT140_Q1',
            IG: 'AQLQS12T',
            IT: 'CHEHEV1L',
            text: 'AQT140_Q1_TEXT',
            title: '',
            className: 'AQT140_Q1',
            widget: {
                id: 'AQT140_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT150_Q1',
            IG: 'AQLQS12T',
            IT: 'CONMED1L',
            text: 'AQT150_Q1_TEXT',
            title: '',
            className: 'AQT150_Q1',
            widget: {
                id: 'AQT150_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT160_Q1',
            IG: 'AQLQS12T',
            IT: 'CLRTHR1L',
            text: 'AQT160_Q1_TEXT',
            title: '',
            className: 'AQT160_Q1',
            widget: {
                id: 'AQT160_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT170_Q1',
            IG: 'AQLQS12T',
            IT: 'EXPDST1L',
            text: 'AQT170_Q1_TEXT',
            title: '',
            className: 'AQT170_Q1',
            widget: {
                id: 'AQT170_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT180_Q1',
            IG: 'AQLQS12T',
            IT: 'DIFBTH1L',
            text: 'AQT180_Q1_TEXT',
            title: '',
            className: 'AQT180_Q1',
            widget: {
                id: 'AQT180_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT190_Q1',
            IG: 'AQLQS12T',
            IT: 'AVODST1L',
            text: 'AQT190_Q1_TEXT',
            title: '',
            className: 'AQT190_Q1',
            widget: {
                id: 'AQT190_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT200_Q1',
            IG: 'AQLQS12T',
            IT: 'MRNSYM1L',
            text: 'AQT200_Q1_TEXT',
            title: '',
            className: 'AQT200_Q1',
            widget: {
                id: 'AQT200_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT210_Q1',
            IG: 'AQLQS12T',
            IT: 'AFRMED1L',
            text: 'AQT210_Q1_TEXT',
            title: '',
            className: 'AQT210_Q1',
            widget: {
                id: 'AQT210_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT220_Q1',
            IG: 'AQLQS12T',
            IT: 'HEVBTH1L',
            text: 'AQT220_Q1_TEXT',
            title: '',
            className: 'AQT220_Q1',
            widget: {
                id: 'AQT220_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT230_Q1',
            IG: 'AQLQS12T',
            IT: 'EXPPOL1L',
            text: 'AQT230_Q1_TEXT',
            title: '',
            className: 'AQT230_Q1',
            widget: {
                id: 'AQT230_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT240_Q1',
            IG: 'AQLQS12T',
            IT: 'ASTWOK1L',
            text: 'AQT240_Q1_TEXT',
            title: '',
            className: 'AQT240_Q1',
            widget: {
                id: 'AQT240_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT250_Q1',
            IG: 'AQLQS12T',
            IT: 'AVDOUT1L',
            text: 'AQT250_Q1_TEXT',
            title: '',
            className: 'AQT250_Q1',
            widget: {
                id: 'AQT250_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT260_Q1',
            IG: 'AQLQS12T',
            IT: 'EXPSML1L',
            text: 'AQT260_Q1_TEXT',
            title: '',
            className: 'AQT260_Q1',
            widget: {
                id: 'AQT260_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT270_Q1',
            IG: 'AQLQS12T',
            IT: 'OUTBTH1L',
            text: 'AQT270_Q1_TEXT',
            title: '',
            className: 'AQT270_Q1',
            widget: {
                id: 'AQT270_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT280_Q1',
            IG: 'AQLQS12T',
            IT: 'ADVSML1L',
            text: 'AQT280_Q1_TEXT',
            title: '',
            className: 'AQT280_Q1',
            widget: {
                id: 'AQT280_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT290_Q1',
            IG: 'AQLQS12T',
            IT: 'INTSLP1L',
            text: 'AQT290_Q1_TEXT',
            title: '',
            className: 'AQT290_Q1',
            widget: {
                id: 'AQT290_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT300_Q1',
            IG: 'AQLQS12T',
            IT: 'FHTAIR1L',
            text: 'AQT300_Q1_TEXT',
            title: '',
            className: 'AQT300_Q1',
            widget: {
                id: 'AQT300_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_ALL_OF_THE_TIME'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_MOST_OF_THE_TIME'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_A_GOOD_BIT_OF_THE_TIME'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_SOME_OF_THE_TIME'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_A_LITTLE_OF_THE_TIME'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_HARDLY_ANY_OF_THE_TIME'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NONE_OF_THE_TIME'
                    }
                ]
            }
        }, {
            id: 'AQT310_Q1',
            IG: 'AQLQS12T',
            IT: 'RNGACT1L',
            text: 'AQT310_Q1_TEXT',
            title: '',
            className: 'AQT310_Q1',
            widget: {
                id: 'AQT310_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT310_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'AQT310_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'AQT310_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'AQT310_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'AQT310_Q1_W0_A4'
                    },
                    {
                        value: '6',
                        text: 'AQT310_Q1_W0_A5'
                    },
                    {
                        value: '7',
                        text: 'AQT310_Q1_W0_A6'
                    }
                ]
            }
        }, {
            id: 'AQT320_Q1',
            IG: 'AQLQS12T',
            IT: 'ALLACT1L',
            text: 'AQT320_Q1_TEXT',
            title: '',
            className: 'AQT320_Q1',
            widget: {
                id: 'AQT320_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AQT_1_TOTALLY_LIMITED'
                    },
                    {
                        value: '2',
                        text: 'AQT_2_EXTREMELY_LIMITED'
                    },
                    {
                        value: '3',
                        text: 'AQT_3_VERY_LIMITED'
                    },
                    {
                        value: '4',
                        text: 'AQT_4_MODERATE_LIMITATION'
                    },
                    {
                        value: '5',
                        text: 'AQT_5_SOME_LIMITATION'
                    },
                    {
                        value: '6',
                        text: 'AQT_6_A_LITTLE_LIMITATION'
                    },
                    {
                        value: '7',
                        text: 'AQT_7_NOT_AT_ALL_LIMITED'
                    }
                ]
            }
        }
    ]
};
