

import AQLQS12T from './AQLQS12T';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, AQLQS12T);

export default { studyDesign };

