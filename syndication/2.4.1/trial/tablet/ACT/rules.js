import Logger from 'core/Logger';
let logger = new Logger('ACT Rules');

export default {
    rules: [
        {
            id: 'Add_ACTSCR1N',
            trigger: ['QUESTIONNAIRE:Navigate/ACT/AFFIDAVIT'],
            evaluate: 'isDirectionForward',
            salience: 10,
            resolve: [
                {
                    action (input, done) {
                        // Spec. ID V174
                        // ACT Total Score
                        let KEPWRK1L = LF.Data.Questionnaire.queryAnswersByIT('KEPWRK1L'),
                            SHRTBR1L = LF.Data.Questionnaire.queryAnswersByIT('SHRTBR1L'),
                            WKUEAR1L = LF.Data.Questionnaire.queryAnswersByIT('WKUEAR1L'),
                            USINNE1L = LF.Data.Questionnaire.queryAnswersByIT('USINNE1L'),
                            ASTCTR1L = LF.Data.Questionnaire.queryAnswersByIT('ASTCTR1L');
                        KEPWRK1L = KEPWRK1L[0] ? KEPWRK1L[0].get('response') : null;
                        SHRTBR1L = SHRTBR1L[0] ? SHRTBR1L[0].get('response') : null;
                        WKUEAR1L = WKUEAR1L[0] ? WKUEAR1L[0].get('response') : null;
                        USINNE1L = USINNE1L[0] ? USINNE1L[0].get('response') : null;
                        ASTCTR1L = ASTCTR1L[0] ? ASTCTR1L[0].get('response') : null;
                        if (!KEPWRK1L || !SHRTBR1L || !WKUEAR1L || !USINNE1L || !ASTCTR1L) {
                            logger.error('Missing a parameter in calculation of ACTSCR1N');
                        }
                        let ACTSCR1N = (parseInt(KEPWRK1L, 10) +
                                        parseInt(SHRTBR1L, 10) +
                                        parseInt(WKUEAR1L, 10) +
                                        parseInt(USINNE1L, 10) +
                                        parseInt(ASTCTR1L, 10)).toString();
                        this.addIT({
                            question_id: 'ACTSCR1N',
                            questionnaire_id: 'ACT',
                            response: ACTSCR1N,
                            IG: 'ACT',
                            IGR: 0,
                            IT: 'ACTSCR1N'
                        });
                        done();
                    }
                }
            ]
        }
    ]
};
