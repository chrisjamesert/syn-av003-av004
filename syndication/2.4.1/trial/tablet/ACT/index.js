
import ACT from './ACT';
import rules from './rules';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, ACT, rules);

export default { studyDesign };

