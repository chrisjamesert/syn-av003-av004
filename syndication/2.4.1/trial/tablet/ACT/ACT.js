
export default {
    questionnaires: [
        {
            id: 'ACT',
            SU: 'ACT',
            deviceType: 'sitepad',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'AC010',
                'AC020',
                'AC030',
                'AC040',
                'AC050'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: [
                'subject'
            ],
            isSitePad: true
        }],
    screens: [
        {
            id: 'AC010',
            className: 'AC010',
            questions: [
                {
                    id: 'AC010_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AC020',
            className: 'AC020',
            questions: [
                {
                    id: 'AC020_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AC030',
            className: 'AC030',
            questions: [
                {
                    id: 'AC030_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AC040',
            className: 'AC040',
            questions: [
                {
                    id: 'AC040_Q1',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }, {
            id: 'AC050',
            className: 'AC050',
            questions: [
                {
                    id: 'AC050_Q1',
                    mandatory: true
                },
                {
                    id: 'AC050_Q2',
                    mandatory: true
                }
            ],
            deviceType: 'sitepad'
        }
    ],
    questions: [
        {
            id: 'AC010_Q1',
            IG: 'ACT',
            IT: 'KEPWRK1L',
            text: 'AC010_Q1_TEXT',
            title: '',
            className: 'AC010_Q1',
            widget: {
                id: 'AC010_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AC010_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'AC010_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'AC010_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'AC010_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'AC010_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'AC020_Q1',
            IG: 'ACT',
            IT: 'SHRTBR1L',
            text: 'AC020_Q1_TEXT',
            title: '',
            className: 'AC020_Q1',
            widget: {
                id: 'AC020_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AC020_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'AC020_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'AC020_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'AC020_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'AC020_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'AC030_Q1',
            IG: 'ACT',
            IT: 'WKUEAR1L',
            text: 'AC030_Q1_TEXT',
            title: '',
            className: 'AC030_Q1',
            widget: {
                id: 'AC030_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AC030_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'AC030_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'AC030_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'AC030_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'AC030_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'AC040_Q1',
            IG: 'ACT',
            IT: 'USINNE1L',
            text: 'AC040_Q1_TEXT',
            title: '',
            className: 'AC040_Q1',
            widget: {
                id: 'AC040_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AC040_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'AC040_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'AC040_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'AC040_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'AC040_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'AC050_Q1',
            IG: 'ACT',
            IT: 'ASTCTR1L',
            text: 'AC050_Q1_TEXT',
            title: '',
            className: 'AC050_Q1',
            widget: {
                id: 'AC050_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'AC050_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'AC050_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'AC050_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'AC050_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'AC050_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'AC050_Q2',
            text: 'AC050_Q2_TEXT',
            title: '',
            className: 'AC050_Q2'
        }
    ]
};
