/**
 * @fileOverview This is an example environments study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.0
 */

/**
 * Replace STUDYNAME and SPONSOR_PROTOCOL with the relevant values
 */


export default {
    // PDE_TODO this sounds important, but I can't find it referenced in any code, should probably figure that out.
    allowEnvironmentSelect: true,

    // when constructing a service name from study name, use this template
    serviceUrlFormat: 'https://{{study}}.phtnetpro.com',

    environments: [
        {
            id: 'individual',
            label: 'Local server (localhost : Syndication)',
            modes: ['provision'],
            url: 'http://localhost:3000',
            studyDbName: 'Syndication',
            timeTravel: true
        },
        {
            id: 'dev',
            label: 'Dev',
            modes: ['provision'],
            url: 'https://AvillionAV003004DEV.phtnetpro.com',
            studyDbName: 'Avillion_AV003004_1',
            timeTravel: true
        },
        {
            id: 'staging',
            label: 'Staging',
            modes: ['provision'],
            url: 'https://AvillionAV003004STAGING.phtnetpro.com',
            studyDbName: 'Avillion_AV003004',
            timeTravel: true
        },
        {
            id: 'ust',
            label: 'UST',
            modes: ['provision'],
            url: 'https://AvillionAV003004UST.phtnetpro.com',
            studyDbName: 'Avillion_AV003004_UST',
            timeTravel: true
        },
        {
            // Production is REQUIRED! Do not remove. // http://localhost:3000
            id: 'production',
            label: 'Production',
            modes: ['provision', 'depot'],
            url: 'https://AvillionAV003004.phtnetpro.com',
            studyDbName: 'Avillion_AV003004',
            timeTravel: false
        }
    ]
};
