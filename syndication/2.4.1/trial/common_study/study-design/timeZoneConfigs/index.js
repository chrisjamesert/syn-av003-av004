import androidTZ from './time-zone-android-config';
import iOSTZ from './time-zone-ios-config';
import winTZ from './time-zone-windows-config';
import webTZ from './time-zone-web-config';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(androidTZ, iOSTZ, winTZ, webTZ);
