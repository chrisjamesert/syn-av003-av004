import affidavits from './affidavits';
import tzConfigs from './timeZoneConfigs';
import roles from './roles';
import commonSD from './common_study-design';
import secQues from './security-questions-config';
import supportOpt from './support-options';
import environments from './environments';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects(commonSD, affidavits, tzConfigs, roles, secQues, supportOpt, environments);
