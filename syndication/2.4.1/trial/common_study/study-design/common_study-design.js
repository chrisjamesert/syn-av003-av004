import ObjectRef from 'core/classes/ObjectRef';

export default {
    // The default language of the study.
    defaultLanguage: 'en',

    // The default locale of the study
    defaultLocale: 'US',

    // The maximum number of consecutive unsuccessful unlock attempts in !!web!! modality.
    maxUnlockAttempts: 3,

    // Number of minutes after which the lockout for the unlock code expires
    unlockCodeLockoutTime: 5,

    // The maximum number of consecutive unsuccessful password attempts at login and reactivation.
    maxLoginAttempts: 3,

    // Number of minutes after which the account is automatically re-enabled
    // (where 0 means the account doesn't get re-enabled until a daily reset/override code is used).
    lockoutTime: 5,

    // The number of minutes to subtract from the updated timestamp when syncing users to close window of client->server time difference.
    userSyncTimeStampThreshold: 20,

    // A string array (of diary SUs) to manually specify the diaries that are unscheduled
    lastDiarySyncUnscheduledSUList: null,

    // The protocol name used for file paths during the build process, does not display in app
    protocolBuild: 'av003_av004',

    // The client name
    clientName: 'Avillion',

    // The study Protocol
    studyProtocol: {
        defaultProtocol: 3,
        protocolList: new ObjectRef({
            3: 'AV003',
            4: 'AV004'
        })
    },

    // The phases used by the study
    studyPhase: new ObjectRef({
        TREATMENT: 20,
        PENDING_DEACTIVATION: 998,
        DEACTIVATION: 999
    }),

    // The phase from studyPhase to be used as the termination phase
    terminationPhase: 999,

    // A Regular Expression or a String of valid characters for defining which characters are allowed for input fields.
    validInputCharacters: 'a-zA-Z0-9\\s\\.-',

    /*
     * List of supported browsers for the web modality.
     * If you do not care about the version number, use version: '*'
     */
    supportedBrowsers: [
        { name: 'Chrome', version: '*' },
        { name: 'Edge', version: '*' }
    ],

    /*
    * mapping list for the LanguageSelectWidget function 'filterLanguagesByLocale'
    * Each key corresponds to a country for a site, with each value an array of languages that should be available
    * to select in that country.
    * If a country code is not defined in this list, then the 'default' array will be used instead.
    *
    * Below is a copy of the full list in PDE_Core. Uncomment and edit the below list to override the PDE_Core list.
    */
    sitelanguagelist: new ObjectRef({
        default: ['en-US'],
        AR: ['en-US', 'es-AR'], // Argentina
        AT: ['en-US', 'de-AT'], // Austria
        CA: ['en-US', 'en-CA', 'fr-CA'], // Canada
        CL: ['en-US', 'es-CL'], // Chile
        DE: ['en-US', 'de-DE'], // Germany
        IT: ['en-US', 'it-IT'], // Italy
        PE: ['en-US', 'es-PE'], // Peru
        RS: ['en-US', 'sr-RS'], // Serbia
        SK: ['en-US', 'sk-SK'], // Slovakia
        ZA: ['en-US', 'af-ZA', 'en-ZA'], // South Africa
        ES: ['en-US', 'es-ES'], // Spain
        UA: ['en-US', 'uk-UA', 'ru-UA'], // Ukraine
        GB: ['en-US', 'en-GB'], // United Kingdom
        US: ['en-US', 'es-US'] // United States
    }),

    siteUserLanguageList: new ObjectRef({
        default: ['en-US']
    })

    // A list of supported devices for the study.
    /*
     supportedDevices : [{
        name: 'iPhone',
        match: [['Safari']]
    }, {
        name: 'Nexus S',
        match: [['Safari']]
    }, {
        name: 'Galaxy Nexus',
        match: [['Safari']]
    }],
    */

};
