export default {
    /*
    If your database tests are all failing due to credential issues,
    add your personal staging sql management studio credentials here to override.
 */
    user: '',
    password: '',

    // add string IGs to this array to exclude them from database validation
    excludeIGs: ['PT', 'Add_User', 'RaterTraining', 'Time_Confirmation', 'TRANSCRIPTION_REPORT_DATE'],

    // add string IGs to this array to explicitly include them in database validation
    includeIGs: ['Protocol'],

    // add string SUs to this array to exclude them from database validation
    excludeSUs: ['New_User', 'Rater_Training', 'TC'],

    // add string SUs to this array to explicitly include them in database validation
    includeSUs: ['Assignment'],

    /*
     * add objects containing a string IG and
     * an array of string ITs to exclude those ITs from database validation
     *
     * IGs listed in excludeIGs will be automatically ignored for the validateITs test
    */
    excludeITs: [
        {
            IG: 'RaterTraining',
            ITs: ['RATER_TRAINING_Q1']
        }
    ],

    // add objects containing a string IG and an array of string ITs
    // to explicitly include those ITs in database validation
    includeITs: [
        {
            IG: 'Protocol',
            ITs: ['Protocol']
        }
    ]
};
