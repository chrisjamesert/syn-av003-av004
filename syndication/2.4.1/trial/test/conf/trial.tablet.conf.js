'use strict';

// jscs:disable disallowKeywords
var manifest = require('../../../app/test/conf/manifest'),
    sharedConfig = require('../../../app/test/conf/shared.conf');

module.exports = function (config) {

    sharedConfig(config);

    config.set({

        // list of files / patterns to load in the browser
        // Order matters!
        files: manifest.mergeFilesFor('thirdparty', 'core', 'helpers', '~tabletUnitTest'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude'),

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'core/templates/*.ejs': ['html2js'],
            'logpad/templates/*.ejs': ['html2js'],
            'trainer/logpad/*.ejs': ['html2js'],
            '../trial/tablet/**/*.spec.js': ['browserify'],
            '../trial/test/specs/tablet/**/*.spec.js': ['browserify'],
            'test/helpers/*.js': ['browserify'],
            '../trial/tablet/**/*.js': ['coverage']
        },

        junitReporter: {
            outputDir: '../junit',
            outputFile: 'TEST-results-trial.tablet.xml',
            suite: 'trial.tablet',
            useBrowserName: false
        },

        coverageReporter: {
            type: 'cobertura',
            dir: '../coverage',
            file: 'trial.tablet-coverage.xml'
        }
    });
};
