Some files in this directory will have the suffix '.partialspec.js'
These are intended to be functions for real jasmine unit tests ('.spec.js'), that will not execute by themselves.

Instead, they will be imported into other specs and executed as part of a larger spec.
This allows the same tests to be shared among specs, or similar tests to be defined once with paramaters passed in.

As the partialspec functions cannot implictly reference variables scoped in the real specs,
they must be passed in as paramaters, or bound to the function and referenced via 'this'.

Note that 'beforeAll()' adjustments of variables seem to not carry into partialspec functions called
    as part of nested describes. Until the cause is determined, synchronus variable adjustments that should occur
    should be done within the describe block but external to any beforeAll call that would otherwise be utilized.
    Such synchronus items will evaluate before test cases are executed, but asychronus still require a beforeAll,
    and so are difficult to separate into partialspec functions.

