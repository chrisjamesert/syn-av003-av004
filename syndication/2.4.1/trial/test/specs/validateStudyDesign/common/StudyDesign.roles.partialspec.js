/* eslint-disable no-use-before-define */
import { getNested } from 'core/utilities/coreUtilities';

export function rolesTests (studyDesign, device) {
    if (device === 'tablet') {
        describe('tablet role settings', () => {
            it('sitePad.loginRoles should be defined', () => {
                let loginRoles = getNested('sitePad.loginRoles', studyDesign);
                expect(loginRoles).toBeDefined();
                expect(loginRoles).toBeAnArray();
                expect(loginRoles).not.toBeEmpty();
            });
            it('sitePad.visitRoles should be defined', () => {
                let visitRoles = getNested('sitePad.visitRoles', studyDesign);
                expect(visitRoles).toBeDefined();
                expect(visitRoles).toBeAnArray();
                expect(visitRoles).not.toBeEmpty();
            });
            it('sitePad.transcriptionRoles should be defined', () => {
                let transcriptionRoles = getNested('sitePad.transcriptionRoles', studyDesign);
                expect(transcriptionRoles).toBeDefined();
                expect(transcriptionRoles).toBeAnArray();
                expect(transcriptionRoles).not.toBeEmpty();
            });
            it('sitePad.diaryBackoutRoles should be defined', () => {
                let diaryBackoutRoles = getNested('sitePad.diaryBackoutRoles', studyDesign);
                expect(diaryBackoutRoles).toBeDefined();
                expect(diaryBackoutRoles).toBeAnArray();
                expect(diaryBackoutRoles).not.toBeEmpty();
            });
            describe('subjectRole', () => {
                let subjectRole = getNested('sitePad.subjectRole', studyDesign);
                it('sitePad.subjectRole should be defined', () => {
                    expect(subjectRole).toBeDefined();
                    expect(subjectRole).toBeString();
                });
                it('sitePad.subjectRole should match \'subject\'', () => {
                    expect(subjectRole).toMatch('subject');
                });
            });
            describe('adminUserFilter', () => {
                let adminUserFilter = getNested('sitePad.adminUserFilter', studyDesign);
                it('sitePad.adminUserFilter should be defined', () => {
                    expect(adminUserFilter).toBeDefined();
                });
                it('sitePad.adminUserFilter should be either null or a function', () => {
                    let definedCorrectly = adminUserFilter === null || typeof adminUserFilter === 'function';
                    expect(definedCorrectly).toBeTruthy();
                });
            });
        });
    }
    describe('lastDiaryRole', () => {
        let lastDiaryRole = getNested('lastDiaryRole', studyDesign);
        it('lastDiaryRole should be defined', () => {
            expect(lastDiaryRole).toBeDefined();
            expect(lastDiaryRole).not.toBeEmpty();
        });
        it('lastDiaryRole.IG should be defined as \'CG\'', () => {
            let ldrIG = getNested('IG', lastDiaryRole);
            expect(ldrIG).toBeDefined();
            expect(ldrIG).toBeString();
            expect(ldrIG).toMatch('CG');
        });
        it('lastDiaryRole.IT should be defined as \'LPARole\'', () => {
            let ldrIT = getNested('IT', lastDiaryRole);
            expect(ldrIT).toBeDefined();
            expect(ldrIT).toBeString();
            expect(ldrIT).toMatch('LPARole');
        });
    });
    it('showPasswordRules should be defined', () => {
        let showPWR = getNested('showPasswordRules', studyDesign);
        expect(showPWR).toBeDefined();
        expect(showPWR).toBeBoolean();
    });

    let pwdF = getNested('defaultPasswordFormat', studyDesign);
    passwordFormat(pwdF, 'defaultPasswordFormat', true);

    let ucc = getNested('unlockCodeConfig', studyDesign);
    unlockCodeConfigTests(ucc);

    describe('roles', () => {
        let roles = getNested('roles', studyDesign);
        it('roles should be defined', () => {
            expect(roles).toBeDefined();
            expect(roles).toBeAnArray();
            expect(roles).not.toBeEmpty();
        });
        describe('subject role', () => {
            let subjectRole = _(roles).find({ id: 'subject' }),
                prefix = 'roles[subject].';
            it('roles should have a subject role element defined', () => {
                expect(subjectRole).toBeDefined();
            });
            displayName(subjectRole, prefix);
            lastDiaryRoleCode(subjectRole, prefix);
            defaultAffidavit(subjectRole, prefix, studyDesign);
            userSelectIconClass(subjectRole, prefix);
            uCC(subjectRole, prefix);

            let subjectPWF = getNested('passwordFormat', subjectRole);
            passwordFormat(subjectPWF, 'passwordFormat', false);

            product(subjectRole, prefix, device, true);

            passwordInputType(subjectRole, prefix);
        });
    });
}

function unlockCodeConfigTests (ucc, prefix = '') {
    it(`${prefix}unlockCodeConfig should be defined`, () => {
        expect(ucc).toBeDefined();
        expect(ucc).not.toBeEmpty();
    });
    describe('seedCode', () => {
        let seedCode = getNested('seedCode', ucc);
        it(`${prefix}unlockCodeConfig.seedCode should be defined`, () => {
            expect(seedCode).toBeDefined();
            expect(seedCode).toBeNumber();
        });
        it(`${prefix}unlockCodeConfig.seedCode should be four digits`, () => {
            expect(seedCode).toBeLessThan(10000);
            expect(seedCode).toBeGreaterThanOrEqual(1000);
        });
    });
    describe('codeLength', () => {
        let codeLength = getNested('codeLength', ucc);
        it(`${prefix}unlockCodeConfig.codeLength should be defined`, () => {
            expect(codeLength).toBeDefined();
            expect(codeLength).toBeNumber();
        });
        it(`${prefix}unlockCodeConfig.codeLength should be 6`, () => {
            expect(codeLength).toBe(6);
        });
    });
}

function displayName (roleObject, prefix = '') {
    it(`${prefix}displayName should be defined`, () => {
        let displayName = getNested('displayName', roleObject);
        expect(displayName).toBeDefined();
        expect(displayName).toBeString();
    });
}

function lastDiaryRoleCode (roleObject, prefix = '') {
    it(`${prefix}lastDiaryRoleCode should be defined`, () => {
        let lastDiaryRoleCode = getNested('lastDiaryRoleCode', roleObject);
        expect(lastDiaryRoleCode).toBeDefined();
        expect(lastDiaryRoleCode).toBeNumber();
    });
}

function defaultAffidavit (roleObject, prefix = '', studyDesign) {
    describe(`${prefix}defaultAffidavit should be defined`, () => {
        let defAff = getNested('defaultAffidavit', roleObject);
        it('roles[subject].defaultAffidavit should be defined', () => {
            expect(defAff).toBeDefined();
            expect(defAff).toBeString();
        });
        it('roles[subject].defaultAffidavit should reference an existing affidavit', () => {
            let affidavits = getNested('affidavits', studyDesign);
            expect(_(affidavits).find({ id: defAff })).toBeTruthy();
        });
    });
}

function userSelectIconClass (roleObject, prefix = '') {
    it(`${prefix}userSelectIconClass should be defined`, () => {
        let usIC = getNested('userSelectIconClass', roleObject);
        expect(usIC).toBeDefined();
        expect(usIC).toBeString();
    });
}

function uCC (roleObject, prefix = '') {
    describe(`${prefix}unlockCodeConfig`, () => {
        let ucc = getNested('unlockCodeConfig', roleObject);
        if (ucc) {
            unlockCodeConfigTests(ucc, prefix);
        } else {
            it(`${prefix}unlockCodeConfig is not required`, () => {
                expect(true).toBeTruthy();
            });
        }
    });
}

function addPermissionList (roleObject, prefix = '') {
    describe(`${prefix}addPermissionList`, () => {
        it(`${prefix}addPermissionList should be defined`, () => {
            let apl = getNested('addPermissionsList', roleObject);
            expect(apl).toBeDefined();
            expect(apl).toBeAnArray();
        });
    });
}

function passwordFormat (pwdF, prefix = 'defaultPasswordFormat', required) {
    describe(`${prefix}`, () => {
        let expectedProperties = ['max', 'min', 'lower', 'upper', 'alpha', 'numeric', 'special', 'custom', 'allowRepeating', 'allowConsecutive'];
        if (required) {
            it(`${prefix} should be defined`, () => {
                expect(pwdF).toBeDefined();
                expect(pwdF).not.toBeEmpty();
            });
            it(`${prefix} should contain all expected properties`, () => {
                expectedProperties.forEach((prop) => {
                    expect(getNested(prop, pwdF)).toBeDefined();
                });
            });
        }
        if (pwdF) {
            it(`${prefix} should not be empty if defined`, () => {
                expect(pwdF).not.toBeEmpty();
            });
            it(`${prefix} should only contain appropriate properties`, () => {
                for (let prop in pwdF) {
                    // noinspection JSUnfilteredForInLoop
                    expect(expectedProperties).toContain(prop);
                }
            });
            let max = getNested('max', pwdF);
            if (max !== undefined) {
                it(`${prefix}.max should be a number`, () => {
                    expect(max).toBeNumber();
                });
            }
            let min = getNested('min', pwdF);
            if (min !== undefined) {
                it(`${prefix}.min should be a number`, () => {
                    expect(min).toBeNumber();
                });
            }
            if (max !== undefined && min !== undefined) {
                it(`${prefix}.min should be less than or equal to ${prefix}.max`, () => {
                    expect(min).toBeLessThanOrEqual(max);
                });
            }
            let lower = getNested('lower', pwdF);
            if (lower !== undefined) {
                it(`${prefix}.lower should be a number`, () => {
                    expect(lower).toBeNumber();
                });
            }
            let upper = getNested('upper', pwdF);
            if (upper !== undefined) {
                it(`${prefix}.upper should be a number`, () => {
                    expect(upper).toBeNumber();
                });
            }
            let alpha = getNested('alpha', pwdF);
            if (alpha !== undefined) {
                it(`${prefix}.alpha should be a number`, () => {
                    expect(alpha).toBeNumber();
                });
            }
            let numeric = getNested('numeric', pwdF);
            if (numeric !== undefined) {
                it(`${prefix}.numeric should be a number`, () => {
                    expect(numeric).toBeNumber();
                });
            }
            let special = getNested('special', pwdF);
            if (special !== undefined) {
                it(`${prefix}.special should be a number`, () => {
                    expect(special).toBeNumber();
                });
            }
            if (
                max !== undefined &&
                (
                    lower !== undefined ||
                    upper !== undefined ||
                    alpha !== undefined ||
                    numeric !== undefined ||
                    special !== undefined
                )
            ) {
                it(`${prefix} character type requirements should not exceed maximum character count`, () => {
                    let sum = _.chain([lower, upper, alpha, numeric, special])
                        .filter(req => !isNaN(req))
                        .reduce((tot, val) => {
                            return tot + val;
                        }, 0)
                        .value();
                    expect(sum).toBeLessThanOrEqual(max);
                });
            }
            let custom = getNested('custom', pwdF);
            if (custom !== undefined) {
                describe(`${prefix}.custom`, () => {
                    it(`${prefix}.custom should an array`, () => {
                        expect(custom).toBeAnArray();
                    });
                    if (custom.length) {
                        it(`${prefix}.custom should have regex or function elements if populated`, () => {
                            let validateElements = custom.reduce((retbool, element) => {
                                let checkElement = element instanceof RegExp || typeof element === 'function';
                                return retbool && checkElement;
                            }, true);
                            expect(validateElements).toBeTruthy();
                        });
                    }
                });
            }
            let allowRepeating = getNested('allowRepeating', pwdF);
            if (allowRepeating !== undefined) {
                it(`${prefix}.allowRepeating should be a boolean`, () => {
                    expect(allowRepeating).toBeBoolean();
                });
            }
            let allowConsecutive = getNested('allowConsecutive', pwdF);
            if (allowConsecutive !== undefined) {
                it(`${prefix}.allowConsecutive should be a boolean`, () => {
                    expect(allowConsecutive).toBeBoolean();
                });
            }
        }
    });
}

function product (roleObject, prefix = '', device, shouldContain) {
    describe(`${prefix}product`, () => {
        let product = getNested('product', roleObject);
        it(`${prefix}product should be defined`, () => {
            expect(product).toBeDefined();
            expect(product).toBeAnArray();
        });
        let productDevice;
        switch (device) {
            case 'tablet':
                productDevice = 'sitepad';
                break;
            case 'handheld':
                productDevice = 'logpad';
                break;
            case 'web':
                productDevice = 'web';
                break;
            default:
        }
        if (shouldContain) {
            it(`${prefix}product should contain \'${productDevice}\'`, () => {
                expect(product).not.toBeEmpty();
                expect(product).toContain(productDevice);
            });
        }
    });
}

function passwordInputType (roleObject, prefix = '') {
    describe(`${prefix}passwordInputType`, () => {
        let pIT = getNested('passwordInputType', roleObject);
        it(`${prefix}passwordInputType should be defined`, () => {
            expect(pIT).toBeDefined();
            expect(pIT).toBeString();
        });
        it(`${prefix}passwordInputType should be an expected type`, () => {
            let expectedTypes = ['password', 'number', 'tel', 'text'];
            expect(expectedTypes).toContain(pIT);
        });
    });
}
