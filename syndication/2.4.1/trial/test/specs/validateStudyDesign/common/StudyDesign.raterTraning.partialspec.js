import { getNested } from 'core/utilities/coreUtilities';

export function raterTrainingConfigTests (studyDesign, device) {
    let rtConfig = getNested(`raterTrainingConfig.${device}`, studyDesign);
    it(`raterTrainingConfig.${device} should be defined`, () => {
        expect(rtConfig).toBeDefined();
        expect(rtConfig).not.toBeEmpty();
    });
    it(`raterTrainingConfig.${device}.useRaterTraining should be defined`, () => {
        let uRT = getNested('useRaterTraining', rtConfig);
        expect(uRT).toBeDefined();
        expect(uRT).toBeBoolean();
    });
    if (getNested('useRaterTraining', rtConfig)) {
        describe('useRaterTraining true', () => {
            describe('awsKey', () => {
                let awsKey = getNested('awsKey', rtConfig);
                it(`raterTrainingConfig.${device}.awsKey should be defined`, () => {
                    expect(awsKey).toBeDefined();
                    expect(awsKey).toBeString();
                });
                it(`raterTrainingConfig.${device}.awsKey should match 'AKIAIVMTD47W6QHZNLXQ'`, () => {
                    expect(awsKey).toMatch('AKIAIVMTD47W6QHZNLXQ');
                });
            });
            describe('awsSecretKey', () => {
                let awsSecretKey = getNested('awsSecretKey', rtConfig);
                it(`raterTrainingConfig.${device}.awsSecretKey should be defined`, () => {
                    expect(awsSecretKey).toBeDefined();
                    expect(awsSecretKey).toBeString();
                });
                it(`raterTrainingConfig.${device}.awsSecretKey should match 'r+sixeWZ/YZ+bE2jj4hsQ7LH5z5SMbcbZXaJ5dRx'`, () => {
                    expect(awsSecretKey).toMatch(/^r\+sixeWZ\/YZ\+bE2jj4hsQ7LH5z5SMbcbZXaJ5dRx$/);
                });
            });
            describe('awsRegion', () => {
                let awsRegion = getNested('awsRegion', rtConfig);
                it(`raterTrainingConfig.${device}.awsRegion should be defined`, () => {
                    expect(awsRegion).toBeDefined();
                    expect(awsRegion).toBeString();
                });
                it(`raterTrainingConfig.${device}.awsRegion should match 'us-east-1'`, () => {
                    expect(awsRegion).toMatch('us-east-1');
                });
            });
            describe('awsBucket', () => {
                let awsBucket = getNested('awsBucket', rtConfig);
                it(`raterTrainingConfig.${device}.awsBucket should be defined`, () => {
                    expect(awsBucket).toBeDefined();
                    expect(awsBucket).toBeString();
                });
                it(`raterTrainingConfig.${device}.awsBucket should match 'ert-training'`, () => {
                    expect(awsBucket).toMatch('ert-training');
                });
            });
            describe('pages', () => {
                let pages = getNested('pages', rtConfig);
                it(`raterTrainingConfig.${device}.pages should be defined`, () => {
                    expect(pages).toBeDefined();
                });
                describe('default', () => {
                    let def = getNested('default', pages);
                    it(`raterTrainingConfig.${device}.pages.default should be defined`, () => {
                        expect(def).toBeDefined();
                        expect(def).toBeString();
                    });
                    it(`raterTrainingConfig.${device}.pages.default should match 'index.html'`, () => {
                        expect(getNested('default', pages)).toMatch('index.html');
                    });
                });
            });
            describe('zipFile', () => {
                let zipFile = getNested('zipFile', rtConfig);
                it(`raterTrainingConfig.${device}.zipFile should be defined`, () => {
                    expect(zipFile).toBeDefined();
                    expect(zipFile).toBeBoolean();
                });
                it(`raterTrainingConfig.${device}.zipFile should be true`, () => {
                    expect(zipFile).toBeTruthy();
                });
            });
            describe('url', () => {
                let url = getNested('fallBackBaseURL', rtConfig);
                let rootDir = getNested('rootDirectory', rtConfig);
                describe('fallBackBaseURL', () => {
                    it(`raterTrainingConfig.${device}.fallBackBaseURL should be defined`, () => {
                        expect(url).toBeDefined();
                        expect(url).toBeString();
                    });
                    it(`raterTrainingConfig.${device}.fallBackBaseURL should match the expected pattern`, () => {
                        let pattern = /^http:\/\/ert-training.s3.amazonaws.com\/[\w{}]+$/i;
                        expect(url).toMatch(pattern);
                    });
                    it(`raterTrainingConfig.${device}.fallBackBaseURL should be updated from the default`, () => {
                        expect(url).not.toMatch('http://ert-training.s3.amazonaws.com/{{{ROOT_DIRECTORY}}}');
                    });
                });
                describe('rootDirectory', () => {
                    it(`raterTrainingConfig.${device}.rootDirectory should be defined`, () => {
                        expect(rootDir).toBeDefined();
                        expect(rootDir).toBeString();
                    });
                    it(`raterTrainingConfig.${device}.rootDirectory should be updated from the default`, () => {
                        expect(rootDir).not.toMatch('{{{ROOT_DIRECTORY}}}');
                    });
                });
                it(`raterTrainingConfig.${device}.rootDirectory should match root path of raterTrainingConfig.${device}.fallBackBaseURL`, () => {
                    let capture = url.match(/^http:\/\/ert-training.s3.amazonaws.com\/([\w{}]+)$/i);
                    expect(capture[1]).toBeDefined();
                    expect(rootDir).toMatch(capture[1]);
                });
            });
        });
    }
}
