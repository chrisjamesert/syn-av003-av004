/* eslint-disable no-use-before-define */
import { getNested } from 'core/utilities/coreUtilities';

export function environmentsTests (studyDesign) {
    it('allowEnvironmentSelect should be defined as true', () => {
        let aES = getNested('allowEnvironmentSelect', studyDesign);
        expect(aES).toBeDefined();
        expect(aES).toBeBoolean();
        expect(aES).toBeTruthy();
    });
    it('serviceUrlFormat should be defined as \'https://{{study}}.phtnetpro.com\'', () => {
        let sUF = getNested('serviceUrlFormat', studyDesign);
        expect(sUF).toBeDefined();
        expect(sUF).toBeString();
        expect(sUF).toMatch('https://{{study}}.phtnetpro.com');
    });
    describe('environments', () => {
        let envs = getNested('environments', studyDesign);
        it('environments should be defined as an array', () => {
            expect(envs).toBeDefined();
            expect(envs).toBeAnArray();
            expect(envs).not.toBeEmpty();
        });
        let dev = _(envs).find(e => e.id === 'dev');
        console.log(dev);
        describe('dev', () => {
            singleEnvironment(dev, 'dev');
        });
        let staging = _(envs).find(e => e.id === 'staging');
        console.log(staging);
        describe('staging', () => {
            singleEnvironment(staging, 'staging');
        });
        let ust = _(envs).find(e => e.id === 'ust');
        console.log(ust);
        describe('ust', () => {
            singleEnvironment(ust, 'ust');
        });
        let prod = _(envs).find(e => e.id === 'production');
        console.log(prod);
        describe('production', () => {
            singleEnvironment(prod, 'production');
        });

        it('all required environments should have appropriately matching studyDbName', () => {
            let devRegex = /^(\w+)_1$/,
                ustRegex = /^(\w+)_UST$/,
                stageProdRegex = /^(\w+)$/,
                devToMatch = '',
                stageToMatch = '',
                ustToMatch = '',
                prodToMatch = '';
            console.info(dev);
            if (dev && getNested('studyDBName', dev)) {
                devToMatch = devRegex.exec(getNested('studyDBName', dev));
                console.info(devToMatch);
            }
            console.info(staging);
            if (staging && getNested('studyDBName', staging)) {
                stageToMatch = stageProdRegex.exec(getNested('studyDBName', staging));
                console.info(stageToMatch);
            }
            console.info(ust);
            if (ust && getNested('studyDBName', ust)) {
                ustToMatch = ustRegex.exec(getNested('studyDBName', ust));
                console.info(ustToMatch);
            }
            console.info(prod);
            if (prod && getNested('studyDBName', prod)) {
                prodToMatch = stageProdRegex.exec(getNested('studyDBName', prod));
                console.info(prodToMatch);
            }
            expect(devToMatch).toMatch(stageToMatch);
            expect(stageToMatch).toMatch(ustToMatch);
            expect(ustToMatch).toMatch(prodToMatch);
            expect(prodToMatch).toMatch(stageToMatch);
        });
    });
}

function singleEnvironment (env, id) {
    let expectedModes,
        urlSegmentToMatch,
        dbSuffix,
        envTimeT;

    expectedModes = [];
    switch (id) {
        case 'production':
            expectedModes.push('depot', 'provision');
            urlSegmentToMatch = '';
            envTimeT = false;
            break;
        default:
            expectedModes.push('provision');
            urlSegmentToMatch = id;
            envTimeT = true;
    }
    switch (id) {
        case 'dev':
            dbSuffix = '_1';
            break;
        case 'ust':
            dbSuffix = '_UST';
            break;
        default:
            dbSuffix = '';
    }

    it(`environments should contain a config for the ${id} environment`, () => {
        expect(env).toBeDefined();
    });
    it(`${id} environment should have timeTravel ${envTimeT ? 'enabled' : 'disabled'}`, () => {
        let timeTravel = getNested('timeTravel', env);
        expect(timeTravel).toBeDefined();
        expect(timeTravel).toBeBoolean();
        expect(timeTravel).toBe(envTimeT);
    });
    describe('modes', () => {
        let modes = getNested('modes', env);
        it(`${id} environment modes should be a populated array`, () => {
            expect(modes).toBeDefined();
            expect(modes).toBeAnArray();
            expect(modes).not.toBeEmpty();
        });
        it(`${id} environment should have the following modes: ${expectedModes.join(', ')}`, () => {
            for (let i = 0; i < expectedModes.length; i++) {
                expect(modes).toContain(expectedModes[i]);
            }
        });
    });
    describe('url', () => {
        let url = getNested('url', env);
        it(`${id} environment should have a url`, () => {
            expect(url).toBeDefined();
            expect(url).toBeString();
        });
        it(`${id} environment url should match the expected pattern`, () => {
            let pattern = new RegExp(`^https:\/\/\\w+${urlSegmentToMatch}\.phtnetpro.com$`, 'i');
            expect(url).toMatch(pattern);
        });
        it(`${id} environment url should be updated from default placeholder`, () => {
            let pattern = new RegExp(`^https:\/\/STUDYNAME${urlSegmentToMatch}.phtnetpro.com$`, 'i');
            expect(url).not.toMatch(pattern);
        });
    });
    describe('studyDbName', () => {
        let studyDbName = getNested('studyDbName', env);
        it(`${id} environment should have a studyDbName`, () => {
            expect(studyDbName).toBeDefined();
            expect(studyDbName).toBeString();
        });
        it(`${id} environment studyDbName should match the expected pattern`, () => {
            let pattern = new RegExp(`^\\w+${dbSuffix}$`);
            expect(studyDbName).toMatch(pattern);
        });
        it(`${id} environment studyDbName should be updated from default placeholder`, () => {
            let pattern = new RegExp(`^SPONSOR_PROTOCOL${dbSuffix}$`);
            expect(studyDbName).not.toMatch(pattern);
        });
    });
}
