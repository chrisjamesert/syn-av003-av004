import { getNested } from 'core/utilities/coreUtilities';

export function timeZoneConfigTests (studyDesign) {
    let androidTZ = getNested('android.timeZoneOptions', studyDesign),
        iOSTZ = getNested('ios.timeZoneOptions', studyDesign),
        webTZ = getNested('web.timeZoneOptions', studyDesign),
        windowsTZ = getNested('windows.timeZoneOptions', studyDesign);

    it('android.timeZoneOptions should be defined and not empty', () => {
        expect(androidTZ).toBeDefined();
        expect(androidTZ).toEqual(jasmine.any(Array));
        expect(androidTZ.length).toBeGreaterThan(0);
    });
    it('ios.timeZoneOptions should be defined and not empty', () => {
        expect(iOSTZ).toBeDefined();
        expect(iOSTZ).toEqual(jasmine.any(Array));
        expect(iOSTZ.length).toBeGreaterThan(0);
    });
    it('web.timeZoneOptions should be defined and not empty', () => {
        expect(webTZ).toBeDefined();
        expect(webTZ).toEqual(jasmine.any(Array));
        expect(webTZ.length).toBeGreaterThan(0);
    });
    it('windows.timeZoneOptions should be defined and not empty', () => {
        expect(windowsTZ).toBeDefined();
        expect(windowsTZ).toEqual(jasmine.any(Array));
        expect(windowsTZ.length).toBeGreaterThan(0);
    });
}
