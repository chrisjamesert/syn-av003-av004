import { getNested } from 'core/utilities/coreUtilities';

/**
 * series of unit tests to validate trial/common_study/common_study-design settings
 */
export function commonStudyDesign (studyDesign) {
    it('StudyDesign should exist as part of the assets object', () => {
        expect(studyDesign).toBeDefined();
    });
    it('defaultLanguage should be defined', () => {
        let defLang = getNested('defaultLanguage', studyDesign);
        expect(defLang).toBeDefined();
        expect(defLang).toBeString();
    });
    it('defaultLocale should be defined', () => {
        let defLoc = getNested('defaultLocale', studyDesign);
        expect(defLoc).toBeDefined();
        expect(defLoc).toBeString();
    });
    it('maxUnlockAttempts should be defined', () => {
        let mUA = getNested('maxUnlockAttempts', studyDesign);
        expect(mUA).toBeDefined();
        expect(mUA).toBeNumber();
    });
    it('unlockCodeLockoutTime should be defined', () => {
        let mCLT = getNested('unlockCodeLockoutTime', studyDesign);
        expect(mCLT).toBeDefined();
        expect(mCLT).toBeNumber();
    });
    it('maxLoginAttempts should be defined', () => {
        let mLA = getNested('maxLoginAttempts', studyDesign);
        expect(mLA).toBeDefined();
        expect(mLA).toBeNumber();
    });
    it('lockoutTime should be defined', () => {
        let lT = getNested('lockoutTime', studyDesign);
        expect(lT).toBeDefined();
        expect(lT).toBeNumber();
    });
    it('userSyncTimeStampThreshold should be defined', () => {
        let uSyncTST = getNested('userSyncTimeStampThreshold', studyDesign);
        expect(uSyncTST).toBeDefined();
        expect(uSyncTST).toBeNumber();
    });
    describe('lastDiarySyncUnscheduledSUList', () => {
        let lDSyc = getNested('lastDiarySyncUnscheduledSUList', studyDesign);
        it('lastDiarySyncUnscheduledSUList should be defined', () => {
            expect(lDSyc).toBeDefined();
        });
        if (lDSyc) {
            it('lastDiarySyncUnscheduledSUList should be an array of strings or null', () => {
                expect(lDSyc).toBeAnArray();
                for (let i = 0; i < lDSyc.length; i++) {
                    expect(lDSyc[i]).toBeString();
                }
            });
        } else {
            it('lastDiarySyncUnscheduledSUList should be an array of strings or null', () => {
                expect(lDSyc).toBeNull();
            });
        }
    });
    describe('protocolBuild', () => {
        let protocolBuild = getNested('protocolBuild', studyDesign);
        it('protocolBuild should be defined', () => {
            expect(protocolBuild).toBeDefined();
            expect(protocolBuild).toBeString();
        });
        it('protocolBuild should be updated from the default "TestStudy"', () => {
            expect(protocolBuild).not.toBe('TestStudy');
        });
    });
    describe('clientName', () => {
        let clientName = getNested('clientName', studyDesign);
        it('clientName should be defined', () => {
            expect(clientName).toBeDefined();
            expect(clientName).toBeString();
        });
        it('clientName should be updated from the default "TestStudy"', () => {
            expect(clientName).not.toBe('TestStudy');
        });
    });
    describe('studyProtocol', () => {
        let studyProtocol = getNested('studyProtocol', studyDesign);
        let defProt = getNested('studyProtocol.defaultProtocol', studyDesign);
        it('studyProtocol should be defined', () => {
            expect(studyProtocol).toBeDefined();
        });
        it('studyProtocol.defaultProtocol should be defined', () => {
            expect(defProt).toBeDefined();
            expect(defProt).toBeNumber();
        });
        it('studyProtocol.protocolList should be defined', () => {
            let protocolList = getNested('studyProtocol.protocolList', studyDesign);
            expect(protocolList).toBeDefined();
        });
        it('studyProtocol.protocolList should contain at least a protocol matching the defaultProtocol codelist', () => {
            let defProtRef = getNested(`studyProtocol.protocolList.${defProt}`, studyDesign);
            expect(defProtRef).toBeDefined();
            expect(defProtRef).toBeString();
            expect(defProtRef).not.toBe(`Protocol${defProt}`);
        });
    });
    describe('phase', () => {
        let studyPhase = getNested('studyPhase', studyDesign);
        it('studyPhase should be defined and not empty', () => {
            expect(studyPhase).toBeDefined();
            expect(studyPhase).not.toBeEmpty();
        });
        describe('terminationPhase', () => {
            let terminationPhase = getNested('terminationPhase', studyDesign);
            it('terminationPhase should be defined', () => {
                expect(terminationPhase).toBeDefined();
                expect(terminationPhase).toBeNumber();
            });
            it('terminationPhase should reference an existing phase definition in studyPhase', () => {
                let foundPhase = Object.keys(studyPhase).reduce((retBool, key) => {
                    let phaseCode = studyPhase[key];
                    return retBool || terminationPhase === phaseCode;
                }, false);
                expect(foundPhase).toBeTruthy();
            });
        });
    });
    describe('validInputCharacters', () => {
        let validInput = getNested('validInputCharacters', studyDesign);
        it('validInputCharacters should be defined', () => {
            expect(validInput).toBeDefined();
        });
        it('validInputCharacters should be a RegExp or a String', () => {
            expect((typeof validInput === 'string') || (validInput instanceof RegExp)).toBeTruthy();
        });
    });
    it('supportedBrowsers should be defined', () => {
        expect(getNested('supportedBrowsers', studyDesign)).toBeDefined();
    });
    xit('sitelanguagelist should be defined', () => {
        expect(getNested('sitelanguagelist', studyDesign)).toBeDefined();
    });
}
