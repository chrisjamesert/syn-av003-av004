import { getNested } from 'core/utilities/coreUtilities';

/**
 * series of unit tests to validate trial/handheld/STUDY/study-design/handheld_study-design settings
 */
export function hhStudyDesign (studyDesign) {
    it('studyVersion should be defined', () => {
        expect(getNested('studyVersion', studyDesign)).toBeDefined();
    });
    it('studyDbVersion should be defined', () => {
        expect(getNested('studyDbVersion', studyDesign)).toBeDefined();
    });
    it('enablePartialSync should be defined', () => {
        expect(getNested('enablePartialSync', studyDesign)).toBeDefined();
    });
    it('sessionTimeout should be defined', () => {
        expect(getNested('sessionTimeout', studyDesign)).toBeDefined();
    });
    it('questionnaireTimeout should be defined', () => {
        expect(getNested('questionnaireTimeout', studyDesign)).toBeDefined();
    });
    it('alarmVolumeConfig should be defined', () => {
        expect(getNested('alarmVolumeConfig', studyDesign)).toBeDefined();
    });
    it('alarmVolumeConfig.alarmVolume should be defined', () => {
        expect(getNested('alarmVolumeConfig.alarmVolume', studyDesign)).toBeDefined();
    });
    it('alarmVolumeConfig.vibrate should be defined', () => {
        expect(getNested('alarmVolumeConfig.vibrate', studyDesign)).toBeDefined();
    });
    it('alarmVolumeConfig.resetDelay should be defined', () => {
        expect(getNested('alarmVolumeConfig.resetDelay', studyDesign)).toBeDefined();
    });
    describe('eSenseConfig', () => {
        it('eSenseConfig should be defined', () => {
            expect(getNested('eSenseConfig', studyDesign)).toBeDefined();
        });
        it('eSenseConfig.disableBluetooth should be defined', () => {
            expect(getNested('eSenseConfig.disableBluetooth', studyDesign)).toBeDefined();
        });
        it('eSenseConfig.apiCallTimeout should be defined', () => {
            expect(getNested('eSenseConfig.apiCallTimeout', studyDesign)).toBeDefined();
        });
        it('eSenseConfig.findDevicesTimeout should be defined', () => {
            expect(getNested('eSenseConfig.findDevicesTimeout', studyDesign)).toBeDefined();
        });
        it('eSenseConfig.getRecordsTimeout should be defined', () => {
            expect(getNested('eSenseConfig.getRecordsTimeout', studyDesign)).toBeDefined();
        });
        it('eSenseConfig.AM1Plus should be defined', () => {
            expect(getNested('eSenseConfig.AM1Plus', studyDesign)).toBeDefined();
        });
        describe('AM1Plus', () => {
            it('eSenseConfig.AM1Plus.timeWindow should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow', studyDesign)).toBeDefined();
            });
            it('eSenseConfig.AM1Plus.timeWindow.windowNumber should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow.windowNumber', studyDesign)).toBeDefined();
            });
            it('eSenseConfig.AM1Plus.timeWindow.startHour should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow.startHour', studyDesign)).toBeDefined();
            });
            it('eSenseConfig.AM1Plus.timeWindow.startMinute should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow.startMinute', studyDesign)).toBeDefined();
            });
            it('eSenseConfig.AM1Plus.timeWindow.endHour should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow.endHour', studyDesign)).toBeDefined();
            });
            it('eSenseConfig.AM1Plus.timeWindow.endMinute should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow.endMinute', studyDesign)).toBeDefined();
            });
            it('eSenseConfig.AM1Plus.timeWindow.maxNumber should be defined', () => {
                expect(getNested('eSenseConfig.AM1Plus.timeWindow.maxNumber', studyDesign)).toBeDefined();
            });
        });
    });
    describe('raterTrainingConfig', () => {
        it('raterTrainingConfig should be defined', () => {
            expect(getNested('raterTrainingConfig', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.useRaterTraining should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.useRaterTraining', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.awsKey should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.awsKey', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.awsSecretKey should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.awsSecretKey', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.awsRegion should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.awsRegion', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.awsBucket should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.awsBucket', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.rootDirectory should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.rootDirectory', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.fallBackBaseURL should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.fallBackBaseURL', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.pages should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.pages', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.pages.default should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.pages.default', studyDesign)).toBeDefined();
        });
        it('raterTrainingConfig.logpad.zipFile should be defined', () => {
            expect(getNested('raterTrainingConfig.logpad.zipFile', studyDesign)).toBeDefined();
        });

        if (getNested('raterTrainingConfig.logpad.useRaterTraining', studyDesign) === true) {
            it('raterTrainingConfig.logpad.awsKey should be populated', () => {
                expect(getNested('raterTrainingConfig.logpad.awsKey', studyDesign)).not.toBe('');
            });
            it('raterTrainingConfig.logpad.awsSecretKey should be populated', () => {
                expect(getNested('raterTrainingConfig.logpad.awsSecretKey', studyDesign)).not.toBe('');
            });
            it('raterTrainingConfig.logpad.awsRegion should be populated', () => {
                expect(getNested('raterTrainingConfig.logpad.awsRegion', studyDesign)).not.toBe('');
            });
            it('raterTrainingConfig.logpad.awsBucket should be populated', () => {
                expect(getNested('raterTrainingConfig.logpad.awsBucket', studyDesign)).not.toBe('');
            });
            it('raterTrainingConfig.logpad.rootDirectory should be populated', () => {
                expect(getNested('raterTrainingConfig.logpad.rootDirectory', studyDesign)).not.toBe('rater');
            });
            it('raterTrainingConfig.logpad.fallBackBaseURL should be populated', () => {
                expect(getNested('raterTrainingConfig.logpad.fallBackBaseURL', studyDesign)).not.toBe('');
            });
            it('raterTrainingConfig.logpad.pages.defaut should be "index.html"', () => {
                expect(getNested('raterTrainingConfig.logpad.pages.default', studyDesign)).toBe('index.html');
            });
            it('raterTrainingConfig.logpad.zipFile should be true', () => {
                expect(getNested('raterTrainingConfig.logpad.zipFile', studyDesign)).toBeTruthy();
            });
        }
    });
}
