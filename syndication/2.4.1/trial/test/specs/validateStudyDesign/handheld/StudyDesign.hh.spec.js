import trialImport from 'trial/handheld/index';
import customMatchers from 'PDE_Core/test/customMatchers/index';
import { getNested } from 'core/utilities/coreUtilities';
import { commonStudyDesign } from 'trial/test/specs/validateStudyDesign/common/StudyDesign.common.partialspec';
import { hhStudyDesign } from './StudyDesign.hh.partialspec';
import { environmentsTests } from 'trial/test/specs/validateStudyDesign/common/StudyDesign.environments.partialspec';
import { raterTrainingConfigTests } from 'trial/test/specs/validateStudyDesign/common/StudyDesign.raterTraning.partialspec';
import { timeZoneConfigTests } from 'trial/test/specs/validateStudyDesign/common/StudyDesign.timeZoneConfig.partialspec';
import { affidavitTests } from 'trial/test/specs/validateStudyDesign/common/StudyDesign.affidavits.partialspec';
import { rolesTests } from 'trial/test/specs/validateStudyDesign/common/StudyDesign.roles.partialspec';

let studyDesign = getNested('assets.studyDesign', trialImport);

beforeAll(() => {
    jasmine.addMatchers(customMatchers);
});


describe('StudyDesign_common', () => {
    commonStudyDesign(studyDesign);
});
describe('StudyDesign_handheld', () => {
    hhStudyDesign(studyDesign);
});
describe('environments', () => {
    environmentsTests(studyDesign);
});
describe('raterTrainingConfig', () => {
    raterTrainingConfigTests(studyDesign, 'logpad');
});
describe('timeZoneConfig', () => {
    timeZoneConfigTests(studyDesign);
});
describe('affidavits', () => {
    affidavitTests(studyDesign);
});
describe('roles', () => {
    rolesTests(studyDesign, 'handheld');
});
