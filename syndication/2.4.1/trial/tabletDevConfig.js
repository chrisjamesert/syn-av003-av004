LF.Utilities.DevConfig = {
    isPDEVisitEnabled: false,
    allowMultiVisitDays: false,
    confOnly: false,

    isConfOnly (visit, reportID) {
        return LF.Utilities.DevConfig.confOnly;
    },
    nextScreenButton(){
        $("body").keydown((a) =>{if(a.key =="`"){$("#nextItem")[0].click()}})
    },
    startDevMode(){
        LF.Utilities.DevConfig.isPDEVisitEnabled = true;
        LF.Utilities.DevConfig.allowMultiVisitDays = true;
        //LF.Utilities.DevConfig.confOnly = true;
        LF.Utilities.DevConfig.nextScreenButton();
    }
};
