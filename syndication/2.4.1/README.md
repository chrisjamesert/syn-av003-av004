# SYNDICATION 2.4.1 PDE TEMPLATE #
Source located at [Syndication_2.4.1_Template_Development](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development)

JIRA Tickets found with [PDE Template Issue Tracker](https://jira.ert.com/jira/issues/?jql=component%20%3D%20%22PDE%20Template%20Issue%20Tracker%22) component

## [VERSION 241.4.0](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/milestones/4#tab-merge-requests) ##
### Merge Requests completed ###
* [Feature: ONEECOA-96873 LanguageSelectNaming](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/17)
* [Feature: New/Edit Patient PT vars](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/13)
* [Feature: New-Patient Protocol](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/14)
* [Feature: ONEECOA-49545 Submit Protocol On Form](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/15)
* [Feature: Subject Sync Custom Vars](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/16)

## HOTFIX VERSION 241.3.3 ##

* separate Validate Study Design unit test feature into separate karma configuration, so it can be ran independently of general trial-folder test scripts.

## HOTFIX VERSION 241.3.2 ##

* [ONEECOA-95041](https://jira.ert.com/jira/browse/ONEECOA-95041) update integration test credentials to generic 'pdereadonly' user

## HOTFIX VERSION 241.3.1 ##

* update .acignore to remove line ignoring root node_modules/**

## [VERSION 241.3.0](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/milestones/1#tab-merge-requests)
### Merge Requests completed ###
* [Feature/ONEECOA-91956: Unit Test Suites](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/11)
* [Feature/ONEECOA-91955 Validation Suites](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/12)
* [ONEECOA-91114: enableFullSync flag](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/10)
* [ONEECOA-83581: getRecordsTimeout](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/9)
* [ONEECOA-83382: VisitUtils](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/8)
* [ONEECOA-90866: case-sensitive path](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/6)

### Testing Suites ###
**Feature/ONEECOA-91956: Unit Test Suites** introduces a framework for easily executing unit tests at a study level, while **Feature/ONEECOA-91955 Validation Suites** provides some out-of-the-box tests for the template that validate your code and its integration with studyworks.
#### Unit Test Suites ####
* PDE_Core/test and trial/test directories to house configuration and test specs
* separate karma conf files (test packages) for each PDE_Core and trial common and modality directory
* spec directories for each common and modality directory, to house jasmine spec files
* grunt tasks for each test package, as well as larger 'test suite' tasks
  * handheld-unit-test-suite for all handheld and common test packages (trial and PDE_Core)
  * tablet-unit-test-suite for all tablet and common test packages (trial and PDE_Core)
  * pde_core-unit-test-suite for template item verification (does not run trial tests)
* a 'force' grunt task, to manipulate the '--force' flag within a series of executing tasks 
* a small xml cleanup utility to prep the test result xml files
* a node module for generating html reports of test results
  * be sure to run npm install from the root directory again to download the required 'junit-viewer' module
* a grunt task to generate html reports as part of the test suites
  * configs for the suites place the generated html in the 'junit/junit-reports' directory
* README files in each spec subdirectory to detail:
  * what test packages the specs will belong to
  * what test suites will execute the specs
  * the intended pattern for describe() statements in the specs, to support html report generation
#### Validation Suites ####
* karma test runner conf files for
  * PDE_Core/common
  * PDE_Core/handheld
  * PDE_Core/tablet
  * trial/common_study
  * trial/handheld
  * trial/tablet
  * integrated handheld
  * integrated tablet
* grunt tasks to execute individual confs, as well as 3 main Suites:
  * handheld-test-suite
  * tablet-test-suite
  * pde_core-test-suite
* a test folder in both PDE_Core and trial, to house configuration files and specs
* a series of .partialspec.js and .spec.js files in trial/test/specs to validate Study Design configurables
* .partialspec.js and .spec.js files in PDE_Core/test/specs/integration to validate SUs, IGs, and ITs against staging
* package.json updates for new node_module dependencies for testing suites
* PDE_Core/tools now includes a TestSupport directory holding:
  * databaseQueries script (creates a small http node service to communicate with a database)
  * xmlCleanup script (corrects minor text issues in junit xml output that prevent html report generation)
  * reportCleanup script (performs minor alterations on junit html reports)
* junit-viewer module, which generates an interactable html report page for each Main Suite
  * html reports can be found within 'junit/junit-reports' after running a {{{handheld/tablet/pde_core}}}-test-suite
* a trial/test/conf/integratedTestConfigs directory with modality specific files, lets you configure some behaviors for the integrated tests:
  * user and password, if you're having sql login issues and want to use your own creds
  * excludeSUs, excludeIGs, excludeITs arrays, to explicitly define items from your configuration that you want to ignore in the database test (example: dummy IGs you don't actually send)
  * includeSUs, includeIGs, includeITs arrays, to add items that weren't caught automatically
    * SUs caught are any from studydesign.questionnaires[*].SU
    * IGs caught are any from studydesign.questions[*].IG
    * ITs caught are any within studydesign.questions[*] where:
      * studydesign.questions[*].IG exists
      * IT exists as a property within any sub object or array of studydesign.questions[*]
      * IT exists as a string value belonging to a property IT, example: IT: 'VST1L'
      
This feature will ensure any template-defined testing suites make it into all future studies ready to be executed.

All study level .spec.js files written and placed within the trial folder, either under a trial/{{{modality}}}, or within trial/test/specs/{{{modality}}}, are executed as part of the individual conf grunt tasks, or as part of their main Test Suite task. They are also bundled within the html reports of the applicable main Test Suite.

**Integration Tests**
These tests will likely produce failures at first, because a configuration can define IGs/ITs/SUs that are later prevented from being submitted. To account for these events, there are configuration files located at trial/test/conf/integratedTestConfigs for both handheld and tablet. These files allow integration tests to explicitly ignore IGs, ITs, and SUs that you add to the appropriate arrays. It also provides 'include' arrays, for values that are an atypical part of the configuration (such as ITs added via studyrules). Using these configs allows a PDE to tailor the automated test to their configuration without requiring the time and sophistication of code that would ignore or pick up the atypical items automatically.

## [VERSION 241.2.0](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/milestones/1#tab-merge-requests) ##
### Merge Requests completed ###
* [ONEECOA-90855: acignore update](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/5)
* [Bugfix/unit test support fixes](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/4)
* [ONEECOA-71284: Backwards Compatibility](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/3)
* [Feature/ONEECOA-83366: addSysvar](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/2)

## HOTFIX VERSION 241.1.1 ##

* [ONEECOA-90841](https://jira.ert.com/jira/browse/ONEECOA-90841): remove retryTransmission action from tablet sync rule

## [VERSION 241.1.0](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/milestones/2#tab-merge-requests) ##
### Merge Requests completed ###
* [Feature/rebase 240.3.0](https://gitlab.ert.com/eCOA-Operations/PDE/Template-Development/Syndication_2.4.1_Template_Development/merge_requests/1)
  * rebase of the 240.3.0 template onto the new core release, 2.4.1
  * includes the 240.3.1 hotfix, noted [here](https://jira.ert.com/jira/browse/ONEECOA-88415)