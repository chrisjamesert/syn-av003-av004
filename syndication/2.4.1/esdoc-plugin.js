exports.onStart = function(ev) {
  // take option
  ev.data.option;
};

exports.onHandleConfig = function(ev) {
  // modify config
  ev.data.config.title = ev.data.config.title;
};

exports.onHandleCode = function(ev) {
  // modify code
  ev.data.code = ev.data.code;
};

exports.onHandleCodeParser = function(ev) {
  // modify parser
  var oldDataParser = ev.data.parser;

  ev.data.parser = function(code){ 
        return oldDataParser(code);
    };
};

exports.onHandleAST = function(ev) {
  // modify AST
  ev.data.ast = ev.data.ast;
};

exports.onHandleTag = function(ev) {
  // modify tag

  for (var i = 0; i < ev.data.tag.length; ++i) {
      var curTag = ev.data.tag[i];
      if (curTag.kind === 'constructor') {
          if (curTag.params.length > 2 && curTag.params[0].name === 'options' && curTag.params[1].name === 'options.model') {
            curTag.params.splice(0, 2);
            for (var i = 0; i < curTag.params.length; ++i) {
              curTag.params[i].name = curTag.params[i].name.replace('options.model.', '');
              if (curTag.params[i].description.indexOf('{dev-only}') === -1) {
                curTag.params[i].name = '**' + curTag.params[i].name + '**';
              } else {
                curTag.params[i].name = curTag.params[i].name.replace('{dev-only}', '');
              }
            }
          }
      }
  }

  ev.data.tag = ev.data.tag;
};

exports.onHandleHTML = function(ev) {
  // modify HTML
  ev.data.html = ev.data.html;
};

exports.onComplete = function(ev) {

  // complete
};