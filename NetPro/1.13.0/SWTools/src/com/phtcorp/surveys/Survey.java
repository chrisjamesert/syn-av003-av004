/*
 * Class: Survey.java
 * Creation date: 05/05/2010
 */

package com.phtcorp.surveys;

/**
 *
 * @author Jsalesi
 */
public class Survey {

    private String m_surveyID;

    private String m_surveyName;

    private Questions m_questions;

    public Survey(String surveyID, String surveyName) {

        m_surveyID = surveyID;
        m_surveyName = surveyName;
    }

    public void setSurveyID(String surveyID) {
        m_surveyID = surveyID;
    }

    public String getSurveyID() {
        return m_surveyID;
    }

    public void setSurveyName(String surveyName) {
        m_surveyID = surveyName;
    }

    public String getSurveyName() {
        return m_surveyName;
    }

    public void addQuestions(Questions questions) {
        m_questions = questions;
    }

    public Questions getQuestions() {
        return m_questions;
    }
}
