/*
 * Name: JSONClinRecord.java
 * Date: 5/17/2010
 */

package com.phtcorp.json;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jsalesi
 */
public class JSONClinRecord {
    
    /**
     * Required strings.
     */
    //Hardcoded for now.
    private transient static final String SUBMIT_METHOD = "ClinSubmitService.submit";
    private transient static final String USERNAME = "cernenwein";
    private transient static final String PASSWORD = "11111111";

    private int id = 0;
    private String method = SUBMIT_METHOD;
    private ArrayList params;

    /**
     * Builds a JSON structure filled with diary data to
     * submit to StudyWorks.
     */
    public JSONClinRecord() {

        params = new ArrayList();
        params.add(USERNAME);
        params.add(PASSWORD);
    }

    /**
     * Getters and setters for each of the fields.
     */
    public int getID() {
        return id;
    }

    public void setID(int newID) {
        id = newID;
    }

    public String getMethodName() {
        return method;
    }

    public void setMethodName(String newMethodName) {
        method = newMethodName;
    }

    public List getParams() {
        return params;
    }

    public void setParam(Object param) {
        params.add(param);
    }

    //This is the actual JSON Object that we'll construct.
    /*{
        "id": 0,
        "method": "ClinSubmitService.submit",
        "params": [
            "cernenwein",
            "11111111",
            "Study_3",
            {
                "igList": {
                    "javaClass": "java.util.Hashtable",
                    "map": {
                        "itemGroup1": {
                            "javaClass": "java.util.ArrayList",
                            "list": [
                                {
                                    "comment": null,
                                    "javaClass": "com.phtcorp.voltron.model.ClinField",
                                    "krIT": "fieldA",
                                    "sysvar": null,
                                    "value": "1"
                                },
                                {
                                    "comment": null,
                                    "javaClass": "com.phtcorp.voltron.model.ClinField",
                                    "krIT": "fieldB",
                                    "sysvar": null,
                                    "value": "1"
                                }
                             ]
                        },
                        "itemGroup2": {
                            "javaClass": "java.util.ArrayList",
                            "list": [
                                {
                                    "comment": null,
                                    "javaClass": "com.phtcorp.voltron.model.ClinField",
                                    "krIT": "fieldAA",
                                    "sysvar": null,
                                    "value": "1"
                                },
                                {
                                    "comment": null,
                                    "javaClass": "com.phtcorp.voltron.model.ClinField",
                                    "krIT": "fieldBA",
                                    "sysvar": null,
                                    "value": "1"
                                }
                            ]
                        }
                    }
                },
                "javaClass": "com.phtcorp.voltron.model.ClinRecord",
                "krDOM": "DOM.1.2.3",
                "krSE": "LogPad",
                "krSER": "0",
                "krSUR": "DailyDiary",
                "krSig": "EnterForm",
                "krTZ": "-18000000",
                "krUC": "UC.1.2.3",
                "krUser": "cernenwein",
                "krpt": "PT.1.2.3",
                "krsu": null,
                "name": "Bob Smith",
                "sigID": "SIGID",
                "signingTS": "18-Aug-2009 10:47:10",
                "startTS": "18-Aug-2009 10:44:00",
                "sysvars": {
                    "javaClass": "java.util.ArrayList",
                    "list": [
                        {
                            "comment": null,
                            "javaClass": "com.phtcorp.voltron.model.ClinField",
                            "krIT": "field1",
                            "sysvar": "SU.hasComments",
                            "value": "1"
                        },
                        {
                            "comment": null,
                            "javaClass": "com.phtcorp.voltron.model.ClinField",
                            "krIT": "field2",
                            "sysvar": null,
                            "value": "No"
                        },
                        {
                            "comment": null,
                            "javaClass": "com.phtcorp.voltron.model.ClinField",
                            "krIT": "field3",
                            "sysvar": "SU.Custom1",
                            "value": "False"
                        }
                    ]
                }
            }
        ]
     }*/
}
