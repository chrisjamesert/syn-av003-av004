/*
 * Name: JSONObject.java
 * Date: 5/17/2010
 */

package com.phtcorp.json;

import com.google.gson.Gson;
import java.lang.reflect.Type;

/**
 *
 * @author Jsalesi
 */
public class JSONObject {

    /**
     * The one and only instance.
     */
    private static JSONObject m_instance;
    /**
     * The 3rd party JSON object wrapped for easy plug-n-play (using googles).
     */
    private static Gson m_jsonObj;

    /**
     * Default ctor.
     */
    private JSONObject() {
        //Do nothing for now.
    }

    // For lazy initialization
    public static synchronized JSONObject getInstance() {

        if (m_instance == null) {
            m_instance = new JSONObject();
            m_jsonObj = new Gson();
        }
        return m_instance;
    }

    /**
     * Calls toJson method from 3rd party Jason lib.
     * @param obj is the object to serialize.
     * @return String containing serialized object.
     */
    public String toJson(Object obj) {
        return m_jsonObj.toJson(obj);
    }

    /**
     * Calls toJson method from 3rd party JSON lib.
     * @param obj is the object to serialize.
     * @param typeOf is object type.
     * @return String containing the serialized object.
     */
    public String toJson(Object obj, Type typeOf) {
        return m_jsonObj.toJson(obj, typeOf);
    }

    /**
     * Calls fromJson method of the the 3rd party JSON lib.
     * @param json is the serialized object to deserialize.
     * @param typeOf is the object type.
     * @return the deserialized object based on the type passed in.
     */
    public Object fromJson(String json, Type typeOf) {
        return m_jsonObj.fromJson(json, typeOf);
    }
}
