/*
 * Name: ClinRecord.java
 * Date: 5/17/2010
 */
package com.phtcorp.clinical;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * This class represents a structure in StudyWorks for submitting clinical data.
 * @author Jsalesi
 */
public class ClinRecord {

    /**
     * Don't know if this is needed yet.
     */
    private transient static final long serialVersionUID = 3763506210812952954L;

    /*<SU krPRESENT="Presentation.001" krPT="PT.140563.A1" sigSE="SE.140563.4"
     krSU="NewUserAgreement" krSUR="2" krSE="LogPad" krPROT="V1" Status="Submitted"
     krDMED="Entry" krDOM="MLPAdmin">
     <IT SysVar="SU.BatteryLevel" krIT="BatteryLevel">56<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.LPVersion" krIT="LPVersion">1.00<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.LogPadDeviceId" krIT="DeviceID">14<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.LogPadStartTime" krIT="LogPadStartTime">18 Aug 2009 10:26:42<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.TZValue" krIT="TZID">3<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.ReportDate" krIT="ReportDate">18 Aug 2009<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.ReportStartDate" krIT="ReportStartDate">18 Aug 2009 10:26:42<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IT SysVar="SU.HasComments" krIT="HasComments">0<Comment ReasonCode="" Purpose="Annotation"><Characters>System Generated</Characters></Comment></IT><IG krIG="NewUserAgreement"><IT krIT="NewUserAgreement">1</IT></IG>
     <Sig krSIG="NewInvestigator" Source="LogPad" krUSER="LP.SP417_B41289.14"
     sigORIG="LP.14.SU45.4" sigPREV="" Type="INK"
     Name="ee" StartTS="18-Aug-2009 10:44:00"
     SigningTS="18-Aug-2009 10:44:00" krTZ="-14400000"
     ID="LP.14.SU45.4"><Ink XSIZE="150" YSIZE="39">
     2.150.39S@59.8@50.11@47.16@45.20@52.22@56.21@60.20@62.18S@50.18@54.17@59.16@66.12@67.17@65.23@68.21@71.16@73.13@73.20@72.24@75.25@78.20@82.14@85.13@85.16@84.21@83.26@83.26</Ink></Sig><Sig CCode="f57ffaf988fec2da27d73e8ebb49c2a3" Type="OTP" StartTS="18-Aug-2009 10:44:00" SigningTS="18-Aug-2009 10:46:01"></Sig><Sig CCode="MC0CFQCBcul+GwI0dRgM/9DF4pDPAf7mNQIUNL7Qn/OFA2hXV7VBtsMLeQmv2V4=" SigningTS="18-Aug-2009 10:46:08" krTZ="-14400000"></Sig></SU>
     *
     */

    /**
     * StudyWorks specific fields.
     */
    private String krpt;
    private String krsu;
    private String krSUR;
    private String krSE;
    private String krSER;
    private String krDOM;

    private String krSig;
    private String krUser;
    private String sigID;
    private String startTS;
    private String krTZ;
    private String signingTS;
    private String Name;
    private String krUC;
    /**
     * Holds StudyWorks specific data.
     */
    private ArrayList<ClinField> sysvars;
    private Hashtable<String,ArrayList<ClinField>> igList;

    /**
     * This is a list of getters and setters for SW clin data.
     */
    public String getKrpt() {
        return krpt;
    }

    public void setKrpt(String krpt) {
        this.krpt = krpt;
    }

    public String getKrsu() {
        return krsu;
    }

    public void setKrsu(String krsu) {
        this.krsu = krsu;
    }

    public String getKrSUR() {
        return krSUR;
    }

    public void setKrSUR(String krSUR) {
        this.krSUR = krSUR;
    }

    public String getKrSE() {
        return krSE;
    }

    public void setKrSE(String krSE) {
        this.krSE = krSE;
    }

    public String getKrSER() {
        return krSER;
    }

    public void setKrSER(String krSER) {
        this.krSER = krSER;
    }

    public String getKrDOM() {
        return krDOM;
    }

    public void setKrDOM(String krDOM) {
        this.krDOM = krDOM;
    }

    public String getKrSig() {
        return krSig;
    }

    public void setKrSig(String krSig) {
        this.krSig = krSig;
    }

    public String getKrUser() {
        return krUser;
    }

    public void setKrUser(String krUser) {
        this.krUser = krUser;
    }

    public String getSigID() {
        return sigID;
    }

    public void setSigID(String sigID) {
        this.sigID = sigID;
    }

    public String getStartTS() {
        return startTS;
    }

    public void setStartTS(String startTS) {
        this.startTS = startTS;
    }

    public String getKrTZ() {
        return krTZ;
    }

    public void setKrTZ(String krTZ) {
        this.krTZ = krTZ;
    }

    public String getSigningTS() {
        return signingTS;
    }

    public void setSigningTS(String signingTS) {
        this.signingTS = signingTS;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getKrUC() {
        return krUC;
    }

    public void setKrUC(String krUC) {
        this.krUC = krUC;
    }

    public ArrayList<ClinField> getSysvars() {
        return sysvars;
    }

    public void setSysvars(ArrayList<ClinField> sysvars) {
        this.sysvars = sysvars;
    }

    public Hashtable<String, ArrayList<ClinField>> getIgList() {
        return igList;
    }

    public void setIgList(Hashtable<String, ArrayList<ClinField>> igList) {
        this.igList = igList;
    }
}
