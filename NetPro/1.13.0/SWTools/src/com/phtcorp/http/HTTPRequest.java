/*
 * Name: HTTPRequest.java
 * Date: 5/17/2010
 */

package com.phtcorp.http;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Jsalesi
 */
public class HTTPRequest {

    /**
     * A reference to the url.
     */
    private URL m_url;

    /**
     * Requests.
     */
    private static final String OPTIONS = "OPTIONS";
    private static final String GET = "GET";
    private static final String HEAD = "HEAD";
    private static final String POST = "POST";
    private static final String PUT = "PUT";
    private static final String DELETE = "DELETE";
    private static final String TRACE = "TRACE";
    private static final String CONNECT = "CONNECT";

    /**
     * A basic http POST method.
     * @param url - is a string containing the url for posting.
     * @param urlParams - any parameters that need to be configured for the url.
     * @param message - is a string containing the actual message to post.
     * @return int containing http status.
     * @throws MalformedURLException
     * @throws IOException
     */
    public int post(String url, String urlParams, String message)
            throws MalformedURLException, IOException {

        HttpURLConnection connection = HttpConnect(url, 10000,
                true, true);
        
        connection.setRequestMethod(POST);

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        writer.write("message=" + message);
        writer.close();

        int status = connection.getResponseCode();
        String connectMsg = connection.getResponseMessage();
        connection.disconnect();

        return status;
    }

    /**
     * This is an Http connection.
     * @param url - is a String containing the url.
     * @param readTimeout - is an integer representing the timeout duration in
     * milliseconds.
     * @param doOutput - is a boolean flag that tells the connection to be configured
     * for output.
     * @param doInput - is a boolean flag that tells the connection to be configured
     * for input.
     * @return HttpURLConnection.
     * @throws MalformedURLException
     * @throws IOException
     */
    public HttpURLConnection HttpConnect(String url, int readTimeout,
            boolean doOutput, boolean doInput)
                throws MalformedURLException, IOException {

        HttpURLConnection connection = null;
        m_url = new URL(url);

        connection = (HttpURLConnection) m_url.openConnection();
        connection.setReadTimeout(readTimeout);
        connection.setDoOutput(doOutput);
        connection.setDoInput(doInput);
        
        return connection;
    }
}
