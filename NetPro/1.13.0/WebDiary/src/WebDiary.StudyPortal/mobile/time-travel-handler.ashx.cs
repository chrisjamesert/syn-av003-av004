﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Web.SessionState;

using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal.mobile
{
    /// <summary>
    /// Summary description for time_travel_handler
    /// </summary>
    public class time_travel_handler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (!context.User.Identity.IsAuthenticated)
            {
                context.Response.StatusCode = 403;
                return;
            }
            if (context.Request.HttpMethod == "POST")
            {
                try
                {
                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        DateTime DTtravel = DateTime.Parse(context.Request.Form["datetime"], CultureInfo.InvariantCulture);
                        SubjectRepository subRep = new SubjectRepository(db);
                        Subject subject = subRep.FindById(new Guid(context.User.Identity.Name));
                        subject.TimeTravelOffset = (int)(TimeSpan.FromTicks((DateTimeHelper.GetUtcTime(DTtravel, subject.StudyId, subject.TimeZone)).Ticks - DateTime.UtcNow.Ticks)).TotalMinutes;
                        db.SubmitChanges();
                        SurveyScheduler.ScheduleEmailAllSurveys(subject, StudyData.study, SurveyScheduler.ScheduleState.TimeTravel);
                        context.Session[DateTimeHelper.TimeTravelOffset] = subject.TimeTravelOffset;
                    }
                    context.Response.Write("passed");
                }
                catch (Exception ex)
                {
                    context.Response.Write("failed");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}