﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;

using WebDiary.Controls;
using WebDiary.Core.Constants;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.StudyPortal.Helpers;

using log4net;
using WebDiary.Core.Repositories;
using WebDiary.StudyPortal.Includes.Usercontrols;
using System.Globalization;

namespace WebDiary.StudyPortal.Mobile
{
    public partial class Index : BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    authBrowser.Value = "false";
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);

                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);

                    authBrowserArea.InnerHtml = browserList;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent));
                    return;
                }

                if (User.Identity.IsAuthenticated)
                    Response.Redirect("~/mobile/home.aspx");

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(Request.QueryString["surveyref"]))
                litSurveyRef.Text = "<input type=\"hidden\" id=\"surveyref\" name=\"surveyref\" value=\"" + Request.QueryString["surveyref"] + "\" />";
        }
        
    }
}