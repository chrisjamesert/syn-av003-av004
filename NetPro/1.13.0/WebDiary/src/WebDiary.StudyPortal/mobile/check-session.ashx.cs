﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace WebDiary.StudyPortal.mobile
{
    /// <summary>
    /// Summary description for check_session
    /// </summary>
    public class check_session : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.User.Identity.IsAuthenticated)
                context.Response.Write('1');
            else
                context.Response.Write('0');
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}