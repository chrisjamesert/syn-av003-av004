﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;

using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using WebDiary.SurveyToolDBAdapters;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;

using log4net;
using System.Collections;
using WebDiary.Core.Errors;
using WebDiary.Membership.User;
using WebDiary.Membership.Provider;

namespace WebDiary.StudyPortal.Mobile
{
    public partial class Home : BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Subject subject;
        WDAPIObject wd;

        protected void Page_Init(object sender, EventArgs e)
        {
            Session["isMobile"] = true;
            HttpCookie timeoutFlag = new HttpCookie("timeout");
            timeoutFlag.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(timeoutFlag);

            //Get values based on study.
            wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

            if (Request.QueryString["surveyref"] != null && string.IsNullOrEmpty(Request.QueryString["surveyref"]) == false)
            {
                try
                {
                    LaunchSurvey(Int32.Parse(Request.QueryString["surveyref"], CultureInfo.InvariantCulture));
                }
                catch (NullReferenceException)
                {
                    Response.Redirect("~/mobile/timeout-redirect.aspx");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //if UST or staging
                if (StudyData.version.ProductEnvironment != WebDiary.Core.Helpers.Version.EnvironmentProduction)
                {
                    DTPanel.Visible = true;
                    litTimeTravel.Text = "<input data-role=\"none\" type=\"datetime-local\" id=\"txtTimeTravel\" name=\"txtTimeTravel\" value=\"" + DateTimeHelper.GetLocalTime().ToString(SWAPIConstants.Html5DateTimeFormat, CultureInfo.InvariantCulture) + "\" />";
                }
                else
                    DTPanel.Visible = false;
            }
            catch (NullReferenceException)
            {
                Response.Redirect("~/mobile/timeout-redirect.aspx");
            }
            if (Session["SubmitResult"] != null)
            {
                if (!string.IsNullOrEmpty(Session["SubmitResult"].ToString()))
                {
                    notificationScriptID.Text = "<script type=\"text/javascript\">notification.text = '" + Session["SubmitResult"].ToString() + "';</script>";
                    Session["SubmitResult"] = null;
                }
            }

            if (Session["homeError"] != null)
            {
                litHomeErrors.Text = Session["homeError"] as string;
                Session["homeError"] = null;
            }

            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                subject = subjectRep.FindById(new Guid(Context.User.Identity.Name));
                int studyId = subject.StudyId;

                var surveyList = db.StudySurveys.Where(s => s.StudyId == studyId && s.FormType == SWAPIConstants.FormTypeSubjectSubmit);
                if (surveyList.Count() == 0)
                    litSurveyInstr.Text = Resources.Resource.NoQuestionnaires;
                else
                    litSurveyInstr.Text = Resources.Resource.SurveyInstr;
            }
        }


        protected void LaunchSurvey(int id)
        {
            Session[WDConstants.FramePostUrl] = null;
            Session[WDConstants.FrameRedirectUrl] = null;
            Session[WDConstants.FrameLeaveUrl] = null;

            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SurveyRepository surveyRep = new SurveyRepository(db);
                    StudySurveys survey = surveyRep.FindById(id);
                    if (subject == null)
                    {
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        subject = subjectRep.FindById(new Guid(Context.User.Identity.Name));
                    }

                    //Get the required values for submitting.
                    Session[WDConstants.VarKrpt] = subject.KrPT;
                    Session[WDConstants.VarStudyId] = subject.StudyId;
                    Session[WDConstants.VarSuTimeZoneValue] = subject.TimeZone;

                    if (!wd.IsSubjectEnabled(subject.KrPT))
                    {
                        Session[WDError.ErrorMessageKey] = Resources.Resource.ErrorSubjectDisabled;
                        Response.Redirect(WDError.Path, true);
                    }

                    SubjectData sd = wd.GetSubjectData(subject.KrPT,
                                                    new ArrayList {SWAPIConstants.ColumnPatientId,
                                                                    SWAPIConstants.ColumnPatientInitials,
                                                                    SWAPIConstants.ColumnPatientKrDom,
                                                                    SWAPIConstants.ColumnPatientKrPhase,
                                                                    SWAPIConstants.ColumnPatientPhaseDate});

                    Session[WDConstants.VarPtPatientId] = sd.data.Patientid;
                    Session[WDConstants.VarPrintedName] = sd.data.Initials;
                    Session[WDConstants.VarKrDom] = sd.data.krdom;

                    //Set current phase info.
                    Session[WDConstants.VarSuPhaseAtEntry] = sd.data.Krphase;
                    //Fix krphasedate if necessary.
                    if (sd.data.Phasedate.Contains('.'))
                    {
                        string[] str = sd.data.Phasedate.Split('.');
                        string strnew = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);

                        Session[WDConstants.VarSuPhaseStartDate] = strnew;
                    }
                    else
                    {
                        Session[WDConstants.VarSuPhaseStartDate] = sd.data.Phasedate;
                    }


                    ScheduleConf config = SurveyScheduler.GetSchedule(survey, subject, StudyData.study);
                    if (!config.Enabled)
                    {
                        Session["homeError"] = "<div class=\"server-error\" id=\"surveyErrMsg\">" + Resources.Resource.SurveyNotAvailable + "</div>";
                        Response.Redirect("~/mobile/home.aspx");
                    }

                    SurveyData surveyData = SurveyDataHelper.CreateSurveyData(survey.Id, survey.TriggeredPhase);
                    surveyData.Sigid = "NP." + WDAPIObject.GenerateSignatureId();
                    NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);

                    if (!survey.IsExternal)
                    {
                        string url = survey.ActivationURL + "&" + queryStrings.ToString();
                        HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                        Response.Redirect("~/survey.aspx?psd_id=" + surveyData.Id, true);
                    }
                    else
                    {
                        Session[WDConstants.IsExternal] = true;
                        ExternalLauncher.LaunchExternalSurvey(Request, Response, survey, surveyData);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                logger.Error("Database error on launching the survey", ex);
                Session[WDError.ErrorMessageKey] = Resources.Resource.IndexPageError;
                Response.Redirect(WDError.Path, true);
            }
        }
    }
}