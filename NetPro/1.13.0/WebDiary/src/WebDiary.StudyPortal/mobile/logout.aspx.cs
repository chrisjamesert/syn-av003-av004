﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using WebDiary.Controls;

namespace WebDiary.StudyPortal.mobile
{
    public partial class logout : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
            Response.Headers.Add("X-Redirect", "index.aspx");
        }
    }
}