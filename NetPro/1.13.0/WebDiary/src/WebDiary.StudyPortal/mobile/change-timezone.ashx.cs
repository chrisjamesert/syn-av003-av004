﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

using log4net;
using System.Globalization;
using WebDiary.Core.Entities;
using System.Configuration;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.Core.Enums;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal.Mobile.Services
{
    /// <summary>
    /// Summary description for change_timezone
    /// </summary>
    public class ChangeTimezone : IHttpHandler, IRequiresSessionState
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(ChangeTimezone));
        public void ProcessRequest(HttpContext context)
        {
            // Set culture
            SubjectHelper.SetCulture(context);

            context.Response.ContentType = "text/plain";
            if (!context.User.Identity.IsAuthenticated)
            {
                context.Response.StatusCode = 403;
                return;
            }
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    //Get the subject
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindByEmailAndStudyId(context.Session["userName"].ToString(), StudyData.study.Id);

                    //Get the timezone
                    TimeZoneRepository tzresp = new TimeZoneRepository(db);
                    TimeZones tz = tzresp.FindByStudyIdAndTimeZoneId(StudyData.study.Id, Int32.Parse(context.Request.Form["timezone-list"], CultureInfo.InvariantCulture));

                    subject.TimeZone = tz.TZId;
                    db.SubmitChanges();

                    context.Session[DateTimeHelper.TimeZoneId] = subject.TimeZone;
                }
                context.Response.Write("passed");
            }
            catch (Exception ex)
            {
                string err = Resources.Resource.ErrUpdatingTZ;
                logger.Error(ex.Message, ex);
                context.Session[WDError.ErrorMessageKey] = err;
                context.Response.Write(Resources.Resource.ErrUpdatingTZ);
            }            
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}