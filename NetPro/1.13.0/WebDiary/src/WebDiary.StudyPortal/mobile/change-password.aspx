﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="change-password.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.ChangePassword" %>

        <asp:Content ID="LeftToolbarContent" ContentPlaceHolderID="LeftToolbarPlaceHolder" runat="server">
            <a href="settings.aspx" data-icon="arrow-l" data-theme="c" data-transition="slide" data-direction="reverse"><%= Resources.Resource.BackBtn %></a>
        </asp:Content>
		<asp:Content ID="RightToolbarContent" ContentPlaceHolderID="RightToolbarPlaceHolder" runat="server">
            <a href="#" data-icon="check" data-theme="c" onclick="$('#changePasswordForm').submit()"><%= Resources.Resource.SaveBtn %></a>
	    </asp:Content>

         <asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
            <div class="center-wrapper">
                <form id="changePasswordForm">
                    <span><%= Resources.Resource.ConfirmOldPswdAndSetPswd%></span><br /><br />
                    <div id="cpfError" class="error"></div>
    
                    <div class="form-row">
                        <label for="txtOldPassword"><%= Resources.Resource.OldPassword%></label>
                        <input type="password" autocomplete="off" maxlength="64" id="txtOldPassword" name="txtOldPassword" class="required" />
                    </div>
                    <div class="form-row">
                        <label for="txtNewPassword"><%= Resources.Resource.ResetNewPswrdLbl%></label>
                        <input type="password" id="txtNewPassword" name="txtNewPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                    </div>
                    <div class="form-row">
                        <label for="txtConfirmNewPassword"><%= Resources.Resource.ConfirmPasswordLbl%></label>
                        <input type="password" id="txtConfirmNewPassword" name="txtConfirmNewPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                    </div>
                    <input type="submit" value="Go" data-role="none" style="visibility:hidden" />
                </form>
            </div>
         </asp:Content>
