﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="change-timezone.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.ChangeTimeZone" %>

        <asp:Content ID="LeftToolbarContent" ContentPlaceHolderID="LeftToolbarPlaceHolder" runat="server">
            <a href="settings.aspx" data-icon="arrow-l" data-theme="c" data-transition="slide" data-direction="reverse"><%= Resources.Resource.BackBtn %></a>
        </asp:Content>
		<asp:Content ID="RightToolbarContent" ContentPlaceHolderID="RightToolbarPlaceHolder" runat="server">
            <a href="#" data-icon="check" data-theme="c" onclick="changeTimeZone()"><%= Resources.Resource.SaveBtn %></a>
	    </asp:Content>

         <asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
            <div class="center-wrapper">
            <form id="timezoneForm" runat="server">
                <label><%= Resources.Resource.currentTZLabel %></label> <asp:Literal ID="litCurrentTimezone" runat="server"></asp:Literal>
                <div id="vsErrors" class="error"></div>
                <fieldset data-role="controlgroup">
                    <legend><%= Resources.Resource.chooseTZ %></legend>
                        <asp:Literal ID="litTimezoneList" runat="server"></asp:Literal>
                </fieldset>
            </form>
            </div>
         </asp:Content>
