﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="settings.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.Settings" %>

        <asp:Content ID="LeftToolbarContent" ContentPlaceHolderID="LeftToolbarPlaceHolder" runat="server">
            <a href="home.aspx" data-icon="home" data-theme="c" data-transition="slide" data-direction="reverse" data-dom-cache="false" data-prefetch><%= Resources.Resource.HomePageLnk %></a>
        </asp:Content>

         <asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">            
            <div class="center-wrapper">
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, emailLbl %>" CssClass="fieldname"></asp:Label> <asp:Label ID="lblEmail" runat="server" CssClass="fieldvalue"></asp:Label><br />
                <asp:Label ID="patientInfoLbl" runat="server" Text="<%$ Resources:Resource, saPatientInfoLabel %>" CssClass="field"></asp:Label> <asp:Label ID="patientIDLbl" runat="server" CssClass="fieldText"></asp:Label>
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider"><%= Resources.Resource.SettingPageHeader %></li>
                    <li><a href="change-password.aspx" data-transition="slide"><%= Resources.Resource.ChangePassLnk %></a></li>
                    <li><a href="change-timezone.aspx" data-transition="slide"><%= Resources.Resource.ChangeTimeZoneLnk %></a></li>
                </ul>
            </div>
         </asp:Content>