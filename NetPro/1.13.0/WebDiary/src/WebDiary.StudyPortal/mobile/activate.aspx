﻿<%@ Page Title="NetPRO" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="activate.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.Activate" %>

         <asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
            <div class="center-wrapper">
                <div class="server-error"><asp:Literal ID="litServerError" runat="server"></asp:Literal></div>
                <input type="hidden" id="authBrowser" value="" runat="server" />
                <div id="unauthMsgArea" class="server-error">
                    <asp:Label ID="lblUnauthorizedMessage" Visible="false" runat="server">
                    </asp:Label>
                </div>
                <div id="authBrowserArea" class="server-error" runat="server">
                </div>      
                <asp:Panel ID="pnlForm" runat="server">
                <form id="activationForm">
                    <span><%= Resources.Resource.confirmUrEmailprovidePswrd%></span><br /><br />
                    <div id="afError" class="error"></div>
                    <div class="form-row">
                        <label for="afEmail"><%= Resources.Resource.emailLbl %></label>
                        <input type="text" autocomplete="off" maxlength="256" id="afEmail" name="afEmail" class="required email" />
                    </div>
                    <div class="form-row">
                        <label for="afPassword"><%= Resources.Resource.PswrdLabel %></label>
                        <input type="password" id="afPassword" name="afPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                    </div>
                    <div class="form-row">
                        <label for="afConfirmPassword"><%= Resources.Resource.ConfirmPasswordLbl%></label>
                        <input type="password" id="afConfirmPassword" name="afConfirmPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                    </div><br />
                    <div class="form-row">
                        <label for="afAffidavit"><%= Resources.Resource.EmailAffidavitCheckbox %></label>
                        <input type="checkbox" id="afAffidavit" name="afAffidavit" />
                    </div>
                    <a href="activation-affidavit.aspx" data-rel="dialog"><%= Resources.Resource.detailLink %></a><br /><br />
                    <div class="form-row">
                        <input type="submit" value="<%= Resources.Resource.EmailAffidavitSubmitBtn %>" />
                    </div>
                    <asp:Literal ID="litToken" runat="server"></asp:Literal>
                </form>
                </asp:Panel>
                <div id="pnlSuccess" style="display:none">
                    <p>
                        <%= Resources.Resource.EmailAcctActivationConf %>
                    </p>
                    <p>
                        <%= String.Format(System.Globalization.CultureInfo.CurrentCulture, Resources.Resource.EmailAcctActivationConfP1, "<a href=\"" + WebDiary.Core.Helpers.StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/mobile/\">", "</a>") %>
                    </p>
                </div>
                <script>
                    $("form#activationForm").validate({
                        onkeyup: false,
                        rules: {
                            afConfirmPassword: {
                                equalTo: '#afPassword'
                            },
                            afAffidavit: {
                                required: true,
                                minlength: 1
                            }
                        },
                        messages: {
                            afEmail: {
                                required: resources.requiredEmail,
                                email: resources.invalidEmail
                            },
                            afPassword: resources.requiredPassword,
                            afConfirmPassword: {
                                required: resources.requiredConfirmPassword,
                                equalTo: resources.passwordMismatch
                            },
                            afAffidavit: resources.requiredAffidavitCheck
                        },
                        submitHandler: function (form) {
                            $.mobile.showPageLoadingMsg();
                            $.ajax({
                                url: "activate.ashx",
                                type: "post",
                                data: $("form#activationForm").serialize(),
                                cache: false
                            }).done(function (data) {
                                if (data.indexOf("passed") >= 0) {
                                    $('div#pnlSuccess').show();
                                    $('div#pnlForm').hide();
                                    $('div#afError').html('');
                                    hideKeyboard();
                                } else {
                                    $('div#afError').html(data);
                                }
                                $.mobile.hidePageLoadingMsg();
                            });
                        },
                        showErrors: function (errorMap, errorList) {
                            $("div#afError").html('');
                            this.defaultShowErrors();
                        }
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var authBrowser = $('#authBrowser').val();
                        if (authBrowser == "false") {
                            $("#activationForm input").prop("disabled", true);
                        }
                    });
                </script>
            </div>
         </asp:Content>