﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Globalization;
using System.Text;

using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using WebDiary.SurveyToolDBAdapters;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;

using log4net;
using System.Collections;
using WebDiary.Core.Errors;
using WebDiary.Membership.User;
using WebDiary.Membership.Provider;

namespace WebDiary.StudyPortal.mobile
{
    /// <summary>
    /// Summary description for questionaires
    /// </summary>
    public class questionaires : IHttpHandler, IRequiresSessionState
    {
        string DATE_TIME_FORMAT = "ddd, dd MMM yyyy, hh:mm tt";

        public void ProcessRequest(HttpContext context)
        {
            // Set culture
            SubjectHelper.SetCulture(context);

            context.Response.ContentType = "text/html";
            if (context.User.Identity.IsAuthenticated)
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindById(new Guid(context.User.Identity.Name));
                    int studyId = subject.StudyId;

                    var surveyList = db.StudySurveys.Where(s => s.StudyId == studyId && s.FormType == SWAPIConstants.FormTypeSubjectSubmit).OrderBy(s => s.SortOrder);
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder();
                    if (surveyList.Count() > 0)
                    {
                        sb.AppendFormat("<ul id=\"questionaireList\" data-role=\"listview\" data-inset=\"true\"><li data-role=\"list-divider\">{0}</li>", Resources.Resource.ToDo);
                        foreach (var survey in surveyList)
                        {
                            ScheduleConf config = SurveyScheduler.GetSchedule(survey, subject, StudyData.study);
                            if (config.Enabled)
                                sb.AppendFormat("<li><a href=\"home.aspx?surveyref={0}\" data-ajax=\"false\">{1}</a></li>", survey.Id, survey.DisplayName);
                            else
                                sb2.AppendFormat("<li><a href=\"home.aspx?surveyref={0}\" data-ajax=\"false\" class=\"ui-disabled\">{1} {2}</a></li>", survey.Id, survey.DisplayName, config.Available.ToString(DATE_TIME_FORMAT, CultureInfo.CurrentCulture) + " - " + config.Due.ToString(DATE_TIME_FORMAT, CultureInfo.CurrentCulture));
                        }

                        sb.Append(sb2.ToString());

                        context.Response.Write(sb.ToString() + "</ul>");
                    }
                }
            }
            else
            {
                context.Response.StatusCode = 403;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}