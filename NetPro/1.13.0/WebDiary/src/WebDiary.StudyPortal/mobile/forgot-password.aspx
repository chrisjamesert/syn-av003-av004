﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="forgot-password.aspx.cs" Inherits="WebDiary.StudyPortal.mobile.forgot_password" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="center-wrapper">
    <input type="hidden" id="authBrowser" value="" runat="server" />
    <div id="unauthMsgArea" class="server-error">
        <asp:Label ID="lblUnauthorizedMessage" Visible="false" runat="server">
        </asp:Label>
    </div>
    <div id="authBrowserArea" class="server-error" runat="server">
    </div>      
        <div id="pnlForm">
            <p>
                <%= Resources.Resource.ForgotPswrdMsgBody %>
            </p>
            <form id="forgotPasswordForm" runat="server">
                <div id="fpError" class="error"></div>

                <div class="form-row">
                    <label for="fpfEmail"><%= Resources.Resource.emailLbl %></label>
                    <input type="text" autocomplete="off" maxlength="256" id="fpfEmail" name="fpfEmail" class="required email" />
                </div>

                <br />
                <input type="submit" value="<%= Resources.Resource.Submit %>" />
            </form>
        </div>
        <div id="pnlSuccess" style="display:none">
            <p>
                <%= Resources.Resource.ForgotPswrdConfirMsgBody%>
            </p>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var authBrowser = $('#authBrowser').val();
                if (authBrowser == "false") {
                    $("#forgotPasswordForm input").prop("disabled", true);
                }
            });
        </script>
    </div>
</asp:Content>
