﻿using System;
using System.Web;
using System.Web.SessionState;

using System.Configuration;

using System.Net.Mail;
using log4net;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Email;
using WebDiary.Core.Constants;
using WebDiary.WDAPI;
using System.Globalization;
using WebDiary.StudyPortal.Helpers;

namespace WebDiary.StudyPortal.Mobile.Services
{
    /// <summary>
    /// Summary description for forgot_password1
    /// </summary>
    public class ForgotPassword : IHttpHandler, IRequiresSessionState
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(ForgotPassword));
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            try
            {
                //Language code from session or cookie
                string languageCode = SubjectHelper.GetLanguageCode(context); 

                CultureInfo culture = new CultureInfo(languageCode);
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                
                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(context.Request.Form["fpfEmail"].Trim(), false);

                    //Check if user exists and if so, check to see if they're assigned to this specific study.
                    if (user == null || StudyData.study == null || user.StudyId != StudyData.study.Id)
                    {
                        context.Response.Write(Resources.Resource.ForgotPasswordNoEmail);
                        return;
                    }

                    //Language code for the user who requeseted forgotpassword
                    languageCode = WDAPIObject.JavaLocaleToDotNetCulture(user.Language);
                    CultureInfo userCulture = new CultureInfo(languageCode);
                    System.Threading.Thread.CurrentThread.CurrentCulture = userCulture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = userCulture;
                    context.Session[WDConstants.Language] = context.Response.Cookies[WDConstants.Language].Value = languageCode;

                    if (user.IsApproved)
                    {
                        WDAPIObject wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                        SubjectStudyEnabled result = wd.IsSubjectAndStudyEnabled(user.KrPT, StudyData.study);
                        if (result == SubjectStudyEnabled.Enabled)
                        {
                            SubjectRepository subjectRep = new SubjectRepository(db);
                            Subject sub = subjectRep.FindByKrptAndStudy(user.KrPT, user.StudyId);
                            context.Session[DateTimeHelper.TimeTravelOffset] = sub.TimeTravelOffset;
                            context.Session[DateTimeHelper.TimeZoneId] = sub.TimeZone;

                            Guid token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);

                            if (token != Guid.Empty)
                            {
                                String resetUrl = String.Format(CultureInfo.InvariantCulture, "{0}/reset-password.aspx?token={1}",
                                                                StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/'),
                                                                token);

                                string body = EmailHelper.GetForgotPasswordEmail(resetUrl, user.Email);
                                Message message = new Message(ConfigurationManager.AppSettings["FROM_ADDRESS"], user.Email, Resources.Resource.EmailAcctForgotPswrdSubject.ToString(), body.ToString());

                                try
                                {
                                    message.Send();
                                    logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, subject krpt: {1}, StudyWorks timezone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging date time in UTC: {6}. Email was sent to the email server successfully.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, StudyData.study.Name, StudyData.study.Id, "reset password request due to forgot password", DateTime.UtcNow));
                                    context.Response.Write("passed");
                                }
                                catch (SmtpException ex)
                                {
                                    logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, StudyWorks timezone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging date time in UTC: {6}, error message: {7}. Sending email failed in StudyPortal.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, StudyData.study.Name, StudyData.study.Id, "reset password request due to forgot password", DateTime.UtcNow, ex.Message), ex);
                                    context.Response.Write(Resources.Resource.ForgotPasswordSendingEmailFailed);
                                    return;
                                }
                            }
                            else
                            {
                                context.Response.Write(Resources.Resource.ErrorProcessingUrRequest);
                            }
                        }
                        else if (result == SubjectStudyEnabled.Error)
                        {
                            context.Response.Write(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>"));
                        }
                        else
                        {
                            context.Response.Write(Resources.Resource.ForgotPswrdAccInactive);
                        }
                    }
                    else
                    {
                        context.Response.Write(Resources.Resource.AcctRetrievePswdforNotActivated);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                context.Response.Write(Resources.Resource.ErrUnexpectedErr);
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}