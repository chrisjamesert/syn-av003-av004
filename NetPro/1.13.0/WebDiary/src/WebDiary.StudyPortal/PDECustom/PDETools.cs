﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.SWAPI.SWData;
using System.Collections;
using WebDiary.SWAPI;
using System.Globalization;
using System.Xml.Linq;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal
{
    public class PDETools : PDECommonTools
    {

        //Diary KRSU's
        public const String KRSU_ASSIGN = "Assignment";
        public const String KRSU_Deactivate = "EndWebProUse";

        //Constants
        public const String EMPTY_STRING = " ";
        public const String STD_YES = "1";
        public const String STD_NO = "0";
        public const String STD_NO_VALID_DATA = "-1";

        public const String DEFAULT_PROTOCOL = "1";

        //Phases
        public const String PHASE_PRESCREENING = "100";
        public const String PHASE_SCREENING = "200";
        public const String PHASE_TREATMENT = "300";
        public const String PHASE_FOLLOWUP = "700";
        public const String PHASE_DEACTIVATION = "999";

        //Languages
        public const String LANG_ENGLISH = "en_US";
        public const String SW_DATE_FORMAT = "dd MMM yyyy";
    }
}