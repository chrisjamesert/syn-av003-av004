﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using WebDiary.Controls;
using WebDiary.Core.Constants;

namespace WebDiary.StudyPortal
{
    public partial class LogOn : BasePage
    {
        
        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                Session.Clear();

                LogOnControl.EnableSessionTimeoutMsg(Request.Cookies[WDConstants.UserStatusCookie] != null &&
                    !String.IsNullOrEmpty(Request.Cookies[WDConstants.UserStatusCookie].Value));
                
            }

            base.OnLoad(e);
        }
    }
}
