﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using WebDiary.Core.Email;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Entities;
using WebDiary.Core.Constants;
using System.Configuration;
using log4net;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using System.Globalization;
using WebDiary.Membership.Provider;

namespace WebDiary.StudyPortal.Helpers
{
    public class EmailHelper
    {
        public static String generateSupportCenterLink()
        {
            return String.Format(CultureInfo.CurrentCulture, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">{0}</a>", Resources.Resource.SupportCenterLink);
        }

        public static string GetChangeEmail(Study study, Subject sub, WDConstants.EmailType type)
        {
            WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);

            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.hello)); //Hello,
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.EmailAcctChange));
            if (type == WDConstants.EmailType.LanguageUpdate)
                sb.Append(string.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", string.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailLangChange, wd.GetLanguageNameFromCode(sub.Language.Trim()))));
            else if (type == WDConstants.EmailType.EmailUpdate)
                sb.Append(string.Format(CultureInfo.CurrentCulture, "<p>{0}</p><p>{1}</p>", string.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailEmailChange, sub.Email), Resources.Resource.EmailEmailChangeP1));
            else if (type == WDConstants.EmailType.EmailLanguageUpdate)
                sb.Append(string.Format(CultureInfo.CurrentCulture, "<p>{0}</p><p>{1}</p><p>{2}</p>", string.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailLangChange, wd.GetLanguageNameFromCode(sub.Language.Trim())), string.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailEmailChange, sub.Email), Resources.Resource.EmailEmailChangeP1));

            sb.Append(string.Format(CultureInfo.CurrentCulture, "<p>{0} {1}</p>", Resources.Resource.EmailAcctChangeP2, generateSupportCenterLink()));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.thanku)); //Thank you

            return sb.ToString();
        }
        
        public static string GetActivationEmail(Study study, Subject sub)
        {
            String activationUrl = String.Format(CultureInfo.InvariantCulture, "{0}/activate.aspx?token={1}", study.WebDiaryStudyBaseUrl.TrimEnd('/'), sub.ActivationToken);
            StringBuilder sb = new StringBuilder();
            sb.Append(Resources.Resource.EmailAcctActivation);
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailAcctActivationP1, study.Name)));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p><a href='{0}'>{0}</a></p>", activationUrl));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailAcctActivationP2, study.TokenExpiration)));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0} {1}</p>", Resources.Resource.contactInstr, generateSupportCenterLink()));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.thanku));

            return sb.ToString();
        }

        public static string GetResetPasswordEmail(Study study, Subject sub)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.hello)); //Hello,
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailAcctForgotResetP1, study.Name))); //This is to confirm that your {0} password has been successfully reset.            
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.ToLogin)); //To login to your account, go to the following website
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p><a href='{0}'>{0}</a></p>", study.WebDiaryStudyBaseUrl));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.UseLink)); //Please use this link to access your account in the future.
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0} {1}</p>", Resources.Resource.contactInstr, generateSupportCenterLink()));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.thanku)); //Thank you

            return sb.ToString();
    }

        public static string GetConfirmationEmail(Study study, Subject sub)
        {
            String loginUrl = study.WebDiaryStudyBaseUrl;
            StringBuilder body = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.hello)); //Hello,
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailAcctActivConfirmationP1, study.Name))); //This message is to confirm that your study account has been activated.            
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.ToLogin)); //To login to your account, go to the following website
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p><a href='{0}'>{0}</a></p>", loginUrl));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.UseLink)); //Please use this link to access your account in the future.
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0} {1}</p>", Resources.Resource.contactInstr, generateSupportCenterLink()));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.thanku)); //Thank you

            body.Append(sb.ToString());

            return body.ToString();
        }

        public static string GetScheduledEmail(Study study, StudySurveys survey)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.hello)); //Hello,
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailScheduleP1, survey.DisplayName, study.Name))); //Please note that your {0} in {1} is ready for completion..            
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.EmailScheduleP2)); //To access this survey, please click the following link.
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p><a href='{0}'>{1}</a></p>", study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/index.aspx?surveyref=" + survey.Id, survey.DisplayName));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0} {1}</p>", Resources.Resource.contactInstr, generateSupportCenterLink()));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.thanku)); //Thank you

            return sb.ToString();
        }

        public static string GetForgotPasswordEmail(string resetUrl, string email)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.hello)); //Hello,
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailAcctForgotPswrdP1, WebDiaryProvider.GetStudyNameByEmail(email)))); //Please click the following link to reset your {0} password.
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p><a href='{0}'>{0}</a></p>", resetUrl));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.EmailAcctForgotPswrdP2)); //Please note that this link will expire after 24 hours.
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.EmailAcctForgotPswrdP3)); //If you did not select ‘forgot password’ on the login page, or if you believe that you received this message in error, please contact us immediately.
            String supportcenterlink = String.Format(CultureInfo.CurrentCulture, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">{0}</a>", Resources.Resource.SupportCenterLink);
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0} {1}</p>", Resources.Resource.contactInstr, supportcenterlink));
            sb.Append(String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", Resources.Resource.thanku)); //Thank you


            return sb.ToString();
        }

    }
}