﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using System.Threading;
using log4net;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Api.Helpers;
using Newtonsoft.Json.Linq;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class ChallengeQuestionsController : ApiController
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(ChallengeQuestionsController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        [Authorize]
        public HttpResponseMessage GET(string id)
        {
            ChallengeQuestionsResult result = new ChallengeQuestionsResult();

            try
            {
                //Check krpt
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByKrptAndStudy(id, StudyData.study.Id);

                if (subject == null)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject for krpt in incoming url does not exist. Krpt in incoming url: {0}", id));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                    throw responseException;
                }

                List<ChallengeQuestionResult> challengeQuestions = new List<ChallengeQuestionResult>();

                if (subject.SecurityQuestion.HasValue || !string.IsNullOrEmpty(subject.SecurityAnswer))
                {
                    challengeQuestions.Add(new ChallengeQuestionResult { Question = subject.SecurityQuestion, Answer = subject.SecurityAnswer });
                    result.ChallengeQuestions = challengeQuestions;
                }
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }

            return Request.CreateResponse<ChallengeQuestionsResult>(HttpStatusCode.OK, result);
       }

        [Authorize]
        public HttpResponseMessage PUT(string id, [FromBody] ChallengeQuestionsModel challengeQuestions)
        {
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;

            try
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByKrptAndStudy(id, StudyData.study.Id);

                if (subject == null)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject for krpt in incoming url does not exist. Krpt in incoming url: {0}", id));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                    throw responseException;
                }

                if (!principal.ApiTokenAuth && principal.UserID != subject.Pin)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Unauthorized);
                    throw responseException;
                }

                //Can not update challenge questions if subject account is not activated yet
                if (string.IsNullOrEmpty(subject.Password))
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject account is not activated yet. Krpt: {0}", subject.KrPT));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserIsNotActive);
                    throw responseException;
                }

                //Prevent updating challenge questions from deactivated LPA device
                if (!principal.ApiTokenAuth)
                {
                    SubjectDeviceRepository deviceRepo = new SubjectDeviceRepository(db);
                    SubjectDevice currentDevice = deviceRepo.FindLatestBySubjectId(principal.SubjectID);
                    if (!string.Equals(currentDevice.DeviceCode.ToString(), principal.DeviceID.ToString(), StringComparison.OrdinalIgnoreCase)) // not active device
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "LogPad App Device for ClientID - DeviceID is not active device. Krpt: {0}, ClientID - DeviceID: {1}", subject.KrPT, principal.DeviceID.ToString()));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.NotActiveClientID);
                        throw responseException;
                    }
                }

                StudyMiddleTier studyMiddleTier = db.StudyMiddleTiers.Where(s => s.StudyId == StudyData.study.Id).SingleOrDefault();

                if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion)
                {
                    ChallengeQuestionModel cq = challengeQuestions.ChallengeQuestions.SingleOrDefault();

                    //Check empty or null for security question and answer
                    if (!cq.Question.HasValue || string.IsNullOrEmpty(cq.Answer) || cq.Answer.Trim().Length == 0)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }

                    subject.SecurityQuestion = cq.Question.Value;
                    subject.SecurityAnswer = cq.Answer;
                    subject.LastSecurityQuestionAnswerChanged = DateTime.UtcNow;
                    db.SubmitChanges();

                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else //AuthenticationLevel is basic
                    throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}