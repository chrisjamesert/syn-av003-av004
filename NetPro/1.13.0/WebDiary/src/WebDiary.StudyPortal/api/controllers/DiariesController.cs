﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;

using log4net;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Helpers;
using WebDiary.Core.Errors;
using WebDiary.SWIS;
using WebDiary.SWAPI;
using WebDiary.WDAPI;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class DiariesController : ApiController
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(DiariesController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        // POST api/<controller>
        [Authorize]
        public HttpResponseMessage Post([FromBody] DiaryModel diary)
        {
            // Retrieve the principal
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
            bool isDuplicate = false;
            bool sigIdGeneratedByNP = false;
            string message = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(diary.SigID))
                {
                    if (principal.ApiTokenAuth && !diary.SigID.StartsWith(SWAPIConstants.SigidPrefixForSitePadApp, false, CultureInfo.InvariantCulture))
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "Sigid from SitePad App must start with \"" + SWAPIConstants.SigidPrefixForSitePadApp + "\".  Sigid: {0} for krdom: {1}, krsu: {2}", diary.SigID, principal.KrDom, diary.SU));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                        throw responseException;
                    }
                    else if (!principal.ApiTokenAuth && !diary.SigID.StartsWith(SWAPIConstants.SigidPrefixForLogPadApp, false, CultureInfo.InvariantCulture))
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "Sigid from LogPad App must start with \"" + SWAPIConstants.SigidPrefixForLogPadApp + "\". Sigid: {0} for subjectId: {1}, krsu: {2}", diary.SigID, principal.SubjectID, diary.SU));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                        throw responseException;
                    }
                }

                // Add diary to Diary Queue
                DiaryQueueItem diaryItem = new DiaryQueueItem();
                if (principal.ApiTokenAuth)
                {
                    if (string.IsNullOrEmpty(diary.KrPT))
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.NoKrPTProvided);
                        throw responseException;
                    }

                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindByKrptAndStudy(diary.KrPT, StudyData.study.Id);
                    if (subject == null)
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                        throw responseException;
                    }
                    else if (!string.Equals(subject.KrPT, diary.KrPT, StringComparison.Ordinal)) //Check case sensitive
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                        throw responseException;
                    }

                    if (string.IsNullOrEmpty(diary.ResponsibleParty))
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.NoResponsiblePartyProvided);
                        throw responseException;
                    }

                    if (diary.Ink != null)
                    {
                        if (!string.Equals(diary.ResponsibleParty, diary.Ink.Author, StringComparison.Ordinal))
                        {
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                            responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ResponsiblePartyNotMatchInkAuthor);
                            throw responseException;
                        }
                    }

                    diaryItem.KrPT = diary.KrPT;
                    diaryItem.ResponsibleParty = diary.ResponsibleParty;
                    diaryItem.SubjectId = subject.Id;
                    diaryItem.SitePadApp = true;
                    if(principal.SWDeviceId.HasValue)
                        diaryItem.SWDeviceId = principal.SWDeviceId.Value; 
                }
                else
                {
                    if (!string.IsNullOrEmpty(diary.KrPT))
                    {
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        Subject subject = subjectRep.FindById(principal.SubjectID);
                        if (subject == null)
                        {
                            // This would not happen because the subject is already checked on authorization, but check.
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Unauthorized);
                            throw responseException;
                        }
                        else if (!String.Equals(diary.KrPT, subject.KrPT, StringComparison.Ordinal))
                        {
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                            responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                            throw responseException;
                        }
                        diaryItem.KrPT = diary.KrPT;
                    }

                    if (!string.IsNullOrEmpty(diary.ResponsibleParty))
                    {
                        if (diary.Ink != null)
                        {
                            if (!string.Equals(diary.ResponsibleParty, diary.Ink.Author, StringComparison.Ordinal))
                            {
                                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ResponsiblePartyNotMatchInkAuthor);
                                throw responseException;
                            }
                        }
                        diaryItem.ResponsibleParty = diary.ResponsibleParty;
                    }
                    diaryItem.SubjectId = principal.SubjectID;
                    diaryItem.SitePadApp = false;
                }

                diaryItem.QueueDate = DateTime.Now;
                diaryItem.ActivityDate = DateTime.Now;                 
                diaryItem.QueueState = QueueState.New;
                diaryItem.BrowserPlatform = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Platform;
                diaryItem.BrowserType = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Type;
                diaryItem.BrowserVersion = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Version;                
                diaryItem.SU = diary.SU;
                diaryItem.Started = diary.Started;
                diaryItem.Completed = diary.Completed;
                diaryItem.ReportDate = diary.ReportDate;
                diaryItem.CompletedTZOffset = diary.CompletedTZOffset;
                diaryItem.CompletedEpoch = diary.DiaryID;
                diaryItem.Phase = diary.Phase;
                diaryItem.ChangePhase = diary.ChangePhase;
                diaryItem.DeviceId = principal.DeviceID.ToString();
                diaryItem.ClientCoreVersion = diary.CoreVersion;
                diaryItem.ClientStudyVersion = diary.StudyVersion;
                diaryItem.PhaseStartDate = diary.PhaseStartDate;
                diaryItem.DateTimeReceived = principal.ServerReceiptTime;
                diaryItem.DateTimeSubmitted = principal.ClientRequestTime.UtcDateTime;
                diaryItem.BatteryLevel = diary.BatteryLevel;
                if (string.IsNullOrEmpty(diary.SigID))
                {
                    sigIdGeneratedByNP = true;
                    diaryItem.Sigid = "NP." + WDAPIObject.GenerateSignatureId();
                }
                else
                    diaryItem.Sigid = diary.SigID.Trim();

                db.DiaryQueue.InsertOnSubmit(diaryItem);
                db.SubmitChanges();

                // Add answers to Diary Queue Answers
                foreach (DiaryAnswerModel answer in diary.Answers)
                {
                    DiaryQueueAnswer answerItem = new DiaryQueueAnswer();
                    answerItem.DiaryQueueId = diaryItem.DiaryQueueId;
                    answerItem.DataChanged = false;
                    answerItem.Response = answer.Response;
                    answerItem.SWAlias = answer.SWAlias;
                    answerItem.QuestionId = answer.QuestionID;
                    db.DiaryQueueAnswers.InsertOnSubmit(answerItem);
                }

                //ink signature is optional, so check Ink is null or not null
                if (diary.Ink != null)
                {
                    InkSignature inkSignature = new InkSignature();
                    inkSignature.DiaryQueueId = diaryItem.DiaryQueueId;
                    inkSignature.Author = diary.Ink.Author;
                    inkSignature.Xsize = diary.Ink.Xsize;
                    inkSignature.Ysize = diary.Ink.Ysize;
                    inkSignature.Vector = diary.Ink.Vector;
                    db.InkSignature.InsertOnSubmit(inkSignature);
                }

                db.SubmitChanges();

                try
                {
                    if (sigIdGeneratedByNP)
                    {
                        var checkDiaryHistoryNoSigid = (from d in db.DiaryHistoryNoSigid
                                                        where d.SubjectId == diaryItem.SubjectId && d.DiaryId == diaryItem.CompletedEpoch
                                                        select d).SingleOrDefault<DiaryHistoryNoSigid>();

                        if (checkDiaryHistoryNoSigid != null)
                        {
                            isDuplicate = true;
                            diaryItem.QueueState = QueueState.Duplicate;
                            message = String.Format(CultureInfo.InvariantCulture, "DiaryId must be unique. Duplicate diaryId: {0} is used for krpt: {1}, krsu: {2}", diary.DiaryID, diaryItem.Subject.KrPT, diaryItem.SU);
                            StuckReportError stuckReport = new StuckReportError();
                            stuckReport.DiaryQueueId = diaryItem.DiaryQueueId;
                            stuckReport.QueueState = QueueState.Duplicate;
                            stuckReport.Message = message;
                            db.StuckReportError.InsertOnSubmit(stuckReport);
                            db.SubmitChanges();
                            logger.Info(message);
                        }
                        else
                        {
                            DiaryHistoryNoSigid diaryHistoryNoSigid = new DiaryHistoryNoSigid();
                            diaryHistoryNoSigid.SubjectId = diaryItem.SubjectId;
                            diaryHistoryNoSigid.DiaryId = diaryItem.CompletedEpoch;
                            diaryHistoryNoSigid.Submitted = false;
                            db.DiaryHistoryNoSigid.InsertOnSubmit(diaryHistoryNoSigid);
                            db.SubmitChanges();
                        }
                    }
                    else
                    {
                        var checkDiaryHistory = (from d in db.DiaryHistory
                                                 where d.Sigid == diaryItem.Sigid
                                                 select d).SingleOrDefault<DiaryHistory>();

                        if (checkDiaryHistory != null)
                        {
                            isDuplicate = true;
                            diaryItem.QueueState = QueueState.Duplicate;
                            message = String.Format(CultureInfo.InvariantCulture, "Sigid must be unique. Duplicate sigId: {0} is used for krpt: {1}, krsu: {2}", diaryItem.Sigid, diaryItem.Subject.KrPT, diaryItem.SU);
                            StuckReportError stuckReport = new StuckReportError();
                            stuckReport.DiaryQueueId = diaryItem.DiaryQueueId;
                            stuckReport.QueueState = QueueState.Duplicate;
                            stuckReport.Message = message;
                            db.StuckReportError.InsertOnSubmit(stuckReport);
                            db.SubmitChanges();
                            logger.Info(message);
                        }
                        else
                        {
                            DiaryHistory diaryHistory = new DiaryHistory();
                            diaryHistory.Sigid = diaryItem.Sigid;
                            diaryHistory.SubjectId = diaryItem.SubjectId;
                            diaryHistory.Submitted = false;
                            db.DiaryHistory.InsertOnSubmit(diaryHistory);
                            db.SubmitChanges();
                        }
                    }
                }
                catch (SqlException sqlEx)
                {
                    if (sqlEx.Number == 2627) //Catch primary or unique key violation. This could happen in high load even if a duplicate is checked above.
                    {
                        isDuplicate = true;
                        if (sigIdGeneratedByNP)
                        {
                            message = String.Format(CultureInfo.InvariantCulture, "DiaryId must be unique. Duplicate diaryId: {0} is used for krpt: {1}, krsu: {2}", diary.DiaryID, diaryItem.Subject.KrPT, diaryItem.SU);
                            logger.Info(message);
                        }
                        else
                        {
                            message = String.Format(CultureInfo.InvariantCulture, "Sigid must be unique. Duplicate sigId: {0} is used for krpt: {1}, krsu: {2}", diaryItem.Sigid, diaryItem.Subject.KrPT, diaryItem.SU); 
                            logger.Info(message);
                        }
                        using (WebDiaryContext db2 = new WebDiaryContext(WebDiaryContext.ConnectionString))
                        {
                            DiaryQueueRepository diaryRepo = new DiaryQueueRepository(db2);
                            DiaryQueueItem diaryItem2 = diaryRepo.FindByDiaryQueueItemId(diaryItem.DiaryQueueId);
                            if (diaryItem2 != null)
                            {
                                diaryItem2.QueueState = QueueState.Duplicate;

                                StuckReportError stuckReport = new StuckReportError();
                                stuckReport.DiaryQueueId = diaryItem.DiaryQueueId;
                                stuckReport.QueueState = QueueState.Duplicate;
                                stuckReport.Message = message;
                                db2.StuckReportError.InsertOnSubmit(stuckReport);
                                db2.SubmitChanges();
                            }
                        }
                    }
                    else
                        throw;
                }

                if (!isDuplicate && diaryItem.QueueState == QueueState.New)
                {
                    DiaryService diaryService = new DiaryService();
                    var task = Task.Factory.StartNew(() => diaryService.DiarySubmit(diaryItem.DiaryQueueId), TaskCreationOptions.LongRunning);
                }                
            }
            catch (SqlException sqlEx)
            {
                logger.Error(sqlEx.Message, sqlEx);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                throw responseException;
            }

            HttpResponseMessage responseMessage = new HttpResponseMessage(HttpStatusCode.Created);
            if (isDuplicate)
                responseMessage.Headers.Add("X-Diary-Status", SWISDiaryStatusCode.Duplicate);
            else
                responseMessage.Headers.Add("X-Diary-Status", SWISDiaryStatusCode.NotDuplicate);
            return responseMessage;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}
