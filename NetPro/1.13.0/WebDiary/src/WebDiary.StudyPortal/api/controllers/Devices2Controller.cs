﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Data.SqlClient;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using System.Threading;
using log4net;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using Newtonsoft.Json.Linq;
using WebDiary.StudyPortal.Api.Helpers;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class Devices2Controller : ApiController
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(Devices2Controller));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        public HttpResponseMessage Post([FromBody] Device2Model device)
        {
            if (!string.IsNullOrEmpty(device.StudyName) && String.Equals(device.StudyName, StudyData.study.Name, StringComparison.OrdinalIgnoreCase))
            {
                RegistrationResult registrationResult = new RegistrationResult();

                try
                {
                    ArrayList parameter = new ArrayList();
                    parameter.Add(device.KrDom);

                    // Perform a SWAPI call
                    WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                    string swResponse = wdapi.GetData(SWAPIConstants.SPSyndGetValidSiteCode, parameter).response;

                    if (string.IsNullOrEmpty(swResponse))
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InValidSiteId);
                        throw responseException;
                    }

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(swResponse);
                    string siteCode = doc.SelectSingleNode("sites/site/siteCode").InnerText;

                    string calculatedStartUpCode = UnlockCode.GenerateStartupCode(StudyData.study.Name, siteCode.TrimEnd());
                    if (!String.Equals(calculatedStartUpCode, device.StartupCode, StringComparison.Ordinal))
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidStartupCode);
                        throw responseException;
                    }

                    // Invalidate any active api token in DEVICES table
                    DeviceRepository deviceRep = new DeviceRepository(db);
                    var activeDevices = deviceRep.FindByDeviceUuidAndStatus(device.DeviceUuid, true).ToList();
                    if (activeDevices != null)
                    {
                        activeDevices.ForEach(a => a.Active = false);
                        db.SubmitChanges();
                    }

                    Device deviceInformation = new Device();
                    deviceInformation.DeviceCode = Guid.NewGuid();

                    // Register device with StudyWorks if imei is specified
				    int? swDeviceId = null;
                    if (!string.IsNullOrEmpty(device.IMEI))
                    {
                        if (string.IsNullOrEmpty(device.Model))
                            device.Model = SWAPIConstants.DeviceUnknownModel;

                        swDeviceId = RegisterDevice.Register(new DeviceData
                        {
                            deviceUsername = deviceInformation.DeviceCode.ToString(),
                            devicePassword = SWAPIConstants.DevicePassword,
                            siteCode = siteCode, 
                            adminDom = SWAPIConstants.AdminDom,
                            deviceType = SWAPIConstants.DeviceTypeSitePadApp,
                            deviceTypeId = SWAPIConstants.DeviceTypeIdSitePadApp,
                            deviceSerialNumber = device.IMEI,
                            model = device.Model
                        }
                        );
                    }

                    string apiToken = Token.GenerateToken();
                    deviceInformation.StudyId = StudyData.study.Id;
                    deviceInformation.SiteCodeAtRegistration = siteCode;
                    deviceInformation.DeviceUuid = device.DeviceUuid;
                    deviceInformation.AssetTag = device.AssetTag;
                    deviceInformation.ApiToken = apiToken;
                    deviceInformation.KrDom = device.KrDom;
                    deviceInformation.Active = true;
                    deviceInformation.SWDeviceId = swDeviceId;
                    deviceInformation.RegistrationDate = DateTime.UtcNow;

                    db.Devices.InsertOnSubmit(deviceInformation);
                    db.SubmitChanges();

                    registrationResult.APIToken = apiToken;
                    if (swDeviceId.HasValue)
                        registrationResult.RegDeviceId = swDeviceId.Value;
                    registrationResult.ClientId = deviceInformation.DeviceCode;
                }
                catch (HttpResponseException httpEx)
                {
                    throw httpEx;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                    if (ex is SqlException || ex is WebException)
                    {
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                    }
                    throw responseException;
                }

                return Request.CreateResponse(HttpStatusCode.Created, registrationResult);
            }
            else
            {
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidStudyName);
                throw responseException;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}