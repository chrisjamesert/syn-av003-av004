﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

namespace WebDiary.StudyPortal.Api
{
    public class WebApiPrincipal : IPrincipal
    {
        public Guid SubjectID { get; set; }
        public long UserID { get; set; }
        public Guid DeviceID { get; set; }
        public DateTimeOffset ClientRequestTime { get; set; }
        public DateTime ServerReceiptTime { get; set; }
        public string KrDom { get; set; }
        public bool ApiTokenAuth { get; set; }
        public int? SWDeviceId { get; set; }
        public int StudyId { get; set; } 

        public bool IsInRole(string role)
        {
            return true;
        }

        public IIdentity Identity
        {
            get
            {
                return new GenericIdentity(this.UserID.ToString());
            }
            set
            {
                this.UserID = long.Parse(value.Name);
            }
        }
    }
}