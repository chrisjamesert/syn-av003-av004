﻿using System.Web;
using Newtonsoft.Json;

namespace WebDiary.StudyPortal.Api.Models
{
    public class DeviceData
    {
        public string deviceUsername { get; set; }

        public string devicePassword { get; set; }

        public string siteCode { get; set; }

        public string adminDom { get; set; }

        public string deviceSerialNumber { get; set; }

        public string deviceType { get; set; }

        public string deviceTypeId { get; set; }

        public string model { get; set; }
    }
}