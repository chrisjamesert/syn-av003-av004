﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class DiaryModel
    {
        [DataMember(IsRequired = true, Name = "I")]
        public long DiaryID { get; set; }
       
        [DataMember(IsRequired = true, Name = "U")]
        public string SU { get; set; }

        [DataMember(IsRequired = true, Name = "S")]
        public DateTime Started { get; set; }

        [DataMember(IsRequired = true, Name = "C")]
        public DateTime Completed { get; set; }

        [DataMember(Name = "R")]
        public DateTime ReportDate { get; set; }

        [DataMember(IsRequired = true, Name = "O")]
        public decimal CompletedTZOffset { get; set; }

        [DataMember(IsRequired = true, Name = "P")]
        public string Phase { get; set; }

        [DataMember(IsRequired = true, Name = "B")]
        public bool ChangePhase { get; set; }

        [DataMember(IsRequired = true, Name = "E")]
        public string CoreVersion { get; set; }

        [DataMember(IsRequired = true, Name = "V")]
        public string StudyVersion { get; set; }

        [DataMember(IsRequired = true, Name = "T")]
        public DateTimeOffset PhaseStartDate { get; set; }

        [DataMember(Name = "L")]
        public string BatteryLevel { get; set; }

        [DataMember(Name = "K")]
        public string KrPT { get; set; }

        [DataMember(Name = "M")]
        public string ResponsibleParty { get; set; }

        [DataMember(Name = "J")]
        public string SigID { get; set; }

        [DataMember(IsRequired = true, Name = "A")]
        public IEnumerable<DiaryAnswerModel> Answers { get; set; }

        [DataMember(Name = "N")]
        public InkSignatureModel Ink { get; set; }

    }

    [DataContract]
    public class DiaryAnswerModel
    {
        [DataMember(IsRequired = true, Name = "G")]
        public string Response { get; set; }

        [DataMember(IsRequired = true, Name = "F")]
        public string SWAlias { get; set; }

        [DataMember(IsRequired = true, Name = "Q")]
        public string QuestionID { get; set; }
    }

    [DataContract]
    public class InkSignatureModel
    {

        [DataMember(IsRequired = true, Name = "H")]
        public string Author { get; set; }

        [DataMember(IsRequired = true, Name = "X")]
        public int Xsize { get; set; }

        [DataMember(IsRequired = true, Name = "Y")]
        public int Ysize { get; set; }

        [DataMember(IsRequired = true, Name = "D")]
        public string Vector { get; set; }
    }
}
