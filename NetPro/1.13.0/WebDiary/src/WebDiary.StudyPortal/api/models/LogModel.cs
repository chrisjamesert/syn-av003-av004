﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class LogModel
    {
        [DataMember(IsRequired=true)]
        [ObjectLength(8000)]
        public object Json { get; set; }
    }
}