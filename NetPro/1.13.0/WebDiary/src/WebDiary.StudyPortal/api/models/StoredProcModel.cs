﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class StoredProcModel
    {
        [DataMember(IsRequired=true)]
        public string StoredProc { get; set; }

        [DataMember]
        public ArrayList Params { get; set; }
    }
}