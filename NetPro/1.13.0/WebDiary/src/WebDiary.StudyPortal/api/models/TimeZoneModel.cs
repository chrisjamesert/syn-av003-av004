﻿using System.Runtime.Serialization;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class TimeZoneModel
    {
        [DataMember(Name = "utc")]
        public string UTC { get; set; }
    }
}