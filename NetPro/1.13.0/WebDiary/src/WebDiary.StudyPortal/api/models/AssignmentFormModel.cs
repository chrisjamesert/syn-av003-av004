﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal.Api.Models
{

    [DataContract]
    public class AssignmentFormModel
    {
        [DataMember(IsRequired = true, Name = "U")]
        public string SU { get; set; }

        [DataMember(IsRequired = true, Name = "S")]
        public DateTime Started { get; set; }

        [DataMember(IsRequired = true, Name = "C")]
        public DateTime Completed { get; set; }

        [DataMember(Name = "R")]
        public DateTime ReportDate { get; set; }

        [DataMember(IsRequired = true, Name = "P")]
        public string Phase { get; set; }

        [DataMember(IsRequired = true, Name = "E")]
        public string CoreVersion { get; set; }

        [DataMember(IsRequired = true, Name = "V")]
        public string StudyVersion { get; set; }

        [DataMember(IsRequired = true, Name = "T")]
        public DateTimeOffset PhaseStartDate { get; set; }

        [DataMember(Name = "L")]
        public string BatteryLevel { get; set; }

        [DataMember(IsRequired = true, Name = "K")]
        public string KrPT { get; set; }

        [DataMember(IsRequired = true, Name = "M")]
        public string ResponsibleParty { get; set; }

        [DataMember(Name = "J")]
        public string SigID { get; set; }

        [DataMember(IsRequired = true, Name = "A")]
        public IEnumerable<QuestionAnswerModel> Answers { get; set; }

        [DataMember(Name = "N")]
        public InkSignature2Model Ink { get; set; }

    }

    [DataContract]
    public class QuestionAnswerModel
    {
        [DataMember(IsRequired = true, Name = "G")]
        public string Response { get; set; }

        [DataMember(IsRequired = true, Name = "F")]
        public string SWAlias { get; set; }

        [DataMember(IsRequired = true, Name = "Q")]
        public string QuestionID { get; set; }
    }

    [DataContract]
    public class InkSignature2Model
    {
        [DataMember(IsRequired = true, Name = "X")]
        public int Xsize { get; set; }

        [DataMember(IsRequired = true, Name = "Y")]
        public int Ysize { get; set; }

        [DataMember(IsRequired = true, Name = "D")]
        public string Vector { get; set; }
    }

    [DataContract]
    public class AssignmentFormResult
    {
        [DataMember(Name = "krpt")]
        public string KrPT { get; set; }
    }
}