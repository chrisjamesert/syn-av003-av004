﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Globalization;
using Newtonsoft.Json;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class SubjectModel
    {
        [DataMember(Name = "U")]
        public long? UserID { get; set; }

        [DataMember(Name = "K")]
        public string KrPT { get; set; }

        [DataMember(Name = "I")]
        public string Initials { get; set; }

        [DataMember(Name = "N")]
        public string SubjectNumber { get; set; }

        [DataMember(Name = "C")]
        public string SiteCode { get; set; }

        [DataMember(Name = "P")]
        public int? Phase { get; set; }

        [DataMember(Name = "L")]
        public string LogLevel { get; set; }

        [DataMember(Name = "S")]
        public bool IsActive { get; set; }
    }

    [DataContract]
    public class SubjectResultModel
    {
        [DataMember(Name = "K")]
        public string KrPT { get; set; }

        [DataMember(Name = "I")]
        public string Initials { get; set; }

        [DataMember(Name = "N")]
        public string SubjectNumber { get; set; }

        [DataMember(Name = "C")]
        public string SiteCode { get; set; }

        [DataMember(Name = "P")]
        public int? Phase { get; set; }

        [DataMember(Name = "L")]
        public string LogLevel { get; set; }

        [DataMember(Name = "S")]
        public bool? IsActive { get; set; }

        [DataMember(Name = "T")]
        public DateTimeOffset? PhaseStartDate {get; set; }

        [DataMember(Name = "E")]
        public DateTimeOffset? EnrollmentDate { get; set; }

        [DataMember(Name = "A")]
        public DateTimeOffset? ActivationDate { get; set; }

        [DataMember(Name = "X")]
        public string Language { get; set; }

        [DataMember(Name = "W")]
        public string Password { get; set; }

        [DataMember(Name = "Z")]
        public string ClientPassword { get; set; }
    }

    /// <summary>
    /// Subject data model for GET api/v1/subjects 
    /// </summary>
    [DataContract]
    public class SubjectResult2Model
    {
        [DataMember(Name = "subjectId")]
        public string SubjectId { get; set; }

        [DataMember(Name = "krpt")]
        public string Krpt { get; set; }

        [DataMember(Name = "initials")]
        public string Initials { get; set; }

        [DataMember(Name = "language")]
        public string Language { get; set; }

        [DataMember(Name = "phase")]
        public string Phase { get; set; }

        [DataMember(Name = "phaseStartDate")]
        public string PhaseStartDate { get; set; }

        [DataMember(Name = "deleted")]
        public bool Deleted { get; set; }

        [DataMember(Name = "dob")]
        public string DOB { get; set; }

        [DataMember(Name = "custom1")]
        public string Custom1 { get; set; }

        [DataMember(Name = "custom2")]
        public string Custom2 { get; set; }

        [DataMember(Name = "custom3")]
        public string Custom3 { get; set; }

        [DataMember(Name = "custom4")]
        public string Custom4 { get; set; }

        [DataMember(Name = "custom5")]
        public string Custom5 { get; set; }

        [DataMember(Name = "custom6")]
        public string Custom6 { get; set; }

        [DataMember(Name = "custom7")]
        public string Custom7 { get; set; }

        [DataMember(Name = "custom8")]
        public string Custom8 { get; set; }

        [DataMember(Name = "custom9")]
        public string Custom9 { get; set; }

        [DataMember(Name = "custom10")]
        public string Custom10 { get; set; }

        [DataMember(Name = "lpStartDate")]
        public string LPStartDate { get; set; }

        [DataMember(Name = "lpEndDate")]
        public string LPEndDate { get; set; }

        [DataMember(Name = "spStartDate")]
        public string SPStartDate { get; set; }

        [DataMember(Name = "spEndDate")]
        public string SPEndDate { get; set; }

        [DataMember(Name = "enrollmentDate")]
        public string EnrollmentDate { get; set; }

        [DataMember(Name = "clientPassword")]
        public string ClientPassword { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "setupCode")]
        public long? SetupCode { get; set; }

        [DataMember(Name = "securityQuestion")]
        public int? SecurityQuestion { get; set; }

        [DataMember(Name = "securityAnswer")]
        public string SecurityAnswer { get; set; }

        [DataMember(Name = "updated")]
        public string Updated { get; set; }

        [JsonIgnore]
        public DateTime UpdatedForComparison {get; set; }
    }

    /// <summary>
    /// Subject data model for GET api/v1/subjects/{krpt} 
    /// </summary>
    [DataContract]
    public class ExtendedSubjectResultModel
    {
        [DataMember(Name = "subjectId")]
        public string SubjectId { get; set; }

        [DataMember(Name = "initials")]
        public string Initials { get; set; }

        [DataMember(Name = "language")]
        public string Language { get; set; }

        [DataMember(Name = "krdom")]
        public string Krdom { get; set; }

        [DataMember(Name = "siteCode")]
        public string SiteCode { get; set; }

        [DataMember(Name = "phase")]
        public string Phase { get; set; }

        [DataMember(Name = "phaseStartDate")]
        public string PhaseStartDate { get; set; }

        [DataMember(Name = "deleted")]
        public bool? Deleted { get; set; }

        [DataMember(Name = "dob")]
        public string DOB { get; set; }

        [DataMember(Name = "gender")]
        public string Gender { get; set; }

        [DataMember(Name = "custom1")]
        public string Custom1 { get; set; }

        [DataMember(Name = "custom2")]
        public string Custom2 { get; set; }

        [DataMember(Name = "custom3")]
        public string Custom3 { get; set; }

        [DataMember(Name = "custom4")]
        public string Custom4 { get; set; }

        [DataMember(Name = "custom5")]
        public string Custom5 { get; set; }

        [DataMember(Name = "custom6")]
        public string Custom6 { get; set; }

        [DataMember(Name = "custom7")]
        public string Custom7 { get; set; }

        [DataMember(Name = "custom8")]
        public string Custom8 { get; set; }

        [DataMember(Name = "custom9")]
        public string Custom9 { get; set; }

        [DataMember(Name = "custom10")]
        public string Custom10 { get; set; }

        [DataMember(Name = "lpStartDate")]
        public string LPStartDate { get; set; }

        [DataMember(Name = "lpEndDate")]
        public string LPEndDate { get; set; }

        [DataMember(Name = "spStartDate")]
        public string SPStartDate { get; set; }

        [DataMember(Name = "spEndDate")]
        public string SPEndDate { get; set; }

        [DataMember(Name = "enrollmentDate")]
        public string EnrollmentDate { get; set; }

        [DataMember(Name = "activationDate")]
        public string ActivationDate { get; set; }

        [DataMember(Name = "clientPassword")]
        public string ClientPassword { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "setupCode")]
        public long? SetupCode { get; set; }

        [DataMember(Name = "securityQuestion")]
        public int? SecurityQuestion { get; set; }

        [DataMember(Name = "securityAnswer")]
        public string SecurityAnswer { get; set; }

        [DataMember(Name = "logLevel")]
        public string LogLevel { get; set; }

        [DataMember(Name = "isActive")]
        public bool? IsActive { get; set; }

        [DataMember(Name = "updated")]
        public string Updated { get; set; }

        [JsonIgnore]
        public DateTime UpdatedForComparison { get; set; }
    }
}
