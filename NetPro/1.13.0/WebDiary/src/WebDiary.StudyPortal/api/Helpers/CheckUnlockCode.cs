﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using System.Threading;
using log4net;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Api.Helpers;
using Newtonsoft.Json.Linq;

namespace WebDiary.StudyPortal.Api.Helpers
{
    public class CheckUnlockCode
    {
        public static bool IsValidCode(StudyMiddleTier studyMiddleTier, DateTime clientDate, string clientUnlockCode)
        {
            short seed = studyMiddleTier.SeedCode;
            byte length = studyMiddleTier.CodeLength;
            string unlockcode = SubjectHelper.GetUnlockCode(seed, length, clientDate); // current date code
            if (unlockcode != clientUnlockCode)
            {
                unlockcode = SubjectHelper.GetUnlockCode(seed, length, clientDate.AddDays(-1)); // previous date code
                if (unlockcode != clientUnlockCode)
                {
                    unlockcode = SubjectHelper.GetUnlockCode(seed, length, clientDate.AddDays(1)); // next date code
                    if (unlockcode != clientUnlockCode)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}