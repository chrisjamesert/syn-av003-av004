﻿using System;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace WebDiary.StudyPortal.Api
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ObjectLengthAttribute : ValidationAttribute
    {
        int length;
        public ObjectLengthAttribute(int length)
        {
            this.length = length;
        }

        public override bool IsValid(object value)
        { 
            return value.ToString().Length <= length;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.InvariantCulture,
              "The maximum Json size is {0} characters", length);
        }
    }
}