﻿<%@ Page Language="C#" AutoEventWireup="true"  validateRequest="false" CodeBehind="forgot-password.aspx.cs" Inherits="WebDiary.StudyPortal.ForgotPassword" MasterPageFile="~/includes/masters/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        autoLock = false;
        $(document).ready(function () {
            $('input[id="txtEmail"]').keydown(function () {
                $('#vsErrors').hide();
            });
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainContent" runat="server">
<pht:DivPanel ID="pnlForm" runat="server">
    <asp:ValidationSummary ID="vsErrors" CssClass="errors" runat="server" 
        DisplayMode="BulletList" EnableClientScript="False"/>
        <div id="unauthBrowser">
            <div id="unauthMsgArea">
                <asp:Label ID="lblUnauthorizedMessage" CssClass="unAuthorizedError" Visible="false" runat="server">
                </asp:Label>
            </div>
            <div id="authBrowserArea" runat="server">
            </div>      
      </div>
    <p>
        <asp:Label ID="lblForgotPswrdMsgBody" runat="server" Text="<%$ Resources:Resource, ForgotPswrdMsgBody%>"></asp:Label>
    </p>
    <table>
        <tr>
            <td class="label"><asp:Label style="font-weight:bold" ID="lblEmail" runat="server" Text="<%$ Resources:Resource, emailLbl %>"></asp:Label></td>
            <td class="field">
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="256" />
                <asp:TextBox ID="InvisibleIEBugTextBox" runat="server" Style="visibility:hidden;display:none;"/>
            </td>
        </tr>
        <tr class="form-empty-row">
            <td />
            <td>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="nb-errors"
                ControlToValidate="txtEmail" 
                ErrorMessage="<%$ Resources:Resource, LoginrfvEmail %>" Display="Dynamic" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, rfvEmailNotValid %>" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                CssClass="nb-errors"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr><td colspan="2" align="right"><asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" runat="server" /></td></tr>
    </table>
    <br />
</pht:DivPanel>

<pht:DivPanel ID="pnlSuccess" Visible="False" runat="server">
    <p><asp:Label ID="lblForgotPswrdConfirMsgBody" runat="server" Text="<%$ Resources:Resource, ForgotPswrdConfirMsgBody%>"></asp:Label></p>
</pht:DivPanel>

</asp:Content>
