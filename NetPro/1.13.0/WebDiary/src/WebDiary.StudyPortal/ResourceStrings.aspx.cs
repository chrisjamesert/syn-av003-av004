﻿using System;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using System.IO;
using System.Text;
using System.Web;
using System.Globalization;
using System.Linq;

namespace WebDiary.StudyPortal
{
    public partial class ResourceStrings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Charset = "UTF-8";
            string lang = null;
            string dir = null;
            
            // if language is specified in the querystring, the specified language will overwrite subject's language
            if (!string.IsNullOrEmpty(Request.QueryString["lang"]))
            {
                // When the language-locale code is valid, this should be the language of the returned resource strings whether the setup code is valid or not
                lang = FormatLanguage(Request.QueryString["lang"]);
                dir = HttpContext.Current.Server.MapPath("~/LF/Trial/NLS/" + lang);

                // If the language-locale code is absent or not valid, the language should be determined by the setup code
                if (!Directory.Exists(dir))
                {
                    lang = GetSubjectLanguage();
                }
            }
            else
            {
                lang = GetSubjectLanguage();
            }

            if(!string.IsNullOrEmpty(lang))
                literalScript.Text = GetResource(lang);
        }

        private string GetSubjectLanguage()
        {
            string lang = null;
            string dir = null;
            // attempt to aunthenticate suject
            Subject subject = SubjectHelper.AuthenticateLFSubject(Context);

            // if authentication fails, find subject by setup code in the querystring
            if (subject == null)
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SubjectRepository subjRepo = new SubjectRepository(db);
                    long pin = 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["code"]))
                        pin = long.Parse(Request.QueryString["code"]);

                    subject = subjRepo.FindByPin(pin);
                }
            }

            if (subject != null)
            {
                lang = FormatLanguage(subject.Language);
                dir = HttpContext.Current.Server.MapPath("~/LF/Trial/NLS/" + lang);

                if (!Directory.Exists(dir))
                {
                    lang = FormatLanguage(Request.UserLanguages[0]);
                }
            }
            else  // if subject is still unknown at this point, browser language should be used
            {
                lang = FormatLanguage(Request.UserLanguages[0]);
            }

            return lang;
        }

        // format language iso code
        private string FormatLanguage(string language)
        {
            string returnValue;
            if (language.Length < 3) // ex: fr
                if (language == "en") // en is the only exception, we don't support en-EN
                    returnValue = "en-US";
                else
                    returnValue = language.ToLower() + "-" + language.ToUpper(); // we got fr-FR
            else
                returnValue = string.Format(CultureInfo.InvariantCulture, "{0}-{1}", language.Substring(0, 2).ToLower(), language.Substring(3, 2).ToUpper());

            return returnValue;
        }

        private string GetResource(string languageCode)
        {
            // get default language
            string defaultLanguageCode = languageCode;
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                var studyMiddleTier = db.StudyMiddleTiers.Single(s => s.StudyId == StudyData.study.Id);
                if (studyMiddleTier != null)
                    defaultLanguageCode = studyMiddleTier.DefaultLanguage;
            }

            StringBuilder preferredLanguageRes = new StringBuilder();

            // generate default language
            if (!string.IsNullOrEmpty(defaultLanguageCode) && languageCode != defaultLanguageCode)
            {
                string defaultDir = HttpContext.Current.Server.MapPath("~/LF/Trial/NLS/" + defaultLanguageCode);
                if (Directory.Exists(defaultDir))
                {
                    preferredLanguageRes.AppendFormat("// default core and study strings{0}", Environment.NewLine);
                    string resourcesFile = defaultDir + "\\Resources.js";

                    if (File.Exists(resourcesFile)) // the concatenation has been done by the production build
                    {
                        preferredLanguageRes.AppendFormat("{0}{1}{1}", File.ReadAllText(resourcesFile), Environment.NewLine);
                    }
                    else // perform the concatenation at runtime
                    {
                        string coreResourcesFile = defaultDir + "\\CoreResources.js";
                        if (File.Exists(coreResourcesFile))
                            preferredLanguageRes.AppendFormat("{0}{1}", File.ReadAllText(coreResourcesFile), Environment.NewLine);

                        preferredLanguageRes.AppendLine("LF.strings.add([");
                        string[] jsonFiles = Directory.GetFiles(defaultDir, "*.json", SearchOption.AllDirectories);
                        for (int i = 0; i < jsonFiles.Length; i++)
                        {
                            if (jsonFiles[i].Contains("coreStrings")) continue;
                            preferredLanguageRes.Append(File.ReadAllText(jsonFiles[i]));
                            if (i + 1 < jsonFiles.Length)
                                preferredLanguageRes.Append("," + Environment.NewLine);
                        }
                        preferredLanguageRes.AppendFormat("]);{0}{0}", Environment.NewLine);
                    }
                }
            }

            // generate preferred language            
            string curDir = HttpContext.Current.Server.MapPath("~/LF/Trial/NLS/" + languageCode);
            if (Directory.Exists(curDir))
            {
                preferredLanguageRes.AppendFormat("// preferred core and study strings{0}", Environment.NewLine);
                string resourcesFile = curDir + "\\Resources.js";
                string preferredLangFile = curDir + "\\PreferredLang.js";

                if (File.Exists(resourcesFile)) // the concatenation has been done by the production build
                {
                    preferredLanguageRes.AppendFormat("{0}{1}{1}", File.ReadAllText(resourcesFile), Environment.NewLine);
                }
                else // perform the concatenation at runtime
                {
                    string coreResourcesFile = curDir + "\\CoreResources.js";
                    if (File.Exists(coreResourcesFile))
                        preferredLanguageRes.AppendFormat("{0}{1}", File.ReadAllText(coreResourcesFile), Environment.NewLine);

                    preferredLanguageRes.AppendLine("LF.strings.add([");
                    string[] files = Directory.GetFiles(curDir, "*.json", SearchOption.AllDirectories);
                    for (int i = 0; i < files.Length; i++)
                    {
                        preferredLanguageRes.Append(File.ReadAllText(files[i]));
                        if (i + 1 < files.Length)
                            preferredLanguageRes.Append("," + Environment.NewLine);
                    }
                    preferredLanguageRes.AppendFormat("]);{0}{0}", Environment.NewLine);
                }

                if (File.Exists(preferredLangFile))
                {
                    preferredLanguageRes.AppendLine(File.ReadAllText(preferredLangFile));
                }
                else
                {
                    preferredLanguageRes.AppendFormat("LF.Preferred = {{language : '{0}', locale : '{1}'}};", languageCode.Substring(0, 2), languageCode.Substring(3, 2));
                }
            }
            else
            {
                preferredLanguageRes.AppendFormat("LF.Preferred = {{language : '{0}', locale : '{1}'}};", defaultLanguageCode.Substring(0, 2), defaultLanguageCode.Substring(3, 2));
            }

            return preferredLanguageRes.ToString();
        }
    }
}