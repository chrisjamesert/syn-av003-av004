﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using System.IO;
using System.Resources;
using System.Web.Routing;
using System.Xml.Linq;

using log4net;
using WebDiary.SWAPI;
using System.Globalization;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.Core.Helpers;
using WebDiary.StudyPortal.Api;
using WebDiary.StudyPortal.Api.Filters;

namespace WebDiary.StudyPortal
{
    public class Global : System.Web.HttpApplication
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            FileInfo configFile = new FileInfo(String.Format(CultureInfo.InvariantCulture, "{0}web.config", AppDomain.CurrentDomain.SetupInformation.ApplicationBase));
            log4net.Config.XmlConfigurator.Configure(configFile);
            if (log4net.GlobalContext.Properties["study_id"] == null)
            {
                log4net.GlobalContext.Properties["study_id"] = StudyData.study.Id.ToString(CultureInfo.InvariantCulture);
            }
            logger.Info("WebDiary.StudyPortal application start");

            Application["rm"] = new ResourceManager(typeof(Resources.Resource));

            //Register a custom http message handler for authorization
            GlobalConfiguration.Configuration.MessageHandlers.Add(new AuthorizationHandler());
            //Custom http message handler for decompression  
            GlobalConfiguration.Configuration.MessageHandlers.Add(new JSONMHandler());

            // apply ValidateModel filter to all Web API controllers
            GlobalConfiguration.Configuration.Filters.Add(new ValidateModelAttribute());

            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

            RegisterRoutes(RouteTable.Routes);            
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapHttpRoute(
            name: "ChildApiV1",
            routeTemplate: "api/v1/subjects/{id}/{controller}/{param}",
            defaults: new { param = RouteParameter.Optional }
            );

            // Routing template for REST Api
            routes.MapHttpRoute(
            name: "ApiV1",
            routeTemplate: "api/v1/{controller}/{id}",
            defaults: new { id = RouteParameter.Optional }
            );

            routes.MapPageRoute("LFResource",
                "ActivateResourceStrings.aspx",
                "~/ResourceStrings.aspx");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Handle various session timeout scenarios.
            if ((Request.Path.IndexOf("index.aspx", StringComparison.OrdinalIgnoreCase) != -1
              || Request.Path.IndexOf("settings.aspx", StringComparison.OrdinalIgnoreCase) != -1
              || Request.Path.IndexOf("submit.aspx", StringComparison.OrdinalIgnoreCase) != -1
              || Request.Path.IndexOf("change-password.aspx", StringComparison.OrdinalIgnoreCase) != -1)
              && Request.Path.IndexOf("mobile", StringComparison.OrdinalIgnoreCase) == -1)
            {
                string returnUrl = Request.QueryString[WDConstants.VarReturnUrl];

                if (String.IsNullOrEmpty(returnUrl))
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    //***** TEMPORARY FIX UNTIL CHANGES ARE MADE IN StudyWorks. *****
                    returnUrl = ConstructSWTimeoutPage();

                    Response.Redirect(returnUrl);
                }
            }
            else if (Request.Path.IndexOf("site-access.aspx", StringComparison.OrdinalIgnoreCase) != -1)
            {
                //Redirect to the appropriate page after a session timeout.
                string returnUrl = Server.UrlDecode(Request.QueryString[WDConstants.VarSwReturnUrl]);

                if (String.IsNullOrEmpty(returnUrl))
                {
                    string err = Resources.Resource.ErrorSWRes;
                    logger.Error(err);
                    Session[WDError.ErrorMessageKey] = err;
                    Response.Redirect(WDError.Path, false);
                }
                else
                {
                    //***** TEMPORARY FIX UNTIL CHANGES ARE MADE IN StudyWorks. *****
                    returnUrl = ConstructSWTimeoutPage();

                    Response.Redirect(returnUrl);
                }
            }
            else if (Request.Path.IndexOf("instruction.aspx", StringComparison.OrdinalIgnoreCase) != -1)
            {
                string returnUrl = ConstructSWTimeoutPage();
                Response.Redirect(returnUrl);
            }

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["FORCE_SSL"], CultureInfo.InvariantCulture)
                && HttpContext.Current.Request.IsSecureConnection.Equals(false))
            {
                Response.Redirect(String.Format(CultureInfo.InvariantCulture, "https://{0}{1}", Request.Url.Host, HttpContext.Current.Request.RawUrl));
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = HttpContext.Current.Server.GetLastError();
            while (ex.InnerException != null) ex = ex.InnerException;

            HttpException httpEx = ex as HttpException;
            if (httpEx != null && httpEx.ErrorCode == 404)
            {
                if (logger.IsWarnEnabled)
                {
                    logger.Warn("404 - Page not found", ex);
                }
            }
            else
            {
                if (logger.IsFatalEnabled)
                {
                    logger.Fatal("Unhandled error!", ex);
                }
            }
            // HttpContext.Current.Server.ClearError();
            // Response.Redirect("SomeOtherPage.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected string ConstructSWTimeoutPage()
        {
            string returnUrl = null;

            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    returnUrl = StudyData.study.SsaBaseUrl + SWAPIConstants.SessionTimeoutPage;
                }
            }
            catch (Exception ex)
            {

                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }

            return returnUrl;
        }
    }
}
