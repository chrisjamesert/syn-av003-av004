﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;

using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using WebDiary.SurveyToolDBAdapters;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;

using log4net;
using System.Collections;
using WebDiary.Core.Errors;
using WebDiary.Membership.User;
using WebDiary.Membership.Provider;
using WebDiary.StudyPortal.Helpers;
using WebDiary.Core.Email;

namespace WebDiary.StudyPortal
{
    public partial class Index : BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(Index));
        private const string QUESTIONNAIRE_LIST = "questionnaire_list";
        private Subject subject;
        WDAPIObject wd; 

        protected void Page_Init(object sender, EventArgs e)
        {
            //Get values based on study.
            wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

            if (Request.QueryString["surveyref"] != null && string.IsNullOrEmpty(Request.QueryString["surveyref"]) == false)
            {
                LaunchSurvey(Int32.Parse(Request.QueryString["surveyref"], CultureInfo.InvariantCulture));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SubmitResult"] != null)
            {
                if (!string.IsNullOrEmpty(Session["SubmitResult"].ToString()))
                {
                    notificationScriptID.Text = "<script type=\"text/javascript\">notification.text = '" + Session["SubmitResult"].ToString() + "';</script>";
                    Session["SubmitResult"] = null;
                }
            }

            //inject script to adjust layout based on language direction when questionnaire list get updated
            if (TextDirection == "rtl")
                ScriptManager.RegisterStartupScript(requiredQuestionnaireList, requiredQuestionnaireList.GetType(), "adjustLayout", "adjustLayout();", true);

            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    subject = subjectRep.FindById(new Guid(Context.User.Identity.Name));
                    WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(subject, db, true);
                    if (user.IsApproved && !user.IsLockedOut)
                    {
                        if (!IsPostBack)
                        {
                            //In case we back out of survey frame
                            Session[WDConstants.FramePostUrl] = null;
                            Session[WDConstants.FrameRedirectUrl] = null;
                            Session[WDConstants.FrameLeaveUrl] = null;

                            try
                            {
                                //if UST or staging
                                if (StudyData.version.ProductEnvironment != WebDiary.Core.Helpers.Version.EnvironmentProduction)
                                {
                                    DTPanel.Visible = true;
                                    txtTimeTravel.Text = DateTimeHelper.GetLocalTime().ToString(SWAPIConstants.DateTimeTravelFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                    DTPanel.Visible = false;
                            }
                            catch (NullReferenceException)
                            {
                                Response.Redirect("~/login.aspx");
                            }

                            //Only if Developer mode, show screen shot checkbox and select language dropdown list  
                            if (StudyData.version.ProductEnvironment == WebDiary.Core.Helpers.Version.EnvironmentDeveloper)
                            {
                                pnlScreenShot.Visible = true;
                                string screenShot = Session[WDConstants.VarScreenShot] as string ?? string.Empty;
                                if (!string.IsNullOrEmpty(screenShot) && (screenShot == "true"))
                                    chkScreenShot.Checked = true;

                                //Begin of PDE language selection for screenshot
                                pnlScreenShotLanguage.Visible = true;

                                //Option 1 - Get language list from studyWorks
                                if (!WDAPIObject.IsTrainer(subject.KrPT))
                                {
                                    var languages = wd.GetLanguages();
                                    if (languages != null)
                                    {
                                        int counter = 0;
                                        int selectedIndex = 0;
                                        string defaultLanguage = "en-US";
                                        if (!string.IsNullOrEmpty(Session[WDConstants.Language] as string))
                                        {
                                            defaultLanguage = Session[WDConstants.Language].ToString();
                                        }
                                        else if (Request.Cookies[WDConstants.Language] != null && !string.IsNullOrEmpty(Request.Cookies[WDConstants.Language].Value))
                                        {
                                            defaultLanguage = Request.Cookies[WDConstants.Language].Value;
                                        }

                                        foreach (var item in languages.list)
                                        {
                                            string languageCode = item.code.Replace("_", "-");
                                            ScreenShotLanguageList.Items.Add(new ListItem(languageCode, languageCode));
                                            if(string.Equals(languageCode, defaultLanguage, StringComparison.OrdinalIgnoreCase))
                                                    selectedIndex = counter;
                                            counter++;
                                        }
                                        ScreenShotLanguageList.SelectedIndex = selectedIndex;
                                    }
                                }
                                else
                                {

                                    //Option2 - Get languages from resource files
                                    List<string> languages = new List<string>();
                                    languages = GetAvailableLanguage();
                                    int counter = 1;
                                    int selectedIndex = 0;

                                    string defaultLanguage = "en-US";
                                    if (!string.IsNullOrEmpty(Session[WDConstants.Language] as string))
                                    {
                                        defaultLanguage = Session[WDConstants.Language].ToString();
                                    }
                                    else if (Request.Cookies[WDConstants.Language] != null && !string.IsNullOrEmpty(Request.Cookies[WDConstants.Language].Value))
                                    {
                                        defaultLanguage = Request.Cookies[WDConstants.Language].Value;
                                    }

                                    ScreenShotLanguageList.Items.Add(new ListItem("en-US", "en-US"));
                                    foreach (var item in languages)
                                    {
                                        ScreenShotLanguageList.Items.Add(new ListItem(item.ToString(CultureInfo.InvariantCulture), item.ToString(CultureInfo.InvariantCulture)));
                                        if (string.Equals(item, defaultLanguage, StringComparison.OrdinalIgnoreCase))
                                                selectedIndex = counter;
                                        counter++;
                                    }
                                    ScreenShotLanguageList.SelectedIndex = selectedIndex;
                                }
                                //End of PDE language selection

                                //Begin of showing email type for screenshot
                                pnlScreenShotEmail.Visible = true;

                                ScreenShotEmailList.Items.Add(new ListItem("Activation", WDConstants.EmailType.Activation.ToString()));
                                ScreenShotEmailList.Items.Add(new ListItem("Confirmation", WDConstants.EmailType.Confirmation.ToString()));
                                ScreenShotEmailList.Items.Add(new ListItem("Email Update", WDConstants.EmailType.EmailUpdate.ToString()));
                                ScreenShotEmailList.Items.Add(new ListItem("Email Language Update", WDConstants.EmailType.EmailLanguageUpdate.ToString()));
                                ScreenShotEmailList.Items.Add(new ListItem("Forgot Password", "ForgotPassword"));
                                ScreenShotEmailList.Items.Add(new ListItem("Language Update", WDConstants.EmailType.LanguageUpdate.ToString()));
                                ScreenShotEmailList.Items.Add(new ListItem("Reset Password", WDConstants.EmailType.ResetPassword.ToString()));
                                ScreenShotEmailList.Items.Add(new ListItem("Scheduled Email Reminder", "ScheduledEmailReminder"));

                                //Populate SurveyList for screenshot
                                var surveyList = db.StudySurveys.Where(s => s.StudyId == subject.StudyId && s.FormType == SWAPIConstants.FormTypeSubjectSubmit).OrderBy(s => s.DisplayName);
                                foreach (var survey in surveyList)
                                {
                                    SurveyList.Items.Add(new ListItem(survey.DisplayName, survey.Id.ToString(CultureInfo.InvariantCulture)));
                                }
                                //End of showing email type for screenshot
                            }
                            else
                                pnlScreenShot.Visible = false;

                            // Get subject number
                            string patientId = Session[WDConstants.VarPtPatientId] as string ?? string.Empty;
                            if (string.IsNullOrEmpty(patientId))
                            {
                                SubjectData sdata = wd.GetSubjectData(Session[WDConstants.VarKrpt].ToString(), new ArrayList { SWAPIConstants.ColumnPatientId });
                                patientId = sdata.data.Patientid;
                            }
                            patientIDLbl.Text = patientId;

                            LoadData();
                        }
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                string err = "A problem occurred while trying to open the page: " + ex.Message;
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = Resources.Resource.IndexPageError;
                Response.Redirect(WDError.Path, true);
            }

            if (!String.IsNullOrEmpty(Context.User.Identity.Name) && Context.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
            }

            Response.Redirect(HttpContext.Current.Request.ApplicationPath);
        }

        protected void LoadData()
        {
            try
            {
                //Set return url.
                string returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/index.aspx";
                Session[WDConstants.VarReturnUrl] = returnUrl;

                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    int studyId = subject.StudyId;
                    
                    if(!wd.IsSubjectEnabled(subject.KrPT))
                    {
                        Response.Redirect("logout.aspx", false);
                        return;
                    }

                    var surveyList = db.StudySurveys.Where(s => s.StudyId == studyId && s.FormType == SWAPIConstants.FormTypeSubjectSubmit);
                    Dictionary<string, string> AvailableDic = new Dictionary<string, string>();
                    Dictionary<string, string> DueDic = new Dictionary<string, string>();
                    Dictionary<string, bool> URLEnabledDic = new Dictionary<string, bool>();
                    List<List<string>> completedDiaries = new List<List<string>>();
                    ArrayList returnItems = new ArrayList(1);
                    returnItems.Add("SU.ReportStartDate");

                    foreach (var survey in surveyList)
                    {
                        ScheduleConf config = SurveyScheduler.GetSchedule(survey, subject, StudyData.study);
                        AvailableDic[survey.ActivationURL] = config.StringAvailable;
                        DueDic[survey.ActivationURL] = config.StringDue;
                        URLEnabledDic[survey.ActivationURL] = config.Enabled;

                        //pull the list of last completed diaries
                        ClinData clinData = wd.GetClinDataByNumberRange(subject.KrPT, survey.KRSU, -10, returnItems);
                        foreach (List<string> item in clinData.datatable)
                            item.Add(survey.DisplayName);
                        completedDiaries.AddRange(clinData.datatable);
                    }

                    var compDiaryList = from c in completedDiaries
                                        select new
                                        {
                                            DateReport = FormatDT(c[0]),
                                            SU = c[1],
                                            HiddenCol = c[0]
                                        };

                    rptHistoryList.DataSource = compDiaryList;
                    rptHistoryList.DataBind();

                    //PDE - list of surveys to hide
                    List<String> hiddenSurveyList = new List<String>();
                    //Flush the hidden survey list and re-calculate
                    hiddenSurveyList.Clear();

                    /*
                     *  PDE - Add custom schedule logic to hide diaries
                     *  
                     *  Add KRSU values to the hidden survey list that you do not want to appear on the gateway
                     *  
                     *  ex: hiddenSurveyList.Add(PDETools.KRSU_BRFreq);
                     */



                    var subjectQuestionnaireList = from s in db.StudySurveys
                                                   where s.StudyId == studyId &&
                                                   s.FormType == SWAPIConstants.FormTypeSubjectSubmit &&
                                                   hiddenSurveyList.Contains(s.KRSU) == false
                                                   orderby s.SortOrder 
                                                   select new
                                                   {
                                                       SurveyID = s.Id,
                                                       DisplayName = s.DisplayName,  //PDE - if study includes non-english languages, this must be updated to read from a resource file (resx) Ex: (String)GetGlobalResourceObject("PDEResource", s.KRSU)
                                                       ActivationURL = s.ActivationURL,
                                                       Availability = AvailableDic[s.ActivationURL],
                                                       Due = DueDic[s.ActivationURL],
                                                       TriggeredPhase = s.TriggeredPhase,
                                                       URLEnabled = URLEnabledDic[s.ActivationURL]
                                                   };

                    if (subjectQuestionnaireList.Count() == 0)
                    {
                        SurveyInstructionLabel.Text = Resources.Resource.NoQuestionnaires;
                        requiredQuestionnaireList.Visible = false;
                    }
                    else
                    {
                        SurveyInstructionLabel.Text = Resources.Resource.SurveyInstr;
                    }
                    //Bind to available questionnaires control.
                    requiredQuestionnaireList.DataSource = subjectQuestionnaireList;
                    requiredQuestionnaireList.DataBind();

                    //Get study and WebDiary versions.

                    //the session variables below are not being used anywhere. they should be removed?
                    Session[WDConstants.VarSuWebDiaryVersion] = StudyData.version.ProductVersionFull;
                    Session[WDConstants.VarSuStudyVersion] = StudyData.version.StudyVersionFull;
                }
            }
            catch (Exception ex)
            {
                string err = "A problem occurred while trying to open the page: " + ex.Message;
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = Resources.Resource.IndexPageError;
                Response.Redirect(WDError.Path, true);
            }
        }

        protected void LaunchSurvey(int id)
        {
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SurveyRepository surveyRep = new SurveyRepository(db);
                    StudySurveys survey = surveyRep.FindById(id);
                    if (subject == null)
                    {
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        subject = subjectRep.FindById(new Guid(Context.User.Identity.Name));
                    }

                    //Get the required values for submitting.
                    Session[WDConstants.VarKrpt] = subject.KrPT;
                    Session[WDConstants.VarSuTimeZoneValue] = subject.TimeZone;

                    if (!wd.IsSubjectEnabled(subject.KrPT))
                    {
                        Session[WDError.ErrorMessageKey] = Resources.Resource.ErrorSubjectDisabled;
                        Response.Redirect(WDError.Path, true);
                    }

                    SubjectData sd = wd.GetSubjectData(subject.KrPT,
                                                    new ArrayList {SWAPIConstants.ColumnPatientId,
                                                                    SWAPIConstants.ColumnPatientInitials,
                                                                    SWAPIConstants.ColumnPatientKrDom,
                                                                    SWAPIConstants.ColumnPatientKrPhase,
                                                                    SWAPIConstants.ColumnPatientPhaseDate});

                    Session[WDConstants.VarPtPatientId] = sd.data.Patientid;
                    Session[WDConstants.VarPrintedName] = sd.data.Initials;
                    Session[WDConstants.VarKrDom] = sd.data.krdom;

                    //Set current phase info.
                    Session[WDConstants.VarSuPhaseAtEntry] = sd.data.Krphase;
                    //Fix krphasedate if necessary.
                    if (sd.data.Phasedate.Contains('.'))
                    {
                        string[] str = sd.data.Phasedate.Split('.');
                        string strnew = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);

                        Session[WDConstants.VarSuPhaseStartDate] = strnew;
                    }
                    else
                    {
                        Session[WDConstants.VarSuPhaseStartDate] = sd.data.Phasedate;
                    }


                    ScheduleConf config = SurveyScheduler.GetSchedule(survey, subject, StudyData.study);
                    if (!config.Enabled)
                    {
                        Page.Validators.Add(new ValidationError(Resources.Resource.SurveyNotAvailable));
                        return;
                    }

                    SurveyData surveyData = SurveyDataHelper.CreateSurveyData(survey.Id, survey.TriggeredPhase);
                    surveyData.Sigid = "NP." + WDAPIObject.GenerateSignatureId(); 
                    NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);

                    if (!survey.IsExternal)
                    {

                        /*
                         *  PDE - pass in hidden items to a survey here
                         */

                        Object subjectID = System.Web.HttpContext.Current.Session[WDConstants.VarPtPatientId].ToString();

                        if (subjectID != null)
                        {
                            queryStrings.Add("subject_id", subjectID.ToString());
                        }

                        Object siteID = System.Web.HttpContext.Current.Session[WDConstants.VarSiteCode];

                        if (siteID != null)
                        {
                            queryStrings.Add("site_id", siteID.ToString());
                        }

                        /* PDE - SWAPI call example
                         * //Get SWAPI hook
                         * StudyRepository sr = new StudyRepository(db);
                         * Study study = sr.FindByName(Session[WDConstants.VarProtocol].ToString());
                         * SubjectRepository subjRep = new SubjectRepository(db);
                         * Subject subject = subjRep.FindByKrptAndStudy(surveyData.Krpt, study.Id);
                         * WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                         * 
                         * 
                         * String krpt = subject.KrPT.ToString();
                         * //Setup arguments for stored procedure
                         * ArrayList PDEArgs = PDETools.setupSwapiArgs(krpt, Session[WDConstants.VarSuTimeZoneValue].ToString());
                         * //Run stored procedure and get results
                         * String launchSurveyData = wd.GetData("PDE_NetProBPInfoSQLV1", PDEArgs).response;
                         */

                        string url = survey.ActivationURL + "&" + queryStrings.ToString();
                        HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                        Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
                    }
                    else
                    {
                        Session[WDConstants.IsExternal] = true;
                        ExternalLauncher.LaunchExternalSurvey(Request, Response, survey, surveyData);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                logger.Error("Database error on launching the survey", ex);
                Session[WDError.ErrorMessageKey] = Resources.Resource.IndexPageError;
                Response.Redirect(WDError.Path, true);
            }
        }

        protected void RequiredQuestionnaireList_ItemCommand(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "launch")
            {
                int surveyid = Int32.Parse(e.CommandArgument.ToString(), CultureInfo.InvariantCulture);
                LaunchSurvey(surveyid);
            }
        }

        protected void RequiredQuestionnaireList_ItemDataBound(object source, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {

            }
        }

        protected void DTApply_Click(object sender, EventArgs e)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                DateTime DTtravel = DateTime.Parse(txtTimeTravel.Text, CultureInfo.InvariantCulture);
                SubjectRepository subRep = new SubjectRepository(db);
                subject = subRep.FindById(subject.Id);
                subject.TimeTravelOffset = (int)(TimeSpan.FromTicks((DateTimeHelper.GetUtcTime(DTtravel, subject.StudyId, subject.TimeZone)).Ticks - DateTime.UtcNow.Ticks)).TotalMinutes;
                db.SubmitChanges();
                SurveyScheduler.ScheduleEmailAllSurveys(subject, StudyData.study, SurveyScheduler.ScheduleState.TimeTravel);
                Session[DateTimeHelper.TimeTravelOffset] = subject.TimeTravelOffset;
            }

            string returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/index.aspx";
            Response.Redirect(returnUrl, true);
        }

        private static string FormatDT(string dtStr)
        {
            DateTime dt = DateTime.Parse(dtStr, CultureInfo.InvariantCulture);

            return dt.ToPreferredShortMonthName(SWAPIConstants.NetProShortDateTime, CultureInfo.CurrentCulture);
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            LoadData();            
        }

        protected void chkScreenShot_CheckedChanged(object sender, EventArgs e)
        {
            if (chkScreenShot.Checked)
                Session[WDConstants.VarScreenShot] = "true";
            else
                Session[WDConstants.VarScreenShot] = null;
            LoadData();  
        }

        protected void ScreenShotLanguageList_IndexChanged(object sender, EventArgs e)
        {
            Session[WDConstants.Language] = Response.Cookies[WDConstants.Language].Value = WDAPIObject.JavaLocaleToDotNetCulture(ScreenShotLanguageList.SelectedItem.Value);

            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                SubjectRepository subRep = new SubjectRepository(db);
                subject = subRep.FindById(subject.Id);
                subject.Language = ScreenShotLanguageList.SelectedItem.Value.Replace("-", "_");
                db.SubmitChanges();
            }

            string returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/index.aspx";
            Response.Redirect(returnUrl, false);
        }

        protected void SendEmail_Click(object sender, EventArgs e)
        {
            string language = ScreenShotLanguageList.SelectedValue; 
            string emailType = ScreenShotEmailList.SelectedValue;
            string surveyId = string.Empty;

            if (emailType.ToLower(CultureInfo.InvariantCulture) == "scheduledemailreminder")
            {
                surveyId = SurveyList.SelectedValue; 
            }

            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                StudyRepository studyRep = new StudyRepository(db);
                Study study = studyRep.FindById(subject.StudyId);

                SurveyRepository surveyRep = new SurveyRepository(db);
                StudySurveys survey = null;

                if (!string.IsNullOrEmpty(surveyId))
                {
                    survey = surveyRep.FindById(Convert.ToInt32(surveyId));
                }

                Message message = new Message();
                message.SetFrom(ConfigurationManager.AppSettings["FROM_ADDRESS"]);
                message.AddTo(subject.Email);
                message.SetSubject(Resources.Resource.EmailAcctActivationSubject);

                string emailBody = string.Empty;
                if (emailType.ToLower(CultureInfo.InvariantCulture) == "activation")
                {
                    message.SetSubject(Resources.Resource.EmailAcctActivationSubject.ToString());
                    emailBody = EmailHelper.GetActivationEmail(study, subject);
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "confirmation")
                {
                    message.SetSubject(Resources.Resource.EmailAcctActivConfirmationSubject.ToString());
                    emailBody = EmailHelper.GetConfirmationEmail(study, subject);
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "forgotpassword")
                {
                    WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(subject.Email.ToString(CultureInfo.InvariantCulture), false);
                    Guid token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);
                    string resetUrl = String.Format(CultureInfo.InvariantCulture, "{0}/reset-password.aspx?token={1}", study.WebDiaryStudyBaseUrl.TrimEnd('/'), token);

                    message.SetSubject(Resources.Resource.EmailAcctForgotPswrdSubject.ToString());
                    emailBody = EmailHelper.GetForgotPasswordEmail(resetUrl, subject.Email.ToString(CultureInfo.InvariantCulture));
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "resetpassword")
                {
                    message.SetSubject(Resources.Resource.EmailAcctForgotResetSubject.ToString());
                    emailBody = EmailHelper.GetResetPasswordEmail(study, subject);
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "scheduledemailreminder")
                {
                    string emailSubject = string.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailScheduleSubject.ToString(), survey.DisplayName);
                    message.SetSubject(emailSubject);
                    emailBody = EmailHelper.GetScheduledEmail(study, survey);
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "emailupdate")
                {
                    message.SetSubject(Resources.Resource.EmailAcctChangeSubject.ToString());
                    emailBody = EmailHelper.GetChangeEmail(study, subject, Core.Constants.WDConstants.EmailType.EmailUpdate);
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "emaillanguageupdate")
                {
                    message.SetSubject(Resources.Resource.EmailAcctChangeSubject.ToString());
                    emailBody = EmailHelper.GetChangeEmail(study, subject, Core.Constants.WDConstants.EmailType.EmailLanguageUpdate);
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "languageupdate")
                {
                    message.SetSubject(Resources.Resource.EmailAcctChangeSubject.ToString());
                    emailBody = EmailHelper.GetChangeEmail(study, subject, Core.Constants.WDConstants.EmailType.LanguageUpdate);
                }
                message.SetBody(emailBody.ToString());

                try
                {
                    message.Send();
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    logger.Error(ex.Message, ex);
                    lblEmailSuccess.Visible = false;
                    lblEmailError.Visible = true;
                    return;
                }
                lblEmailSuccess.Visible = true;
                lblEmailError.Visible = false;
            }
        }

        protected List<string> GetAvailableLanguage()
        {
            List<string> languages = new List<string>();

            string resourcesPath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "App_GlobalResources");
            DirectoryInfo dirInfo = new DirectoryInfo(resourcesPath);
            foreach (FileInfo fi in dirInfo.GetFiles("*.*.resx", SearchOption.AllDirectories))
            {
                //take the cultureName from resx filename
                string cultureName = Path.GetFileNameWithoutExtension(fi.Name); //get rid of .resx
                cultureName = cultureName.Substring(cultureName.LastIndexOf(".") + 1);
                languages.Add(cultureName);
            }
            return languages;
        }
    }
}
