﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.SWAPI.SWData;
using System.Collections;
using WebDiary.SWAPI;
using System.Globalization;
using System.Xml.Linq;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal
{
    public class PDECommonTools
    {

        //Creates a clinfield that can be added to a clinRecord (form)
        //IT - column in the database 
        //val - value to be added
        public static ClinField generateClinField(String IT, String val)
        {
            ClinField newClinField = new ClinField();
            newClinField.krit = IT;
            newClinField.value = val;
            newClinField.edit = false;

            return newClinField;
        }


        //Sets up parameters for making a swapi call to execute a stored procedure
        public static ArrayList setupSwapiArgs(String krpt, String tz)
        {
            ArrayList PDEArgs = new ArrayList();
            PDEArgs.Add(HttpContext.Current.Server.UrlEncode(krpt));
            //Add device ID as NetPRO
            PDEArgs.Add("NetPRO");
            //Add current local date time
            DateTime now = DateTimeHelper.GetLocalTime();
            PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));

            PDEArgs.Add(tz);

            return PDEArgs;
        }


        //If study uses PDE_NetPro_LastDiaryTime.sql, pulls last diary time out of result
        public static DateTime? getLastDiaryTS(String krsu, String lastDiaryDataXML)
        {
            XElement xmlDoc = XElement.Parse(lastDiaryDataXML);
            DateTime? retVal = null;


            IEnumerable<XElement> elements = from el in xmlDoc.Elements("Diary")
                                             where el.Attribute("KRSU").Value.Equals(krsu)
                                             select el;

            if (elements.Count() != 0)
            {
                XAttribute attr = ((XElement)elements.FirstOrDefault()).Attribute("lastTS");
                if (attr != null)
                {
                    retVal = DateTime.Parse(attr.Value, CultureInfo.InvariantCulture);
                }
            }

            return retVal;
        }


        //Used to parse XML attributes for string values
        public static String getStringValue(String key, String sqlDataXML)
        {
            XElement xmlDoc = XElement.Parse(sqlDataXML);
            String retVal = "";

            try
            {
                retVal = (String)xmlDoc.Attribute(key);
            }
            catch (Exception e)
            {
                retVal = "";
            }

            return retVal;
        }


        //Used to parse XML attributes for datetime values
        public static DateTime? getDateTimeValue(String key, String sqlDataXML)
        {
            XElement xmlDoc = XElement.Parse(sqlDataXML);
            DateTime? retVal = null;

            try
            {
                retVal = DateTime.Parse((String)xmlDoc.Attribute(key), CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                retVal = null;
            }

            return retVal;
        }


        //Get report start date for clinRecord
        public static string getReportStartDate(PDEClinRecord clinRecord)
        {
            ClinField clinReportstartDate = clinRecord.clinRecord.sysvars.Find(item => item.sysvar == "SU.ReportStartDate");
            String reportStartDate = clinReportstartDate.value;

            return reportStartDate;
        }


    }
}