﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebDiary.StudyPortal
{
    public partial class CheckSession : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Server.Transfer("SurveySupport.aspx");
        }
    }
}