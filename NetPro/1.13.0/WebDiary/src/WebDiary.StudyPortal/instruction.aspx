﻿<%@ Page Language="C#" MasterPageFile="~/includes/masters/Site.Master" AutoEventWireup="true" CodeBehind="instruction.aspx.cs" Inherits="WebDiary.StudyPortal.Instruction" %>
<%@ Register assembly="WebDiary.Controls" namespace="WebDiary.Controls" tagprefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">

<asp:Literal ID="litResult" runat="server" />

    <!-- Instructions panel after adding a subject for both NetPRO and LogPad App. -->
    <cc1:DivPanel ID="instructionPanel" runat="server" Visible="false">
        <asp:Label ID="successMsg" runat="server" Text="<%$ Resources:Resource, SubmitAssignmentSucc%>"></asp:Label>
            <cc1:DivPanel ID="npInstruction" runat="server" Visible="false">
            <ul id="npStep" class="instruction">
            <li>
                <asp:Label ID="npStep1" runat="server"></asp:Label>
                </li>
                <li>
                <asp:Label ID="npStep2" runat="server"></asp:Label>
                </li>
            </ul>
            </cc1:DivPanel>
            <cc1:DivPanel ID="lpaInstruction" runat="server" Visible="false">
                <ul id="lpaStep" class="instruction">
                </ul>
            </cc1:DivPanel>
        <asp:CheckBox ID="dontShow" runat="server" oncheckedchanged="DontShow_CheckedChanged" AutoPostBack="true" Text="<%$ Resources:Resource, DontShow%>"/><br /><br />
        <asp:Button ID="goStudyWork" runat="server" OnClick="GoStudyWork_Click" Text="<%$ Resources:Resource, SubmitContinue%>" />
    </cc1:DivPanel>       
   
</asp:Content>


