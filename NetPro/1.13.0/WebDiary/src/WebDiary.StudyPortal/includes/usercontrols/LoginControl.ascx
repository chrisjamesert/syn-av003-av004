﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeBehind="LoginControl.ascx.cs" Inherits="WebDiary.StudyPortal.Includes.Usercontrols.LogOnControl" %>

<script type="text/javascript">
    function validateHtmlForm() 
    {
        var xtxtEmail = document.forms["form1"]["txtEmail"].value;
        var openBracket = xtxtEmail.indexOf("<");
        var openBracketChr = xtxtEmail.substring(openBracket, 2);
        var openBracketChrleg = openBracketChr.length;
        // alert("openBracketChrleg " + openBracketChrleg + "openBracketChr;" + openBracketChr);
        if (openBracketChrleg < 0)
        {
            // alert("Not a valid e-mail address now");
            // window.location = "../WebDiary.StudyPortal/login.aspx"
            // return false;    
        }
    }
</script>
<asp:ValidationSummary ID="vsErrors" CssClass="errors" runat="server" 
    DisplayMode="BulletList" EnableClientScript="False" />

<div id="unauthBrowser">
    <div id="unauthMsgArea">
        <asp:Label ID="lblUnauthorizedMessage" CssClass="unAuthorizedError" Visible="false" runat="server">
        </asp:Label>
    </div>
    <div id="authBrowserArea" runat="server">
    </div>      
</div>

<p>

    <asp:Label ID="SessionTimeoutLabel" runat="server" 
        Text="<%$ Resources:Resource, SessionExpiredLogin %>" Visible="False"></asp:Label>

    <asp:Label ID="lblLogOnMsgBody" runat="server" 
        Text="<%$ Resources:Resource, LoginMessageBody %>"></asp:Label>  
 </p>

<div class="form-row">
    <table>
        <tr>
            <td class="label"><asp:Label id="lblEmail" runat="server" Text="<%$ Resources:Resource, emailLbl %>"></asp:Label></td>
            <td class="field"><asp:TextBox ID="txtEmail" autocomplete="off" MaxLength="256" runat="server"/></td>
        </tr>
        <tr class="form-empty-row">
            <td></td>
            <td>
                   <asp:RegularExpressionValidator
                        id="regEmail"
                        ControlToValidate="txtEmail"
                        CssClass="nb-errors"
                        Display="Dynamic"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        Runat="server" 
                       ErrorMessage="<%$ Resources:Resource, rfvEmailNotValid %>" />
                    <asp:RequiredFieldValidator ID="rfvEmail" CssClass="nb-errors"
                        runat="server" 
                        ControlToValidate="txtEmail" 
                        ErrorMessage="<%$ Resources:Resource, LoginrfvEmail %>" 
                       Display="Dynamic" />
             </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label id="lblPassword" runat="server" 
                    Text="<%$ Resources:Resource, PswrdLabel %>">
                </asp:Label> 
            </td>
            <td class="field">
                            <!--Prevent autocomplete for latest browsers such as IE 11-->
                            <input id="prevent_autocomplete" name="prevent_autocomplete" type="password" class="hide"/>
                            <asp:TextBox ID="txtPassword" autocomplete="off" runat="server" MaxLength="64" TextMode="Password" oncopy="return false;" onpaste="return false;" oncut="return false;"/>
            </td>
        </tr>
        <tr class="form-empty-row">
            <td />
            <td>
                <asp:RequiredFieldValidator ID="rfvPassword" CssClass="nb-errors"
                    runat="server" 
                    ControlToValidate="txtPassword" 
                    ErrorMessage="<%$ Resources:Resource, LoginrfvPassword %>" 
                    Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right"><asp:Button ID="btnLogOn" runat="server" Text="<%$ Resources:Resource, LoginBtnText %>" /></td>
        </tr>
    </table>
</div>

<div class="button-row">
    <p>
        <a href="./forgot-password.aspx">
            <asp:Label ID="lblforgotPsswrd" runat="server" Text="<%$ Resources:Resource, ForgotPassword %>"></asp:Label>
        </a>
   </p>
   <p>
    <a href="#" onclick="$('#ForgotEmailMSG').slideDown();"><%= Resources.Resource.ForgotEmail %></a>
   </p>
   <p style="display:none" id="ForgotEmailMSG"><%= Resources.Resource.ForgotEmailMSG %></p>
</div>
