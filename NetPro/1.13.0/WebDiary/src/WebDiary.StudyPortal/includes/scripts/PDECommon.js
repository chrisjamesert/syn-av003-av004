﻿//Function is used for dynamic text
//Add Subject to display query items values in URL to be display for spans that contain a queryItem class
//and a name of a defined query item
function displayQueryItems() {
    var queryItems = $('span.queryItem');

    $.each(queryItems, function (index, queryItem) {
        var name = $(queryItem).attr('name'),
            queryValue;

        if (typeof name !== 'undefined') {
            //Get query value
            queryValue = getParam(name);

            //Update span with value
            if (typeof queryValue !== 'undefined') {
                $(queryItem).text(decodeURIComponent(queryValue));
            }
        }
    });
}


var PDECommonUtils = PDECommonUtils || {};

//Add back button validation
PDECommonUtils.AddBackButtionValidation = function (func) {
    // Override the Back Button
    $('#ResponseView_LayoutTemplate__previousZone__prevBtn').off('click');
    $('#ResponseView_LayoutTemplate__previousZone__prevBtn').on('click', func);
};

//Add next button validation
PDECommonUtils.AddNextButtionValidation = function (func) {
    // Override the Next Button
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').off('click');
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').on('click', func);
};

//Add finish button validation
PDECommonUtils.AddFinishButtionValidation = function (func) {
    // Override the Finish Button
    $('#ResponseView_LayoutTemplate__nextZone__finishBtn').off('click');
    $('#ResponseView_LayoutTemplate__nextZone__finishBtn').on('click', func);
};

//Enable next button
PDECommonUtils.enableNextButton = function() {
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').removeAttr("disabled");
};

//Enable finish button
PDECommonUtils.enableFinishButton = function () {
    $('#ResponseView_LayoutTemplate__nextZone__finishBtn').removeAttr("disabled");
};
			  
//Check if a string is a positive integer value
PDECommonUtils.isNormalInteger = function (str) {
    var n = Math.floor(Number(str));
    return String(n) === str && n >= 0;
};

//URL Decode string value
PDECommonUtils.urlDecode = function (str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
};


$(document).ready(function () {
    displayQueryItems();
});