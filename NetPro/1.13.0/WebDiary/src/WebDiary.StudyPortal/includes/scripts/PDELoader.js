
/**
 * Execute the specific function upon checking all require object(s) are defined. If a require object is missing and 
 * displayLoaderIcon is true, the waiting icon will be display indefinitely
 * IMPORTANT: This javascript file is never reference or used. The PDELoader is minified and exist in SurveySupport.aspx.cs.
 * This file is intended for reference
 * @param {Array|String} requireObjs is a list of string name of require objects/namespaces OR a string of a require object/namespace
 * @param {Function} func is the function to be executed once all require objects are verified to be defined
 * @param {Boolean} displayLoaderIcon indicates if a waiting icon is display while all required objects is being verified
 * @example <caption>Example of PDELoader Usage.</caption>
 *	PDELoader(['MASTER_MEDS', 'AddMedSelectionControl'], function() {
 *		AddMedSelectionControl();
 *	}, true);
 */
function PDELoader(requireObjs, func, displayLoaderIcon) {

    $(document).ready(function () {
		//Check if a string name of an object/namespace exist as a global
        var isObjectDefined = function (objName){
            var j,
                scope = window,
                scopeSplit = objName.split('.');

                //Check if object with nested namespace exist by
                //Iterating through each parent of the object
                for (j = 0; j < scopeSplit.length; j++)
                {
                    scope = scope[scopeSplit[j]];

                    if (typeof scope === 'undefined') {
                        return false;
                    }
                }

                return true;
            },        
            waitAndLoad = function () {
            var i;

            //Iterate through all the require objects
            for (i = 0; i < requireObjs.length; i++) {

                //If a require object is undefined, set isLoaded to false
                if (!isObjectDefined(requireObjs[i])) {

                    if(displayLoaderIcon){
                        //Display loading icon
                        $(parentTag).addClass("PDELoader");
                    }

                    //Not is loaded, wait again
                    setTimeout(waitAndLoad, 100);
                    return;
                }
            }

            //Execute function and enable next button
            if (typeof func === 'function') {
                try {                    
                    func();
                    PDECommonUtils.enableNextButton();
                    PDECommonUtils.enableFinishButton();

                    if(displayLoaderIcon){
                        //Hide loading icon
                        $(parentTag).removeClass('PDELoader');
                    }
                }
                catch (err) {
                    //Something bad already happen
                    console.error('PDELoader - Failed to execute function: ' + err);
                }
            }
            else {
                console.error('PDELoader - Invalid function: ' + func);
            }
        },
        scriptTags = document.getElementsByTagName('script'),
        //Get the parent element of the current script tag
        currentScriptTag = scriptTags[scriptTags.length - 1],
        parentTag = currentScriptTag.parentNode;

        //Disable next button
        $('#ResponseView_LayoutTemplate__nextZone__nextBtn').attr('disabled', 'disabled');
        //Disable finish button
        $('#ResponseView_LayoutTemplate__nextZone__finishBtn').attr('disabled', 'disabled');

        //If requireObjs is not an array, wrap it into an array
        if (!Array.isArray(requireObjs)) {
            requireObjs = [requireObjs];
        }

        //Add requirement check for PDECommonUtils
        requireObjs.push('PDECommonUtils');

        waitAndLoad();
    });
}