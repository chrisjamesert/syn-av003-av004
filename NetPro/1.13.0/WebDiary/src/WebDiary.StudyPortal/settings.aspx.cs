﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WebDiary.StudyPortal.Includes.Usercontrols;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using log4net;
using WebDiary.Core.Helpers;
using WebDiary.WDAPI;
using System.Globalization;

namespace WebDiary.StudyPortal
{
    public partial class Settings : BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(Settings));
        public string MapFile = WebDiary.Core.Helpers.StudyData.version.ProductPortalUrl.TrimEnd('/') + "/kml/wts.kmz";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Setup Home Link
                HyperLink homeLink = (HyperLink)Master.FindControl("HomeSettingsLink");
                homeLink.NavigateUrl = "~/";
                homeLink.Text = Resources.Resource.HomePageLnk;

                try
                {
                    lblEmail.Text = Session["userName"].ToString();

                    //Get subject's timezone
                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                        WebDiaryUser user = (WebDiaryUser)provider.GetUser(lblEmail.Text, true);

                        WDAPIObject wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                        if (!wd.IsSubjectEnabled(user.KrPT))
                        {
                            Response.Redirect("logout.aspx", false);
                            return;
                        }

                        TimeZoneRepository tzresp = new TimeZoneRepository(db);
                        TimeZones tz = tzresp.FindByStudyIdAndTimeZoneId(user.StudyId, user.TimeZone);

                        litCurrentTimezone.Text = tz.TZName;

                        foreach (var item in wd.GetTZList().list)
                        {
                            selectTimeZoneList.Items.Add(new ListItem(item.decode, item.code));
                            if (Int32.Parse(item.code, CultureInfo.InvariantCulture) == tz.TZId)
                                selectTimeZoneList.SelectedIndex = selectTimeZoneList.Items.Count - 1;
                        }
                    }
                }
                catch (System.Net.WebException webex)
                {
                    Session[WDError.ErrorMessageKey] = Resources.Resource.ServerDown;
                    Response.Redirect(WDError.Path);
                }
            }
        }

        protected void TimeZoneList_IndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    //Get the subject
                    WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(lblEmail.Text, true);
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject sub = subjectRep.FindByKrptAndStudy(user.KrPT, user.StudyId);

                    //Get the timezone
                    TimeZoneRepository tzresp = new TimeZoneRepository(db);
                    TimeZones tz = tzresp.FindByStudyIdAndTimeZoneId(user.StudyId, Int32.Parse(selectTimeZoneList.SelectedItem.Value, CultureInfo.InvariantCulture));

                    sub.TimeZone = tz.TZId;
                    db.SubmitChanges();

                    HttpContext.Current.Session[DateTimeHelper.TimeZoneId] = sub.TimeZone;

                    //Update current timezone
                    litCurrentTimezone.Text = tz.TZName;

                    PlaceholderUserNotice.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string err = Resources.Resource.ErrUpdatingTZ;
                logger.Error(ex.Message, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, false);
            }
        }

        protected void LnkResetPwd_Click(object sender, EventArgs e)
        {
            Guid token = new Guid();

            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                //Get the subject
                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                WebDiaryUser user = (WebDiaryUser)provider.GetUser(lblEmail.Text, true);

                token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);
            }           

            if (token != Guid.Empty)
            {
                Response.Redirect("change-password.aspx?token=" + token);
            }
            else
            {
                Page.Validators.Add(new ValidationError(Resources.Resource.ErrorProcessingUrRequest));
            }
        }
    }
}
