﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LPASetupInstruction.aspx.cs" Inherits="WebDiary.StudyPortal.LPASetupInstruction" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">      
          function printInstruction() {
              window.print();
          }
    </script>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    <link rel="stylesheet" type="text/css" href="~/includes/css/print.css" />
</head>
<body>
    <form id="form1" runat="server">

        <div id="envelope">
            <asp:Literal ID="litResult" runat="server" Visible="false" />
            <asp:Panel ID="container" runat="server">
            <div id="printBtnContainer" runat="server">
                <input type="button" runat="server" id="printOutBtn" onclick="printInstruction()"/>
            </div>        
            <div id="header" runat="server" class="title">
                <asp:Label ID="instructionTitle" runat="server"></asp:Label>        
            </div>
                        
            <div id="content" runat="server">
                <div id="insStart"><asp:Label ID="insMsg" runat="server" CssClass="openingLine"></asp:Label></div>

                <div id="insArea">
                    <asp:Panel ID="instruction" runat="server">
                        <ol id="step">
                            <li>
                                <asp:Label ID="step1" runat="server"></asp:Label>
                            </li>
                            <li>
                                <asp:Label ID="step2" runat="server"></asp:Label>
                            </li>
                            <li>
                                <asp:Label ID="step3" runat="server"></asp:Label>
                                <div>
                                    <img id="studyImg" src="img/lf/common/study.bmp" runat="server" class="alignImg" alt=""/>
                                </div>
                            </li>
                            <li>
                                <asp:Label ID="step4" runat="server"></asp:Label> 
                                <div>
                                    <img id="setupcodeImg" src="" runat="server" class="alignImg" alt=""/>
                                </div>
                            </li>

                            <li>
                                <asp:Label ID="step5" runat="server"></asp:Label>
                                <div>
                                    <img id="passwordImg" src="" runat="server" class="alignImg" alt=""/>
                                </div>
                                <div>
                                    <asp:Label ID="step5Note" runat="server"></asp:Label>
                                </div>
                            </li>

                            <li>
                                <asp:Label ID="step6" runat="server"></asp:Label>
                                <div>
                                    <img id="securityquestionImg" src="" runat="server" class="alignImg" alt=""/>
                                </div>
                            </li>
                        </ol>
                    </asp:Panel>
                </div>
                <div id="insEnd"><asp:Label ID="insEndMsg" runat="server" CssClass="openingLine"></asp:Label>
                </div>
            </div>
            <div id="footer">
                <ul>
                    <li class="last">&copy; <%= DateTime.Now.Year %> <asp:Label ID="phtCo" runat="server"></asp:Label></li>
                </ul>
            </div>
            </asp:Panel>

        </div>
    </form>
</body>
</html>
